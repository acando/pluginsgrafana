import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  console.log(data);

  let st_ups1 = [];
  let st_ups2 = [];
  let vol_ups1 = [];
  let vol_ups2 = [];
  let alm_ups1 = [];
  let alm_ups2 = [];
  let man_ups1 = [];
  let man_ups2 = [];
  let hab_ups1 = [];
  let hab_ups2 = [];
  let est_ups1 = [];
  let est_ups2 = [];

  let tabla_apa = [];
  let tabla_ope = [];
  let tabla_almp = [];
  let tabla_alm = [];
  let tabla_man = [];
  let tabla_des = [];
  for (let i = 1; i <= 6; i++) {
    st_ups1[i] = data.series.find(({ name }) => name === 'V_UPS1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    st_ups2[i] = data.series.find(({ name }) => name === 'V_UPS2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    vol_ups1[i] = data.series.find(({ name }) => name === 'V_UPS1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    vol_ups2[i] = data.series.find(({ name }) => name === 'V_UPS2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_ups1[i] = data.series.find(({ name }) => name === 'ALM_UPS1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_ups2[i] = data.series.find(({ name }) => name === 'ALM_UPS2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_ups1[i] = data.series.find(({ name }) => name === 'MANT_UPS1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_ups2[i] = data.series.find(({ name }) => name === 'MANT_UPS2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_ups1[i] = data.series.find(({ name }) => name === 'HABIL_UPS1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_ups2[i] = data.series.find(({ name }) => name === 'HABIL_UPS2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    vol_ups1[i] = (vol_ups1[i] * 1.73).toFixed(0);
    vol_ups2[i] = (vol_ups2[i] * 1.73).toFixed(0);
    if (st_ups1[i] === 0 || st_ups1[i] === null) {
      est_ups1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    } else {
      est_ups1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    }
    if (
      (st_ups1[i] === 1 && parseFloat(vol_ups1[i]) >= 427.7) ||
      (st_ups1[i] === 1 && parseFloat(vol_ups1[i]) <= 402.7)
    ) {
      est_ups1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    }
    if (alm_ups1[i] === 1) {
      est_ups1[i] = 'alarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    }
    if (man_ups1[i] === 1) {
      est_ups1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    }
    if (hab_ups1[i] === 0) {
      est_ups1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'UPS-1-' + i + 'B', est_ups1[i]));
    }
    if (st_ups2[i] === 0 || st_ups2[i] === null) {
      est_ups2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    } else {
      est_ups2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    }
    if (
      (st_ups2[i] === 1 && parseFloat(vol_ups2[i]) >= 427.7) ||
      (st_ups2[i] === 1 && parseFloat(vol_ups2[i]) <= 402.7)
    ) {
      est_ups2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    }
    if (alm_ups2[i] === 1) {
      est_ups2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    }
    if (man_ups2[i] === 1) {
      est_ups2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    }
    if (hab_ups2[i] === 0) {
      est_ups2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'UPS-2-' + i + 'B', est_ups2[i]));
    }
  }

  let st_gen = [];
  let man_gen = [];
  let hab_gen = [];
  let alm_gen = [];
  let fuel_gen = [];
  let est_gen = [];
  let gen = [4, 5, 6, 10, 11, 12];

  for (let i = 0; i < gen.length; i++) {
    let sis = 1;
    st_gen[gen[i]] = data.series.find(({ name }) => name === 'CUR_GEN' + gen[i])?.fields[1].state?.calcs?.lastNotNull;
    alm_gen[gen[i]] = data.series.find(({ name }) => name === 'ALM_GEN' + gen[i])?.fields[1].state?.calcs?.lastNotNull;
    fuel_gen[gen[i]] = data.series.find(
      ({ name }) => name === 'FUEL_GEN' + gen[i]
    )?.fields[1].state?.calcs?.lastNotNull;
    man_gen[gen[i]] = data.series.find(({ name }) => name === 'MANT_GEN' + gen[i])?.fields[1].state?.calcs?.lastNotNull;
    hab_gen[gen[i]] = data.series.find(
      ({ name }) => name === 'HABIL_GEN' + gen[i]
    )?.fields[1].state?.calcs?.lastNotNull;
    if (gen[i] > 6) {
      sis = 2;
    }
    if (st_gen[gen[i]] === 0 || st_gen[i] === null) {
      est_gen[gen[i]] = 'apagado';
      tabla_apa.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    } else {
      est_gen[gen[i]] = 'operativo';
      tabla_ope.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    }
    if (fuel_gen[gen[i]] < 30 || fuel_gen[gen[i]] > 80) {
      est_gen[gen[i]] = 'almpreventiva';
      tabla_almp.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    }
    if (alm_gen[gen[i]] === 1) {
      est_gen[gen[i]] = 'alarma';
      tabla_alm.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    }
    if (man_gen[gen[i]] === 1) {
      est_gen[gen[i]] = 'mantenimiento';
      tabla_man.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    }
    if (hab_gen[gen[i]] === 0) {
      est_gen[gen[i]] = 'deshabilitado';
      tabla_des.push(datostablaups(sis, 'ELÉCTRICO', 'GEN-' + gen[i], est_gen[gen[i]]));
    }
  }

  let st_pdu1 = [];
  let st_pdu2 = [];
  let vol_pdu1 = [];
  let vol_pdu2 = [];
  let alm_pdu1 = [];
  let alm_pdu2 = [];
  let man_pdu1 = [];
  let man_pdu2 = [];
  let hab_pdu1 = [];
  let hab_pdu2 = [];
  let est_pdu1 = [];
  let est_pdu2 = [];
  for (let i = 2; i <= 9; i++) {
    st_pdu1[i] = data.series.find(({ name }) => name === 'VOL_PDU1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    vol_pdu1[i] = data.series.find(({ name }) => name === 'VOL_PDU1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_pdu1[i] = data.series.find(({ name }) => name === 'ALM_PDU1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_pdu1[i] = data.series.find(({ name }) => name === 'MANT_PDU1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_pdu1[i] = data.series.find(({ name }) => name === 'HABIL_PDU1B' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (st_pdu1[i] === 0 || st_pdu1[i] === null) {
      est_pdu1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    } else {
      est_pdu1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    }
    if ((st_pdu1[i] > 0 && vol_pdu1[i] >= 436) || (st_pdu1[i] > 0 && vol_pdu1[i] <= 395)) {
      est_pdu1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    }
    if (alm_pdu1[i] === 1) {
      est_pdu1[i] = 'alarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    }
    if (man_pdu1[i] === 1) {
      est_pdu1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    }
    if (hab_pdu1[i] === 0) {
      est_pdu1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'PDU-01B-F' + i, est_pdu1[i]));
    }
  }
  for (let i = 1; i <= 9; i++) {
    st_pdu2[i] = data.series.find(({ name }) => name === 'VOL_PDU2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    vol_pdu2[i] = data.series.find(({ name }) => name === 'VOL_PDU2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_pdu2[i] = data.series.find(({ name }) => name === 'ALM_PDU2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_pdu2[i] = data.series.find(({ name }) => name === 'MANT_PDU2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_pdu2[i] = data.series.find(({ name }) => name === 'HABIL_PDU2B' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (st_pdu2[i] === 0 || st_pdu2[i] === null) {
      est_pdu2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    } else {
      est_pdu2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    }
    if ((st_pdu2[i] > 0 && vol_pdu2[i] >= 436) || (st_pdu2[i] > 0 && vol_pdu2[i] <= 395)) {
      est_pdu2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    }
    if (alm_pdu2[i] === 1) {
      est_pdu2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    }
    if (man_pdu2[i] === 1) {
      est_pdu2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    }
    if (hab_pdu2[i] === 0) {
      est_pdu2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'PDU-02B-F' + i, est_pdu2[i]));
    }
  }

  let st_ats = [];
  let man_ats = [];
  let hab_ats = [];
  let est_ats = [];
  let ats = [3, 7, 9, 4, 8, 10, 11];
  let ats1 = ['3B', 'CHI-7B', 'PDU-9B', '4B', 'CHI-8B', 'PDU-10B', 'SG-11B'];

  for (let i = 0; i < 7; i++) {
    let sis = 1;
    st_ats[ats[i]] = data.series.find(
      ({ name }) => name === 'ST_ATS' + ats[i] + 'B'
    )?.fields[1].state?.calcs?.lastNotNull;
    man_ats[ats[i]] = data.series.find(
      ({ name }) => name === 'MANT_ATS' + ats[i] + 'B'
    )?.fields[1].state?.calcs?.lastNotNull;
    hab_ats[ats[i]] = data.series.find(
      ({ name }) => name === 'HABIL_ATS' + ats[i] + 'B'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (st_ats[ats[i]] === 0 || st_ats[i] === null) {
      est_ats[ats[i]] = 'apagado';
      tabla_apa.push(datostablaups(sis, 'ELÉCTRICO', 'ATS-' + ats1[i], est_ats[ats[i]]));
    } else {
      est_ats[ats[i]] = 'operativo';
      tabla_ope.push(datostablaups(sis, 'ELÉCTRICO', 'ATS-' + ats1[i], est_ats[ats[i]]));
    }
    if (man_ats[ats[i]] === 1) {
      est_ats[ats[i]] = 'mantenimiento';
      tabla_man.push(datostablaups(sis, 'ELÉCTRICO', 'ATS-' + ats1[i], est_ats[ats[i]]));
    }
    if (hab_ats[ats[i]] === 0) {
      est_ats[ats[i]] = 'deshabilitado';
      tabla_des.push(datostablaups(sis, 'ELÉCTRICO', 'ATS-' + ats1[i], est_ats[ats[i]]));
    }
    if (ats1[i] === '4B' || ats1[i] === 'CHI-8B' || ats1[i] === 'PDU-10B') {
      sis = 2;
    }
  }

  let est_ups10;
  let st_ups10 = data.series.find(({ name }) => name === 'ST_UPS10')?.fields[1].state?.calcs?.lastNotNull;
  let vol_ups10 = data.series.find(({ name }) => name === 'ST_UPS10')?.fields[1].state?.calcs?.lastNotNull;
  let man_ups10 = data.series.find(({ name }) => name === 'MANT_UPS10')?.fields[1].state?.calcs?.lastNotNull;
  let hab_ups10 = data.series.find(({ name }) => name === 'HABIL_UPS10')?.fields[1].state?.calcs?.lastNotNull;
  if (st_ups10 === 0 || st_ups10 === null) {
    est_ups10 = 'apagado';
    tabla_apa.push(datostablaups('1 & 2', 'ELÉCTRICO', 'UPS-10KVA', est_ups10));
  } else {
    est_ups10 = 'operativo';
    tabla_ope.push(datostablaups('1 & 2', 'ELÉCTRICO', 'UPS-10KVA', est_ups10));
  }
  if ((st_ups10 !== 0 && vol_ups10 > 230) || (st_ups10 !== 0 && vol_ups10 < 185)) {
    est_ups10 = 'almpreventiva';
    tabla_almp.push(datostablaups('1 & 2', 'ELÉCTRICO', 'UPS-10KVA', est_ups10));
  }
  if (man_ups10 === 1) {
    est_ups10 = 'mantenimiento';
    tabla_man.push(datostablaups('1 & 2', 'ELÉCTRICO', 'UPS-10KVA', est_ups10));
  }
  if (hab_ups10 === 0) {
    est_ups10 = 'deshabilitado';
    tabla_des.push(datostablaups('1 & 2', 'ELÉCTRICO', 'UPS-10KVA', est_ups10));
  }

  ////// TABLEROS
  let st_psg1 = [];
  let st_psg2 = [];
  let alm_psg1 = [];
  let alm_psg2 = [];
  let trip_psg1 = [];
  let trip_psg2 = [];
  let man_psg1 = [];
  let man_psg2 = [];
  let hab_psg1 = [];
  let hab_psg2 = [];
  let est_psg1 = [];
  let est_psg2 = [];
  for (let i = 1; i <= 3; i++) {
    st_psg1[i] = data.series.find(({ name }) => name === 'POS_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_psg1[i] = data.series.find(({ name }) => name === 'ALM_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    trip_psg1[i] = data.series.find(({ name }) => name === 'TRIP_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_psg1[i] = data.series.find(({ name }) => name === 'MANT_PSG' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_psg1[i] = data.series.find(({ name }) => name === 'HABIL_PSG' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (st_psg1[i] === 0 || st_psg1[i] === null) {
      est_psg1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    } else {
      est_psg1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    }
    if (trip_psg1[i] === 1) {
      est_psg1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    }
    if (alm_psg1[i] === 1) {
      est_psg1[i] = 'almarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    }
    if (man_psg1[i] === 1) {
      est_psg1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    }
    if (hab_psg1[i] === 0) {
      est_psg1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'PSG-01B-' + i, est_psg1[i]));
    }
  }

  for (let i = 4; i <= 6; i++) {
    st_psg2[i] = data.series.find(({ name }) => name === 'POS_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_psg2[i] = data.series.find(({ name }) => name === 'ALM_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    trip_psg2[i] = data.series.find(({ name }) => name === 'TRIP_PSG_' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_psg2[i] = data.series.find(({ name }) => name === 'MANT_PSG' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_psg2[i] = data.series.find(({ name }) => name === 'HABIL_PSG' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (st_psg2[i] === 0 || st_psg2[i] === null) {
      est_psg2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    } else {
      est_psg2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    }
    if (trip_psg2[i] === 1) {
      est_psg2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    }
    if (alm_psg2[i] === 1) {
      est_psg2[i] = 'almarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    }
    if (man_psg2[i] === 1) {
      est_psg2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    }
    if (hab_psg2[i] === 0) {
      est_psg2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'PSG-02B-' + (i - 3), est_psg2[i]));
    }
  }

  let st_tats1 = [];
  let st_tats2 = [];
  let alm_tats1 = [];
  let alm_tats2 = [];
  let trip_tats1 = [];
  let trip_tats2 = [];
  let man_tats1 = [];
  let man_tats2 = [];
  let hab_tats1 = [];
  let hab_tats2 = [];
  let est_tats1 = [];
  let est_tats2 = [];
  for (let i = 0; i <= 2; i++) {
    st_tats1[i] = data.series.find(({ name }) => name === 'POS_TATS3B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_tats1[i] = data.series.find(({ name }) => name === 'ALM_TATS3B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    trip_tats1[i] = data.series.find(({ name }) => name === 'TRIP_TATS3B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_tats1[i] = data.series.find(({ name }) => name === 'MANT_TATS3B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_tats1[i] = data.series.find(({ name }) => name === 'HABIL_TATS3B' + i)?.fields[1].state?.calcs?.lastNotNull;
    let equi = '';
    if (i === 0) {
      equi = 'ATS-3B-EEQ';
    }
    if (i === 1) {
      equi = 'ATS-3B-A';
    }
    if (i === 2) {
      equi = 'ATS-3B-B';
    }
    if (st_tats1[i] === 0 || st_tats1[i] === null) {
      est_tats1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    } else {
      est_tats1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    }
    if (trip_tats1[i] === 1) {
      est_tats1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    }
    if (alm_tats1[i] === 1) {
      est_tats1[i] = 'almarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    }
    if (man_tats1[i] === 1) {
      est_tats1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    }
    if (hab_tats1[i] === 0) {
      est_tats1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats1[i]));
    }
  }

  for (let i = 0; i <= 2; i++) {
    st_tats2[i] = data.series.find(({ name }) => name === 'POS_TATS4B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    alm_tats2[i] = data.series.find(({ name }) => name === 'ALM_TATS4B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    trip_tats2[i] = data.series.find(({ name }) => name === 'TRIP_TATS4B_' + i)?.fields[1].state?.calcs?.lastNotNull;
    man_tats2[i] = data.series.find(({ name }) => name === 'MANT_TATS4B' + i)?.fields[1].state?.calcs?.lastNotNull;
    hab_tats2[i] = data.series.find(({ name }) => name === 'HABIL_TATS4B' + i)?.fields[1].state?.calcs?.lastNotNull;
    let equi = '';
    if (i === 0) {
      equi = 'ATS-4B-EEQ';
    }
    if (i === 1) {
      equi = 'ATS-4B-A';
    }
    if (i === 2) {
      equi = 'ATS-4B-B';
    }
    if (st_tats2[i] === 0 || st_tats2[i] === null) {
      est_tats2[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    } else {
      est_tats2[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    }
    if (trip_tats2[i] === 1) {
      est_tats2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    }
    if (alm_tats2[i] === 1) {
      est_tats2[i] = 'almarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    }
    if (man_tats1[i] === 1) {
      est_tats1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    }
    if (hab_tats2[i] === 0) {
      est_tats2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', equi, est_tats2[i]));
    }
  }

  let st_tdb = [];
  let alm_tdb = [];
  let trip_tdb = [];
  let man_tdb = [];
  let hab_tdb = [];
  let est_tdb = [];
  for (let i = 0; i < 3; i++) {
    st_tdb[i] = data.series.find(({ name }) => name === 'POS_TDB_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tdb[i] = data.series.find(({ name }) => name === 'ALM_TDB_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tdb[i] = data.series.find(({ name }) => name === 'TRIP_TDB_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tdb[i] = data.series.find(({ name }) => name === 'MANT_TDB' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tdb[i] = data.series.find(({ name }) => name === 'HABIL_TDB' + [i])?.fields[1].state?.calcs?.lastNotNull;
    if (st_tdb[i] === 0 || st_tdb[i] === null) {
      est_tdb[i] = 'apagado';
      tabla_apa.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    } else {
      est_tdb[i] = 'operativo';
      tabla_ope.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    }
    if (alm_tdb[i] === 1) {
      est_tdb[i] = 'alarma';
      tabla_alm.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    }
    if (trip_tdb[i] === 1) {
      est_tdb[i] = 'almpreventiva';
      tabla_almp.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    }
    if (man_tdb[i] === 1) {
      est_tdb[i] = 'mantenimiento';
      tabla_man.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    }
    if (hab_tdb[i] === 0) {
      est_tdb[i] = 'deshabilitado';
      tabla_des.push(datostablaups('1 & 2', 'ELÉCTRICO', 'TDB-0' + [i], est_tdb[i]));
    }
  }

  let st_tdps1 = [];
  let st_tdps2 = [];
  let alm_tdps1 = [];
  let alm_tdps2 = [];
  let trip_tdps1 = [];
  let trip_tdps2 = [];
  let man_tdps1 = [];
  let man_tdps2 = [];
  let hab_tdps1 = [];
  let hab_tdps2 = [];
  let est_tdps1 = [];
  let est_tdps2 = [];
  for (let i = 0; i < 10; i++) {
    st_tdps1[i] = data.series.find(({ name }) => name === 'POS_TDP1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tdps1[i] = data.series.find(({ name }) => name === 'ALM_TDP1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tdps1[i] = data.series.find(({ name }) => name === 'TRIP_TDP1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tdps1[i] = data.series.find(({ name }) => name === 'MANT_TDP1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tdps1[i] = data.series.find(({ name }) => name === 'HABIL_TDP1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    st_tdps2[i] = data.series.find(({ name }) => name === 'POS_TDP2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tdps2[i] = data.series.find(({ name }) => name === 'ALM_TDP2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tdps2[i] = data.series.find(({ name }) => name === 'TRIP_TDP2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tdps2[i] = data.series.find(({ name }) => name === 'MANT_TDP2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tdps2[i] = data.series.find(({ name }) => name === 'HABIL_TDP2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    if (i !== 9) {
      if (st_tdps1[i] === 0 || st_tdps1[i] === null) {
        est_tdps1[i] = 'apagado';
        tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      } else {
        est_tdps1[i] = 'operativo';
        tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      }
      if (alm_tdps1[i] === 1) {
        est_tdps1[i] = 'alarma';
        tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      }
      if (trip_tdps1[i] === 1) {
        est_tdps1[i] = 'almpreventiva';
        tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      }
      if (man_tdps1[i] === 1) {
        est_tdps1[i] = 'mantenimiento';
        tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      }
      if (hab_tdps1[i] === 0) {
        est_tdps1[i] = 'deshabilitado';
        tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'TDP-01B-' + [i], est_tdps1[i]));
      }
    }

    if (st_tdps2[i] === 0 || st_tdps2[i] === null) {
      est_tdps2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    } else {
      est_tdps2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    }
    if (alm_tdps2[i] === 1) {
      est_tdps2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    }
    if (trip_tdps2[i] === 1) {
      est_tdps2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    }
    if (man_tdps2[i] === 1) {
      est_tdps2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    }
    if (hab_tdps2[i] === 0) {
      est_tdps2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'TDP-02B-' + [i], est_tdps2[i]));
    }
  }

  let st_tupsins1 = [];
  let st_tupsins2 = [];
  let alm_tupsins1 = [];
  let alm_tupsins2 = [];
  let trip_tupsins1 = [];
  let trip_tupsins2 = [];
  let man_tupsins1 = [];
  let man_tupsins2 = [];
  let hab_tupsins1 = [];
  let hab_tupsins2 = [];
  let est_tupsins1 = [];
  let est_tupsins2 = [];
  for (let i = 0; i <= 6; i++) {
    st_tupsins1[i] = data.series.find(
      ({ name }) => name === 'POS_TUPSIN1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    alm_tupsins1[i] = data.series.find(
      ({ name }) => name === 'ALM_TUPSIN1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    trip_tupsins1[i] = data.series.find(
      ({ name }) => name === 'TRIP_TUPSIN1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    man_tupsins1[i] = data.series.find(
      ({ name }) => name === 'MANT_TUPSIN1B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    hab_tupsins1[i] = data.series.find(
      ({ name }) => name === 'HABIL_TUPSIN1B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    st_tupsins2[i] = data.series.find(
      ({ name }) => name === 'POS_TUPSIN2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    alm_tupsins2[i] = data.series.find(
      ({ name }) => name === 'ALM_TUPSIN2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    trip_tupsins2[i] = data.series.find(
      ({ name }) => name === 'TRIP_TUPSIN2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    man_tupsins2[i] = data.series.find(
      ({ name }) => name === 'MANT_TUPSIN2B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    hab_tupsins2[i] = data.series.find(
      ({ name }) => name === 'HABIL_TUPSIN2B ' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    if (st_tupsins1[i] === 0 || st_tupsins1[i] === null) {
      est_tupsins1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    } else {
      est_tupsins1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    }
    if (alm_tupsins1[i] === 1) {
      est_tupsins1[i] = 'alarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    }
    if (trip_tupsins1[i] === 1) {
      est_tupsins1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    }
    if (man_tupsins1[i] === 1) {
      est_tupsins1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    }
    if (hab_tupsins1[i] === 0) {
      est_tupsins1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-IN-01B-' + [i], est_tupsins1[i]));
    }
    if (st_tupsins2[i] === 0 || st_tupsins2[i] === null) {
      est_tupsins2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    } else {
      est_tupsins2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    }
    if (alm_tupsins2[i] === 1) {
      est_tupsins2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    }
    if (trip_tupsins2[i] === 1) {
      est_tupsins2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    }
    if (man_tupsins2[i] === 1) {
      est_tupsins2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    }
    if (hab_tupsins2[i] === 0) {
      est_tupsins2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-IN-02B-' + [i], est_tupsins2[i]));
    }
  }

  let st_tupsouts1 = [];
  let st_tupsouts2 = [];
  let alm_tupsouts1 = [];
  let alm_tupsouts2 = [];
  let trip_tupsouts1 = [];
  let trip_tupsouts2 = [];
  let man_tupsouts1 = [];
  let man_tupsouts2 = [];
  let hab_tupsouts1 = [];
  let hab_tupsouts2 = [];
  let est_tupsouts1 = [];
  let est_tupsouts2 = [];
  for (let i = 1; i <= 8; i++) {
    st_tupsouts1[i] = data.series.find(
      ({ name }) => name === 'POS_TUPSOUT1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    alm_tupsouts1[i] = data.series.find(
      ({ name }) => name === 'ALM_TUPSOUT1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    trip_tupsouts1[i] = data.series.find(
      ({ name }) => name === 'TRIP_TUPSOUT1B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    man_tupsouts1[i] = data.series.find(
      ({ name }) => name === 'MANT_TUPSOUT1B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    hab_tupsouts1[i] = data.series.find(
      ({ name }) => name === 'HABIL_TUPSOUT1B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    st_tupsouts2[i] = data.series.find(
      ({ name }) => name === 'POS_TUPSOUT2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    alm_tupsouts2[i] = data.series.find(
      ({ name }) => name === 'ALM_TUPSOUT2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    trip_tupsouts2[i] = data.series.find(
      ({ name }) => name === 'TRIP_TUPSOUT2B_' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    man_tupsouts2[i] = data.series.find(
      ({ name }) => name === 'MANT_TUPSOUT2B' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    hab_tupsouts2[i] = data.series.find(
      ({ name }) => name === 'HABIL_TUPSOUT2B ' + [i]
    )?.fields[1].state?.calcs?.lastNotNull;
    if (st_tupsouts1[i] === 0 || st_tupsouts1[i] === null) {
      est_tupsouts1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    } else {
      est_tupsouts1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    }
    if (alm_tupsouts1[i] === 1) {
      est_tupsouts1[i] = 'alarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    }
    if (trip_tupsouts1[i] === 1) {
      est_tupsouts1[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    }
    if (man_tupsouts1[i] === 1) {
      est_tupsouts1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    }
    if (hab_tupsouts1[i] === 0) {
      est_tupsouts1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'T-UPS-OUT-01B-' + [i], est_tupsouts1[i]));
    }
    if (st_tupsouts2[i] === 0 || st_tupsouts2[i] === null) {
      est_tupsouts2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    } else {
      est_tupsouts2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    }
    if (alm_tupsouts2[i] === 1) {
      est_tupsouts2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    }
    if (trip_tupsouts2[i] === 1) {
      est_tupsouts2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    }
    if (man_tupsouts2[i] === 1) {
      est_tupsouts2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    }
    if (hab_tupsouts2[i] === 0) {
      est_tupsouts2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'T-UPS-OUT-02B-' + [i], est_tupsouts2[i]));
    }
  }

  let st_tpdus1 = [];
  let st_tpdus2 = [];
  let alm_tpdus1 = [];
  let alm_tpdus2 = [];
  let trip_tpdus1 = [];
  let trip_tpdus2 = [];
  let man_tpdus1 = [];
  let man_tpdus2 = [];
  let hab_tpdus1 = [];
  let hab_tpdus2 = [];
  let est_tpdus1 = [];
  let est_tpdus2 = [];
  for (let i = 0; i <= 11; i++) {
    st_tpdus1[i] = data.series.find(({ name }) => name === 'POS_TPDU1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tpdus1[i] = data.series.find(({ name }) => name === 'ALM_TPDU1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tpdus1[i] = data.series.find(({ name }) => name === 'TRIP_TPDU1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tpdus1[i] = data.series.find(({ name }) => name === 'MANT_TPDU1B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tpdus1[i] = data.series.find(({ name }) => name === 'HABIL_TPDU1B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    st_tpdus2[i] = data.series.find(({ name }) => name === 'POS_TPDU2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tpdus2[i] = data.series.find(({ name }) => name === 'ALM_TPDU2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tpdus2[i] = data.series.find(({ name }) => name === 'TRIP_TPDU2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tpdus2[i] = data.series.find(({ name }) => name === 'MANT_TPDU2B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tpdus2[i] = data.series.find(({ name }) => name === 'HABIL_TPDU2B ' + [i])?.fields[1].state?.calcs?.lastNotNull;
    if (st_tpdus1[i] === 0 || st_tpdus1[i] === null) {
      est_tpdus1[i] = 'apagado';
      tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    } else {
      est_tpdus1[i] = 'operativo';
      tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    }
    if (alm_tpdus1[i] === 1) {
      est_tpdus1[i] = 'alarma';
      tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    }
    if (trip_tpdus1[i] === 1) {
      est_tpdus1[i] = 'alarma_prev';
      tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    }
    if (man_tpdus1[i] === 1) {
      est_tpdus1[i] = 'mantenimiento';
      tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    }
    if (hab_tpdus1[i] === 0) {
      est_tpdus1[i] = 'deshabilitado';
      tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'T-PDU-01B-' + [i], est_tpdus1[i]));
    }
    if (st_tpdus2[i] === 0 || st_tpdus2[i] === null) {
      est_tpdus2[i] = 'apagado';
      tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    } else {
      est_tpdus2[i] = 'operativo';
      tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    }
    if (alm_tpdus2[i] === 1) {
      est_tpdus2[i] = 'alarma';
      tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    }
    if (trip_tpdus2[i] === 1) {
      est_tpdus2[i] = 'almpreventiva';
      tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    }
    if (man_tpdus2[i] === 1) {
      est_tpdus2[i] = 'mantenimiento';
      tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    }
    if (hab_tpdus2[i] === 0) {
      est_tpdus2[i] = 'deshabilitado';
      tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'T-PDU-02B-' + [i], est_tpdus2[i]));
    }
  }

  let st_tchis1 = [];
  let st_tchis2 = [];
  let alm_tchis1 = [];
  let alm_tchis2 = [];
  let trip_tchis1 = [];
  let trip_tchis2 = [];
  let man_tchis1 = [];
  let man_tchis2 = [];
  let hab_tchis1 = [];
  let hab_tchis2 = [];
  let est_tchis1 = [];
  let est_tchis2 = [];
  for (let i = 0; i <= 22; i++) {
    st_tchis1[i] = data.series.find(({ name }) => name === 'POS_TCHI1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tchis1[i] = data.series.find(({ name }) => name === 'ALM_TCHI1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tchis1[i] = data.series.find(({ name }) => name === 'TRIP_TCHI1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tchis1[i] = data.series.find(({ name }) => name === 'MANT_TCHI1B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tchis1[i] = data.series.find(({ name }) => name === 'HABIL_TCHI1B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    st_tchis2[i] = data.series.find(({ name }) => name === 'POS_TCHI2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tchis2[i] = data.series.find(({ name }) => name === 'ALM_TCHI2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tchis2[i] = data.series.find(({ name }) => name === 'TRIP_TCHI2B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tchis2[i] = data.series.find(({ name }) => name === 'MANT_TCHI2B' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tchis2[i] = data.series.find(({ name }) => name === 'HABIL_TCHI2B ' + [i])?.fields[1].state?.calcs?.lastNotNull;
    if (i !== 12) {
      if (st_tchis1[i] === 0 || st_tchis1[i] === null) {
        est_tchis1[i] = 'apagado';
        tabla_apa.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      } else {
        est_tchis1[i] = 'operativo';
        tabla_ope.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      }
      if (alm_tchis1[i] === 1) {
        est_tchis1[i] = 'alarma';
        tabla_alm.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      }
      if (trip_tchis1[i] === 1) {
        est_tchis1[i] = 'alarma_prev';
        tabla_almp.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      }
      if (man_tchis1[i] === 1) {
        est_tchis1[i] = 'mantenimiento';
        tabla_man.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      }
      if (hab_tchis1[i] === 0) {
        est_tchis1[i] = 'deshabilitado';
        tabla_des.push(datostablaups(1, 'ELÉCTRICO', 'T-CHI-01B-' + [i], est_tchis1[i]));
      }
      if (st_tchis2[i] === 0 || st_tchis2[i] === null) {
        est_tchis2[i] = 'apagado';
        tabla_apa.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      } else {
        est_tchis2[i] = 'operativo';
        tabla_ope.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      }
      if (alm_tchis2[i] === 1) {
        est_tchis2[i] = 'alarma';
        tabla_alm.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      }
      if (trip_tchis2[i] === 1) {
        est_tchis2[i] = 'almpreventiva';
        tabla_almp.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      }
      if (man_tchis2[i] === 1) {
        est_tchis2[i] = 'mantenimiento';
        tabla_man.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      }
      if (hab_tchis2[i] === 0) {
        est_tchis2[i] = 'deshabilitado';
        tabla_des.push(datostablaups(2, 'ELÉCTRICO', 'T-CHI-02B-' + [i], est_tchis2[i]));
      }
    }
  }

  let st_tsg = [];
  let alm_tsg = [];
  let trip_tsg = [];
  let man_tsg = [];
  let hab_tsg = [];
  let est_tsg = [];
  for (let i = 0; i <= 5; i++) {
    st_tsg[i] = data.series.find(({ name }) => name === 'POS_TSG1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    alm_tsg[i] = data.series.find(({ name }) => name === 'ALM_TSG1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    trip_tsg[i] = data.series.find(({ name }) => name === 'TRIP_TSG1B_' + [i])?.fields[1].state?.calcs?.lastNotNull;
    man_tsg[i] = data.series.find(({ name }) => name === 'MANT_TSG' + [i])?.fields[1].state?.calcs?.lastNotNull;
    hab_tsg[i] = data.series.find(({ name }) => name === 'HABIL_TSG' + [i])?.fields[1].state?.calcs?.lastNotNull;
    if (st_tsg[i] === 0 || st_tsg[i] === null) {
      est_tsg[i] = 'apagado';
      tabla_apa.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    } else {
      est_tsg[i] = 'operativo';
      tabla_ope.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    }
    if (alm_tsg[i] === 1) {
      est_tsg[i] = 'alarma';
      tabla_alm.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    }
    if (trip_tsg[i] === 1) {
      est_tsg[i] = 'almpreventiva';
      tabla_almp.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    }
    if (man_tsg[i] === 1) {
      est_tsg[i] = 'mantenimiento';
      tabla_man.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    }
    if (hab_tsg[i] === 0) {
      est_tsg[i] = 'deshabilitado';
      tabla_des.push(datostablaups('1 & 2', 'ELÉCTRICO', 'T-SG-01B-' + [i], est_tsg[i]));
    }
  }

  function datostablaups(sistema: any, tipo: any, equipo: any, dato: any) {
    let tabla = (
      <tr>
        <td>B</td>
        <td>{sistema}</td>
        <td>{tipo}</td>
        <td>{equipo}</td>
        <td id={dato}> {dato.toUpperCase()} </td>
      </tr>
    );
    return tabla;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${'100%'}px;
          height: ${'100%'}px;
        `
      )}
    >
      <div id="table-scroll">
        <table className="table">
          <thead>
            <tr>
              <th>FASE</th>
              <th>SISTEMA</th>
              <th>TIPO DE SISTEMA</th>
              <th>EQUIPO </th>
              <th>ESTADO </th>
            </tr>
          </thead>
          <tbody>
            {tabla_alm}
            {tabla_almp}
            {tabla_man}
            {tabla_des}
            {tabla_ope}
            {tabla_apa}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
