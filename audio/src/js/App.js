import React from "react";
import { RadarChart } from "./RadarChart";
import "../css/styles.css";

export default function App() {
  return (
    <div className="App">
      <h1>Radar Chart</h1>
      <RadarChart />
    </div>
  );
}
