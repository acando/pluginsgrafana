import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  ///  UPS IN-OUT
  let volin_ups = data.series.find(({ name }) => name === 'Average DATA.VOL_IN.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  volin_ups = volin_ups.toFixed(0);
  let volout_ups = data.series.find(({ name }) => name === 'Average DATA.VOL_OUT.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  volout_ups = volout_ups.toFixed(0);

  /// VOL CMT
  let vol_cmtp = data.series.find(({ name }) => name === 'VOL_CMT')?.fields[1].state?.calcs?.lastNotNull;

  ///// UMAS
  let tsum_uma = data.series.find(({ name }) => name === 'TSUM_UMA')?.fields[1].state?.calcs?.mean;
  let tret_uma = data.series.find(({ name }) => name === 'TRET_UMA')?.fields[1].state?.calcs?.mean;
  if (tsum_uma === null) {
    tsum_uma = 0;
  } else {
    tsum_uma = (tsum_uma / 10).toFixed(1);
  }
  if (tret_uma === null) {
    tret_uma = 0;
  } else {
    tret_uma = (tret_uma / 10).toFixed(1);
  }

  //// CHILLERS
  let tsum_chill;
  let tret_chill;

  let st_chill1 = data.series.find(({ name }) => name === 'ST_CHI1')?.fields[1].state?.calcs?.lastNotNull;
  let st_chill2 = data.series.find(({ name }) => name === 'ST_CHI2')?.fields[1].state?.calcs?.lastNotNull;
  let st_chill3 = data.series.find(({ name }) => name === 'ST_CHI3')?.fields[1].state?.calcs?.lastNotNull;
  let st_chill4 = data.series.find(({ name }) => name === 'ST_CHI4')?.fields[1].state?.calcs?.lastNotNull;
  let tsum_chill1 = data.series.find(({ name }) => name === 'TSUM_CHI1')?.fields[1].state?.calcs?.lastNotNull;
  let tsum_chill2 = data.series.find(({ name }) => name === 'TSUM_CHI2')?.fields[1].state?.calcs?.lastNotNull;
  let tsum_chill3 = data.series.find(({ name }) => name === 'TSUM_CHI3')?.fields[1].state?.calcs?.lastNotNull;
  let tsum_chill4 = data.series.find(({ name }) => name === 'TSUM_CHI4')?.fields[1].state?.calcs?.lastNotNull;
  let tret_chill1 = data.series.find(({ name }) => name === 'TRET_CHI1')?.fields[1].state?.calcs?.lastNotNull;
  let tret_chill2 = data.series.find(({ name }) => name === 'TRET_CHI2')?.fields[1].state?.calcs?.lastNotNull;
  let tret_chill3 = data.series.find(({ name }) => name === 'TRET_CHI3')?.fields[1].state?.calcs?.lastNotNull;
  let tret_chill4 = data.series.find(({ name }) => name === 'TRET_CHI4')?.fields[1].state?.calcs?.lastNotNull;

  if (st_chill1 === 1) {
    tsum_chill = tsum_chill1.toFixed(1);
    tret_chill = tret_chill1.toFixed(1);
  } else if (st_chill2 === 1) {
    tsum_chill = tsum_chill2.toFixed(1);
    tret_chill = tret_chill2.toFixed(1);
  } else if (st_chill3 === 1) {
    tsum_chill = tsum_chill3.toFixed(1);
    tret_chill = tret_chill3.toFixed(1);
  } else if (st_chill4 === 1) {
    tsum_chill = tsum_chill4.toFixed(1);
    tret_chill = tret_chill4.toFixed(1);
  } else {
    tsum_chill = 0;
    tret_chill = 0;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox="0 0 508 71.437503"
        id="svg20059"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        xmlns="http://www.w3.org/2000/svg"
        //{...props}
      >
        <defs id="defs20056">
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1984-3-5">
            <path
              d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
              id="path1986-8-26"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2072-7-9">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path2074-3-1"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1163-2-3">
            <path
              d="M131.347 106.211v-6.425l-23.718 6.614s8.126 31.75 8.504 32.79c.162.444 21.545-1.323 21.261-.095-.283 1.229 10.017-4.819 9.639-4.819h-3.213l-5.197-6.615z"
              id="path1165-5-5"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20198">
            <path
              d="M131.347 106.211v-6.425l-23.718 6.614s8.126 31.75 8.504 32.79c.162.444 21.545-1.323 21.261-.095-.283 1.229 10.017-4.819 9.639-4.819h-3.213l-5.197-6.615z"
              id="path20196"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20257">
            <path
              d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
              id="path20255"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20261">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path20259"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath835-8-7">
            <path
              d="M96.618 96.28l-37.15-13.897V92.94H44.9l-4.96 27.807 32.334 7.637z"
              id="path837-7-9"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.565}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={21.3543}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath835-9">
            <path
              d="M96.618 96.28l-37.15-13.897V92.94H44.9l-4.96 27.807 32.334 7.637z"
              id="path837-0"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.565}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={21.3543}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath835-87-0">
            <path
              d="M96.618 96.28l-37.15-13.897V92.94H44.9l-4.96 27.807 32.334 7.637z"
              id="path837-76-5"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.565}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={21.3543}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
            />
          </clipPath>
          <linearGradient
            gradientTransform="matrix(.5951 0 0 .60261 -87.453 139.25)"
            gradientUnits="userSpaceOnUse"
            y2={273.52362}
            x2={258.93073}
            y1={223.38387}
            x1={258.93073}
            id="linearGradient8863"
            xlinkHref="#linearGradient8861"
          />
          <linearGradient id="linearGradient8861">
            <stop id="stop8857" offset={0} stopColor="#178299" stopOpacity={1} />
            <stop id="stop8859" offset={1} stopColor="#178299" stopOpacity={0} />
          </linearGradient>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath24362">
            <path
              id="lpe_path-effect24366"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath11875">
            <path
              id="lpe_path-effect11879"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect24410">
            <path
              id="lpe_path-effect24410"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath66030-1">
            <path
              d="M152.248 105.554l18.403 61.903 18.556-14.753-2.89-34.373-14.6-9.887-20.077-3.346"
              id="path66032-8"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect21225-4">
            <path
              id="lpe_path-effect21225"
              className="powerclip"
              d="M117.214 101.986h71.83v73.178h-71.83zm35.034 3.568l18.403 61.903 18.556-14.753-2.89-34.373-14.6-9.887-20.077-3.346"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect9683">
            <path
              id="lpe_path-effect9683"
              className="powerclip"
              d="M106.867 93.712h89.847v88.242h-89.847zm4.07 45.728l5.116 37.271 52.18.877 10.817-9.208-5.7-5.7c-28.695 18.124-49.986.628-53.642-23.24z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22263">
            <path
              id="lpe_path-effect22263"
              className="powerclip"
              d="M648.863 153.786h37.684v37.505h-37.684zm4.856 25.072l13.296 10.98 13.824-8.334 3.175-8.07-5.225-11.641z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22267">
            <path
              id="lpe_path-effect22267"
              className="powerclip"
              d="M652.175 156.677h31.597v31.663h-31.597zm1.941 22.578l4.697 5.358 5.225-5.82-2.58-3.242z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22271">
            <path
              id="lpe_path-effect22271"
              className="powerclip"
              d="M651.729 148.954h39.88v47.417h-39.88zm19.85 39.327l.42 1.965 1.918-.515-.702-1.94zm2.629-.84l.728 1.901 1.736-.86-1.088-1.836zm2.282-1.158l1.058 1.637 1.571-1.108-1.29-1.57zm2.133-1.67l1.356 1.554 1.356-1.306-1.57-1.389zm1.836-1.918l1.521 1.29 1.042-1.505-1.588-1.059z"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22275">
            <path
              id="lpe_path-effect22275"
              className="powerclip"
              d="M645.941 171.047h22.634v21.443h-22.634zm9.828 12.65a1.38 1.427 0 00-1.38-1.426 1.38 1.427 0 00-1.38 1.427 1.38 1.427 0 001.38 1.426 1.38 1.427 0 001.38-1.426z"
              opacity={0.5}
              fill="#000"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.517861}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath19972-0">
            <path
              d="M659.54 185.87c.926-1.323 18.72-23.548 18.72-23.548l5.225 10.054-5.953 11.046-12.237 4.961z"
              id="path19974-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20540-8">
            <path
              d="M660.334 185.142l17.793-23.283 4.895 5.49.066 6.284-.86 5.556-8.797 7.673-7.475.463z"
              id="path20542-4"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20929-5">
            <path
              d="M664.633 147.373l.926 8.863 17.066 12.965 1.852 11.245 6.482 2.381 5.424-25.4z"
              id="path20931-3"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient4462"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(.70856 0 0 .71958 -219.148 -126.274)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <linearGradient id="linearGradient15175">
            <stop offset={0} id="stop15171" stopColor="#178497" stopOpacity={1} />
            <stop offset={1} id="stop15173" stopColor="#178497" stopOpacity={0} />
          </linearGradient>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1163">
            <path
              d="M131.347 106.211v-6.425l-23.718 6.614s8.126 31.75 8.504 32.79c.162.444 21.545-1.323 21.261-.095-.283 1.229 10.017-4.819 9.639-4.819h-3.213l-5.197-6.615z"
              id="path1165"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1984">
            <path
              d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
              id="path1986"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2072">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path2074"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient15215"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-.70078 0 0 .71958 442.42 -126.436)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient84362"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-.70856 0 0 .71958 417.526 -126.274)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient84432"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(.7027 0 0 .72155 -116.87 -126.843)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22279">
            <path
              d="M664.766 146.447c.132 1.455.926 9.393.926 9.393l23.548 25.929-9.922 16.404-28.906-3.704c-7.221-10.956-9.047-23.11-6.747-36.182z"
              id="path22281"
              display="block"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22263-1">
            <path
              id="lpe_path-effect22263-2"
              className="powerclip"
              d="M648.863 153.786h37.684v37.505h-37.684zm4.856 25.072l13.296 10.98 13.824-8.334 3.175-8.07-5.225-11.641z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22267-2">
            <path
              id="lpe_path-effect22267-2"
              className="powerclip"
              d="M652.175 156.677h31.597v31.663h-31.597zm1.941 22.578l4.697 5.358 5.225-5.82-2.58-3.242z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22271-7">
            <path
              id="lpe_path-effect22271-0"
              className="powerclip"
              d="M651.729 148.954h39.88v47.417h-39.88zm19.85 39.327l.42 1.965 1.918-.515-.702-1.94zm2.629-.84l.728 1.901 1.736-.86-1.088-1.836zm2.282-1.158l1.058 1.637 1.571-1.108-1.29-1.57zm2.133-1.67l1.356 1.554 1.356-1.306-1.57-1.389zm1.836-1.918l1.521 1.29 1.042-1.505-1.588-1.059z"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect22275-6">
            <path
              id="lpe_path-effect22275-4"
              className="powerclip"
              d="M645.941 171.047h22.634v21.443h-22.634zm9.828 12.65a1.38 1.427 0 00-1.38-1.426 1.38 1.427 0 00-1.38 1.427 1.38 1.427 0 001.38 1.426 1.38 1.427 0 001.38-1.426z"
              opacity={0.5}
              fill="#000"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.517861}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath19972-0-1">
            <path
              d="M659.54 185.87c.926-1.323 18.72-23.548 18.72-23.548l5.225 10.054-5.953 11.046-12.237 4.961z"
              id="path19974-5-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20540-8-3">
            <path
              d="M660.334 185.142l17.793-23.283 4.895 5.49.066 6.284-.86 5.556-8.797 7.673-7.475.463z"
              id="path20542-4-6"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20929-5-3">
            <path
              d="M664.633 147.373l.926 8.863 17.066 12.965 1.852 11.245 6.482 2.381 5.424-25.4z"
              id="path20931-3-2"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect88167">
            <path
              id="lpe_path-effect88167"
              className="powerclip"
              d="M645.941 171.047h22.634v21.443h-22.634zm9.828 12.65a1.38 1.427 0 00-1.38-1.426 1.38 1.427 0 00-1.38 1.427 1.38 1.427 0 001.38 1.426 1.38 1.427 0 001.38-1.426z"
              opacity={0.5}
              fill="#000"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.517861}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect88163">
            <path
              id="lpe_path-effect88163"
              className="powerclip"
              d="M651.729 148.954h39.88v47.417h-39.88zm19.85 39.327l.42 1.965 1.918-.515-.702-1.94zm2.629-.84l.728 1.901 1.736-.86-1.088-1.836zm2.282-1.158l1.058 1.637 1.571-1.108-1.29-1.57zm2.133-1.67l1.356 1.554 1.356-1.306-1.57-1.389zm1.836-1.918l1.521 1.29 1.042-1.505-1.588-1.059z"
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect88159">
            <path
              id="lpe_path-effect88159"
              className="powerclip"
              d="M652.175 156.677h31.597v31.663h-31.597zm1.941 22.578l4.697 5.358 5.225-5.82-2.58-3.242z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect88155">
            <path
              id="lpe_path-effect88155"
              className="powerclip"
              d="M648.863 153.786h37.684v37.505h-37.684zm4.856 25.072l13.296 10.98 13.824-8.334 3.175-8.07-5.225-11.641z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath88332">
            <path
              d="M524.25 194.144l10.953-5.832-.855-11.382 7.546-9.372 4.093-1.34-.532-7.5-.411-3.104-7.47.73-6.62 4.139-4.822 4.808-2.724 5.568-1.81 5.965.152 6.907.35 7.288z"
              id="path88334"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath90787">
            <path
              d="M-3.701 137.282l.344 7.832 1.808 11.533 8.567 10.003-2.284 6.006-25.047-7.316 1.033-21.431z"
              id="path90789"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath91678">
            <path
              d="M-3.873 135.818l.565 7.381 7.487 20.28 2.84 3.17-1.769 6.438-27.886-8.09 2.754-17.56z"
              id="path91680"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath91797">
            <path
              d="M-3.733 139.125l.29-3.393-17.214 4.046 1.722 25.82 21 7.833 4.953-6.781z"
              id="path91799"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect91949" />
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect91945" />
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect91941">
            <path
              d="M654.116 179.255l7.342-3.704 2.58 3.241-5.226 5.821z"
              id="path91971"
              display="block"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect91937">
            <path
              d="M653.72 178.858l25.069-17.065 5.225 11.641-3.175 8.07-13.824 8.335z"
              id="path91976"
              display="block"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath24362-5">
            <path
              id="lpe_path-effect24366-5"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath11875-4">
            <path
              id="lpe_path-effect11879-9"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect24410-7">
            <path
              id="lpe_path-effect24410-6"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect101497">
            <path
              id="lpe_path-effect101497"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect101493">
            <path
              id="lpe_path-effect101493"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect101489">
            <path
              id="lpe_path-effect101489"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath101660">
            <path
              d="M563.536 135.093l-.711-11.339-12.98-38.986-13.192 10.534-4.446 30.787z"
              id="path101662"
              fill="none"
              stroke="#fff"
              strokeWidth=".379166px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath24362-1">
            <path
              id="lpe_path-effect24366-8"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath11875-8">
            <path
              id="lpe_path-effect11879-6"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect24410-5">
            <path
              id="lpe_path-effect24410-3"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102028">
            <path
              id="lpe_path-effect102028"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102024">
            <path
              id="lpe_path-effect102024"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102020">
            <path
              id="lpe_path-effect102020"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath24362-6">
            <path
              id="lpe_path-effect24366-3"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath11875-2">
            <path
              id="lpe_path-effect11879-8"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect24410-8">
            <path
              id="lpe_path-effect24410-2"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102364">
            <path
              id="lpe_path-effect102364"
              className="powerclip"
              d="M507.87 41.54h53.9v54.37h-53.9zm5.025 18.124l4.233 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102360">
            <path
              id="lpe_path-effect102360"
              className="powerclip"
              d="M528.845 77.653h42.571v42.875h-42.571zm25.582 34.784l.171 1.179c3.304-.702 5.51-2.841 7.51-5.248l-.913-.703c-2.057 2.225-4.208 4.154-6.768 4.772z"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.0999998}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipath_lpe_path-effect102356">
            <path
              id="lpe_path-effect102356"
              className="powerclip"
              d="M508.174 41.846h53.292v53.76h-53.292zm4.72 17.818l4.234 2.778 11.642-11.113-4.498-4.1-11.113 6.217z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath102527">
            <path
              d="M550.578 86.304l3.014 12.09 16.635 21.418 5.77 3.912-3.226 7.406-11.154 3.194-11.17-.706-14.946-9.714.112-25.26z"
              id="path102529"
              fill="none"
              stroke="#fff"
              strokeWidth=".372278px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
        </defs>
        <g id="layer1">
          <path
            id="rect103402"
            fill="none"
            fillRule="evenodd"
            strokeWidth={0.264583}
            d="M-22.722469 -68.855972H530.879521V132.20347800000002H-22.722469z"
          />
          <path
            id="path15213"
            d="M75.844 38.665l15.923-16.639 25.27-.075 3.486 3.237v1.506l-3.407 2.861v19.651l1.426 1.506v1.882l-5.228 5.346v2.861l2.06 2.033v3.313h-12.517l-2.456-1.958-4.278 4.141H80.438l-4.357-4.291z"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient15215)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.204763}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g101209"
            transform="translate(126.35 -60.294) scale(1.01235)"
            fill="none"
            strokeMiterlimit={4}
            strokeOpacity={1}
          >
            <circle
              id="path90586"
              cx={-2.3422031}
              cy={153.79666}
              r={11.182785}
              clipPath="url(#clipPath91678)"
              transform="translate(-24.736 -49.318)"
              stroke="#123d50"
              strokeWidth={0.7}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeDasharray="none"
              strokeDashoffset={0}
              paintOrder="stroke fill markers"
            />
            <circle
              id="circle91801"
              cx={-2.3422031}
              cy={153.79666}
              r={11.182785}
              clipPath="url(#clipPath91678)"
              transform="matrix(.9082 0 0 .9082 -25.077 -35.261)"
              fillOpacity={1}
              stroke="#fc0"
              strokeWidth={0.550544}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeDasharray="none"
              strokeDashoffset={0}
              paintOrder="stroke fill markers"
            />
            <circle
              id="circle91883"
              cx={-2.3422031}
              cy={153.79666}
              r={11.182785}
              clipPath="url(#clipPath91678)"
              transform="matrix(1.34152 0 0 1.34152 -24.088 -101.69)"
              stroke="#123d50"
              strokeWidth={0.372713}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeDasharray="none"
              strokeDashoffset={0}
              paintOrder="stroke fill markers"
            />
            <path
              d="M-28.951 90.984c-11.477 3.801-14.88 13.592-8.505 22.063 4.633 5.66 10.845 5.444 17.255 2.777"
              id="path93375"
              stroke="#0ff"
              strokeWidth={1.2}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeDasharray=".3,.3"
              strokeDashoffset={0}
            />
            <path
              d="M-28.737 89.642l.616 3.613"
              id="path100801"
              stroke="#123d50"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeDasharray="none"
            />
            <path
              d="M-20.926 113.831l2.148 3.473"
              id="path100934"
              stroke="#123d50"
              strokeWidth={0.632113}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeDasharray="none"
            />
          </g>
          <text
            id="text5111-7"
            y={10.104395}
            x={395.41766}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.90236 1.1082)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.224079}
          >
            <tspan
              style={{}}
              y={10.104395}
              x={395.41766}
              id="tspan5109-4"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.224079}
            >
              {'SISTEMA ELÉCTRICO'}
            </tspan>
          </text>
          <text
            id="text5111-7-8"
            y={10.104072}
            x={97.315369}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.90236 1.1082)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.224079}
          >
            <tspan
              style={{}}
              y={10.104072}
              x={97.315369}
              id="tspan5109-4-7"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.224079}
            >
              {'SISTEMA AACC'}
            </tspan>
          </text>
          <g id="g81465" transform="translate(-207.77 -199.479)" fillOpacity={1} strokeOpacity={1}>
            <ellipse
              id="ellipse10245"
              cx={381.29398}
              cy={208.00029}
              rx={6.0641184}
              ry={5.9608936}
              display="inline"
              opacity={0.5}
              fill="none"
              stroke="#0deff7"
              strokeWidth={0.398785}
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
            />
            <path
              d="M381.307 202.999l-.385.322.022 1.336-.86-.435-.26.157v.408l1.08.614v1.04l-.816-.449-.018.906-.873-.47v-1.314l-.314-.215-.3.166v.986l-1.282-.749-.359.206.036.516 1.152.618-.735.457-.01.368.409.229 1.066-.597.73.467-.64.47.668.466-.811.47-1.05-.618-.371.18.004.41.743.429-1.155.667v.488l.404.21 1.2-.707v.887l.315.197.322-.237v-1.224l.928-.448v.87l.758-.512.004 1.063-1.045.591v.368l.31.22.793-.458v1.331l.394.323.296-.332v-1.313l.825.462.282-.188-.013-.516-1.013-.51v-1.032l.793.493v-.829l.883.484v1.192l.323.238.34-.246v-.942l1.143.735.417-.188-.027-.475-1.134-.627.722-.471v-.39l-.292-.184-1.062.556-.829-.435.695-.48-.686-.403.798-.569 1.116.623.318-.197-.027-.403-.757-.435 1.174-.654-.045-.507-.354-.161-1.174.623-.014-.91-.345-.197-.367.228v1.35l-.892.456v-.918l-.73.515v-1.02l1.113-.643v-.386l-.339-.195-.836.483v-1.48zm.376 4.43l.37.591-.36.596-.733.005-.37-.591.361-.596z"
              id="aacc1"
              display="inline"
              opacity={0.9}
              fill="#0df0f7"
              stroke="none"
              strokeWidth=".0177471px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
          </g>
          <ellipse
            ry={5.9608936}
            rx={6.0641184}
            cy={8.5214396}
            cx={336.12643}
            id="ellipse3268-9"
            display="inline"
            opacity={0.5}
            fill="none"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth={0.398785}
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <g
            id="g15976"
            transform="translate(-12.849 -10.71)"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M345.147 19.368h-.86l-.004-.848.784-.076z"
              id="path2108"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path2110"
              d="M345.22 17.93l-.776-.37.362-.767.74.269z"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M345.935 16.57l-.586-.63.618-.58.59.521z"
              id="path2112"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path2114"
              d="M347.073 15.604l-.297-.807.794-.297.343.71z"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M348.478 15.132l.057-.86.847.017.058.785z"
              id="path2116"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path2118"
              d="M349.982 15.27l.363-.78.784.32-.23.754z"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M351.307 15.932l.645-.571.59.607-.51.6z"
              id="path2120"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path2122"
              d="M352.283 17.086l.805-.305.337.777-.69.38z"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M352.808 18.42l.86.068-.028.847-.886.036z"
              id="path2124"
              display="inline"
              opacity={1}
              fill="#bd9700"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path2126"
              d="M346.007 19.467l3.478-5.945-1.167 3.787 2.892-.87-4.347 8.51 1.598-5.84z"
              display="inline"
              opacity={1}
              fill="#0ff"
              fillOpacity={1}
              stroke="#07787c"
              strokeWidth={0.289164}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
            />
            <path
              d="M345.653 21.369c.898 2.35 5.458 2.63 6.76-.244"
              id="path2128"
              display="inline"
              fill="none"
              stroke="#bd9700"
              strokeWidth=".264583px"
            />
          </g>
          <g id="g10270" transform="translate(320.71 -249.304)">
            <ellipse
              id="path8847"
              cx={66.569695}
              cy={289.01181}
              rx={15.092405}
              ry={15.322902}
              display="inline"
              opacity={0.4}
              fill="url(#linearGradient8863)"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.605064}
              strokeLinejoin="round"
              strokeDasharray=".605064,.605064"
              paintOrder="stroke fill markers"
            />
            <g id="g32010" transform="translate(-476.734 181.757) scale(.97349)">
              <path
                id="path24067"
                clipPath="url(#clipPath24362)"
                transform="translate(23.493 41.856)"
                d="M556.216 68.726a21.396 21.63 0 01-21.396 21.63 21.396 21.63 0 01-21.396-21.63 21.396 21.63 0 0121.396-21.63 21.396 21.63 0 0121.396 21.63z"
                opacity={1}
                fill="none"
                fillOpacity={1}
                fillRule="evenodd"
                stroke="#168498"
                strokeWidth={0.5}
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeDashoffset={0}
                strokeOpacity={1}
              />
              <ellipse
                id="path205"
                cx={558.24988}
                cy={110.5346}
                rx={17.370872}
                ry={17.415409}
                fill="none"
                stroke="#fc0"
                strokeWidth={0.510103}
                strokeLinecap="round"
                strokeLinejoin="bevel"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
                paintOrder="stroke fill markers"
              />
              <ellipse
                id="path7875"
                cx={558.24988}
                cy={110.5346}
                rx={10.032257}
                ry={9.6787653}
                fill="none"
                fillOpacity={1}
                fillRule="evenodd"
                stroke="none"
                strokeWidth={0.499999}
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeDashoffset={0}
                strokeOpacity={1}
              />
              <ellipse
                id="path8017"
                cx={558.24988}
                cy={110.5346}
                rx={4.92032}
                ry={4.4363542}
                fill="none"
                fillOpacity={1}
                fillRule="evenodd"
                stroke="none"
                strokeWidth={0.499999}
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeDashoffset={0}
                strokeOpacity={1}
              />
              <path
                id="path11254"
                clipPath="url(#clipPath11875)"
                transform="translate(8.12 11.444)"
                d="M565.416 99.09a15.286 15.438 0 01-15.286 15.438 15.286 15.438 0 01-15.285-15.438 15.286 15.438 0 0115.285-15.437 15.286 15.438 0 0115.286 15.437z"
                fill="none"
                fillOpacity={1}
                fillRule="evenodd"
                stroke="#168498"
                strokeWidth={2}
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeDashoffset={0}
                strokeOpacity={1}
              />
              <path
                d="M562.825 123.754l.174 1.077c3.09-.681 5.133-2.665 6.981-4.89l-.864-.634c-1.904 2.061-3.898 3.851-6.291 4.447z"
                id="path11873"
                fill="#fc0"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.1}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
              />
              <path
                id="path24406"
                clipPath="url(#clipath_lpe_path-effect24410)"
                transform="matrix(.90107 0 0 .9011 76.144 48.578)"
                d="M556.216 68.726a21.396 21.63 0 01-21.396 21.63 21.396 21.63 0 01-21.396-21.63 21.396 21.63 0 0121.396-21.63 21.396 21.63 0 0121.396 21.63z"
                opacity={1}
                fill="none"
                fillOpacity={1}
                fillRule="evenodd"
                stroke="#0deff7"
                strokeWidth={1.10978}
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeDashoffset={0}
                strokeOpacity={1}
              />
              <g
                aria-label="\u25A0\u25A0\u25A0\u25A0"
                transform="matrix(-.99133 -.25228 .25058 -.96829 1081.808 355.859)"
                id="text13519"
                style={{
                  lineHeight: 1.25,
                }}
                fontSize="7.05556px"
                letterSpacing={0}
                wordSpacing={0}
                fill="#fc0"
                strokeWidth={0.261578}
              >
                <path d="M576.835 111.487l-.44 3.222-3.223-.44.44-3.223z" id="path29088" />
                <path d="M575.878 116.557l-1.3 2.981-2.981-1.3 1.3-2.98z" id="path29090" />
                <path d="M573.576 121.177l-2.062 2.515L569 121.63l2.062-2.515z" id="path29092" />
                <path d="M570.103 124.998l-2.67 1.857-1.857-2.67 2.67-1.857z" id="path29094" />
              </g>
            </g>
          </g>
          <text
            id="text5111-7-2"
            y={59.126209}
            x={531.10333}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            transform="scale(.85681 1.16712)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#0ab5bd"
            fillOpacity={0.996078}
            stroke="none"
            strokeWidth={0.224079}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={59.126209}
              x={531.10333}
              id="tspan5109-4-6"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#0ab5bd"
              fillOpacity={0.996078}
              strokeWidth={0.224079}
            >
              {'VOL UPS OUT'}
            </tspan>
          </text>
          <text
            id="text5111-7-9"
            y={66.130264}
            x={389.728}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            transform="scale(.96535 1.0359)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#0ab5bd"
            fillOpacity={0.996078}
            stroke="none"
            strokeWidth={0.224079}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={66.130264}
              x={389.728}
              id="tspan5109-4-61"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#0ab5bd"
              fillOpacity={0.996078}
              strokeWidth={0.224079}
            >
              {'VOL UPS IN'}
            </tspan>
          </text>
          <text
            id="text5111-7-9-7"
            y={66.130562}
            x={315.94171}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            transform="scale(.96535 1.0359)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#0ab5bd"
            fillOpacity={0.996078}
            stroke="none"
            strokeWidth={0.224079}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={66.130562}
              x={315.94171}
              id="tspan5109-4-61-7"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#0ab5bd"
              fillOpacity={0.996078}
              strokeWidth={0.224079}
            >
              {'VOL. CMT'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={368.5766}
            y={43.804829}
            id="vin_ups"
            transform="scale(1.05085 .95161)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.5833px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              id="tspan5101-4"
              x={368.5766}
              y={43.804829}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'center',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.5833px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              {volin_ups}
            </tspan>
          </text>
          <text
            transform="scale(1.05085 .95161)"
            id="text5123-6"
            y={50.437382}
            x={365.97568}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.9361px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={50.437382}
              x={365.97568}
              id="tspan5121-1"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.9361px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              <tspan
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan5125-6"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="condensed"
                fontSize="9.87778px"
                fontFamily="Franklin Gothic Medium Cond"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                strokeWidth={0.244913}
              >
                {'v'}
              </tspan>
            </tspan>
          </text>
          <g id="g32229" transform="matrix(.4685 0 0 .4685 269.362 -104.746)">
            <g
              aria-label="\u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584 \u2584"
              transform="scale(1 -1) rotate(-47.083 -388.436 -19.606)"
              id="text21175"
              style={{
                lineHeight: 1,
              }}
              fontSize="10.5833px"
              letterSpacing="-.79375px"
              wordSpacing={0}
              fill="#0deff7"
              strokeWidth={0.264583}
            >
              <path d="M185.056 145.085l.802-7.69 6.383.666-.801 7.69z" id="path22417" />
              <path d="M182.311 152.695l2.666-7.256 6.024 2.213-2.665 7.256z" id="path22419" />
              <path d="M177.783 159.405l4.357-6.385 5.302 3.618-4.358 6.385z" id="path22421" />
              <path d="M171.765 164.835l5.767-5.148 4.274 4.789-5.768 5.147z" id="path22423" />
              <path d="M164.62 168.667l6.833-3.615 3 5.674-6.833 3.614z" id="path22425" />
              <path d="M156.764 170.668l7.502-1.866 1.549 6.229-7.502 1.866z" id="path22427" />
              <path d="M148.648 170.742l7.73-.03.027 6.417-7.731.031z" id="path22429" />
              <path d="M140.745 168.898l7.517 1.805-1.498 6.241-7.517-1.805z" id="path22431" />
              <path d="M133.527 165.209l6.863 3.558-2.954 5.698-6.863-3.559z" id="path22433" />
              <path d="M127.404 159.894l5.81 5.1-4.235 4.823-5.81-5.1z" id="path22435" />
              <path d="M122.74 153.265l4.41 6.35-5.271 3.661-4.41-6.35z" id="path22437" />
              <path d="M119.839 145.708l2.726 7.234-6.005 2.264-2.727-7.234z" id="path22439" />
              <path d="M118.89 137.673l.865 7.683-6.378.718-.865-7.682z" id="path22441" />
              <path d="M119.92 129.64l-1.027 7.662-6.362-.853 1.028-7.662z" id="path22443" />
              <path d="M122.89 122.115l-2.88 7.175-5.956-2.39 2.88-7.175z" id="path22445" />
              <path d="M127.613 115.54l-4.542 6.255-5.193-3.771 4.542-6.256z" id="path22447" />
              <path d="M133.786 110.285l-5.914 4.979-4.134-4.91 5.914-4.979z" id="path22449" />
            </g>
            <ellipse
              id="ellipse21177"
              cx={94.9599}
              cy={309.1369}
              rx={21.778206}
              ry={21.632132}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#123d50"
              strokeWidth={2}
              strokeMiterlimit={4}
              strokeDasharray="1,1"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <ellipse
              id="ellipse21179"
              cx={95.277969}
              cy={308.38461}
              rx={26.042831}
              ry={25.307619}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0deff7"
              strokeWidth={0.5}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              d="M116.205 293.46l-2.532.053-5.994-5.478 2.945-.052c2.619 1.467 4.264 3.395 5.581 5.478z"
              id="path21181"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M71.647 321.78l2.74-.11c1.047 2.36 3.043 4.087 4.933 5.883l-2.375.987z"
              id="path21183"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M73.803 294.082l1.279.366c4.513-6.045 10.472-10.715 20.5-10.086l.73-1.132c-8.27-.4-15.893 2.59-22.51 10.852z"
              id="path21185"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M116.117 323.607l-1.405-1.264 1.533-2.116 1.334 1.115z"
              id="path21187"
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M103.884 331.915l-.073-1.187c2.097-.416 6.438-1.926 11.037-8.346"
              id="path21189"
              fill="none"
              stroke="#0deff7"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <ellipse
              id="ellipse21191"
              ry={30.840395}
              rx={30.164873}
              cy={138.5744}
              cx={153.12949}
              clipPath="url(#clipPath66030-1)"
              transform="translate(-57.634 170.574)"
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#c0ad2b"
              strokeWidth={1.5}
              strokeMiterlimit={4}
              strokeDasharray="3,1.5"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              id="path21193"
              clipPath="url(#clipath_lpe_path-effect21225-4)"
              transform="translate(-57.634 170.574)"
              d="M183.294 138.574a30.165 30.84 0 01-30.165 30.84 30.165 30.84 0 01-30.164-30.84 30.165 30.84 0 0130.164-30.84 30.165 30.84 0 0130.165 30.84z"
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#123d50"
              strokeWidth={1.5}
              strokeMiterlimit={4}
              strokeDasharray="3,1.5"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              id="path21195-2"
              clipPath="url(#clipath_lpe_path-effect9683)"
              transform="matrix(.98812 0 0 1 -55.246 170.574)"
              d="M188.696 137.833a36.906 36.103 0 01-36.906 36.103 36.906 36.103 0 01-36.905-36.103 36.906 36.103 0 0136.905-36.103 36.906 36.103 0 0136.906 36.103z"
              opacity={0.5}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#123d50"
              strokeWidth={6.03596}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={0}
            />
            <ellipse
              id="ellipse21197"
              cx={94.744263}
              cy={308.98154}
              rx={43.064537}
              ry={42.586826}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#168498"
              strokeWidth={1.5}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              d="M55.601 327.92c3.765 7.928 9.803 14.982 19.783 19.901"
              id="path21199"
              fill="none"
              stroke="#168498"
              strokeWidth={1.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M101.16 351.416v2.743c6.999-1.36 13.966-2.989 20.542-8.013l-.269-3.602c-5.743 4.513-12.38 7.655-20.273 8.872z"
              id="path21201"
              fill="#168498"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M55.936 290.383l-1.667-1.936c2.994-5.84 7.409-10.685 12.422-15.11l1.72 2.258c-6.573 4.826-9.688 9.752-12.475 14.788z"
              id="path21203"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M96.68 266.13v-2.42c9.338.05 18.12 2.432 26.026 8.497l-1.29 2.474c-7.455-5.301-15.85-7.684-24.736-8.55z"
              id="path21205"
              fill="#168498"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M122.177 272.207l4.894.054 2.688 2.527-4.086 3.71-4.786-3.817z"
              id="path21207"
              fill="#168498"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M116.56 334.46l12.48 11.071"
              id="path21209"
              fill="none"
              stroke="#0deff7"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M108.15 348.564l-.494-1.939c5.332-1.865 10.18-4.967 14.601-9.164"
              id="path21211"
              fill="none"
              stroke="#0deff7"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M71.951 287.84c-7.265 8.068-8.803 17.101-8.002 25.821"
              id="path21213"
              fill="none"
              stroke="#0deff7"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M62.369 313.77l1.58-.11c.31 4.666 2.31 8.654 4.297 12.646h-2.072c-2.79-4.823-3.545-8.886-3.805-12.536z"
              id="path21215"
              fill="none"
              stroke="#0deff7"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              d="M116.457 334.698l1.342-1.283"
              id="path21217"
              fill="#0deff7"
              fillOpacity={1}
              stroke="#0deff7"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M52.886 320.595l-4.281.25 2.661 7.605h4.335c-1.2-2.437-2.297-4.92-2.715-7.855z"
              id="path21219"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </g>
          <text
            transform="scale(1.05085 .95161)"
            id="text5123-6-2"
            y={51.037636}
            x={293.30045}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#f9f9f9"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={51.037636}
              x={293.30045}
              id="tspan5121-1-3"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#f9f9f9"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              {'kv'}
            </tspan>
          </text>
          <g id="g32089" transform="translate(-54.84 -131.818) scale(.95242)">
            <path
              id="ellipse995-7"
              clipPath="url(#clipath_lpe_path-effect22263)"
              transform="translate(-118.833 9.394)"
              d="M680.797 172.54a13.092 13.004 0 01-13.092 13.004 13.092 13.004 0 01-13.092-13.004 13.092 13.004 0 0113.092-13.004 13.092 13.004 0 0113.092 13.004z"
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#123d50"
              strokeWidth={1.5}
              strokeMiterlimit={4}
              strokeDasharray=".75,.75"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              id="path1379-7"
              clipPath="url(#clipath_lpe_path-effect22267)"
              transform="translate(-119.098 9.262)"
              d="M678.513 172.508a10.54 10.573 0 01-10.54 10.573 10.54 10.573 0 01-10.539-10.573 10.54 10.573 0 0110.54-10.572 10.54 10.573 0 0110.54 10.572z"
              fill="none"
              fillOpacity={1}
              stroke="#fc0"
              strokeWidth={0.518266}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={0.8}
              paintOrder="stroke fill markers"
            />
            <path
              d="M533.96 189.013l-2.15 1.058c-1.051-3.06-2.261-6.029-1.686-10.054.456-3.748 1.62-7.405 4.928-10.782.958-.942 1.78-1.92 3.208-2.745l1.223 1.588a16.619 16.619 0 00-3.042 2.844l-1.026-.992c-2.42 2.951-4.042 6.264-4.53 10.087h1.52c-.198 2.975-.097 5.96 1.555 8.996z"
              id="path1802-5"
              fill="#123d50"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.1}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M532.79 179.63h-1.357c.607-2.808 1.113-5.595 4.022-8.887l.89.608c-1.951 2.513-3.346 5.205-3.556 8.278z"
              id="path2524-7"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.1}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M539.08 165.902l.608 1.099c2.616-1.767 5.675-2.89 9.659-2.666v-1.17c-3.745-.248-7.146.741-10.267 2.737z"
              id="path2832-8"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.1}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M658.118 185.87l-1.39 1.72c3.809 3.051 8.563 4.366 14.487 3.538 5.281-1.068 9.622-3.828 12.7-8.863 2.242-3.457 2.005-5.797 2.646-8.533.393-5.52-1.565-11.137-7.21-15.842-2.91-2.114-6.037-3.75-9.823-3.936l-.198 2.15c2.569.21 5.36 1.086 8.632 3.407l.727-.662c6.306 5.288 6.501 10.353 6.681 14.883l-.992.033c-.418 2.43-.826 4.866-2.018 6.78l-1.025-.496c-2.43 3.852-5.734 6.736-10.715 7.772l-.034 1.19c-4.566.43-8.783-.398-12.468-3.141z"
              id="path3447-7"
              clipPath="url(#clipath_lpe_path-effect22271)"
              transform="translate(-119.098 9.262)"
              opacity={1}
              fill="#0deff7"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.1}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <ellipse
              id="path5242-0"
              cx={548.84296}
              cy={181.96854}
              rx={19.579998}
              ry={19.646145}
              fill="none"
              fillOpacity={1}
              stroke="#fc0"
              strokeWidth={0.49834}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <path
              d="M650.941 182.199l11.113-6.152 1.521 1.786-8.467 9.657c-1.636-1.446-3.143-3.058-4.167-5.291z"
              id="path5411-8"
              clipPath="url(#clipath_lpe_path-effect22275)"
              transform="translate(-119.098 9.262)"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <ellipse
              id="path16925-3"
              cx={548.84772}
              cy={181.89784}
              rx={20.435747}
              ry={20.537249}
              fill="none"
              fillOpacity={1}
              stroke="#123d50"
              strokeWidth={0.5}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray=".05,.05"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <path
              d="M530.289 190.667l-5.093 2.612c-7.216-16.875 1.683-34.652 19.38-37.14l.827 5.523"
              id="path17964-5"
              fill="none"
              stroke="#123d50"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <ellipse
              id="path19508-0"
              clipPath="url(#clipPath19972-0)"
              ry={12.37312}
              rx={12.227744}
              cy={172.73984}
              cx={668.18518}
              transform="translate(-119.098 9.262)"
              fill="none"
              fillOpacity={1}
              stroke="#0deff7"
              strokeWidth={0.558449}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray=".0558449,.0558449"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <ellipse
              id="path20325-0"
              clipPath="url(#clipPath20540-8)"
              ry={13.511155}
              rx={13.617785}
              cy={172.70677}
              cx={667.93988}
              transform="translate(-119.098 9.262)"
              fill="none"
              fillOpacity={1}
              stroke="#123d50"
              strokeWidth={0.729795}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray=".0729795,.0729795"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <ellipse
              id="path20678-2"
              cx={667.09784}
              cy={173.18381}
              rx={23.605532}
              ry={23.259073}
              clipPath="url(#clipPath20929-5)"
              transform="translate(-119.098 9.262)"
              fill="none"
              fillOpacity={1}
              stroke="#123d50"
              strokeWidth={0.5}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray=".05,.05"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <path
              d="M567.377 190.714l2.713.655"
              id="path21074-9"
              fill="none"
              stroke="#123d50"
              strokeWidth={0.5}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              d="M527.052 190.964c-4.073-15.037-.74-26.755 16.838-32.086"
              id="path21709-2"
              fill="none"
              stroke="#0deff7"
              strokeWidth={4}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="1,1"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
          </g>
          <text
            transform="scale(.95881 1.04296)"
            id="vout_ups"
            y={40.597492}
            x={487.73935}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.5833px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'center',
              }}
              y={40.597492}
              x={487.73935}
              id="tspan5117-5"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.5833px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              {volout_ups}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={441.32184}
            y={52.437054}
            id="text5131-7"
            transform="scale(1.05085 .95161)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.9361px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              id="tspan5129-7"
              x={441.32184}
              y={52.437054}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.9361px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              <tspan
                id="tspan5127-4"
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="condensed"
                fontSize="9.87778px"
                fontFamily="Franklin Gothic Medium Cond"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                strokeWidth={0.244913}
              >
                {'v'}
              </tspan>
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={298.46957}
            y={43.804829}
            id="v_cmt"
            transform="scale(1.05085 .95161)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.5833px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.244913}
          >
            <tspan
              id="tspan5101-4-9"
              x={298.46957}
              y={43.804829}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'center',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.5833px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.244913}
            >
              {vol_cmtp}
            </tspan>
          </text>
          <g
            id="g1170"
            transform="matrix(.83834 0 0 .79678 28.866 -14.42)"
            display="inline"
            opacity={0.75}
            stroke="#0deff7"
            strokeWidth={0.945421}
            strokeOpacity={1}
            fillOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path
              d="M55.846 66.713L74.839 45.83l30.144-.095 4.158 4.063v1.89l-4.064 3.591v24.663l1.701 1.89v2.362l-6.236 6.71v3.59l2.457 2.551v4.158h-14.93l-2.93-2.457-5.103 5.197h-18.71l-5.197-5.386z"
              id="path829"
              fill="none"
              strokeWidth={0.250536}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path
              d="M62.555 59.437l-.094-4.536 8.22-9.172 4.159-.016z"
              id="path831"
              fill="#0cedf7"
              strokeWidth=".250143px"
            />
            <path
              d="M106.637 53.909l.267 27.914-1.827-1.881V55.279z"
              id="path833"
              fill="#0cedf7"
              strokeWidth=".250143px"
            />
          </g>
          <path
            id="path975"
            d="M87.873 33.786l-1.912-1.671 4.643-3.997h25.765l3.369-3.706"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.215653}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".646956,.646956"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <g
            transform="matrix(.77764 0 0 .79864 69.46 -9.939)"
            id="g6932"
            display="inline"
            strokeWidth={0.901086}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M9.324 85.44l12.428 11.975"
              id="path852"
              display="inline"
              strokeWidth={0.541371}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M14.046 86.783l6.118 5.757" id="path852-0" display="inline" strokeWidth=".253518px" />
            <path d="M11.397 91.24l4.387 4.135" id="path852-0-7" display="inline" strokeWidth=".253518px" />
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={89.601814}
            y={47.438854}
            id="tsum_chill"
            transform="scale(1.02101 .97942)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.0721px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.251246}
          >
            <tspan
              id="tspan5844"
              x={89.601814}
              y={47.438854}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.0721px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.251246}
            >
              {tsum_chill}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={102.3194}
            y={24.345043}
            id="text3573"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.176503}
          >
            <tspan
              id="tspan3571"
              x={102.3194}
              y={24.345043}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium Cond"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.176503}
            >
              <tspan
                style={{}}
                id="tspan22115"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontFamily="Franklin Gothic Medium"
              >
                {'T.SUMINISTRO'}
              </tspan>
            </tspan>
          </text>
          <path
            id="path6966"
            d="M185.512 38.726l-16.1-16.64-25.55-.075-3.524 3.238v1.506l3.444 2.86v19.652l-1.442 1.506v1.882L147.626 58v2.861l-2.082 2.033v3.313h12.655l2.483-1.957 4.325 4.14h15.86l4.405-4.291z"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient4462)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.205896}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g1170-0"
            transform="matrix(-.85134 0 0 .7917 233.017 -14.013)"
            display="inline"
            opacity={0.75}
            strokeWidth={0.945386}
            fillOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M55.846 66.713L74.839 45.83l30.144-.095 4.158 4.063v1.89l-4.064 3.591v24.663l1.701 1.89v2.362l-6.236 6.71v3.59l2.457 2.551v4.158h-14.93l-2.93-2.457-5.103 5.197h-18.71l-5.197-5.386z"
              id="path829-0"
              fill="none"
              stroke="#04e6f4"
              strokeWidth={0.250527}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path
              d="M62.555 59.437l-.094-4.536 8.22-9.172 4.159-.016z"
              id="path831-8"
              fill="#0cedf7"
              stroke="none"
              strokeWidth=".250133px"
            />
            <path
              d="M106.637 53.909l.267 27.914-1.827-1.881V55.279z"
              id="path833-1"
              fill="#0cedf7"
              stroke="none"
              strokeWidth=".250133px"
            />
          </g>
          <g
            transform="matrix(.77764 0 0 .79864 -18.033 -13.39)"
            id="g6103"
            display="inline"
            strokeWidth={0.901086}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M260.444 88.374l-12.62 12.458"
              id="path852-9"
              display="inline"
              strokeWidth={0.556448}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M255.17 89.828l-6.212 5.99" id="path852-0-1" display="inline" strokeWidth=".260578px" />
            <path d="M257.861 94.466l-4.455 4.302" id="path852-0-7-2" display="inline" strokeWidth=".260578px" />
          </g>
          <path
            id="rect2077"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 51.134789H150.7648519V53.6806865H145.01624z"
          />
          <path
            id="rect2077-1"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 47.68718H150.7648519V50.2330772H145.01624z"
          />
          <path
            id="rect2077-7"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 44.239574H150.7648519V46.785471199999996H145.01624z"
          />
          <path
            id="rect2077-1-2"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 40.791874H150.7648519V43.3377712H145.01624z"
          />
          <path
            id="rect2077-9"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 37.344143H150.7648519V39.8900402H145.01624z"
          />
          <path
            id="rect2077-1-5"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M145.01624 33.896568H150.7648519V36.4424652H145.01624z"
          />
          <text
            transform="scale(.98676 1.01342)"
            id="tret_uma"
            y={46.045177}
            x={155.95673}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.0721px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.187886}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={46.045177}
              x={155.95673}
              id="tspan9221-2"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.0721px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.187886}
            >
              {tret_uma}
            </tspan>
          </text>
          <path
            d="M174.306 33.455l1.83-1.654-4.445-3.954H147.03l-3.225-3.668"
            id="path3447"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.209881}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".629642,.629642"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={161.92322}
            y={51.368404}
            id="text6381"
            transform="scale(.98676 1.01342)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9999px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#f9f9f9"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.187886}
          >
            <tspan
              id="tspan6379"
              x={161.92322}
              y={51.368404}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.9999px"
              fontFamily="Franklin Gothic Medium Cond"
              fill="#f9f9f9"
              fillOpacity={1}
              strokeWidth={0.187886}
            >
              {'\xB0C'}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={163.74779}
            y={24.345097}
            id="text3573-5"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.180275}
          >
            <tspan
              x={163.74779}
              y={24.345097}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan9353"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.180275}
            >
              {'T.RETORNO'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={97.067436}
            y={51.368404}
            id="text6381-0"
            transform="scale(.98676 1.01342)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9999px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.187886}
          >
            <tspan
              id="tspan6379-3"
              x={97.067436}
              y={51.368404}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.9999px"
              fontFamily="Franklin Gothic Medium Cond"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.187886}
            >
              {'\xB0C'}
            </tspan>
          </text>
          <path
            id="path84314"
            d="M12.866 38.726l16.099-16.64 25.55-.075 3.525 3.238v1.506l-3.444 2.86v19.652l1.442 1.506v1.882L50.75 58v2.861l2.083 2.033v3.313H40.178l-2.483-1.957-4.325 4.14H17.511L13.106 64.1z"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient84362)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.205896}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g84322"
            transform="matrix(.85134 0 0 .7917 -34.639 -14.013)"
            display="inline"
            opacity={0.75}
            strokeWidth={0.945386}
            fillOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M55.846 66.713L74.839 45.83l30.144-.095 4.158 4.063v1.89l-4.064 3.591v24.663l1.701 1.89v2.362l-6.236 6.71v3.59l2.457 2.551v4.158h-14.93l-2.93-2.457-5.103 5.197h-18.71l-5.197-5.386z"
              id="path84316"
              fill="none"
              stroke="#04e6f4"
              strokeWidth={0.250527}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path
              d="M62.555 59.437l-.094-4.536 8.22-9.172 4.159-.016z"
              id="path84318"
              fill="#0cedf7"
              stroke="none"
              strokeWidth=".250133px"
            />
            <path
              d="M106.637 53.909l.267 27.914-1.827-1.881V55.279z"
              id="path84320"
              fill="#0cedf7"
              stroke="none"
              strokeWidth=".250133px"
            />
          </g>
          <g
            transform="matrix(-.77764 0 0 .79864 216.41 -13.39)"
            id="g84330"
            display="inline"
            strokeWidth={0.901086}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M260.444 88.374l-12.62 12.458"
              id="path84324"
              display="inline"
              strokeWidth={0.556448}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M255.17 89.828l-6.212 5.99" id="path84326" display="inline" strokeWidth=".260578px" />
            <path d="M257.861 94.466l-4.455 4.302" id="path84328" display="inline" strokeWidth=".260578px" />
          </g>
          <path
            id="rect84332"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 51.134789H-47.6121971V53.6806865H-53.360809z"
          />
          <path
            id="rect84334"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 47.68718H-47.6121971V50.2330772H-53.360809z"
          />
          <path
            id="rect84336"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 44.239574H-47.6121971V46.785471199999996H-53.360809z"
          />
          <path
            id="rect84338"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 40.791874H-47.6121971V43.3377712H-53.360809z"
          />
          <path
            id="rect84340"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 37.344143H-47.6121971V39.8900402H-53.360809z"
          />
          <path
            id="rect84342"
            transform="scale(-1 1)"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#17b9ed"
            strokeWidth={0.438521}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M-53.360809 33.896568H-47.6121971V36.4424652H-53.360809z"
          />
          <text
            transform="scale(.98676 1.01342)"
            id="tret_chill"
            y={46.045177}
            x={28.682709}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.0721px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.187886}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={46.045177}
              x={28.682709}
              id="tspan84344"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.0721px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.187886}
            >
              {tret_chill}
            </tspan>
          </text>
          <path
            d="M24.12 33.455l-1.878-1.654 4.563-3.954h25.319l3.31-3.668"
            id="path84348"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.212656}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".637966,.637966"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={33.563343}
            y={51.368404}
            id="text84352"
            transform="scale(.98676 1.01342)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9999px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#f9f9f9"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.187886}
          >
            <tspan
              id="tspan84350"
              x={33.563343}
              y={51.368404}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.9999px"
              fontFamily="Franklin Gothic Medium Cond"
              fill="#f9f9f9"
              fillOpacity={1}
              strokeWidth={0.187886}
            >
              {'\xB0C'}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={37.529305}
            y={24.345097}
            id="text84356"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.180275}
          >
            <tspan
              id="tspan84354"
              x={37.529305}
              y={24.345097}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.180275}
            >
              {'T.RETORNO '}
            </tspan>
            <tspan
              x={37.529305}
              y={28.809271}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan7816"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.180275}
            >
              {'CHILLERS'}
            </tspan>
          </text>
          <path
            transform="matrix(-.49175 0 0 .5032 224.733 14.335)"
            clipPath="url(#clipPath1984)"
            id="path84358"
            d="M386.1 38.143a22.25 22.248 0 01.495.026l-.025 4.723a17.518 17.517 0 00-.47-.017 17.518 17.517 0 00-17.519 17.516 17.518 17.517 0 006.337 13.466l-2.697 3.896a22.25 22.248 0 01-8.371-17.362 22.25 22.248 0 0122.25-22.248zm1.66.087a22.25 22.248 0 0112.97 5.444l-3.744 3.013a17.518 17.517 0 00-9.25-3.718zm13.831 6.248a22.25 22.248 0 016.125 10.75l-4.823.209a17.518 17.517 0 00-5.013-7.972zm6.384 11.907a22.25 22.248 0 01.374 4.006 22.25 22.248 0 01-1.586 8.153l-4.246-2.106a17.518 17.517 0 001.101-6.047 17.518 17.517 0 00-.431-3.8zm-5.898 11.137l4.225 2.094a22.25 22.248 0 01-7.292 8.852l-2.52-4a17.518 17.517 0 005.587-6.946zm-26.232 7.045a17.518 17.517 0 009.558 3.314l-.212 4.711a22.25 22.248 0 01-12.04-4.133zm19.675.55l2.536 4.025a22.25 22.248 0 01-11.7 3.487l.214-4.745a17.518 17.517 0 008.95-2.767z"
            display="inline"
            opacity={0.75}
            fill="#fc0"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={1.31553}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(-.6253 0 0 .63985 276.52 6.51)"
            clipPath="url(#clipPath2072)"
            id="path84360"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="path84364"
            d="M250.712 38.711l-15.966-16.685-25.34-.075-3.495 3.246v1.51l3.415 2.87V49.28l-1.43 1.51v1.887l5.243 5.36v2.87l-2.065 2.038v3.322h12.55l2.463-1.963 4.29 4.152h15.728l4.37-4.303z"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient84432)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.205325}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g84372"
            transform="matrix(-.84064 0 0 .79897 297.82 -14.52)"
            display="inline"
            opacity={0.75}
            stroke="#0deff7"
            strokeWidth={0.945421}
            strokeOpacity={1}
            fillOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path
              d="M55.846 66.713L74.839 45.83l30.144-.095 4.158 4.063v1.89l-4.064 3.591v24.663l1.701 1.89v2.362l-6.236 6.71v3.59l2.457 2.551v4.158h-14.93l-2.93-2.457-5.103 5.197h-18.71l-5.197-5.386z"
              id="path84366"
              fill="none"
              strokeWidth={0.250536}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path
              d="M62.555 59.437l-.094-4.536 8.22-9.172 4.159-.016z"
              id="path84368"
              fill="#0cedf7"
              strokeWidth=".250143px"
            />
            <path
              d="M106.637 53.909l.267 27.914-1.827-1.881V55.279z"
              id="path84370"
              fill="#0cedf7"
              strokeWidth=".250143px"
            />
          </g>
          <path
            id="path84374"
            d="M238.203 33.722l1.835-1.659-4.457-3.965h-24.73l-3.233-3.677"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.210456}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".631368,.631368"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <g
            transform="matrix(-.77977 0 0 .80083 257.114 -10.026)"
            id="g84382"
            display="inline"
            strokeWidth={0.901086}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M9.324 85.44l12.428 11.975"
              id="path84376"
              display="inline"
              strokeWidth={0.541371}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M14.046 86.783l6.118 5.757" id="path84378" display="inline" strokeWidth=".253518px" />
            <path d="M11.397 91.24l4.387 4.135" id="path84380" display="inline" strokeWidth=".253518px" />
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={212.03419}
            y={47.507603}
            id="tsum_uma"
            transform="scale(1.02101 .97942)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="10.0998px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.251934}
          >
            <tspan
              id="tspan84384"
              x={212.03419}
              y={47.507603}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="10.0998px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.251934}
            >
              {tsum_uma}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={231.95012}
            y={24.473095}
            id="text84390"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.58113px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.176987}
          >
            <tspan
              x={231.95012}
              y={24.473095}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan10035"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.58113px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.176987}
            >
              {'T.SUMINISTRO'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={225.93864}
            y={51.449986}
            id="text84394"
            transform="scale(.98676 1.01342)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.0136px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.188401}
          >
            <tspan
              id="tspan84392"
              x={225.93864}
              y={51.449986}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.0136px"
              fontFamily="Franklin Gothic Medium Cond"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.188401}
            >
              {'\xB0C'}
            </tspan>
          </text>
          <path
            transform="matrix(-.84037 0 0 .84823 339.289 -55.645)"
            clipPath="url(#clipPath1163)"
            id="path84396"
            d="M132.164 101.266a17.845 17.845 0 00-.397.02l.02 3.789a14.05 14.05 0 01.377-.014 14.05 14.05 0 0114.051 14.05 14.05 14.05 0 01-5.082 10.801l2.163 3.126a17.845 17.845 0 006.714-13.927 17.845 17.845 0 00-17.846-17.845zm-1.331.07a17.845 17.845 0 00-10.402 4.366l3.003 2.417a14.05 14.05 0 017.418-2.982zm-11.093 5.011a17.845 17.845 0 00-4.913 8.624l3.869.166a14.05 14.05 0 014.02-6.394zm-5.12 9.55a17.845 17.845 0 00-.3 3.214 17.845 17.845 0 001.272 6.54l3.405-1.69a14.05 14.05 0 01-.883-4.85 14.05 14.05 0 01.346-3.048zm4.73 8.934l-3.389 1.68a17.845 17.845 0 005.849 7.1l2.021-3.208a14.05 14.05 0 01-4.48-5.572zm21.04 5.65a14.05 14.05 0 01-7.667 2.66l.17 3.778a17.845 17.845 0 009.658-3.315zm-15.78.442l-2.035 3.228a17.845 17.845 0 009.384 2.798l-.172-3.806a14.05 14.05 0 01-7.178-2.22z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.832064}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <g
            transform="matrix(-1.2495 0 0 1.2495 435.446 12.415)"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            id="g84430"
            aria-label="\u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0 \u25A0"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="2.11667px"
            fontFamily="Franklin Gothic Medium"
            display="inline"
            fill="#c09d05"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <path
              id="path84398"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M170.872 32.984l-.81.544-.544-.81.81-.544z"
            />
            <path
              id="path84400"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M169.1 34.012l-.918.328-.328-.919.919-.328z"
            />
            <path
              id="path84402"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M167.132 34.574l-.972.092-.092-.97.97-.094z"
            />
            <path
              id="path84404"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M165.087 34.637l-.965-.146.146-.965.965.146z"
            />
            <path
              id="path84406"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M163.088 34.2l-.9-.376.376-.9.9.377z"
            />
            <path
              id="path84408"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M161.254 33.29l-.78-.587.586-.78.78.586z"
            />
            <path
              id="path84410"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M159.701 31.956l-.613-.758.758-.614.614.759z"
            />
            <path
              id="path84412"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M158.518 30.286l-.41-.885.885-.41.41.885z"
            />
            <path
              id="path84414"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M157.78 28.376l-.181-.959.959-.18.18.958z"
            />
            <path
              id="path84416"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M157.532 26.345l.058-.974.974.058-.059.974z"
            />
            <path
              id="path84418"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M157.785 24.315l.294-.93.93.293-.293.93z"
            />
            <path
              id="path84420"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M158.526 22.406l.513-.83.83.512-.513.83z"
            />
            <path
              id="path84422"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M159.713 20.738l.7-.68.68.7-.7.68z"
            />
            <path
              id="path84424"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M161.27 19.41l.843-.49.49.845-.845.49z"
            />
            <path
              id="path84426"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M163.104 18.5l.938-.267.267.938-.938.268z"
            />
            <path
              id="path84428"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              d="M165.105 18.069l.975-.03.03.975-.975.03z"
            />
          </g>
          <g
            id="g32010-8"
            transform="scale(-.71949 .71949) rotate(14.182 352.341 -3074.14)"
            clipPath="url(#clipPath102527)"
          >
            <path
              id="path24067-6"
              clipPath="url(#clipath_lpe_path-effect102356)"
              transform="translate(23.493 41.856)"
              d="M556.216 68.726a21.396 21.63 0 01-21.396 21.63 21.396 21.63 0 01-21.396-21.63 21.396 21.63 0 0121.396-21.63 21.396 21.63 0 0121.396 21.63z"
              opacity={1}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#168498"
              strokeWidth={0.5}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <ellipse
              id="path205-9"
              cx={558.24988}
              cy={110.5346}
              rx={17.370872}
              ry={17.415409}
              fill="none"
              stroke="#fc0"
              strokeWidth={0.510103}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
            <ellipse
              id="path7875-0"
              cx={558.24988}
              cy={110.5346}
              rx={10.032257}
              ry={9.6787653}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="none"
              strokeWidth={0.499999}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <ellipse
              id="path8017-0"
              cx={558.24988}
              cy={110.5346}
              rx={4.92032}
              ry={4.4363542}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="none"
              strokeWidth={0.499999}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              id="path11254-5"
              clipPath="url(#clipath_lpe_path-effect102360)"
              transform="translate(8.12 11.444)"
              d="M565.416 99.09a15.286 15.438 0 01-15.286 15.438 15.286 15.438 0 01-15.285-15.438 15.286 15.438 0 0115.285-15.437 15.286 15.438 0 0115.286 15.437z"
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#168498"
              strokeWidth={2}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <path
              d="M562.825 123.754l.174 1.077c3.09-.681 5.133-2.665 6.981-4.89l-.864-.634c-1.904 2.061-3.898 3.851-6.291 4.447z"
              id="path11873-6"
              fill="#fc0"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.1}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
            <path
              id="path24406-3"
              clipPath="url(#clipath_lpe_path-effect102364)"
              transform="matrix(.90107 0 0 .9011 76.144 48.578)"
              d="M556.216 68.726a21.396 21.63 0 01-21.396 21.63 21.396 21.63 0 01-21.396-21.63 21.396 21.63 0 0121.396-21.63 21.396 21.63 0 0121.396 21.63z"
              opacity={1}
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0deff7"
              strokeWidth={1.10978}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
            />
            <g
              aria-label="\u25A0\u25A0\u25A0\u25A0"
              transform="matrix(-.99133 -.25228 .25058 -.96829 1081.808 355.859)"
              id="text13519-0"
              style={{
                lineHeight: 1.25,
              }}
              fontSize="7.05556px"
              letterSpacing={0}
              wordSpacing={0}
              fill="#fc0"
              strokeWidth={0.261578}
            >
              <path d="M576.835 111.487l-.44 3.222-3.223-.44.44-3.223z" id="path29088-5" />
              <path d="M575.878 116.557l-1.3 2.981-2.981-1.3 1.3-2.98z" id="path29090-3" />
              <path d="M573.576 121.177l-2.062 2.515L569 121.63l2.062-2.515z" id="path29092-6" />
              <path d="M570.103 124.998l-2.67 1.857-1.857-2.67 2.67-1.857z" id="path29094-2" />
            </g>
          </g>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={106.92568}
            y={29.208267}
            id="text11582"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.176503}
          >
            <tspan
              id="tspan11578"
              x={106.92568}
              y={29.208267}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.176503}
            >
              {'CHILLERS'}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={168.535}
            y={29.208267}
            id="text13262"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.176503}
          >
            <tspan
              id="tspan13258"
              x={168.535}
              y={29.208267}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.176503}
            >
              {'UMAS'}
            </tspan>
          </text>
          <text
            transform="scale(.91903 1.0881)"
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={238.20564}
            y={29.208267}
            id="text14138"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.57134px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.176503}
          >
            <tspan
              id="tspan14134"
              x={238.20564}
              y={29.208267}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="3.57134px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.176503}
            >
              {'UMAS'}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
