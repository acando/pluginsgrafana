import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  let valor = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
  let on = '#00aa44';
  let off = '#d40000';
  let color = off;
  let text_on = 'ON';
  let text_off = 'OFF';
  let texto = text_off;
  if (valor === null || valor === 0) {
    color = off;
    texto = text_off;
  } else {
    color = on;
    texto = text_on;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox="0 0 132.29166 132.29167"
        id="svg8"
        //{...props}
      >
        <g id="layer1">
          <path
            id="rect833"
            opacity={0.908503}
            fill={color}
            strokeWidth={0.343876}
            d="M0.43901572 0.43901572H131.85265571999997V131.85265571999997H0.43901572z"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={64.719566}
            y={75.391777}
            id="text837"
            fontStyle="normal"
            fontWeight={400}
            fontSize="25.4px"
            fontFamily="sans-serif"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan835"
              x={64.719566}
              y={75.391777}
              style={{
                textAlign: 'center',
              }}
              fontSize="25.4px"
              textAnchor="middle"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {texto}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
