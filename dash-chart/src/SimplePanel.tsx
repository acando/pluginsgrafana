import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import ChartDataLabels from "chartjs-plugin-datalabels";

//import { url } from 'inspector';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    ChartDataLabels
  );

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options,data,width, height }) => {
  ///const theme = useTheme();
  const styles = getStyles();
  console.log("cmdb",data)
  let arch=['REG GESTIoN DE CAMBIOS','REG RESUMEN MTTOS','REG CAP. SALA PRINCIPAL','PRUEBAS DE CONTINGENCIA','REUNIoN','MANTENIMIENTO PREVENTIVO PROGRAMADO','MANTENIMIENTO PREVENTIVO NVL 2','MANTENIMIENTO CORRECTIVO','RONDA PDU','RONDA INFRAESTRUCTURA','REG ROTACIoN EQUIPOS','REG CAP. ELeCTRICA','REG CAP. AACC','REVISIoN','IMPRESIoN RGC EQ CRiTICOS','SIMULACRO DE CONTINUIDAD','REG DISPONIBILIDAD','APAGADO DE EQUIPO','MANTENIMIENTO PREDICTIVO','ROTACIoN','ROTACIoN DE EQUIPOS','ENTREGA INICIAL','ARRANQUE','PROYECTO','INSTALACIoN','CALIBRACIoN EQ DE MEDICIoN','ENERGIZACIoN','FACTIBILIDAD','INSPECCIoN ','ABASTECIMIENTO','INSTALACIoN ','APAGADO','EJERCITACIoN DE EQUIPOS','DESINSTALACIoN','DESENERGIZACIoN','EVENTO','MANTENIMIENTO PREVENTIVO NO PROGRAMADO','ENCENDIDO']
  let arch2= ['REG. GEST. CAMBIOS','REG. RES. MTTOS','REG. CAP. SALA PRINCIPAL','PRUE. CONTINGENCIA','REUNION','MNT. PREV. PROGRAMADO','MNT. PREV. NVL 2','MNT. CORRECTIVO','RONDA PDU','RONDA INFRA.','REG. ROT. EQUIPOS','REG. CAP. ELÉCTRICA','REG. CAP. AACC','REVISION','IMP. RGC . EQ. CRÍTICOS','SIMU. CONTINUIDAD','REG. DISPONIBILIDAD','APA. DE EQUIPO','MNT. PREDICTIVO','ROTACION','ROTACION','ENTREGA INICIAL','ARRANQUE ','PROYECTO','INSTALACION','CAL. EQ DE MEDICIÓN','ENERGIZACION','FACTIBILIDAD','INSPECCION','ABASTECIMIENTO','INSTALACION','APAGADO','EJER. EQUIPO','DESINSTALACION','DESENERGIZACION','EVENTO','MNT. PREV. NO PROG','ENCENDIDO']
  let labels=[]//data.series[0].fields[0].values.buffer
  let datos=[]//data.series[0].fields[1].values.buffer
  let j=0
  let arr
  try {
    arr = data.series[0].fields.find(({ name }) => name === "TIPO_MANTENIMIENTO")?.values.buffer
  } catch (error) {
    console.log("no hay datos")
    arr={"SIN DATA":0}
  }
  
  let resultado = {}
  for (let i = 0; i < arr.length; i++) {
    const el = arr[i];
    resultado[el] = resultado[el] + 1 || 1;
  }
  for (const property in resultado) {
    
    for(let k=0;k<arch.length;k++){
      if(`${property}`===arch[k]){
        labels[j]=arch2[k]
      }
    }
    datos[j]=`${resultado[property]}`
    j++
  }
  console.log("resultado",labels,datos)
  const options2 = {
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: false
      },
    },
    scales: {
      y: {
        ticks: { color: 'rgba(255, 255, 255, 0.7)', beginAtZero: true},
        title: { display: true, text: "Cantidad",color: 'rgba(255, 255, 255, 0.7)'},
      },
      x: {
        ticks: { color: 'rgba(255, 255, 255, 0.7)', beginAtZero: true }
      }
    }
  };

  const data2 = {
    labels,
    datasets: [
      {
        label: "Cantidad",
        data: datos,
        backgroundColor: '#fff',
      }
    ],
  };

  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
    <Bar options={options2} data={data2} />
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
      top: 5px;
      left: 0;
      padding: 5px;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 5px;
    `,
  };
});
