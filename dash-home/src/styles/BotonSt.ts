import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    botonOn: css`
      fill: green;
    `,
    botonOff: css`
      fill: red;
    `,
  };
});

const button = getStyles();

export default button;
