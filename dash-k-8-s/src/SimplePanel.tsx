import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  var per: any = data.series.find(({ refId }) => refId === 'PERCENT')?.name;
  let val = parseFloat(per);

  var r = 90;
  var c = Math.PI * (r * 2);

  if (val < 0) {
    val = 0;
  }
  if (val > 100) {
    val = 100;
  }

  var pct = ((100 - val) / 100) * c;
  console.log(pct);

  //const theme = useTheme();
  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <div id="cont" data-pct={per}>
        <svg id="svg" width="200" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset={0}></circle>
          <circle
            id="bar"
            r="90"
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray="565.48"
            stroke-dashoffset={pct}
          ></circle>
          <circle r="75" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset={0}></circle>
          <circle
            id="bar"
            r="75"
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray="565.48"
            stroke-dashoffset={pct + 10}
          ></circle>
          <circle r="60" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset={0}></circle>
          <circle
            id="bar"
            r="60"
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray="565.48"
            stroke-dashoffset={pct + 25}
          ></circle>
          <circle r="45" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset={0}></circle>
          <circle
            id="bar"
            r="45"
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray="565.48"
            stroke-dashoffset={pct + 15}
          ></circle>
        </svg>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
