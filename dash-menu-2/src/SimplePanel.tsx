import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
//import { url } from 'inspector';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  ///const theme = useTheme();
  const styles = getStyles();
  let ats_url = 'https://bmsclouduio.i.telconet.net/d/vr6vgbd7z/boc-grp-ats?orgId=1&from=now-3h&to=now&refresh=5s';
  let ups_url = 'https://bmsclouduio.i.telconet.net/d/NB5bzbdnk/boc-grp-ups?orgId=1&from=now-3h&to=now&refresh=5s';
  let pdu_url = 'https://bmsclouduio.i.telconet.net/d/pSO0zxdnz/boc-grp-pdu?orgId=1&from=now-3h&to=now&refresh=5s';
  let gen_url = 'https://bmsclouduio.i.telconet.net/d/LbFTkxO7k/boc-grp-gen?orgId=1&refresh=5s&from=now-3h&to=now';
  let uma_url = 'https://bmsclouduio.i.telconet.net/d/DzdJkbd7k/boc-grp-uma?orgId=1&from=now-3h&to=now&refresh=5s';
  let chill_url = 'https://bmsclouduio.i.telconet.net/d/r5Lnkbd7z/boc-grp-chi?orgId=1&refresh=5s&from=now-3h&to=now';
  let var_url = 'https://bmsclouduio.i.telconet.net/d/td_ONbOnz/boc-grp-var?orgId=1&from=now-3h&to=now&refresh=5s';
  let elec_url = 'https://bmsclouduio.i.telconet.net/d/U3YPmbdnk/boc-prin-elect?orgId=1&from=now-3h&to=now&refresh=10s';
  let aacc_url = 'https://bmsclouduio.i.telconet.net/d/-g2Bmbdnz/boc-prin-acc?orgId=1&from=now-3h&to=now&refresh=10s';
  let inc_url = 'https://bmsclouduio.i.telconet.net/d/yE-smxd7k/boc-prin-sis-inc?orgId=1&refresh=10s&from=now-3h&to=now';
  let principal_url =
    'http://bmsclouduio.i.telconet.net/d/e1ULmxd7z/boc-prin-dc?orgId=1&refresh=10s&from=now-3h&to=now';
  let historian =
    "http://172.30.165.13:5601/app/dashboards#/view/831b9470-305c-11ec-9d08-3b27680be30c?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:'2016-01-01T07:30:00.000Z',to:'2016-01-10T04:30:00.000Z'))&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),query:(language:kuery,query:''),timeRestore:!f,title:'Data%20Historian',viewMode:view)";

  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox="0 0 555.625 47.625002"
        id="svg14957"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        xmlns="http://www.w3.org/2000/svg"
        //{...props}
      >
        <defs id="defs14954">
          <linearGradient id="linearGradient1156">
            <stop id="stop1152" offset={0} stopColor="#09a8ae" stopOpacity={1} />
            <stop id="stop1154" offset={1} stopColor="#09a8ae" stopOpacity={0} />
          </linearGradient>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath10124">
            <rect
              id="rect10126"
              width={221.73434}
              height={84.917526}
              x={-107.32134}
              y={40.867176}
              ry={10.796124}
              transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
              fill="#00f"
              fillRule="evenodd"
              strokeWidth={0.349927}
            />
          </clipPath>
          <clipPath id="clipPath1069" clipPathUnits="userSpaceOnUse">
            <ellipse
              ry={92.971107}
              rx={51.450279}
              cy={346.14044}
              cx={-248.095}
              id="ellipse1071"
              opacity={1}
              fill="olive"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="none"
              strokeWidth={7.53068}
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
          </clipPath>
          <clipPath id="clipPath5494" clipPathUnits="userSpaceOnUse">
            <rect
              ry={1.2665578}
              y={373.44049}
              x={1007.6845}
              height={226.78572}
              width={102.80952}
              id="rect5496"
              fill="none"
              fillRule="evenodd"
              stroke="#61625b"
              strokeWidth={4.99999}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14840">
            <rect
              id="rect14838"
              width={221.73434}
              height={84.917526}
              x={-107.32134}
              y={40.867176}
              ry={10.796124}
              transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
              fill="#00f"
              fillRule="evenodd"
              strokeWidth={0.349927}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14848">
            <rect
              id="rect14846"
              width={221.73434}
              height={84.917526}
              x={-107.32134}
              y={40.867176}
              ry={10.796124}
              transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
              fill="#00f"
              fillRule="evenodd"
              strokeWidth={0.349927}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1984">
            <path
              d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
              id="path1986"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2072">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path2074"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <filter
            height={1.1713483}
            y={-0.085674155}
            width={1.1634072}
            x={-0.081703613}
            id="filter2091-9-7"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur2093-4-3" stdDeviation={0.2879845} />
          </filter>
          <linearGradient
            gradientTransform="matrix(1 0 0 .65243 -4.131 -67.054)"
            gradientUnits="userSpaceOnUse"
            y2={145.97466}
            x2={260.96414}
            y1={172.82849}
            x1={260.96414}
            id="linearGradient1158"
            xlinkHref="#linearGradient1156"
          />
        </defs>
        <g id="layer1">
          <path
            d="M375.908 10.801v12.161h46.371l6.548-5.746V5.256h-46.572z"
            id="path3861"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.5}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path3863"
            d="M376.228 14.804v-4.027l6.123-5.42h4.681"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M428.546 12.745v4.244l-6.198 5.713h-4.737"
            id="path3865"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.03278}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={387.22861}
            y={16.393799}
            id="historian"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3927"
              x={387.22861}
              y={16.393799}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={historian} target="_blank" style={{ fill: '#00abd6' }}>
                {' '}
                {'HISTORIAN'}
              </a>
            </tspan>
          </text>
          <path
            d="M541.985 32.835l-9.268-6.287-518.71-.044-3.21 1.877v.874l3.136 1.659c-.049 2.778-.206 4.236.027 6.172l3.474 3.1v1.66l-1.896 1.178v1.921h506.97l2.26-1.135 3.938 2.402h8.242l4.818-2.482z"
            id="path1142"
            opacity={0.2}
            fill="url(#linearGradient1158)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.484638}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g2002"
            transform="translate(9.048 -.169)"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M140.102 14.825h-.86l-.004-.847.784-.077z"
              id="path10142-9"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path10142-3-4"
              d="M140.176 13.388l-.776-.37.362-.767.74.269z"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M140.89 12.027l-.585-.63.617-.58.59.522z"
              id="path10142-3-2-5"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path10142-3-2-1-2"
              d="M142.029 11.061l-.297-.807.793-.296.343.709z"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M143.434 10.59l.056-.86.848.016.057.786z"
              id="path10142-3-2-1-6-1"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path10142-3-2-1-6-7-2"
              d="M144.938 10.728l.363-.78.784.32-.23.754z"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M146.263 11.39l.644-.572.592.608-.511.599z"
              id="path10142-3-2-1-6-7-9-1"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path10142-3-2-1-6-7-9-8-1"
              d="M147.238 12.544l.806-.306.337.778-.69.379z"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              d="M147.764 13.878l.859.067-.027.847-.886.036z"
              id="path10142-3-2-1-6-7-9-8-7-9"
              display="inline"
              opacity={0.998}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".0159077px"
            />
            <path
              id="path10270-8"
              d="M140.962 14.925l3.479-5.945-1.167 3.786 2.892-.87-4.347 8.51 1.598-5.839z"
              display="inline"
              opacity={0.998}
              fill="#ff0"
              fillOpacity={1}
              stroke="#fd6425"
              strokeWidth={0.289164}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
            />
            <path
              d="M140.609 16.826c.898 2.35 5.457 2.631 6.759-.244"
              id="path949"
              display="inline"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
            />
          </g>
          <path
            d="M165.633 9.672l-.385.322.022 1.336-.86-.435-.26.157v.408l1.08.614v1.04l-.816-.448-.018.905-.873-.47v-1.314l-.314-.215-.3.166v.986l-1.282-.749-.359.207.036.515 1.152.618-.735.458-.01.367.409.229 1.066-.596.73.466-.64.47.668.466-.811.471-1.05-.619-.371.18.004.41.743.429-1.155.667v.488l.404.21 1.2-.707v.887l.315.197.322-.237v-1.224l.928-.448v.87l.758-.511.004 1.062-1.045.591v.368l.31.22.793-.458v1.331l.394.323.296-.332V18.03l.825.462.282-.188-.013-.516-1.013-.51v-1.031l.793.493v-.83l.883.484v1.193l.323.237.34-.246v-.942l1.143.735.417-.188-.027-.475-1.134-.627.722-.47v-.39l-.292-.185-1.062.556-.829-.435.695-.48-.686-.402.798-.57 1.116.623.318-.197-.027-.403-.757-.435 1.174-.654-.045-.507-.354-.161-1.174.623-.014-.91-.345-.197-.367.228v1.35l-.892.457v-.92l-.73.516v-1.02l1.113-.643v-.386l-.339-.195-.836.483v-1.48zm.376 4.43l.37.591-.36.596-.733.005-.37-.591.361-.596z"
            id="aacc"
            display="inline"
            fill="#3fbefa"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0177471px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="incendios"
            transform="matrix(.0645 0 0 .0645 194.611 -8.664)"
            clipPath="url(#clipPath1069)"
            display="inline"
            opacity={0.8}
          >
            <image
              transform="translate(-1302.83 -112.597)"
              clipPath="url(#clipPath5494)"
              id="image2379"
              xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5gAAAKACAYAAAD5Otf/AAAgAElEQVR4XuzdTawm13kf+Lomm/De CGwhDhgBBuyVrbEkUAlJbRIkMSI2BQRhAwqz6GA8YEReMhvBgA2thBlgwE3ISzLMOABnQREggwBs xoFjw56Fu5mYkOWRvbIBA44RCbQn8D7opvgOziWrVV1dH6fOW1XvOVW/u+mPW6fqnN9T9/b991Mf Z5UPAgQIECBAgAABAgQIECAwg8DZDPuwCwIECBAgQIAAAQIECBAgUAmYTgICBAgQIECAAAECBAgQ mEVAwJyF0U4IECBAgAABAgQIECBAQMB0DhAgQIAAAQIECBAgQIDALAIC5iyMdkKAAAECBAgQIECA AAECAqZzgAABAgQIECBAgAABAgRmERAwZ2G0EwIECBAgQIAAAQIECBAQMJ0DBAgQIECAAAECBAgQ IDCLgIA5C6OdECBAgAABAgQIECBAgICA6RwgQIAAAQIECBAgQIAAgVkEBMxZGO2EAAECBAgQIECA AAECBARM5wABAgQIECBAgAABAgQIzCIgYM7CaCcECBAgQIAAAQIECBAgIGA6BwgQIECAAAECBAgQ IEBgFgEBcxZGOyFAgAABAgQIECBAgAABAdM5QIAAAQIECBAgQIAAAQKzCAiYszDaCQECBAgQIECA AAECBAgImM4BAgQIECBAgAABAgQIEJhFQMCchdFOCBAgQIAAAQIECBAgQEDAdA4QIECAAAECBAgQ IECAwCwCAuYsjHZCgAABAgQIECBAgAABAgKmc4AAAQIECBAgQIAAAQIEZhEQMGdhtBMCBAgQIECA AAECBAgQEDCdAwQIECBAgAABAgQIECAwi4CAOQujnRAgQIAAAQIECBAgQICAgOkcIECAAAECBAgQ IECAAIFZBATMWRjthAABAgQIECBAgAABAgQETOcAAQIECBAgQIAAAQIECMwiIGDOwmgnBAgQIECA AAECBAgQICBgOgcIECBAgAABAgQIECBAYBYBAXMWRjshQIAAAQIECBAgQIAAAQHTOUCAAAECBAgQ IECAAAECswgImLMw2gkBAgQIECBAgAABAgQICJjOAQIECBAgQIAAAQIECBCYRUDAnIXRTggQIECA AAECBAgQIEBAwHQOECBAgAABAgQIECBAgMAsAgLmLIx2QoAAAQIECBAgQIAAAQICpnOAAAECBAgQ IJCZwAvPPXu4ePW16t0bN6qrV6/6eS2z+pgOAQL9Ar5hOTsIECBAgAABAicWqANlexoC5okL4/AE CEwWEDAnkxlAgAABAgQIEDhO4L333ju8+OKL1fu3bg3uSMA8ztloAgTWFxAw1zd3RAIECBAgQGCH An1dykBxraqqJ8/Oqqc+dXnwcLj8nYC5wxPFkgkULiBgFl5A0ydAgAABAgTyFBjrUoZQ+e2z7h/F BMw8a2pWBAiMCwiY40a2IECAAAECBAiMCsQEymaXcmiHAuYotw0IEMhUQMDMtDCmRYAAAQIECOQt EALl7/72b1Xhaa99H281LnsdWs3ZlSvV7aefrq688cblZgJm3rU3OwIE+gUETGcHAQIECBAgQCBS YM4u5Z3r16uHHnmk+uiLX6we/NznLmdweOCBewLmx4eDn9Uia2MzAgTyEPBNK486mAUBAgQIECCQ oUBKoAzdyMOdO/etpu5S/thzz90NlM2NPvzww+qnfvqnBcwMzwNTIkAgXkDAjLeyJQECBAhsVGDo 6Z7NJT/62GPVzZs3/du50fOgXtbFxcXhheef71xleDBP+Oh7OE97UN2l/PArX6k+85nPDMoJmBs/ sSyPwE4E/CO5k0JbJgECBAjcK/D4448fxt5BOGQWwuZTTz1VnZ+f+7e08JNr7D8Y2q8Q6Vtu3aEM l71Wv/zLSSoukU1iM4gAgYwE/KOYUTFMhQABAgSWFxgLliE4/uIv/Hz1Mz/7c9Wf/emfVH/4R398 OamhMOo+ueXrNucRxh7O0w6UITiGj5TLXqfMWwdzipZtCRDIVUDAzLUy5kWAAAECswr0danOn/16 9Su/+mujly/Wk3nllVeqd955pzNwCpqzlmzWnQ11KWM7lGFCY/dRHjNpAfMYPWMJEMhFQMDMpRLm QYAAAQKLCfzY2dmhvfN3b9yonnjiid5jhh/2h+6ZC5+/du3afUEz7Pfq1av+fV2smnE7Tnk4T9ee 6+5ldXFRxdxHGTe77q0EzGP0jCVAIBcB/wDmUgnzIECAAIFFBNrhsi9Y1oGyHSz7/r6e7H/8j/+x evHFF+8Jmi+9/LJ7Mxep5vBOhy5/zqVLOcbiHswxIZ8nQCB3AQEz9wqZHwECBAgkCzTDZbi38vd+ 7/fu21dfp7Lr75thM+yo2eH8V+fPVRevvnZ3/+HS25deedW/s8nVGx849LTXMDqEypinvS552ev4 Kn60hQ7mFC3bEiCQq4B/+HKtjHkRIECAwFECzXAZwt6/vnhlcH9jHczm4HbQrMNmuD+z+XoLl8se VcLOwVO6lH3voww7rl8f8tEXv9j5Tsr5Zz6+RwFz3MgWBAjkLyBg5l8jMyRAgACBiQJD4bIdJGM7 mF3j2tMKHc1wyexXn3zy7qeEzInFa2weOpR9D1QKm8Ve9lrfR3n76aerv/7Wt6If6JQ+87SRAmaa m1EECOQlIGDmVQ+zIUCAAIEjBZpPC+27LLZ9iLHuZTOEht+Pffyf/8f/fs/lsp4uOyb2o8+Hh/M0 A3p7ZGyoDONCl/LHnnuu+h8/+ZPZhsr2+tyDGX+u2JIAgTwFBMw862JWBAgQIJAg0A4nP/z443v2 ktq9DDuJ6WA2D9YMmSHo3rx507+5HTWd2qXsu+y1vo/yoUceqapf/uWEs+f0Q3QwT18DMyBA4HgB /9gdb2gPBAgQIJCJQPPS2PbTYscuhe17emxzaUPdyz/4gz+ovvCFL9wj8dN/82/e/bMu5icU4T8B /uIv/uKee1Xbp89bZ2fVUxHnVC4P54mYatQmH33ve9UDn//85bYPHj55s47zJorORgQIZCQgYGZU DFMhQIAAgXSBZvey+VCfsWDZPmL7cthwX2Vs97IOmfWv/+E//Ie7QWrPXcxw2fIf/tEf3/fO0GAf LnkNH09GhMotdCmHzjcBM/3r30gCBPIREDDzqYWZECBAgMARAs3uZfvS2LDbKfdZ9oWAmPsv2yHz 2rVrd4PVXrpR4bLXP/vTP7nnPtSmaex9lFsIlO3zLoTIcE9o+Pgbf/VXl0+wDX9X/9ruYHpI1BHf FAwlQOAkAgLmSdgdlAABAgTmFqgDZvuVJH0dzGboHAqU7Q5me9t2oOz6c/3Qmi2/G3PonZR1oAx2 Q5e+1k97rS4uqpxeHxJ7rjbDZFd4DPtpB8r2vgXMWG3bESCQq4CAmWtlzIsAAQIEogWa4aZ972Uz SKZ2MWM6l+E4fWFzi/diDl32GixCqPz22fiPGSXcR9nXhQwhMnQjw6/tjzpIRp/En24oYE4Vsz0B ArkJjH/nz23G5kOAAAECBFoCfZfHDnUvm8GzDdr3tNmuoDnUwQz7DQ/+2cplss1XwLTNplz2ejk2 wy5lV5BsdxzrQNn++7HOZMwX7YPf+U51eOaZy03rh/y4RDZGzjYECOQkIGDmVA1zIUCAAIEkgTpg dr33cuzVJH1dzWYAjelgNoNmHSy7Hvbz0ssvV+fn50X8+ztHoAwWl5e+njhQDoXHdmisT8K+7mTS SdoYFILk5SXAHb8KmMfqGk+AwKkFivgH7tRIjk+AAAECeQvUATOEt+eee+5ysqndy/bYsXA51sGs 5er7MHMOmOFJvC+++GLn017DOmK7lGHbO9evVz/23HOX9xyu/THlXsgwt/blrKmXt9aB8XKfAyGy HS7D9vXfCZhrny2OR4DA3AIC5tyi9keAAAECqwvUATPm/suuADn0KpL6c+1FNd972RUy6+3DJbLh 83XAzOl1JXMFyubTXj/8yleqYLbUx5T7IVODYszcpwbImH2GbQTMWCnbESCQq4CAmWtlzIsAAQIE ogSaD/j5/g9+cE+46epijnU22wdN6WCGfdTBst5fHTDDn0/5upLg9c477wx2KccezlM/7fX2008v 0qUcCpHN0Ni8hLX593MFy2M6knXd685k+HPz930nt4AZ9WVvIwIEMhYQMDMujqkRIECAwLhA8z7B +v2X7RA5FDSnPNCnns3YZbHtcBn+fKonycYEyifPzu6+PiSEx8OdO/fBz/2019q9+Q7Ivnsh57qE tets6upENsNgOxTGhMTxs7Z7i9sffFBdeeONy096yE+qonEECJxaQMA8dQUcnwABAgSOEnj88ccP 79+6dbmPOmDWO2wGy6GH+bQn0Nx2bHJDl8fWY9cMmOGy19/97d+qLl59rXPqU++jfOiRR6rql395 jOG+z7cDZN0Jbr4f8jJIfe5zVQiZ7d93/XnyJBoDpgTJrhA5V7AMIfLStKqq+vfNXwXMY6psLAEC OQgImDlUwRwIECBAIFmgq4M5trOhrmXfPZf1Pqd2L+txzYfnzH2J7Ng7Kd/69H2UT43ATH3aa8xr PU4dIMOShx6qM3aupH6+KzyGYNn19+EYdz/35puXHWQdzFR54wgQOLWAgHnqCjg+AQIECBwl0NXB HAuQfQfsGjc0uZjuZRg/dwdz6mWvXWuIuY+y2QGuL2UNfxe6kH2Xsx5VzJHBfV3IU4TIvk5kvYR2 mIx10cGMlbIdAQK5CgiYuVbGvAgQIEAgSmBqBzPmvZh9B07tXob9HdvBbD7MqD2/lMteL7t6Ha8Q Gbo3cihUHvNgnb6H6TTXmUsXMsypKzymBsp2LQXMqC97GxEgkLGAgJlxcUyNAAECBMYF+u7BHAuS zT2377kcu0w2jI3tXtbHaT5FdnxV41vEXPY61qVshsnmE1nHj37cFs1OZNjTUHic697HsRk3L10d C5Fj+zrm8wLmMXrGEiCQg4CAmUMVzIEAAQIEkgWmdDDHQufQw32O6V6GxdUdzJdefrl6+OGHB9cb Lqm9du3aPa8SSelStt9J2b68tWsSqZ3IsVd6NI/VFSiTT4CRgX33PLaHzdWBnLKOh958swqvemn+ Gsa7B3OKom0JEMhNQMDMrSLmQ4AAAQKTBGKeIjv27sspT40Nk5vavQxj6g7muzdu3H1HZv06k65f 64D50acP6BlCuXP9+uVlm+3LXutAOQk0YuOueyHbobHr/Y9rdCPHOpFDD9qJWPrkTerwGAZ2BcoQ MJufa3cw534g1OQFGECAAIGJAgLmRDCbEyBAgEBeAkMP+WnPdKiDWb9Go2t1x3YvmwHz+z/4wWVA rT/a78wMf193MH/61q3q2x0Bs/lOynBpa7ikN3zUr/s4pkIxncg1O5D1WqZ2Ii9D26dPbT3GI2Zs X3BM+fv6HaT1U2QFzJgK2IYAgZwEBMycqmEuBAgQIDBZYOwezLDDoQ7mUk+ObS8kdDAffeyx6u23 374bMNvhstnJDB3MOmDWgbLdpZyjQznUjbwMreFhQN/5zicBtvH79vrm6k6OvsajERrXuqx1ahcy JlgGv/ry2KalgDn5W4ABBAhkJiBgZlYQ0yFAgACBaQJDl8i2w2U7TDY/39fBHOtehn10dSGbq/jd 3/6t6uLV16r68tj6c/W+u/4cAmb4+H9efvnylSDhI7weJPZjLDjW+1kjNF6GqQ8+GHz6aj2frq7j 0p3IsUAY5ja0TT33rvsp+4JkO1w2/9y8B1MHM/aMtx0BArkICJi5VMI8CBAgQCBJIKaD2bfjY+69 jAmW9XFD9/L82a9Xv/Krv3b5V0PBMnw+XPL65S9/+XLb0PHsC5Z974VMvR/y2C7k0Lshh17tcRng Tnw5692A2/PgnWYA7Pp9Vzey6+9iTnIBM0bJNgQI5CogYOZaGfMiQIAAgSiB2A5mV/cyHKDuXHa9 mmSoeznWtQz7/ou/+Ivqheefv7w09vd+7/fuHmtsYc2AGTqY4WPKqz3G9p/6+ZgAeTeodVzKmnrc mHFjl7GGfbQ7jEOhsKsbOdR1jJlj7DYCZqyU7QgQyFFAwMyxKuZEgACBmQTee++9Qwg5fR9jr8u4 evXqWdhH+HWmKR29m3o+9a9DD/kZu/cyTGYoWNaTTXlqbBhb33cZwmX90dU1bc+hHTD7wuXRmD07 iLmctdlx7Pv9UvOLeRrr2GWvMZe81vNP7UROXX99/2UY5yE/U/VsT4BALgLZ/MCQC4h5ECBAYAsC P3Z2dtjCOqau4Ycff9w5JOXey7Cj5qWszZAZPjfWwewKl1PW86/On6v+8I/++PIezPohO1PG923b FR7bl6eO/XmOefTto68TGbbv60COXbI6NSBO3X4uDwFzLkn7IUDglAIC5in1HZsAAQILCLzw3LOH 8ECZPX60A2ZXB3PqU2ObQbN+vUhMuAzj+gJvTG3qezB/75//85jNe7cZCpRjQXLJ+yKndhjDAse6 jjGXsMZc+noUeMLgZrCsh+tgJkAaQoBAFgICZhZlMAkCBAjMJ9C8ZHS+vZaxpzrQtYNlX9DsW9Ux 916++OKL1fu3bh0VLsO8Qgfz/3v1teqtf/tvJ4XH9sZLhsS+iU0Nj2PBsT5OX2dxSrDM6UzuCpYC Zk4VMhcCBFIEBMwUNWMIECCQscBeL48NJenqGDbD5Rzdy6HS1w/1+f4PfnB5b+cxH+2A2exE1vsd ejLr0sFy7KE6IfSFjylhM2w/drnrWNjsMw9hLrxPNIePoWApYOZQIXMgQOAYAQHzGD1jCRAgkKGA gPlJUfoe8DP2apKY7mXfJbL160j+9cUrR58ZzYDZDpdLh8e7AbbjlR3NS0ynBshT3dsY5tkMdWsF zZgg2XeiuET26C8hOyBA4EQCAuaJ4B2WAAECSwkImPfLHtu5HLvnMhxxzu5l2F8dMP/vf/EvljpV Ovc7teO46uSOONhY2Auhs9nlrH9fj2t/vp5Ke7sjpnjZYa2PJ2AeI2ksAQKnFBAwT6nv2AQIEFhA QMD8Ufey717M+t2Xbf6h7mXYdihohnsvw0fzlSTHlHeugNkMjH3zie3uxW7XDF/tY67RPRwLk8fU Za2xAuZa0o5DgMDcAgLm3KL2R4AAgRMKhHdDhss09/ox9NTWsUtjh8LmWLgMnw/uL738cvXcc8/N wt8MmDEhMRy0K1iNBbqhMNbsqE0NiilzaR+jvY+++QzNc5ZiLLiTvrkLmAui2zUBAosKCJiL8to5 AQIE1hXY8xNkg3QzYPa9+7KvIjH3Xg5VMwTMd2/cqJ544onLzcYeLhQeAtR16W79cKA6YH777Ef/ VPeFxbGO3VDIHBs7tOau/cbsb6n5rPvVNn60Y4JvHTDDOXX16lU/r41z24IAgUwEfMPKpBCmQYAA gWMF9vz+y9purIMZtquD3ZSgGVObEDDrp8f2XYIbs596ju2AeWwoSw2DMXM+JkgdM3Ys/MaE3Zj1 zb1NzJoFzLnV7Y8AgbUEBMy1pB2HAAECCwvs+d7LdsDs6wwuce9lfey6gxnu0zz2I4TgL3/5y9VP 37pVNTuYYb/NoDg1QMUEm2Pn3jxG1/HWmEPXGtrHjZlb7Fxjtxuz7XrIjw7mmJrPEyCQm4CAmVtF zIcAAQIJAsLlJ2hD78GM7So2L5UN+xx6sE94cuzDDz98eez6Hsx/8k/+yd0Kxlx2W++/+WvYwbVr 1zoDZsLpMXnIWEisg+7UgBszkbH7PtsPGlpiDjHzXHobHcylhe2fAIGlBATMpWTtlwABAisJCJc/ gu66BzN8dihcxoTAoVLWIbN+B+av/OqvVWGfzXDaFSLHjnvKgLnEqRvTMWwfd2pncOr2S6yzK3xP Cc31nATMpapjvwQILC0gYC4tbP8ECBBYSGDvD/TpYu3rYIZth+69HHs9SUwJw2tK3r916/I+zDpg xozr2iYE0qUDZswlo1MCXy7hrstzbG5jn6/3mWI2xbB5nAdu3778o0tkU7+KjCNA4FQCAuap5B2X AAECiQJ7fxXJEFu7gxkbLMN27Utj6+P0XSJbdy6bv77w/POXryqpL5tNLPHlsBBY2/dgpnQBw75i A9Qx822OTTlezJjU9U+ZW8zlwXM5De1HB3MNZccgQGAJAQFzCVX7JECAwEICFxcXhxBifHQLDHUw h8yO6WCGgBk+QqgMl8k++thj1Te+8Y2jS9QVMI/eqR0UIyBgFlMqEyVAoCUgYDolCBAgUIiAS2LH C1UHzOZTZLtGjd3/OHaJa7NrWYfLOmiG/wA4f/br1d/7B/+wqv+uvc3YSkJYPSZgxnQD++Ywx2Wg zX2n7G/O+Y9ZH3OssO+luqoC5ljlfJ4AgVwFBMxcK2NeBAgQaAh4kE/c6TD1Etmw166wGf5+6Omx 4fPtkFnP8J133rm8F7O+VLYZMuNW8Uk3dCxgHhOMUkJfe+5z7KMvoI0dK3ZcrPeU7Y5xj513OIZ7 MKdUxbYECOQkIGDmVA1zIUCAQEvA/ZbTTok5OphjRxzqXoax9aWy4fchZKZ+hKDa9R7Mrv2lPKU0 ZV7HhquUYx4zZo7u4jHHr8emuOlgziFvHwQInEJAwDyFumMSIEAgQsD9lhFIrU26Ophje0m5/7Kv e9k8Vt3JDJfL/szP/lz1Z3/6J2NTufv5sH1XwIwJKjHbxExkjv2k7GOtsDxmkDL3sX1O+byAOUXL tgQI5CQgYOZUDXMhQIDApwIvPPfs4eLV13hMFGh2MLuGpoTJ5n66nhw7NMX6gUwhZIaPZtAcC51/ +Ed/HN3BjGWa67LW5vFi97nU01mX2u/QGuvPxa69rz5DIVbAjD2rbUeAQG4CAmZuFTEfAgR2LyBc pp8CsZfI1kdICZwx3cvQrQwBMny0Q2bs6sJ/MFyrqurbZ2n/VKd04PrGpOwrdp1hu6W7ljHrWmKN qft0D+aUs8e2BAjkJpD2r1ZuqzAfAgQIbETglOEyvF4jfPziL/z85a91QAr3FKY8qCbso/k+yKtX ry7yb07TbMkOZmz3sg6XzV9DWAy+tW3oTobf1792nb59Hcw1OnZDX06poWnOL9FTzuHYY8eO18Gc 84yxLwIE1hRY5B/7NRfgWAQIENiSwJpPi63f17hU8FurLs3Xt4wFzDCnY15REtO9DMcYCpl1iG+G zLZVCJ/HdjDrfaZcxhkbgmJqPHVfU7fvm8Ox+4kdH7tdjFVzmzpgfnw4+FltKp7tCRA4qYBvWifl d3ACBAj8SGCtcBmC5c2bNzfz/b8dMMM7MNsfdais/74ZMmPOwa5g2dXZbV4a23yoT+gG15fK1p3i rlAZQmf9EV51MuUS2aWCzpBPSjc1dZ6p42Lqe+ptutYmYJ66Ko5PgECqwGZ+wEgFMI4AAQI5CKwR LsODZl565dXNfd+f0sE8pnsZzpOYDmZX9zImYLbPw6kB89jzeCzAjX0+9fjH3H+ZMqeUMalrO2ac gHmMnrEECJxSYHM/aJwS07EJECCQItAMSCnjx8ZsNVjW6276ff8HP+jkaHYwU7qX9U777kftCpXN icwZMGMDUtd2Uy+XjT1WGz3m2GPn7dTPzzXXqUbNeY6NjZljvY2AOfUMsD0BArkICJi5VMI8CBDY pcB77713+OqTTy6y9q0Hy66AGe7B7LpENmx7bPcy7GPsYUd9QbMZMKcUO1wi++TZWfXUwKCY0DLl mDHbLnnMJfa9xD5jnMa2GZqXgDmm5/MECOQqIGDmWhnzIkBg8wJLhcuXXn65Oj8/383396EO5hz3 XsaciGMdzLCPOd5r+tZI2IyZ69g2U8NY6iWuU46Tcq/n2DqP/fyU+accS8BMUTOGAIEcBHbzA0gO 2OZAgACBpsDc912+e+NGVfoTYVPOkCmXyKa897Ke09j9l0Mhs93BjHkyaPgPiBdffLEK92J2ffR1 N2MuT00JRzFjYrbpWssS48b2Ofb5lHMxdUzXXATMVE3jCBA4tYCAeeoKOD4BArsUmPN9lzFhZcvI sR3MY+697POL6VzWY5sdzJSajXW8Q3czfAxdTjtnuJt6Tk0JdFO2recRM6Zvm2PunZzawY2ZZ1iT gDn1DLM9AQK5CAiYuVTCPAgQ2JXAHN3LvXYs2yfKWAcz5d7LulvZPFbq/ZdhHykdzKEviBA2f/e3 f6v3stuYezd39QW34mJjA+RY2BcwVyyaQxEgMKuAgDkrp50RIEBgXGCOcJnSARufWZlbxHQwp3Yv g8RYoAzbrNnBHAucQ5fTTulujnXzwjxiQlTMNrFn3Jz7ij1m33ZrzUXAPLZSxhMgcCoBAfNU8o5L gMAuBS4uLg4vPP988toffeyx6ubNm753NwS7AubQw32+8IUv3PNE2XYxmt3Lofsu63BZB83wa+hU hr9vf8zdwRw7gYYuwV6qu5kSvGLCbNdax4419vkxvyU+P3VOAuYSVbBPAgTWEPBDyhrKjkGAAIFP BY7pXgqX3afREh3MsQf61DPJpYM59AWW+rCgOb9oU4JkbCCL3S6sJ2YeU/Y3p1F7XwLmkrr2TYDA kgIC5pK69k2AAIGGwDHhMuzGZbHxAbPrvssweqx7GbZphsuHH36491LZKeFy7Q7m0BfeWBd9jVeh lPSNISZwxmzTteahcQJmSWeJuRIg0BQQMJ0PBAgQWEFg7AmgY1MQLvuF5uxgLtG5rGd+7FNkx86R lM/P0d2M6QqmzK09JiXEpYyJnevUp8dO3a+AGStmOwIEchMQMHOriPkQILBJgWO6l54WO3xK9AXM vi5mzAkWEzSbHcywz777L+vPNe+9zfU/DJbobk4NeTHbLxXuxs6NmLmNBePYfQiYY9XweQIEchUQ MHOtjHkRILAZgWO6l+67HD8Nmg+0+f4PfnA5YOwS2a69Tg2VdaAcCpbN4+TYwRzTbYb39rYpDwuK fQ/l2Lyan48NbGOd1tj9TJlb7LZdx64Dpv9gilW0HQECuQgImLlUwjwIENiswDHdy1w7XTkVa+gp siFoho/63suhecfee1nvo93BHNp3TvdgpttVJZkAACAASURBVNZu7HLaKa9CmTKHUwW/Kcedsm3s 2gXMWCnbESCQm4CAmVtFzIcAgU0JHNO91LmIOxWmdDD79hjTvQxj61eQtLuWMV3MEjuYfV7hvP7d 3/6tqrmm5rYx3c0lQlncGVPGVgJmGXUySwIE7hcQMJ0VBAgQWFBg6H2EQ4c9f/br1UuvvOp7dERt YjqYYTdTu5h9h57y9Nh6H1voYA6VYqy7GRM4I0p9d5PSw+nY/MPnH7h9+3K9/qNpyplhWwIEchDw w0sOVTAHAgQ2K5B6eaxLY+NPiWM7mKF7WX/0vZYkJVS2V7ClDuZYdYYeFjQUNo99eE9McDvcuTM2 /Xs+P7bPvp2ljqv3p4M5qUw2JkAgIwEBM6NimAoBAtsTSAmYwuW08yDmKbKxe4y5VLYOm2Gfsfdh br2DOeQbwuY777xTvX/rVudmqYEztqZTQ+vUYDh1+655e8hPbDVtR4BACQICZglVMkcCBIoUSLk8 9qWXX67Oz899b55Q8aZzuJww5lLY5u6bHcyhw8aGyb597KmDORY4m69saW8bHhb0VGT95wh3kYc6 arOUeepgHkVuMAECJxTwQ8wJ8R2aAIFtCwy94qFv5bqX98sc86Ck0s6wvdV/7Xs35zofpnZFpx7X PZhTxWxPgEBOAgJmTtUwFwIENiUw9fJYD/PoLn8zYIb3goZLLZu/Nke1P1dflllvn/sJtreA2a7H XIFzyvs2u7Zt/l1K93HoPBt7H2c9Vgcz969W8yNAoE9AwHRuECBAYAGBlK7b3sNFXxlqy+//4AfV Zz7zmdFqffjhh73bhPdixlxCO3TZbP2qkq6D/OEf/XH1i7/w81X4deyjGYbrbZ0D96oNXWY+95Np x+rV+4PUlStV34ODjgmnAmZqRYwjQODUAgLmqSvg+AQIbFJg6v2X7r3sPw3GAmYIlCF49gXLECrr j6Fw2RcqY+69jA2WXaGyuXIBc/g8ePHFF5MeFlTiNxkBs8SqmTMBAkFAwHQeECBAYAGBqZfHChbH Bcy+0XXHMny+GTSb28c+OXboNGkGzLEOZt9lvGH/zoP4L8ZTdDeP6UhOGesezPjzwJYECOQnIGDm VxMzIkBgAwJTAqbu5XDBhzqYfd3L5qWwza5lXwczpnsZXjXSvjx2rmBZCwiY6V/8Qw/VGrucdkr4 65vhHPto7lsHM/1cMJIAgdMKCJin9Xd0AgQ2KDD1/kuhYvgkCO9RDK+16LsHc+jS2DpQDgXLhx9+ uOrrYqZcHtvVway7lmGlOpjLf9HHvHvz22fH/Qg0R6Ac2oeAufx54ggECCwjcNx312XmZK8ECBAo WmDq60kEzLSAOXTvZV8Hs+tIQ5fINgNmVwcz7C+1i9k1F+fC/F/6cz2Zdv6Zde+xDp0C5lrijkOA wNwCAubcovZHgMDuBaZcHnv+7Nerl1551ffigbMmtYNZ77K+93LuB/xMCZZ9ncv261MEzOW/faR0 N1O7lWOvQBlabR0wnRPLnxOOQIDAvAJ+qJnX094IECBQTQmYfngcP2H6AmbMpbFh7zHBsr5Mtjmb mMtjw/YxQXPostjmMZ0P4+fDnFuE7ubv/vZvVRevvta527F7N8Og1PDZPGDXPgTMOSttXwQIrCkg YK6p7VgECGxeoA5DsQsVKMalatMffvzx+MafbjH26pKwWd9TZcPnlnwPpnswo8u4+oY5XU4rYK5e fgckQGAmAQFzJki7IUCAQBCYcv9luDzy5s2bvg+PnDp1wAxec3w0H7jT3F9X8Gtfwhq27/q7OeYV 9uGJwnNJzrOfU7wKpZ65gDlPDe2FAIH1Bfxgs765IxIgsGGBKZfHvnvjRnX16lXfh3vOh7Fu0oZP o8uluT83vwof8yqUqasRMKeK2Z4AgVwE/GCTSyXMgwCBTQhMCZguj+0u+ZQu8CZOmohF+M+ICKSV Nxl7WNBbZ2fVU0fMScA8As9QAgROKiBgnpTfwQkQ2JKA91+mV3PqvavpRyp7pMuq863fHN3N5sN+ BMx8a21mBAgMCwiYzhACBAjMJDCl8+byx0/QBcu0k0/QTHNba9TY5d3h6bTfPhv+EUzAXKtajkOA wNwCAubcovZHgMBuBVweG196wTLeamhLQXMexyX3EhM2n+y4nFbAXLIq9k2AwJICAuaSuvZNgMCu BATM8XJP6fKO780WTQH39JZxPsQEztDdFDDLqKdZEiBwv4CA6awgQIDATAICZj+kjuVMJ9nIbnQ0 13Ge8yhDr0IJx/EfB3Nq2xcBAmsICJhrKDsGAQKbF5jygJ+93X85JXh3nSjhfrVwCeG1K1c2fx4d 7typ/tnhUL195Eq9T/NIwBMN7+puCpgnKobDEiCQLCBgJtMZSIAAgR8JjHUhmlZ7eeXEsZfDhtc8 7CFUdn0dhaAZPo4Nm8JJ2d+lwveVl1551c9qZZfR7AnsTsA3rd2V3IIJEFhCYEqXbus/9B9zOWz9 dM3wuoa9f4SQWTu8fedO9bXDIYnEZbNJbAYRIECAQKKAgJkIZxgBAgSaAgJmVU25TLh99jSDZTNY Ocuqqu5mhrB5TNDc26XZzh0CBAgQOI2AgHkad0clQGBjAnsPmFMuEW6Wfu2OZR3Wjjn9cuiuHhM0 t95BP6a2xhIgQIDA8QIC5vGG9kCAwM4FpoSrLV6uOCVc16dKCJZvPfTQomdOX5hMDYhj4TR1v8cg pAZNDwE6Rt1YAgQIEBgSEDCdHwQIEDhSYMrDbLb0gJ/US2I/Oju7e2/hkfSXw4eCXwh9t59++nK7 hx55pProi1+875D/4yd/8u7ffeYzn6k+/PDDu3/+G3/1V/ds/+B3vnP3z7c/+KB66M03R48/xxrH 9vG127cnP3nWJbNjqj5PgAABAikCAmaKmjEECBBoCEzp4G3l8sQpobqmqp8KO8c9ll2h8s7163eD 5Idf+UoVwmL9UYfGZmAMwbIdINsndh0+m/vqO/k/+t73qhBA6+BZh9+1OpuprzjZyjnpmxIBAgQI 5CEgYOZRB7MgQKBggb0FzCnrDWWd63LYdqisu5M/9txzl2dPsxM5Fhz7gmT4+7qLGX4NoTF81J3L EB6bH6GDWX/UndK+zy99itc+71TV5CfOCplLV8f+CRAgsB8BAXM/tbZSAgQWEpgSuEr+QT7lkthj L4dth8rQpawvdX3wc5+7eznrlEBZdy7Dr3Vnsu4+hlOkDpHNy1+bXcjmnOq/H7tMd6FTb3C3Uy+b 3eL9wadwd0wCBAjsXUDA3PsZYP0ECBwtEBswS77nbeq7LevLYVNx7wtxFxeX90/WXcqYQNm8BLb+ fR1K6/Efv/LK3fsoQ1jsCopdl7h2XeY790OFUu2a46Y+BEjInEPdPggQILBvAQFz3/W3egIEjhSY 8gTZUgPm1M5larhsh8pwyWm4/HVKqAzlbN9bWXcqQ5cyBMrwUXcnxzqQKfdPNt9beeTpNcvwqfdm CpmzsNsJAQIEdisgYO629BZOgMAcAlMedlPiE2SnBOhj7rVshrIQLP/6W9+6LM9Qp7KrQ1nXtBlK L++fPD+/7E62u5QpAXKO8+YU+5jazSz5cu5T+DomAQIECHwiIGA6EwgQIHCEQOzlseEQpf3APuWy 2JRwed9De15//e5lsDGXwLbLds8TX3/91+8+zTX2stcjToPsh9bhWsjMvlQmSIAAgeIFBMziS2gB BAicUmCrAXNKZzblQT73XEZ6cVGF14qEj65gOdSpDGPal8BeeeON+06JPXUqY74epjwAqLT/GIlZ v20IECBAYDkBAXM5W3smQGAHAlsMmEuGy/alsPU9lsd0LMPY5sN6wmlXXworWPZ/EQqZO/gGZYkE CBA4gYCAeQJ0hyRAYBsCU+5PLOXBKVPC5Q8feujufY1jFW1ephpeNTIWLNsP6qk7lSFMdnUsm/dW CpVj1fjR54XMeCtbEiBAgECcgIAZ52QrAgQI3CcwJYyV8ATZ2MA89X7Le17p8enlsCkdy2bIDB3L 5qWwQmX6F+iU+zJdLpvubCQBAgT2IiBg7qXS1kmAwOwCW7o8dolwOaVr2dWxbHctw58/8xu/cfeJ sOHPguXxp3Wo0ztVVX3tcIjamZAZxWQjAgQI7FZAwNxt6S2cAIFjBbYSMGOfFjulc3lM17LrPZah 4/ngl750eUlu/SFcHnsG3zs+tpNZyuXe8+rYGwECBAjECgiYsVK2I0CAQEtgCwHzvffeO3z1ySdH azs1XNbdxY9+//cv75nsuyR27HNhPz/xzW9eXg4rUI6W6egNhMyjCe2AAAECuxcQMHd/CgAgQCBV oPSAuVS4DEHw9tNPV3/9rW/1Bssx8zp4NruWAuaY2jyfD13iByMul9XJnMfbXggQILA1AQFzaxW1 HgIEVhGIvWexnkyO963FBOTYzmXXey2ndi1DqKw/wr2Wh2eeuftn4XKV0/ryIFPuyXz3xo3q6tWr fpZYrzyORIAAgewF/KOQfYlMkACBHAWmPEE2x05PTLgM7uFVJGMfzfstxy6JHdpX3bVsPiFWsBzT X+7zsZfL5vifJ8up2DMBAgQIjAkImGNCPk+AAIEOgdiAFobm9oqS2O5rbLgMawxBcChcjr3XMuzD g3zy+1KLfU+mkJlf7cyIAAECpxIQME8l77gECBQtMCVg5nYZYczcx8LllFeQjBU6hM/6ktgQVO95 Au3YYJ9fXCAmZOb2nyiLozgAAQIECPQKCJhODgIECCQIxIS0erc5dXdi5v3R2dnoE1vrgHnn+vXJ D/Opu5n1PZf1U2LrTmhCOQxZWCAmZL708svV+fm5nysWroXdEyBAIHcB/xDkXiHzI0AgO4HYp6/m FjBj7ht96+ysunblyuXU+zqJU8Nl36tIwt8Ll9md3r0TeuD27dHJ5vSfKaOTtQEBAgQILCIgYC7C aqcECGxZ4OLi4vDC889HLzGHH7pj7ruMeWLs1HDZRmp2L4XL6FMoiw099CeLMpgEAQIEshcQMLMv kQkSIJCbQExYa845h4AZc2ls7H2X4bLYH3vuucGy9HUtwyCdy9zO6Pj5xFwq637MeE9bEiBAYIsC AuYWq2pNBAgsKhAT1uoJ5PCKkpj5jt13eUznstm1bD8p1mtIFj1VF9l5TMjM4T9VFlm8nRIgQIDA qICAOUpkAwIECNwrEBPYcgmYMfeLNu+77Kp1+z2XfefDUNcyjNG53MZXUjgfHjwcBheTw3+sbEPb KggQIFCegIBZXs3MmACBEwtMCZinfkXJ2Fxj77vse8/l2KWwoWPpabEnPmEXOHxMyPRU2QXg7ZIA AQIFCAiYBRTJFAkQyEtgLLQ1Z3vKSwVj7hUduu+y+a7LH373u5dBMQTGqR86l1PF8t6+7mi7VDbv OpkdAQIETiUgYJ5K3nEJEChSIOaS01wC5lgQjrk0Nqzl7PXXqw+/8pXecPmb//2/V7/0t/7W3WU3 g2j4/Wd+4zeqwzPPXH7ePZdFnva9kx57dYku5rbqbTUECBCIERAwY5RsQ4AAgU8FYt4lmUPAHAuX Y5fGHvNQn3r9wuX2v2xiXl1y6svEt18FKyRAgEBeAgJmXvUwGwIEMhcoIWDGdFljnhobuo0f/vmf 93Yu25fMtjuX4XLaBz7/+bsV1b3M/OROnN7YpbIe+JMIaxgBAgQKFRAwCy2caRMgcBqBsc5ge1an uAdzbI4xl8b2PdQnRr0Omg9+6UtV8wm0MWNtU5ZA3ekee6qsLmZZdTVbAgQIHCMgYB6jZywBArsT GAtvpw6YMd3L+sE+XeGvDgxj910219nVyfyJb36zuvLGG+653MlXiC7mTgptmQQIEIgQEDAjkGxC gACBWmBKwDzFpYFj8xvqXk6577LvibLuu9zv18rYA39O0c3fbzWsnAABAqcTEDBPZ+/IBAgUJhDT HWwu6fzZr1cvvfLqat9nx+YX+2Cfoe7lULAMaw/3Xbo0trATe6bpjj3w5xT/4TLT0uyGAAECBCYI rPaDz4Q52ZQAAQJZClxcXBxeeP756Lmt/YqGse7l0IN9pnQv+wAuu5ef/az7LqPPkO1tqIu5vZpa EQECBKYKCJhTxWxPgMBuBXJ+guyU7mXfvZf1g31iC9z3vktPi40V3N52upjbq6kVESBAYKqAgDlV zPYECOxWYKxD2IZZ856zsbnFdC9TLo0Na66DZv1KEgFzt18ilwvXxdx3/a2eAAECAqZzgAABApEC YyHuVAFzrHsZ5hWeHDv01Ng7169Xf/2tb/W+87K5Nk+NjTxhdrqZLuZOC2/ZBAgQ+FRAwHQqECBA IFIg14A5Nq+YJ8f2dS/7HupTk3lqbOTJs7PNdDF3VnDLJUCAQENAwHQ6ECBAIFJgLMidqoM5NK+Y J8fGdC+7gmb4u/DhwT6RJ9CONhvrYq79hOUd0VsqAQIETi4gYJ68BCZAgEAJAi889+zh4tXXoqe6 1g/QY5fHxnQvf/jd7969j3JsgX0P9gnj3Hs5prevz+ti7qveVkuAAIFaQMB0LhAgQCBCYOoTZNd6 RclYVzXce9n3Ud+T+eGf//l9917GXBrrnZcRJ86ON/na7dvV2wPrX/MhWDsug6UTIEBgdQEBc3Vy ByRAoESBsSDXXtO7N25UV69eXfx7bOrlsXcf+HNxUX34la9EPdynuUb3XpZ4Fq8753COPXg49B50 rS7/uqt2NAIECBBY/IcfxAQIENiCwNSAuUZ3Zuyy3bFXk4RLWru6l+0gGTqV4aPZ1Xzwc5+rqh// 8c4n026h3tYwj4DLZOdxtBcCBAiUJCBgllQtcyVA4GQCOQbMsTn1BcyY7uXQJbK6lyc7DYs6cDjP 3qmq6msDXcy1Ov1FwZksAQIEChcQMAsvoOkTILCOwFiYa89ijQ7m0JzGHu7T1b0cu++y2cV88Etf 0r1c59Qr+ihjl8k++thj1c2bN/0sUnSVTZ4AAQL3Cvim7owgQIDAiMDYk1q7hi8dMMceOjT0cJ8w 39tPP1399be+FXXvpSfH+hJJFQgB858dDh72kwpoHAECBAoUEDALLJopEyCwrsDUgLlGV+aYh/sE vbPXX68++uIX74Mc62K693Ldc28LRxt7J+bS/xmzBUNrIECAQEkCAmZJ1TJXAgROIjD2MJ32pNZ4 RUnq5bH1XNsP9+kLlu3upVeTnOQULP6gQw/7WeM/ZIoHtAACBAgUJCBgFlQsUyVA4DQCud1/eXFx cXjh+ed7MUafHvv666OvJukKnOHvfuKb36yuvPFGFe7h9EEgVmDonZgCZqyi7QgQIFCGgIBZRp3M kgCBEwpMCZhr/LCcev9luB8ufPzwu9+9R3Pssth648unx372sx7uc8JzscRDxzxN1mWyJVbWnAkQ INAtIGA6MwgQIDAiEO7BfPHFF6OcvvGNb1RXr15d9HvrMfdf3rl+ffDhPu2wWf+5/vWBz39e9zLq TLBRU2DsPkyvK3G+ECBAYDsCi/4QtB0mKyFAgEA+Aqn3X4ZOUjNgxnYuw8rDw31u/6//q8tj8zkN ipvJ0H2Y589+vXrplVf9TFJcVU2YAAEC9wv4Zu6sIECAQEECYw8c6ns9SfPy2L77K8MDfMJHX/D0 7suCTpQMpzp0H2aYrstkMyyaKREgQCBBQMBMQDOEAAECpxI49v7Lv/z+96Pefdlc3+WrSX7916vD M89c/rUH/Jyq+uUe132Y5dbOzAkQIDBVQMCcKmZ7AgQInFBg7vsvu15DUi+vef+lp8eesOgbOXQI mQ8eDr2rcR/mRgptGQQI7F5AwNz9KQCAAIGSBFLuv6wvjz2LeD1Jl8VlB/PHf9zTY0s6UTKdq/sw My2MaREgQGBGAQFzRky7IkCAwNICQwEz5v7L5vxi78UMAfPwwAMujV26uDvYv/dh7qDIlkiAwO4F BMzdnwIACBAoRSC8LuWrTz7ZO92+gFkP+Oj3f7/3AT5jT5T1epJSzpK85+lBP3nXx+wIECAwh4CA OYeifRAgQGAFgYuLi8MLzz8/OWC2X08SdjAUKJuf83qSFQq7o0OMvQ/Tk2R3dDJYKgECmxUQMDdb WgsjQGBrAikP+Knvv2y+/7LPpSt0uv9ya2fRadcjYJ7W39EJECCwhoCAuYayYxAgQGAGgZQH/ITD dnUwh7qYIVR+9L3v3e1yev/lDMWzi0uBsSfJ6mA6UQgQIFC+gIBZfg2tgACBnQikBsxLnouL6sOv fKX3HZhDl8yG+y/Dh/df7uREW3iZQ0+S9aqShfHtngABAisICJgrIDsEAQIE5hAYCpgfnZ11BsD6 Etkffve790yhL1DW3cuw8eXlsb/+69XhmWeEyzkKaB+XAkMB86WXX67Oz8/9bOJcIUCAQMECvokX XDxTJ0BgXwLHBMyzH/7w8rLXro9mqGx+PoTQz/zGbwiY+zrNFl/t0JNkz5/9evXSK6/62WTxKjgA AQIElhPwTXw5W3smQIDAbALHPEH28tLW//k/7wbMsVeS1JP2BNnZymdHDQGvKnE6ECBAYNsCAua2 62t1BAhsROCF5549XLz6Wu9qxt6B2QyY9U7aryOpO5x1R9MTZDdy8mS2DE+SzawgpkOAAIGZBQTM mUHtjgABAksIDAXMa1VVvfXQQ72Hvf3009VD/+7f9V4i2zcwBMzDAw+4/3KJgu54nwLmjotv6QQI 7EJAwNxFmS2SAIHSBVICZvMdmM2A2X6QT/PezPbnBMzSz5z85i9g5lcTMyJAgMCcAgLmnJr2RYAA gYUEHn/88cP7t2517n2og1m/A3Osg9l+0E/95/CKEq8nWaioO92tgLnTwls2AQK7ERAwd1NqCyVA oGSB1IAZ1hwukf3rb33rnndgjj3oxxNkSz5b8p173VV/8HDoneTHh4OfTfItoZkRIEBgVMA38VEi GxAgQOD0AkOvKBnrYJ69/nr14Ve+ck/ADCsau1TWOzBPX/ctzkAHc4tVtSYCBAj8SEDAdDYQIECg AIGUgFl3i9oBs++9l02G0MH8iW9+s7ryxhsukS3g/ChpiuG81MEsqWLmSoAAgWkCAuY0L1sTIEDg JAJDAfOts7PqWnjXZcdH+GG+r4PZ7mI2h3sH5knKvIuDCpi7KLNFEiCwYwEBc8fFt3QCBMoRSA2Y lyu8uOi8RLYdKJtPk728B/Ozn60uA2pPeC1Hz0xzEXAPZi6VMA8CBAgsJyBgLmdrzwQIEJhNIDVg tjuY9cN9Yi6TffBLXxIwZ6ugHdUCOpjOBQIECGxbQMDcdn2tjgCBjQikBszYDmYXk4C5kZMno2Xo YGZUDFMhQIDAQgIC5kKwdkuAAIE5BVIDZtc9mDHdyzB3AXPOCtqXDqZzgAABAvsQEDD3UWerJECg cIHUgJnawXzwO9+pDs88czncPZiFnzwZTV8HM6NimAoBAgQWEhAwF4K1WwIECMwpkBowx54i2zfH OmAKl3NW0b6CgHswnQcECBDYtoCAue36Wh0BAhsRSA2Y7Q5m9OWxn3YwBcyNnECZLEMHM5NCmAYB AgQWFBAwF8S1awIECMwlkBowUzuYH7/ySnXljTdcHjtXAe3nroAOppOBAAEC2xYQMLddX6sjQGAj AqkBs93BjOUQMGOlbDdFQAdzipZtCRAgUKaAgFlm3cyaAIGdCaQGzNQOpifI7uwEW3G5OpgrYjsU AQIETiAgYJ4A3SEJECAwVSA1YNYdzI+++MXoQ/6Pn/zJ6jOf/ezlw1jcgxnNZsMIAR3MCCSbECBA oHABAbPwApo+AQL7EEgNmHUHcyxghqfGNrd54POfv4QVMPdxfq25Sh3MNbUdiwABAusLCJjrmzsi AQIEJgukBsyUDmYYI2BOLpEBEQI6mBFINiFAgEDhAgJm4QU0fQIE9iGQGjCHOph117Lr18Mzz+hg 7uPUWn2VOpirkzsgAQIEVhUQMFfldjACBAikCaQGzJQOZgicAmZanYwaFtDBdIYQIEBg+wIC5vZr bIUECGxAIDVg9nUwm/dctjuY9StKApt7MDdw8mS2BB3MzApiOgQIEJhZQMCcGdTuCBAgsITA3AGz nmP74T7h770Dc4kK2mcQ0MF0HhAgQGD7AgLm9mtshQQIbEBgzoDZDpV9HUzdyw2cOBkuQQczw6KY EgECBGYUEDBnxLQrAgQILCUwZ8Ds6l7ec8nsl77kHZhLFXLn+9XB3PkJYPkECOxCQMDcRZktkgCB 0gXmCph93cvgc7eTKWCWfrpkPX8dzKzLY3IECBA4WkDAPJrQDggQILC8wFwBs929bF8eexk0Bczl C7rTI+hg7rTwlk2AwK4EBMxdldtiCRAoVWCugDn09NjwucuP83OXyJZ6ohQwbx3MAopkigQIEDhC QMA8As9QAgQIrCUwV8Ac62DW78D0gJ+1Kruv4+hg7qveVkuAwD4FBMx91t2qCRAoTGCugNl5Sex3 vlN99MUvXt6DGT4Ozzzj/ZeFnR8lTVcHs6RqmSsBAgSmCwiY082MIECAwOoCcwTMdrisA2UdLutf BczVy7ubA+pg7qbUFkqAwI4FBMwdF9/SCRAoR2COgFmvdihofvzKK9WVN97QwSzn1ChupjqYxZXM hAkQIDBJQMCcxGVjAgQInEZgroDZFS6bHUwB8zT13ctRdTD3UmnrJEBgzwIC5p6rb+0ECBQjMEfA HAqXAeIyaHpFSTHnRKkT1cEstXLmTYAAgTgBATPOyVYECBA4qUBKwKy7RWevv373IT7t+y3vBsv6 QT8C5knrvPWD62BuvcLWR4AAgaoSMJ0FBAgQKEAgJWCGZYUf6LsCZjNY1svXwSzgRNjAFHUwN1BE SyBAgMCAgIDp9CBAgEABAikBs6uD2Q6W93U0z88/CaVXrhSgYoqlCehgllYx8yVAgMB0AQFzupkR BAgQWF0gJWAOdTCb7728ZzEC5uq13dsBdTD3VnHrJUBgbwIC5t4qbr0ECBQpkBIwh+7BbCI0w2Z4 B2b40MEs8jTJftI6mNmXyAQJECBw9ADNyQAAIABJREFUtICAeTShHRAgQGB5gZSA2dXBDH/X96Af ryhZvo6O8Ml9wQ8eDr0UHx8OfjZxohAgQKBgAd/ECy6eqRMgsB+BlIAZ28GsFW9/8EF15Y03dC/3 c1qtvlIdzNXJHZAAAQKrCwiYq5M7IAECBKYLpATMvg5ms4tZzyR0NXUwp9fFiOkCOpjTzYwgQIBA SQICZknVMlcCBHYrkBIwYzqY9zzsxwN+dnt+rbVwHcy1pB2HAAECpxMQME9n78gECBCIFkgJmEMd zPaBvQMzuhQ2PFJAB/NIQMMJECCQuYCAmXmBTI8AAQJBICVg9r0HM+yv8zUlOphOtoUFdDAXBrZ7 AgQIZCAgYGZQBFMgQIDAmMCxAbPef+/7L8MG5+dj0/B5AkcL6GAeTWgHBAgQyFpAwMy6PCZHgACB TwSODZiDwfJT5PAOTO+/dMYtKaCDuaSufRMgQCAPAQEzjzqYBQECBAYFUgJm2GH4gf7s9dfjdDfa wQwG78QJ2GoBgaeq6p7/uNDBXADZLgkQIJCRgICZUTFMhQABAn0CKQFz6B7M9nG2+g7MOlx+7XBw cp1I4KOzs7sBUwfzREVwWAIECKwoIGCuiO1QBAgQSBVICZh1B/PO9evVQ488MnjorQbMsOi379yp BMzUM+/4cc2AWZ+TDw4E/o8PBz+bHM9uDwQIEDiZgG/iJ6N3YAIECMQLpAbMcITbTz8tYH4aaM6f /Xr1K7/6a9Uf/MEfxOPbcpLAF77wheratWvV+7duXY7TwZzEZ2MCBAgULyBgFl9CCyBAYA8CSwfM 8ATZy/s1r1zZHGezg/nujRvVE088sbk15ragL3/5y50BM8zTPZi5Vct8CBAgMK+AgDmvp70RIEBg EQEBM51VwEy3Sx3ZFzDdg5kqahwBAgTKERAwy6mVmRIgsGMBATO9+AJmul3qSB3MVDnjCBAgUL6A gFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBgyYAZniD70JtvugdzB+fR WkvUwVxL2nEIECCQn4CAmV9NzIgAAQL3CcwdMC9D5aevLqlfURIO6iE/Tr45BHQw51C0DwIECJQp IGCWWTezJkBgZwJzB8wm35bfgRnW6RLZ9b9YdDDXN3dEAgQI5CIgYOZSCfMgQIDAgMBcAbPduQxd TAHTqTe3gA7m3KL2R4AAgXIEBMxyamWmBAjsWCA1YIbXQty5fv3u5bA1Ydclslu8PFYH8zRfNDqY p3F3VAIECOQgIGDmUAVzIECAwIhAasAMu7399NP33G9Z33t595Dn55t9wI+AeZovLR3M07g7KgEC BHIQEDBzqII5ECBAYKGAGdPBrARM59/MAjqYM4PaHQECBAoSEDALKpapEiCwX4G5O5jNS2QFzP2e V0utXAdzKVn7JUCAQP4CAmb+NTJDAgQIVKkBM6aDeXjmmUth92A60eYS0MGcS9J+CBAgUJ6AgFle zcyYAIEdCqQGzEBV34NZdy3bv4aAudVwGdbvNSXrf8HoYK5v7ogECBDIRUDAzKUS5kGAAIEBgdSA 2e5g3nNp7KfHEzCdenML6GDOLWp/BAgQKEdAwCynVmZKgMCOBVID5lgHM3z+yhtv6GDu+NxaYuk6 mEuo2icBAgTKEBAwy6iTWRIgsHOB1IDZ18FsXiYrYO785Fpg+TqYC6DaJQECBAoREDALKZRpEiCw b4HUgNnsYF7+/oMPLt+JKWDu+3xaevU6mEsL2z8BAgTyFRAw862NmREgQOCuQGrAbHYw2+Hycucb fwdmWKKH/Kz/haSDub65IxIgQCAXAQEzl0qYBwECBAYE5giYXR1MAdNpt4SADuYSqvZJgACBMgQE zDLqZJYECOxcYI6A2fmakjffrEKX02tKdn6Czbx8HcyZQe2OAAECBQkImAUVy1QJENivwNwBM0iG ezF1MPd7Ti25ch3MJXXtmwABAnkLCJh518fsCBAgcCkwd8C8+6AfHUxn2AICOpgLoNolAQIEChEQ MAsplGkSILBvgbkCZt25DJfLho/wipLw4RLZfZ9fc69eB3NuUfsjQIBAOQICZjm1MlMCBHYsMFfA rDuXNeVDb765eVVPkV2/xDqY65s7IgECBHIREDBzqYR5ECBAYEBgroB5N1h++i7M0MHccvcyrFfA XP9LSwdzfXNHJECAQC4CAmYulTAPAgQIrBAw2x1MAdNpt4SADuYSqvZJgACBMgQEzDLqZJYECOxc YIkO5h6eIKuDeZovHB3M07g7KgECBHIQEDBzqII5ECBAYETg2IBZ777ZwQz3X279HZgC5mm+tHQw T+PuqAQIEMhBQMDMoQrmQIAAgQUCZgiP4ePO9euX77ysnxwb/m4v78AUME/zpaWDeRp3RyVAgEAO AgJmDlUwBwIECCwQMMMuQ8gMAbP9EQLm4ZlnNv+AHwHzNF9aOpincXdUAgQI5CAgYOZQBXMgQIDA AgGz2cGsu5bNLuYeHvAjYJ7mS0sH8zTujkqAAIEcBATMHKpgDgQIEFggYA51MC8D5w7egSlgnuZL SwfzNO6OSoAAgRwEBMwcqmAOBAgQWCBgDt2DKWA65ZYU0MFcUte+CRAgkLeAgJl3fcyOAAEClwJz PUX2Mlh++sCfcIls+Di7cmXTym/fuVN97XC4XOO7N25UTzzxxKbXm8PidDBzqII5ECBA4DQCAuZp 3B2VAAECkwRSAmb7Hsz2Ad2DOakENp4goIM5AcumBAgQ2JiAgLmxgloOAQLbFEgJmEGi9ymyO3kH ZjDQwVz/a0IHc31zRyRAgEAuAgJmLpUwDwIECAwIpATMvg7mnt6BKWCe5stKB/M07o5KgACBHAQE zByqYA4ECBAYEUgJmDqYn6DqYK7/5aWDub65IxIgQCAXAQEzl0qYBwECBBbsYNYP9qkPEV5REjqc W3/Aj4B5mi8rHczTuDsqAQIEchAQMHOogjkQIEBggQ7m0EN+BEyn3JICOphL6to3AQIE8hYQMPOu j9kRIEDgUiDlElkB85OTxyWy638R6WCub+6IBAgQyEVAwMylEuZBgACBAYGUgBl2136KbH2pbOhg 7uVDwFy/0jqY65s7IgECBHIREDBzqYR5ECBAYOaAOdTB3Ms7MHUwT/NlpYN5GndHJUCAQA4CAmYO VTAHAgQIjAjM1cGsDyNgOuWWFNDBXFLXvgkQIJC3gICZd33MjgABApcCKQGzq4O5t3dg6mCe5gtI B/M07o5KgACBHAQEzByqYA4ECBBYsYO5pyfICpin+dLSwTyNu6MSIEAgBwEBM4cqmAMBAgQWCJg6 mJ+gesjP+l9eOpjrmzsiAQIEchEQMHOphHkQIEBgQCDlEtmwu66nyB6eeebySGdXruzCXMBcv8w6 mOubOyIBAgRyERAwc6mEeRAgQGDmgNn3FNnwgB8B0+m2pIAO5pK69k2AAIG8BQTMvOtjdgQIELgU mKuDGfa1p3dghvXqYK7/RaSDub65IxIgQCAXAQEzl0qYBwECBBbsYIanx97+4IPLI+zpFSUC5mm+ rHQwT+PuqAQIEMhBQMDMoQrmQIAAgRGBOTuYAuYTzreFBXQwFwa2ewIECGQsIGBmXBxTI0CAQC2Q EjCH7sHcywN+dDBP8zWkg3kad0clQIBADgICZg5VMAcCBAjoYC52DrgHczHa3h3rYK5v7ogECBDI RUDAzKUS5kGAAIEBgWM7mPU9mOEBP6GzqYPpdFtSQAdzSV37JkCAQN4CAmbe9TE7AgQIXAocGzBr RgHzRvXEE+7BXPrLSgdzaWH7J0CAQL4CAma+tTEzAgQI3BUQMNNPBpfIptuljtTBTJUzjgABAuUL CJjl19AKCBDYgUBKwAws4XLYO9ev3xXSwdTBXOPLRQdzDWXHIECAQJ4CAmaedTErAgQI3COQEjCb T5F1D+bh0vPdGwLmGl9aOphrKDsGAQIE8hQQMPOsi1kRIEDg6IDZ18HcG61LZNevuA7m+uaOSIAA gVwEBMxcKmEeBAgQGBA4toNZ7/rKG2/s6gmyYd0C5vpfWjqY65s7IgECBHIREDBzqYR5ECBAYOaA 2dXBFDBdIrvGF5oO5hrKjkGAAIE8BQTMPOtiVgQIELhHQAcz/YTQwUy3Sx2pg5kqZxwBAgTKFxAw y6+hFRAgsAOBlIDZ7GCGh/xU5+eXT5U9u3JlB2I/WqKAuX65dTDXN3dEAgQI5CIgYOZSCfMgQIDA gEBKwGw/RVbA9BTZtb7IdDDXknYcAgQI5CcgYOZXEzMiQIDAfQIpAVMH8xNGHcz1v6B0MNc3d0QC BAjkIiBg5lIJ8yBAgMCCHcyw64fefNMlst6DucrXmQ7mKswOQoAAgSwFBMwsy2JSBAgQuFfg2A6m gHm4BH1XwFzlS0sHcxVmByFAgECWAgJmlmUxKQIECBwfMJv3YAqYAuaaX1M6mGtqOxYBAgTyEhAw 86qH2RAgQKBTYI4O5h7fgRkw3YO5/heVDub65o5IgACBXAQEzFwqYR4ECBAYEEgJmO0OpoDpEtm1 vsh0MNeSdhwCBAjkJyBg5lcTMyJAgMB9AikBM+wkhMw7169f7k/AFDDX+tLSwVxL2nEIECCQn4CA mV9NzIgAAQKzB8y9PkE2QLpEdv0vKB3M9c0dkQABArkICJi5VMI8CBAgMCBwbAdTwPSQnzW/wHQw 19R2LAIECOQlIGDmVQ+zIUCAQKeAgJl+YuhgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5k ATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4Ejg2Ye33ATzg1BMz1v0B0MNc3d0QCBAjkIiBg5lIJ 8yBAgMCAgICZfnoImOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiCQEjCb78HUwfSQnzW/THQw19R2 LAIECOQlIGDmVQ+zIUCAQKdASsAMO6rfgylgCphrfmnpYK6p7VgECBDIS0DAzKseZkOAAIFZA2a9 sxA0z65c2aWuS2TXL7sO5vrmjkiAAIFcBATMXCphHgQIEBgQSO1gCpge8nOKLywdzFOoOyYBAgTy EBAw86iDWRAgQGBQ4NiAuWdeHcz1q6+Dub65IxIgQCAXAQEzl0qYBwECBBbsYO4ZV8Bcv/o6mOub OyIBAgRyERAwc6mEeRAgQEDAXOQcEDAXYR3cqQ7m+uaOSIAAgVwEBMxcKmEeBAgQEDAXOQcEzEVY kwJmGBQeOPXg4ZOn+nZ9fHw4+Nlk/ZI5IgECBGYT8E18Nko7IkCAwHIC7sFMtxUw0+1SR+pgpsoZ R4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzrSPZipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptul jtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0 IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAg sAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM 8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEg QKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQO ZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nA TLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQvoCAWX4NrYAAgR0I CJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNVzjgCBAiULyBgll9D KyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbbpY7UwUyVM44AAQLl CwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM9CILmOl2qSN1MFPl jCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQILADAQEzvcgCZrpd 6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUEzPJraAUECOxAQMBM L7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYRIECgfAEBs/waWgEB AjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DA LL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwB AgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfq YKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEF zHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiB gICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1 tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQ voCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNV zjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbb pY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM 9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQ ILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUE zPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYR IECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWk DmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZ wEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEd CAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZf QysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC 5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT 5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6 XepIHcxUOeMIECBQvoCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDA TC+ygJlulzpSBzNVzjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloB AQI7EBAw04ssYKbbpY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9A wCy/hlZAgMAOBATM9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmcc AQIEyhcQMMuvoRUQILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH 6mCmyhlHgACB8gUEzPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqR Bcx0u9SROpipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDY gYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5 NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAg UL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6Ugcz Vc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm 26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQE zPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EV ECCwAwEBM73IzYAZ9vLoY49d7uz9W7cuf+/XeR1q27piH52dVWdXrlz+8XDnzuWvDx4OvQX9+HDw s0n66W4kAQIETi7gm/jJS2ACBAgQGBcQMMeN+rZoB8z0PRmZItAMmHXIFDBTJI0hQIBAGQICZhl1 MksCBHYuIGCmnwACZrrdHCN1MOdQtA8CBAiUIyBgllMrMyVAYMcCAuaOi5/J0sPlrXeuX68eeuSR yxnd/uCDy9/Xv9Z/d+WNN+5eEts19bAfHcxMimoaBAgQWEBAwFwA1S4JECAwt4CAObeo/U0RqO+d DAFz7GMoYLoHc0zP5wkQIFC+gIBZfg2tgACBHQgImDsocsZLnCtghiXqYGZcaFMjQIDADAIC5gyI dkGAAIGlBQTMpYXtf0gghMLwJNjbTz89CqWDOUpkAwIECGxaQMDcdHktjgCBrQgImFupZJnr6AqY 9f2X7RW5B7PMGps1AQIE5hIQMOeStB8CBAgsKCBgLohr19ECOpjRVDYkQIDAbgUEzN2W3sIJEChJ QMAsqVrbm2vMJbKXT5c9Px9dvHswR4lsQIAAgaIFBMyiy2fyBAjsRUDA3Eul815n6GA2L43tukz2 oTff7F2Ep8jmXV+zI0CAwBwCAuYcivZBgACBhQUEzIWB7X5UIOby2LAT92COUtqAAAECmxYQMDdd XosjQGArAgLmVipZ3jqmvKJkLGDqYJZXfzMmQIDAVAEBc6qY7QkQIHACAQHzBOgOeSnQFzA9RdYJ QoAAAQJdAgKm84IAAQIFCAiYBRRpw1MMIfPO9etRK/QezCgmGxEgQGCzAgLmZktrYQQIbElAwNxS NctaS7uD2flgn0ceqW5/8MHlwtyDWVZ9zZYAAQJzCwiYc4vaHwECBBYQEDAXQLXLSQJzPOTHPZiT yG1MgACBIgUEzCLLZtIECOxNQMDcW8XzWW/MOzCbs9XBzKd2ZkKAAIFTCAiYp1B3TAIECEwUEDAn gtl8doGuDmbX5bLuwZyd3g4JECBQlICAWVS5TJYAgb0KCJh7rXw+664DZt/TY+uZ6mDmUzMzIUCA wCkEBMxTqDsmAQIEJgoImBPBbD67gHswZye1QwIECGxSQMDcZFktigCBrQkImFuraFnriQ2XYVU6 mGXV1mwJECAwt4CAObeo/REgQGABAQFzAVS7HBVovqKk79LY9t+7B3OU1QYECBDYtICAuenyWhwB AlsREDC3Usny1hFC5p3r1++Z+NB9mDqY5dXYjAkQIDCngIA5p6Z9ESBAYCEBAXMhWLsdFGh2MGOp dDBjpWxHgACBbQoImNusq1URILAxAQFzYwUtZDlzB8yw7LDPBw+HXoGPDwc/mxRyfpgmAQIEugR8 E3deECBAoAABAbOAIm1wiiEMnl25UsW+oiQQ6GBu8ESwJAIECEwQEDAnYNmUAAECpxIQME8lv+/j tgNmU6PvPkz3YO77nLF6AgQICJjOAQIECBQgIGAWUKQNT3Gu15TUl9y6RHbDJ4ulESCwewEBc/en AAACBEoQEDBLqNJ25zhXwAxC7sHc7nliZQQIEAgCAqbzgAABAgUICJgFFGmDU2y/oqR9WWzXZbLu wdzgiWBJBAgQmCAgYE7AsikBAgROJSBgnkp+38ftegfmmIh7MMeEfJ4AAQLbFhAwt11fqyNAYCMC AuZGClnQMlJeURKWp4NZUJFNlQABAgsICJgLoNolAQIE5hYQMOcWtb8xgaGA2fcE2bGAGT7vHswx eZ8nQIBA2QICZtn1M3sCBHYiIGDupNAZLjM84GcoULanrIOZYRFNiQABAisKCJgrYjsUAQIEUgUE zFQ541IFht6BObRP92CmihtHgACBbQgImNuoo1UQILBxAQFz4wXOeHlDryjxFNmMC2dqBAgQOJGA gHkieIclQIDAFAEBc4qWbecUaAbMmEtldTDn1LcvAgQIlCcgYJZXMzMmQGCHAgLmDoueyZKHOphd U3QPZiaFMw0CBAicSEDAPBG8wxIgQGCKgIA5Rcu2cwlMDZfhuDqYc+nbDwECBMoUEDDLrJtZEyCw MwEBc2cFP/Fym68oibkstjl2c3t8AAAeAUlEQVRdHcwTF8/hCRAgcGIBAfPEBXB4AgQIxAgImDFK tplLYOwdmO/fuFE9+uST1e0PPrjnkN/5y7+s/u5v/mZ1duVK71S8B3OuKtkPAQIE8hQQMPOsi1kR IEDgHgEB0wmxtkAIgneuX598WB3MyWQGECBAYFMCAuamymkxBAhsVUDA3Gpl813XEgEzrFYHM9+a mxkBAgTmEBAw51C0DwIECCwsIGAuDGz39wjUl8ievf765WWwU+7D1MF0MhEgQGDfAgLmvutv9QQI FCIgYBZSqI1MMwTMcB/l1KfIugdzIyeAZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOM0BaYGzDBWB9M5 RIAAgX0LCJj7rr/VEyBQiICAWUihNjbNuQNm4HEP5sZOEsshQIBAS0DAdEoQIECgAAEBs4AibWSK x7wDUwdzIyeBZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOPUXcaUV5S4B9P5Q4AAAQICpnOAAAECBQgI mAUUaSNTbHYwU5bkHswUNWMIECCwHQEBczu1tBICBDYsIGBuuLiZLe2YgKmDmVkxTYcAAQInEBAw T4DukAQIEJgqIGBOFbN9qkDqK0rq4+lgpsobR4AAgW0ICJjbqKNVECCwcQEBc+MFzmh5XQHzoUce qW5/8MHoLHUwR4lsQIAAgc0LCJibL7EFEiCwBQEBcwtVLGsNzVeUhOD4xZ/6qd4FND+vg1lWnc2W AAECcwsImHOL2h8BAgQWEBAwF0C1y04BHUwnBgECBAgcIyBgHqNnLAECBFYSEDBXgnaYuwLNDmaT JXQrH33yyer9GzfudjV1MJ04BAgQIFALCJjOBQIECBQgIGAWUKQNTbEvXHYtsRku3YO5oZPAUggQ IJAoIGAmwhlGgACBNQWGAua1qqreeuihNafjWBsVmPqKknY386E336zqS2y7iOr9P3g49Ap+fDj4 2WSj55dlESCwDwHfxPdRZ6skQKBwgccff/zw/q1bnasQMAsvbkbTHwuYdbey3bWsHwAU08F8+86d 6msCZkZVNxUCBAjMKyBgzutpbwQIEFhEQMBchNVOOwRCyLxz/fqgTXhtSdc9mGMdzLBTAdNpR4AA gW0LCJjbrq/VESCwEQEBcyOFLGAZXQGzq2PZ7maGP4ePv/ubv1mdXbnSu1IBs4CTwBQJECBwhICA eQSeoQQIEFhLQMBcS9pxgkD9kJ//63Ofq/63733vPpT2ezHrP+tgOn8IECBAQMB0DhAgQKAAgRee e/Zw8eprvTP9oYf8FFDF/KfY9Q7MMOu+ey/D58L9lwJm/rU1QwIECKwlIGCuJe04BAgQOEJAwDwC z9DJAmOvKem7ZPbR3/mdwafIhol87fbt6u2BGXmK7ORyGUCAAIGsBATMrMphMgQIEOgWuLi4OLzw /PO9PDqYzpy5BJrhsus+y2bHsvn5cPwQMMc+BMwxIZ8nQIBA2QICZtn1M3sCBHYkMPQuzI/OzgYf rLIjJktNFBh7RUnYbd+DferQGRMwH7h9u3eG589+vXrplVf9bJJYQ8MIECCQg4Bv4jlUwRwIECAQ ISBgRiDZJFmgef/llM5lHS7DgY8NmO/euFFdvXrVzybJVTSQAAECpxfwTfz0NTADAgQIRAkMBcy3 zs6qawOvhog6gI12LRD7gJ++LmbM/ZcBeKiD6f7LXZ+CFk+AwEYEBMyNFNIyCBDYvsBQwLxWVdVb niS7/ZNgwRUOdTDDYccujw3bjL0DMxzjwcOhdxUC5oIFtmsCBAisJCBgrgTtMAQIEDhWYOhJsgLm sbrG1wLtJ8jGBMswNqaD+fadO9XXBEwnGwECBDYtIGBuurwWR4DAlgQ8SXZL1cxzLSFcDt1/GWbd vOey+eeYgOkJsnnW3awIECAwp4CAOaemfREgQGBBgffee+/w1Sef7D2CV5UsiL/xXXfdfzk1aMY8 4GcoYD762GPVzZs3/Vyy8XPN8ggQ2L6Ab+Tbr7EVEiCwIQFPkt1QMTNaytgTZNudy3YXM3x+7P7L sI1XlGRUdFMhQIDAQgIC5kKwdkuAAIElBDxJdglV+wwB87/80i/dvfy1DpB9v7bFYi6PHXvAj1eU OA8JECCwDQEBcxt1tAoCBHYi4EmyOyn0issMwS983Ll+vfdJse3ptDuYMQHTA35WLKpDESBA4IQC AuYJ8R2aAAECUwUef/zxw/u3bvUOcx/mVFHbt++/HLr3suvS2CB47P2XYR9eUeJcJECAwDYEBMxt 1NEqCBDYkYD7MHdU7BWW2nf/ZX3ovlBZfz4mXIZth+6/9ICfFQrtEAQIEFhJQMBcCdphCBAgMJeA +zDnkrSfsctjx8Jl3b2sQ2qfqPsvnWsECBDYj4CAuZ9aWykBAhsRcB/mRgqZwTKGupfNcDkUNGM6 mO6/zKDYpkCAAIGVBATMlaAdhgABAnMJvPDcs4eLV1/r3Z37MOeS3vZ+6u5leHps+Gg/MTZ29TGv Jxl6/2U4jvsvY7VtR4AAgfwFBMz8a2SGBAgQuEfgvffeO3z1ySd7Vd46O6uuXblCjcCoQAiZ9dNj mxvHXBobto95emzYzv2Xo6WwAQECBDYjIGBuppQWQoDAngTch7mnai+71vf//t8f7F4Ohc2Y7qX7 L5etn70TIEAgNwEBM7eKmA8BAgQiBIYCZhjuMtkIxJ1vEoJfuDy2HSDn7l66PHbnJ5rlEyCwOwEB c3clt2ACBLYgMHYf5kdnZ9WZy2S3UOpF1xC6l/VHbLCst5/j8tiwL/dfLlpiOydAgMDqAgLm6uQO SIAAgXkEPE12Hsc97qXdvUzpYsZcHjv29NjzZ79evfTKq34W2eNJaM0ECGxWwDf1zZbWwggQ2LqA y2S3XuFl1le/mqTZvZx6pBAuw8dYl9zlsVNlbU+AAIHyBQTM8mtoBQQI7FTg4uLi8MLzz/eu3tNk d3pijCx7rHsZoxbTvQz7GXp6bPi8y2NjtG1DgACBsgQEzLLqZbYECBC4R8Blsk6IFIGu7mXsPZix 3cuxy2PfvXGjunr1qp9DUgpoDAECBDIW8I094+KYGgECBMYEHn/88cP7t271buZpsmOC+/p83b0M q44NlLXQ93/zN6uf/qVfqmK7ly6P3de5ZbUECBCoBQRM5wIBAgQKF9DFLLyAK0//mHsvY58cO9a9 fPSxx6qbN2/6GWTl2jscAQIE1hDwzX0NZccgQIDAggIe9rMg7kZ2HTqX4SO893JK97LuWtYMc3Uv X3r55er8/NzPIBs5vyyDAAECTQHf3J0PBAgQKFxg7DJZD/spvMAzTv+Y7mXsvZchzD54OAzO2sN9 ZiyqXREgQCAzAQEzs4KYDgECBFIEdDFT1PYxJvW1JM3uZfj9tStXosDG7r3UvYxitBEBAgSKFRAw iy2diRMgQOBHAi889+zh4tXXekl0Mfd5trQvjQ0KYw/3aV8WG8bE3nupe7nP88yqCRAg0BQQMJ0P BAgQ2IiALuZGCjnjMlK7l2EKddAMvz5VVdVZRAdT93LG4tkVAQIEChUQMAstnGkTIECgLeBeTOdE U+CYcNncT2z3Mox54PbtwSK499I5SoAAge0LCJjbr7EVEiCwIwFdzB0Ve2CpXZfGjsk0O5bhfZfh I/bBPmFb3csxYZ8nQIDAPgQEzH3U2SoJENiJgC7mTgoducyYp8Z23XMZdj/l0lj3XkYWxGYECBDY gYCAuYMiWyIBAvsSGOtifnR2FnU/3b7UtrPa1Etj2x3M2HdeBrmxS2PPn/169dIrr/qZYzunmZUQ IECgV8A3eycHAQIENiYw1sW8VlXVWw89tLFVW04QmBou+7qXUy6NffvOnepr3nvpBCRAgACBTwUE TKcCAQIENigw1sX02pLtFT3lvstaodm9DH8X+9RY3cvtnUdWRIAAgWMFBMxjBY0nQIBAhgJj78UM U/6hLmaGlUubUkq47HqoTzh6eGps7MfYg33Cfjw5NlbTdgQIENiGgIC5jTpaBQECBO4TGOtiulR2 GydNHS7DeypTH+rz7z/7t6u/89q/uexcho+Yd17GPNjn3Rs3qqtXr/pZYxunmlUQIEAgSsA3/Sgm GxEgQKA8gffee+/w1SefHJy4S2XLq2vfjGPCZXNsu4M55X2XYT9jD/Z59LHHqps3b/o5YzunmJUQ IEAgSsA3/igmGxEgQKBMgbEuZliVS2XLrG2YdexDfZoP8+l6sM+Uh/qE43qwT7nnjJkTIEBgaQEB c2lh+ydAgMCJBcZCpktlT1yghMPXwTIMndq5DGNCyPyvX/+Xky+LjQ2XL738cnV+fu5njITaGkKA AIHSBXzzL72C5k+AAIERgYuLi8MLzz8/uJVLZcs5jVLvuezqXIa/m/LE2KA0dmls2MaDfco5n8yU AAECcwsImHOL2h8BAgQyFBjrYoYpf3R2FvVwlwyXt8sppXQuA1QdNKdeFhsbLj3YZ5eno0UTIEDg roCA6WQgQIDATgRiQqb7MfM9GaZ0LtsP8On689TOZcwrSVwam+/5Y2YECBBYS0DAXEvacQgQIHBi gZhLZd2PeeIi9Rw+5T2XXbsKryP517/+7+4+HCh2tTEP9Qn7cmlsrKjtCBAgsF0BAXO7tbUyAgQI 3Cfw+OOPH96/dWtQxv2YeZ04U8Nl1xNjj7ksNuZ9l8JlXueM2RAgQOCUAgLmKfUdmwABAicQiLlU Vsg8QWFah5xySWzXbOtQGbqW//TP/1uVcs9lbLh03+XpzxczIECAQC4CAmYulTAPAgQIrCggZK6I nXCoqeFy6D2X9ZNiwzTOrlyZNJuYJ8a673ISqY0JECCweQEBc/MltkACBAjcL/Dee+8dvvrkk6M0 niw7SjTrBnWwDDv9L7/0S5P23X4NSf3nR3/ndybfcxkOHPNQn7Cd+y4nlcnGBAgQ2LyAgLn5Elsg AQIEugVi7scMI4XMdc6gqV3LvlmFYPlfv/4vq7/z2r+5fMdl+FiicylcrnNeOAoBAgRKExAwS6uY +RIgQGBGgZhLZcPhvL5kRvSOXYVwWYfAqe+3bL+CJOw+tWsZxupcLltreydAgMDWBQTMrVfY+ggQ IDAiEBsydTLnP5VSu5bty2HDzMLDfELX8tqVK0mXxAqX89fXHgkQILBHAQFzj1W3ZgIECLQEhMx1 T4nUYNk1y/a9lmGbqZfECpfr1t/RCBAgsGUBAXPL1bU2AgQITBCIDZleYTIBtbVp8yE+IQTWl8N+ 8ad+qvrOX/5l1I67upcpryBpHiz2stjzZ79evfTKq352iKqUjQgQILBPAf9I7LPuVk2AAIH7BGKf LBsGCpnTTqBmxzKMnHqfZdfRmq8fCZ9P6VrGvucy7F+4nFZzWxMgQGCvAgLmXitv3QQIEOgQEDLn PS3GOpaxncuuV5CEey3DR/MBQVNmPyVcPvrYY9XNmzf9zDAF2LYECBDYqYB/LHZaeMsmQIBAn8CU kHktdDMfeghmQ6AvVIZNYgNlH2iza5nSsawDafj1wcMhqm7CZRSTjQgQIEDgUwEB06lAgAABAvcJ XFxcHF54/vkoGSHzky5i86O+v7IOlO1gOTVohmB5zNNhm3N7+86d6mvCZdS5bSMCBAgQmC4gYE43 M4IAAQK7EYh98E8A2dtrTNqdymAQ7q2cK1SG/dUdy9RuZftEjX2YTxjnnsvdfJlbKAECBGYVEDBn 5bQzAgQIbE9gSsjc8sN/2l3KUOm+TmWzQzm1Wxn2G54KO1eoDPubcr9l2P6ll1+uzs/P/YywvS9n KyJAgMDiAv7xWJzYAQgQIFC+wJSQWfols11Bsg6TdSVvP/30PUUNrxjp6lxODZf160bqnc8RMqdc EhuO++6NG9XVq1f9fFD+l60VECBA4CQC/gE5CbuDEiBAoDyBxx9//PD+rVvREz9lN7MvJMZMvh3q 2mEy7KN+Z2Xs5bBDQfPR3/mde+7hnCNU1uuccklsGPPx4eDngpiTxDYECBAg0CvgHxInBwECBAhE C0wNmafuZoZw+NCbb/aur/58V4hsD2p3Keug2Q6ZMV3LECrDR/P9mO13ZUYXpWPDqV1L4fIYbWMJ ECBAoCkgYDofCBAgQGCSwJTXmNQ7PmU3sx0eH3rkkctp3f7gg8tfw5/r33dB1MGyHSjDn4fCZPNz 7UAZxoZOZeo7LPsKlhIsPcxn0ulvYwIECBAYERAwnSIECBAgkCQw5b7M+gBrP2l2qCvY17XsCpzN kFmvpeu+yzpIhm26Xl0yd6BsFm7q5bBhrHCZdOobRIAAAQIDAgKm04MAAQIEkgVeeO7Zw8Wrr00a Hy6b/fbZ2axPSR2bQFew6wqAdTCceh/knJe3jq2l/fmUrmXYh/stp0rbngABAgRiBATMGCXbECBA gECvwMXFxeGF55+fLHTKy2brIFlPug6UqUFxyc5kH2wIljcOh+rtifKPPvZYdfPmTf/+T3SzOQEC BAjECfgHJs7JVgQIECAwIpByyWzYZQiaT316TyLkcYHUjmXYs1eQjPvaggABAgSOExAwj/MzmgAB AgQaAimXzNbDT3HpbEnFOyZY6lqWVGlzJUCAQNkCAmbZ9TN7AgQIZCkw9XUmzUUImj/SCJfevlNV 1dcOh+Q6n+pey9SOdvJCDSxa4FTnadFoJk8gUwEBM9PCmBYBAgRKF0h5nUl7zXu9fDb1/sqm36mf EBsCptBQ+lfxOvN3rqzj7CgE1hIQMNeSdhwCBAjsVCD1IUDtruaTZ2fVtStXNqsYupX/LOGhPV0g OQQ7oWGzp+rsC3OuzE5qhwROKiBgnpTfwQkQILAfgWMum+0Km6U/GKi+/DXlSbB9Z01OD/ERGvbz tX3sSp0rxwoaTyAvAQEzr3qYDQECBDYvMFfQrKHCPZuldDeXCJXBIadgWddFaNj8l/JsC3SuzEZp RwSyEBAwsyiDSRAgQGB/AnMHzXbgDB3O8FG/43Jt4TpMhuPO2aWs15H7k2GFhrXPuHKP51wpt3Zm TqBLQMB0XhAgQIDASQWOebVJ7MRDlzN8hE7n3JfWLh0k22vMPVjW8xUaYs9O2zlXnAMEtiUgYG6r nlZDgACBYgXmeBjQsYuvg2jfft4+9gBHjD/1U2GnTl1omCq23+2dK/utvZVvU0DA3GZdrYoAAQJF C6zR1SwBKHQrv/GNb1RXr14t7t9roaGEMyyPOTpX8qiDWRCYS6C4f7DmWrj9ECBAgEAZAkvdq5nz 6l96+eXq/Pzcv9E5F8ncZhMQMGejtCMCWQj4xyuLMpgEAQIECIwJvPfee4cXX3yxev/WrbFNi/t8 6FQ+9dRTQmUBlQvd9ZdeefVkPz+d6vhLHlfALODEN0UCEwRO9g1ywhxtSoAAAQIE7hMo/TLa0u6p nHoKLhlIps7F9nkLCJh518fsCEwVEDCnitmeAAECBLIUCA8Jeuedd7LtcG49UGZ5UrQmNUfojdlH zDZ9XseMLaEGXXMUMEutnHkT6BYQMJ0ZBAgQILBZgXBZ7e/+9m9Vf/hHf7xK8AyXuv7iL/x89TM/ +3PVww8/XOTDeeY6GfYYlGLtcrSZY06p+xAwY88c2xEoQ0DALKNOZkmAAAECCwqEIBp2X4fR9qFC aPx7/+AfXv51iU90XZBud7tODVG7g5qwYAFzApZNCRQgIGAWUCRTJECAAAECBAhsVUDA3GplrWuv AgLmXitv3QQIECBAYEGB8HqZ0PltfoSnr4YOYPuwQ39/7BRjO45hvjdv3hz9uSh2f8fOuz1+juPW 9sc+BXeOuTTXJ2DOfbbYH4HTCox+Iz3t9BydAAECBAgQKFHg2NBw7PgpZiFchu1jAuaU/ea+bWpQ TB3XF5rXrHXuNTE/AlsQEDC3UEVrIECAAAECmQmUEhpCWAoPgYoNmHOFq6nlOtVxu+Y511zq/ZRy rkytme0J7FVAwNxr5a2bAAECBAgsKLBWaDgm7DTDZWzAXJBscNfHrHNszlP3PXX7seOvda6MzcPn CRCYR0DAnMfRXggQIECAAIGGwClDQ2wAqi+Nracdc4ls7L7nPhlOddyudcw1Fx3Muc8S+yOQh4CA mUcdzIIAAQIECGxKIDVgTgkvXdvGjm+Hy4AfEzBPVaTYda0xv7nmImCuUS3HILC+gIC5vrkjEiBA gACBzQukBsw1YNqXxm6hgxm8H33ssUG+uQK0gLnGWeoYBMoVEDDLrZ2ZEyBAgACBbAVSAuZcwWUI pS9chleqHPv6jiWLMWbT1ZFtzyc2YI7VbmwuUx3Gjjd1f7YnQOC0AgLmaf0dnQABAgQIbFJg7dAQ G3qGglhMAIs9ztxFHTvunAFzbO5jcxkbX3/eJbKxUrYjUJaAgFlWvcyWAAECBAgUIbBmwIwNPGMh LCZgngp/bI1jawvznmt9Y3OJNRIwY6VsR6AsAQGzrHqZLQECBAgQKEJgzYAZAzJXAJsrXMXMubnN 2HHnWl/MvMbmErOPsI2AGStlOwJlCQiYZdXLbAkQIECAQBECuQXMItAGJjkW6up7S8O9pF0fc95f OjaXqdbOlalitieQt4CAmXd9zI4AAQIECBQpEBsajgkrx4xdAzXMb+px+oLg0mudsv8p2w6tXwdz 6tlhewJlCAiYZdTJLAkQIECAQFECsQHzmEXNFXSOmcNYgPqnf/7fJu3+sf/0nzp/NstprXPNRcCc dGrYmEAxAgJmMaUyUQIECBAgUI5ATMCcElS6tp0y/lRyt/7xPz78+8/+7SoEzfBr/fEzP/tz1Z/9 6Z9U7V9P1cGc4jOXu4A5Rd22BMoREDDLqZWZEiBAgACBYgRiAuaxi5kr6Bw7j77xYX51B7MOmUPH CtusGTBT/VLH9a19jXNlqRrbLwEC9wsImM4KAgQIECBAYHaBNULD3EFnboQ6YHZ1MMOxml3N+vdr Bczw1Nn3b92qPj4cJv8sOJe7DubcZ5z9EchDYPI3lTymbRYECBAgQIBAzgIC5ifVCZfIhl/bHcz/ 9x/9o+p/+c//uWr/uqd7MOvzd41zJeevFXMjsDUBAXNrFbUeAgQIECCQgcBaoaF+Umvo/B37+5hX edRdt/avXeRdHcy6U9n+tZ7/Wh3MNU6R/7+9O7xpGwzCAMwETNBFGIFR2g0YgKIu0I7CAPnBIigD MECDHNWVFTmJP5Jz7LvnF61wLr7nrlJfvpCcO+l0gjnHFDwHgfkFBMz5zT0jAQIECBBIL3AsYJ4L HUOYlmuXCjp2gjl8yezwpbGn+p3bYs7nm+uHEUvdEfdFIJuAgJltovohQIAAAQILEMgaGlqC19gJ 5sPvP3dvP77vf//y6ePj7uX+fv91s9nsT2BvcYLZ0lPEamXdlQgrNQmsQUDAXMOU3CMBAgQIEFiZ wFhoaAkyLdcumaY7wXx/ff0fKrt7nfsEc4rllGuinAXMKFl1CdxGQMC8jbtnJUCAAAECqQWyhoaW IHbsBPPb4+M+ZPanmf3XbiGiTjBb7nu4mF99XMtyZ92VFgPXEsgkIGBmmqZeCBAgQIDAQgTWFhqi glT/O5j9WA5PL4cnmt2fb/kuslEG51Zybbtyrh/fJ1BdQMCsvgH6J0CAAAECAQLXeJOfgNu6qOR2 u939+vl89JTxsHh/gnkYIod/P/yMzKgTzFONTw2WU68bczj1Dr0C5kVr6cEEFicgYC5uJG6IAAEC BAisX2BqaPhqaIkSWtr9RPXZ1b201/5jYcbuccpHvvSPm7orkRZqEyBwPQEB83qWKhEgQIAAAQL/ BFpCQ8u1gPMJmH++meqotoCAWXv+uidAgAABAiECQkMIa8qidiXlWDVVWEDALDx8rRMgQIAAgSgB oSFKNl9du5JvpjqqLSBg1p6/7gkQIECAQIiA0BDCmrKoXUk5Vk0VFhAwCw9f6wQIECBAIEpAaIiS zVfXruSbqY5qCwiYteevewIECBAgECIgNISwpixqV1KOVVOFBQTMwsPXOgECBAgQiBIQGqJk89W1 K/lmqqPaAgJm7fnrngABAgQIhAh0oSGksKIpBf7udv5PmnKymqoo4B9zxanrmQABAgQIECBAgAAB AgECAmYAqpIECBAgQIAAAQIECBCoKCBgVpy6ngkQIECAAAECBAgQIBAgIGAGoCpJgAABAgQIECBA gACBigICZsWp65kAAQIECBAgQIAAAQIBAgJmAKqSBAgQIECAAAECBAgQqCggYFacup4JECBAgAAB AgQIECAQICBgBqAqSYAAAQIECBAgQIAAgYoCAmbFqeuZAAECBAgQIECAAAECAQICZgCqkgQIECBA gAABAgQIEKgo8AnOCS0ENzfoqAAAAABJRU5ErkJggg=="
              preserveAspectRatio="none"
              height={169.33333}
              width={243.41667}
              x={933.47174}
              y={394.28033}
            />
            <rect
              ry={0.045448378}
              y={433.12564}
              x={-229.30182}
              height={15.486614}
              width={36.968048}
              id="rect2381"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.288906}
            />
          </g>
          <path
            id="path3859"
            d="M204.09 10.816v12.108h150.843l6.376-5.722V5.294H210.813z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.5}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M204.184 15.346l-.094-4.53 6.723-5.522h3.586"
            id="path3883"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.0708}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path3885"
            d="M361.032 12.871l.094 4.53-6.722 5.523h-3.586"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.0708}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={112.2066}
            y={16.393799}
            id="main"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3891"
              x={112.2066}
              y={16.393799}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'MAIN'}
            </tspan>
          </text>
          <text
            id="dc"
            y={16.458912}
            x={134.5816}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.458912}
              x={134.5816}
              id="tspan3895"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={principal_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DC'}
              </a>
            </tspan>
          </text>
          <text
            id="grupal"
            y={16.393799}
            x={207.45554}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.393799}
              x={207.45554}
              id="tspan3899"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'GRUPAL'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={232.85519}
            y={16.393799}
            id="ats"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3903"
              x={232.85519}
              y={16.393799}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={ats_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'ATS'}
              </a>
            </tspan>
          </text>
          <text
            id="ups"
            y={16.393799}
            x={246.08418}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.393799}
              x={246.08418}
              id="tspan3907"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={ups_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'UPS'}
              </a>
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={259.84244}
            y={16.393799}
            id="pdu"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3911"
              x={259.84244}
              y={16.393799}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={pdu_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'PDU'}
              </a>
            </tspan>
          </text>
          <text
            id="gen"
            y={16.393799}
            x={273.60098}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.393799}
              x={273.60098}
              id="tspan3915"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={gen_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'GEN'}
              </a>
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={287.35959}
            y={16.393799}
            id="uma"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3919"
              x={287.35959}
              y={16.393799}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={uma_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'UMA'}
              </a>
            </tspan>
          </text>
          <text
            id="chiller"
            y={16.393799}
            x={301.64725}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.393799}
              x={301.64725}
              id="tspan3923"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={chill_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'CHILLER'}
              </a>
            </tspan>
          </text>
          <text
            id="variador"
            y={16.393799}
            x={327.31628}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.393799}
              x={327.31628}
              id="tspan3923-2"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={var_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'VARIADOR'}
              </a>
            </tspan>
          </text>
          <path
            id="path14613"
            d="M107.263 10.826v12.072h77.8l6.42-5.705V5.32H114.03z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.50093}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M107.332 15.346l-.08-4.53 5.697-5.522h3.04"
            id="path14615"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.985775}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path14617"
            d="M191.203 12.875l.095 4.517-6.768 5.506h-3.61"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.0728}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={149.82877}
            y={16.225285}
            id="text1069"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="red"
            fillOpacity={0}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1067"
              x={149.82877}
              y={16.225285}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="red"
              fillOpacity={0}
              strokeWidth={0.264583}
            >
              <a href={elec_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'EL'}
              </a>
            </tspan>
          </text>
          <text
            id="text1073"
            y={16.225285}
            x={163.58672}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="red"
            fillOpacity={0}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={16.225285}
              x={163.58672}
              id="tspan1071"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="red"
              fillOpacity={0}
              strokeWidth={0.264583}
            >
              <a href={aacc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'AC'}
              </a>
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={177.87399}
            y={16.225285}
            id="text1077"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="red"
            fillOpacity={0}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1075"
              x={177.87399}
              y={16.225285}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="red"
              fillOpacity={0}
              strokeWidth={0.264583}
            >
              <a href={inc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'IN'}
              </a>
            </tspan>
          </text>
          <path
            id="path829-0"
            d="M541.985 32.9l-9.268-6.352-518.71-.044-3.21 1.897v.882l3.136 1.676c-.049 2.807.027 3.73.027 6.127l3.474 3.242v1.676l-1.896 1.191v1.941h506.97l2.26-1.147 3.938 2.427h8.242l4.818-2.507z"
            opacity={0.75}
            fill="none"
            fillOpacity={1}
            stroke="#04e6f4"
            strokeWidth={0.4}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path831-8"
            d="M542.118 33.177l.05-1.95-6.81-4.844-3.2-.038z"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".29085px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path833-1"
            d="M14.076 30.86l-.116 6.226.794-.42v-5.5z"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".121051px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            transform="matrix(1.26642 0 0 1.26654 -227.057 -83.96)"
            id="g6103"
            display="inline"
            strokeWidth={1.39987}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M260.444 88.374l-12.62 12.458"
              id="path852-9"
              display="inline"
              strokeWidth={0.864463}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M255.17 89.828l-6.212 5.99" id="path852-0-1" display="inline" strokeWidth=".404818px" />
            <path d="M257.861 94.466l-4.455 4.302" id="path852-0-7-2" display="inline" strokeWidth=".404818px" />
          </g>
          <path
            id="rect2077-1"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M19.093596 40.844654H24.374095V43.121956499999996H19.093596z"
          />
          <path
            id="rect2077-7"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M19.093596 37.760723H24.374095V40.038025499999996H19.093596z"
          />
          <path
            id="rect2077-1-2"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M19.093596 34.6768H24.374095V36.9541025H19.093596z"
          />
          <path
            id="rect2077-9"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M19.093596 31.592876H24.374095V33.8701785H19.093596z"
          />
          <path
            id="rect2077-1-5"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M19.093596 28.508953H24.374095V30.786255500000003H19.093596z"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={27.539854}
            y={38.61882}
            id="text1049"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1047"
              x={27.539854}
              y={38.61882}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ESTADOS EQUIPOS'}
            </tspan>
          </text>
          <circle
            id="st_chill1"
            cx={145.3047}
            cy={36.595806}
            r={3.75}
            display="inline"
            opacity={1}
            fill="#1aea78"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            id="ellipse2176"
            cx={39.309525}
            cy={175.58884}
            rx={4.2296953}
            ry={4.0336714}
            transform="matrix(.50256 0 0 .44492 125.64 -43.264)"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            id="text1066"
            y={38.61882}
            x={107.97327}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={38.61882}
              x={107.97327}
              id="tspan1064"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'OPERATIVO'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={163.53526}
            y={38.61882}
            id="text1070"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1068"
              x={163.53526}
              y={38.61882}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'INHABILITADO'}
            </tspan>
          </text>
          <circle
            r={3.75}
            cy={36.595806}
            cx={208.80385}
            id="circle1072"
            display="inline"
            opacity={1}
            fill="#f95"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            transform="matrix(.50256 0 0 .44492 189.14 -43.264)"
            ry={4.0336714}
            rx={4.2296953}
            cy={175.58884}
            cx={39.309525}
            id="ellipse1074"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            id="text1078"
            y={38.61882}
            x={224.91777}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={38.61882}
              x={224.91777}
              id="tspan1076"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'MANTENIMIENTO'}
            </tspan>
          </text>
          <circle
            id="circle1080"
            cx={278.12439}
            cy={36.595806}
            r={3.75}
            display="inline"
            opacity={1}
            fill="#95f"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            id="ellipse1082"
            cx={39.309525}
            cy={175.58884}
            rx={4.2296953}
            ry={4.0336714}
            transform="matrix(.50256 0 0 .44492 258.461 -43.264)"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={294.23877}
            y={38.61882}
            id="text1086"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1084"
              x={294.23877}
              y={38.61882}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ALARMA PREVENTIVA'}
            </tspan>
          </text>
          <circle
            r={3.75}
            cy={36.595806}
            cx={361.20483}
            id="circle1088"
            display="inline"
            opacity={1}
            fill="#fc0"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            transform="matrix(.50256 0 0 .44492 341.541 -43.264)"
            ry={4.0336714}
            rx={4.2296953}
            cy={175.58884}
            cx={39.309525}
            id="ellipse1090"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            id="text1094"
            y={38.61882}
            x={378.37756}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={38.61882}
              x={378.37756}
              id="tspan1092"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ALARMA EMERGENTE'}
            </tspan>
          </text>
          <circle
            id="circle1096"
            cx={445.34363}
            cy={36.595806}
            r={3.75}
            display="inline"
            opacity={1}
            fill="red"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            id="ellipse1098"
            cx={39.309525}
            cy={175.58884}
            rx={4.2296953}
            ry={4.0336714}
            transform="matrix(.50256 0 0 .44492 425.679 -43.264)"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={461.98718}
            y={38.61882}
            id="text1102"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1100"
              x={461.98718}
              y={38.61882}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'APAGADO/STAND BY'}
            </tspan>
          </text>
          <circle
            r={3.75}
            cy={36.595806}
            cx={530.0116}
            id="circle1104"
            display="inline"
            opacity={1}
            fill="#4d4d4d"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            transform="matrix(.50256 0 0 .44492 510.346 -43.264)"
            ry={4.0336714}
            rx={4.2296953}
            cy={175.58884}
            cx={39.309525}
            id="ellipse1106"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <path
            id="path1112"
            d="M220.62 26.527l6.314 2.352h98.488l6.743-2.512"
            fill="#0ceef7"
            fillOpacity={0.74902}
            stroke="none"
            strokeWidth=".21253px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path1140"
            clipPath="url(#clipPath2072)"
            transform="matrix(.259 0 0 .26502 45.27 21.017)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(.259 0 0 .26502 108.77 21.017)"
            clipPath="url(#clipPath2072)"
            id="path1162"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path1164"
            clipPath="url(#clipPath2072)"
            transform="matrix(.259 0 0 .26502 178.091 21.017)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(.259 0 0 .26502 261.17 21.017)"
            clipPath="url(#clipPath2072)"
            id="path1166"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path1168"
            clipPath="url(#clipPath2072)"
            transform="matrix(.259 0 0 .26502 345.309 21.017)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(.259 0 0 .26502 429.976 21.017)"
            clipPath="url(#clipPath2072)"
            id="path1170"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
