import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
//import { url } from 'inspector';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  ///const theme = useTheme();
  const styles = getStyles();
  let ats_url = 'https://bmsclouduio.i.telconet.net/d/4WrHF8uGk/grupal_ats?orgId=1&from=now-3h&to=now&refresh=10s';
  let ups_url = 'https://bmsclouduio.i.telconet.net/d/5SprcRrGz/grupal_ups?orgId=1&from=now-3h&to=now&refresh=10s';
  let pdu_url = 'https://bmsclouduio.i.telconet.net/d/AsVfJW9Gk/grupal_pdu?orgId=1&from=now-3h&to=now&refresh=10s';
  let gen_url = 'https://bmsclouduio.i.telconet.net/d/OeQEGN9Gz/grupal_gen?orgId=1&from=now-3h&to=now&refresh=10s';
  let uma_url = 'https://bmsclouduio.i.telconet.net/d/r6tdROrMk/grupal_umas?orgId=1&from=now-3h&to=now&refresh=10s';
  let chill_url = 'https://bmsclouduio.i.telconet.net/d/zuwosVXMz/grupal_chill?orgId=1&from=now-3h&to=now&refresh=10s';
  let var_url = 'https://bmsclouduio.i.telconet.net/d/wm2F2Td7k/grupal_var?orgId=1&from=now-3h&to=now&refresh=10s';
  let ger_url = 'https://bmsclouduio.i.telconet.net/d/-MP4guXMk/gerencia?orgId=1&from=now-3h&to=now&refresh=10s';
  let elec_url = 'https://bmsclouduio.i.telconet.net/d/GqPfZd9Mz/unifilar-electrico?orgId=1&from=now-3h&to=now&refresh=10s';
  let aacc_url = 'https://bmsclouduio.i.telconet.net/d/WiGOxcXMz/aacc?orgId=1&from=now-3h&to=now&refresh=10s';
  let inc_url = 'https://bmsclouduio.i.telconet.net/d/ruoLmnrMk/sistema-contraincendios?orgId=1&from=now-3h&to=now&refresh=10s';
  let rele_url = 'https://bmsclouduio.i.telconet.net/d/pl_nX_XGz/reporte-electrico?orgId=1&from=now-3h&to=now';
  let raacc_url = 'https://bmsclouduio.i.telconet.net/d/eV1MDOZnk/reporte-aacc?orgId=1&from=now-3h&to=now';
  let dele_url = 'http://172.30.128.208:1880/ui/#!/1?socketid=953mpJJRWzyDbgyJAAAq&from=now-1h&to=now';
  let daacc_url = 'http://172.30.128.208:1880/ui/#!/2?socketid=953mpJJRWzyDbgyJAAAq&from=now-1h&to=now';
  let arq_url = 'https://bmsclouduio.i.telconet.net/d/YE3CT_qMk/arquitectura?orgId=1&from=now-3h&to=now';
  let principal_url = 'https://bmsclouduio.i.telconet.net/d/bz40p9_Mz/principal?orgId=1&from=now-3h&to=now';
  let dse_url = 'https://bmsclouduio.i.telconet.net/d/kDsk0H3Gz/dse-7420?orgId=1&var-fase=B&from=now-3h&to=now';
  let cmt_url = 'https://bmsclouduio.i.telconet.net/d/HCipT2pMk/cmt-ion-8500?orgId=1&from=now-6h&to=now';
  let sila_url = 'https://bmsclouduio.i.telconet.net/d/ADIxiCgnz/modulo-sila?orgId=1&from=now-6h&to=now';
  let pqm_url = 'https://bmsclouduio.i.telconet.net/d/AetGlxuMk/pqm?orgId=1&from=now-3h&to=now';
  let pm_url = 'https://bmsclouduio.i.telconet.net/d/NN9jELcnk/pm5330?orgId=1';
  let cmdb_url = 'https://bmsclouduio.i.telconet.net/d/uYGY6T9Vk/cmdb?orgId=1&refresh=5s';
  let hist_url =
    "http://172.30.165.13:5601/app/dashboards#/view/831b9470-305c-11ec-9d08-3b27680be30c?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:'2016-01-01T07:30:00.000Z',to:'2016-01-10T04:30:00.000Z'))&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),query:(language:kuery,query:''),timeRestore:!f,title:'Data%20Historian',viewMode:view)";

  //<a href={ger_url}>{"G"}</a>
  /*<a href={ats_url}  target="_blank" style={{fill:"white"}}> {"ATS"}</a>
  <a href={ups_url}  target="_blank" style={{fill:"white"}}> {"UPS"}</a>
  <a href={pdu_url}  target="_blank" style={{fill:"white"}}> {"PDU"}</a>
  <a href={gen_url}  target="_blank" style={{fill:"white"}}> {"GEN"}</a>
  <a href={uma_url}  target="_blank" style={{fill:"white"}}> {"UMA"}</a>
  <a href={chill_url}  target="_blank" style={{fill:"white"}}> {"CHILLER"}</a>*/

  /*let id_img = ['path3815','g2002','aacc','incendios','elect2','aacc1','elect3','aacc3','cpu','dc','dse','pqm','sila','ion','ats','ups','pdu','gen','uma','chiller'];
  let id_url = [ger_url, elec_url, aacc_url, inc_url, rele_url, raacc_url, dele_url, daacc_url, arq_url, principal_url, dse_url, pqm_url, sila_url, cmt_url, ats_url, ups_url, pdu_url, gen_url, uma_url, chill_url];

  }*/

  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
    <svg
      width={"100%"}
      height={"100%"}
      viewBox="0 0 601.92707 47.625002"
      id="svg14957"
      xmlSpace="preserve"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      xmlns="http://www.w3.org/2000/svg"
      //{...props}
    >
      <defs id="defs14954">
        <linearGradient id="linearGradient1156">
          <stop id="stop1152" offset={0} stopColor="#09a8ae" stopOpacity={1} />
          <stop id="stop1154" offset={1} stopColor="#09a8ae" stopOpacity={0} />
        </linearGradient>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath10124">
          <rect
            id="rect10126"
            width={221.73434}
            height={84.917526}
            x={-107.32134}
            y={40.867176}
            ry={10.796124}
            transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
            fill="#00f"
            fillRule="evenodd"
            strokeWidth={0.349927}
          />
        </clipPath>
        <clipPath id="clipPath1069" clipPathUnits="userSpaceOnUse">
          <ellipse
            ry={92.971107}
            rx={51.450279}
            cy={346.14044}
            cx={-248.095}
            id="ellipse1071"
            opacity={1}
            fill="olive"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={7.53068}
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
        </clipPath>
        <clipPath id="clipPath5494" clipPathUnits="userSpaceOnUse">
          <rect
            ry={1.2665578}
            y={373.44049}
            x={1007.6845}
            height={226.78572}
            width={102.80952}
            id="rect5496"
            fill="none"
            fillRule="evenodd"
            stroke="#61625b"
            strokeWidth={4.99999}
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14840">
          <rect
            id="rect14838"
            width={221.73434}
            height={84.917526}
            x={-107.32134}
            y={40.867176}
            ry={10.796124}
            transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
            fill="#00f"
            fillRule="evenodd"
            strokeWidth={0.349927}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14848">
          <rect
            id="rect14846"
            width={221.73434}
            height={84.917526}
            x={-107.32134}
            y={40.867176}
            ry={10.796124}
            transform="matrix(.69323 -.72072 .63286 .77426 0 0)"
            fill="#00f"
            fillRule="evenodd"
            strokeWidth={0.349927}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1984">
          <path
            d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
            id="path1986"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2072">
          <path
            d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
            id="path2074"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <filter
          height={1.1713483}
          y={-0.085674155}
          width={1.1634072}
          x={-0.081703613}
          id="filter2091-9-7"
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            id="feGaussianBlur2093-4-3"
            stdDeviation={0.2879845}
          />
        </filter>
        <linearGradient
          gradientTransform="matrix(1 0 0 .65243 28.677 -67.054)"
          gradientUnits="userSpaceOnUse"
          y2={145.97466}
          x2={260.96414}
          y1={172.82849}
          x1={260.96414}
          id="linearGradient1158"
          xlinkHref="#linearGradient1156"
        />
      </defs>
      <g id="layer1">
        <path
          d="M574.793 32.835l-9.268-6.287-518.71-.044-3.209 1.877v.874l3.136 1.659c-.05 2.778-.206 4.236.026 6.172l3.474 3.1v1.66l-1.896 1.178v1.921h506.97l2.261-1.135 3.938 2.402h8.241l4.819-2.482z"
          id="path1142"
          opacity={0.2}
          fill="url(#linearGradient1158)"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.484638}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <text
          id="sila"
          y={15.864633}
          x={133.37317}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={133.37317}
            id="tspan12985"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={sila_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'SIL-A'}{' '}
              </a>
          </tspan>
        </text>
        <path
          id="path1902"
          d="M3.412 10.272v12.16h46.371l6.548-5.745V4.727H9.76z"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.5}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M3.732 14.522v-4.027l6.123-5.42h4.681"
          id="path3005"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <g
          id="g2002"
          transform="translate(21.167 -.53)"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        >
          <path
            d="M140.102 14.825h-.86l-.004-.847.784-.077z"
            id="path10142-9"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path10142-3-4"
            d="M140.176 13.388l-.776-.37.362-.767.74.269z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M140.89 12.027l-.585-.63.617-.58.59.522z"
            id="path10142-3-2-5"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path10142-3-2-1-2"
            d="M142.029 11.061l-.297-.807.793-.296.343.709z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M143.434 10.59l.056-.86.848.016.057.786z"
            id="path10142-3-2-1-6-1"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path10142-3-2-1-6-7-2"
            d="M144.938 10.728l.363-.78.784.32-.23.754z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M146.263 11.39l.644-.572.592.608-.511.599z"
            id="path10142-3-2-1-6-7-9-1"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path10142-3-2-1-6-7-9-8-1"
            d="M147.238 12.544l.806-.306.337.778-.69.379z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M147.764 13.878l.859.067-.027.847-.886.036z"
            id="path10142-3-2-1-6-7-9-8-7-9"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path10270-8"
            d="M140.962 14.925l3.479-5.945-1.167 3.786 2.892-.87-4.347 8.51 1.598-5.839z"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="#fd6425"
            strokeWidth={0.289164}
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
          />
          <path
            d="M140.609 16.826c.898 2.35 5.457 2.631 6.759-.244"
            id="path949"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
          />
        </g>
        <path
          d="M176.165 9.311l-.386.323.022 1.335-.86-.434-.26.156v.408l1.08.614v1.04l-.816-.448-.017.905-.874-.47v-1.314l-.314-.215-.3.166v.986l-1.282-.748-.359.206.036.515 1.152.619-.735.457-.01.367.409.229 1.066-.596.73.466-.64.47.668.467-.811.47-1.05-.618-.371.18.004.409.743.43-1.155.666v.489l.404.21 1.201-.708v.887l.314.198.322-.238v-1.223l.928-.449v.87l.758-.511.004 1.062-1.045.592v.367l.31.22.793-.457v1.33l.394.323.296-.331V17.67l.825.461.282-.188-.013-.515-1.013-.511v-1.031l.793.493v-.83l.883.485v1.192l.323.237.34-.246v-.941l1.143.735.417-.188-.027-.475-1.134-.628.722-.47v-.39l-.292-.184-1.062.556-.829-.435.695-.48-.686-.403.798-.57 1.116.624.318-.198-.027-.403-.757-.435 1.174-.654-.045-.506-.354-.162-1.174.623-.014-.91-.345-.197-.367.229v1.349l-.892.457v-.919l-.73.516v-1.02l1.113-.643v-.386l-.339-.196-.836.483v-1.48zm.375 4.43l.37.592-.36.595-.733.005-.37-.59.361-.597z"
          id="aacc"
          display="inline"
          fill="#3fbefa"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".0177471px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <g
          id="incendios"
          transform="matrix(.0645 0 0 .0645 200.909 -9.025)"
          clipPath="url(#clipPath1069)"
          display="inline"
          opacity={0.8}
        >
          <image
            transform="translate(-1302.83 -112.597)"
            clipPath="url(#clipPath5494)"
            id="image2379"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5gAAAKACAYAAAD5Otf/AAAgAElEQVR4XuzdTawm13kf+Lomm/De CGwhDhgBBuyVrbEkUAlJbRIkMSI2BQRhAwqz6GA8YEReMhvBgA2thBlgwE3ISzLMOABnQREggwBs xoFjw56Fu5mYkOWRvbIBA44RCbQn8D7opvgOziWrVV1dH6fOW1XvOVW/u+mPW6fqnN9T9/b991Mf Z5UPAgQIECBAgAABAgQIECAwg8DZDPuwCwIECBAgQIAAAQIECBAgUAmYTgICBAgQIECAAAECBAgQ mEVAwJyF0U4IECBAgAABAgQIECBAQMB0DhAgQIAAAQIECBAgQIDALAIC5iyMdkKAAAECBAgQIECA AAECAqZzgAABAgQIECBAgAABAgRmERAwZ2G0EwIECBAgQIAAAQIECBAQMJ0DBAgQIECAAAECBAgQ IDCLgIA5C6OdECBAgAABAgQIECBAgICA6RwgQIAAAQIECBAgQIAAgVkEBMxZGO2EAAECBAgQIECA AAECBARM5wABAgQIECBAgAABAgQIzCIgYM7CaCcECBAgQIAAAQIECBAgIGA6BwgQIECAAAECBAgQ IEBgFgEBcxZGOyFAgAABAgQIECBAgAABAdM5QIAAAQIECBAgQIAAAQKzCAiYszDaCQECBAgQIECA AAECBAgImM4BAgQIECBAgAABAgQIEJhFQMCchdFOCBAgQIAAAQIECBAgQEDAdA4QIECAAAECBAgQ IECAwCwCAuYsjHZCgAABAgQIECBAgAABAgKmc4AAAQIECBAgQIAAAQIEZhEQMGdhtBMCBAgQIECA AAECBAgQEDCdAwQIECBAgAABAgQIECAwi4CAOQujnRAgQIAAAQIECBAgQICAgOkcIECAAAECBAgQ IECAAIFZBATMWRjthAABAgQIECBAgAABAgQETOcAAQIECBAgQIAAAQIECMwiIGDOwmgnBAgQIECA AAECBAgQICBgOgcIECBAgAABAgQIECBAYBYBAXMWRjshQIAAAQIECBAgQIAAAQHTOUCAAAECBAgQ IECAAAECswgImLMw2gkBAgQIECBAgAABAgQICJjOAQIECBAgQIAAAQIECBCYRUDAnIXRTggQIECA AAECBAgQIEBAwHQOECBAgAABAgQIECBAgMAsAgLmLIx2QoAAAQIECBAgQIAAAQICpnOAAAECBAgQ IJCZwAvPPXu4ePW16t0bN6qrV6/6eS2z+pgOAQL9Ar5hOTsIECBAgAABAicWqANlexoC5okL4/AE CEwWEDAnkxlAgAABAgQIEDhO4L333ju8+OKL1fu3bg3uSMA8ztloAgTWFxAw1zd3RAIECBAgQGCH An1dykBxraqqJ8/Oqqc+dXnwcLj8nYC5wxPFkgkULiBgFl5A0ydAgAABAgTyFBjrUoZQ+e2z7h/F BMw8a2pWBAiMCwiY40a2IECAAAECBAiMCsQEymaXcmiHAuYotw0IEMhUQMDMtDCmRYAAAQIECOQt EALl7/72b1Xhaa99H281LnsdWs3ZlSvV7aefrq688cblZgJm3rU3OwIE+gUETGcHAQIECBAgQCBS YM4u5Z3r16uHHnmk+uiLX6we/NznLmdweOCBewLmx4eDn9Uia2MzAgTyEPBNK486mAUBAgQIECCQ oUBKoAzdyMOdO/etpu5S/thzz90NlM2NPvzww+qnfvqnBcwMzwNTIkAgXkDAjLeyJQECBAhsVGDo 6Z7NJT/62GPVzZs3/du50fOgXtbFxcXhheef71xleDBP+Oh7OE97UN2l/PArX6k+85nPDMoJmBs/ sSyPwE4E/CO5k0JbJgECBAjcK/D4448fxt5BOGQWwuZTTz1VnZ+f+7e08JNr7D8Y2q8Q6Vtu3aEM l71Wv/zLSSoukU1iM4gAgYwE/KOYUTFMhQABAgSWFxgLliE4/uIv/Hz1Mz/7c9Wf/emfVH/4R398 OamhMOo+ueXrNucRxh7O0w6UITiGj5TLXqfMWwdzipZtCRDIVUDAzLUy5kWAAAECswr0danOn/16 9Su/+mujly/Wk3nllVeqd955pzNwCpqzlmzWnQ11KWM7lGFCY/dRHjNpAfMYPWMJEMhFQMDMpRLm QYAAAQKLCfzY2dmhvfN3b9yonnjiid5jhh/2h+6ZC5+/du3afUEz7Pfq1av+fV2smnE7Tnk4T9ee 6+5ldXFRxdxHGTe77q0EzGP0jCVAIBcB/wDmUgnzIECAAIFFBNrhsi9Y1oGyHSz7/r6e7H/8j/+x evHFF+8Jmi+9/LJ7Mxep5vBOhy5/zqVLOcbiHswxIZ8nQCB3AQEz9wqZHwECBAgkCzTDZbi38vd+ 7/fu21dfp7Lr75thM+yo2eH8V+fPVRevvnZ3/+HS25deedW/s8nVGx849LTXMDqEypinvS552ev4 Kn60hQ7mFC3bEiCQq4B/+HKtjHkRIECAwFECzXAZwt6/vnhlcH9jHczm4HbQrMNmuD+z+XoLl8se VcLOwVO6lH3voww7rl8f8tEXv9j5Tsr5Zz6+RwFz3MgWBAjkLyBg5l8jMyRAgACBiQJD4bIdJGM7 mF3j2tMKHc1wyexXn3zy7qeEzInFa2weOpR9D1QKm8Ve9lrfR3n76aerv/7Wt6If6JQ+87SRAmaa m1EECOQlIGDmVQ+zIUCAAIEjBZpPC+27LLZ9iLHuZTOEht+Pffyf/8f/fs/lsp4uOyb2o8+Hh/M0 A3p7ZGyoDONCl/LHnnuu+h8/+ZPZhsr2+tyDGX+u2JIAgTwFBMw862JWBAgQIJAg0A4nP/z443v2 ktq9DDuJ6WA2D9YMmSHo3rx507+5HTWd2qXsu+y1vo/yoUceqapf/uWEs+f0Q3QwT18DMyBA4HgB /9gdb2gPBAgQIJCJQPPS2PbTYscuhe17emxzaUPdyz/4gz+ovvCFL9wj8dN/82/e/bMu5icU4T8B /uIv/uKee1Xbp89bZ2fVUxHnVC4P54mYatQmH33ve9UDn//85bYPHj55s47zJorORgQIZCQgYGZU DFMhQIAAgXSBZvey+VCfsWDZPmL7cthwX2Vs97IOmfWv/+E//Ie7QWrPXcxw2fIf/tEf3/fO0GAf LnkNH09GhMotdCmHzjcBM/3r30gCBPIREDDzqYWZECBAgMARAs3uZfvS2LDbKfdZ9oWAmPsv2yHz 2rVrd4PVXrpR4bLXP/vTP7nnPtSmaex9lFsIlO3zLoTIcE9o+Pgbf/VXl0+wDX9X/9ruYHpI1BHf FAwlQOAkAgLmSdgdlAABAgTmFqgDZvuVJH0dzGboHAqU7Q5me9t2oOz6c/3Qmi2/G3PonZR1oAx2 Q5e+1k97rS4uqpxeHxJ7rjbDZFd4DPtpB8r2vgXMWG3bESCQq4CAmWtlzIsAAQIEogWa4aZ972Uz SKZ2MWM6l+E4fWFzi/diDl32GixCqPz22fiPGSXcR9nXhQwhMnQjw6/tjzpIRp/En24oYE4Vsz0B ArkJjH/nz23G5kOAAAECBFoCfZfHDnUvm8GzDdr3tNmuoDnUwQz7DQ/+2cplss1XwLTNplz2ejk2 wy5lV5BsdxzrQNn++7HOZMwX7YPf+U51eOaZy03rh/y4RDZGzjYECOQkIGDmVA1zIUCAAIEkgTpg dr33cuzVJH1dzWYAjelgNoNmHSy7Hvbz0ssvV+fn50X8+ztHoAwWl5e+njhQDoXHdmisT8K+7mTS SdoYFILk5SXAHb8KmMfqGk+AwKkFivgH7tRIjk+AAAECeQvUATOEt+eee+5ysqndy/bYsXA51sGs 5er7MHMOmOFJvC+++GLn017DOmK7lGHbO9evVz/23HOX9xyu/THlXsgwt/blrKmXt9aB8XKfAyGy HS7D9vXfCZhrny2OR4DA3AIC5tyi9keAAAECqwvUATPm/suuADn0KpL6c+1FNd972RUy6+3DJbLh 83XAzOl1JXMFyubTXj/8yleqYLbUx5T7IVODYszcpwbImH2GbQTMWCnbESCQq4CAmWtlzIsAAQIE ogSaD/j5/g9+cE+46epijnU22wdN6WCGfdTBst5fHTDDn0/5upLg9c477wx2KccezlM/7fX2008v 0qUcCpHN0Ni8hLX593MFy2M6knXd685k+HPz930nt4AZ9WVvIwIEMhYQMDMujqkRIECAwLhA8z7B +v2X7RA5FDSnPNCnns3YZbHtcBn+fKonycYEyifPzu6+PiSEx8OdO/fBz/2019q9+Q7Ivnsh57qE tets6upENsNgOxTGhMTxs7Z7i9sffFBdeeONy096yE+qonEECJxaQMA8dQUcnwABAgSOEnj88ccP 79+6dbmPOmDWO2wGy6GH+bQn0Nx2bHJDl8fWY9cMmOGy19/97d+qLl59rXPqU++jfOiRR6rql395 jOG+z7cDZN0Jbr4f8jJIfe5zVQiZ7d93/XnyJBoDpgTJrhA5V7AMIfLStKqq+vfNXwXMY6psLAEC OQgImDlUwRwIECBAIFmgq4M5trOhrmXfPZf1Pqd2L+txzYfnzH2J7Ng7Kd/69H2UT43ATH3aa8xr PU4dIMOShx6qM3aupH6+KzyGYNn19+EYdz/35puXHWQdzFR54wgQOLWAgHnqCjg+AQIECBwl0NXB HAuQfQfsGjc0uZjuZRg/dwdz6mWvXWuIuY+y2QGuL2UNfxe6kH2Xsx5VzJHBfV3IU4TIvk5kvYR2 mIx10cGMlbIdAQK5CgiYuVbGvAgQIEAgSmBqBzPmvZh9B07tXob9HdvBbD7MqD2/lMteL7t6Ha8Q Gbo3cihUHvNgnb6H6TTXmUsXMsypKzymBsp2LQXMqC97GxEgkLGAgJlxcUyNAAECBMYF+u7BHAuS zT2377kcu0w2jI3tXtbHaT5FdnxV41vEXPY61qVshsnmE1nHj37cFs1OZNjTUHic697HsRk3L10d C5Fj+zrm8wLmMXrGEiCQg4CAmUMVzIEAAQIEkgWmdDDHQufQw32O6V6GxdUdzJdefrl6+OGHB9cb Lqm9du3aPa8SSelStt9J2b68tWsSqZ3IsVd6NI/VFSiTT4CRgX33PLaHzdWBnLKOh958swqvemn+ Gsa7B3OKom0JEMhNQMDMrSLmQ4AAAQKTBGKeIjv27sspT40Nk5vavQxj6g7muzdu3H1HZv06k65f 64D50acP6BlCuXP9+uVlm+3LXutAOQk0YuOueyHbobHr/Y9rdCPHOpFDD9qJWPrkTerwGAZ2BcoQ MJufa3cw534g1OQFGECAAIGJAgLmRDCbEyBAgEBeAkMP+WnPdKiDWb9Go2t1x3YvmwHz+z/4wWVA rT/a78wMf193MH/61q3q2x0Bs/lOynBpa7ikN3zUr/s4pkIxncg1O5D1WqZ2Ii9D26dPbT3GI2Zs X3BM+fv6HaT1U2QFzJgK2IYAgZwEBMycqmEuBAgQIDBZYOwezLDDoQ7mUk+ObS8kdDAffeyx6u23 374bMNvhstnJDB3MOmDWgbLdpZyjQznUjbwMreFhQN/5zicBtvH79vrm6k6OvsajERrXuqx1ahcy JlgGv/ry2KalgDn5W4ABBAhkJiBgZlYQ0yFAgACBaQJDl8i2w2U7TDY/39fBHOtehn10dSGbq/jd 3/6t6uLV16r68tj6c/W+u/4cAmb4+H9efvnylSDhI7weJPZjLDjW+1kjNF6GqQ8+GHz6aj2frq7j 0p3IsUAY5ja0TT33rvsp+4JkO1w2/9y8B1MHM/aMtx0BArkICJi5VMI8CBAgQCBJIKaD2bfjY+69 jAmW9XFD9/L82a9Xv/Krv3b5V0PBMnw+XPL65S9/+XLb0PHsC5Z974VMvR/y2C7k0Lshh17tcRng Tnw5692A2/PgnWYA7Pp9Vzey6+9iTnIBM0bJNgQI5CogYOZaGfMiQIAAgSiB2A5mV/cyHKDuXHa9 mmSoeznWtQz7/ou/+Ivqheefv7w09vd+7/fuHmtsYc2AGTqY4WPKqz3G9p/6+ZgAeTeodVzKmnrc mHFjl7GGfbQ7jEOhsKsbOdR1jJlj7DYCZqyU7QgQyFFAwMyxKuZEgACBmQTee++9Qwg5fR9jr8u4 evXqWdhH+HWmKR29m3o+9a9DD/kZu/cyTGYoWNaTTXlqbBhb33cZwmX90dU1bc+hHTD7wuXRmD07 iLmctdlx7Pv9UvOLeRrr2GWvMZe81vNP7UROXX99/2UY5yE/U/VsT4BALgLZ/MCQC4h5ECBAYAsC P3Z2dtjCOqau4Ycff9w5JOXey7Cj5qWszZAZPjfWwewKl1PW86/On6v+8I/++PIezPohO1PG923b FR7bl6eO/XmOefTto68TGbbv60COXbI6NSBO3X4uDwFzLkn7IUDglAIC5in1HZsAAQILCLzw3LOH 8ECZPX60A2ZXB3PqU2ObQbN+vUhMuAzj+gJvTG3qezB/75//85jNe7cZCpRjQXLJ+yKndhjDAse6 jjGXsMZc+noUeMLgZrCsh+tgJkAaQoBAFgICZhZlMAkCBAjMJ9C8ZHS+vZaxpzrQtYNlX9DsW9Ux 916++OKL1fu3bh0VLsO8Qgfz/3v1teqtf/tvJ4XH9sZLhsS+iU0Nj2PBsT5OX2dxSrDM6UzuCpYC Zk4VMhcCBFIEBMwUNWMIECCQscBeL48NJenqGDbD5Rzdy6HS1w/1+f4PfnB5b+cxH+2A2exE1vsd ejLr0sFy7KE6IfSFjylhM2w/drnrWNjsMw9hLrxPNIePoWApYOZQIXMgQOAYAQHzGD1jCRAgkKGA gPlJUfoe8DP2apKY7mXfJbL160j+9cUrR58ZzYDZDpdLh8e7AbbjlR3NS0ynBshT3dsY5tkMdWsF zZgg2XeiuET26C8hOyBA4EQCAuaJ4B2WAAECSwkImPfLHtu5HLvnMhxxzu5l2F8dMP/vf/EvljpV Ovc7teO46uSOONhY2Auhs9nlrH9fj2t/vp5Ke7sjpnjZYa2PJ2AeI2ksAQKnFBAwT6nv2AQIEFhA QMD8Ufey717M+t2Xbf6h7mXYdihohnsvw0fzlSTHlHeugNkMjH3zie3uxW7XDF/tY67RPRwLk8fU Za2xAuZa0o5DgMDcAgLm3KL2R4AAgRMKhHdDhss09/ox9NTWsUtjh8LmWLgMnw/uL738cvXcc8/N wt8MmDEhMRy0K1iNBbqhMNbsqE0NiilzaR+jvY+++QzNc5ZiLLiTvrkLmAui2zUBAosKCJiL8to5 AQIE1hXY8xNkg3QzYPa9+7KvIjH3Xg5VMwTMd2/cqJ544onLzcYeLhQeAtR16W79cKA6YH777Ef/ VPeFxbGO3VDIHBs7tOau/cbsb6n5rPvVNn60Y4JvHTDDOXX16lU/r41z24IAgUwEfMPKpBCmQYAA gWMF9vz+y9purIMZtquD3ZSgGVObEDDrp8f2XYIbs596ju2AeWwoSw2DMXM+JkgdM3Ys/MaE3Zj1 zb1NzJoFzLnV7Y8AgbUEBMy1pB2HAAECCwvs+d7LdsDs6wwuce9lfey6gxnu0zz2I4TgL3/5y9VP 37pVNTuYYb/NoDg1QMUEm2Pn3jxG1/HWmEPXGtrHjZlb7Fxjtxuz7XrIjw7mmJrPEyCQm4CAmVtF zIcAAQIJAsLlJ2hD78GM7So2L5UN+xx6sE94cuzDDz98eez6Hsx/8k/+yd0Kxlx2W++/+WvYwbVr 1zoDZsLpMXnIWEisg+7UgBszkbH7PtsPGlpiDjHzXHobHcylhe2fAIGlBATMpWTtlwABAisJCJc/ gu66BzN8dihcxoTAoVLWIbN+B+av/OqvVWGfzXDaFSLHjnvKgLnEqRvTMWwfd2pncOr2S6yzK3xP Cc31nATMpapjvwQILC0gYC4tbP8ECBBYSGDvD/TpYu3rYIZth+69HHs9SUwJw2tK3r916/I+zDpg xozr2iYE0qUDZswlo1MCXy7hrstzbG5jn6/3mWI2xbB5nAdu3778o0tkU7+KjCNA4FQCAuap5B2X AAECiQJ7fxXJEFu7gxkbLMN27Utj6+P0XSJbdy6bv77w/POXryqpL5tNLPHlsBBY2/dgpnQBw75i A9Qx822OTTlezJjU9U+ZW8zlwXM5De1HB3MNZccgQGAJAQFzCVX7JECAwEICFxcXhxBifHQLDHUw h8yO6WCGgBk+QqgMl8k++thj1Te+8Y2jS9QVMI/eqR0UIyBgFlMqEyVAoCUgYDolCBAgUIiAS2LH C1UHzOZTZLtGjd3/OHaJa7NrWYfLOmiG/wA4f/br1d/7B/+wqv+uvc3YSkJYPSZgxnQD++Ywx2Wg zX2n7G/O+Y9ZH3OssO+luqoC5ljlfJ4AgVwFBMxcK2NeBAgQaAh4kE/c6TD1Etmw166wGf5+6Omx 4fPtkFnP8J133rm8F7O+VLYZMuNW8Uk3dCxgHhOMUkJfe+5z7KMvoI0dK3ZcrPeU7Y5xj513OIZ7 MKdUxbYECOQkIGDmVA1zIUCAQEvA/ZbTTok5OphjRxzqXoax9aWy4fchZKZ+hKDa9R7Mrv2lPKU0 ZV7HhquUYx4zZo7u4jHHr8emuOlgziFvHwQInEJAwDyFumMSIEAgQsD9lhFIrU26Ophje0m5/7Kv e9k8Vt3JDJfL/szP/lz1Z3/6J2NTufv5sH1XwIwJKjHbxExkjv2k7GOtsDxmkDL3sX1O+byAOUXL tgQI5CQgYOZUDXMhQIDApwIvPPfs4eLV13hMFGh2MLuGpoTJ5n66nhw7NMX6gUwhZIaPZtAcC51/ +Ed/HN3BjGWa67LW5vFi97nU01mX2u/QGuvPxa69rz5DIVbAjD2rbUeAQG4CAmZuFTEfAgR2LyBc pp8CsZfI1kdICZwx3cvQrQwBMny0Q2bs6sJ/MFyrqurbZ2n/VKd04PrGpOwrdp1hu6W7ljHrWmKN qft0D+aUs8e2BAjkJpD2r1ZuqzAfAgQIbETglOEyvF4jfPziL/z85a91QAr3FKY8qCbso/k+yKtX ry7yb07TbMkOZmz3sg6XzV9DWAy+tW3oTobf1792nb59Hcw1OnZDX06poWnOL9FTzuHYY8eO18Gc 84yxLwIE1hRY5B/7NRfgWAQIENiSwJpPi63f17hU8FurLs3Xt4wFzDCnY15REtO9DMcYCpl1iG+G zLZVCJ/HdjDrfaZcxhkbgmJqPHVfU7fvm8Ox+4kdH7tdjFVzmzpgfnw4+FltKp7tCRA4qYBvWifl d3ACBAj8SGCtcBmC5c2bNzfz/b8dMMM7MNsfdais/74ZMmPOwa5g2dXZbV4a23yoT+gG15fK1p3i rlAZQmf9EV51MuUS2aWCzpBPSjc1dZ6p42Lqe+ptutYmYJ66Ko5PgECqwGZ+wEgFMI4AAQI5CKwR LsODZl565dXNfd+f0sE8pnsZzpOYDmZX9zImYLbPw6kB89jzeCzAjX0+9fjH3H+ZMqeUMalrO2ac gHmMnrEECJxSYHM/aJwS07EJECCQItAMSCnjx8ZsNVjW6276ff8HP+jkaHYwU7qX9U777kftCpXN icwZMGMDUtd2Uy+XjT1WGz3m2GPn7dTPzzXXqUbNeY6NjZljvY2AOfUMsD0BArkICJi5VMI8CBDY pcB77713+OqTTy6y9q0Hy66AGe7B7LpENmx7bPcy7GPsYUd9QbMZMKcUO1wi++TZWfXUwKCY0DLl mDHbLnnMJfa9xD5jnMa2GZqXgDmm5/MECOQqIGDmWhnzIkBg8wJLhcuXXn65Oj8/383396EO5hz3 XsaciGMdzLCPOd5r+tZI2IyZ69g2U8NY6iWuU46Tcq/n2DqP/fyU+accS8BMUTOGAIEcBHbzA0gO 2OZAgACBpsDc912+e+NGVfoTYVPOkCmXyKa897Ke09j9l0Mhs93BjHkyaPgPiBdffLEK92J2ffR1 N2MuT00JRzFjYrbpWssS48b2Ofb5lHMxdUzXXATMVE3jCBA4tYCAeeoKOD4BArsUmPN9lzFhZcvI sR3MY+697POL6VzWY5sdzJSajXW8Q3czfAxdTjtnuJt6Tk0JdFO2recRM6Zvm2PunZzawY2ZZ1iT gDn1DLM9AQK5CAiYuVTCPAgQ2JXAHN3LvXYs2yfKWAcz5d7LulvZPFbq/ZdhHykdzKEviBA2f/e3 f6v3stuYezd39QW34mJjA+RY2BcwVyyaQxEgMKuAgDkrp50RIEBgXGCOcJnSARufWZlbxHQwp3Yv g8RYoAzbrNnBHAucQ5fTTulujnXzwjxiQlTMNrFn3Jz7ij1m33ZrzUXAPLZSxhMgcCoBAfNU8o5L gMAuBS4uLg4vPP988toffeyx6ubNm753NwS7AubQw32+8IUv3PNE2XYxmt3Lofsu63BZB83wa+hU hr9vf8zdwRw7gYYuwV6qu5kSvGLCbNdax4419vkxvyU+P3VOAuYSVbBPAgTWEPBDyhrKjkGAAIFP BY7pXgqX3afREh3MsQf61DPJpYM59AWW+rCgOb9oU4JkbCCL3S6sJ2YeU/Y3p1F7XwLmkrr2TYDA kgIC5pK69k2AAIGGwDHhMuzGZbHxAbPrvssweqx7GbZphsuHH36491LZKeFy7Q7m0BfeWBd9jVeh lPSNISZwxmzTteahcQJmSWeJuRIg0BQQMJ0PBAgQWEFg7AmgY1MQLvuF5uxgLtG5rGd+7FNkx86R lM/P0d2M6QqmzK09JiXEpYyJnevUp8dO3a+AGStmOwIEchMQMHOriPkQILBJgWO6l54WO3xK9AXM vi5mzAkWEzSbHcywz777L+vPNe+9zfU/DJbobk4NeTHbLxXuxs6NmLmNBePYfQiYY9XweQIEchUQ MHOtjHkRILAZgWO6l+67HD8Nmg+0+f4PfnA5YOwS2a69Tg2VdaAcCpbN4+TYwRzTbYb39rYpDwuK fQ/l2Lyan48NbGOd1tj9TJlb7LZdx64Dpv9gilW0HQECuQgImLlUwjwIENiswDHdy1w7XTkVa+gp siFoho/63suhecfee1nvo93BHNp3TvdgpttVJZkAACAASURBVNZu7HLaKa9CmTKHUwW/Kcedsm3s 2gXMWCnbESCQm4CAmVtFzIcAgU0JHNO91LmIOxWmdDD79hjTvQxj61eQtLuWMV3MEjuYfV7hvP7d 3/6tqrmm5rYx3c0lQlncGVPGVgJmGXUySwIE7hcQMJ0VBAgQWFBg6H2EQ4c9f/br1UuvvOp7dERt YjqYYTdTu5h9h57y9Nh6H1voYA6VYqy7GRM4I0p9d5PSw+nY/MPnH7h9+3K9/qNpyplhWwIEchDw w0sOVTAHAgQ2K5B6eaxLY+NPiWM7mKF7WX/0vZYkJVS2V7ClDuZYdYYeFjQUNo99eE9McDvcuTM2 /Xs+P7bPvp2ljqv3p4M5qUw2JkAgIwEBM6NimAoBAtsTSAmYwuW08yDmKbKxe4y5VLYOm2Gfsfdh br2DOeQbwuY777xTvX/rVudmqYEztqZTQ+vUYDh1+655e8hPbDVtR4BACQICZglVMkcCBIoUSLk8 9qWXX67Oz899b55Q8aZzuJww5lLY5u6bHcyhw8aGyb597KmDORY4m69saW8bHhb0VGT95wh3kYc6 arOUeepgHkVuMAECJxTwQ8wJ8R2aAIFtCwy94qFv5bqX98sc86Ck0s6wvdV/7Xs35zofpnZFpx7X PZhTxWxPgEBOAgJmTtUwFwIENiUw9fJYD/PoLn8zYIb3goZLLZu/Nke1P1dflllvn/sJtreA2a7H XIFzyvs2u7Zt/l1K93HoPBt7H2c9Vgcz969W8yNAoE9AwHRuECBAYAGBlK7b3sNFXxlqy+//4AfV Zz7zmdFqffjhh73bhPdixlxCO3TZbP2qkq6D/OEf/XH1i7/w81X4deyjGYbrbZ0D96oNXWY+95Np x+rV+4PUlStV34ODjgmnAmZqRYwjQODUAgLmqSvg+AQIbFJg6v2X7r3sPw3GAmYIlCF49gXLECrr j6Fw2RcqY+69jA2WXaGyuXIBc/g8ePHFF5MeFlTiNxkBs8SqmTMBAkFAwHQeECBAYAGBqZfHChbH Bcy+0XXHMny+GTSb28c+OXboNGkGzLEOZt9lvGH/zoP4L8ZTdDeP6UhOGesezPjzwJYECOQnIGDm VxMzIkBgAwJTAqbu5XDBhzqYfd3L5qWwza5lXwczpnsZXjXSvjx2rmBZCwiY6V/8Qw/VGrucdkr4 65vhHPto7lsHM/1cMJIAgdMKCJin9Xd0AgQ2KDD1/kuhYvgkCO9RDK+16LsHc+jS2DpQDgXLhx9+ uOrrYqZcHtvVway7lmGlOpjLf9HHvHvz22fH/Qg0R6Ac2oeAufx54ggECCwjcNx312XmZK8ECBAo WmDq60kEzLSAOXTvZV8Hs+tIQ5fINgNmVwcz7C+1i9k1F+fC/F/6cz2Zdv6Zde+xDp0C5lrijkOA wNwCAubcovZHgMDuBaZcHnv+7Nerl1551ffigbMmtYNZ77K+93LuB/xMCZZ9ncv261MEzOW/faR0 N1O7lWOvQBlabR0wnRPLnxOOQIDAvAJ+qJnX094IECBQTQmYfngcP2H6AmbMpbFh7zHBsr5Mtjmb mMtjw/YxQXPostjmMZ0P4+fDnFuE7ubv/vZvVRevvta527F7N8Og1PDZPGDXPgTMOSttXwQIrCkg YK6p7VgECGxeoA5DsQsVKMalatMffvzx+MafbjH26pKwWd9TZcPnlnwPpnswo8u4+oY5XU4rYK5e fgckQGAmAQFzJki7IUCAQBCYcv9luDzy5s2bvg+PnDp1wAxec3w0H7jT3F9X8Gtfwhq27/q7OeYV 9uGJwnNJzrOfU7wKpZ65gDlPDe2FAIH1Bfxgs765IxIgsGGBKZfHvnvjRnX16lXfh3vOh7Fu0oZP o8uluT83vwof8yqUqasRMKeK2Z4AgVwE/GCTSyXMgwCBTQhMCZguj+0u+ZQu8CZOmohF+M+ICKSV Nxl7WNBbZ2fVU0fMScA8As9QAgROKiBgnpTfwQkQ2JKA91+mV3PqvavpRyp7pMuq863fHN3N5sN+ BMx8a21mBAgMCwiYzhACBAjMJDCl8+byx0/QBcu0k0/QTHNba9TY5d3h6bTfPhv+EUzAXKtajkOA wNwCAubcovZHgMBuBVweG196wTLeamhLQXMexyX3EhM2n+y4nFbAXLIq9k2AwJICAuaSuvZNgMCu BATM8XJP6fKO780WTQH39JZxPsQEztDdFDDLqKdZEiBwv4CA6awgQIDATAICZj+kjuVMJ9nIbnQ0 13Ge8yhDr0IJx/EfB3Nq2xcBAmsICJhrKDsGAQKbF5jygJ+93X85JXh3nSjhfrVwCeG1K1c2fx4d 7typ/tnhUL195Eq9T/NIwBMN7+puCpgnKobDEiCQLCBgJtMZSIAAgR8JjHUhmlZ7eeXEsZfDhtc8 7CFUdn0dhaAZPo4Nm8JJ2d+lwveVl1551c9qZZfR7AnsTsA3rd2V3IIJEFhCYEqXbus/9B9zOWz9 dM3wuoa9f4SQWTu8fedO9bXDIYnEZbNJbAYRIECAQKKAgJkIZxgBAgSaAgJmVU25TLh99jSDZTNY Ocuqqu5mhrB5TNDc26XZzh0CBAgQOI2AgHkad0clQGBjAnsPmFMuEW6Wfu2OZR3Wjjn9cuiuHhM0 t95BP6a2xhIgQIDA8QIC5vGG9kCAwM4FpoSrLV6uOCVc16dKCJZvPfTQomdOX5hMDYhj4TR1v8cg pAZNDwE6Rt1YAgQIEBgSEDCdHwQIEDhSYMrDbLb0gJ/US2I/Oju7e2/hkfSXw4eCXwh9t59++nK7 hx55pProi1+875D/4yd/8u7ffeYzn6k+/PDDu3/+G3/1V/ds/+B3vnP3z7c/+KB66M03R48/xxrH 9vG127cnP3nWJbNjqj5PgAABAikCAmaKmjEECBBoCEzp4G3l8sQpobqmqp8KO8c9ll2h8s7163eD 5Idf+UoVwmL9UYfGZmAMwbIdINsndh0+m/vqO/k/+t73qhBA6+BZh9+1OpuprzjZyjnpmxIBAgQI 5CEgYOZRB7MgQKBggb0FzCnrDWWd63LYdqisu5M/9txzl2dPsxM5Fhz7gmT4+7qLGX4NoTF81J3L EB6bH6GDWX/UndK+zy99itc+71TV5CfOCplLV8f+CRAgsB8BAXM/tbZSAgQWEpgSuEr+QT7lkthj L4dth8rQpawvdX3wc5+7eznrlEBZdy7Dr3Vnsu4+hlOkDpHNy1+bXcjmnOq/H7tMd6FTb3C3Uy+b 3eL9wadwd0wCBAjsXUDA3PsZYP0ECBwtEBswS77nbeq7LevLYVNx7wtxFxeX90/WXcqYQNm8BLb+ fR1K6/Efv/LK3fsoQ1jsCopdl7h2XeY790OFUu2a46Y+BEjInEPdPggQILBvAQFz3/W3egIEjhSY 8gTZUgPm1M5larhsh8pwyWm4/HVKqAzlbN9bWXcqQ5cyBMrwUXcnxzqQKfdPNt9beeTpNcvwqfdm CpmzsNsJAQIEdisgYO629BZOgMAcAlMedlPiE2SnBOhj7rVshrIQLP/6W9+6LM9Qp7KrQ1nXtBlK L++fPD+/7E62u5QpAXKO8+YU+5jazSz5cu5T+DomAQIECHwiIGA6EwgQIHCEQOzlseEQpf3APuWy 2JRwed9De15//e5lsDGXwLbLds8TX3/91+8+zTX2stcjToPsh9bhWsjMvlQmSIAAgeIFBMziS2gB BAicUmCrAXNKZzblQT73XEZ6cVGF14qEj65gOdSpDGPal8BeeeON+06JPXUqY74epjwAqLT/GIlZ v20IECBAYDkBAXM5W3smQGAHAlsMmEuGy/alsPU9lsd0LMPY5sN6wmlXXworWPZ/EQqZO/gGZYkE CBA4gYCAeQJ0hyRAYBsCU+5PLOXBKVPC5Q8feujufY1jFW1ephpeNTIWLNsP6qk7lSFMdnUsm/dW CpVj1fjR54XMeCtbEiBAgECcgIAZ52QrAgQI3CcwJYyV8ATZ2MA89X7Le17p8enlsCkdy2bIDB3L 5qWwQmX6F+iU+zJdLpvubCQBAgT2IiBg7qXS1kmAwOwCW7o8dolwOaVr2dWxbHctw58/8xu/cfeJ sOHPguXxp3Wo0ztVVX3tcIjamZAZxWQjAgQI7FZAwNxt6S2cAIFjBbYSMGOfFjulc3lM17LrPZah 4/ngl750eUlu/SFcHnsG3zs+tpNZyuXe8+rYGwECBAjECgiYsVK2I0CAQEtgCwHzvffeO3z1ySdH azs1XNbdxY9+//cv75nsuyR27HNhPz/xzW9eXg4rUI6W6egNhMyjCe2AAAECuxcQMHd/CgAgQCBV oPSAuVS4DEHw9tNPV3/9rW/1Bssx8zp4NruWAuaY2jyfD13iByMul9XJnMfbXggQILA1AQFzaxW1 HgIEVhGIvWexnkyO963FBOTYzmXXey2ndi1DqKw/wr2Wh2eeuftn4XKV0/ryIFPuyXz3xo3q6tWr fpZYrzyORIAAgewF/KOQfYlMkACBHAWmPEE2x05PTLgM7uFVJGMfzfstxy6JHdpX3bVsPiFWsBzT X+7zsZfL5vifJ8up2DMBAgQIjAkImGNCPk+AAIEOgdiAFobm9oqS2O5rbLgMawxBcChcjr3XMuzD g3zy+1KLfU+mkJlf7cyIAAECpxIQME8l77gECBQtMCVg5nYZYczcx8LllFeQjBU6hM/6ktgQVO95 Au3YYJ9fXCAmZOb2nyiLozgAAQIECPQKCJhODgIECCQIxIS0erc5dXdi5v3R2dnoE1vrgHnn+vXJ D/Opu5n1PZf1U2LrTmhCOQxZWCAmZL708svV+fm5nysWroXdEyBAIHcB/xDkXiHzI0AgO4HYp6/m FjBj7ht96+ysunblyuXU+zqJU8Nl36tIwt8Ll9md3r0TeuD27dHJ5vSfKaOTtQEBAgQILCIgYC7C aqcECGxZ4OLi4vDC889HLzGHH7pj7ruMeWLs1HDZRmp2L4XL6FMoiw099CeLMpgEAQIEshcQMLMv kQkSIJCbQExYa845h4AZc2ls7H2X4bLYH3vuucGy9HUtwyCdy9zO6Pj5xFwq637MeE9bEiBAYIsC AuYWq2pNBAgsKhAT1uoJ5PCKkpj5jt13eUznstm1bD8p1mtIFj1VF9l5TMjM4T9VFlm8nRIgQIDA qICAOUpkAwIECNwrEBPYcgmYMfeLNu+77Kp1+z2XfefDUNcyjNG53MZXUjgfHjwcBheTw3+sbEPb KggQIFCegIBZXs3MmACBEwtMCZinfkXJ2Fxj77vse8/l2KWwoWPpabEnPmEXOHxMyPRU2QXg7ZIA AQIFCAiYBRTJFAkQyEtgLLQ1Z3vKSwVj7hUduu+y+a7LH373u5dBMQTGqR86l1PF8t6+7mi7VDbv OpkdAQIETiUgYJ5K3nEJEChSIOaS01wC5lgQjrk0Nqzl7PXXqw+/8pXecPmb//2/V7/0t/7W3WU3 g2j4/Wd+4zeqwzPPXH7ePZdFnva9kx57dYku5rbqbTUECBCIERAwY5RsQ4AAgU8FYt4lmUPAHAuX Y5fGHvNQn3r9wuX2v2xiXl1y6svEt18FKyRAgEBeAgJmXvUwGwIEMhcoIWDGdFljnhobuo0f/vmf 93Yu25fMtjuX4XLaBz7/+bsV1b3M/OROnN7YpbIe+JMIaxgBAgQKFRAwCy2caRMgcBqBsc5ge1an uAdzbI4xl8b2PdQnRr0Omg9+6UtV8wm0MWNtU5ZA3ekee6qsLmZZdTVbAgQIHCMgYB6jZywBArsT GAtvpw6YMd3L+sE+XeGvDgxj910219nVyfyJb36zuvLGG+653MlXiC7mTgptmQQIEIgQEDAjkGxC gACBWmBKwDzFpYFj8xvqXk6577LvibLuu9zv18rYA39O0c3fbzWsnAABAqcTEDBPZ+/IBAgUJhDT HWwu6fzZr1cvvfLqat9nx+YX+2Cfoe7lULAMaw/3Xbo0trATe6bpjj3w5xT/4TLT0uyGAAECBCYI rPaDz4Q52ZQAAQJZClxcXBxeeP756Lmt/YqGse7l0IN9pnQv+wAuu5ef/az7LqPPkO1tqIu5vZpa EQECBKYKCJhTxWxPgMBuBXJ+guyU7mXfvZf1g31iC9z3vktPi40V3N52upjbq6kVESBAYKqAgDlV zPYECOxWYKxD2IZZ856zsbnFdC9TLo0Na66DZv1KEgFzt18ilwvXxdx3/a2eAAECAqZzgAABApEC YyHuVAFzrHsZ5hWeHDv01Ng7169Xf/2tb/W+87K5Nk+NjTxhdrqZLuZOC2/ZBAgQ+FRAwHQqECBA IFIg14A5Nq+YJ8f2dS/7HupTk3lqbOTJs7PNdDF3VnDLJUCAQENAwHQ6ECBAIFJgLMidqoM5NK+Y J8fGdC+7gmb4u/DhwT6RJ9CONhvrYq79hOUd0VsqAQIETi4gYJ68BCZAgEAJAi889+zh4tXXoqe6 1g/QY5fHxnQvf/jd7969j3JsgX0P9gnj3Hs5prevz+ti7qveVkuAAIFaQMB0LhAgQCBCYOoTZNd6 RclYVzXce9n3Ud+T+eGf//l9917GXBrrnZcRJ86ON/na7dvV2wPrX/MhWDsug6UTIEBgdQEBc3Vy ByRAoESBsSDXXtO7N25UV69eXfx7bOrlsXcf+HNxUX34la9EPdynuUb3XpZ4Fq8753COPXg49B50 rS7/uqt2NAIECBBY/IcfxAQIENiCwNSAuUZ3Zuyy3bFXk4RLWru6l+0gGTqV4aPZ1Xzwc5+rqh// 8c4n026h3tYwj4DLZOdxtBcCBAiUJCBgllQtcyVA4GQCOQbMsTn1BcyY7uXQJbK6lyc7DYs6cDjP 3qmq6msDXcy1Ov1FwZksAQIEChcQMAsvoOkTILCOwFiYa89ijQ7m0JzGHu7T1b0cu++y2cV88Etf 0r1c59Qr+ihjl8k++thj1c2bN/0sUnSVTZ4AAQL3Cvim7owgQIDAiMDYk1q7hi8dMMceOjT0cJ8w 39tPP1399be+FXXvpSfH+hJJFQgB858dDh72kwpoHAECBAoUEDALLJopEyCwrsDUgLlGV+aYh/sE vbPXX68++uIX74Mc62K693Ldc28LRxt7J+bS/xmzBUNrIECAQEkCAmZJ1TJXAgROIjD2MJ32pNZ4 RUnq5bH1XNsP9+kLlu3upVeTnOQULP6gQw/7WeM/ZIoHtAACBAgUJCBgFlQsUyVA4DQCud1/eXFx cXjh+ed7MUafHvv666OvJukKnOHvfuKb36yuvPFGFe7h9EEgVmDonZgCZqyi7QgQIFCGgIBZRp3M kgCBEwpMCZhr/LCcev9luB8ufPzwu9+9R3Pssth648unx372sx7uc8JzscRDxzxN1mWyJVbWnAkQ INAtIGA6MwgQIDAiEO7BfPHFF6OcvvGNb1RXr15d9HvrMfdf3rl+ffDhPu2wWf+5/vWBz39e9zLq TLBRU2DsPkyvK3G+ECBAYDsCi/4QtB0mKyFAgEA+Aqn3X4ZOUjNgxnYuw8rDw31u/6//q8tj8zkN ipvJ0H2Y589+vXrplVf9TFJcVU2YAAEC9wv4Zu6sIECAQEECYw8c6ns9SfPy2L77K8MDfMJHX/D0 7suCTpQMpzp0H2aYrstkMyyaKREgQCBBQMBMQDOEAAECpxI49v7Lv/z+96Pefdlc3+WrSX7916vD M89c/rUH/Jyq+uUe132Y5dbOzAkQIDBVQMCcKmZ7AgQInFBg7vsvu15DUi+vef+lp8eesOgbOXQI mQ8eDr2rcR/mRgptGQQI7F5AwNz9KQCAAIGSBFLuv6wvjz2LeD1Jl8VlB/PHf9zTY0s6UTKdq/sw My2MaREgQGBGAQFzRky7IkCAwNICQwEz5v7L5vxi78UMAfPwwAMujV26uDvYv/dh7qDIlkiAwO4F BMzdnwIACBAoRSC8LuWrTz7ZO92+gFkP+Oj3f7/3AT5jT5T1epJSzpK85+lBP3nXx+wIECAwh4CA OYeifRAgQGAFgYuLi8MLzz8/OWC2X08SdjAUKJuf83qSFQq7o0OMvQ/Tk2R3dDJYKgECmxUQMDdb WgsjQGBrAikP+Knvv2y+/7LPpSt0uv9ya2fRadcjYJ7W39EJECCwhoCAuYayYxAgQGAGgZQH/ITD dnUwh7qYIVR+9L3v3e1yev/lDMWzi0uBsSfJ6mA6UQgQIFC+gIBZfg2tgACBnQikBsxLnouL6sOv fKX3HZhDl8yG+y/Dh/df7uREW3iZQ0+S9aqShfHtngABAisICJgrIDsEAQIE5hAYCpgfnZ11BsD6 Etkffve790yhL1DW3cuw8eXlsb/+69XhmWeEyzkKaB+XAkMB86WXX67Oz8/9bOJcIUCAQMECvokX XDxTJ0BgXwLHBMyzH/7w8rLXro9mqGx+PoTQz/zGbwiY+zrNFl/t0JNkz5/9evXSK6/62WTxKjgA AQIElhPwTXw5W3smQIDAbALHPEH28tLW//k/7wbMsVeS1JP2BNnZymdHDQGvKnE6ECBAYNsCAua2 62t1BAhsROCF5549XLz6Wu9qxt6B2QyY9U7aryOpO5x1R9MTZDdy8mS2DE+SzawgpkOAAIGZBQTM mUHtjgABAksIDAXMa1VVvfXQQ72Hvf3009VD/+7f9V4i2zcwBMzDAw+4/3KJgu54nwLmjotv6QQI 7EJAwNxFmS2SAIHSBVICZvMdmM2A2X6QT/PezPbnBMzSz5z85i9g5lcTMyJAgMCcAgLmnJr2RYAA gYUEHn/88cP7t2517n2og1m/A3Osg9l+0E/95/CKEq8nWaioO92tgLnTwls2AQK7ERAwd1NqCyVA oGSB1IAZ1hwukf3rb33rnndgjj3oxxNkSz5b8p173VV/8HDoneTHh4OfTfItoZkRIEBgVMA38VEi GxAgQOD0AkOvKBnrYJ69/nr14Ve+ck/ADCsau1TWOzBPX/ctzkAHc4tVtSYCBAj8SEDAdDYQIECg AIGUgFl3i9oBs++9l02G0MH8iW9+s7ryxhsukS3g/ChpiuG81MEsqWLmSoAAgWkCAuY0L1sTIEDg JAJDAfOts7PqWnjXZcdH+GG+r4PZ7mI2h3sH5knKvIuDCpi7KLNFEiCwYwEBc8fFt3QCBMoRSA2Y lyu8uOi8RLYdKJtPk728B/Ozn60uA2pPeC1Hz0xzEXAPZi6VMA8CBAgsJyBgLmdrzwQIEJhNIDVg tjuY9cN9Yi6TffBLXxIwZ6ugHdUCOpjOBQIECGxbQMDcdn2tjgCBjQikBszYDmYXk4C5kZMno2Xo YGZUDFMhQIDAQgIC5kKwdkuAAIE5BVIDZtc9mDHdyzB3AXPOCtqXDqZzgAABAvsQEDD3UWerJECg cIHUgJnawXzwO9+pDs88czncPZiFnzwZTV8HM6NimAoBAgQWEhAwF4K1WwIECMwpkBowx54i2zfH OmAKl3NW0b6CgHswnQcECBDYtoCAue36Wh0BAhsRSA2Y7Q5m9OWxn3YwBcyNnECZLEMHM5NCmAYB AgQWFBAwF8S1awIECMwlkBowUzuYH7/ySnXljTdcHjtXAe3nroAOppOBAAEC2xYQMLddX6sjQGAj AqkBs93BjOUQMGOlbDdFQAdzipZtCRAgUKaAgFlm3cyaAIGdCaQGzNQOpifI7uwEW3G5OpgrYjsU AQIETiAgYJ4A3SEJECAwVSA1YNYdzI+++MXoQ/6Pn/zJ6jOf/ezlw1jcgxnNZsMIAR3MCCSbECBA oHABAbPwApo+AQL7EEgNmHUHcyxghqfGNrd54POfv4QVMPdxfq25Sh3MNbUdiwABAusLCJjrmzsi AQIEJgukBsyUDmYYI2BOLpEBEQI6mBFINiFAgEDhAgJm4QU0fQIE9iGQGjCHOph117Lr18Mzz+hg 7uPUWn2VOpirkzsgAQIEVhUQMFfldjACBAikCaQGzJQOZgicAmZanYwaFtDBdIYQIEBg+wIC5vZr bIUECGxAIDVg9nUwm/dctjuY9StKApt7MDdw8mS2BB3MzApiOgQIEJhZQMCcGdTuCBAgsITA3AGz nmP74T7h770Dc4kK2mcQ0MF0HhAgQGD7AgLm9mtshQQIbEBgzoDZDpV9HUzdyw2cOBkuQQczw6KY EgECBGYUEDBnxLQrAgQILCUwZ8Ds6l7ec8nsl77kHZhLFXLn+9XB3PkJYPkECOxCQMDcRZktkgCB 0gXmCph93cvgc7eTKWCWfrpkPX8dzKzLY3IECBA4WkDAPJrQDggQILC8wFwBs929bF8eexk0Bczl C7rTI+hg7rTwlk2AwK4EBMxdldtiCRAoVWCugDn09NjwucuP83OXyJZ6ohQwbx3MAopkigQIEDhC QMA8As9QAgQIrCUwV8Ac62DW78D0gJ+1Kruv4+hg7qveVkuAwD4FBMx91t2qCRAoTGCugNl5Sex3 vlN99MUvXt6DGT4Ozzzj/ZeFnR8lTVcHs6RqmSsBAgSmCwiY082MIECAwOoCcwTMdrisA2UdLutf BczVy7ubA+pg7qbUFkqAwI4FBMwdF9/SCRAoR2COgFmvdihofvzKK9WVN97QwSzn1ChupjqYxZXM hAkQIDBJQMCcxGVjAgQInEZgroDZFS6bHUwB8zT13ctRdTD3UmnrJEBgzwIC5p6rb+0ECBQjMEfA HAqXAeIyaHpFSTHnRKkT1cEstXLmTYAAgTgBATPOyVYECBA4qUBKwKy7RWevv373IT7t+y3vBsv6 QT8C5knrvPWD62BuvcLWR4AAgaoSMJ0FBAgQKEAgJWCGZYUf6LsCZjNY1svXwSzgRNjAFHUwN1BE SyBAgMCAgIDp9CBAgEABAikBs6uD2Q6W93U0z88/CaVXrhSgYoqlCehgllYx8yVAgMB0AQFzupkR BAgQWF0gJWAOdTCb7728ZzEC5uq13dsBdTD3VnHrJUBgbwIC5t4qbr0ECBQpkBIwh+7BbCI0w2Z4 B2b40MEs8jTJftI6mNmXyAQJECBw9ADNyQAAIABJREFUtICAeTShHRAgQGB5gZSA2dXBDH/X96Af ryhZvo6O8Ml9wQ8eDr0UHx8OfjZxohAgQKBgAd/ECy6eqRMgsB+BlIAZ28GsFW9/8EF15Y03dC/3 c1qtvlIdzNXJHZAAAQKrCwiYq5M7IAECBKYLpATMvg5ms4tZzyR0NXUwp9fFiOkCOpjTzYwgQIBA SQICZknVMlcCBHYrkBIwYzqY9zzsxwN+dnt+rbVwHcy1pB2HAAECpxMQME9n78gECBCIFkgJmEMd zPaBvQMzuhQ2PFJAB/NIQMMJECCQuYCAmXmBTI8AAQJBICVg9r0HM+yv8zUlOphOtoUFdDAXBrZ7 AgQIZCAgYGZQBFMgQIDAmMCxAbPef+/7L8MG5+dj0/B5AkcL6GAeTWgHBAgQyFpAwMy6PCZHgACB TwSODZiDwfJT5PAOTO+/dMYtKaCDuaSufRMgQCAPAQEzjzqYBQECBAYFUgJm2GH4gf7s9dfjdDfa wQwG78QJ2GoBgaeq6p7/uNDBXADZLgkQIJCRgICZUTFMhQABAn0CKQFz6B7M9nG2+g7MOlx+7XBw cp1I4KOzs7sBUwfzREVwWAIECKwoIGCuiO1QBAgQSBVICZh1B/PO9evVQ488MnjorQbMsOi379yp BMzUM+/4cc2AWZ+TDw4E/o8PBz+bHM9uDwQIEDiZgG/iJ6N3YAIECMQLpAbMcITbTz8tYH4aaM6f /Xr1K7/6a9Uf/MEfxOPbcpLAF77wheratWvV+7duXY7TwZzEZ2MCBAgULyBgFl9CCyBAYA8CSwfM 8ATZy/s1r1zZHGezg/nujRvVE088sbk15ragL3/5y50BM8zTPZi5Vct8CBAgMK+AgDmvp70RIEBg EQEBM51VwEy3Sx3ZFzDdg5kqahwBAgTKERAwy6mVmRIgsGMBATO9+AJmul3qSB3MVDnjCBAgUL6A gFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBgyYAZniD70JtvugdzB+fR WkvUwVxL2nEIECCQn4CAmV9NzIgAAQL3CcwdMC9D5aevLqlfURIO6iE/Tr45BHQw51C0DwIECJQp IGCWWTezJkBgZwJzB8wm35bfgRnW6RLZ9b9YdDDXN3dEAgQI5CIgYOZSCfMgQIDAgMBcAbPduQxd TAHTqTe3gA7m3KL2R4AAgXIEBMxyamWmBAjsWCA1YIbXQty5fv3u5bA1Ydclslu8PFYH8zRfNDqY p3F3VAIECOQgIGDmUAVzIECAwIhAasAMu7399NP33G9Z33t595Dn55t9wI+AeZovLR3M07g7KgEC BHIQEDBzqII5ECBAYKGAGdPBrARM59/MAjqYM4PaHQECBAoSEDALKpapEiCwX4G5O5jNS2QFzP2e V0utXAdzKVn7JUCAQP4CAmb+NTJDAgQIVKkBM6aDeXjmmUth92A60eYS0MGcS9J+CBAgUJ6AgFle zcyYAIEdCqQGzEBV34NZdy3bv4aAudVwGdbvNSXrf8HoYK5v7ogECBDIRUDAzKUS5kGAAIEBgdSA 2e5g3nNp7KfHEzCdenML6GDOLWp/BAgQKEdAwCynVmZKgMCOBVID5lgHM3z+yhtv6GDu+NxaYuk6 mEuo2icBAgTKEBAwy6iTWRIgsHOB1IDZ18FsXiYrYO785Fpg+TqYC6DaJQECBAoREDALKZRpEiCw b4HUgNnsYF7+/oMPLt+JKWDu+3xaevU6mEsL2z8BAgTyFRAw862NmREgQOCuQGrAbHYw2+Hycucb fwdmWKKH/Kz/haSDub65IxIgQCAXAQEzl0qYBwECBAYE5giYXR1MAdNpt4SADuYSqvZJgACBMgQE zDLqZJYECOxcYI6A2fmakjffrEKX02tKdn6Czbx8HcyZQe2OAAECBQkImAUVy1QJENivwNwBM0iG ezF1MPd7Ti25ch3MJXXtmwABAnkLCJh518fsCBAgcCkwd8C8+6AfHUxn2AICOpgLoNolAQIEChEQ MAsplGkSILBvgbkCZt25DJfLho/wipLw4RLZfZ9fc69eB3NuUfsjQIBAOQICZjm1MlMCBHYsMFfA rDuXNeVDb765eVVPkV2/xDqY65s7IgECBHIREDBzqYR5ECBAYEBgroB5N1h++i7M0MHccvcyrFfA XP9LSwdzfXNHJECAQC4CAmYulTAPAgQIrBAw2x1MAdNpt4SADuYSqvZJgACBMgQEzDLqZJYECOxc YIkO5h6eIKuDeZovHB3M07g7KgECBHIQEDBzqII5ECBAYETg2IBZ777ZwQz3X279HZgC5mm+tHQw T+PuqAQIEMhBQMDMoQrmQIAAgQUCZgiP4ePO9euX77ysnxwb/m4v78AUME/zpaWDeRp3RyVAgEAO AgJmDlUwBwIECCwQMMMuQ8gMAbP9EQLm4ZlnNv+AHwHzNF9aOpincXdUAgQI5CAgYOZQBXMgQIDA AgGz2cGsu5bNLuYeHvAjYJ7mS0sH8zTujkqAAIEcBATMHKpgDgQIEFggYA51MC8D5w7egSlgnuZL SwfzNO6OSoAAgRwEBMwcqmAOBAgQWCBgDt2DKWA65ZYU0MFcUte+CRAgkLeAgJl3fcyOAAEClwJz PUX2Mlh++sCfcIls+Di7cmXTym/fuVN97XC4XOO7N25UTzzxxKbXm8PidDBzqII5ECBA4DQCAuZp 3B2VAAECkwRSAmb7Hsz2Ad2DOakENp4goIM5AcumBAgQ2JiAgLmxgloOAQLbFEgJmEGi9ymyO3kH ZjDQwVz/a0IHc31zRyRAgEAuAgJmLpUwDwIECAwIpATMvg7mnt6BKWCe5stKB/M07o5KgACBHAQE zByqYA4ECBAYEUgJmDqYn6DqYK7/5aWDub65IxIgQCAXAQEzl0qYBwECBBbsYNYP9qkPEV5REjqc W3/Aj4B5mi8rHczTuDsqAQIEchAQMHOogjkQIEBggQ7m0EN+BEyn3JICOphL6to3AQIE8hYQMPOu j9kRIEDgUiDlElkB85OTxyWy638R6WCub+6IBAgQyEVAwMylEuZBgACBAYGUgBl2136KbH2pbOhg 7uVDwFy/0jqY65s7IgECBHIREDBzqYR5ECBAYOaAOdTB3Ms7MHUwT/NlpYN5GndHJUCAQA4CAmYO VTAHAgQIjAjM1cGsDyNgOuWWFNDBXFLXvgkQIJC3gICZd33MjgABApcCKQGzq4O5t3dg6mCe5gtI B/M07o5KgACBHAQEzByqYA4ECBBYsYO5pyfICpin+dLSwTyNu6MSIEAgBwEBM4cqmAMBAgQWCJg6 mJ+gesjP+l9eOpjrmzsiAQIEchEQMHOphHkQIEBgQCDlEtmwu66nyB6eeebySGdXruzCXMBcv8w6 mOubOyIBAgRyERAwc6mEeRAgQGDmgNn3FNnwgB8B0+m2pIAO5pK69k2AAIG8BQTMvOtjdgQIELgU mKuDGfa1p3dghvXqYK7/RaSDub65IxIgQCAXAQEzl0qYBwECBBbsYIanx97+4IPLI+zpFSUC5mm+ rHQwT+PuqAQIEMhBQMDMoQrmQIAAgRGBOTuYAuYTzreFBXQwFwa2ewIECGQsIGBmXBxTI0CAQC2Q EjCH7sHcywN+dDBP8zWkg3kad0clQIBADgICZg5VMAcCBAjoYC52DrgHczHa3h3rYK5v7ogECBDI RUDAzKUS5kGAAIEBgWM7mPU9mOEBP6GzqYPpdFtSQAdzSV37JkCAQN4CAmbe9TE7AgQIXAocGzBr RgHzRvXEE+7BXPrLSgdzaWH7J0CAQL4CAma+tTEzAgQI3BUQMNNPBpfIptuljtTBTJUzjgABAuUL CJjl19AKCBDYgUBKwAws4XLYO9ev3xXSwdTBXOPLRQdzDWXHIECAQJ4CAmaedTErAgQI3COQEjCb T5F1D+bh0vPdGwLmGl9aOphrKDsGAQIE8hQQMPOsi1kRIEDg6IDZ18HcG61LZNevuA7m+uaOSIAA gVwEBMxcKmEeBAgQGBA4toNZ7/rKG2/s6gmyYd0C5vpfWjqY65s7IgECBHIREDBzqYR5ECBAYOaA 2dXBFDBdIrvGF5oO5hrKjkGAAIE8BQTMPOtiVgQIELhHQAcz/YTQwUy3Sx2pg5kqZxwBAgTKFxAw y6+hFRAgsAOBlIDZ7GCGh/xU5+eXT5U9u3JlB2I/WqKAuX65dTDXN3dEAgQI5CIgYOZSCfMgQIDA gEBKwGw/RVbA9BTZtb7IdDDXknYcAgQI5CcgYOZXEzMiQIDAfQIpAVMH8xNGHcz1v6B0MNc3d0QC BAjkIiBg5lIJ8yBAgMCCHcyw64fefNMlst6DucrXmQ7mKswOQoAAgSwFBMwsy2JSBAgQuFfg2A6m gHm4BH1XwFzlS0sHcxVmByFAgECWAgJmlmUxKQIECBwfMJv3YAqYAuaaX1M6mGtqOxYBAgTyEhAw 86qH2RAgQKBTYI4O5h7fgRkw3YO5/heVDub65o5IgACBXAQEzFwqYR4ECBAYEEgJmO0OpoDpEtm1 vsh0MNeSdhwCBAjkJyBg5lcTMyJAgMB9AikBM+wkhMw7169f7k/AFDDX+tLSwVxL2nEIECCQn4CA mV9NzIgAAQKzB8y9PkE2QLpEdv0vKB3M9c0dkQABArkICJi5VMI8CBAgMCBwbAdTwPSQnzW/wHQw 19R2LAIECOQlIGDmVQ+zIUCAQKeAgJl+YuhgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5k ATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4Ejg2Ye33ATzg1BMz1v0B0MNc3d0QCBAjkIiBg5lIJ 8yBAgMCAgICZfnoImOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiCQEjCb78HUwfSQnzW/THQw19R2 LAIECOQlIGDmVQ+zIUCAQKdASsAMO6rfgylgCphrfmnpYK6p7VgECBDIS0DAzKseZkOAAIFZA2a9 sxA0z65c2aWuS2TXL7sO5vrmjkiAAIFcBATMXCphHgQIEBgQSO1gCpge8nOKLywdzFOoOyYBAgTy EBAw86iDWRAgQGBQ4NiAuWdeHcz1q6+Dub65IxIgQCAXAQEzl0qYBwECBBbsYO4ZV8Bcv/o6mOub OyIBAgRyERAwc6mEeRAgQEDAXOQcEDAXYR3cqQ7m+uaOSIAAgVwEBMxcKmEeBAgQEDAXOQcEzEVY kwJmGBQeOPXg4ZOn+nZ9fHw4+Nlk/ZI5IgECBGYT8E18Nko7IkCAwHIC7sFMtxUw0+1SR+pgpsoZ R4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzrSPZipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptul jtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0 IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAg sAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM 8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEg QKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQO ZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nA TLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQvoCAWX4NrYAAgR0I CJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNVzjgCBAiULyBgll9D KyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbbpY7UwUyVM44AAQLl CwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM9CILmOl2qSN1MFPl jCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQILADAQEzvcgCZrpd 6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUEzPJraAUECOxAQMBM L7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYRIECgfAEBs/waWgEB AjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DA LL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwB AgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfq YKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEF zHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiB gICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1 tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQ voCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNV zjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbb pY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM 9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQ ILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUE zPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYR IECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWk DmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZ wEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEd CAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZf QysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC 5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT 5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6 XepIHcxUOeMIECBQvoCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDA TC+ygJlulzpSBzNVzjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloB AQI7EBAw04ssYKbbpY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9A wCy/hlZAgMAOBATM9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmcc AQIEyhcQMMuvoRUQILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH 6mCmyhlHgACB8gUEzPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqR Bcx0u9SROpipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDY gYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5 NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAg UL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6Ugcz Vc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm 26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQE zPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EV ECCwAwEBM73IzYAZ9vLoY49d7uz9W7cuf+/XeR1q27piH52dVWdXrlz+8XDnzuWvDx4OvQX9+HDw s0n66W4kAQIETi7gm/jJS2ACBAgQGBcQMMeN+rZoB8z0PRmZItAMmHXIFDBTJI0hQIBAGQICZhl1 MksCBHYuIGCmnwACZrrdHCN1MOdQtA8CBAiUIyBgllMrMyVAYMcCAuaOi5/J0sPlrXeuX68eeuSR yxnd/uCDy9/Xv9Z/d+WNN+5eEts19bAfHcxMimoaBAgQWEBAwFwA1S4JECAwt4CAObeo/U0RqO+d DAFz7GMoYLoHc0zP5wkQIFC+gIBZfg2tgACBHQgImDsocsZLnCtghiXqYGZcaFMjQIDADAIC5gyI dkGAAIGlBQTMpYXtf0gghMLwJNjbTz89CqWDOUpkAwIECGxaQMDcdHktjgCBrQgImFupZJnr6AqY 9f2X7RW5B7PMGps1AQIE5hIQMOeStB8CBAgsKCBgLohr19ECOpjRVDYkQIDAbgUEzN2W3sIJEChJ QMAsqVrbm2vMJbKXT5c9Px9dvHswR4lsQIAAgaIFBMyiy2fyBAjsRUDA3Eul815n6GA2L43tukz2 oTff7F2Ep8jmXV+zI0CAwBwCAuYcivZBgACBhQUEzIWB7X5UIOby2LAT92COUtqAAAECmxYQMDdd XosjQGArAgLmVipZ3jqmvKJkLGDqYJZXfzMmQIDAVAEBc6qY7QkQIHACAQHzBOgOeSnQFzA9RdYJ QoAAAQJdAgKm84IAAQIFCAiYBRRpw1MMIfPO9etRK/QezCgmGxEgQGCzAgLmZktrYQQIbElAwNxS NctaS7uD2flgn0ceqW5/8MHlwtyDWVZ9zZYAAQJzCwiYc4vaHwECBBYQEDAXQLXLSQJzPOTHPZiT yG1MgACBIgUEzCLLZtIECOxNQMDcW8XzWW/MOzCbs9XBzKd2ZkKAAIFTCAiYp1B3TAIECEwUEDAn gtl8doGuDmbX5bLuwZyd3g4JECBQlICAWVS5TJYAgb0KCJh7rXw+664DZt/TY+uZ6mDmUzMzIUCA wCkEBMxTqDsmAQIEJgoImBPBbD67gHswZye1QwIECGxSQMDcZFktigCBrQkImFuraFnriQ2XYVU6 mGXV1mwJECAwt4CAObeo/REgQGABAQFzAVS7HBVovqKk79LY9t+7B3OU1QYECBDYtICAuenyWhwB AlsREDC3Usny1hFC5p3r1++Z+NB9mDqY5dXYjAkQIDCngIA5p6Z9ESBAYCEBAXMhWLsdFGh2MGOp dDBjpWxHgACBbQoImNusq1URILAxAQFzYwUtZDlzB8yw7LDPBw+HXoGPDwc/mxRyfpgmAQIEugR8 E3deECBAoAABAbOAIm1wiiEMnl25UsW+oiQQ6GBu8ESwJAIECEwQEDAnYNmUAAECpxIQME8lv+/j tgNmU6PvPkz3YO77nLF6AgQICJjOAQIECBQgIGAWUKQNT3Gu15TUl9y6RHbDJ4ulESCwewEBc/en AAACBEoQEDBLqNJ25zhXwAxC7sHc7nliZQQIEAgCAqbzgAABAgUICJgFFGmDU2y/oqR9WWzXZbLu wdzgiWBJBAgQmCAgYE7AsikBAgROJSBgnkp+38ftegfmmIh7MMeEfJ4AAQLbFhAwt11fqyNAYCMC AuZGClnQMlJeURKWp4NZUJFNlQABAgsICJgLoNolAQIE5hYQMOcWtb8xgaGA2fcE2bGAGT7vHswx eZ8nQIBA2QICZtn1M3sCBHYiIGDupNAZLjM84GcoULanrIOZYRFNiQABAisKCJgrYjsUAQIEUgUE zFQ541IFht6BObRP92CmihtHgACBbQgImNuoo1UQILBxAQFz4wXOeHlDryjxFNmMC2dqBAgQOJGA gHkieIclQIDAFAEBc4qWbecUaAbMmEtldTDn1LcvAgQIlCcgYJZXMzMmQGCHAgLmDoueyZKHOphd U3QPZiaFMw0CBAicSEDAPBG8wxIgQGCKgIA5Rcu2cwlMDZfhuDqYc+nbDwECBMoUEDDLrJtZEyCw MwEBc2cFP/Fym68oibkstjl2c3t8AAAeAUlEQVRdHcwTF8/hCRAgcGIBAfPEBXB4AgQIxAgImDFK tplLYOwdmO/fuFE9+uST1e0PPrjnkN/5y7+s/u5v/mZ1duVK71S8B3OuKtkPAQIE8hQQMPOsi1kR IEDgHgEB0wmxtkAIgneuX598WB3MyWQGECBAYFMCAuamymkxBAhsVUDA3Gpl813XEgEzrFYHM9+a mxkBAgTmEBAw51C0DwIECCwsIGAuDGz39wjUl8ievf765WWwU+7D1MF0MhEgQGDfAgLmvutv9QQI FCIgYBZSqI1MMwTMcB/l1KfIugdzIyeAZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOM0BaYGzDBWB9M5 RIAAgX0LCJj7rr/VEyBQiICAWUihNjbNuQNm4HEP5sZOEsshQIBAS0DAdEoQIECgAAEBs4AibWSK x7wDUwdzIyeBZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOPUXcaUV5S4B9P5Q4AAAQICpnOAAAECBQgI mAUUaSNTbHYwU5bkHswUNWMIECCwHQEBczu1tBICBDYsIGBuuLiZLe2YgKmDmVkxTYcAAQInEBAw T4DukAQIEJgqIGBOFbN9qkDqK0rq4+lgpsobR4AAgW0ICJjbqKNVECCwcQEBc+MFzmh5XQHzoUce qW5/8MHoLHUwR4lsQIAAgc0LCJibL7EFEiCwBQEBcwtVLGsNzVeUhOD4xZ/6qd4FND+vg1lWnc2W AAECcwsImHOL2h8BAgQWEBAwF0C1y04BHUwnBgECBAgcIyBgHqNnLAECBFYSEDBXgnaYuwLNDmaT JXQrH33yyer9GzfudjV1MJ04BAgQIFALCJjOBQIECBQgIGAWUKQNTbEvXHYtsRku3YO5oZPAUggQ IJAoIGAmwhlGgACBNQWGAua1qqreeuihNafjWBsVmPqKknY386E336zqS2y7iOr9P3g49Ap+fDj4 2WSj55dlESCwDwHfxPdRZ6skQKBwgccff/zw/q1bnasQMAsvbkbTHwuYdbey3bWsHwAU08F8+86d 6msCZkZVNxUCBAjMKyBgzutpbwQIEFhEQMBchNVOOwRCyLxz/fqgTXhtSdc9mGMdzLBTAdNpR4AA gW0LCJjbrq/VESCwEQEBcyOFLGAZXQGzq2PZ7maGP4ePv/ubv1mdXbnSu1IBs4CTwBQJECBwhICA eQSeoQQIEFhLQMBcS9pxgkD9kJ//63Ofq/63733vPpT2ezHrP+tgOn8IECBAQMB0DhAgQKAAgRee e/Zw8eprvTP9oYf8FFDF/KfY9Q7MMOu+ey/D58L9lwJm/rU1QwIECKwlIGCuJe04BAgQOEJAwDwC z9DJAmOvKem7ZPbR3/mdwafIhol87fbt6u2BGXmK7ORyGUCAAIGsBATMrMphMgQIEOgWuLi4OLzw /PO9PDqYzpy5BJrhsus+y2bHsvn5cPwQMMc+BMwxIZ8nQIBA2QICZtn1M3sCBHYkMPQuzI/OzgYf rLIjJktNFBh7RUnYbd+DferQGRMwH7h9u3eG589+vXrplVf9bJJYQ8MIECCQg4Bv4jlUwRwIECAQ ISBgRiDZJFmgef/llM5lHS7DgY8NmO/euFFdvXrVzybJVTSQAAECpxfwTfz0NTADAgQIRAkMBcy3 zs6qawOvhog6gI12LRD7gJ++LmbM/ZcBeKiD6f7LXZ+CFk+AwEYEBMyNFNIyCBDYvsBQwLxWVdVb niS7/ZNgwRUOdTDDYccujw3bjL0DMxzjwcOhdxUC5oIFtmsCBAisJCBgrgTtMAQIEDhWYOhJsgLm sbrG1wLtJ8jGBMswNqaD+fadO9XXBEwnGwECBDYtIGBuurwWR4DAlgQ8SXZL1cxzLSFcDt1/GWbd vOey+eeYgOkJsnnW3awIECAwp4CAOaemfREgQGBBgffee+/w1Sef7D2CV5UsiL/xXXfdfzk1aMY8 4GcoYD762GPVzZs3/Vyy8XPN8ggQ2L6Ab+Tbr7EVEiCwIQFPkt1QMTNaytgTZNudy3YXM3x+7P7L sI1XlGRUdFMhQIDAQgIC5kKwdkuAAIElBDxJdglV+wwB87/80i/dvfy1DpB9v7bFYi6PHXvAj1eU OA8JECCwDQEBcxt1tAoCBHYi4EmyOyn0issMwS983Ll+vfdJse3ptDuYMQHTA35WLKpDESBA4IQC AuYJ8R2aAAECUwUef/zxw/u3bvUOcx/mVFHbt++/HLr3suvS2CB47P2XYR9eUeJcJECAwDYEBMxt 1NEqCBDYkYD7MHdU7BWW2nf/ZX3ovlBZfz4mXIZth+6/9ICfFQrtEAQIEFhJQMBcCdphCBAgMJeA +zDnkrSfsctjx8Jl3b2sQ2qfqPsvnWsECBDYj4CAuZ9aWykBAhsRcB/mRgqZwTKGupfNcDkUNGM6 mO6/zKDYpkCAAIGVBATMlaAdhgABAnMJvPDcs4eLV1/r3Z37MOeS3vZ+6u5leHps+Gg/MTZ29TGv Jxl6/2U4jvsvY7VtR4AAgfwFBMz8a2SGBAgQuEfgvffeO3z1ySd7Vd46O6uuXblCjcCoQAiZ9dNj mxvHXBobto95emzYzv2Xo6WwAQECBDYjIGBuppQWQoDAngTch7mnai+71vf//t8f7F4Ohc2Y7qX7 L5etn70TIEAgNwEBM7eKmA8BAgQiBIYCZhjuMtkIxJ1vEoJfuDy2HSDn7l66PHbnJ5rlEyCwOwEB c3clt2ACBLYgMHYf5kdnZ9WZy2S3UOpF1xC6l/VHbLCst5/j8tiwL/dfLlpiOydAgMDqAgLm6uQO SIAAgXkEPE12Hsc97qXdvUzpYsZcHjv29NjzZ79evfTKq34W2eNJaM0ECGxWwDf1zZbWwggQ2LqA y2S3XuFl1le/mqTZvZx6pBAuw8dYl9zlsVNlbU+AAIHyBQTM8mtoBQQI7FTg4uLi8MLzz/eu3tNk d3pijCx7rHsZoxbTvQz7GXp6bPi8y2NjtG1DgACBsgQEzLLqZbYECBC4R8Blsk6IFIGu7mXsPZix 3cuxy2PfvXGjunr1qp9DUgpoDAECBDIW8I094+KYGgECBMYEHn/88cP7t271buZpsmOC+/p83b0M q44NlLXQ93/zN6uf/qVfqmK7ly6P3de5ZbUECBCoBQRM5wIBAgQKF9DFLLyAK0//mHsvY58cO9a9 fPSxx6qbN2/6GWTl2jscAQIE1hDwzX0NZccgQIDAggIe9rMg7kZ2HTqX4SO893JK97LuWtYMc3Uv X3r55er8/NzPIBs5vyyDAAECTQHf3J0PBAgQKFxg7DJZD/spvMAzTv+Y7mXsvZchzD54OAzO2sN9 ZiyqXREgQCAzAQEzs4KYDgECBFIEdDFT1PYxJvW1JM3uZfj9tStXosDG7r3UvYxitBEBAgSKFRAw iy2diRMgQOBHAi889+zh4tXXekl0Mfd5trQvjQ0KYw/3aV8WG8bE3nupe7nP88yqCRAg0BQQMJ0P BAgQ2IiALuZGCjnjMlK7l2EKddAMvz5VVdVZRAdT93LG4tkVAQIEChUQMAstnGkTIECgLeBeTOdE U+CYcNncT2z3Mox54PbtwSK499I5SoAAge0LCJjbr7EVEiCwIwFdzB0Ve2CpXZfGjsk0O5bhfZfh I/bBPmFb3csxYZ8nQIDAPgQEzH3U2SoJENiJgC7mTgoducyYp8Z23XMZdj/l0lj3XkYWxGYECBDY gYCAuYMiWyIBAvsSGOtifnR2FnU/3b7UtrPa1Etj2x3M2HdeBrmxS2PPn/169dIrr/qZYzunmZUQ IECgV8A3eycHAQIENiYw1sW8VlXVWw89tLFVW04QmBou+7qXUy6NffvOnepr3nvpBCRAgACBTwUE TKcCAQIENigw1sX02pLtFT3lvstaodm9DH8X+9RY3cvtnUdWRIAAgWMFBMxjBY0nQIBAhgJj78UM U/6hLmaGlUubUkq47HqoTzh6eGps7MfYg33Cfjw5NlbTdgQIENiGgIC5jTpaBQECBO4TGOtiulR2 GydNHS7DeypTH+rz7z/7t6u/89q/uexcho+Yd17GPNjn3Rs3qqtXr/pZYxunmlUQIEAgSsA3/Sgm GxEgQKA8gffee+/w1SefHJy4S2XLq2vfjGPCZXNsu4M55X2XYT9jD/Z59LHHqps3b/o5YzunmJUQ IEAgSsA3/igmGxEgQKBMgbEuZliVS2XLrG2YdexDfZoP8+l6sM+Uh/qE43qwT7nnjJkTIEBgaQEB c2lh+ydAgMCJBcZCpktlT1yghMPXwTIMndq5DGNCyPyvX/+Xky+LjQ2XL738cnV+fu5njITaGkKA AIHSBXzzL72C5k+AAIERgYuLi8MLzz8/uJVLZcs5jVLvuezqXIa/m/LE2KA0dmls2MaDfco5n8yU AAECcwsImHOL2h8BAgQyFBjrYoYpf3R2FvVwlwyXt8sppXQuA1QdNKdeFhsbLj3YZ5eno0UTIEDg roCA6WQgQIDATgRiQqb7MfM9GaZ0LtsP8On689TOZcwrSVwam+/5Y2YECBBYS0DAXEvacQgQIHBi gZhLZd2PeeIi9Rw+5T2XXbsKryP517/+7+4+HCh2tTEP9Qn7cmlsrKjtCBAgsF0BAXO7tbUyAgQI 3Cfw+OOPH96/dWtQxv2YeZ04U8Nl1xNjj7ksNuZ9l8JlXueM2RAgQOCUAgLmKfUdmwABAicQiLlU Vsg8QWFah5xySWzXbOtQGbqW//TP/1uVcs9lbLh03+XpzxczIECAQC4CAmYulTAPAgQIrCggZK6I nXCoqeFy6D2X9ZNiwzTOrlyZNJuYJ8a673ISqY0JECCweQEBc/MltkACBAjcL/Dee+8dvvrkk6M0 niw7SjTrBnWwDDv9L7/0S5P23X4NSf3nR3/ndybfcxkOHPNQn7Cd+y4nlcnGBAgQ2LyAgLn5Elsg AQIEugVi7scMI4XMdc6gqV3LvlmFYPlfv/4vq7/z2r+5fMdl+FiicylcrnNeOAoBAgRKExAwS6uY +RIgQGBGgZhLZcPhvL5kRvSOXYVwWYfAqe+3bL+CJOw+tWsZxupcLltreydAgMDWBQTMrVfY+ggQ IDAiEBsydTLnP5VSu5bty2HDzMLDfELX8tqVK0mXxAqX89fXHgkQILBHAQFzj1W3ZgIECLQEhMx1 T4nUYNk1y/a9lmGbqZfECpfr1t/RCBAgsGUBAXPL1bU2AgQITBCIDZleYTIBtbVp8yE+IQTWl8N+ 8ad+qvrOX/5l1I67upcpryBpHiz2stjzZ79evfTKq352iKqUjQgQILBPAf9I7LPuVk2AAIH7BGKf LBsGCpnTTqBmxzKMnHqfZdfRmq8fCZ9P6VrGvucy7F+4nFZzWxMgQGCvAgLmXitv3QQIEOgQEDLn PS3GOpaxncuuV5CEey3DR/MBQVNmPyVcPvrYY9XNmzf9zDAF2LYECBDYqYB/LHZaeMsmQIBAn8CU kHktdDMfeghmQ6AvVIZNYgNlH2iza5nSsawDafj1wcMhqm7CZRSTjQgQIEDgUwEB06lAgAABAvcJ XFxcHF54/vkoGSHzky5i86O+v7IOlO1gOTVohmB5zNNhm3N7+86d6mvCZdS5bSMCBAgQmC4gYE43 M4IAAQK7EYh98E8A2dtrTNqdymAQ7q2cK1SG/dUdy9RuZftEjX2YTxjnnsvdfJlbKAECBGYVEDBn 5bQzAgQIbE9gSsjc8sN/2l3KUOm+TmWzQzm1Wxn2G54KO1eoDPubcr9l2P6ll1+uzs/P/YywvS9n KyJAgMDiAv7xWJzYAQgQIFC+wJSQWfols11Bsg6TdSVvP/30PUUNrxjp6lxODZf160bqnc8RMqdc EhuO++6NG9XVq1f9fFD+l60VECBA4CQC/gE5CbuDEiBAoDyBxx9//PD+rVvREz9lN7MvJMZMvh3q 2mEy7KN+Z2Xs5bBDQfPR3/mde+7hnCNU1uuccklsGPPx4eDngpiTxDYECBAg0CvgHxInBwECBAhE C0wNmafuZoZw+NCbb/aur/58V4hsD2p3Keug2Q6ZMV3LECrDR/P9mO13ZUYXpWPDqV1L4fIYbWMJ ECBAoCkgYDofCBAgQGCSwJTXmNQ7PmU3sx0eH3rkkctp3f7gg8tfw5/r33dB1MGyHSjDn4fCZPNz 7UAZxoZOZeo7LPsKlhIsPcxn0ulvYwIECBAYERAwnSIECBAgkCQw5b7M+gBrP2l2qCvY17XsCpzN kFmvpeu+yzpIhm26Xl0yd6BsFm7q5bBhrHCZdOobRIAAAQIDAgKm04MAAQIEkgVeeO7Zw8Wrr00a Hy6b/fbZ2axPSR2bQFew6wqAdTCceh/knJe3jq2l/fmUrmXYh/stp0rbngABAgRiBATMGCXbECBA gECvwMXFxeGF55+fLHTKy2brIFlPug6UqUFxyc5kH2wIljcOh+rtifKPPvZYdfPmTf/+T3SzOQEC BAjECfgHJs7JVgQIECAwIpByyWzYZQiaT316TyLkcYHUjmXYs1eQjPvaggABAgSOExAwj/MzmgAB AgQaAimXzNbDT3HpbEnFOyZY6lqWVGlzJUCAQNkCAmbZ9TN7AgQIZCkw9XUmzUUImj/SCJfevlNV 1dcOh+Q6n+pey9SOdvJCDSxa4FTnadFoJk8gUwEBM9PCmBYBAgRKF0h5nUl7zXu9fDb1/sqm36mf EBsCptBQ+lfxOvN3rqzj7CgE1hIQMNeSdhwCBAjsVCD1IUDtruaTZ2fVtStXNqsYupX/LOGhPV0g OQQ7oWGzp+rsC3OuzE5qhwROKiBgnpTfwQkQILAfgWMum+0Km6U/GKi+/DXlSbB9Z01OD/ERGvbz tX3sSp0rxwoaTyAvAQEzr3qYDQECBDYvMFfQrKHCPZuldDeXCJXBIadgWddFaNj8l/JsC3SuzEZp RwSyEBAwsyiDSRAgQGB/AnMHzXbgDB3O8FG/43Jt4TpMhuPO2aWs15H7k2GFhrXPuHKP51wpt3Zm TqBLQMB0XhAgQIDASQWOebVJ7MRDlzN8hE7n3JfWLh0k22vMPVjW8xUaYs9O2zlXnAMEtiUgYG6r nlZDgACBYgXmeBjQsYuvg2jfft4+9gBHjD/1U2GnTl1omCq23+2dK/utvZVvU0DA3GZdrYoAAQJF C6zR1SwBKHQrv/GNb1RXr14t7t9roaGEMyyPOTpX8qiDWRCYS6C4f7DmWrj9ECBAgEAZAkvdq5nz 6l96+eXq/Pzcv9E5F8ncZhMQMGejtCMCWQj4xyuLMpgEAQIECIwJvPfee4cXX3yxev/WrbFNi/t8 6FQ+9dRTQmUBlQvd9ZdeefVkPz+d6vhLHlfALODEN0UCEwRO9g1ywhxtSoAAAQIE7hMo/TLa0u6p nHoKLhlIps7F9nkLCJh518fsCEwVEDCnitmeAAECBLIUCA8Jeuedd7LtcG49UGZ5UrQmNUfojdlH zDZ9XseMLaEGXXMUMEutnHkT6BYQMJ0ZBAgQILBZgXBZ7e/+9m9Vf/hHf7xK8AyXuv7iL/x89TM/ +3PVww8/XOTDeeY6GfYYlGLtcrSZY06p+xAwY88c2xEoQ0DALKNOZkmAAAECCwqEIBp2X4fR9qFC aPx7/+AfXv51iU90XZBud7tODVG7g5qwYAFzApZNCRQgIGAWUCRTJECAAAECBAhsVUDA3GplrWuv AgLmXitv3QQIECBAYEGB8HqZ0PltfoSnr4YOYPuwQ39/7BRjO45hvjdv3hz9uSh2f8fOuz1+juPW 9sc+BXeOuTTXJ2DOfbbYH4HTCox+Iz3t9BydAAECBAgQKFHg2NBw7PgpZiFchu1jAuaU/ea+bWpQ TB3XF5rXrHXuNTE/AlsQEDC3UEVrIECAAAECmQmUEhpCWAoPgYoNmHOFq6nlOtVxu+Y511zq/ZRy rkytme0J7FVAwNxr5a2bAAECBAgsKLBWaDgm7DTDZWzAXJBscNfHrHNszlP3PXX7seOvda6MzcPn CRCYR0DAnMfRXggQIECAAIGGwClDQ2wAqi+Nracdc4ls7L7nPhlOddyudcw1Fx3Muc8S+yOQh4CA mUcdzIIAAQIECGxKIDVgTgkvXdvGjm+Hy4AfEzBPVaTYda0xv7nmImCuUS3HILC+gIC5vrkjEiBA gACBzQukBsw1YNqXxm6hgxm8H33ssUG+uQK0gLnGWeoYBMoVEDDLrZ2ZEyBAgACBbAVSAuZcwWUI pS9chleqHPv6jiWLMWbT1ZFtzyc2YI7VbmwuUx3Gjjd1f7YnQOC0AgLmaf0dnQABAgQIbFJg7dAQ G3qGglhMAIs9ztxFHTvunAFzbO5jcxkbX3/eJbKxUrYjUJaAgFlWvcyWAAECBAgUIbBmwIwNPGMh LCZgngp/bI1jawvznmt9Y3OJNRIwY6VsR6AsAQGzrHqZLQECBAgQKEJgzYAZAzJXAJsrXMXMubnN 2HHnWl/MvMbmErOPsI2AGStlOwJlCQiYZdXLbAkQIECAQBECuQXMItAGJjkW6up7S8O9pF0fc95f OjaXqdbOlalitieQt4CAmXd9zI4AAQIECBQpEBsajgkrx4xdAzXMb+px+oLg0mudsv8p2w6tXwdz 6tlhewJlCAiYZdTJLAkQIECAQFECsQHzmEXNFXSOmcNYgPqnf/7fJu3+sf/0nzp/NstprXPNRcCc dGrYmEAxAgJmMaUyUQIECBAgUI5ATMCcElS6tp0y/lRyt/7xPz78+8/+7SoEzfBr/fEzP/tz1Z/9 6Z9U7V9P1cGc4jOXu4A5Rd22BMoREDDLqZWZEiBAgACBYgRiAuaxi5kr6Bw7j77xYX51B7MOmUPH CtusGTBT/VLH9a19jXNlqRrbLwEC9wsImM4KAgQIECBAYHaBNULD3EFnboQ6YHZ1MMOxml3N+vdr Bczw1Nn3b92qPj4cJv8sOJe7DubcZ5z9EchDYPI3lTymbRYECBAgQIBAzgIC5ifVCZfIhl/bHcz/ 9x/9o+p/+c//uWr/uqd7MOvzd41zJeevFXMjsDUBAXNrFbUeAgQIECCQgcBaoaF+Umvo/B37+5hX edRdt/avXeRdHcy6U9n+tZ7/Wh3MNU6R/7+9O7xpGwzCAMwETNBFGIFR2g0YgKIu0I7CAPnBIigD MECDHNWVFTmJP5Jz7LvnF61wLr7nrlJfvpCcO+l0gjnHFDwHgfkFBMz5zT0jAQIECBBIL3AsYJ4L HUOYlmuXCjp2gjl8yezwpbGn+p3bYs7nm+uHEUvdEfdFIJuAgJltovohQIAAAQILEMgaGlqC19gJ 5sPvP3dvP77vf//y6ePj7uX+fv91s9nsT2BvcYLZ0lPEamXdlQgrNQmsQUDAXMOU3CMBAgQIEFiZ wFhoaAkyLdcumaY7wXx/ff0fKrt7nfsEc4rllGuinAXMKFl1CdxGQMC8jbtnJUCAAAECqQWyhoaW IHbsBPPb4+M+ZPanmf3XbiGiTjBb7nu4mF99XMtyZ92VFgPXEsgkIGBmmqZeCBAgQIDAQgTWFhqi glT/O5j9WA5PL4cnmt2fb/kuslEG51Zybbtyrh/fJ1BdQMCsvgH6J0CAAAECAQLXeJOfgNu6qOR2 u939+vl89JTxsHh/gnkYIod/P/yMzKgTzFONTw2WU68bczj1Dr0C5kVr6cEEFicgYC5uJG6IAAEC BAisX2BqaPhqaIkSWtr9RPXZ1b201/5jYcbuccpHvvSPm7orkRZqEyBwPQEB83qWKhEgQIAAAQL/ BFpCQ8u1gPMJmH++meqotoCAWXv+uidAgAABAiECQkMIa8qidiXlWDVVWEDALDx8rRMgQIAAgSgB oSFKNl9du5JvpjqqLSBg1p6/7gkQIECAQIiA0BDCmrKoXUk5Vk0VFhAwCw9f6wQIECBAIEpAaIiS zVfXruSbqY5qCwiYteevewIECBAgECIgNISwpixqV1KOVVOFBQTMwsPXOgECBAgQiBIQGqJk89W1 K/lmqqPaAgJm7fnrngABAgQIhAh0oSGksKIpBf7udv5PmnKymqoo4B9zxanrmQABAgQIECBAgAAB AgECAmYAqpIECBAgQIAAAQIECBCoKCBgVpy6ngkQIECAAAECBAgQIBAgIGAGoCpJgAABAgQIECBA gACBigICZsWp65kAAQIECBAgQIAAAQIBAgJmAKqSBAgQIECAAAECBAgQqCggYFacup4JECBAgAAB AgQIECAQICBgBqAqSYAAAQIECBAgQIAAgYoCAmbFqeuZAAECBAgQIECAAAECAQICZgCqkgQIECBA gAABAgQIEKgo8AnOCS0ENzfoqAAAAABJRU5ErkJggg=="
            preserveAspectRatio="none"
            height={169.33333}
            width={243.41667}
            x={933.47174}
            y={394.28033}
          />
          <rect
            ry={0.045448378}
            y={433.12564}
            x={-229.30182}
            height={15.486614}
            width={36.968048}
            id="rect2381"
            fill="#fff"
            fillRule="evenodd"
            strokeWidth={0.288906}
          />
        </g>
        <path
          id="path3811"
          d="M56.05 12.464v4.244l-6.198 5.712h-4.737"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.03278}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3815"
          d="M41.382 17.577H49.8v-6.095l-.685-.803-1.607 1.607-1.264-.34-2.28 3.95-1.628-1.628-.852.852v2.48"
          display="inline"
          fill="#02728e"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          id="path3817"
          d="M41.564 14.465l.819-.819 1.503 1.504 2.087-3.615 1.473.395 1.79-1.679"
          display="inline"
          fill="none"
          stroke="#fff"
          strokeWidth={0.265}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          transform="scale(.6992) rotate(13.076 118.467 111.93)"
          id="path3853"
          d="M49.952 28.832l-1.166-.791 1.269-.614z"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264999}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <path
          id="path3859"
          d="M199.328 10.286v12.108h118.766l6.377-5.72V4.763H206.05z"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.5}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M333.613 10.272v12.16h46.372l6.548-5.745V4.727H339.96z"
          id="path3861"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.5}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3863"
          d="M333.933 14.522v-4.027l6.123-5.42h4.682"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M386.251 12.464v4.244l-6.197 5.712h-4.738"
          id="path3865"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.03278}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3871"
          d="M395.234 10.272v12.16h69.238l5.14-5.745V4.727h-69.866z"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.5}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3873"
          d="M395.346 14.266v-3.997l4.385-5.38h3.353"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M469.413 12.364v4.285l-4.69 5.767h-3.587"
          id="path3875"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.0708}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M477.785 10.272v12.16h69.238l5.14-5.745V4.727h-69.867z"
          id="path3877"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.5}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M477.896 14.266v-3.997l4.386-5.38h3.353"
          id="path3879"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3881"
          d="M551.963 12.364v4.285l-4.69 5.767h-3.586"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.0708}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M199.422 14.817l-.094-4.53 6.722-5.522h3.586"
          id="path3883"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.0708}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path3885"
          d="M324.193 12.342l.094 4.53-6.722 5.522h-3.586"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.0708}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <text
          id="gerencial"
          y={15.864633}
          x={6.902349}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={6.902349}
            id="tspan3887"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"GERENCIAL"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={65.639877}
          y={15.864633}
          id="main"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3891"
            x={65.639877}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"MAIN"}
          </tspan>
        </text>
        <text
          id="dc"
          y={15.864633}
          x={82.044067}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={82.044067}
            id="tspan3895"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={principal_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DC'}{' '}
              </a>
          </tspan>
        </text>
        <text
          id="grupal"
          y={15.864633}
          x={202.6931}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={202.6931}
            id="tspan3899"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"GRUPAL"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={228.09276}
          y={15.864633}
          id="ats"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3903"
            x={228.09276}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={ats_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'ATS'}
              </a>
          </tspan>
        </text>
        <text
          id="ups"
          y={15.864633}
          x={241.32175}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={241.32175}
            id="tspan3907"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={ups_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'UPS'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={255.0799}
          y={15.864633}
          id="pdu"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3911"
            x={255.0799}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
             <a href={pdu_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'PDU'}
              </a>
          </tspan>
        </text>
        <text
          id="gen"
          y={15.864633}
          x={268.83841}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={268.83841}
            id="tspan3915"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={gen_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'GEN'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={282.59702}
          y={15.864633}
          id="uma"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3919"
            x={282.59702}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={uma_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'UMA'}
              </a>
          </tspan>
        </text>
        <text
          id="chiller"
          y={15.864633}
          x={296.88467}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={296.88467}
            id="tspan3923"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
           <a href={var_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'VAR'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={336.57275}
          y={15.864633}
          id="reporte"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3927"
            x={336.57275}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"REPORTE"}
          </tspan>
        </text>
        <path
          id="aacc1"
          d="M379.895 8.9l-.386.323.023 1.335-.86-.435-.26.157v.408l1.08.614v1.04l-.816-.448-.018.905-.874-.47v-1.314l-.314-.215-.3.166v.986l-1.282-.749-.358.207.035.515 1.152.619-.735.457-.009.367.408.229 1.067-.596.73.466-.64.47.667.467-.811.47-1.049-.618-.372.179.004.41.744.43-1.155.666v.488l.403.211 1.201-.708v.887l.314.197.323-.237v-1.224l.928-.448v.87l.757-.511.004 1.062-1.044.592v.367l.309.22.793-.458v1.332l.395.322.296-.331v-1.314l.824.462.283-.188-.014-.516-1.013-.51v-1.031l.793.493v-.83l.883.485v1.192l.323.237.34-.246v-.941l1.143.735.417-.189-.027-.475-1.134-.627.722-.47v-.39l-.291-.184-1.062.555-.83-.434.695-.48-.686-.403.798-.57 1.116.623.318-.197-.027-.403-.757-.435 1.174-.654-.045-.507-.354-.161-1.174.623-.013-.91-.345-.197-.368.229v1.349l-.892.457v-.92l-.73.516v-1.02l1.114-.642v-.386l-.34-.196-.836.483v-1.48zm.376 4.43l.37.591-.361.596-.732.005-.37-.591.36-.596z"
          display="inline"
          fill="#3fbefa"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".0177471px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          d="M461.916 9.311l-.385.323.022 1.335-.86-.434-.26.156v.408l1.08.614v1.04l-.816-.448-.018.905-.874-.47v-1.314l-.313-.215-.3.166v.986l-1.282-.748-.359.206.036.515 1.152.619-.735.457-.01.367.408.229 1.067-.596.73.466-.64.47.668.467-.812.47-1.048-.618-.372.18.004.409.743.43-1.155.666v.489l.404.21 1.2-.708v.887l.315.198.322-.238v-1.223l.928-.449v.87l.757-.511.004 1.062-1.044.592v.367l.31.22.793-.457v1.33l.394.323.296-.331V17.67l.825.461.282-.188-.014-.515-1.013-.511v-1.031l.794.493v-.83l.883.485v1.192l.322.237.341-.246v-.941l1.143.735.417-.188-.027-.475-1.134-.628.721-.47v-.39l-.29-.184-1.063.556-.83-.435.695-.48-.685-.403.798-.57 1.115.624.319-.198-.027-.403-.758-.435 1.175-.654-.045-.506-.354-.162-1.174.623-.014-.91-.345-.197-.368.229v1.349l-.891.457v-.919l-.73.516v-1.02l1.113-.643v-.386l-.339-.196-.836.483v-1.48zm.376 4.43l.37.592-.36.595-.733.005-.37-.59.361-.597z"
          id="aacc3"
          display="inline"
          fill="#3fbefa"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".0177471px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <text
          id="disponibilidad"
          y={15.864633}
          x={399.01538}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={399.01538}
            id="tspan3987"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"DISPONIBILIDAD"}
          </tspan>
        </text>
        <image
          id="cpu"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAToAAAE7CAYAAABNHk35AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz AAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURB VHic7d15eGRlnS/w7+89VUllqSTdSWXrTjcNNIhcF8AFFEFZHEEHvO4ouI5zFXG/3rnqODouKOrj vtyLcllFxyuM4IJwVdQBFRBEAWURGmhSWSqV5aSSSqrOOb/5I9190yepJUlVnaqT7+d5+tG871vn /For3z7b+x7BBk1NTXU5jvM8ETkRwFEAdgPoA9AKoHmj26e65AGYATAL4GEReRDAbY7j/Lqvr++R YEsjWknW86E9e/bE4vH4K1T1dSJyOgCrwnVR4/oLgKsBXNbT0zMcdDFEwBqDLplMtjY1Nb0TwHsA 9FenJAqJHICrXNe9sK+v7+Ggi6HNreygm5iYeBmALwLYUb1yKIQWAXwum81eODQ0lA26GNqcSgbd +Ph4uzHmGwDOKzF0UkR+73ne/caYvZ7nzRtjpipTJtUTVW0D0KKq/caYI1T1WABHlPjYfcaY12zd uvXeGpRIdJCiQTc1NbXTdd0bsHSTYTVjInKV53lX9/T03C0iXuVLpEYwMTGxDcDLAbwewHEFhmUB nNvT03NtzQojQpGgS6fTT1bV/wdgcJXuEVX9eCaTuWzXrl0L1SuPGlEqlXq+Mebjqvq8VbpdVb0g kUj8r5oXRpvWqkE3PT29y3GcW7Ay5BTAN0TkQ93d3XbVq6OGpaqSTqfPBfAVAF3+bgBv6OnpubL2 ldFmtCLoJicnOz3Pux0rr7nMquq5iUTi+tqURmEwNTV1iOu6P8DK01lHVV+YSCRuDqIu2lzM8h9U VTzPuwQrQ25cVZ/PkKO12rJly6Oe5z0fwE2+roiIXJ1KpQYCKIs2mYOCbt+pxst9Y2ZU9YxEInFX 7cqiMOnt7c1ks9mXishvfF39IvLNQIqiTeXAqev09PQWx3HuB9C7rN8TkRd3d3f/rPalUdjMzMxs zefzdwI4ZHm7qp7NswWqpgNHdI7jvB8HhxwAfI4hR5XS2dk5KSLnAHCXt4vIp1XVFPgY0YYZYGli PoALfH0PZ7PZf619SRRm3d3dvwfwDV/zkycnJ/2XTIgqxgCA67rnAuhc3qGqH+GUHaqGaDT6MSyt fHKAqp4fTDW0Gew/XXi9r/3hnp6e79e6GNoc9p3C+h8YPnlqauqQIOqh8DOpVGoQwDN97ZeKiLva B4gqwfO8b2PpweH9xHXds4Kqh8LNiMgp/kbXdb8XRDG0eSQSiQcB3O1rPjWIWij8DIATfG2Pcf0w qgUR+YWv6fhACqHQMwCe5Gu7PYhCaFPyf9d6Z2ZmtgZSCYWaAXDY8gZVvT+gWmiTEZEH/G35fP7w IGqhcDPwrSwhImMB1UKbTD6fX/FdE5HO1cYSbUQEQJuvLbORDaZSqXg+FvsCRKIb2Q7Vt6jqZYl4 /Fcb2UYul7NbWlr8zR0b2SbRaiL7/iy3ocdKWlpa2kZFzronFvNPJ6OQaPc8HLOwYAP41Ua2s337 diedTvub/d9Hog2rypfKEXHmDKcuhhXfbUmNhmlERKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9 Bh0RhR6DjohCj0FHRKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKHHoCOi 0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKEXCboAIqofqhp5/PHH4wAQjUYXBwcH 54OuqRIYdESb1OTkZGc2m32RMea5qnosgMNGRkb6otGo7B+TTCanAewFcLeI3GaM+VlfX9/DgRW9 Tgw6ok1EVc3o6OgZnue9bWFh4YUi0qSqxT7Ste/PU1T1PNd1kUwm/6SqlzQ1NV2WSCRma1P5xvAa HdEmMTw8fPbIyMg9qvpjEXkJgKZ1buppIvKVfD6/N5lMfjiZTLZWss5qYNARhVwymdyRTCZvFJEf AnhyBTfdCeCTAO4fHh4+vYLbrTgGHVGIDQ8Pnw3gHgAvrOJuhkTkxmQy+QVVrcvLYQw6opAaHh7+ HyJyLYCOGuxOALx3ZGTkR6lUKl6D/a0Jg44ohIaHh/9JRC5C7X/HX5TP539Wb2FXl4eZRLR+w8PD 7xSRz5Q7XkTQHI0ialmIWBZElp4uUVW4noe84yDnOHA9r9xNPiefz1+jqmeKiLOOv0LFMeiIQmR4 ePg0EflCOWMjloX2lhbEotED4VZMznEwt7CAhVyunM2fPjIy8jkA7y1ncLXx1JUoJPbu3bvVGHM5 ShzAiAg629qQ6OxES1NTWSEHAE2RCLa0t6OnowMRyyrnI+8ZGRk5o6yNVxmDjigkLMv6oqoOFhsT sSwkOjvR2ty87v1EIxH0dHSUtQ1Vvbgertcx6IhCIJlMHgvg3GJjopaFno4OWGbjv/b7jwrbW1pK Dd2ey+X++4Z3uEEMOqJw+ASK/D5HLAtb4/GyT1PLFW9pKXlkJyLvfeKJJ7oruuM1YtARNbjh4eEj ARS8FiYi2NLeDlOBI7nVdLS2IhopelkwblnWm6uy8zIx6IganIi8FUsP7K6qPRYr9+bBevePztaS 013/sWoFlIFBR9T4Xlaow4igLRaregHRSAQtRU5hVfXwvXv3PrXqhRTAoCNqYCMjI08GsKtQf2ss VvHrcoW0lbhWZ1nWi2tSyCoYdEQNzPO8Zxfrb2la70pMaxeNREqdIhettZoYdESN7bhCHZYxVb02 t5rmaLRY97G1qsOPU8CooT3xxBPnNTc3f8KyLDfoWtbLdV1rcXHxI9u3b79yrZ81xuwotEJwiTuh VVFin9vvu+++pqOPPrqsOWSVxKCjRtfa1ta2s1bXoapBVbG4uLiuVXqLzYSoxIPBa1Vin9LV1dWH pXdQ1BRPXYkaW8GpCSaA8C+1TxFpr1EpB2HQETW2RvsdLvomnmpptP+RiOhgBd+7GkSirLJPXfaf aoxZrGU9+/EaHTU0VZ2dm5t7xBjTsDcjPM+zVHW9rw0cLdSxhoUyK8ZxVqyzKcv+U2dnZ5O1rWgJ g44a2tDQ0NUArg66jgA9VqgjvzJ0qi7vFv33ZnT37t2BHNHx1JWosf2xUIfjunCKB0/FLebzBftU 9c4alnIQBh1RA3Nd97Zi/WUue14ReccpGqzGmDtqVox/30HtmIg2bvv27fcAeLxQ//ziIgo9UFxp 84vFz0pV9Sc1KWQVDDqiBiYiCuCHhfpdz8PcwkLV63Bct1TQPTYwMHBX1QspgEFH1OCMMd8q1p9Z WKj6tbrpTKbUkG/vC+VAMOiIGlx/f/+9qvrLQv2qium5uaqdwtrz86Xuti4YYy6uys7LxKAjCoeP FOvMOw6mMpmKh93cwkLJU2NV/Wp/f/94RXe8Rgw6ohDYtm3bbwH8oNiYxXweaduGV6Gws+fnYc8X nJix33gul7uwIjvcAAYdUUhYlnUBgHSxMXnXRWp6ekOPnTiui4mZmXJvcrx9165d0+veWYUw6IhC oq+vb0xV3wyg6NwvTxVTmQzStr2mwMs7DqYzGaRmZkpdk9vvksHBwWvL3kEVcQoYUYhs27bt+mQy +VEsvee1qJzjIJfJwIigORpFUzQKy5gDSy0plh5PyTsOFvP5td65vWVubu4d6/pLVAGDjihkBgcH P5lMJrsBvKec8Z4qsrkcspWbRXGX67pnBzWvdTXhDzpVeGVcS1ARWGW8Fs7LZJBLll6AwcTjaBoY KDnOXViAV/qCLkwsBqv0uzOJAAADAwPvGxkZyQL4YC33KyK/y+VyL965c+dULfdbSuiDbnF4GL13 3om29uILm+5Np4FXvark9vL33IMTWwou6nrA3Y89BqeMoPNuuw1HlbHk9YMLC8Dpp5ccRwQcmDHx oZGRkQdU9RsAavGv5CWZTOYd9XQkt1/ogw6eh0MGB7F169aiwzKqsMvcZDweLznGmp5GOYvkWJEI hvr7S457ZHi4+BVmolUMDAxcPjw8/HsRuRzVe93gOIDzBwcHr6nS9jeMd12JQm7btm0PDAwMPEdE 3oDKvpgmKyKfzefzT6rnkAM2wxEdEUFEPABXqOp3k8nkK0TkHQBOwPoOdh4HcLFlWd/u6+sbq2ih VcKgI9pERCQP4LsAvvv4448PRiKRvwfwHADPAHAoAP8dOQUwLCJ/BHCHqv50YGDgriAn6K9H6IPO NDfjvmQSzdPFH86eymTQVM72Egn8fqz0P2LzsRjKeUe6G4vhd3v2lBy32NKCou9AJ1qjHTt2JAH8 731/AAAjIyMJY0wcABYXFxeGhobGRaT2a7JXWOiDLtrXB+fss0veGCgn5AAgevjhyB5+eMlx5YQc AESPOw7lTKRhyFEtDAwMpACkgq6j0ngzgohCj0FHRKHHoCOi0GPQEVHoMeiIKPQMsOKGZOjvxFJ9 GBkZWXEzWVULvwGZaJ0MAP/re4rPfieqkEgk0rFKc7lTjonKZlTVv5xK6SU3iCpjxWoGqhr4stsU PkZE/uZrOzKQSmjTEZEn+dsikYj/+0i0YREA9wNYvtDZ8RvZYDabnetrbh7+u7m50Q1VRnVLVE3E 8/6w4e2I+L9rI1u2bOERHVVcRERuVdV3LmvbNjExcVRPT89f17PBRCIxC+BZlSmPQu4038+/DaQK Cr1IPp+/ORKJKADZ36iq5wD4l+DKorCbnJx8qud5R/uafxFIMYS//e1v73Rd9xW+ZolGo1859NBD i74vthFE+vv7x1Op1K0icuL+RhF5o6p+Yt+SLkQV53neW31NrqpeF0gxBFXdOT8/f9LyNhGBZVmh +P/EAIAx5nJf+9Dk5OTrA6iHNoHx8fF+AG/xNf88kUiUfusQ0ToYAHBd93uqOrG8Q1X/NZ1Or/ac E9GGGGMuBHDQG4ZU9WsBlUObgAGA3t7ejIh8yde3TVU/HUBNFGITExOnAnijr/munp6enwRQDm0S B+a6ep73Zax8ccb56XT6lbUticIqlUoNALgKy2587fOBRluamxrLgaDr7e3NAHi3f4CqXjY5Ofnc mlZFoZNOpztE5MdYORviOz09Pb8MoibaPA5avaSnp+ffAfwf35hWz/N+PD4+fhKI1sG27W5VvQHA sb6uR6PR6LuCqIk2lxXLNGWz2QtE5G5fc5cx5saJiYk31qYsCot0On10Lpe7FUtvmlpuAcCrOzs7 JwMoizaZFUE3NDSU9TzvTBF5xNcVA3BpOp2+emxsrK825VGjUtXIxMTEu1T1DqycP+2o6qt7enpu D6I22nxWXXgzkUiMWJZ1GoCH/X2qeo5lWfdPTEx8aHJysrPqFVJDUVUzMTHxsnQ6fReAL8P3GAmA nKqel0gkrg+gPNqkCq4w3NXVtcdxnOcAWO1f3S4An/I87/GJiYlLJiYmTtuzZ4//xbe0SaiqTE5O PiWdTn80nU4/AOAaAE9ZZeg0gDMTicT3alshbXZFVxPu7+8ff+ihh07q6ur6jIi8GysfC+gA8GYA b47H4wsTExN/BPBXVd2LpQU9uYhiCIlIDEsLtPYDOCKdTh8LIFHiY7+3LOucLVu2PFrt+oj8Si6b vnv37kUA702lUteLyFcA/JcCQ2MATgBwgog/D2kTs0XkY1u3bv1qGN74To2p7JfjJBKJm7u7u48R kX8A8FAVa6JwmBWRz7uue0R3d/cXGXIUpDW9CGffl/USVb10YmLiDGPM61T1LABt1SmPGoynqr8T ke9EIpHvdXV1+ZfpJwrEut74JSIegJ8A+ImqNk1NTT3L87wTARwF4HAR6VfVdgAr3vJEq1NV8Txv 1SNsY4xXb1OkRGTK87xZEXkYwIMAbotGo7/hc3FUjzb8akMRyQG4Zd+furX3scd+JJa129/uOo5G m5qOGxwcnA+irv0eeeSRc1tbWy9f7fpmJpP51q5du94WQFlEobBp3uEqIrFIJLLai3/GpqamAn+R t2VZiEQiZrWgsyyL17eINiDwX3Aiompj0BFR6DHoiCj0GHREFHoMOiIKPQYdEYUeg46IQo9BR0Sh x6AjotBj0BFR6DHoiCj0GHREFHqbZlK/53kR13VTK9pdN9/d3R34Ekiqms5ms3cbY1ZM4HddNx1E TURh0dBBNz09fYfjOOW+elFUNbeitalJROSvExMTlS2ugpqbm980MTHxpnLGRqPRizs7Oz9Z7ZqI GklDB53jOFhYWBgKuo56Eo1Gm4Ougaje8BodEYUeg46IQo9BR0Sh19DX6CKRyHwsFvtL0HXUE8uy +OYtIp+GDrqurq6Tg66BiOofT12JKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKHHoCOi0GPQEVHo MeiIKPQYdEQUeg09BQwA9uzZ83YAz/e3i8g9hxxyCBegJCrPXCwWe2h5g6oaAHZA9VRUwwed53lP A/Aqf7uI8GiVqEy7d+/+KICPBl1HtTAMiCj0GHREFHoMOiIKPQYdEYUeg46IQo9BR0Shx6AjotBj 0BFR6DHoiCj0GHREFHoNPwWMNpepqakuEZGg66DK6+zsXBCRbDW2zaCjuqOq1szMzDHGmJMBPBvA EQAOA9AebGVUTbOzs7BtWwGMAXgIwF9V9ZZoNHpza2vrExvZNoOO6sbs7OxRqvqW2dnZc4wxg0HX Q4EQAP37/jxPRP7RcRzYtv17Vf2O4zhXdHd3r3lFFV6jo8BNT08fa9v2D1X1XgDvB8CQI7/jReSr 0Wj0Mdu2PzU1NdW1lg8z6Cgw09PTW2ZmZr5pjLkDwNng95FK6wLwIcuyHrBt+43lfoinrhQI27af KyJXq+qOIsMcAHeq6l0AHgQwJiLzxpi52lRJtaSqWzzPazHG7ABwlKo+B8AhBYb3ArjUtu2zVfUt nZ2dk8W2zaCjmrNt+x8AfFNVV/v+qYj83PO8K3K53HWJRGK21vVR/Zienj7MsqxzVPVNAA5dZchL ReRptm2f0dHR8UCh7fBUgWrKtu0PArgYq/8je50x5ph4PP7Czs7Oqxhy1NXV9XA8Hv9kPB4/QlVf C+CRVYbtAnDL9PT0Mwpth0FHNTMzM/MuABdi6c7aciOq+uKOjo6Xtre3/ymA0qjOiYjb2dn53Xg8 fjSWvkOub0iPMeaG2dnZo1b7PIOOasK27bNF5EurdN0sIk/r7Oz8ac2LooYjIgsdHR0fFpHTAaR9 3T2qeoNt293+zzHoqOqmp6d3AbgUK4/kfhCPx8+Ix+OpAMqiBhaPx28GcKKq7vV17QRwhaoe9F1j 0FHVici3AGzxtd0Uj8dfJyKLAZVFDa6jo+N+AKdgaSbFcmfOzs6+ZXkDg46qamZm5lwROdXX/Jds NvtyEckFUhSFRmdn599E5NVYehRpuYts2+7Z/wODjqpGVaMi8nFfc05EXt3b25sJpCgKnXg8/mtV /ZSveSuAD+z/gUFHVTM7O/taLN36X+7z8Xj83iDqofDq6Oj4NAD/c3Tn778xwaCjanq77+fJxcXF zwRSCYWaiCyKyD/7mttV9XUAg46qxLbtI7G0xNIBqvp1PgRM1dLe3n4tfEd1xpg3AAw6qp6zfD+r ql4WRCG0OYiIh6XHmA5Q1WPn5+e3M+ioWl7g+/n2rq6u1abvEFWM53nf97fl8/lTGHRULcf7fv5F IFXQptLV1bUHK+fDHs+go4qbnZ3the8BYQB/CKIW2nxU9aDvmogcyaCjivM8b8VyOiJScAkdogrz f9cO43p0VA2d/gZVHa/Gjtra2vqamrxTsHIeLTUO9bymn83MzExVaHv+udMdDDqqOGNMm6oe1BaP x6uyKrDjOE99zeneFcce6fK73KDufshyr77RvATAzyqxPRHxvzyHQUdVsdr3yr9+WMXsGvC8447y qrV5qrKZOdHSo8qnqq7v1b8Wr9ERUegx6Igo9Bh0RBR6DDoiCj0GHRGFHoOOiEKPQUdEocegI6LQ Y9ARUegx6Igo9Bh0RBR6DDoiCj0GHRGFXsOvXmKMmQdwv79dRBYCKIeI6lDDB92uXbveB+B9QddB RPWLp65EFHoMOiIKPQYdEYUeg46IQo9BR0Shx6AjotBj0BFR6DHoiCj0GHREFHoMOiIKPQYdEYUe g46IQq+hJ/WnUqkBAMe6rnu4iAwA2AIAqrogIsOqutfzvLsHBgYeEBEv2GqJKCgNF3RjY2MnAHgl gLM8zzsMAETkoDH7fxYRWJaF8fHxmbGxsZtU9dp8Pn/d0NBQttZ1E1FwGiLoVNWMj4+/FsD7ATx9 HZvoBPBKEXllU1NTenR09GLP874wODg4UdlKiage1f01ulQqdfL4+PgfAVyJ9YWcX7eIfNCyrIfG xsY+oKoNEfZEtH51G3R79uyJjY6OftXzvJsBPLUKu+gC8Nnx8fFbU6nUEVXYPhHViboMulQqNdDa 2nqziFwAQEp+YGOe5XnenaOjo39f5f0QUUDqLuhGRkYO8TzvtwCOr+Fu20Xk38fHx99Qw30SUY3U VdCl0+ntxphfAzhkLZ/bf3c1GokgGokgYlkwsuYDQUtVLxkdHT1nrR8kovpWNxfiR0dH2xzHuR7A jnLGG2MQa2pCNBKBZVmrjvFUkXcc5HI55B2nnM1aInLp2NjYo319fb8rv3oiqmd1c0QnIl8HcEyp ccYYtLe2oiseR6y5uWDIAYARQXM0inhbG7ricTRFo+WU0gzgB8lksqfs4omortVF0I2Ojp4NoOT1 sVhzMzrb28sNrIPsD8j21lYYU/KvPWhZ1lfWvBMiqkuBB92ePXtiIlIyVNpbW9Eai62YBbFWTdEo Otraygm7c8bGxk7f0M6IqC4EHnStra0XoMR1ubaWlnUdxRVijEG8rQ1WibBT1QtVtdqPtxBRlQUa dPfdd18TgPcVG9MSi6G5qani+7b2ncoWO0IUkWekUqm/q/jOiaimAg267u7uVwAYKNQfiUTQ0txc tf1bllVy+6r69qoVQEQ1EWjQGWOKPrPWGotVvYZYc3Op63Vn7t27d2vVCyGiqgks6MbHx9tV9bRC /fsf/K2FEkd1kaampjNqUggRVUVgQed53rMBFDxkq8Z1uUKaotGi1+pU9ZSaFUNEFRfYzAhjzDNV ddU+EUE0UrvS9u8vl88X6j9urdu88cYb22Kx2NM2XFwd8Dzvzy94wQsyQddBtF6BBZ2qPqlQn2XM hp+XW6uIZRUMOgBPVlWzluXYY7HYyf39/T8pNnOjEXieh/Hx8dcA+LegayFaryDnum4r1FHGw7wV VyKQomNjYz0Axte6zWgFn/8LglPeHGGiuhbkXdeCc0mDOAoq4+HhRI1KIaIKCzLoCqdZgWt3QRKR wGeRENH6BPnLW/h6V42vz61qKWz3J64u++9E1GCCvEaXKtThebV/Bavr3+dS2O5PXDHGrOmNYSIy MzMzc69lWQXvcDQCz/MirutOBV0H0UYEGXQjhTpc161lHQBKhmsukUis6UbESSeddCuAp2yoKCKq iMBOXUXkL4X6XM9DoWfsqqXECsQPruXREiKqL4EFnareWaSv3KXPK1VL0f2p6u01K4aIKi7IoPst gIVC/Yu5XM1qyTtO0SNIY8xvalYMEVVcYEHX398/p6q/LNSfdxw4NbpWN79QMG8BIL+4uPijmhRC RFUR9LNhVxXrLBFAFbGwuFj0RoSI3Dg0NDRZ9UKIqGoCDbp0On0NikyrchwH2cXFqu3fdd2S21fV r1WtACKqiUCD7uijj84B+HyxMdmFhapcr/NUkZmfL3V39+7e3t6bKr5zIqqpoE9dkcvlvgbgiWJj 5rJZLBZeWWTNPM+DncmsfEh4pQ+ICGdEEDW4wINuaGgoq6oXlBo3Nz+P+YWFDT9fl8vnYc/NlZx9 oarf7+vr+/mGdkZEdSHwoAOA/v7+6wBcWWrcwuIiZjKZYuvGFeR5HjLz88jMz5czxWzUcZzz17wT IqpLQU4BO4jrum+zLOspAJ5ebNz+wDLGINbUhGgkUnBZJ08V+XweecdZSzguAnjF9u3b02v6CxBR 3aqboBscHJxPJpNnW5b1GwA7S433PO/A4yciAmMMzL5VT1QVqlrONbgVmwXwlr6+vlvX+kEiql91 E3QAMDg4+PjY2NipAH4JYEe5n1NVuK6LDT5e7InIm3t7e7+zsc1Q2HlqMLFwFCDFF4gVOEjECk7p PmDe2YLx3Eklx0UxiW2t/1FyXNbtxlTuySXHNcs4umMPlBwXBnUVdADQ19f3cCqVOl5Vr1PVZ9Zo t7MAXt/b2/vDGu2PGtii0wL0fxgD23cXHbfnwduAfMn7bMjkh7DzmP+J1tbWouMe+vM1gFs66MYX jsOhz/pUyXGP3HM1usGgC0wikRjZu3fvydFo9CIRuQD/f124ilPVP0QikfN6enrur9Y+KHysiIWm Eq/ktCwDBLQaYanaACCAV7MEpm7/qkNDQ9n+/v53eZ53KoA/V2EXMwD+qa+v7wSGHFG41W3Q7Tcw MHBzb2/vsSLyRgB/qsAmJwFclM/nD+vr6/usiPA1V0QhV5enrn4i4gK4HMDlY2NjzxWRV6jq2QB2 lbmJDICbAFyXy+X+79DQULZatRJR/WmIoFtu36MftwJ4byqVGnBd95kADhWRIQDtAKCqOREZB/Co qt7d19f3Vx65UaVETB6TT/wCmfFfFR2XzS7s+0YW1x55Asl7P4dSsw2NMwXESm+vI/oo7r/9SyXH We4TZW0vDBou6JZLJBIjAK4Pug7aXKJWDrtbLi49sIyQA4DW6CRacW3pgWX+tm5pehBb8GDpgY39 bvU1qftrdEREG8WgI6LQY9ARUegx6Igo9Bh0RBR6DDqqOFVdsfb92NjYJrrHR0ESEf93Lcego4oz xmT8be3t7WU+bEG0Mara6WuyGXRUcY7jTK3Sti2IWmjzEZFBX9NUQz8wTPXJdd2HjG9pDBF5EoC7 Kr0vY8zslTdEJ677j0j1XwJMVWHPSczzPLuCmzzS9/NDVVv+iDY327afAHDgKE5EvhaPx98ZYEm0 CaiqzM7OjgFILGv+Ak9dqSpU9Vbfz6cHVQttHtPT00/HwSEHALcw6KgqVJGwyAAAA/RJREFUjDG/ 8DUdOT09fWwgxdCmYVnWa31Nrud5v2LQUVWIyHUADloxxhjz3wIqhzYBVW0BcN7yNhH5ZVdX1xSD jqqivb19DMDPfM1vmJ+f3x5EPRR+tm2/FUDf8jbP864A+MAwVZHneV/1NTW7rntRIMVQqNm23S0i /+xrHuno6LgGYNBRFXV1dd0E4Lblbar62pmZmRcHVBKFlIh8Gb6bEKp6kYhkgSq+XYsIAGzbPhHA b3Dwdy3tuu5xW7ZseSygsihE9p2y+ldCfTAejz9VRBYBHtFRlXV0dNwC4FJfc7dlWTfYtt0TRE0U HjMzMy8C8HVfsxpj3rE/5AAGHdVAPp9/L7Bibe+jAPx8bm5uIICSKARs236JMeYarFwU/kvt7e0/ X97AU1eqiUwm81TP827FyjcpPKaqr+rs7Lw9iLqo8eyb/fA+ABcBsHzdt8Tj8VNE5KBXh/OIjmqi vb39z8aY/wpg0de1U0RumZ2d/RdV3STvpKL1mp6ePnR2dvanAD6PlSF3j+d5Z/lDDuARHdWYbdsv EZF/U9XWVbofAfCpeDx+lYisWNOONq+5ublB13XfD+B8rP6Sxj8ZY17U3t4+utrnGXRUc7ZtnwDg hwB6CwwZFZHvqeq18Xj8Nobe5mTbdo+InKqqrwFwJoCm1caJyE25XO6V3d3dBVdAYdBRIObm5gYc x7lSRE4tNk5E5lX1XizdzBgVkYyqckmmcOoQkXZV3YGlm1VHoHhGOSLysfb29k+LiFdswww6Coyq im3b54nIZ+GbukNUwh2qekG5N7EYdBS4ycnJzmg0eoGqvhsrl9ghWu4OABfG4/HrRETL/RCDjuqG qrbYtv1SEXkdgFOx+kVnWiPV1fNApGF+/UdV9RoR+U5HR8fv1rOBhvmb0uaiqrFMJnO8qj4bwG4A h4lIXFW3BF3bctlsdmsul2vxt6uqtLa2TjU1Nc0FUdcyYtv2kOM4Kx4li0ajuXg8ngyiqALy+67B jorIQwD+iqXn4v6y0Q0z6Ig24NFHH/3a4uLiO/ztIoJYLPamHTt2XBZAWQeMjo62ZbPZv7mu2+/v i0ajv9i5c+dpQdRVa3xgmIhCj0FHRKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohC j0FHRKHHoCOi0GPQEVHoRYIugKjBGWPMaisei6oWXfW2FvL5vBeJRKZFZMbfZ4zxv6gotLh6CZFP KpW62HGcE8scbkTE/zYqAICqOgDKXhyynhlj/tDX1/f6oOtYLx7REfl4ntfquu5RQddRTyzLujfo GjaC1+iIKPQYdEQUegw6Igo9XqMjWoVlWXuDrqHONPSNy/8EScVb89IAHxkAAAAASUVORK5CYII="
          preserveAspectRatio="none"
          height={12.024793}
          width={11.986619}
          x={497.17865}
          y={8.0592403}
          display="inline"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={482.09583}
          y={15.864633}
          id="arquitectura"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan4005"
            x={482.09583}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"ARQ"}
          </tspan>
        </text>
        <text
          id="ion"
          y={15.864633}
          x={148.71883}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={148.71883}
            id="tspan10945"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={cmt_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'ION'}
              </a>
          </tspan>
        </text>
        <text
          id="dse"
          y={15.864633}
          x={91.569069}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={91.569069}
            id="tspan13775"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={dse_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DSE'}{' '}
              </a>
          </tspan>
        </text>
        <path
          id="path14613"
          d="M64.4 10.297v12.072h119.563l6.419-5.705V4.791H71.167z"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.50093}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M64.47 14.817l-.08-4.53 5.697-5.522h3.039"
          id="path14615"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.985775}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path14617"
          d="M190.103 12.346l.094 4.517-6.767 5.506h-3.61"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={1.0728}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <g
          id="elect2"
          transform="translate(216.96 -.53)"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        >
          <path
            d="M148.04 14.825h-.86l-.004-.847.784-.077z"
            id="path2004"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2006"
            d="M148.113 13.388l-.775-.37.361-.767.74.269z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M148.828 12.027l-.586-.63.618-.58.59.522z"
            id="path2008"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2010"
            d="M149.966 11.061l-.297-.807.794-.296.343.709z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M151.371 10.59l.057-.86.847.016.058.786z"
            id="path2012"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2014"
            d="M152.875 10.728l.363-.78.785.32-.23.754z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M154.2 11.39l.645-.572.591.608-.51.599z"
            id="path2016"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2018"
            d="M155.176 12.544l.805-.306.337.778-.69.379z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M155.702 13.878l.859.067-.028.847-.886.036z"
            id="path2020"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2022"
            d="M148.9 14.925l3.478-5.945-1.166 3.786 2.891-.87-4.346 8.51 1.597-5.839z"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="#fd6425"
            strokeWidth={0.289164}
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
          />
          <path
            d="M148.546 16.826c.898 2.35 5.458 2.631 6.76-.244"
            id="path2024"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
          />
        </g>
        <g
          id="elect3"
          transform="translate(299.51 -.53)"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        >
          <path
            d="M148.04 14.825h-.86l-.004-.847.784-.077z"
            id="path2108"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2110"
            d="M148.113 13.388l-.775-.37.361-.767.74.269z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M148.828 12.027l-.586-.63.618-.58.59.522z"
            id="path2112"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2114"
            d="M149.966 11.061l-.297-.807.794-.296.343.709z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M151.371 10.59l.057-.86.847.016.058.786z"
            id="path2116"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2118"
            d="M152.875 10.728l.363-.78.785.32-.23.754z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M154.2 11.39l.645-.572.591.608-.51.599z"
            id="path2120"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2122"
            d="M155.176 12.544l.805-.306.337.778-.69.379z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            d="M155.702 13.878l.859.067-.028.847-.886.036z"
            id="path2124"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
          />
          <path
            id="path2126"
            d="M148.9 14.925l3.478-5.945-1.166 3.786 2.891-.87-4.346 8.51 1.597-5.839z"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="#fd6425"
            strokeWidth={0.289164}
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
          />
          <path
            d="M148.546 16.826c.898 2.35 5.458 2.631 6.76-.244"
            id="path2128"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
          />
        </g>
        <text
          id="pqm"
          y={15.864633}
          x={114.32327}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={114.32327}
            id="tspan1456"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={pqm_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'PQM-II'}
              </a>
          </tspan>
        </text>
        <text
          id="text1065"
          y={15.864633}
          x={43.944023}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={43.944023}
            id="tspan1063"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={ger_url} target="_blank">
                {' '}
                {'G'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={161.94778}
          y={15.864633}
          id="text1069"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1067"
            x={161.94778}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={elec_url} target="_blank">
                {' '}
                {'EL'}
              </a>
          </tspan>
        </text>
        <text
          id="text1073"
          y={15.864633}
          x={174.11845}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={174.11845}
            id="tspan1071"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={aacc_url} target="_blank">
                {' '}
                {'A'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={184.17249}
          y={15.864633}
          id="text1077"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1075"
            x={184.17249}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={inc_url} target="_blank">
                {' '}
                {'G'}
              </a>
          </tspan>
        </text>
        <text
          id="text1081"
          y={15.864633}
          x={363.56067}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={363.56067}
            id="tspan1079"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={rele_url} target="_blank">
                {' '}
                {'EL1'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={376.26086}
          y={15.864633}
          id="text1085"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1083"
            x={376.26086}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={raacc_url} target="_blank">
                {' '}
                {'AC'}
              </a>
          </tspan>
        </text>
        <text
          id="text1089"
          y={15.864633}
          x={446.11194}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={446.11194}
            id="tspan1087"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={dele_url} target="_blank">
                {' '}
                {'EL2'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={458.28296}
          y={15.864633}
          id="text1093"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1091"
            x={458.28296}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={daacc_url} target="_blank">
                {' '}
                {'A1'}
              </a>
          </tspan>
        </text>
        <text
          id="text1097"
          y={15.864633}
          x={498.50024}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={498.50024}
            id="tspan1095"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={arq_url} target="_blank">
                {' '}
                {'ARQ'}
              </a>
          </tspan>
        </text>
        <path
          id="path829-0"
          d="M574.793 32.9l-9.268-6.352-518.71-.044-3.209 1.897v.882l3.136 1.676c-.05 2.807.026 3.73.026 6.127l3.474 3.242v1.676l-1.896 1.191v1.941h506.97l2.261-1.147 3.938 2.427h8.241l4.819-2.507z"
          opacity={0.75}
          fill="none"
          fillOpacity={1}
          stroke="#04e6f4"
          strokeWidth={0.4}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path831-8"
          d="M574.926 33.177l.05-1.95-6.809-4.844-3.201-.038z"
          opacity={0.75}
          fill="#0cedf7"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".29085px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          id="path833-1"
          d="M46.884 30.86l-.116 6.226.795-.42v-5.5z"
          opacity={0.75}
          fill="#0cedf7"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".121051px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <g
          transform="matrix(1.26642 0 0 1.26654 -194.248 -83.96)"
          id="g6103"
          display="inline"
          strokeWidth={1.39987}
          fill="#168498"
          fillOpacity={1}
          stroke="#0deff7"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        >
          <path
            d="M260.444 88.374l-12.62 12.458"
            id="path852-9"
            display="inline"
            strokeWidth={0.864463}
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M255.17 89.828l-6.212 5.99"
            id="path852-0-1"
            display="inline"
            strokeWidth=".404818px"
          />
          <path
            d="M257.861 94.466l-4.455 4.302"
            id="path852-0-7-2"
            display="inline"
            strokeWidth=".404818px"
          />
        </g>
        <path
          id="rect2077-1"
          display="inline"
          opacity={0.75}
          fill="#10677d"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.61753}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeDashoffset={0}
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          d="M51.901947 40.844654H57.182446V43.121956499999996H51.901947z"
        />
        <path
          id="rect2077-7"
          display="inline"
          opacity={0.75}
          fill="#10677d"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.61753}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeDashoffset={0}
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          d="M51.901947 37.760723H57.182446V40.038025499999996H51.901947z"
        />
        <path
          id="rect2077-1-2"
          display="inline"
          opacity={0.75}
          fill="#10677d"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.61753}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeDashoffset={0}
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          d="M51.901947 34.6768H57.182446V36.9541025H51.901947z"
        />
        <path
          id="rect2077-9"
          display="inline"
          opacity={0.75}
          fill="#10677d"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.61753}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeDashoffset={0}
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          d="M51.901947 31.592876H57.182446V33.8701785H51.901947z"
        />
        <path
          id="rect2077-1-5"
          display="inline"
          opacity={0.75}
          fill="#10677d"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.61753}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeDashoffset={0}
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          d="M51.901947 28.508953H57.182446V30.786255500000003H51.901947z"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={60.348213}
          y={38.61882}
          id="text1049"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1047"
            x={60.348213}
            y={38.61882}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"ESTADOS EQUIPOS"}
          </tspan>
        </text>
        <circle
          id="st_chill1"
          cx={178.11284}
          cy={36.595806}
          r={3.75}
          display="inline"
          opacity={1}
          fill="#1aea78"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          id="ellipse2176"
          cx={39.309525}
          cy={175.58884}
          rx={4.2296953}
          ry={4.0336714}
          transform="matrix(.50256 0 0 .44492 158.448 -43.264)"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <text
          id="text1066"
          y={38.61882}
          x={140.78151}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={38.61882}
            x={140.78151}
            id="tspan1064"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"OPERATIVO"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={196.3434}
          y={38.61882}
          id="text1070"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1068"
            x={196.3434}
            y={38.61882}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"INHABILITADO"}
          </tspan>
        </text>
        <circle
          r={3.75}
          cy={36.595806}
          cx={241.61198}
          id="circle1072"
          display="inline"
          opacity={1}
          fill="#f95"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          transform="matrix(.50256 0 0 .44492 221.948 -43.264)"
          ry={4.0336714}
          rx={4.2296953}
          cy={175.58884}
          cx={39.309525}
          id="ellipse1074"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <text
          id="text1078"
          y={38.61882}
          x={257.72595}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={38.61882}
            x={257.72595}
            id="tspan1076"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"MANTENIMIENTO"}
          </tspan>
        </text>
        <circle
          id="circle1080"
          cx={310.93274}
          cy={36.595806}
          r={3.75}
          display="inline"
          opacity={1}
          fill="#95f"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          id="ellipse1082"
          cx={39.309525}
          cy={175.58884}
          rx={4.2296953}
          ry={4.0336714}
          transform="matrix(.50256 0 0 .44492 291.27 -43.264)"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={327.04712}
          y={38.61882}
          id="text1086"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1084"
            x={327.04712}
            y={38.61882}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"ALARMA PREVENTIVA"}
          </tspan>
        </text>
        <circle
          r={3.75}
          cy={36.595806}
          cx={394.01318}
          id="circle1088"
          display="inline"
          opacity={1}
          fill="#fc0"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          transform="matrix(.50256 0 0 .44492 374.35 -43.264)"
          ry={4.0336714}
          rx={4.2296953}
          cy={175.58884}
          cx={39.309525}
          id="ellipse1090"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <text
          id="text1094"
          y={38.61882}
          x={411.18591}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={38.61882}
            x={411.18591}
            id="tspan1092"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"ALARMA EMERGENTE"}
          </tspan>
        </text>
        <circle
          id="circle1096"
          cx={478.15198}
          cy={36.595806}
          r={3.75}
          display="inline"
          opacity={1}
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          id="ellipse1098"
          cx={39.309525}
          cy={175.58884}
          rx={4.2296953}
          ry={4.0336714}
          transform="matrix(.50256 0 0 .44492 458.487 -43.264)"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={494.79553}
          y={38.61882}
          id="text1102"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1100"
            x={494.79553}
            y={38.61882}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"APAGADO/STAND BAY"}
          </tspan>
        </text>
        <circle
          r={3.75}
          cy={36.595806}
          cx={562.82043}
          id="circle1104"
          display="inline"
          opacity={1}
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.0833686}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="stroke fill markers"
        />
        <ellipse
          transform="matrix(.50256 0 0 .44492 543.155 -43.264)"
          ry={4.0336714}
          rx={4.2296953}
          cy={175.58884}
          cx={39.309525}
          id="ellipse1106"
          display="inline"
          opacity={0.29}
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={2.01348}
          strokeLinecap="square"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
          filter="url(#filter2091-9-7)"
        />
        <path
          id="path1112"
          d="M253.428 26.527l6.314 2.352h98.488l6.743-2.512"
          fill="#0ceef7"
          fillOpacity={0.74902}
          stroke="none"
          strokeWidth=".21253px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          id="path1140"
          clipPath="url(#clipPath2072)"
          transform="matrix(.259 0 0 .26502 78.078 21.017)"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          transform="matrix(.259 0 0 .26502 141.578 21.017)"
          clipPath="url(#clipPath2072)"
          id="path1162"
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          id="path1164"
          clipPath="url(#clipPath2072)"
          transform="matrix(.259 0 0 .26502 210.9 21.017)"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          transform="matrix(.259 0 0 .26502 293.98 21.017)"
          clipPath="url(#clipPath2072)"
          id="path1166"
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          id="path1168"
          clipPath="url(#clipPath2072)"
          transform="matrix(.259 0 0 .26502 378.117 21.017)"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          transform="matrix(.259 0 0 .26502 462.784 21.017)"
          clipPath="url(#clipPath2072)"
          id="path1170"
          d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
          display="inline"
          opacity={0.75}
          fill="#168498"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#0deff7"
          strokeWidth={1.03457}
          strokeLinecap="square"
          strokeLinejoin="round"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          d="M561.124 10.253v12.228h32.4l4.576-5.778V4.676h-32.54z"
          id="path828"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.419107}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path830"
          d="M561.348 14.527v-4.05l4.278-5.45h3.27"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.838213}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M597.903 12.457v4.267l-4.33 5.745h-3.31"
          id="path832"
          display="inline"
          fill="none"
          stroke="#3fdbe4"
          strokeWidth={0.86569}
          strokeLinecap="round"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={564.11792}
          y={15.864633}
          id="text836"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan834"
            x={564.11792}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"CMDB"}
          </tspan>
        </text>
        <path
          d="M590.698 17.871a9.136 9.136 0 01-.05-.28 2.16 2.16 0 00-.046-.232 3.08 3.08 0 00-.407-.16c-.016 0-.129.07-.25.154-.205.143-.224.151-.282.12-.08-.042-.285-.263-.284-.304 0-.018.073-.134.162-.257l.16-.224-.045-.09a1.54 1.54 0 01-.077-.19c-.041-.134-.042-.135-.215-.165a16.596 16.596 0 01-.281-.052l-.132-.026v-.489l.27-.043c.148-.024.283-.057.299-.074a1.3 1.3 0 00.106-.216l.077-.186-.173-.242-.173-.241.167-.17c.096-.098.182-.165.201-.157.019.007.13.082.247.167l.212.154.181-.07a.87.87 0 00.221-.114c.022-.025.054-.14.072-.266.047-.322.036-.31.312-.31h.233l.046.29.045.29.166.075c.26.118.245.12.502-.065l.231-.166.07.05c.12.085.285.278.265.311-.01.017-.085.129-.167.249l-.149.217.083.204c.094.229.066.212.438.276l.226.04.007.245c.004.134.004.245 0 .245-.032 0-.58.1-.583.106a5.21 5.21 0 00-.166.405c0 .012.074.122.164.244.09.123.163.232.163.243 0 .03-.31.333-.342.333-.014 0-.129-.072-.255-.161l-.229-.161-.194.084a.919.919 0 00-.212.114c-.01.016-.039.147-.064.29l-.047.262h-.245c-.234 0-.246-.003-.258-.057zm.421-1.262a.68.68 0 00.507-.462.642.642 0 00-.038-.527c-.218-.434-.734-.54-1.094-.223a.655.655 0 00-.236.533c0 .394.267.669.691.713.015.002.091-.014.17-.034zm-3.638.913c-1.036-.052-2.008-.318-2.597-.71-.213-.142-.43-.367-.521-.544l-.071-.137-.008-.616-.008-.615.05.096c.322.628 1.317 1.104 2.636 1.262.174.021.539.045.81.054l.492.015.03.081a.72.72 0 00.305.358c.118.065.124.073.106.144-.024.098-.022.405.003.506l.02.08-.184.015c-.307.024-.719.03-1.064.011zm.226-1.682c-.664-.023-1.025-.067-1.508-.185-.96-.234-1.606-.618-1.848-1.097l-.074-.147V13.196l.076.15c.244.49.99.906 2.01 1.125.515.11.932.147 1.652.146h.66v.098c0 .054.017.14.037.19.021.05.035.092.032.095l-.124.071a.734.734 0 00-.373.647c0 .073-.012.122-.031.128a7.068 7.068 0 01-.509-.006zm-.638-1.72c-.875-.1-1.732-.37-2.21-.695-.187-.127-.418-.377-.497-.538l-.072-.148-.006-.603-.006-.603.063.118c.089.167.34.427.542.561.447.297 1.038.501 1.87.644.292.05.415.057 1.143.058.872.001.992-.01 1.658-.153.966-.206 1.706-.637 1.94-1.128l.071-.15V12c0 .285-.011.58-.026.654-.03.158-.122.353-.22.465l-.068.078-.34.015c-.435.019-.536.053-.702.235a.76.76 0 00-.142.198c-.018.072-.015.072-.16.02a.681.681 0 00-.491.042c-.062.03-.194.13-.294.224l-.183.17-.32.029c-.414.037-1.184.032-1.55-.01zm.261-1.67a6.509 6.509 0 01-1.696-.34c-1.291-.45-1.718-1.27-1.004-1.93.48-.443 1.341-.76 2.402-.884.418-.048 1.35-.05 1.76-.001.774.091 1.328.245 1.88.522.412.207.675.447.807.735.056.122.064.169.056.335-.011.24-.075.378-.266.585-.532.574-1.752.958-3.135.986-.262.005-.624.002-.804-.01z"
          id="path986"
          fill="#00aad4"
          strokeWidth={0.0251273}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={584.69849}
          y={16.38105}
          id="cmdb"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="red"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan868"
            x={584.69849}
            y={16.38105}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="red"
            fillOpacity={0}
            strokeWidth={0.264583}
          >
            <a href={cmdb_url} target="_blank">
                {' '}
                {'CM'}
              </a>
          </tspan>
        </text>
        <text
          id="text1173"
          y={15.864633}
          x={309.64322}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={309.64322}
            id="tspan1171"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={chill_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'CHIL'}
              </a>
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={517.55054}
          y={15.864633}
          id="text1177"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1175"
            x={517.55054}
            y={15.864633}
            style={{
              
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={hist_url} target="_blank" style={{ fill: '#00abd6' }}>
                {' '}
                {'HISTORIAN'}
              </a>
          </tspan>
        </text>
        <text
          id="text1976"
          y={15.864633}
          x={103.73995}
          style={{
            lineHeight: 1.25
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87777px"
          fontFamily="sans-serif"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              
            }}
            y={15.864633}
            x={103.73995}
            id="tspan1974"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.35px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            <a href={pm_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'PM'}
              </a>
          </tspan>
        </text>
      </g>
    </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
