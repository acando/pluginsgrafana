import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory} from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  //nombre
  //firstNotNull  lastNotNull
  let fecha: any = data.timeRange.raw.from;
  let tiempo = '';
  let unidades = '';
  let trafo = 4.7;
  let h = 1;
  let d = 1;
  if (typeof fecha === 'string') {
    tiempo = fecha.replace(/[-now.mdyh*+?^${}()|[\]\\]/g, '');
    unidades = fecha.replace(/[-now.1234567890*+?^${}()|[\]\\]/g, '');
  }
  if (typeof fecha === 'object') {
    let fecha: any = data.timeRange.raw;
    let fecha_in = fecha.from._d;
    let fecha_out = fecha.to._d;
    var difference = Math.abs(fecha_in - fecha_out);
    console.log('UNO', fecha_in, fecha_out, difference / (1000 * 3600 * 24));
    tiempo = '' + difference / (1000 * 3600 * 24);
    unidades = 'd';
  }
  let tiempo2 = parseInt(tiempo);

  if (unidades === 'm') {
    h = 1;
    d = 1;
  } else if (unidades === 'h') {
    h = tiempo2;
    d = 1;
  } else if (unidades === 'd') {
    h = tiempo2;
    d = 24;
  }
  let dse_pot_ini = data.series.find(({ name }) => name === 'DSE_POT')?.fields[1].state?.calcs?.firstNotNull;
  let dse_pot_fin = data.series.find(({ name }) => name === 'DSE_POT')?.fields[1].state?.calcs?.lastNotNull;
  let dse_pot = dse_pot_fin - dse_pot_ini;
  console.log('pot dse', dse_pot);
  let pot_trafo = trafo * h * d;
  console.log("fechas",h,d)
  let tdpsis1_pot = data.series.find(({ name }) => name === 'TDPSIS1_POT')?.fields[1].state?.calcs?.mean;
  let tdpsis2_pot = data.series.find(({ name }) => name === 'TDPSIS2_POT')?.fields[1].state?.calcs?.mean;
  let tdpsis = tdpsis1_pot + tdpsis2_pot;
  let tdpsis_consumo = (tdpsis * h * d).toFixed(1);

  let gen4_ini = data.series.find(({ name }) => name === 'GEN4_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen4_fin = data.series.find(({ name }) => name === 'GEN4_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen4_pot = gen4_fin - gen4_ini;
  let gen5_ini = data.series.find(({ name }) => name === 'GEN5_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen5_fin = data.series.find(({ name }) => name === 'GEN5_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen5_pot = gen5_fin - gen5_ini;
  let gen6_ini = data.series.find(({ name }) => name === 'GEN6_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen6_fin = data.series.find(({ name }) => name === 'GEN6_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen6_pot = gen6_fin - gen6_ini;
  let gen10_ini = data.series.find(({ name }) => name === 'GEN10_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen10_fin = data.series.find(({ name }) => name === 'GEN10_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen10_pot = gen10_fin - gen10_ini;
  let gen11_ini = data.series.find(({ name }) => name === 'GEN11_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen11_fin = data.series.find(({ name }) => name === 'GEN11_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen11_pot = gen11_fin - gen11_ini;
  let gen12_ini = data.series.find(({ name }) => name === 'GEN12_POT')?.fields[1].state?.calcs?.firstNotNull;
  let gen12_fin = data.series.find(({ name }) => name === 'GEN12_POT')?.fields[1].state?.calcs?.lastNotNull;
  let gen12_pot = gen12_fin - gen12_ini;
  let totGen=gen4_pot+gen5_pot+gen6_pot+gen10_pot+gen11_pot+gen12_pot;
  console.log("pot generadores",gen4_pot,gen5_pot,gen6_pot,gen10_pot,gen11_pot,gen12_pot)

  let tdb0_pot = validar(data.series.find(({ name }) => name === 'TDB0_POT')?.fields[1].state?.calcs?.mean);
  tdb0_pot=parseFloat((tdb0_pot*h*d).toFixed(1));
  let tdp2s1_pot = data.series.find(({ name }) => name === 'TDP2S1_POT')?.fields[1].state?.calcs?.mean;
  let tdp2s2_pot = data.series.find(({ name }) => name === 'TDP2S2_POT')?.fields[1].state?.calcs?.mean;
  let tdps1_pot = data.series.find(({ name }) => name === 'TDPS1_POT')?.fields[1].state?.calcs?.sum;
  let tdps2_pot = data.series.find(({ name }) => name === 'TDPS2_POT')?.fields[1].state?.calcs?.sum;
  let tchis1_pot = data.series.find(({ name }) => name === 'TCHIS1_POT')?.fields[1].state?.calcs?.mean;
  if(tchis1_pot===null ||tchis1_pot===0){
    tchis1_pot=0;
  }else{
    tchis1_pot=(tchis1_pot/10)*h*d;
  }
  let tchis2_pot = data.series.find(({ name }) => name === 'TCHIS2_POT')?.fields[1].state?.calcs?.mean;
  if(tchis2_pot===null ||tchis2_pot===0){
    tchis2_pot=0;
  }else{
    tchis2_pot=(tchis2_pot/10)*h*d;
  }
  //let aacc_pot = data.series.find(({ name }) => name === 'AACC_POT')?.fields[1].state?.calcs?.sum;
  let tsg_pot = data.series.find(({ name }) => name === 'POT_SSGG')?.fields[1].state?.calcs?.mean;
  let tsg_pot_pm = data.series.find(({ name }) => name === 'PM_TOTKW')?.fields[1].state?.calcs?.mean;
  
  if(tsg_pot===null ||tsg_pot===0){
    tsg_pot=0;
  }else{
    tsg_pot=(tsg_pot/10)*h*d;;
  }
  let tpdus1_pot = data.series.find(({ name }) => name === 'TPDUS1_POT')?.fields[1].state?.calcs?.mean;
  if(tpdus1_pot===null ||tpdus1_pot===0){
    tpdus1_pot=0;
  }else{
    tpdus1_pot=(tpdus1_pot/10)*h*d;
  }
  let tpdus2_pot = data.series.find(({ name }) => name === 'TPDUS2_POT')?.fields[1].state?.calcs?.mean;
  if(tpdus2_pot===null ||tpdus2_pot===0){
    tpdus2_pot=0;
  }else{
    tpdus2_pot=(tpdus2_pot/10)*h*d;
  }

  let tmin = data.series.find(({ name }) => name === 'TEMP')?.fields[1].state?.calcs?.min;
  let tmax = data.series.find(({ name }) => name === 'TEMP')?.fields[1].state?.calcs?.max;
  let hmin = data.series.find(({ name }) => name === 'HUM')?.fields[1].state?.calcs?.min;
  let hmax = data.series.find(({ name }) => name === 'HUM')?.fields[1].state?.calcs?.min + 122;
  let ups10_pot = data.series.find(({ name }) => name === 'UPS10_POT')?.fields[1].state?.calcs?.mean;
  if(ups10_pot===null ||ups10_pot===0){
    ups10_pot=0;
  }else{
    ups10_pot=ups10_pot/1000;
  }
  let tups_in1 = validar(data.series.find(({ name }) => name === 'POT_TUPSIN1')?.fields[1].state?.calcs?.mean)/10;
  let tups_in2 = validar(data.series.find(({ name }) => name === 'POT_TUPSIN2')?.fields[1].state?.calcs?.mean)/10;
  let tups_out1 = validar(data.series.find(({ name }) => name === 'POT_TUPSOUT1')?.fields[1].state?.calcs?.mean)/10;
  let tups_out2 = validar(data.series.find(({ name }) => name === 'POT_TUPSOUT2')?.fields[1].state?.calcs?.mean)/10;
  let tupss1_pot = (tups_in1 - tups_out1)*h*d;
  let tupss2_pot = (tups_in2 - tups_out2)*h*d;
  let pdu2_p1a = data.series.find(({ name }) => name === 'PDU1B_F2_P1A')?.fields[1].state?.calcs?.mean;
  let pdu2_p1b = data.series.find(({ name }) => name === 'PDU1B_F2_P1B')?.fields[1].state?.calcs?.mean;
  let pdu2_p1c = data.series.find(({ name }) => name === 'PDU1B_F2_P1C')?.fields[1].state?.calcs?.mean;
  let pdu2_p2a = data.series.find(({ name }) => name === 'PDU1B_F2_P2A')?.fields[1].state?.calcs?.mean;
  let pdu2_p2b = data.series.find(({ name }) => name === 'PDU1B_F2_P2B')?.fields[1].state?.calcs?.mean;
  let pdu2_p2c = data.series.find(({ name }) => name === 'PDU1B_F2_P2C')?.fields[1].state?.calcs?.mean;
  let pdu3_p1a = data.series.find(({ name }) => name === 'PDU1B_F3_P1A')?.fields[1].state?.calcs?.mean;
  let pdu3_p1b = data.series.find(({ name }) => name === 'PDU1B_F3_P1B')?.fields[1].state?.calcs?.mean;
  let pdu3_p1c = data.series.find(({ name }) => name === 'PDU1B_F3_P1C')?.fields[1].state?.calcs?.mean;
  let pdu3_p2a = data.series.find(({ name }) => name === 'PDU1B_F3_P2A')?.fields[1].state?.calcs?.mean;
  let pdu3_p2b = data.series.find(({ name }) => name === 'PDU1B_F3_P2B')?.fields[1].state?.calcs?.mean;
  let pdu3_p2c = data.series.find(({ name }) => name === 'PDU1B_F3_P2C')?.fields[1].state?.calcs?.mean;
  let pdu4_p1a = data.series.find(({ name }) => name === 'PDU1B_F4_P1A')?.fields[1].state?.calcs?.mean;
  let pdu4_p1b = data.series.find(({ name }) => name === 'PDU1B_F4_P1B')?.fields[1].state?.calcs?.mean;
  let pdu4_p1c = data.series.find(({ name }) => name === 'PDU1B_F4_P1C')?.fields[1].state?.calcs?.mean;
  let pdu4_p2a = data.series.find(({ name }) => name === 'PDU1B_F4_P2A')?.fields[1].state?.calcs?.mean;
  let pdu4_p2b = data.series.find(({ name }) => name === 'PDU1B_F4_P2B')?.fields[1].state?.calcs?.mean;
  let pdu4_p2c = data.series.find(({ name }) => name === 'PDU1B_F4_P2C')?.fields[1].state?.calcs?.mean;
  let pdu5_p1a = data.series.find(({ name }) => name === 'PDU1B_F5_P1A')?.fields[1].state?.calcs?.mean;
  let pdu5_p1b = data.series.find(({ name }) => name === 'PDU1B_F5_P1B')?.fields[1].state?.calcs?.mean;
  let pdu5_p1c = data.series.find(({ name }) => name === 'PDU1B_F5_P1C')?.fields[1].state?.calcs?.mean;
  let pdu5_p2a = data.series.find(({ name }) => name === 'PDU1B_F5_P2A')?.fields[1].state?.calcs?.mean;
  let pdu5_p2b = data.series.find(({ name }) => name === 'PDU1B_F5_P2B')?.fields[1].state?.calcs?.mean;
  let pdu5_p2c = data.series.find(({ name }) => name === 'PDU1B_F5_P2C')?.fields[1].state?.calcs?.mean;
  let pdu6_p1a = data.series.find(({ name }) => name === 'PDU1B_F6_P1A')?.fields[1].state?.calcs?.mean;
  let pdu6_p1b = data.series.find(({ name }) => name === 'PDU1B_F6_P1B')?.fields[1].state?.calcs?.mean;
  let pdu6_p1c = data.series.find(({ name }) => name === 'PDU1B_F6_P1C')?.fields[1].state?.calcs?.mean;
  let pdu6_p2a = data.series.find(({ name }) => name === 'PDU1B_F6_P2A')?.fields[1].state?.calcs?.mean;
  let pdu6_p2b = data.series.find(({ name }) => name === 'PDU1B_F6_P2B')?.fields[1].state?.calcs?.mean;
  let pdu6_p2c = data.series.find(({ name }) => name === 'PDU1B_F6_P2C')?.fields[1].state?.calcs?.mean;
  let pdu7_p1a = data.series.find(({ name }) => name === 'PDU1B_F7_P1A')?.fields[1].state?.calcs?.mean;
  let pdu7_p1b = data.series.find(({ name }) => name === 'PDU1B_F7_P1B')?.fields[1].state?.calcs?.mean;
  let pdu7_p1c = data.series.find(({ name }) => name === 'PDU1B_F7_P1C')?.fields[1].state?.calcs?.mean;
  let pdu7_p2a = data.series.find(({ name }) => name === 'PDU1B_F7_P2A')?.fields[1].state?.calcs?.mean;
  let pdu7_p2b = data.series.find(({ name }) => name === 'PDU1B_F7_P2B')?.fields[1].state?.calcs?.mean;
  let pdu7_p2c = data.series.find(({ name }) => name === 'PDU1B_F7_P2C')?.fields[1].state?.calcs?.mean;
  let pdu8_p1a = data.series.find(({ name }) => name === 'PDU1B_F8_P1A')?.fields[1].state?.calcs?.mean;
  let pdu8_p1b = data.series.find(({ name }) => name === 'PDU1B_F8_P1B')?.fields[1].state?.calcs?.mean;
  let pdu8_p1c = data.series.find(({ name }) => name === 'PDU1B_F8_P1C')?.fields[1].state?.calcs?.mean;
  let pdu8_p2a = data.series.find(({ name }) => name === 'PDU1B_F8_P2A')?.fields[1].state?.calcs?.mean;
  let pdu8_p2b = data.series.find(({ name }) => name === 'PDU1B_F8_P2B')?.fields[1].state?.calcs?.mean;
  let pdu8_p2c = data.series.find(({ name }) => name === 'PDU1B_F8_P2C')?.fields[1].state?.calcs?.mean;
  let pdu9_p1a = data.series.find(({ name }) => name === 'PDU1B_F9_P1A')?.fields[1].state?.calcs?.mean;
  let pdu9_p1b = data.series.find(({ name }) => name === 'PDU1B_F9_P1B')?.fields[1].state?.calcs?.mean;
  let pdu9_p1c = data.series.find(({ name }) => name === 'PDU1B_F9_P1C')?.fields[1].state?.calcs?.mean;
  let pdu9_p2a = data.series.find(({ name }) => name === 'PDU1B_F9_P2A')?.fields[1].state?.calcs?.mean;
  let pdu9_p2b = data.series.find(({ name }) => name === 'PDU1B_F9_P2B')?.fields[1].state?.calcs?.mean;
  let pdu9_p2c = data.series.find(({ name }) => name === 'PDU1B_F9_P2C')?.fields[1].state?.calcs?.mean;
  let pdu22_p1a = data.series.find(({ name }) => name === 'PDU2B_F2_P1A')?.fields[1].state?.calcs?.mean;
  let pdu22_p1b = data.series.find(({ name }) => name === 'PDU2B_F2_P1B')?.fields[1].state?.calcs?.mean;
  let pdu22_p1c = data.series.find(({ name }) => name === 'PDU2B_F2_P1C')?.fields[1].state?.calcs?.mean;
  let pdu22_p2a = data.series.find(({ name }) => name === 'PDU2B_F2_P2A')?.fields[1].state?.calcs?.mean;
  let pdu22_p2b = data.series.find(({ name }) => name === 'PDU2B_F2_P2B')?.fields[1].state?.calcs?.mean;
  let pdu22_p2c = data.series.find(({ name }) => name === 'PDU2B_F2_P2C')?.fields[1].state?.calcs?.mean;
  let pdu23_p1a = data.series.find(({ name }) => name === 'PDU2B_F3_P1A')?.fields[1].state?.calcs?.mean;
  let pdu23_p1b = data.series.find(({ name }) => name === 'PDU2B_F3_P1B')?.fields[1].state?.calcs?.mean;
  let pdu23_p1c = data.series.find(({ name }) => name === 'PDU2B_F3_P1C')?.fields[1].state?.calcs?.mean;
  let pdu23_p2a = data.series.find(({ name }) => name === 'PDU2B_F3_P2A')?.fields[1].state?.calcs?.mean;
  let pdu23_p2b = data.series.find(({ name }) => name === 'PDU2B_F3_P2B')?.fields[1].state?.calcs?.mean;
  let pdu23_p2c = data.series.find(({ name }) => name === 'PDU2B_F3_P2C')?.fields[1].state?.calcs?.mean;
  let pdu24_p1a = data.series.find(({ name }) => name === 'PDU2B_F4_P1A')?.fields[1].state?.calcs?.mean;
  let pdu24_p1b = data.series.find(({ name }) => name === 'PDU2B_F4_P1B')?.fields[1].state?.calcs?.mean;
  let pdu24_p1c = data.series.find(({ name }) => name === 'PDU2B_F4_P1C')?.fields[1].state?.calcs?.mean;
  let pdu24_p2a = data.series.find(({ name }) => name === 'PDU2B_F4_P2A')?.fields[1].state?.calcs?.mean;
  let pdu24_p2b = data.series.find(({ name }) => name === 'PDU2B_F4_P2B')?.fields[1].state?.calcs?.mean;
  let pdu24_p2c = data.series.find(({ name }) => name === 'PDU2B_F4_P2C')?.fields[1].state?.calcs?.mean;
  let pdu25_p1a = data.series.find(({ name }) => name === 'PDU2B_F5_P1A')?.fields[1].state?.calcs?.mean;
  let pdu25_p1b = data.series.find(({ name }) => name === 'PDU2B_F5_P1B')?.fields[1].state?.calcs?.mean;
  let pdu25_p1c = data.series.find(({ name }) => name === 'PDU2B_F5_P1C')?.fields[1].state?.calcs?.mean;
  let pdu25_p2a = data.series.find(({ name }) => name === 'PDU2B_F5_P2A')?.fields[1].state?.calcs?.mean;
  let pdu25_p2b = data.series.find(({ name }) => name === 'PDU2B_F5_P2B')?.fields[1].state?.calcs?.mean;
  let pdu25_p2c = data.series.find(({ name }) => name === 'PDU2B_F5_P2C')?.fields[1].state?.calcs?.mean;
  let pdu26_p1a = data.series.find(({ name }) => name === 'PDU2B_F6_P1A')?.fields[1].state?.calcs?.mean;
  let pdu26_p1b = data.series.find(({ name }) => name === 'PDU2B_F6_P1B')?.fields[1].state?.calcs?.mean;
  let pdu26_p1c = data.series.find(({ name }) => name === 'PDU2B_F6_P1C')?.fields[1].state?.calcs?.mean;
  let pdu26_p2a = data.series.find(({ name }) => name === 'PDU2B_F6_P2A')?.fields[1].state?.calcs?.mean;
  let pdu26_p2b = data.series.find(({ name }) => name === 'PDU2B_F6_P2B')?.fields[1].state?.calcs?.mean;
  let pdu26_p2c = data.series.find(({ name }) => name === 'PDU2B_F6_P2C')?.fields[1].state?.calcs?.mean;
  let pdu27_p1a = data.series.find(({ name }) => name === 'PDU2B_F7_P1A')?.fields[1].state?.calcs?.mean;
  let pdu27_p1b = data.series.find(({ name }) => name === 'PDU2B_F7_P1B')?.fields[1].state?.calcs?.mean;
  let pdu27_p1c = data.series.find(({ name }) => name === 'PDU2B_F7_P1C')?.fields[1].state?.calcs?.mean;
  let pdu27_p2a = data.series.find(({ name }) => name === 'PDU2B_F7_P2A')?.fields[1].state?.calcs?.mean;
  let pdu27_p2b = data.series.find(({ name }) => name === 'PDU2B_F7_P2B')?.fields[1].state?.calcs?.mean;
  let pdu27_p2c = data.series.find(({ name }) => name === 'PDU2B_F7_P2C')?.fields[1].state?.calcs?.mean;
  let pdu28_p1a = data.series.find(({ name }) => name === 'PDU2B_F8_P1A')?.fields[1].state?.calcs?.mean;
  let pdu28_p1b = data.series.find(({ name }) => name === 'PDU2B_F8_P1B')?.fields[1].state?.calcs?.mean;
  let pdu28_p1c = data.series.find(({ name }) => name === 'PDU2B_F8_P1C')?.fields[1].state?.calcs?.mean;
  let pdu28_p2a = data.series.find(({ name }) => name === 'PDU2B_F8_P2A')?.fields[1].state?.calcs?.mean;
  let pdu28_p2b = data.series.find(({ name }) => name === 'PDU2B_F8_P2B')?.fields[1].state?.calcs?.mean;
  let pdu28_p2c = data.series.find(({ name }) => name === 'PDU2B_F8_P2C')?.fields[1].state?.calcs?.mean;
  let pdu29_p1a = data.series.find(({ name }) => name === 'PDU2B_F9_P1A')?.fields[1].state?.calcs?.mean;
  let pdu29_p1b = data.series.find(({ name }) => name === 'PDU2B_F9_P1B')?.fields[1].state?.calcs?.mean;
  let pdu29_p1c = data.series.find(({ name }) => name === 'PDU2B_F9_P1C')?.fields[1].state?.calcs?.mean;
  let pdu29_p2a = data.series.find(({ name }) => name === 'PDU2B_F9_P2A')?.fields[1].state?.calcs?.mean;
  let pdu29_p2b = data.series.find(({ name }) => name === 'PDU2B_F9_P2B')?.fields[1].state?.calcs?.mean;
  let pdu29_p2c = data.series.find(({ name }) => name === 'PDU2B_F9_P2C')?.fields[1].state?.calcs?.mean;

  let tot_pdu1bf2 = pdu2_p1a + pdu2_p1b + pdu2_p1c + pdu2_p2a + pdu2_p2b + pdu2_p2c;
  let tot_pdu2bf2 = pdu22_p1a + pdu22_p1b + pdu22_p1c + pdu22_p2a + pdu22_p2b + pdu22_p2c;
  let tot_pdu1bf3 = pdu3_p1a + pdu3_p1b + pdu3_p1c + pdu3_p2a + pdu3_p2b + pdu3_p2c;
  let tot_pdu2bf3 = pdu23_p1a + pdu23_p1b + pdu23_p1c + pdu23_p2a + pdu23_p2b + pdu23_p2c;
  let tot_pdu1bf4 = pdu4_p1a + pdu4_p1b + pdu4_p1c + pdu4_p2a + pdu4_p2b + pdu4_p2c;
  let tot_pdu2bf4 = pdu24_p1a + pdu24_p1b + pdu24_p1c + pdu24_p2a + pdu24_p2b + pdu24_p2c;
  let tot_pdu1bf5 = pdu5_p1a + pdu5_p1b + pdu5_p1c + pdu5_p2a + pdu5_p2b + pdu5_p2c;
  let tot_pdu2bf5 = pdu25_p1a + pdu25_p1b + pdu25_p1c + pdu25_p2a + pdu25_p2b + pdu25_p2c;
  let tot_pdu1bf6 = pdu6_p1a + pdu6_p1b + pdu6_p1c + pdu6_p2a + pdu6_p2b + pdu6_p2c;
  let tot_pdu2bf6 = pdu26_p1a + pdu26_p1b + pdu26_p1c + pdu26_p2a + pdu26_p2b + pdu26_p2c;
  let tot_pdu1bf7 = pdu7_p1a + pdu7_p1b + pdu7_p1c + pdu7_p2a + pdu7_p2b + pdu7_p2c;
  let tot_pdu2bf7 = pdu27_p1a + pdu27_p1b + pdu27_p1c + pdu27_p2a + pdu27_p2b + pdu27_p2c;
  let tot_pdu1bf8 = pdu8_p1a + pdu8_p1b + pdu8_p1c + pdu8_p2a + pdu8_p2b + pdu8_p2c;
  let tot_pdu2bf8 = pdu28_p1a + pdu28_p1b + pdu28_p1c + pdu28_p2a + pdu28_p2b + pdu28_p2c;
  let tot_pdu1bf9 = pdu9_p1a + pdu9_p1b + pdu9_p1c + pdu9_p2a + pdu9_p2b + pdu9_p2c;
  let tot_pdu2bf9 = pdu29_p1a + pdu29_p1b + pdu29_p1c + pdu29_p2a + pdu29_p2b + pdu29_p2c;

  let pdu_f2 = (tot_pdu1bf2 + tot_pdu2bf2)/10;
  let pdu_f3 = (tot_pdu1bf3 + tot_pdu2bf3)/10;
  let pdu_f4 = (tot_pdu1bf4 + tot_pdu2bf4)/10;
  let pdu_f5 = (tot_pdu1bf5 + tot_pdu2bf5)/10;
  let pdu_f6 = (tot_pdu1bf6 + tot_pdu2bf6)/10;
  let pdu_f7 = (tot_pdu1bf7 + tot_pdu2bf7)/10;
  let pdu_f8 = (tot_pdu1bf8 + tot_pdu2bf8)/10;
  let pdu_f9 = (tot_pdu1bf9 + tot_pdu2bf9)/10;

  let tot_pow_pdu = pdu_f2 + pdu_f3 + pdu_f4 + pdu_f5 + pdu_f6 + pdu_f7 + pdu_f8 + pdu_f9 ;

  let pdu_f2_consumo = (pdu_f2*h*d).toFixed(1);
  let pdu_f3_consumo = (pdu_f3*h*d).toFixed(1);
  let pdu_f4_consumo = (pdu_f4*h*d).toFixed(1);
  let pdu_f5_consumo = (pdu_f5*h*d).toFixed(1);
  let pdu_f6_consumo = (pdu_f6*h*d).toFixed(1);
  let pdu_f9_consumo = (pdu_f9*h*d).toFixed(1);
  
  let precio = data.series.find(({ name }) => name === 'PRECIO')?.fields[1].state?.calcs?.lastNotNull;
  let pot_chi = data.series.find(({ name }) => name === 'POT_CHI')?.fields[1].state?.calcs?.lastNotNull;
  if (pot_chi === null || pot_chi === undefined){
    pot_chi = 0;
  }else{
    pot_chi = (pot_chi/1000);
  }
  let pot_var = data.series.find(({ name }) => name === 'POT_VAR')?.fields[1].state?.calcs?.mean;
  if(pot_var === null || pot_var === undefined){
    pot_var = 0; 
  }else {
    pot_var = pot_var +((-0.0263)*(Math.pow(pot_var,6)) + 0.6645*(Math.pow(pot_var,5)) - 6.6366*(Math.pow(pot_var,4)) + 33.182*(Math.pow(pot_var,3)) - 86.277*(Math.pow(pot_var,2)) + 108.46*(pot_var) - 48.82);
    pot_var = (pot_var);
  }

  let pot_uma1 = data.series.find(({ name }) => name === 'POT1_UMA')?.fields[1].state?.calcs?.mean;
  let pot_uma2 = data.series.find(({ name }) => name === 'POT2_UMA')?.fields[1].state?.calcs?.mean;
  let cur_rect1 = validar(data.series.find(({ name }) => name === 'CUR_RECT1')?.fields[1].state?.calcs?.mean);
  let vol_rect1 = validar(data.series.find(({ name }) => name === 'VOL_RECT1')?.fields[1].state?.calcs?.mean);
  let cur_rect2 = validar(data.series.find(({ name }) => name === 'CUR_RECT2')?.fields[1].state?.calcs?.mean);
  let vol_rect2 = validar(data.series.find(({ name }) => name === 'VOL_RECT2')?.fields[1].state?.calcs?.mean);
  let pot_umas = (pot_uma1 + pot_uma2) - pot_chi - pot_var;

  let elec_consumo = (tdb0_pot).toFixed(1);
  console.log(dse_pot,totGen)
  let elec_gasto = (parseFloat(elec_consumo) * precio).toFixed(1);
  let aacc_consumo = (tchis1_pot + tchis2_pot).toFixed(1);
  let aacc_gasto = (parseFloat(aacc_consumo) * precio).toFixed(1);
  let ti_consumo_antes = (tpdus1_pot + tpdus2_pot).toFixed(1);
  let ti_consumo = ((tot_pow_pdu*h*d)).toFixed(1);
  let pdu_perdida_consumo = ((ti_consumo_antes - parseFloat(ti_consumo))).toFixed(1);
  let pdu_per_gasto = (parseFloat(pdu_perdida_consumo) * precio).toFixed(1);
  console.log("ANTES:", ti_consumo_antes)
  
  let ups_trafo_consumo = (tupss1_pot + tupss2_pot + pot_trafo).toFixed(1);
  //let ups_trafo_gasto = (parseFloat(ups_trafo_consumo) * precio).toFixed(1);
  let tpro = (parseFloat(tmin + tmax) / 20).toFixed(0);
  let hpro = (parseFloat(hmin + hmax) / 20).toFixed(0);
  let rack_consumo = (parseFloat(ti_consumo) / 91).toFixed(1);
  
  let prehot_consumo = ((tdp2s1_pot + tdp2s2_pot) * d * h).toFixed(1);
  let prehot_gasto = (parseFloat(prehot_consumo) * precio).toFixed(1);
  tpro = tpro;
  hpro = hpro;

  let tdb0_consumo = (tdb0_pot-totGen).toFixed(1);
  if(parseFloat(tdb0_consumo)<0)tdb0_consumo='0'
  let tdb0_gasto = (parseFloat(tdb0_consumo) * precio).toFixed(1);
  let gen_consumo = (totGen).toFixed(1);
  if(parseFloat(gen_consumo)<0)gen_consumo='0'
  let gen_gasto = (parseFloat(gen_consumo) * precio).toFixed(1);

  let pot_rect1 = vol_rect1*cur_rect1;
  let pot_rect2 = vol_rect2*cur_rect2;
  //let pot_tot_rect = pot_rect1 +pot_rect2;

  let rect1_consumo=validar((pot_rect1*h*d)/1000);
  let rect2_consumo=validar((pot_rect2*h*d)/1000);
  let rect1_gasto=validar(rect1_consumo* precio);
  let rect2_gasto=validar(rect2_consumo* precio);

  ti_consumo=(parseFloat(ti_consumo)+rect1_consumo+rect2_consumo).toFixed(1)
  let ti_gasto = (parseFloat(ti_consumo) * precio).toFixed(1);
  let rack_gasto = (parseFloat(ti_gasto) / 91).toFixed(1);
  

  function validar(dato:any){
    if(dato===null||dato===undefined){
      dato=0;
    }else{
      dato=dato.toFixed(1)
    }
    return parseFloat(dato); 
  }

  if (parseFloat(gen_consumo) < 0) {
    tdb0_consumo = parseFloat(elec_consumo).toFixed(1);
    tdb0_gasto = (parseFloat(tdb0_consumo) * precio).toFixed(1);
    gen_consumo = (totGen * 0).toFixed(1);
    gen_gasto = (parseFloat(gen_consumo) * precio).toFixed(1);
  }

  console.log(tdpsis_consumo, tdb0_consumo, parseFloat(tdpsis_consumo) - parseFloat(tdb0_consumo));

  
  tmin = (tmin / 10).toFixed(0);
  tmax = (tmax / 10).toFixed(0);
  hmin = (hmin / 10).toFixed(0);
  hmax = (hmax / 10).toFixed(0);

  ///////
  let iluminacion = data.series.find(({ name }) => name === 'ILUM')?.fields[1].state?.calcs?.mean;
  let split_cur1 = data.series.find(({ name }) => name === 'AC_CUR1')?.fields[1].state?.calcs?.lastNotNull;
  let split_cur2 = data.series.find(({ name }) => name === 'AC_CUR2')?.fields[1].state?.calcs?.lastNotNull;
  let split_cur3 = data.series.find(({ name }) => name === 'AC_CUR3')?.fields[1].state?.calcs?.lastNotNull;
  let ilum_cur1 = data.series.find(({ name }) => name === 'ILUM_CUR1')?.fields[1].state?.calcs?.lastNotNull;
  let ilum_cur2 = data.series.find(({ name }) => name === 'ILUM_CUR2')?.fields[1].state?.calcs?.lastNotNull;
  let ilum_cur3 = data.series.find(({ name }) => name === 'ILUM_CUR3')?.fields[1].state?.calcs?.lastNotNull;
  let ups_cur1 = data.series.find(({ name }) => name === 'UPS_CUR1')?.fields[1].state?.calcs?.lastNotNull;
  let ups_cur2 = data.series.find(({ name }) => name === 'UPS_CUR2')?.fields[1].state?.calcs?.lastNotNull;
  let ups_cur3 = data.series.find(({ name }) => name === 'UPS_CUR3')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_cur1 = data.series.find(({ name }) => name === 'TOT_CUR1')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_cur2 = data.series.find(({ name }) => name === 'TOT_CUR2')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_cur3 = data.series.find(({ name }) => name === 'TOT_CUR3')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_vol1 = data.series.find(({ name }) => name === 'TOT_VOL1')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_vol2 = data.series.find(({ name }) => name === 'TOT_VOL2')?.fields[1].state?.calcs?.lastNotNull;
  let tsg_vol3 = data.series.find(({ name }) => name === 'TOT_VOL3')?.fields[1].state?.calcs?.lastNotNull;
  let cur1 = (split_cur1 + ilum_cur1 + ups_cur1);
  let cur2 = (split_cur2 + ilum_cur2 + ups_cur2);
  let cur3 = (split_cur3 + ilum_cur3 + ups_cur3);
  let toma_cur1 = tsg_cur1 - cur1;
  let toma_cur2 = tsg_cur2 - cur2;
  let toma_cur3 = tsg_cur3 - cur3;
  let cur_toma = (toma_cur1 + toma_cur2 + toma_cur3)/3;
  let vol_toma = (tsg_vol1 + tsg_vol2 + tsg_vol3)/3;
  let pot_toma = (cur_toma * vol_toma)/1000;
  console.log("corrientes:", cur1, cur2, cur3, tsg_cur1, tsg_cur2, tsg_cur3, cur_toma)
  console.log("voltajes:", tsg_vol1, tsg_vol2, tsg_vol3, vol_toma)

  let splits_curs = (split_cur1 + split_cur2 + split_cur3);
  if (splits_curs === null || splits_curs === undefined){
    splits_curs = 0;
  }else {
    splits_curs = (splits_curs/3).toFixed(1);
  }

  let pot_splits = (vol_toma * splits_curs)/1000;

  let ssgg_consumo = (tsg_pot_pm*h*d).toFixed(1);
  let ssgg_gasto = (parseFloat(ssgg_consumo) * precio).toFixed(1);
  let ups10_consumo = (ups10_pot*h*d).toFixed(1);
  let ups10_gasto = (parseFloat(ups10_consumo) * precio).toFixed(1);
  //let ilu2_consumo = ((ssgg_consumo-parseFloat(ups10_consumo)) * 0.62).toFixed(1);
  let ilu2_consumo = (iluminacion*h*d).toFixed(1);
  let ilu2_gasto = (parseFloat(ilu2_consumo) * precio).toFixed(1);
  //let tomac_consumo = ((ssgg_consumo-parseFloat(ups10_consumo)) * 0.08).toFixed(1);
  let tomac_consumo = (pot_toma*h*d).toFixed(1);
  let tomac_gasto = (parseFloat(tomac_consumo) * precio).toFixed(1);
  //let split_consumo = ((ssgg_consumo-parseFloat(ups10_consumo)) * 0.30).toFixed(1);
  let split_consumo = (pot_splits*h*d).toFixed(1);
  let split_gasto = (parseFloat(split_consumo) * precio).toFixed(1);

  ///////
  //let chill_consumo = (parseFloat(aacc_consumo) * 0.52).toFixed(1);
  let chill_consumo = (parseFloat(pot_chi)*h*d).toFixed(1);
  let chill_gasto = (parseFloat(chill_consumo) * precio).toFixed(1);
  let umas_consumo = (pot_umas*h*d).toFixed(1);
  let umas_gasto = (parseFloat(umas_consumo) * precio).toFixed(1);
  let var_consumo = (parseFloat(pot_var)*h*d).toFixed(1);
  let var_gasto = (parseFloat(var_consumo) * precio).toFixed(1);
  //////
  let recttot_consumo= ((rect1_consumo/0.93)+(rect2_consumo/0.89)-(rect1_consumo+rect2_consumo)).toFixed(1)
  let recttot_gasto= (parseFloat(recttot_consumo)*precio).toFixed(1)

  let sep_consumo = (parseFloat(ups_trafo_consumo) + parseFloat(prehot_consumo)+parseFloat(recttot_consumo)+parseFloat(pdu_perdida_consumo)).toFixed(1);
  
  let trafo_consumo = pot_trafo.toFixed(1);
  let trafo_gasto = (parseFloat(trafo_consumo) * precio).toFixed(1);
  let ups200_consumo = (tupss1_pot + tupss2_pot ).toFixed(1);
  let ups200_gasto = (parseFloat(ups200_consumo) * precio).toFixed(1);
  let total = parseFloat(elec_consumo);
  let total_datos = parseFloat(ssgg_consumo) + parseFloat(sep_consumo) + parseFloat(ti_consumo) + parseFloat(aacc_consumo);

  let cable_consumo= (total-total_datos).toFixed(1);
  if(parseFloat(cable_consumo)<0){
    cable_consumo="0";
  }

  let cable_gasto= (parseFloat(cable_consumo)*precio).toFixed(1);

  sep_consumo = (parseFloat(sep_consumo) + parseFloat(cable_consumo)).toFixed(1);
  let sep_gasto = (parseFloat(sep_consumo) * precio).toFixed(1);
  console.log(total,tdb0_consumo,cable_consumo)

  let ssgg_per = ((parseFloat(ssgg_consumo) / total) * 100).toFixed(1);
  let ssgg_per1 = ((parseFloat(ilu2_consumo) / total) * 100).toFixed(2);
  let ssgg_per2 = ((parseFloat(tomac_consumo) / total) * 100).toFixed(2);
  let ssgg_per3 = ((parseFloat(ups10_consumo) / total) * 100).toFixed(2);
  let ssgg_per4 = ((parseFloat(split_consumo) / total) * 100).toFixed(2);

  let sep_per = ((parseFloat(sep_consumo) / total) * 100).toFixed(1);
  let sep_per1 = ((parseFloat(ups200_consumo) / total) * 100).toFixed(2);
  let sep_per2 = ((parseFloat(prehot_consumo) / total) * 100).toFixed(2);
  let sep_per3 = ((parseFloat(trafo_consumo) / total) * 100).toFixed(2);
  let sep_per4 = ((parseFloat(cable_consumo) / total) * 100).toFixed(2);
  let sep_per5 = ((parseFloat(recttot_consumo) / total) * 100).toFixed(2);
  let sep_per6 = ((parseFloat(pdu_perdida_consumo) / total) * 100).toFixed(2);

  let aacc_per = ((parseFloat(aacc_consumo) / total) * 100).toFixed(1);
  let aacc_per1 = ((parseFloat(chill_consumo) / total) * 100).toFixed(2);
  let aacc_per2 = ((parseFloat(umas_consumo) / total) * 100).toFixed(2);
  let aacc_per3 = ((parseFloat(var_consumo) / total) * 100).toFixed(2);
  //let comb_per = ((parseFloat(comb_consumo) / total) * 100).toFixed(1);
  let pue = ((tdps1_pot + tdps2_pot) / (tpdus1_pot + tpdus2_pot)).toFixed(2);
  let dcie = ((1 / parseFloat(pue)) * 100).toFixed(2);
  dcie = dcie;

  ///// CUARTO TI FILAS
  
  let fila2_gasto = ((pdu_f2_consumo)*precio).toFixed(1);
  let fila3_gasto = ((pdu_f3_consumo)*precio).toFixed(1);
  let fila4_gasto = ((pdu_f6_consumo)*precio).toFixed(1);
  let fila5_gasto = ((pdu_f5_consumo)*precio).toFixed(1);
  let fila6_gasto = ((pdu_f6_consumo)*precio).toFixed(1);
  let fila9_gasto = ((pdu_f9_consumo)*precio).toFixed(1);
  
  
  let fila2_consumo = (parseFloat(rack_consumo)*3).toFixed(1);
  let fila3_consumo = (parseFloat(rack_consumo)*17).toFixed(1);
  let fila4_consumo = (parseFloat(rack_consumo)*21).toFixed(1);
  let fila5_consumo = (parseFloat(rack_consumo)*19).toFixed(1);
  let fila6_consumo = (parseFloat(rack_consumo)*18).toFixed(1);
  let fila9_consumo = (parseFloat(rack_consumo)*20).toFixed(1);

  let ti_per = ((parseFloat(ti_consumo) / total) * 100).toFixed(1);
  
  /*let f2_per = ((parseFloat(fila2_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f3_per = ((parseFloat(fila3_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f4_per = ((parseFloat(fila4_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f5_per = ((parseFloat(fila5_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f6_per = ((parseFloat(fila6_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f9_per = ((parseFloat(fila9_consumo)*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);*/

  let f2_per = ((pdu_f2_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f3_per = ((pdu_f3_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f4_per = ((pdu_f4_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f5_per = ((pdu_f5_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f6_per = ((pdu_f6_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);
  let f9_per = ((pdu_f9_consumo*parseFloat(ti_per))/parseFloat(ti_consumo)).toFixed(2);



  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      id="svg1468"
      viewBox="0 0 507.99999 185.20834"
      height={"100%"}
      width={"100%"}
      //{...props}
    >
      <defs id="defs1462">
        <linearGradient id="linearGradient7188">
          <stop id="stop7184" offset={0} stopColor="#08a" stopOpacity={1} />
          <stop id="stop7186" offset={1} stopColor="#045" stopOpacity={1} />
        </linearGradient>
        <linearGradient id="linearGradient7180">
          <stop id="stop7176" offset={0} stopColor="#08a" stopOpacity={1} />
          <stop id="stop7178" offset={1} stopColor="#045" stopOpacity={1} />
        </linearGradient>
        <linearGradient id="linearGradient7172">
          <stop id="stop7168" offset={0} stopColor="#045" stopOpacity={1} />
          <stop id="stop7170" offset={1} stopColor="#08a" stopOpacity={1} />
        </linearGradient>
        <linearGradient id="linearGradient7164">
          <stop id="stop7160" offset={0} stopColor="#08a" stopOpacity={1} />
          <stop id="stop7162" offset={1} stopColor="#045" stopOpacity={1} />
        </linearGradient>
        <linearGradient id="linearGradient7000">
          <stop id="stop6996" offset={0} stopColor="#194d5d" stopOpacity={1} />
          <stop
            id="stop6998"
            offset={1}
            stopColor="#194d5d"
            stopOpacity={0.45065793}
          />
        </linearGradient>
        <linearGradient id="linearGradient9988">
          <stop offset={0} id="stop9986" stopColor="#00f0f5" stopOpacity={1} />
        </linearGradient>
        <filter
          id="filter7141"
          x={-0.019802738}
          width={1.0396055}
          y={-0.12585447}
          height={1.2517089}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur stdDeviation={0.39814246} id="feGaussianBlur7143" />
        </filter>
        <filter
          height={1.0514778}
          y={-0.025738916}
          width={1.0581433}
          x={-0.029071641}
          id="filter8333"
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur id="feGaussianBlur8335" stdDeviation={0.26484327} />
        </filter>
        <filter
          id="filter7407"
          x={-0.055740872}
          width={1.1114817}
          y={-0.028531172}
          height={1.0570623}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur stdDeviation={0.80389736} id="feGaussianBlur7409" />
        </filter>
        <filter
          id="filter21682"
          x={-0.0018121826}
          width={1.0036244}
          y={-0.0087073163}
          height={1.0174146}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0024847713}
            id="feGaussianBlur21684"
          />
        </filter>
        <filter
          id="filter21678"
          x={-0.0021083336}
          width={1.0042167}
          y={-0.005198628}
          height={1.0103973}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0026413731}
            id="feGaussianBlur21680"
          />
        </filter>
        <filter
          id="filter21654"
          x={-0.0066299141}
          width={1.0132598}
          y={-0.0019386038}
          height={1.0038772}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0018919514}
            id="feGaussianBlur21656"
          />
        </filter>
        <filter
          id="filter21658"
          x={-0.0082067285}
          width={1.0164135}
          y={-0.0023996684}
          height={1.0047993}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0023419206}
            id="feGaussianBlur21660"
          />
        </filter>
        <filter
          id="filter21650"
          x={-0.0082067285}
          width={1.0164135}
          y={-0.0023996684}
          height={1.0047993}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0023419206}
            id="feGaussianBlur21652"
          />
        </filter>
        <filter
          id="filter21642"
          x={-0.0076571002}
          width={1.0153142}
          y={-0.0022389556}
          height={1.0044779}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0021850753}
            id="feGaussianBlur21644"
          />
        </filter>
        <filter
          id="filter21662"
          x={-0.0076571002}
          width={1.0153142}
          y={-0.0022389556}
          height={1.0044779}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0021850753}
            id="feGaussianBlur21664"
          />
        </filter>
        <filter
          id="filter21646"
          x={-0.0076571002}
          width={1.0153142}
          y={-0.0022389556}
          height={1.0044779}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0021850753}
            id="feGaussianBlur21648"
          />
        </filter>
        <filter
          id="filter21666"
          x={-0.0076571002}
          width={1.0153142}
          y={-0.0022389556}
          height={1.0044779}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0021850753}
            id="feGaussianBlur21668"
          />
        </filter>
        <filter
          id="filter21674"
          x={-0.0082067285}
          width={1.0164135}
          y={-0.0023996684}
          height={1.0047993}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0023419206}
            id="feGaussianBlur21676"
          />
        </filter>
        <filter
          id="filter21670"
          x={-0.0082067285}
          width={1.0164135}
          y={-0.0023996684}
          height={1.0047993}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.0023419206}
            id="feGaussianBlur21672"
          />
        </filter>
        <filter
          id="filter21611-1-15-7-08-1"
          x={-0.078807582}
          width={1.1568241}
          y={-0.092125793}
          height={1.1842516}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.05935181}
            id="feGaussianBlur21613-4-6-6-8-6"
          />
        </filter>
        <filter
          id="filter21611-1-15-7-08-11"
          x={-0.078807582}
          width={1.1568241}
          y={-0.092125793}
          height={1.1842516}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.05935181}
            id="feGaussianBlur21613-4-6-6-8-0"
          />
        </filter>
        <filter
          id="filter21611-1-15-7-08-8"
          x={-0.078807582}
          width={1.1568241}
          y={-0.092125793}
          height={1.1842516}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.05935181}
            id="feGaussianBlur21613-4-6-6-8-63"
          />
        </filter>
        <filter
          id="filter21611-1-15-7-08-2"
          x={-0.078807582}
          width={1.1568241}
          y={-0.092125793}
          height={1.1842516}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.05935181}
            id="feGaussianBlur21613-4-6-6-8-66"
          />
        </filter>
        <filter
          id="filter21611-1-15-7-08-0"
          x={-0.078807582}
          width={1.1568241}
          y={-0.092125793}
          height={1.1842516}
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            stdDeviation={0.05935181}
            id="feGaussianBlur21613-4-6-6-8-01"
          />
        </filter>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath5827-4">
          <path
            d="M283.523 291.878l2.004-2.374-14.6-9.72-9.657-1.543-1.082.109-.313 2.533z"
            id="path5829-9"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient id="linearGradient5782">
          <stop offset={0} id="stop5778" stopColor="#2c89aa" stopOpacity={1} />
          <stop offset={1} id="stop5780" stopColor="#2c89a0" stopOpacity={0} />
        </linearGradient>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath5827-9-3">
          <path
            d="M283.523 291.878l2.004-2.374-14.6-9.72-9.657-1.543-1.082.109-.313 2.533z"
            id="path5829-7-6"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6585-2">
          <path
            d="M192.49 373.533l22.82-4.18 86.916-23.655.801-9.77 11.133.61-12.591-42.09-16.624 1.123-92.616 27.372-10.153.531 1.147 26.478z"
            id="path6587-1"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath9908">
          <path
            d="M183.787 322.464l8.542.479.203 26.35 10.93 24.347-7.724 1.24-12.825-24.967z"
            id="path9910"
            fill="none"
            stroke="#000"
            strokeWidth=".210475px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath5827-4-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path5829-9-6"
            d="M283.523 291.878l2.004-2.374-14.6-9.72-9.657-1.543-1.082.109-.313 2.533z"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient
          gradientUnits="userSpaceOnUse"
          y2={294.75299}
          x2={238.0835}
          y1={267.95944}
          x1={231.43199}
          id="linearGradient5784-1"
          xlinkHref="#linearGradient5782"
          gradientTransform="matrix(.00725 1.15317 1.01345 -.0155 -110.36 -180.49)"
        />
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12540">
          <path
            d="M215.686 36.081l-6.949-6.414-35.546 27.529-7.751 29.934-.267 21.381 8.82 25.658s11.225 17.106 11.225 18.442c0 1.336 13.63 13.096 13.63 13.096l29.667 12.562 36.884 2.405 15.234-6.414-3.475-8.286-30.468 3.475-33.142-12.562-16.838-17.105-7.75-16.838-.802-16.57 1.336-27.262 10.424-20.58 14.7-16.838z"
            id="path12542"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12546">
          <path
            d="M190.5 47.436l2.835 3.213s-7.938 10.394-10.206 17.387c-2.267 6.992-5.67 14.174-5.67 14.174l-1.133 30.994-5.48.567 3.968 15.308s8.693 19.844 8.882 21.167c.19 1.322 13.797 13.985 14.93 15.497 1.134 1.511 18.71 9.449 18.71 9.449s28.349 4.347 29.86 5.103c1.512.756 24.569-1.134 24.569-1.134l9.638-5.103-2.079-9.26s-20.977 4.535-24.19 4.913c-3.213.378-32.317-3.78-33.073-3.968-.756-.19-28.915-22.112-28.915-22.112l-11.528-16.82-2.646-18.143 3.59-33.073 12.852-24.19 10.961-10.206 9.45-6.425-6.804-5.67z"
            id="path12548"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath12571" clipPathUnits="userSpaceOnUse">
          <path
            id="path12573"
            d="M288.396 170.278l-3.969-6.803 36.286-44.79 7.37 4.346-21.733 39.31z"
            fill="none"
            stroke="#fdfdfd"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12598">
          <path
            d="M327.705 114.905l-6.803-2.268 2.079-17.009-3.402-14.174-2.835-10.961 6.237-3.402 4.346 10.961s2.835 20.6 2.835 22.112c0 1.512-2.457 14.74-2.457 14.74z"
            id="path12600"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient
          gradientTransform="translate(-121.423 15.667) scale(1.21219)"
          gradientUnits="userSpaceOnUse"
          y2={5.0686235}
          x2={268.2865}
          y1={5.8799295}
          x1={234.79628}
          id="linearGradient7002"
          xlinkHref="#linearGradient7000"
        />
        <linearGradient
          spreadMethod="reflect"
          gradientUnits="userSpaceOnUse"
          y2={89.036316}
          x2={155.03049}
          y1={89.169952}
          x1={127.20146}
          id="linearGradient7166"
          xlinkHref="#linearGradient7164"
        />
        <linearGradient
          gradientUnits="userSpaceOnUse"
          y2={147.14075}
          x2={372.33994}
          y1={147.14075}
          x1={322.57486}
          id="linearGradient7174"
          xlinkHref="#linearGradient7172"
        />
        <linearGradient
          gradientTransform="matrix(.9701 0 0 1 10.493 0)"
          spreadMethod="reflect"
          gradientUnits="userSpaceOnUse"
          y2={97.470978}
          x2={353.40775}
          y1={97.470978}
          x1={336.27649}
          id="linearGradient7182"
          xlinkHref="#linearGradient7180"
        />
        <linearGradient
          spreadMethod="reflect"
          gradientUnits="userSpaceOnUse"
          y2={34.632069}
          x2={358.2836}
          y1={34.632069}
          x1={312.15826}
          id="linearGradient7190"
          xlinkHref="#linearGradient7188"
        />
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3157">
          <path
            d="M188.415 160.438l10.691-12.16 34.077 17.104-3.608 14.834z"
            id="path3159"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath8141">
          <path
            d="M322.106 85.417l1.984-.567 3.45-.52 1.795 14.08-1.37 4.347-6.26-.307z"
            id="path8143"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
      </defs>
      <g id="layer1">
        <path
          d="M163.207 27.799V19.59l5.12-3.867h36.388v8.327l-5.33 3.906z"
          id="path1056-1-64-0"
          fill="url(#linearGradient7002)"
          fillOpacity={1}
          stroke="#4ddee5"
          strokeWidth={0.3}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M192.038 15.711l1.59.851h11.103l-.018-.823z"
          id="path1060-8-5-6"
          fill="#ffcd00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M203.616 92.918l1.125.013a44.073 45.387 63.803 01.004-.013zm-24.324 1.059a68.638 70.684 63.803 001.35 22.326l24.184-7.683a44.073 45.387 63.803 01-.298-14.367z"
          id="path8799"
          opacity={0.797}
          fill="#47638f"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          id="path8799-2"
          d="M216.455 39.007a68.638 70.684 63.803 00-28.578 27.903l23.389 9.203a44.073 45.387 63.803 0116.046-15.04z"
          opacity={0.797}
          fill="#5476ab"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M187.312 68.22a68.638 70.684 63.803 00-7.863 24.421l25.296.277a44.073 45.387 63.803 015.883-15.523z"
          id="path8799-4"
          opacity={0.797}
          fill="#4e6d9e"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M205.187 109.913l-24.14 7.669a68.638 70.684 63.803 004.826 13.138 68.638 70.684 63.803 002.098 3.926l20.622-15.05a44.073 45.387 63.803 01-.021-.044 44.073 45.387 63.803 01-3.385-9.639z"
          id="path8842"
          opacity={0.797}
          fill="#405982"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M240.159 143.301l-1.608 24.843a68.638 70.684 63.803 0013.957.41l-4.58-24.543a44.073 45.387 63.803 01-7.769-.71z"
          id="path8844"
          opacity={0.797}
          fill="#25344b"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M209.42 120.739l-20.557 15.003a68.638 70.684 63.803 0013.42 15.924l14.393-21.043a44.073 45.387 63.803 01-7.255-9.884zm-20.095 13.026l-1.153.808a68.638 70.684 63.803 00.014.023z"
          id="path8872"
          opacity={0.797}
          fill="#395074"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M217.465 131.728l-14.336 20.96a68.638 70.684 63.803 0018.67 11.07l6.976-24.212a44.073 45.387 63.803 01-11.31-7.817z"
          id="path8883"
          opacity={0.797}
          fill="#324667"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M230.079 139.968l-6.951 24.127a68.638 70.684 63.803 0014.295 3.673l1.612-24.916a44.073 45.387 63.803 01-8.956-2.884z"
          id="path8900"
          opacity={0.797}
          fill="#2c3d59"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M266.289 140.297a44.073 45.387 63.803 01-17.001 3.665l4.563 24.45a68.638 70.684 63.803 0023.294-6.049z"
          id="path8902"
          opacity={0.797}
          fill="#222f44"
          fillOpacity={1}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M260.378 30.973l-.634 25.062a44.073 45.387 63.803 0116.659 7.32l.128.043 12.294-21.968a68.638 70.684 63.803 00-28.447-10.456z"
          id="path8914"
          opacity={0.797}
          fill="#468b80"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M251.077 30.635a68.638 70.684 63.803 00-28.938 6.09L233 58.803a44.073 45.387 63.803 0125.673-2.731l.632-24.968a68.638 70.684 63.803 00-8.229-.469z"
          id="path8919"
          opacity={0.797}
          fill="#366d65"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M289.986 42.095l-12.351 22.069a44.073 45.387 63.803 0111.044 12.581l22.73-11.183a68.638 70.684 63.803 00-21.423-23.467z"
          id="path8933"
          opacity={0.797}
          fill="#449e8d"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M283.047 127.865a44.073 45.387 63.803 01-4.683 4.622l17.63 17.276a68.638 70.684 63.803 007.22-7.51z"
          id="path8942"
          opacity={0.797}
          fill="#515e71"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M288.15 121.154a44.073 45.387 63.803 01-4.116 5.84l20.143 14.371a68.638 70.684 63.803 006.518-9.676z"
          id="path8947"
          opacity={0.797}
          fill="#495465"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M291.57 113.957a44.073 45.387 63.803 01-2.837 6.164l22.52 10.524a68.638 70.684 63.803 005.135-12.244z"
          id="path8949"
          opacity={0.797}
          fill="#444b55"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M277.744 133.473a44.073 45.387 63.803 01-5.86 4.034l10.863 22.077a68.638 70.684 63.803 0012.656-8.807z"
          id="path8951"
          opacity={0.797}
          fill="#5d6774"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M313.933 71.103l-22.734 11.185a44.073 45.387 63.803 012.205 7.158l24.472-6.31a68.638 70.684 63.803 00-3.943-12.033z"
          id="path8966"
          opacity={0.797}
          fill="#498f9e"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M318.345 84.43l-24.492 6.315a44.073 45.387 63.803 01.69 6.748l25.17.62a68.638 70.684 63.803 00-1.368-13.683z"
          id="path8971"
          opacity={0.797}
          fill="#397c88"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M294.58 98.626a44.073 45.387 63.803 01-1.083 9.383l24.643 4.413a68.638 70.684 63.803 001.581-13.177z"
          id="path8973"
          opacity={0.797}
          fill="#3c6d77"
          fillOpacity={0.8}
          stroke="#fff"
          strokeWidth={0.0658159}
          strokeLinecap="round"
          strokeLinejoin="bevel"
          strokeMiterlimit={4}
          strokeDasharray="none"
          paintOrder="stroke fill markers"
        />
        <path
          d="M225.962 291.006c18.403-8.862 33.778-6.248 48.253-.224l-1.706-1.572c-15.048-7.909-30.561-7.39-46.429-.477z"
          id="path6979"
          transform="matrix(1.28858 .14438 -.13168 1.21159 -25.587 -348.139)"
          display="inline"
          opacity={0.25}
          fill="#dbe3de"
          stroke="none"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
          filter="url(#filter7141)"
        />
        <path
          transform="matrix(-.82763 .98174 -.88633 -.80526 697.38 139.982)"
          d="M160.971 282.061c4.624-9.818 10.925-18.373 20.758-24.266l1.106 1.847c-9.205 5.116-15.114 13.589-20.251 22.848z"
          id="path7171"
          display="inline"
          opacity={0.15}
          fill="#dbe3de"
          stroke="none"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
          filter="url(#filter8333)"
        />
        <path
          d="M200.098 319.822c-7.6 12.14 2.39 41.428 16.956 55.21l12.782 11.814c-29.629-15.041-37.897-49.458-33.517-67.623z"
          id="path7297"
          transform="matrix(1.0795 .02735 -.04186 1.12132 -17.426 -280.753)"
          display="inline"
          opacity={0.2}
          fill="#dbe3de"
          stroke="none"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
          filter="url(#filter7407)"
        />
        <g
          id="g953"
          transform="matrix(.40767 0 0 .40767 352.167 103.934)"
          opacity={0.9}
        >
          <g id="g935" fill="#fff">
            <path
              id="path21580"
              d="M230.693 277.238a14.822 14.57 0 00-14.822 14.57 14.822 14.57 0 007.625 12.737v8.412h14.522v-8.459h-.043a14.822 14.57 0 007.54-12.69 14.822 14.57 0 00-14.822-14.57z"
              transform="scale(.26458)"
              stroke="none"
              strokeWidth={0.637565}
              strokeLinecap="round"
              strokeLinejoin="bevel"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="stroke fill markers"
            />
            <rect
              id="rect21590"
              width={3.290756}
              height={0.68487817}
              x={59.467468}
              y={83.638657}
              ry={0.20880432}
              transform="matrix(1.03532 0 0 1.08433 -2.133 -7.136)"
              fillRule="evenodd"
              strokeWidth={0.264583}
              filter="url(#filter21682)"
            />
            <path
              d="M59.601 85.025h3.007l-1.153 1.22h-.785z"
              id="path21592"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
              filter="url(#filter21678)"
            />
            <rect
              id="rect21596"
              width={0.68487817}
              height={2.3422441}
              x={60.7537}
              y={70.071083}
              ry={0.2977429}
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21654)"
            />
            <rect
              ry={0.2977429}
              y={21.494026}
              x={94.024086}
              height={2.3422441}
              width={0.68487817}
              id="rect21598"
              transform="rotate(34.72)"
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21658)"
            />
            <rect
              transform="scale(-1 1) rotate(34.721)"
              id="rect21600"
              width={0.68487817}
              height={2.3422441}
              x={-6.3927069}
              y={91.058784}
              ry={0.2977429}
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21650)"
            />
            <rect
              ry={0.2977429}
              y={74.327843}
              x={54.754318}
              height={2.3422441}
              width={0.68487817}
              id="rect21602"
              transform="scale(-1 1) rotate(72.447)"
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21642)"
            />
            <rect
              transform="rotate(72.447)"
              id="rect21604"
              width={0.68487817}
              height={2.3422441}
              x={91.75103}
              y={-42.076519}
              ry={0.2977429}
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21662)"
            />
            <rect
              transform="rotate(-107.553)"
              id="rect21606"
              width={0.68487817}
              height={2.3422441}
              x={-92.41497}
              y={27.704556}
              ry={0.2977429}
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21646)"
            />
            <rect
              ry={0.2977429}
              y={-88.681435}
              x={-55.581753}
              height={2.3422441}
              width={0.68487817}
              id="rect21608"
              transform="scale(1 -1) rotate(72.447)"
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21666)"
            />
            <rect
              ry={0.2977429}
              y={-35.785442}
              x={-94.368423}
              height={2.3422441}
              width={0.68487817}
              id="rect21610"
              transform="rotate(-145.28)"
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21674)"
            />
            <rect
              transform="scale(1 -1) rotate(34.721)"
              id="rect21612"
              width={0.68487817}
              height={2.3422441}
              x={6.0061536}
              y={-105.34128}
              ry={0.2977429}
              fillRule="evenodd"
              strokeWidth={0.326467}
              filter="url(#filter21670)"
            />
          </g>
          <g
            id="g921"
            strokeLinecap="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
          >
            <path
              id="path21614"
              d="M182.125 267.143c-1.533 0-2.768 1.437-2.768 3.222v7.76h-7.304v-7.67c0-1.785-1.234-3.223-2.768-3.223-1.533 0-2.767 1.438-2.767 3.223v7.67h-3.239a3.919 3.919 0 00-3.927 3.928 3.913 3.913 0 003.261 3.863 14.598 14.241 0 00-1.63 6.54 14.598 14.241 0 0014.597 14.241 14.598 14.241 0 0014.598-14.242 14.598 14.241 0 00-1.602-6.482 3.915 3.915 0 003.834-3.92 3.919 3.919 0 00-3.928-3.928h-3.59v-7.76c0-1.785-1.234-3.222-2.767-3.222z"
              transform="scale(.26458)"
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0}
              strokeLinejoin="bevel"
              paintOrder="stroke fill markers"
            />
            <path
              d="M46.379 81.096v4.705c.04 1.838.738 3.168 2.598 3.6h17.412"
              id="path21686"
              fill="none"
              stroke="#fff"
              strokeWidth={1.48515}
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </g>
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={8.2545443}
          y={12.445184}
          id="text3241-1-9-3-5-3-2-9-7"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-9-4"
            x={8.2545443}
            y={12.445184}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {"GR\xC1FICO CIRCULAR DE CONSUMO"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={386.00793}
          y={135.27148}
          id="text3241-1-9-3-5-3-2-9-4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-9-1"
            x={386.00793}
            y={135.27148}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#0ff"
            strokeWidth={0.264583}
          >
            {"SSGG"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.713864}
          y={85.133423}
          id="text3241-1-9-3-5-3-2-94-5"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-5-4"
            x={10.713864}
            y={85.133423}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#0ff"
            strokeWidth={0.264583}
          >
            {"CUARTO PRINCIPAL TI"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={11.15614}
          y={34.147854}
          id="text3241-1-9-3-5-3-2-7"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-4"
            x={11.15614}
            y={34.147854}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#0ff"
            strokeWidth={0.264583}
          >
            {"ENERG\xCDA CONSUMO"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={381.77432}
          y={77.389465}
          id="text3241-1-9-3-5-3-2-9-9"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-9-2"
            x={381.77432}
            y={77.389465}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#0ff"
            strokeWidth={0.264583}
          >
            {"SISTEMA ELECTRICO PERDIDA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={380.64438}
          y={26.571835}
          id="text3241-1-9-3-5-3-2-6-0"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-4-3-9"
            x={380.64438}
            y={26.571835}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#0ff"
            strokeWidth={0.264583}
          >
            {"SISTEMA AACC"}
          </tspan>
        </text>
        <path
          transform="matrix(28.46168 0 0 -.39082 -1181.953 231.019)"
          id="path21615-1-7-4-9-7"
          d="M54.82 213.162h3.214v2.721h-3.15z"
          display="inline"
          opacity={0.75}
          fill="#152c4e"
          fillOpacity={1}
          stroke="#0eeef6"
          strokeWidth={0.216552}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-15-7-08-1)"
        />
        <path
          transform="matrix(28.46168 0 0 -.39082 -1546.258 131.66)"
          id="path21615-1-7-4-9-0"
          d="M54.82 213.162h3.214v2.721h-3.15z"
          display="inline"
          opacity={0.75}
          fill="#152c4e"
          fillOpacity={1}
          stroke="#0eeef6"
          strokeWidth={0.216552}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-15-7-08-11)"
        />
        <path
          transform="matrix(28.46168 0 0 -.39082 -1179.086 126.32)"
          id="path21615-1-7-4-9-6"
          d="M54.82 213.162h3.214v2.721h-3.15z"
          display="inline"
          opacity={0.75}
          fill="#152c4e"
          fillOpacity={1}
          stroke="#0eeef6"
          strokeWidth={0.216552}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-15-7-08-8)"
        />
        <path
          transform="matrix(28.46168 0 0 -.39082 -1547.551 182.887)"
          id="path21615-1-7-4-9-3"
          d="M54.82 213.162h3.214v2.721h-3.15z"
          display="inline"
          opacity={0.75}
          fill="#152c4e"
          fillOpacity={1}
          stroke="#0eeef6"
          strokeWidth={0.216552}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-15-7-08-2)"
        />
        <path
          transform="matrix(28.46168 0 0 -.39082 -1181.953 176.782)"
          id="path21615-1-7-4-9-08"
          d="M54.82 213.162h3.214v2.721h-3.15z"
          display="inline"
          opacity={0.75}
          fill="#152c4e"
          fillOpacity={1}
          stroke="#0eeef6"
          strokeWidth={0.216552}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-15-7-08-0)"
        />
        <g
          transform="matrix(.94783 0 0 .94774 356.55 16.139)"
          id="g2743-2"
          display="inline"
          strokeWidth={1.05509}
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          fill="none"
        >
          <g
            transform="matrix(.88806 0 0 .88806 .052 -1.662)"
            id="g2255-7"
            strokeWidth={0.356428}
            stroke="#fff"
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <g transform="matrix(-1 0 0 1 38.298 0)" id="g2187-4">
              <path id="path2161-4" d="M19.315 14.552v12.965" />
              <path
                id="path2163-44"
                d="M21.1 15.589l-1.785 1.356-2.018-1.456"
              />
              <path
                id="path2163-4-3"
                d="M21.366 26.27l-2.117-1.355-2.018 1.455"
                display="inline"
              />
            </g>
            <g
              transform="translate(.695 1.158)"
              id="g2187-0-9"
              display="inline"
            >
              <g transform="rotate(63.117 19.893 20)" id="g2216-2">
                <path id="path2161-9-7" d="M18.95 14.42v12.964" />
                <path
                  id="path2163-9-2"
                  d="M21.15 15.611l-2.133 1.4-2.018-1.455"
                />
                <path
                  id="path2163-4-6-0"
                  d="M21.123 26.405l-2.106-1.457L17 26.403"
                  display="inline"
                />
              </g>
              <g
                transform="rotate(132.18 18.916 20.266)"
                id="g2216-5-1"
                display="inline"
              >
                <path id="path2161-9-0-0" d="M18.95 14.42v12.964" />
                <path
                  id="path2163-9-6-8"
                  d="M20.898 15.44l-1.881 1.57-2.018-1.454"
                />
                <path
                  id="path2163-4-6-1-2"
                  d="M21.124 26.326l-2.107-1.378L17 26.403"
                  display="inline"
                />
              </g>
            </g>
          </g>
          <circle
            r={7.7829833}
            cy={17.065624}
            cx={17.131771}
            id="path2613-2-8-9-9"
            display="inline"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#3edce5"
            strokeWidth={0.279598}
            strokeLinecap="square"
            paintOrder="markers stroke fill"
          />
        </g>
        <ellipse
          ry={7.3762083}
          rx={7.3769684}
          id="path2613-2-8-9-5-3"
          cx={374.26611}
          cy={136.51807}
          display="inline"
          fill="none"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="#3edce5"
          strokeWidth={0.264999}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <g
          transform="matrix(.94783 0 0 .94774 73.875 73.906)"
          id="g2726-4"
          display="inline"
          strokeWidth={1.05509}
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        >
          <g
            transform="matrix(.82927 0 0 .8315 -9.484 -12.86)"
            id="g2434-8"
            strokeWidth={1.27061}
            stroke="#000"
          >
            <rect
              rx={0.21038543}
              ry={0.20984229}
              y={28.889191}
              x={54.867966}
              height={12.865365}
              width={8.1359377}
              id="rect2292-9-5"
              display="inline"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.381182}
              strokeLinecap="square"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.21038543}
              ry={0.20984229}
              y={29.170311}
              x={55.223503}
              height={2.8773437}
              width={7.4083333}
              id="rect2292-2"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.381183}
              strokeLinecap="square"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.21038543}
              ry={0.20984229}
              y={32.279167}
              x={55.223503}
              height={2.8773437}
              width={7.4083333}
              id="rect2292-8-2"
              display="inline"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.381183}
              strokeLinecap="square"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.21038543}
              ry={0.20984229}
              y={35.40456}
              x={55.223503}
              height={2.8773437}
              width={7.4083333}
              id="rect2292-5-3"
              display="inline"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.381183}
              strokeLinecap="square"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.21038543}
              ry={0.20984229}
              y={38.513416}
              x={55.223503}
              height={2.8773437}
              width={7.4083333}
              id="rect2292-8-3-2"
              display="inline"
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.381183}
              strokeLinecap="square"
              paintOrder="markers stroke fill"
            />
            <path
              id="path2354-2-8"
              d="M61.672 40.272c-.04-.123-.118-.245-.359-.368.091-.113.275-.192.37-.427.171.292.223.566-.01.795z"
              display="inline"
              fill="none"
              strokeWidth={0.20965}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              id="path2354-2-2-7"
              d="M61.672 37.357c-.04-.123-.118-.245-.359-.368.091-.113.275-.192.37-.427.171.292.223.566-.01.795z"
              display="inline"
              fill="none"
              strokeWidth={0.20965}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              id="path2354-2-20-5"
              d="M61.672 34.281c-.04-.123-.118-.245-.359-.368.091-.113.275-.192.37-.427.171.292.223.566-.01.795z"
              display="inline"
              fill="none"
              strokeWidth={0.20965}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              id="path2354-2-9-6"
              d="M61.672 31.106c-.04-.123-.118-.245-.359-.368.091-.113.275-.192.37-.427.171.292.223.566-.01.795z"
              display="inline"
              fill="none"
              strokeWidth={0.20965}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
          </g>
          <circle
            r={7.7829833}
            cy={16.536457}
            cx={39.422916}
            id="path2613-2-8-9-5-5"
            display="inline"
            fill="none"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#3edce5"
            strokeWidth={0.279598}
            strokeLinecap="square"
            paintOrder="markers stroke fill"
          />
        </g>
        <g
          transform="matrix(.94783 0 0 .94774 135.15 -1.336)"
          id="g2709-6"
          display="inline"
          strokeWidth={1.05509}
        >
          <g
            transform="translate(-62.574 5.556)"
            id="g2752-0"
            strokeWidth={1.05509}
            strokeOpacity={1}
          >
            <path
              id="path2515-0"
              d="M34.889 35.998h2.042l1.213-1.484h1.368l1.936-1.937"
              fill="none"
              stroke="#fff"
              strokeWidth={0.596127}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <rect
              rx={0.55829108}
              ry={0.55834854}
              y={37.212257}
              x={35.794075}
              height={2.3512859}
              width={1.772269}
              id="rect2517-0"
              fill="#fff"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.279598}
              strokeLinecap="square"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.55829108}
              ry={0.55834854}
              y={35.574066}
              x={38.433212}
              height={4.0280223}
              width={1.772269}
              id="rect2517-1-8"
              display="inline"
              fill="#fff"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.279597}
              strokeLinecap="square"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.55829108}
              ry={0.55834854}
              y={34.234604}
              x={40.9953}
              height={5.3193026}
              width={1.772269}
              id="rect2517-1-3-5"
              display="inline"
              fill="#fff"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.279598}
              strokeLinecap="square"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="markers stroke fill"
            />
            <rect
              rx={0.55829108}
              ry={0.55834854}
              y={40.48864}
              x={34.503399}
              height={0.53963941}
              width={9.3429394}
              id="rect2517-8-6"
              display="inline"
              fill="#fff"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.279597}
              strokeLinecap="square"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="markers stroke fill"
            />
            <path
              id="path2564-7"
              d="M40.841 32.047l1.243 1.243.684-1.898z"
              fill="#fff"
              stroke="#fff"
              strokeWidth=".27916px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <circle
              r={7.7829833}
              cy={36.644791}
              cx={39.158333}
              id="path2613-2-2"
              display="inline"
              fill="none"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#3edce5"
              strokeWidth={0.279598}
              strokeLinecap="square"
              strokeMiterlimit={4}
              strokeDasharray="none"
              paintOrder="markers stroke fill"
            />
          </g>
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={384.13602}
          y={143.59589}
          id="ssgg_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8"
            x={384.13602}
            y={143.59589}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {ssgg_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={476.30472}
          y={143.1153}
          id="ssgg_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-1-6-0"
            x={476.30472}
            y={143.1153}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {ssgg_consumo+"Kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.118043}
          y={43.494991}
          id="gen_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-9"
            x={10.118043}
            y={43.494991}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {elec_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={102.28674}
          y={43.0144}
          id="gen_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-1-6-0-4"
            x={102.28674}
            y={43.0144}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {elec_consumo+"Kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={9.9331026}
          y={95.567505}
          id="ti2_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-9-6"
            x={9.9331026}
            y={95.567505}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {ti_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={102.10181}
          y={95.086914}
          id="ti2_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-1-6-0-4-3"
            x={102.10181}
            y={95.086914}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {ti_consumo+"Kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={381.98569}
          y={39.177486}
          id="aacc2_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-9-0"
            x={381.98569}
            y={39.177486}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {aacc_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={476.47314}
          y={39.08337}
          id="aacc2_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-1-6-0-4-1"
            x={476.47314}
            y={39.08337}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {aacc_consumo+"Kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={385.61871}
          y={86.655426}
          id="ups2_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-9-7"
            x={385.61871}
            y={86.655426}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {sep_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={477.78711}
          y={86.174835}
          id="ups2_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.05556px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-1-6-0-4-5"
            x={477.78711}
            y={86.174835}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.05556px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {sep_consumo+"Kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.39026}
          y={152.67871}
          id="ssgg_gasto-2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8"
            x={372.39026}
            y={152.67871}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"ILUMINACION"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.75201}
          y={157.89194}
          id="ssgg_gasto-2-8"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5"
            x={372.75201}
            y={157.89194}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"TOMA COR."}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.42166}
          y={163.11362}
          id="ssgg_gasto-2-8-0"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9"
            x={372.42166}
            y={163.11362}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"UPS 10KVA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.73514}
          y={168.31848}
          id="ssgg_gasto-2-8-2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1"
            x={372.73514}
            y={168.31848}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"AACC SPLITS"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.76074}
          y={152.9512}
          id="ssgg_gasto1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-0"
            x={411.76074}
            y={152.9512}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ilu2_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={412.12247}
          y={158.16449}
          id="ssgg_gasto2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4"
            x={412.12247}
            y={158.16449}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {tomac_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.79214}
          y={163.36923}
          id="ssgg_gasto3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1"
            x={411.79214}
            y={163.36923}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ups10_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={412.10562}
          y={168.59097}
          id="ssgg_gasto4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9"
            x={412.10562}
            y={168.59097}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {split_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47595}
          y={152.69556}
          id="ssgg_consumo1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-0-3"
            x={476.47595}
            y={152.69556}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ilu2_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47595}
          y={157.90881}
          id="ssgg_consumo2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4-3"
            x={476.47595}
            y={157.90881}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {tomac_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47595}
          y={163.11362}
          id="ssgg_consumo3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1-1"
            x={476.47595}
            y={163.11362}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ups10_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47595}
          y={168.33533}
          id="ssgg_consumo4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0"
            x={476.47595}
            y={168.33533}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {split_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.56384}
          y={48.87402}
          id="ssgg_gasto-2-8-9"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-19"
            x={372.56384}
            y={48.87402}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {"CHILLERS"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.42154}
          y={54.095699}
          id="ssgg_gasto-2-8-0-5"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-2"
            x={372.42154}
            y={54.095699}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {"UMAS"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.74234}
          y={59.300533}
          id="ssgg_gasto-2-8-2-1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-6"
            x={372.74234}
            y={59.300533}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {"VAR+PUMPS"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={49.146511}
          id="aacc2_gasto1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4-5"
            x={411.7612}
            y={49.146511}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {chill_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={54.368191}
          id="aacc2_gasto2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1-6"
            x={411.7612}
            y={54.368191}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {umas_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={59.573025}
          id="aacc2_gasto3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-3"
            x={411.7612}
            y={59.573025}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            strokeWidth={0.264583}
          >
            {var_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={48.890896}
          id="aacc2_consumo1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4-3-7"
            x={476.47562}
            y={48.890896}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="gray"
            strokeWidth={0.264583}
          >
            {chill_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={54.112576}
          id="aacc2_consumo2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1-1-0"
            x={476.47562}
            y={54.112576}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="gray"
            strokeWidth={0.264583}
          >
            {umas_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={59.31741}
          id="aacc2_consumo3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-0"
            x={476.47562}
            y={59.31741}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="gray"
            strokeWidth={0.264583}
          >
            {var_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.42154}
          y={98.570496}
          id="ssgg_gasto-2-8-9-4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-19-1"
            x={372.42154}
            y={98.570496}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"UPS 200KVA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.39026}
          y={103.79216}
          id="ssgg_gasto-2-8-0-5-2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-2-4"
            x={372.39026}
            y={103.79216}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"PRE-HOT"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.75195}
          y={108.99701}
          id="ssgg_gasto-2-8-2-1-6"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-6-1"
            x={372.75195}
            y={108.99701}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"TRAFO"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={98.843018}
          id="ups2_gasto1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4-5-6"
            x={411.7612}
            y={98.843018}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ups200_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={104.0647}
          id="ups2_gasto2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1-6-7"
            x={411.7612}
            y={104.0647}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {prehot_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.7612}
          y={109.26957}
          id="ups2_gasto3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-3-6"
            x={411.7612}
            y={109.26957}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {trafo_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={98.587395}
          id="ups2_consumo1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-4-3-7-5"
            x={476.47562}
            y={98.587395}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {ups200_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={103.80904}
          id="ups2_consumo2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-9-1-1-0-7"
            x={476.47562}
            y={103.80904}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {prehot_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.47562}
          y={109.01389}
          id="ups2_consumo3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-0-2"
            x={476.47562}
            y={109.01389}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {trafo_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={11.044225}
          y={53.723263}
          id="ssgg_gasto-2-8-2-9"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2954"
            x={11.044225}
            y={53.723263}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"ENERG\xCDA EEQ"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={42.115864}
          y={54.055721}
          id="gen_gasto1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34"
            x={42.115864}
            y={54.055721}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {tdb0_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={53.800106}
          id="gen_consumo1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-2"
            x={99.87352}
            y={53.800106}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {tdb0_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={11.094953}
          y={60.544155}
          id="ssgg_gasto-2-8-2-9-0"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2954-0"
            x={11.094953}
            y={60.544155}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"ENERGIA GEN"}
          </tspan>
        </text>
        <g
          transform="translate(29.675 -273.023)"
          id="g2693-6-8"
          display="inline"
          fillOpacity={1}
        >
          <ellipse
            ry={7.3762083}
            rx={7.3769674}
            cy={355.06357}
            cx={344.5408}
            id="path2613-2-2-2-8-1"
            display="inline"
            fill="none"
            fillRule="evenodd"
            stroke="#3edce5"
            strokeWidth={0.264999}
            strokeLinecap="square"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <g
            transform="translate(-17.086 -26.396) scale(1.09612)"
            id="g2685-8-4"
            fill="#fff"
            stroke="#fff"
          >
            <path
              d="M329.92 342.743c-.26-.002-.517.082-.585.257v6.063a.975.975 0 00-.4.788.975.975 0 00.976.975.975.975 0 00.975-.975.975.975 0 00-.36-.758v-1.047h2.095v-.647h-2.095v-1.01h2.121v-.633h-2.121v-1.022h2.108v-.634h-2.108V343c-.082-.168-.346-.256-.606-.257z"
              id="path2579-3-0"
              strokeWidth={0.0251936}
            />
            <path
              id="path2590-0-6"
              d="M324.724 350.555v-.887c1.25-.75 1.301.849 3.495-.1v.987c-1.495.905-2.153-.748-3.495 0z"
              strokeWidth=".0342964px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              id="path2590-6-8-0"
              d="M335.172 350.524v-.887c-1.25-.75-1.301.849-3.495-.1v.987c1.496.905 2.153-.747 3.495 0z"
              display="inline"
              strokeWidth=".0342964px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <path
              id="path2663-9-0"
              d="M325.157 351.332c1.01-.405 1.433.122 2.07.31.588-.1 1.117-.517 1.423-.504.444.002 1.133.532 1.332.504.264.001.82-.471 1.23-.491.395-.01 1.259.527 1.539.491.216.01 1.05-.779 1.98-.297l.025.88c-.198-.22-.489-.275-.789-.272-.257-.002-.917.504-1.242.491-.33-.008-1.046-.452-1.475-.426-.353.02-.86.423-1.28.426-.345-.003-1.09-.529-1.398-.491-.211 0-.875.508-1.32.491-.247.005-1.12-.468-1.423-.465-.2.017-.405.05-.672.271z"
              strokeWidth=".0342964px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </g>
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={42.431087}
          y={60.816708}
          id="gen_gasto2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-0"
            x={42.431087}
            y={60.816708}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {gen_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={60.561031}
          id="gen_consumo2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-2-6"
            x={99.87352}
            y={60.561031}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {gen_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={53.800106}
          id="per1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06"
            x={110.58955}
            y={53.800106}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {""}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={60.561031}
          id="per2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-6"
            x={110.58955}
            y={60.561031}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
	    fillOpacity={0}
            strokeWidth={0.264583}
          >
            {"10.1%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59622}
          y={152.69556}
          id="per4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-1"
            x={487.59622}
            y={152.69556}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ssgg_per1+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59622}
          y={157.90881}
          id="per5"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-3"
            x={487.59622}
            y={157.90881}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ssgg_per2+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59622}
          y={163.11362}
          id="per6"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-37"
            x={487.59622}
            y={163.11362}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ssgg_per3+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59622}
          y={168.33533}
          id="per7"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-0"
            x={487.59622}
            y={168.33533}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ssgg_per4+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={48.890896}
          id="per10"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-1"
            x={487.59628}
            y={48.890896}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {aacc_per1+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={54.112576}
          id="per11"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-18"
            x={487.59628}
            y={54.112576}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {aacc_per2+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={59.31741}
          id="per12"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-6"
            x={487.59628}
            y={59.31741}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {aacc_per3+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={98.587395}
          id="per14"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-8"
            x={487.59628}
            y={98.587395}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per1+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={103.80904}
          id="per15"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-13"
            x={487.59628}
            y={103.80904}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per2+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={109.01389}
          id="per16"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5"
            x={487.59628}
            y={109.01389}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per3+"%"}
          </tspan>
        </text>
        <path
          id="path4115"
          d="M103.144 63.028h16.289"
          display="inline"
          fill="#4b4b4b"
          fillOpacity={0.991936}
          stroke="#524e4e"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0}
        />
        <path
          id="path4115-6"
          d="M479.749 170.822h16.289"
          display="inline"
          fill="#4b4b4b"
          fillOpacity={0.991936}
          stroke="#524e4e"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          id="path4115-2"
          d="M479.748 126.493h16.29"
          display="inline"
          fill="#4b4b4b"
          fillOpacity={0.991936}
          stroke="#524e4e"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <path
          id="path4115-7"
          d="M480.16 61.338h16.29"
          display="inline"
          fill="#4b4b4b"
          fillOpacity={0.991936}
          stroke="#524e4e"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={131.21288}
          id="per17"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-9"
            x={487.59628}
            y={131.21288}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59628}
          y={67.528107}
          id="per13"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-5"
            x={487.59628}
            y={67.528107}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {aacc_per+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.59622}
          y={176.39374}
          id="per8"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-6"
            x={487.59622}
            y={176.39374}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ssgg_per+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={67.980957}
          id="per3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-8"
            x={110.58955}
            y={67.980957}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {""}
          </tspan>
        </text>
        <circle
          r={0.030441597}
          cy={32.674526}
          cx={121.77518}
          id="path5925"
          display="inline"
          fill="#35bbc3"
          stroke="#2c89a0"
          strokeWidth={0.264583}
        />
        <text
          id="text5767"
          y={106.42789}
          x={10.844617}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={106.42789}
            x={10.844617}
            id="tspan5765"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 1"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.844617}
          y={110.78939}
          id="text5771"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan5769"
            x={10.844617}
            y={110.78939}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 2"}
          </tspan>
        </text>
        <text
          id="text5775"
          y={115.13393}
          x={10.844617}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={115.13393}
            x={10.844617}
            id="tspan5773"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 3"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.844617}
          y={119.5376}
          id="text5779"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan5777"
            x={10.844617}
            y={119.5376}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 4"}
          </tspan>
        </text>
        <text
          id="text5783"
          y={123.8484}
          x={10.844617}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={123.8484}
            x={10.844617}
            id="tspan5781"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 5"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.844617}
          y={128.24365}
          id="text5787"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan5785"
            x={10.844617}
            y={128.24365}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 6"}
          </tspan>
        </text>
        <text
          id="text5791"
          y={132.61357}
          x={10.844617}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={132.61357}
            x={10.844617}
            id="tspan5789"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 7"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.844617}
          y={136.98344}
          id="text5795"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan5793"
            x={10.844617}
            y={136.98344}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 8"}
          </tspan>
        </text>
        <text
          id="text5799"
          y={141.35338}
          x={10.844617}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={141.35338}
            x={10.844617}
            id="tspan5797"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"FILA 9"}
          </tspan>
        </text>
        <text
          id="gas_f1"
          y={106.68339}
          x={41.916256}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={106.68339}
            x={41.916256}
            id="tspan7066"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={41.916256}
          y={111.05131}
          id="gas_f2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7070"
            x={41.916256}
            y={111.05131}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila2_gasto+"$"}
          </tspan>
        </text>
        <text
          id="gas_f3"
          y={115.41912}
          x={41.916256}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={115.41912}
            x={41.916256}
            id="tspan7074"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila3_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={41.916256}
          y={119.78704}
          id="gas_f4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7078"
            x={41.916256}
            y={119.78704}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila4_gasto+"$"}
          </tspan>
        </text>
        <text
          id="gas_f5"
          y={124.15494}
          x={41.916256}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={124.15494}
            x={41.916256}
            id="tspan7082"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila5_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={41.916256}
          y={128.5228}
          id="gas_f6"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7086"
            x={41.916256}
            y={128.5228}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila6_gasto+"$"}
          </tspan>
        </text>
        <text
          id="gas_f7"
          y={132.89064}
          x={41.916256}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={132.89064}
            x={41.916256}
            id="tspan7090"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={41.916256}
          y={137.2585}
          id="gas_f8"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7094"
            x={41.916256}
            y={137.2585}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          id="gas_f9"
          y={141.62636}
          x={41.916256}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={141.62636}
            x={41.916256}
            id="tspan7098"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {fila9_gasto+"$"}
          </tspan>
        </text>
        <text
          id="com_f1"
          y={106.42773}
          x={99.87352}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={106.42773}
            x={99.87352}
            id="tspan7102"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          id="per_f1"
          y={106.42773}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={106.42773}
            x={110.58955}
            id="tspan7106"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={110.79562}
          id="com_f2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7110"
            x={99.87352}
            y={110.79562}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f2_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={110.79562}
          id="per_f2"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7114"
            x={110.58955}
            y={110.79562}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f2_per+"%"}
          </tspan>
        </text>
        <text
          id="com_f3"
          y={115.16354}
          x={99.87352}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={115.16354}
            x={99.87352}
            id="tspan7118"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f3_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          id="per_f3"
          y={115.16349}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={115.16349}
            x={110.58955}
            id="tspan7122"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f3_per+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={119.53133}
          id="com_f4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7126"
            x={99.87352}
            y={119.53133}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f4_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={119.53133}
          id="per_f4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7130"
            x={110.58955}
            y={119.53133}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f4_per+"%"}
          </tspan>
        </text>
        <text
          id="com_f5"
          y={123.89923}
          x={99.87352}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={123.89923}
            x={99.87352}
            id="tspan7134"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f5_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          id="per_f5"
          y={123.89923}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={123.89923}
            x={110.58955}
            id="tspan7138"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f5_per+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={128.26715}
          id="com_f6"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7142"
            x={99.87352}
            y={128.26715}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f6_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={128.26715}
          id="per_f6"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7146"
            x={110.58955}
            y={128.26715}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f6_per+"%"}
          </tspan>
        </text>
        <text
          id="com_f7"
          y={132.63499}
          x={99.87352}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={132.63499}
            x={99.87352}
            id="tspan7150"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          id="per_f7"
          y={132.63503}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={132.63503}
            x={110.58955}
            id="tspan7154"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.87352}
          y={137.00288}
          id="com_f8"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7158"
            x={99.87352}
            y={137.00288}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={110.58955}
          y={137.00288}
          id="per_f8"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7162"
            x={110.58955}
            y={137.00288}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {"-"}
          </tspan>
        </text>
        <text
          id="com_f9"
          y={141.37074}
          x={99.87352}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={141.37074}
            x={99.87352}
            id="tspan7166"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_f9_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={10.806723}
          y={158.3531}
          id="rec1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan5793-5"
            x={10.806723}
            y={158.3531}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"RECT 1"}
          </tspan>
        </text>
        <text
          id="rect2"
          y={162.72304}
          x={10.806723}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={162.72304}
            x={10.806723}
            id="tspan5797-7"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"RECT 2"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={41.878349}
          y={158.62816}
          id="costo_rect1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7094-9"
            x={41.878349}
            y={158.62816}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {rect1_gasto+"$"}
          </tspan>
        </text>
        <text
          id="costo_rect2"
          y={162.99602}
          x={41.878349}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={162.99602}
            x={41.878349}
            id="tspan7098-0"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {rect2_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={99.83564}
          y={158.37254}
          id="consumo_rect1"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan7158-1"
            x={99.83564}
            y={158.37254}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {rect1_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          id="consumo_rect2"
          y={162.7404}
          x={99.83564}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            y={162.7404}
            x={99.83564}
            id="tspan7166-0"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {rect2_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          id="per_f9"
          y={141.37074}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={141.37074}
            x={110.58955}
            id="tspan7170"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {f9_per+"%"}
          </tspan>
        </text>
        <path
          d="M103.144 143.461h16.289"
          id="path7174"
          display="inline"
          fill="#4b4b4b"
          fillOpacity={0.991936}
          stroke="#524e4e"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
        />
        <text
          id="per_ti"
          y={148.41545}
          x={110.58955}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={148.41545}
            x={110.58955}
            id="tspan7176"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {ti_per+"%"}
          </tspan>
        </text>
        <circle
          transform="matrix(-1.12559 -.25082 -.21153 .99125 605.03 -189.021)"
          clipPath="url(#clipPath5827-4)"
          r={57.915268}
          cy={335.71622}
          cx={247.7966}
          id="path4658-2"
          display="inline"
          opacity={0.763645}
          fill="none"
          fillOpacity={1}
          stroke="#fc0"
          strokeWidth={2.58993864}
          strokeMiterlimit={4}
          strokeDasharray="1.29496932,1.29496932"
          strokeDashoffset={0}
          strokeOpacity={1}
        />
        <path
          d="M296.818 34.671l-.664-2.484c-4.29-2.897-8.984-5.24-13.803-7.41l-2.975.788c7.049 2.768 12.201 5.558 17.442 9.106zm14.844 20.139l1.447-.802c-1.588-2.61-3.266-4.295-5.597-7.01l-1.191-4.5c-10.781-10.649-24.217-16.722-38.74-20.923l-4.663 1.254a86.596 86.596 0 00-9.473-1.154l-.136 1.31c5.09.36 10.406 1.137 16.126 2.661l1.338 2.298 9.726 3.705 2.865-.742c11.92 5.596 21.13 13.775 28.298 23.903z"
          id="path2648"
          fill="#1a5061"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth=".286042px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.99607843}
          opacity={1}
        />
        <text
          id="tot_per"
          y={28.282476}
          x={177.31125}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          transform="matrix(1.0314 -.01526 .01623 .96932 0 0)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="9.67624px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#0ff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.403175}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={28.282476}
            x={177.31125}
            id="tspan3239-1-9-7-9-7-9-2-90"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.67624px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#0ff"
            fillOpacity={1}
            strokeWidth={0.403175}
          >
            {"100%"}
          </tspan>
        </text>
        <g
          id="g3228"
          transform="matrix(.7398 .70497 -.74895 .6942 167.582 129.32)"
          display="inline"
          opacity={0.85}
          strokeWidth={1.757}
          fill="#3edce5"
          fillOpacity={1}
          stroke="none"
        >
          <path
            d="M18.19-78.912C6.722-86.528 5.328-92.442 4.366-99.152c10.064.682 21.129 4.814 15.412 18.388-.06-4.943-2.479-8.705-6.02-11.906 4.374 5.28 4.88 10.309 5.49 15.346 2.5-4.354 6.198-8.302 16.008-13.494-5.601 1.372-9.31 4.908-13.23 8.202 4.08-13.414 14.382-13.342 25.07-12.436-3.76 11.52-14.203 11.979-23.862 14.443-2.164.553-3.07 3.562-3.17 5.086l-1.706.125z"
            id="path3176"
            opacity={1}
            strokeWidth=".464872px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M35.407-63.33l8.559-9.681-7.063-5.145c-4.08-.107-9.048 4.196-8.232 9.214z"
            id="path3178"
            opacity={1}
            strokeWidth=".464872px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M38.82-64.359c-.29-.27.882-1.628 1.334-1.145l2.713 2.712c.373.373-.663 1.606-1.17 1.17z"
            id="path3180"
            opacity={1}
            strokeWidth=".464872px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M42.774-68.59c-.29-.27.881-1.629 1.333-1.146l2.713 2.713c.374.373-.662 1.606-1.17 1.17z"
            id="path3180-0"
            display="inline"
            opacity={1}
            strokeWidth=".464872px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path3197"
            d="M36.943-88.834l5.389.714a19.182 17.496 0 00-5.389-.714zm8.261 1.72l-2.934 1.106a17.066 15.572 0 0111.639 14.763 17.066 15.572 0 01-17.066 15.572 17.066 15.572 0 01-17.065-15.572 17.066 15.572 0 01.808-4.736l-2.288.406a19.182 17.496 0 00-.57 4.237 19.182 17.496 0 0019.181 17.495 19.182 17.496 0 0019.183-17.495 19.182 17.496 0 00-10.888-15.776z"
            opacity={1}
            fillRule="evenodd"
            strokeWidth={0.465602}
            strokeLinecap="square"
            paintOrder="markers stroke fill"
          />
          <path
            d="M20.307-75.87c4.821-3.392 7.562-1.41 10.319.53l-.86 1.587c-5.863-3.79-7.607-.425-10.32 1.257z"
            id="path3220"
            opacity={1}
            strokeWidth=".464872px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </g>
        <text
          id="ti_per"
          y={293.78073}
          x={158.40865}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          transform="matrix(.83082 -.6082 .55677 .79605 0 0)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.98243px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.3326}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={293.78073}
            x={158.40865}
            id="tspan3239-1-9-7-9-7-9-2"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.98243px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            strokeWidth={0.3326}
          >
            {ssgg_per+"%"}
          </tspan>
        </text>
        <text
          id="aacc_per"
          y={-29.737719}
          x={265.40793}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          transform="matrix(.98646 .28809 -.30117 .92577 0 0)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.98243px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.3326}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={-29.737719}
            x={265.40793}
            id="tspan3239-1-9-7-9-7-9-2-97"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.98243px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            strokeWidth={0.3326}
          >
            {aacc_per+"%"}
          </tspan>
        </text>
        <text
          id="ups_per"
          y={-262.87427}
          x={160.76672}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          transform="matrix(.24929 .94445 -1.00022 .22202 0 0)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.98243px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.3326}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={-262.87427}
            x={160.76672}
            id="tspan3239-1-9-7-9-7-9-2-9"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.98243px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            strokeWidth={0.3326}
          >
            {sep_per+"%"}
          </tspan>
        </text>
        <text
          id="ssgg_per"
          y={-4.378242}
          x={251.69763}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          transform="matrix(.81471 .57005 -.52655 .859 0 0)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="7.98243px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.3326}
        >
          <tspan
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            y={-4.378242}
            x={251.69763}
            id="tspan3239-1-9-7-9-7-9"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.98243px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            strokeWidth={0.3326}
          >
            {ti_per+"%"}
          </tspan>
        </text>
        <path
          id="path2648-0"
          d="M176.053 69.288l-2.273 1.203c-1.864 4.829-3.097 9.928-4.134 15.11l1.432 2.724c1.122-7.49 2.689-13.135 4.975-19.037zm16.308-18.971l-1.104-1.231c-2.189 2.131-3.456 4.143-5.58 7.022l-4.12 2.168c-7.969 12.89-10.884 27.343-11.73 42.437l2.265 4.265c.162 3.266.836 6.443 1.328 9.492l1.775-.161c-.788-5.042-2.022-10.397-1.815-16.313l1.94-1.818 1.436-10.308-1.363-2.626c2.789-12.87 8.7-23.677 16.968-32.927z"
          fill="url(#linearGradient5784-1)"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth=".286042px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.996625}
        />
        <circle
          r={75.661049}
          cy={99.224358}
          cx={248.70412}
          id="path835"
          clipPath="url(#clipPath12546)"
          transform="translate(0 .53)"
          fill="none"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth={2.8}
          strokeMiterlimit={4}
          strokeDasharray="1.4,1.4"
          strokeDashoffset={0}
          strokeOpacity={0.996078}
        />
        <circle
          r={76.4963}
          cy={99.33371}
          cx={249.38861}
          id="path835-2"
          clipPath="url(#clipPath12598)"
          transform="translate(-.53)"
          opacity={1}
          fill="none"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth={3.5}
          strokeMiterlimit={4}
          strokeDasharray=".35000001,.35000001"
          strokeDashoffset={0}
          strokeOpacity={0.99607843}
        />
        <circle
          clipPath="url(#clipPath12571)"
          id="path835-2-6"
          cx={249.38861}
          cy={99.33371}
          r={76.4963}
          transform="translate(-.565 -.435)"
          opacity={1}
          fill="none"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth={2.224}
          strokeMiterlimit={4}
          strokeDasharray=".2224,.2224"
          strokeDashoffset={0}
          strokeOpacity={0.99607843}
        />
        <path
          id="path2305"
          d="M303.137 154.12c7.35-7.323 12.868-16.02 17.198-25.608l1.7 6.709c-1.978 6.954-6.163 12.438-11.15 17.387z"
          fill="#0ff"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
          opacity={0.75}
        />
        <path
          d="M163.86 16.606l.568.439 3.292-2.486h2.018l-.62-.587h-1.705z"
          id="path1058-4-8-2"
          fill="#ffcd00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M190.302 29.678l.646-.6h9.275l3.252-2.4.203.67-3.085 2.33z"
          id="path1062-0-9-8"
          fill="#0ff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M203.641 27.097l3.067-2.4v-4.772"
          id="path1064-0-6-3"
          fill="none"
          stroke="#2c89a0"
          strokeWidth={0.4}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M169.379 14.263h10.242"
          id="path1066-7-8-1"
          fill="none"
          stroke="#2c89a0"
          strokeWidth={0.4}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M164.043 16.828l-2.753 2.078v8.749"
          id="path1068-2-3-9"
          fill="none"
          stroke="#2c89a0"
          strokeWidth={0.4}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <ellipse
          id="path1070-63-0-5"
          cx={180.78154}
          cy={14.22102}
          rx={0.48922074}
          ry={0.38998049}
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          ry={0.38998049}
          rx={0.48922077}
          cy={14.22102}
          cx={182.70311}
          id="path1070-63-2-3-7"
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          ry={0.38998049}
          rx={0.48922077}
          cy={14.22102}
          cx={184.59488}
          id="path1070-63-1-6-1"
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          ry={0.38998049}
          rx={0.48922077}
          cy={29.280132}
          cx={184.91185}
          id="path1070-63-3-3-9"
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-2-4-5-8"
          cx={186.83342}
          cy={29.280132}
          rx={0.4892208}
          ry={0.38998049}
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-2-9-8"
          cx={188.72519}
          cy={29.280132}
          rx={0.4892208}
          ry={0.38998049}
          fill="#0ff"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.2243}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          id="path7004"
          d="M107.156 99.03h39.499l20.788-20.789h4.158"
          fill="none"
          stroke="url(#linearGradient7166)"
          strokeWidth={0.765}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.99607843}
          strokeMiterlimit={4}
          strokeDasharray="none"
          fillOpacity={1}
        />
        <path
          id="path1058-4-8-2-0"
          d="M150.776 95.56l-.793-.465-3.584 3.552h-2.567l.79.746h2.167z"
          fill="#55b5cf"
          fillOpacity={0.996078}
          stroke="none"
          strokeWidth={0.285227}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <ellipse
          id="path1070-63-1-6-1-8"
          cx={107.40633}
          cy={99.024277}
          rx={0.37017485}
          ry={0.36347598}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.188364}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7"
          cx={171.51617}
          cy={78.278244}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          id="path7058"
          d="M376.48 147.228h-35.644l-15.734 3.601h-12.378"
          fill="none"
          stroke="url(#linearGradient7174)"
          strokeWidth={0.765}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <path
          d="M374.784 92.982h-26.958l-11.375 8.978h-9.361"
          id="path7060"
          fill="none"
          stroke="url(#linearGradient7182)"
          strokeWidth={0.765}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <path
          id="path7062"
          d="M378.127 42.467h-53.505l-20.607-15.67h-16.992"
          fill="none"
          stroke="url(#linearGradient7190)"
          strokeWidth={0.765}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.99607843}
        />
        <ellipse
          ry={0.46345618}
          rx={0.4744541}
          cy={150.8125}
          cx={312.60992}
          id="path1070-63-1-6-1-7-2"
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7-2-5"
          cx={375.80829}
          cy={147.14886}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7-2-0"
          cx={375.63202}
          cy={92.970673}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7-2-3"
          cx={327.21716}
          cy={101.95285}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7-2-1"
          cx={378.25967}
          cy={42.427826}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <ellipse
          id="path1070-63-1-6-1-7-2-6"
          cx={287.02814}
          cy={26.849123}
          rx={0.4744541}
          ry={0.46345618}
          fill="#4ddee5"
          fillOpacity={1}
          fillRule="evenodd"
          stroke="none"
          strokeWidth={0.240801}
          strokeLinecap="square"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          paintOrder="markers stroke fill"
        />
        <path
          d="M335.288 148.138l.225.66 5.487-1.152 2.335.089-.614-.896-1.946-.004z"
          id="path1058-4-8-2-0-6"
          fill="#55b5cf"
          fillOpacity={0.996078}
          stroke="none"
          strokeWidth={0.285226}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <path
          id="path7141"
          d="M343.149 95.958l.626.614 4.136-3.217h2.566l-.789-.747h-2.168z"
          fill="#55b5cf"
          fillOpacity={0.996078}
          stroke="none"
          strokeWidth={0.285226}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <path
          d="M320.133 39.558l.626-.614 4.136 3.217h2.566l-.789.747h-2.168z"
          id="path7141-0"
          fill="#55b5cf"
          fillOpacity={0.996078}
          stroke="none"
          strokeWidth={0.285226}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={0.996078}
        />
        <path
          d="M176.053 68.816l-2.273 1.203c-1.864 4.829-3.097 9.927-4.134 15.11l1.432 2.724c1.122-7.49 2.689-13.136 4.975-19.037z"
          id="path2796"
          fill="#0fffff"
          fillOpacity={1}
          stroke="#2c89a0"
          strokeWidth=".286042px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.996625}
        />
        <circle
          r={75.661049}
          cy={99.224358}
          cx={248.70412}
          id="circle2986"
          clipPath="url(#clipPath3157)"
          transform="translate(.01 .537)"
          fill="none"
          fillOpacity={1}
          stroke="#ffcd00"
          strokeWidth={2.8}
          strokeMiterlimit={4}
          strokeDasharray="1.4,1.4"
          strokeDashoffset={0}
          strokeOpacity={0.996078}
        />
        <circle
          r={76.4963}
          cy={99.33371}
          cx={249.38861}
          id="circle5365"
          clipPath="url(#clipPath8141)"
          transform="translate(-.512 -.067)"
          opacity={1}
          fill="none"
          fillOpacity={1}
          stroke="#ffcd00"
          strokeWidth={3.5}
          strokeMiterlimit={4}
          strokeDasharray=".35,.35"
          strokeDashoffset={0}
          strokeOpacity={0.996078}
        />
        <path
          d="M269.444 25.582l1.28 2.357 9.795 3.746 2.86-.763z"
          id="path10611"
          fill="#0fffff"
          stroke="#2c89a0"
          strokeWidth=".264583px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={1}
          fillOpacity={1}
          opacity={0.9}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.60956}
          y={114.15363}
          id="per16-3"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-50"
            x={487.60956}
            y={114.15363}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per4+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.94144}
          y={118.95189}
          id="per_rectt"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-34-06-2-5-50-2"
            x={487.94144}
            y={118.95189}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per5+"%"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={372.77695}
          y={114.05912}
          id="ssgg_gasto-2-8-2-1-6-4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-6-1-6"
            x={372.77695}
            y={114.05912}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"CABLE/LECTURA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={373.01688}
          y={119.33076}
          id="rectt"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-6-1-6-5"
            x={373.01688}
            y={119.33076}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"RECTIFICADOR"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.78619}
          y={114.3317}
          id="ups2_gasto4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-3-6-6"
            x={411.78619}
            y={114.3317}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {cable_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.50061}
          y={114.076}
          id="ups2_consumo4"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-0-2-2"
            x={476.50061}
            y={114.076}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {cable_consumo+"kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.60703}
          y={119.00076}
          id="rectt_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-3-6-6-1"
            x={411.60703}
            y={119.00076}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {recttot_gasto+"$"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.32144}
          y={118.7451}
          id="rectt_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3239-1-9-0-0-7-8-4-8-8-5-1-9-0-0-2-2-0"
            x={476.32144}
            y={118.7451}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {recttot_consumo+"kw/h"}
          </tspan>
          </text>
          <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={373.01688}
          y={124.09327}
          id="text29485"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan29483"
            x={373.01688}
            y={124.09327}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {"PERDIDAS PDU"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={411.60703}
          y={124.36578}
          id="per_pdu_gasto"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan30057"
            x={411.60703}
            y={124.36578}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_per_gasto}$
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "end"
          }}
          x={476.32144}
          y={124.11015}
          id="per_pdu_consumo"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="end"
          display="inline"
          fill="#999"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan30061"
            x={476.32144}
            y={124.11015}
            style={{
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "end"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="end"
            fill="#999"
            strokeWidth={0.264583}
          >
            {pdu_perdida_consumo+ "kw/h"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal",
            textAlign: "center"
          }}
          x={487.94144}
          y={124.11015}
          id="per_pdu"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.9389px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#4d4d4d"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan30065"
            x={487.94144}
            y={124.11015}
            style={{
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal",
              textAlign: "center"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="4.9389px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#4d4d4d"
            strokeWidth={0.264583}
          >
            {sep_per6}%
          </tspan>
        </text>
      </g>
    </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
