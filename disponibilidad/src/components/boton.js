import React from 'react';
import ReactDOM from 'react-dom';
import Switch from 'react-switch';
   
class App extends Component {
  constructor() {
    super ()
    this.state = {
      checked : false 
    }
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(checked) {
    this.setState({checked})
  }
  render() {
    return (
      <div>
        <Switch
          className="react-switch"
          onChange={this.handleChange}
          checked={this.state.checked}
          handleDiameter={28}
          offColor="#4d4d4d"
          onColor="#4d4d4d"
          onHandleColor="#2693e6"
          onHandleColor="blue"
          height={48}
          width={60}
          
        />
      </div>
    )
  }
}
ReactDOM.render(<App />, document.getElementById('components'));
export default App;

/*function onChange(value, event) {
  console.log(`switch checked: ${value}`, event); // eslint-disable-line
}

class Demo extends React.Component {
  state = {
    disabled: false,
  };

  toggle = () => {
    const { disabled } = this.state;
    this.setState({
      disabled: !disabled,
    });
  };

  render() {
    const { disabled } = this.state;
    return (
      <div style={{ margin: 20 }}>
        <Switch
          onChange={onChange}
          onClick={onChange}
          disabled={disabled}
          checkedChildren="ON"
          unCheckedChildren="OFF"
        />
        <div style={{ marginTop: 20 }}>
          <button type="button" onClick={this.toggle}>
            toggle disabled
          </button>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, document.getElementById('boton'));
export default Demo;*/