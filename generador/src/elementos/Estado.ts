import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    alarmaOn: css`
      fill: #f51628;
    `,
    alarmaOff: css`
      fill: #4d4d4d;
    `,
    estadoOn: css`
      fill: #1aea78;
    `,
    estadoOff: css`
      fill: #4d4d4d;
    `,
    botonOn: css`
      fill: #4bee8e;
    `,
    botonOff: css`
      fill: #f51628;
    `,
    rectOn: css`
      fill: #1aea78;
    `,
    rectOff: css`
      fill: #4d4d4d;
    `,
  };
});

const equipo = getStyles();

export default equipo;
