import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/stylesPop.css';
const swal = require('sweetalert');
//import styleEstado from 'elementos/Estado';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  const d = new Date();
  d.setTime(d.getTime() + 30 * 60 * 1000);
  let expira = ';expires=' + d.toUTCString();
  //let sadvertencia = 'http://172.30.128.202:1880/uimedia/audio/alarma.mp4'; //amarillo
  let salarma = 'http://172.30.128.202:1880/uimedia/audio/alarma.mp4';
  let imgUmaAdv = 'http://172.30.128.202:1880/uimedia/icon/var_adv.png';
  let imgUmaAlm = 'http://172.30.128.202:1880/uimedia/icon/var_alm.png';
  let msgEstado = '';
  let colorEstado = '';
  let imgUma = '';

  let equipo: any;
  let data_repla: any = data.series.find(({ refId }) => refId === 'B')?.name;
  if (data_repla === undefined) {
    equipo = data_repla.replace(/[.*+?^${}|[\]\\]/g, '');
  } else {
    equipo = data_repla.replace(/[.*+?^${}|[\]\\]/g, '');
  }
  let perfil: any = data.series.find(({ refId }) => refId === 'F')?.name;
  let url = '';

  /////******** LINKS **********/////////
  if (perfil === 'BOC') {
    switch (equipo) {
      case '2/B1(1)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(1)';
        break;
      case '2/B1(2)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(2)';
        break;
      case '2/B1(3)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(3)';
        break;
      case '2/B1(4)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(4)';
        break;
      case '2/B1(5)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(5)';
        break;
      case '2/B1(6)':
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(6)';
        break;
      default:
        url =
          'http://bmsclouduio.i.telconet.net/d/tI2xibOnk/boc-ind-var?orgId=1&from=now-3h&to=now&refresh=10s&var-EQUIPO=2%2FB1(1)';
    }
  } else {
    switch (equipo) {
      case '2/B1(1)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(1)&from=now-3h&to=now';
        break;
      case '2/B1(2)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(2)&from=now-3h&to=now';
        break;
      case '2/B1(3)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(3)&from=now-3h&to=now';
        break;
      case '2/B1(4)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(4)&from=now-3h&to=now';
        break;
      case '2/B1(5)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(5)&from=now-3h&to=now';
        break;
      case '2/B1(6)':
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(6)&from=now-3h&to=now';
        break;
      default:
        url =
          'https://bmsclouduio.i.telconet.net/d/b1Z7zaXMk/variador?orgId=1&refresh=5s&var-EQUIPO=2%2FB1(1)&from=now-3h&to=now';
    }
  }

  let cur = data.series.find(({ name }) => name === 'Average DATA.MOT_CURR.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  if (cur === null || cur === 0) {
    cur = 0;
  } else {
    cur = cur.toFixed(1);
  }

  let pow = data.series.find(({ name }) => name === 'Average DATA.POWER.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  if (pow === null || pow === 0) {
    pow = 0;
  } else {
    pow = (pow / 1000).toFixed(1);
  }

  let freq = data.series.find(({ name }) => name === 'Average DATA.FREQ.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  if (freq === null || freq === 0) {
    freq = 0;
  } else {
    freq = freq.toFixed(1);
  }

  let temp = data.series.find(({ name }) => name === 'Average DATA.TEMP.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  if (temp === null || temp === 0) {
    temp = 0;
  } else {
    temp = temp.toFixed(1);
  }

  let vol_a = data.series.find(({ name }) => name === 'VOL_A')?.fields[1].state?.calcs?.lastNotNull;
  let vol_b = data.series.find(({ name }) => name === 'VOL_B')?.fields[1].state?.calcs?.lastNotNull;
  let vol_c = data.series.find(({ name }) => name === 'VOL_C')?.fields[1].state?.calcs?.lastNotNull;
  let vol = (((vol_a + vol_b + vol_c) * 1.73) / 3).toFixed(1);

  let led;
  let alm_adv;
  let sys_on = data.series.find(({ name }) => name === 'Average DATA.RUNN.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  //let modo = '';

  /////***** ALARMAS *****/////
  //let classFault;
  let st_mant = data.series.find(({ name }) => name === 'Average DATA.ST_MANT.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let st_hab = data.series.find(({ name }) => name === 'Average DATA.ST_HABIL.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (sys_on === 1 && freq <= 27) {
    alm_adv = 1;
  } else {
    alm_adv = 0;
  }

  let estado_alm = revisar_data_status1(st_mant, st_hab);
  let alm = '';
  let detalle = '';
  let detalle1 = '';
  let datos = '';
  let alarma = data.series.find(({ name }) => name === 'Average DATA.FAULT.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let st_alarma = revisar_data_status(alm_adv, alarma);
  let cookieEstado = getCookie('cookie_' + equipo);
  let cookieAlm = getCookie('cookie_' + equipo + 'alm');
  let cookieAdv = getCookie('cookie_' + equipo + 'adv');

  if (sys_on === 0) {
    led = '#4d4d4d'; //color gris
    detalle = '#4d4d4d';
    detalle1 = '#4d4d4d';
    datos = '#4d4d4d';
    msgEstado = 'apagado';
    colorEstado = 'alarma';
    imgUma = imgUmaAlm;
  } else {
    led = '#1aea78'; //color verde
    detalle = '#00aad4';
    detalle1 = '#d4aa00';
    datos = '#fff';
    msgEstado = 'encendido';
    colorEstado = 'advertencia';
    imgUma = imgUmaAdv;
  }
  PopUp(cookieEstado, equipo, sys_on, equipo);

  if (alm_adv === 1 && st_mant === 0 && st_hab === 1) {
    msgEstado = 'alarmado';
    colorEstado = 'advertencia';
    imgUma = imgUmaAdv;
    PopUp(cookieAdv, equipo, 1, equipo + 'adv');
  } else {
    document.cookie = 'cookie_' + equipo + 'adv' + '=' + 0 + expira;
  }

  if (alarma > 0 && st_mant === 0 && st_hab === 1) {
    msgEstado = 'alarmado';
    colorEstado = 'alarma';
    imgUma = imgUmaAlm;
    PopUp(cookieAlm, equipo, 1, equipo + 'alm');
  } else {
    document.cookie = 'cookie_' + equipo + 'alm' + '=' + 0 + expira;
  }

  if (alarma === 17 || alarma === 34) {
    alm = 'PERDIDA DE COMUNICACIÓN';
  } else if (alarma === 13 || alarma === 40 || alarma === 41 || alarma === 42 || alarma === 59) {
    alm = 'SOBRECORRIENTE';
  } else if (
    alarma === 11 ||
    alarma === 29 ||
    alarma === 65 ||
    alarma === 69 ||
    alarma === 74 ||
    alarma === 244 ||
    alarma === 245 ||
    alarma === 247
  ) {
    alm = 'SOBRECALENTAMIENTO';
  } else if (alarma === 49 || alarma === 62) {
    alm = 'MAXIMO DE VELOCIDAD';
  } else if (alarma === 5 || alarma === 7 || alarma === 64) {
    alm = 'SOBREVOLTAJE';
  } else if (alarma === 1 || alarma === 6 || alarma === 8) {
    alm = 'BAJO VOLTAJE';
  } else if (alarma === 16) {
    alm = 'CORTOCIRCUITO';
  } else if (alarma === 14) {
    alm = 'FALLA DE TIERRA';
  } else if (alarma === 10 || alarma === 50 || alarma === 58 || alarma === 222) {
    alm = 'SOBRECARGA DE MOTOR';
  } else if (alarma === 9) {
    alm = 'SOBRECARGA DEL INVERSOR';
  } else if (alarma === 12) {
    alm = 'SOBRETORQUE';
  } else if (alarma === 142) {
    alm = 'FALLA EXTERNA';
  } else if (alarma === 13) {
    alm = 'ERROR DE OPERACIÓN';
  } else if (alarma === 3 || alarma === 95 || alarma === 229) {
    alm = 'PERDIDA DE CARGA';
  } else if (alarma === 70 || alarma === 76 || alarma === 79 || alarma === 81 || alarma === 82 || alarma === 91) {
    alm = 'ERROR DE CONFIGURACION';
  } else if (alarma === 60 || alarma === 90 || alarma === 192) {
    alm = 'FALLA DE RETROALIMENTACION';
  } else if (alarma === 30 || alarma === 31 || alarma === 32) {
    alm = 'PERDIDA DE SALIDA DE FASE';
  } else if (alarma === 99) {
    alm = 'PARADA DEL MOTOR';
  } else if (
    alarma === 4 ||
    alarma === 33 ||
    alarma === 36 ||
    alarma === 37 ||
    alarma === 46 ||
    alarma === 228 ||
    alarma === 246
  ) {
    alm = 'ERROR UNIDAD DE POTENCIA';
  } else if (alarma === 20) {
    alm = 'FASE DE ENTRADA';
  } else if (
    alarma === 23 ||
    alarma === 27 ||
    alarma === 38 ||
    alarma === 39 ||
    alarma === 47 ||
    alarma === 48 ||
    alarma === 73 ||
    alarma === 85 ||
    alarma === 86
  ) {
    alm = 'FALLO UNIDAD INTERNA';
  } else {
    alm = '--- ';
  }

  function reproducir(sonido: any) {
    const audio = new Audio(sonido);
    audio.play();
  }
  function revisar_data_status(adv: any, alarma: any) {
    let data_ret_string = [];
    if (adv === null || adv === 0) {
      data_ret_string[0] = '#4d4d4d';
      data_ret_string[1] = '1s';
    } else {
      data_ret_string[0] = '#ffcc00'; //color amarillo
      data_ret_string[1] = '1s';
    }
    if (alarma === 1) {
      data_ret_string[0] = '#f51628'; //color rojo
      data_ret_string[1] = '1s';
    }
    return data_ret_string;
  }

  let text_led;
  function revisar_data_status1(mant: any, hab: any) {
    let data_ret_string = [];
    if (mant === null || mant === 0) {
      data_ret_string[0] = '#4d4d4d';
      data_ret_string[1] = '1s';
      text_led = 'MNT';
    } else {
      data_ret_string[0] = '#9955ff'; //color lila
      data_ret_string[1] = '1s';
      text_led = 'MNT';
    }
    if (hab === 0) {
      data_ret_string[0] = '#ff9955'; //color naranja
      data_ret_string[1] = '1s';
      text_led = 'INHB';
    }
    return data_ret_string;
  }

  //
  function PopUp(cookieVar: any, equipo: any, variable: any, nomCookie: any) {
    if (cookieVar === '') {
      document.cookie = 'cookie_' + nomCookie + '=' + variable + expira;
    } else {
      if (cookieVar !== '' + variable) {
        reproducir(salarma);
        swal({
          className: colorEstado,
          title: equipo,
          text: 'Equipo ' + msgEstado,
          icon: imgUma,
        }).then((value: any) => {
          document.cookie = 'cookie_' + nomCookie + '=' + variable + expira;
        });
      }
    }
  }

  function getCookie(cname: any) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox="0 0 383.64583 280.45835"
        id="svg5"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        xmlns="http://www.w3.org/2000/svg"
        //{...props}
      >
        <defs id="defs2">
          <linearGradient id="linearGradient117825">
            <stop offset={0} id="stop117821" stopColor="#377e96" stopOpacity={1} />
            <stop offset={1} id="stop117823" stopColor="#377e96" stopOpacity={0} />
          </linearGradient>
          <filter
            id="filter4280"
            x={-0.19200001}
            width={1.384}
            y={-0.19200001}
            height={1.384}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282" />
          </filter>
          <filter
            id="filter4280-8"
            x={-0.19200001}
            width={1.384}
            y={-0.19200001}
            height={1.384}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282-7" />
          </filter>
          <filter
            id="filter4280-5"
            x={-0.19200001}
            width={1.384}
            y={-0.19200001}
            height={1.384}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282-8" />
          </filter>
          <radialGradient
            xlinkHref="#brilloborde"
            id="radialGradient28275-1"
            cx={395.74109}
            cy={-510.35718}
            fx={395.74109}
            fy={-510.35718}
            r={5.6696429}
            gradientTransform="matrix(5.0031 0 0 223.80455 811.74 115202.75)"
            gradientUnits="userSpaceOnUse"
          />
          <linearGradient id="brilloborde">
            <stop offset={0} id="stop28229" stopColor="#bbb" stopOpacity={1} />
            <stop offset={1} id="stop28231" stopColor="#bbb" stopOpacity={0} />
          </linearGradient>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath4142-0">
            <path
              id="rect4144-0"
              transform="scale(-1)"
              opacity={0.75}
              fill="#166e86"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0eeef6"
              strokeWidth={0.765}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
              d="M-57.885147 -222.49622H-54.9743287V-206.85483499999998H-57.885147z"
            />
          </clipPath>
          <clipPath id="clipPath4142-0-8" clipPathUnits="userSpaceOnUse">
            <path
              transform="scale(-1)"
              id="rect4144-0-9"
              opacity={0.75}
              fill="#166e86"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0eeef6"
              strokeWidth={0.765}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
              d="M-57.885147 -222.49622H-54.9743287V-206.85483499999998H-57.885147z"
            />
          </clipPath>
          <clipPath id="clipPath4142-0-81" clipPathUnits="userSpaceOnUse">
            <path
              transform="scale(-1)"
              id="rect4144-0-5"
              opacity={0.75}
              fill="#166e86"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0eeef6"
              strokeWidth={0.765}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
              d="M-57.885147 -222.49622H-54.9743287V-206.85483499999998H-57.885147z"
            />
          </clipPath>
          <clipPath id="clipPath4142-0-2" clipPathUnits="userSpaceOnUse">
            <path
              transform="scale(-1)"
              id="rect4144-0-4"
              opacity={0.75}
              fill="#166e86"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="#0eeef6"
              strokeWidth={0.765}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
              d="M-57.885147 -222.49622H-54.9743287V-206.85483499999998H-57.885147z"
            />
          </clipPath>
          <clipPath id="clipPath4874" clipPathUnits="userSpaceOnUse">
            <path
              id="path4876"
              d="M-180.28 31.466l-7.971.628-21.167-1.389.397-15.114s13.394-2.68 13.527-2.778c.132-.1 7.276-2.216 7.276-2.216l2.943-3.11 1.852-1.719 5.953.827s1.092 1.058 1.092 1.653c0 .596-.76 14.023-.76 14.023l-2.647 6.88z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient117825"
            id="linearGradient155687"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(.87631 0 0 1.00066 -1062.246 -565.9)"
            x1={1432.0464}
            y1={645.29901}
            x2={1432.0464}
            y2={755.78326}
          />
        </defs>
        <g id="layer1">
          <path
            id="rect28269-1"
            display="inline"
            opacity={0.899}
            fill="url(#radialGradient28275-1)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={5.00315}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={0}
            paintOrder="markers stroke fill"
            d="M814.58691 -382.49292H830.428143V317.28509999999994H814.58691z"
          />
          <g id="g155421" transform="translate(-1240.64 -355.242)">
            <path
              id="path109751"
              d="M1275.3 612.131l-11.632-15.369V398.245l12.6-14.942h315.02l11.954 14.088v198.944l-11.632 14.943z"
              display="inline"
              fill="none"
              stroke="#3fdce4"
              strokeWidth=".837236px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
            <g id="g155354" transform="matrix(.90785 0 0 1.00015 129.939 -.076)">
              <g id="g109745" transform="matrix(0 1.96265 -3.08887 0 1544.27 459.438)" display="inline">
                <path
                  d="M-17.773 99.38l1.42 1.892h2.177l.338-.78h66.963l.428.57h2.27l1.171-1.559H55.86l-.153.353H-16.37l-.23-.53z"
                  id="path109743"
                  display="inline"
                  fill="#168198"
                  fillOpacity={1}
                  stroke="#3fdce4"
                  strokeWidth=".264583px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
              </g>
              <path
                id="path109753"
                d="M1306.105 371.767l4.97-5.696h7.615l1.182 2.347h234.282l1.496-1.714h7.942l4.097 4.695h-3.97l-.537-1.064h-252.169l-.804 1.597z"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".858945px"
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeOpacity={1}
              />
              <path
                d="M1604.87 622.328l-1.571 3.555h5.767l.663 1.564c5.677-.394 9.74-2.734 13.526-5.407l.165-2.74 11.76-15.358 1.656.216 1.877-6.417-1.16-.576v-12.113h-2.594v14.42c-15.274 19.676-16.72 24.101-30.089 22.856z"
                id="path109755"
                display="inline"
                fill="none"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.860287}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
              />
              <path
                d="M1306.805 621.65l4.97 5.696h7.614l1.183-2.347h234.281l1.496 1.714h7.943l4.097-4.694h-3.971l-.536 1.063h-252.17l-.804-1.597z"
                id="path109757"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".858945px"
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeOpacity={1}
              />
              <g
                id="g109775"
                transform="matrix(3.8052 0 0 4.36013 1154.708 -49.808)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".210875px"
                strokeOpacity={1}
                strokeLinecap="butt"
                strokeLinejoin="miter"
              >
                <path id="path109759" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
                <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path109761" display="inline" />
                <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path109763" display="inline" />
                <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path109765" display="inline" />
                <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path109767" display="inline" />
                <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path109769" display="inline" />
                <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path109771" display="inline" />
                <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path109773" display="inline" />
              </g>
              <g
                id="g109793"
                transform="matrix(-3.8052 0 0 4.36013 1719.169 -50.202)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".210875px"
                strokeOpacity={1}
                strokeLinecap="butt"
                strokeLinejoin="miter"
              >
                <path d="M30.577 96.918l.472-.85h.551l-.481.853z" id="path109777" />
                <path id="path109779" d="M31.555 96.918l.472-.85h.551l-.481.853z" display="inline" />
                <path id="path109781" d="M32.555 96.918l.472-.85h.551l-.482.853z" display="inline" />
                <path id="path109783" d="M33.52 96.918l.471-.85h.552l-.482.853z" display="inline" />
                <path id="path109785" d="M34.52 96.918l.471-.85h.552l-.482.853z" display="inline" />
                <path id="path109787" d="M35.472 96.918l.472-.85h.552l-.482.853z" display="inline" />
                <path id="path109789" d="M36.472 96.918l.472-.85h.551l-.481.853z" display="inline" />
                <path id="path109791" d="M37.536 96.918l.472-.85h.551l-.481.853z" display="inline" />
              </g>
              <g
                transform="matrix(3.8052 0 0 4.36013 1454.907 203.27)"
                id="g109811"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".210875px"
                strokeOpacity={1}
                strokeLinecap="butt"
                strokeLinejoin="miter"
              >
                <path id="path109795" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
                <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path109797" display="inline" />
                <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path109799" display="inline" />
                <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path109801" display="inline" />
                <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path109803" display="inline" />
                <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path109805" display="inline" />
                <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path109807" display="inline" />
                <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path109809" display="inline" />
              </g>
              <g
                transform="matrix(-3.8052 0 0 4.36013 1418.269 202.96)"
                id="g109829"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth=".210875px"
                strokeOpacity={1}
                strokeLinecap="butt"
                strokeLinejoin="miter"
              >
                <path id="path109813" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
                <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path109815" display="inline" />
                <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path109817" display="inline" />
                <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path109819" display="inline" />
                <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path109821" display="inline" />
                <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path109823" display="inline" />
                <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path109825" display="inline" />
                <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path109827" display="inline" />
              </g>
              <g
                id="g109837"
                transform="matrix(3.8052 0 0 4.36013 1156.519 -48.86)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.120347}
                strokeOpacity={1}
                fillRule="evenodd"
                strokeLinecap="square"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                paintOrder="markers stroke fill"
              >
                <path id="rect109831" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
                <path
                  id="rect109833"
                  display="inline"
                  opacity={1}
                  d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
                />
                <path
                  id="rect109835"
                  display="inline"
                  opacity={1}
                  d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
                />
              </g>
              <g
                id="g109845"
                transform="matrix(3.8052 0 0 4.36013 1156.697 110.997)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.120347}
                strokeOpacity={1}
                fillRule="evenodd"
                strokeLinecap="square"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                paintOrder="markers stroke fill"
              >
                <path id="rect109839" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
                <path
                  id="rect109841"
                  display="inline"
                  opacity={1}
                  d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
                />
                <path
                  id="rect109843"
                  display="inline"
                  opacity={1}
                  d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
                />
              </g>
              <g
                id="g109853"
                transform="matrix(3.8052 0 0 4.36013 1556.79 111.405)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.120347}
                strokeOpacity={1}
                fillRule="evenodd"
                strokeLinecap="square"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                paintOrder="markers stroke fill"
              >
                <path id="rect109847" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
                <path
                  id="rect109849"
                  display="inline"
                  opacity={1}
                  d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
                />
                <path
                  id="rect109851"
                  display="inline"
                  opacity={1}
                  d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
                />
              </g>
              <g
                id="g109861"
                transform="matrix(3.8052 0 0 4.36013 1558.102 -50.416)"
                display="inline"
                fill="#168198"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.120347}
                strokeOpacity={1}
                fillRule="evenodd"
                strokeLinecap="square"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                paintOrder="markers stroke fill"
              >
                <path id="rect109855" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
                <path
                  id="rect109857"
                  display="inline"
                  opacity={1}
                  d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
                />
                <path
                  id="rect109859"
                  display="inline"
                  opacity={1}
                  d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
                />
              </g>
              <path
                id="path109863"
                d="M1239.222 398.504l18.154-20.801h357.201l18.332 21.617v197.815l-17.442 20.597h-357.735l-18.688-22.433z"
                display="inline"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth=".858945px"
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeOpacity={1}
              />
              <path
                id="path109865"
                d="M1605.892 372.1l-1.571-3.555h5.767l.663-1.564c5.677.394 9.74 2.734 13.526 5.408l.165 2.74 11.76 15.357 1.656-.216 1.877 6.417-1.16.577v12.112h-2.595v-14.42c-15.273-19.676-16.72-24.101-30.088-22.856z"
                display="inline"
                fill="none"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.860287}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
              />
              <path
                id="path109867"
                d="M1266.884 374.14l1.572-3.556h-5.767l-.663-1.564c-5.677.394-9.74 2.734-13.526 5.408l-.165 2.74-11.76 15.357-1.656-.216-1.877 6.417 1.16.577v12.113h2.594v-14.42c15.274-19.677 16.72-24.102 30.089-22.857z"
                display="inline"
                fill="none"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.860287}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
              />
              <path
                id="path109869"
                d="M1266.706 621.627l1.572 3.555h-5.767l-.663 1.565c-5.677-.395-9.74-2.734-13.526-5.408l-.166-2.74-11.759-15.357-1.656.216-1.877-6.417 1.16-.577V584.35h2.594v14.42c15.274 19.676 16.72 24.102 30.088 22.856z"
                display="inline"
                fill="none"
                fillOpacity={1}
                stroke="#3fdce4"
                strokeWidth={0.860287}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
                strokeOpacity={1}
              />
              <g id="g109873" transform="matrix(0 1.96265 3.08887 0 1327.389 459.014)" display="inline">
                <path
                  d="M-17.773 99.38l1.42 1.892h2.177l.338-.78h66.963l.428.57h2.27l1.171-1.559H55.86l-.153.353H-16.37l-.23-.53z"
                  id="path109871"
                  display="inline"
                  fill="#168198"
                  fillOpacity={1}
                  stroke="#3fdce4"
                  strokeWidth=".264583px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
              </g>
            </g>
          </g>
          <g id="g155559" transform="matrix(.94752 0 0 .94752 -1165.714 -320.547)">
            <g id="g155451" transform="matrix(.79356 0 0 .8255 1369.308 475.5)" strokeOpacity={1}>
              <rect
                id="rect155423"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={1}
                fill="none"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
              <path
                id="path155425"
                d="M-22.926 104.901v6.336l5.282 3.044v17.447l-5.346 3.44v6.88l-3.575 2.673H-40.67l-1.674-1.955h-33.289l-2.732 2.058h-13.936l-2.87-2.03V104.75l4.054-3.01h10.11l4.873 3.885h32.426l4.509-3.955h9.928z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155427"
                d="M-20.092 112.796v-8.39l-7.213-3.613h-65.566l-4.991 3.44v5.148l-3.768 2.202v20.467l4.025 3.02v3.142l2.48 1.782"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155429"
                d="M-31.092 99.5h-13.071l-2.664-2.712H-74.79l-2.482 2.607h-10.976"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155431"
                d="M-45.817 99.32l-1.757-1.9h-26.845l-1.449 1.9z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155433"
                d="M-40.861 100.821l-3.666 3.456h-31.049l-3.86-3.508z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155435"
                d="M-27.627 105.743H-40.8l1.997-2.203h8.888z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155437"
                d="M-19.48 130.684l-3.688 2.135v-19.74l3.598 2.136z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155439"
                d="M-96.768 111.806v20.887l-3.349-2.326V113.39z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155441"
                d="M-83.83 146.294h6.33l2.414-2.17h31.652l1.912 2.1h4.327"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155443"
                d="M-26.275 142.741l-1.03.842h-11.883l-.419-.817z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-78.99 105.978h-13.17l1.996-2.203h8.888z"
                id="path155445"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-91.73 143.03l1.03.841h11.883l.418-.816z"
                id="path155447"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <rect
                id="rect155449"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={0.5}
                fill="#377e96"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
            </g>
            <g id="g155481" transform="matrix(.79356 0 0 .8255 1444.475 475.5)" strokeOpacity={1}>
              <rect
                id="rect155453"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={1}
                fill="none"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
              <path
                id="path155455"
                d="M-22.926 104.901v6.336l5.282 3.044v17.447l-5.346 3.44v6.88l-3.575 2.673H-40.67l-1.674-1.955h-33.289l-2.732 2.058h-13.936l-2.87-2.03V104.75l4.054-3.01h10.11l4.873 3.885h32.426l4.509-3.955h9.928z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155457"
                d="M-20.092 112.796v-8.39l-7.213-3.613h-65.566l-4.991 3.44v5.148l-3.768 2.202v20.467l4.025 3.02v3.142l2.48 1.782"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155459"
                d="M-31.092 99.5h-13.071l-2.664-2.712H-74.79l-2.482 2.607h-10.976"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155461"
                d="M-45.817 99.32l-1.757-1.9h-26.845l-1.449 1.9z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155463"
                d="M-40.861 100.821l-3.666 3.456h-31.049l-3.86-3.508z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155465"
                d="M-27.627 105.743H-40.8l1.997-2.203h8.888z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155467"
                d="M-19.48 130.684l-3.688 2.135v-19.74l3.598 2.136z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155469"
                d="M-96.768 111.806v20.887l-3.349-2.326V113.39z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155471"
                d="M-83.83 146.294h6.33l2.414-2.17h31.652l1.912 2.1h4.327"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155473"
                d="M-26.275 142.741l-1.03.842h-11.883l-.419-.817z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-78.99 105.978h-13.17l1.996-2.203h8.888z"
                id="path155475"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-91.73 143.03l1.03.841h11.883l.418-.816z"
                id="path155477"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <rect
                id="rect155479"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={0.5}
                fill="#377e96"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
            </g>
            <g id="g155511" transform="matrix(.79356 0 0 .8255 1519.223 475.5)" strokeOpacity={1}>
              <rect
                id="rect155483"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={1}
                fill="none"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
              <path
                id="path155485"
                d="M-22.926 104.901v6.336l5.282 3.044v17.447l-5.346 3.44v6.88l-3.575 2.673H-40.67l-1.674-1.955h-33.289l-2.732 2.058h-13.936l-2.87-2.03V104.75l4.054-3.01h10.11l4.873 3.885h32.426l4.509-3.955h9.928z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155487"
                d="M-20.092 112.796v-8.39l-7.213-3.613h-65.566l-4.991 3.44v5.148l-3.768 2.202v20.467l4.025 3.02v3.142l2.48 1.782"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155489"
                d="M-31.092 99.5h-13.071l-2.664-2.712H-74.79l-2.482 2.607h-10.976"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155491"
                d="M-45.817 99.32l-1.757-1.9h-26.845l-1.449 1.9z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155493"
                d="M-40.861 100.821l-3.666 3.456h-31.049l-3.86-3.508z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155495"
                d="M-27.627 105.743H-40.8l1.997-2.203h8.888z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155497"
                d="M-19.48 130.684l-3.688 2.135v-19.74l3.598 2.136z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155499"
                d="M-96.768 111.806v20.887l-3.349-2.326V113.39z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155501"
                d="M-83.83 146.294h6.33l2.414-2.17h31.652l1.912 2.1h4.327"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155503"
                d="M-26.275 142.741l-1.03.842h-11.883l-.419-.817z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-78.99 105.978h-13.17l1.996-2.203h8.888z"
                id="path155505"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-91.73 143.03l1.03.841h11.883l.418-.816z"
                id="path155507"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <rect
                id="rect155509"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={0.5}
                fill="#377e96"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
            </g>
            <g id="g155541" transform="matrix(.79356 0 0 .8255 1593.97 475.5)" strokeOpacity={1}>
              <rect
                id="rect155513"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={1}
                fill="none"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
              <path
                id="path155515"
                d="M-22.926 104.901v6.336l5.282 3.044v17.447l-5.346 3.44v6.88l-3.575 2.673H-40.67l-1.674-1.955h-33.289l-2.732 2.058h-13.936l-2.87-2.03V104.75l4.054-3.01h10.11l4.873 3.885h32.426l4.509-3.955h9.928z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155517"
                d="M-20.092 112.796v-8.39l-7.213-3.613h-65.566l-4.991 3.44v5.148l-3.768 2.202v20.467l4.025 3.02v3.142l2.48 1.782"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155519"
                d="M-31.092 99.5h-13.071l-2.664-2.712H-74.79l-2.482 2.607h-10.976"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155521"
                d="M-45.817 99.32l-1.757-1.9h-26.845l-1.449 1.9z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155523"
                d="M-40.861 100.821l-3.666 3.456h-31.049l-3.86-3.508z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155525"
                d="M-27.627 105.743H-40.8l1.997-2.203h8.888z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155527"
                d="M-19.48 130.684l-3.688 2.135v-19.74l3.598 2.136z"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155529"
                d="M-96.768 111.806v20.887l-3.349-2.326V113.39z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155531"
                d="M-83.83 146.294h6.33l2.414-2.17h31.652l1.912 2.1h4.327"
                fill="none"
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path155533"
                d="M-26.275 142.741l-1.03.842h-11.883l-.419-.817z"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-78.99 105.978h-13.17l1.996-2.203h8.888z"
                id="path155535"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                d="M-91.73 143.03l1.03.841h11.883l.418-.816z"
                id="path155537"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
                stroke="#3fdbe4"
                strokeWidth={0.461814}
                strokeLinecap="butt"
                strokeLinejoin="miter"
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <rect
                id="rect155539"
                width={64.573235}
                height={32.26384}
                x={-91.653595}
                y={108.84748}
                ry={4.0586295}
                opacity={0.5}
                fill="#377e96"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.63167}
                strokeLinejoin="round"
                paintOrder="fill markers stroke"
              />
            </g>
            <text
              xmlSpace="preserve"
              style={{
                lineHeight: 1.25,

                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={1806.0752}
              y={423.98865}
              id="text155545"
              transform="scale(.72621 1.37701)"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="8.72133px"
              fontFamily="Franklin Gothic Medium"
              letterSpacing={0}
              wordSpacing={0}
              writingMode="lr-tb"
              textAnchor="start"
              display="inline"
              opacity={0.8}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.865038}
            >
              <tspan
                x={1806.0752}
                y={423.98865}
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan155543"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="8.72133px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.865038}
              >
                {'STATUS'}
              </tspan>
            </text>
            <text
              xmlSpace="preserve"
              style={{
                lineHeight: 1.25,

                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={1899.8866}
              y={423.98865}
              id="text155549"
              transform="scale(.72621 1.37701)"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="8.72133px"
              fontFamily="Franklin Gothic Medium"
              letterSpacing={0}
              wordSpacing={0}
              writingMode="lr-tb"
              textAnchor="start"
              display="inline"
              opacity={0.8}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.865038}
            >
              <tspan
                x={1899.8866}
                y={423.98865}
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan155547"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="8.72133px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.865038}
              >
                {'QUICK MENU'}
              </tspan>
            </text>
            <text
              xmlSpace="preserve"
              style={{
                lineHeight: 1.25,

                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={2004.3073}
              y={423.98865}
              id="text155553"
              transform="scale(.72621 1.37701)"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="8.72133px"
              fontFamily="Franklin Gothic Medium"
              letterSpacing={0}
              wordSpacing={0}
              writingMode="lr-tb"
              textAnchor="start"
              display="inline"
              opacity={0.8}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.865038}
            >
              <tspan
                x={2004.3073}
                y={423.98865}
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan155551"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="8.72133px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.865038}
              >
                {'MAIN MENU'}
              </tspan>
            </text>
            <text
              xmlSpace="preserve"
              style={{
                lineHeight: 1.25,

                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={2107.4097}
              y={423.98865}
              id="text155557"
              transform="scale(.72621 1.37701)"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="8.72133px"
              fontFamily="Franklin Gothic Medium"
              letterSpacing={0}
              wordSpacing={0}
              writingMode="lr-tb"
              textAnchor="start"
              display="inline"
              opacity={0.8}
              fill="#fff"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.865038}
            >
              <tspan
                x={2107.4097}
                y={423.98865}
                style={{
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan155555"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="8.72133px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.865038}
              >
                {'ALARM LOG'}
              </tspan>
            </text>
          </g>
          <rect
            id="rect155561"
            width={241.54265}
            height={112.56418}
            x={76.632324}
            y={80.40699}
            ry={0}
            opacity={0.15}
            fill="url(#linearGradient155687)"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={1.09196}
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="fill markers stroke"
          />
          <rect
            id="rect155563"
            width={241.5426}
            height={112.56418}
            x={76.632324}
            y={80.40699}
            ry={0}
            fill="none"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={1.09196}
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="fill markers stroke"
          />
          <text
            transform="scale(1.1229 .89056)"
            id="text155569"
            y={143.17799}
            x={83.985527}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155565"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={143.17799}
              x={83.985527}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'CORRIENTE:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155575"
            y={107.97223}
            x={108.6864}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="11.0032px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle1}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155571"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={107.97223}
              x={108.6864}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="12.7px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle1}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'INFORMACI\xD3N GENERAL'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155581"
            y={158.93623}
            x={83.930229}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155577"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={158.93623}
              x={83.930229}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'POTENCIA:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155587"
            y={174.55647}
            x={83.580498}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155583"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={174.55647}
              x={83.580498}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'TEMPERATURA:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155593"
            y={203.83388}
            x={83.950127}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155589"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={203.83388}
              x={83.950127}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'C\xD3DIGO DE FALLA:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="cur"
            y={143.17799}
            x={211.25702}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155595"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={143.17799}
              x={211.25702}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              strokeWidth={0.52937}
            >
              {cur} A
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="pow"
            y={158.93623}
            x={210.95941}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155601"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={158.93623}
              x={210.95941}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              strokeWidth={0.52937}
            >
              {pow} KW
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="temp"
            y={174.55647}
            x={211.2901}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155607"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={174.55647}
              x={211.2901}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              strokeWidth={0.52937}
            >
              {temp} °C
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155617"
            y={188.81673}
            x={83.580498}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155613"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={188.81673}
              x={83.580498}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'FRECUENCIA:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="freq"
            y={188.81673}
            x={211.2901}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155619"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={188.81673}
              x={211.2901}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              strokeWidth={0.52937}
            >
              {freq} Hz
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="cod"
            y={203.83388}
            x={211.31757}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155625"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={203.83388}
              x={211.31757}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              strokeWidth={0.52937}
            >
              {alm}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="text155635"
            y={129.51192}
            x={83.985527}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={detalle}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155631"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={129.51192}
              x={83.985527}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={detalle}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {'VOLTAJE:'}
            </tspan>
          </text>
          <text
            transform="scale(1.1229 .89056)"
            id="volt"
            y={129.51192}
            x={210.73615}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="10.2484px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill={datos}
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.52937}
          >
            <tspan
              id="tspan155637"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={129.51192}
              x={210.73615}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="11.2889px"
              fontFamily="Franklin Gothic Medium"
              fill={datos}
              fillOpacity={1}
              strokeWidth={0.52937}
            >
              {vol} V
            </tspan>
          </text>
          <path
            d="M318.721 193.517v-3.673h-43.188l-3.744 3.416z"
            id="path155643"
            display="inline"
            fill="#3fdbe4"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".366727px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M76.086 79.861v3.673h43.189l3.744-3.416z"
            id="path155645"
            display="inline"
            fill="#3fdbe4"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".366727px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={78.028557}
            y={42.814827}
            id="text155651"
            transform="scale(.72621 1.37701)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="14.3696px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.979742}
          >
            <tspan
              x={78.028557}
              y={42.814827}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              id="tspan155649"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="14.3696px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.979742}
            >
              {
                <a href={url} target="_blank" style={{ fill: 'white' }}>
                  {' '}
                  {equipo}{' '}
                </a>
              }
            </tspan>
          </text>
          <g
            id="g156164"
            transform="translate(-1236.408 -342.013)"
            fillRule="evenodd"
            stroke="none"
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <ellipse
              ry={7.6262836}
              rx={7.5597944}
              cy={443.91296}
              cx={1293.3873}
              id="ellipse155653"
              display="inline"
              fill={led}
              fillOpacity={1}
              strokeWidth={0.582092}
            />
            <circle
              transform="matrix(.37022 0 0 .3649 1177.227 406.288)"
              r={11.973675}
              cy={94.162521}
              cx={312.73749}
              id="circle155655"
              display="inline"
              opacity={0.65}
              fill="#fff"
              fillOpacity={0.996078}
              strokeWidth={7.34263}
              filter="url(#filter4280)"
            />
          </g>
          <text
            transform="scale(.8236 1.2142)"
            id="text155659"
            y={86.522171}
            x={34.452347}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.784115}
          >
            <tspan
              id="tspan155657"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={86.522171}
              x={34.452347}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.784115}
            >
              {'EST'}
            </tspan>
          </text>
          <text
            transform="scale(.80848 1.2369)"
            id="text155663"
            y={113.87425}
            x={35.799671}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.798778}
          >
            <tspan
              id="tspan155661"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={113.87425}
              x={35.799671}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.798778}
            >
              {'ALM'}
            </tspan>
          </text>
          <text
            transform="scale(.78045 1.2813)"
            id="text155667"
            y={138.3324}
            x={36.396378}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.827456}
          >
            <tspan
              id="tspan155665"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={138.3324}
              x={36.396378}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87778px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.827456}
            >
              {text_led}
            </tspan>
          </text>
          <g
            id="g156160"
            transform="translate(-1236.408 -337.606)"
            fillRule="evenodd"
            stroke="none"
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <ellipse
              ry={7.6262836}
              rx={7.5597944}
              cy={475.22662}
              cx={1293.3873}
              id="ellipse155669"
              display="inline"
              fill="#4d4d4d"
              fillOpacity={1}
              strokeWidth={0.582092}
            >
              <animate
                attributeName="fill"
                from="#4d4d4d"
                to={st_alarma[0]}
                dur={st_alarma[1]}
                repeatCount="indefinite"
              />
            </ellipse>
            <circle
              transform="matrix(.37022 0 0 .3649 1177.226 437.602)"
              r={11.973675}
              cy={94.162521}
              cx={312.73749}
              id="circle155671"
              display="inline"
              opacity={0.65}
              fill="#fff"
              fillOpacity={0.996078}
              strokeWidth={7.34263}
              filter="url(#filter4280-8)"
            />
          </g>
          <g
            id="g156156"
            transform="translate(-1236.408 -367.942)"
            fillRule="evenodd"
            stroke="none"
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <ellipse
              ry={7.6262836}
              rx={7.5597944}
              cy={541.81287}
              cx={1293.3873}
              id="ellipse155673"
              display="inline"
              fill="#4d4d4d"
              fillOpacity={1}
              strokeWidth={0.582092}
            >
              <animate
                attributeName="fill"
                from="#4d4d4d"
                to={estado_alm[0]}
                dur={estado_alm[1]}
                repeatCount="indefinite"
              />
            </ellipse>
            <circle
              transform="matrix(.37022 0 0 .3649 1177.226 504.188)"
              r={11.973675}
              cy={94.162521}
              cx={312.73749}
              id="circle155675"
              display="inline"
              opacity={0.65}
              fill="#fff"
              fillOpacity={0.996078}
              strokeWidth={7.34263}
              filter="url(#filter4280-5)"
            />
          </g>
          <path
            d="M163.597 66.57a2.007 2.007 0 00-.277-.034 4.712 4.712 0 01-.376-.043l-.367-.067a7.104 7.104 0 01-.555-.11l-.2-.054c-.455-.122-.618-.205-.834-.426-.097-.099-.234-.336-.234-.405a.146.146 0 00-.022-.065c-.078-.08-.024-1.186.071-1.398.105-.237.621-.767 1.134-1.166.164-.127.301-.24.305-.25 0-.01.017-.018.03-.018.013 0 .104-.063.208-.14.303-.224.711-.497 1.134-.76.134-.083.31-.193.389-.244a6.34 6.34 0 01.278-.167c.073-.04.178-.1.233-.134l.234-.139c.073-.043.273-.154.445-.245l.4-.217c.123-.069 1.363-.689 1.378-.689.014 0 .148-.065.314-.145a6.82 6.82 0 01.312-.144s.052-.02.103-.043l.416-.18c.178-.075.353-.15.39-.167.035-.017.146-.061.244-.099.098-.037.198-.077.222-.088l.245-.09a7.16 7.16 0 00.355-.133c.036-.014.125-.046.2-.07.075-.025.154-.053.179-.063.025-.01.099-.035.166-.056.068-.021.143-.046.167-.057.025-.01.119-.04.211-.066.091-.027.192-.056.223-.067.03-.01.116-.036.189-.056.073-.02.153-.045.177-.055.025-.01.11-.036.193-.058.082-.022.157-.056.168-.074.014-.02.07-.125.128-.235l.167-.311c.033-.061.124-.231.2-.378.076-.147.146-.277.155-.29.014-.012.078-.141.155-.288a28.9 28.9 0 01.378-.712c.053-.097.15-.278.213-.4s.135-.257.16-.3l.194-.367c.204-.388.304-.573.323-.6.014-.013.104-.193.212-.4.107-.208.202-.388.21-.4.015-.013.074-.128.14-.256l.2-.378.183-.345c.059-.11.128-.24.156-.289.028-.049.12-.224.206-.39.086-.164.188-.36.228-.433.183-.339.257-.477.35-.654l.31-.58c.05-.091.145-.271.212-.4.068-.128.152-.288.19-.356.096-.177.388-.726.454-.856.032-.06.116-.22.19-.355.073-.135.199-.373.281-.529.082-.156.166-.312.187-.348.02-.036.032-.071.024-.079-.035-.034-.59-.045-2.254-.045-1.5 0-1.894.007-2.357.04-.58.041-1.068.087-1.334.124l-.4.057c-.134.019-.33.049-.434.067a24.38 24.38 0 01-.345.056 3.52 3.52 0 00-.233.044l-.189.043c-.188.04-.504.114-.645.15-.073.02-.178.044-.233.055-.055.01-.126.029-.156.04s-.175.052-.322.09c-.147.04-.284.08-.305.092a.185.185 0 01-.064.02 4.56 4.56 0 00-.514.158.184.184 0 01-.066.02 1.2 1.2 0 00-.173.053c-.08.03-.215.075-.3.102a3.21 3.21 0 00-.2.067c-.024.012-.16.061-.3.112-.245.088-.382.14-.479.18l-.544.213a7.237 7.237 0 00-.334.138c-.03.014-.165.068-.3.122a10.4 10.4 0 00-.389.165c-.08.036-.235.103-.345.148-.11.046-.219.093-.242.105-.048.025-.826.384-1.08.498-.093.04-.3.14-.464.22a3.63 3.63 0 01-.316.145c-.014 0-.021.01-.021.022 0 .013-.014.022-.032.022-.03 0-1.815.889-2.381 1.186l-.5.26a4.661 4.661 0 00-.245.133c-.032.03-.43.223-.44.212-.014-.014.014-.068.105-.223.037-.061.073-.125.082-.14.014-.025.202-.355.437-.76l.139-.245a32.848 32.848 0 01.389-.678c.02-.037.07-.12.107-.185a1.42 1.42 0 00.071-.125c0-.007.113-.204.2-.346.014-.019.086-.149.166-.29a57.419 57.419 0 01.378-.655c.073-.131.568-.99.615-1.067l.07-.123.154-.266.179-.313c.105-.19.115-.198.273-.256a11.68 11.68 0 01.712-.257c.073-.024.153-.053.177-.063.025-.011.14-.05.256-.09l.244-.082c.186-.069.552-.196.668-.232.078-.025.16-.053.178-.063a.579.579 0 01.11-.034.7.7 0 00.111-.034c.02-.01.14-.05.268-.089.128-.039.253-.078.278-.087.024-.01.105-.034.178-.056.073-.021.153-.047.177-.057.025-.01.1-.033.167-.052l.423-.122a26.316 26.316 0 01.533-.15c.156-.044.297-.08.534-.14.273-.069.446-.113.522-.135.037-.01.147-.035.245-.056.098-.02.213-.046.256-.057.106-.027.33-.075.556-.12.103-.02.229-.047.277-.058.05-.011.144-.03.212-.04.068-.012.212-.037.322-.058.11-.02.26-.045.334-.056a16.304 16.304 0 01.845-.125l.389-.052c.107-.016.23-.03.7-.077 1.85-.19 4.019-.206 6.17-.046.838.063.94.072 1.155.1.247.032.218.053.417-.287.069-.117.13-.218.138-.224.014-.007.026-.037.04-.067.015-.03.05-.092.078-.137l.123-.202c.086-.146.051-.136.74-.204.054-.006.24-.025.41-.045.172-.02.407-.045.523-.056a102.518 102.518 0 011.69-.178c.129-.012.32-.034.425-.047.105-.014.203-.022.217-.017.014.005-.146.253-.354.553a133.323 133.323 0 00-.791 1.145c-.043.06-.091.13-.11.155a39.93 39.93 0 01-.155.22c-.244.346-.303.44-.292.469 0 .016.122.085.256.153.436.22.767.432 1.137.727.874.699 1.418 1.536 1.643 2.526l.073.319c.015.07.032.358.037.678.015.523 0 .789-.062 1.123a5.446 5.446 0 00-.044.278 1.343 1.343 0 01-.043.2c-.014.024-.04.13-.064.233a5.706 5.706 0 01-.113.434c-.014.024-.051.15-.091.278a8.063 8.063 0 01-.09.267c-.014.018-.034.078-.053.133a8.388 8.388 0 01-.21.496 1.936 1.936 0 00-.068.164 19.114 19.114 0 01-.631 1.219 16.06 16.06 0 01-.18.295c-.086.137-.166.268-.178.29a25.99 25.99 0 01-.25.376c-.125.185-.217.343-.206.35.034.022 1.767.013 1.775-.008 0-.015.214-.366.305-.51a5.59 5.59 0 01.303-.43c.747-1 1.754-1.853 2.61-2.207.409-.17.568-.216.909-.258.228-.028.697-.016.869.023l.189.043c.149.032.473.173.704.307.112.065.213.118.223.118.014 0 .143-.127.298-.28l.281-.281.33-.03.587-.053c.14-.012.336-.032.434-.044.322-.04.723-.075.735-.064.014.011-.083.2-.234.473l-.179.334a9.347 9.347 0 01-.156.289c-.044.076-.177.324-.339.634-.06.116-.135.254-.165.307l-.14.256c-.21.401-.296.561-.311.582-.015.02-.061.105-.345.645a23.329 23.329 0 00-.48.9.843.843 0 01-.063.111 1.528 1.528 0 00-.156.323c0 .04.082.043.211.01.247-.065.67-.3 1.012-.564.375-.29 1.078-.943 1.41-1.312.868-.962 1.198-1.366 1.926-2.359.11-.15.54-.771.554-.8a2.66 2.66 0 01.157-.234c.014-.007.02-.027.026-.046.014-.028.06-.04.21-.051l.608-.048a96.369 96.369 0 011.15-.089.936.936 0 01.162-.006c0 .006-.061.119-.136.252l-.259.465-.179.323-.16.289c-.06.104-.13.23-.158.282-.029.05-.046.1-.037.107.014.013.186-.086.219-.124a2.37 2.37 0 00.233-.155c.085-.061.22-.157.3-.212l1-.69c.08-.054.31-.213.511-.353.61-.424.794-.522 1.128-.598a.826.826 0 01.455-.005c.388.101.577.412.687 1.128l.026.17-.117.208c-.064.114-.17.292-.233.396-.063.104-.193.324-.289.49-.095.165-.23.395-.3.51a24.93 24.93 0 00-.33.568.556.556 0 01-.05.078c0 .007-.071.115-.144.243-.171.3-.212.37-.362.624-.07.116-.135.23-.15.255-.088.163-.15.27-.168.293-.014.016-.014.031 0 .04.033.022.594-.263.863-.438.033-.021.087-.054.122-.073.138-.078.63-.423.883-.618l.167-.127a13.664 13.664 0 001.668-1.534c.337-.372.844-1.063.808-1.1-.014-.01-.596.008-1.385.048l-.286.014.014-.054c.014-.06.164-.498.194-.564.022-.048.165-.455.211-.6l.044-.138c.021-.061.057-.068.445-.085.172-.007.417-.022.545-.034.128-.01.429-.032.667-.045.239-.014.45-.029.47-.034.032-.01.12-.131.213-.298.051-.092.81-1.331 1.097-1.793.11-.175.199-.324.199-.33 0-.007.029-.053.064-.106.036-.052.173-.27.304-.484.132-.214.263-.42.291-.458a.263.263 0 00.051-.093c0-.013.014-.027.018-.03.014-.006.041-.048.073-.097.03-.049.172-.267.316-.485.144-.218.263-.401.263-.407 0-.007.014-.025.028-.041.015-.017.082-.112.15-.212.068-.1.157-.231.2-.29l.233-.331c.409-.585.912-1.241 1.482-1.93.178-.216.245-.292.617-.695.918-.996 1.899-1.8 2.804-2.3.209-.115.644-.325.83-.4.096-.039.36-.048.382-.013a.085.085 0 00.058.022c.073 0 .238.086.335.174.2.181.268.367.266.735 0 .239-.016.342-.095.637-.073.272-.394 1-.613 1.39l-.116.207a12.076 12.076 0 01-1.225 1.727c-.014.007-.052.06-.1.122-.05.061-.092.116-.1.122 0 .007-.109.12-.229.252-.641.706-1.595 1.6-2.605 2.44l-.255.213c-.073.063-.697.579-1.053.871l-.324.267-.315.26c-.15.122-.393.327-.543.455-.149.129-.297.254-.328.278-.114.09-.24.212-.245.238 0 .019.044.025.173.021.178-.006.735-.053 1.067-.092l.511-.056c.184-.019.369-.04.411-.046.071-.011.388-.044.914-.098.105-.01.268-.03.363-.043.095-.014.23-.024.3-.024.071 0 .158-.01.193-.02.036-.011.197-.027.36-.035l.294-.013v.353l-.172.11c-.762.484-1.555 1.276-2.144 2.14a6.888 6.888 0 00-.542.945l-.12.266a1.6 1.6 0 00-.066.178c-.015.05-.04.14-.06.2a1.979 1.979 0 00-.097.618c0 .192.014.234.063.358.085.188.165.244.366.255.172.01.39-.036.59-.122.571-.25 1.103-.624 1.637-1.153.33-.327.477-.502.728-.867.182-.264.395-.75.395-.899 0-.055-.046-.135-.124-.217-.066-.069-.207-.41-.258-.623-.037-.158-.049-.706-.018-.851.144-.67.452-1.089 1.09-1.483.33-.204.372-.218.643-.218.225 0 .24.005.368.075a.67.67 0 01.301.303c.12.205.143.32.143.711 0 .345-.016.477-.094.767a2.748 2.748 0 01-.234.608.808.808 0 00-.078.16c.015.012.546-.17.69-.231.058-.026.113-.048.123-.05.075-.016.61-.255.906-.404.41-.208.9-.528 1.285-.843.353-.287.818-.848.844-1.018 0-.043.016-.103.024-.134 0-.03.018-.105.024-.167.014-.06.024-.156.034-.21.014-.056.037-.19.057-.301.049-.267.166-.75.19-.777.015-.011.02-.035.02-.052 0-.059.136-.335.262-.538.042-.068.179-.223.303-.346.191-.19.26-.24.448-.337.287-.147.478-.193.748-.18.175.006.23.02.357.078.314.147.398.33.351.757a1.61 1.61 0 01-.115.487c-.117.338-.203.462-.562.82-.498.494-.763.838-.978 1.27-.102.205-.13.286-.148.425-.014.094-.028.198-.035.23-.03.143.104.584.203.666.028.022.15-.006.288-.067.025-.01.274-.095.556-.187.82-.269 1.294-.443 1.734-.636.049-.022.134-.058.189-.08a19.377 19.377 0 001.24-.639c.385-.23.362-.196.383-.561.06-1.037.174-1.403.685-2.2a1.546 1.546 0 011.272-.708c.236-.005.37.052.475.202.13.183.152.273.151.609 0 .529-.165 1.092-.39 1.332a4.324 4.324 0 00-.159.18 35.04 35.04 0 01-.433.488 96.49 96.49 0 00-.504.564l-.137.156.018.578c.014.318.036 1.069.06 1.668.056 1.55.054 1.662-.02 1.926a3.606 3.606 0 01-.088.278c-.231.567-.39.804-.81 1.212-.211.204-.38.333-.628.48-.462.272-.806.401-1.372.518-.19.04-.27.044-.545.033-.62-.025-.832-.066-1.167-.224a1.317 1.317 0 01-.487-.474c-.162-.319-.14-.851.053-1.326.147-.36.244-.519.551-.906.051-.066.374-.388.493-.494a6.82 6.82 0 01.492-.378l.294-.195c.142-.095.262-.172.267-.172 0 0 .105-.054.22-.12.202-.114.89-.458.918-.458.015 0 .068-.024.266-.111l.155-.067c.138-.058.177-.093.274-.243.12-.186.138-.284.105-.575a2.03 2.03 0 00-.042-.27c-.018-.03-.068-.03-.118-.005-.046.026-.312.147-.65.296l-.856.383-.211.095-.9.405a221.762 221.762 0 01-1.428.644c-.135.058-.13.043-.162.457a6.998 6.998 0 01-.043.4c-.014.061-.037.211-.058.334a4.914 4.914 0 01-.189.756c-.015.024-.036.094-.057.155a3.978 3.978 0 01-.328.666c-.073.123-.14.228-.147.235-.014.007-.049.06-.09.122-.56.807-1.29 1.232-2.346 1.368-.29.038-1.097.051-1.267.022-.499-.087-.797-.336-.942-.79-.046-.144-.05-.697 0-.889.083-.367.19-.627.37-.897.179-.27.567-.668.79-.808a.875.875 0 00.152-.115c.017-.02.038-.036.049-.036.014 0 .097-.052.191-.115a16.914 16.914 0 012.51-1.287c.072-.03.145-.062.16-.073.02-.015.028-.128.028-.43 0-.345 0-.408-.034-.408a.679.679 0 00-.175.134c-.27.254-.872.699-1.14.842a6.325 6.325 0 00-.197.113c-.182.108-.788.4-.83.4a.774.774 0 00-.14.053 4.72 4.72 0 01-.342.125c-.123.04-.242.081-.267.091-.024.01-.179.05-.344.089l-.378.088a6.186 6.186 0 01-.604.089c-.053 0-.076.03-.209.26-.23.402-.517.845-.784 1.207-.336.455-.6.756-1.103 1.257-.787.785-1.094 1.028-1.653 1.309-.232.116-.396.179-.694.266-.444.13-1.26.138-1.546.015a12.677 12.677 0 00-.289-.114c-.287-.112-.428-.197-.57-.345-.099-.102-.241-.347-.241-.414 0-.017-.014-.04-.02-.051-.032-.035-.074-.356-.085-.655-.015-.266.02-.503.082-.69.014-.03.037-.115.06-.188.022-.074.052-.159.068-.19a.298.298 0 00.029-.076c0-.083.289-.627.484-.913.223-.327.639-.854.903-1.145.039-.043.135-.158.213-.256l.157-.189a.617.617 0 00.11-.175c0-.03-.103-.032-.229-.005a7.638 7.638 0 01-.327.057 24.345 24.345 0 00-.667.111c-.448.075-.5.087-.539.119-.02.018-.039.04-.039.048s-.049.105-.106.213c-.152.282-.31.58-.471.888a5.103 5.103 0 01-.157.289c-.014.012-.084.152-.166.31a7.772 7.772 0 01-.165.312c-.014.013-.082.147-.162.3-.185.355-.301.572-.35.656-.033.058-.139.257-.393.745a1.51 1.51 0 01-.075.133c0 .007-.06.107-.119.223-.06.116-.143.276-.187.356-.078.142-.113.21-.332.622-.163.309-.272.51-.329.612-.027.049-.105.194-.172.322l-.236.45a3.08 3.08 0 00-.115.23c0 .017.2.004.656-.046.184-.02.43-.045.545-.056.116-.011.256-.027.312-.034.153-.019.498-.054.744-.076.122-.011.313-.03.423-.043.37-.043.621-.069.789-.08.091-.008.242-.022.334-.034.09-.011.286-.033.433-.046l.456-.045a93 93 0 011.345-.122l.623-.054 1.122-.1c.068-.007.263-.022.434-.035.171-.014.421-.033.556-.045a140.79 140.79 0 012.501-.175c.202-.01.422-.027.49-.034.067-.007.292-.023.5-.034l.989-.055c.336-.02.797-.044 1.023-.055.226-.011.501-.026.611-.034.324-.022 2.639-.111 3.747-.145l1.033-.033c.39-.014 2.192-.053 3.221-.07.755-.013.775-.013.753.029a.782.782 0 01-.062.086 2.045 2.045 0 00-.107.133 1.235 1.235 0 01-.095.12c-.025.026-.157.193-.361.459-.239.31-.337.432-.356.444-.05.032-.014.096.129.2.082.061.22.172.306.245.087.073.406.328.707.567.3.238.547.44.548.446 0 .018-.772.005-1.267-.024-.942-.052-4.772-.112-7.148-.112-2.528 0-6.034.055-7.06.112-.226.012-.736.033-1.134.047-1.568.054-2.575.098-3.001.131-.153.013-.513.034-.8.048l-.801.04-.767.043c-.27.014-.57.035-.667.046-.153.018-.44.037-1.234.08-.091.007-.297.02-.456.033-.159.013-.44.032-.623.043-.183.01-.36.03-.393.042a.326.326 0 00-.121.13l-.316.529c-.254.421-.512.855-.72 1.21a4.978 4.978 0 01-.152.248.318.318 0 00-.043.073c0 .007-.038.075-.086.152-.082.134-.445.74-.516.86-.02.032-.09.156-.162.275l-.129.217h-2.464l.069-.128c.037-.07.075-.138.085-.15.015-.013.146-.248.305-.523a354.051 354.051 0 01.985-1.7c.02-.037.111-.192.2-.345.394-.682.399-.691.365-.704a1.02 1.02 0 00-.215 0l-.729.048-.878.056c-.183.01-.404.026-.489.033l-.478.035a244.399 244.399 0 00-4.069.321c-.238.02-.514.045-.611.056-.098.011-.298.032-.445.045-.147.012-.367.033-.49.045l-.344.034-.311.03c-.104.011-.31.03-.456.043a9.6 9.6 0 00-.422.046c-.086.012-.241.028-.345.035a4.232 4.232 0 00-.311.033c-.068.01-.283.036-.478.055-.196.02-.421.044-.5.055-.08.011-.205.027-.279.033a6.076 6.076 0 00-.222.024 43.405 43.405 0 00-1.19.141c-.178.024-.585.074-1 .124a63.143 63.143 0 00-1.79.234l-.244.034-.245.033a111.252 111.252 0 00-2.057.29 2.746 2.746 0 00-.222.035 3.03 3.03 0 01-.2.033c-.08.01-.25.034-.378.053-.234.035-.512.075-.582.084-.02.005-.04-.005-.042-.013 0-.011.147-.221.332-.468.37-.494.356-.483.636-.515.06-.007.146-.021.189-.032a5.34 5.34 0 01.311-.057c.128-.02.274-.045.322-.056a8.63 8.63 0 01.467-.089c.061-.01.202-.033.312-.053.11-.02.26-.045.333-.057.073-.012.184-.031.245-.044.06-.012.201-.036.311-.055l.267-.046c.073-.015.36-.064.656-.112.103-.017.239-.04.3-.052.163-.031.512-.09.678-.114.08-.01.17-.027.2-.035.055-.014.14-.028.523-.088.387-.06.706-.114.867-.144.06-.011.221-.037.355-.056a18.724 18.724 0 00.679-.109c.174-.027.553-.083.689-.102.08-.011.174-.026.211-.034.036-.007.131-.022.211-.032.08-.011.25-.035.378-.055.129-.02.304-.045.39-.057l.233-.035a19.685 19.685 0 01.9-.132c.21-.026.586-.081.679-.1.055-.01.175-.025.266-.033.091-.007.212-.022.267-.033.055-.012.2-.032.322-.047a63.447 63.447 0 00.779-.095c.146-.018.386-.048.533-.068.326-.044.553-.071.845-.1.122-.013.298-.033.39-.045.09-.013.311-.038.488-.057.178-.019.398-.043.49-.055.09-.01.242-.026.333-.033.091-.007.237-.022.323-.033.086-.011.325-.036.533-.055l.478-.045c.186-.02.854-.08 1.123-.1.384-.029.794-.065 1.012-.09.103-.01.329-.027.5-.035.171-.008.416-.026.543-.04l.233-.026.058-.1c.032-.054.1-.179.154-.277a27.7 27.7 0 01.201-.366c.183-.33.384-.697.445-.812.033-.061.134-.246.226-.411l.27-.492c.191-.354.191-.353.146-.353-.025 0-.595.375-.876.577-.472.34-1.107.746-1.38.884-.383.194-.395.2-1.004.415-.54.192-.868.21-1.125.062-.216-.124-.429-.377-.476-.564-.036-.143.024-.558.109-.763.014-.024.035-.094.055-.156.082-.251.367-.826.643-1.296.014-.02.057-.098.098-.171a5.56 5.56 0 01.155-.256l.107-.167c.112-.187.21-.34.638-.997a1.73 1.73 0 00.161-.274c-.014-.01-.206.115-.236.15-.014.011-.133.111-.542.439a7.447 7.447 0 00-.156.127l-.192.166a24.603 24.603 0 00-2.056 2.037 36.606 36.606 0 00-1.634 1.98l-.145.18-.289.026-.367.036c-.068.008-.293.03-1.028.096a.517.517 0 01-.173 0c0-.008.167-.347.371-.751s.364-.74.357-.748c-.014-.007-.041.006-.076.028-.071.047-.1.064-.285.174a10.53 10.53 0 00-.167.1c-.1.067-.477.3-.611.381l-.74.457c-.32.2-.589.363-.596.363-.014 0-.044.023-.082.05-.116.082-.148.1-.415.234-.333.166-.449.205-.613.205-.185 0-.319-.071-.572-.305-.293-.272-.418-.362-.5-.362-.037 0-.082.013-.098.028-.215.2-1 .706-1.665 1.072-.454.251-.524.268-1.11.268-.417 0-.491-.011-.75-.103a2.255 2.255 0 01-.758-.474c-.134-.126-.148-.151-.239-.402-.06-.166-.064-.2-.064-.511 0-.188.014-.372.026-.423.014-.048.043-.168.062-.266.021-.098.052-.245.072-.327.042-.186.033-.22-.064-.207-.04.006-.313.02-.607.032-.834.035-.93.041-.97.057a.975.975 0 00-.172.174 30.894 30.894 0 01-.32.38c-.29.347-.689.775-1.227 1.315-.503.505-.75.732-1.522 1.4a30.91 30.91 0 01-2.263 1.723 57.98 57.98 0 01-1.825 1.215l-.495.319c-.39.265-.758.459-1.634.862l-.467.216a35.088 35.088 0 01-1.634.716c-.293.12-.366.15-.52.218a.417.417 0 01-.098.033.883.883 0 00-.152.054l-.174.07c-.315.114-.422.151-.545.19-.08.026-.21.07-.289.098-.08.028-.25.082-.378.121l-.367.112a6.271 6.271 0 01-.333.088c-.11.026-.225.056-.256.066-.03.01-.135.036-.234.056-.098.02-.237.05-.31.068-.18.04-.35.072-.601.113-.025.005-.129.017-.23.03a2.565 2.565 0 00-.223.034 3.701 3.701 0 01-.326.037c-.301.027-1.838.052-1.86.03zm3.794-1.898c.201-.025.223-.028.467-.069a10.002 10.002 0 001.667-.448c.044-.02.318-.122.512-.19.05-.018.113-.043.144-.056a5.39 5.39 0 01.334-.13 33.218 33.218 0 001.178-.478 17.044 17.044 0 00.8-.353 25.067 25.067 0 001.863-.933c.471-.266 1.386-.836 1.528-.953l.279-.206a13.393 13.393 0 001.657-1.402c.451-.448.738-.756.933-1 .048-.062.095-.117.102-.123.015-.007.095-.12.196-.255s.195-.26.21-.278c.107-.136.281-.405.272-.42-.015-.018-.187.01-.39.063l-.179.044a20.39 20.39 0 01-.811.192c-.232.05-.464.102-.545.12-.043.012-.143.031-.222.046-.08.014-.165.034-.19.044a.68.68 0 01-.122.03 4.603 4.603 0 00-.234.048l-.3.067-.545.121c-.15.034-.367.086-.611.146l-.356.088a7.319 7.319 0 00-.255.067 4.734 4.734 0 01-.212.056c-.042.01-.132.034-.2.055l-.189.056c-.416.12-.974.299-1.039.334a.177.177 0 01-.063.022.363.363 0 00-.095.031 3.347 3.347 0 01-.208.08.581.581 0 00-.17.086 8.89 8.89 0 00-.248.358l-.248.367-.995 1.494c-.53.797-1.003 1.491-1.05 1.542-.22.237-.497.317-.772.223-.195-.068-.259-.112-.259-.183a.136.136 0 00-.02-.073c-.015-.007-.028-.058-.037-.113-.014-.055-.024-.105-.034-.112-.035-.021-.022-.094.033-.192.095-.173.594-1.14 1.049-2.036.12-.235.21-.435.203-.443-.014-.013-.235.042-.337.083-.025.01-.154.053-.289.095a10.409 10.409 0 00-.9.327c-.684.26-1.76.846-2.272 1.237a.96.96 0 01-.14.097c-.014 0-.394.298-.47.366-.448.402-.823.833-.988 1.134-.123.226-.16.319-.2.497a.348.348 0 000 .208c.067.204.334.384.734.496.115.032.24.068.278.08.038.011.162.03.273.04.112.01.211.026.22.034.022.022 1.025 0 1.261-.028zm19.23-6.755a5.43 5.43 0 001.945-1.352 4.51 4.51 0 00.85-1.313 2.24 2.24 0 00.132-.403c0-.017.014-.049.02-.07.015-.021.038-.118.059-.216.02-.098.06-.231.09-.296.062-.139.064-.184.017-.337a.451.451 0 00-.348-.306.893.893 0 00-.449.055c-.21.09-.916.522-.99.605 0 .007-.081.069-.166.138a5.96 5.96 0 00-1.088 1.235c-.14.222-.423.776-.54 1.063-.14.338-.169.46-.17.711 0 .203 0 .23.06.315.061.099.139.161.26.215.106.045.092.047.32-.044zm-12.344-.655a.301.301 0 01.113-.02c.04 0 .105-.01.14-.022.035-.013.134-.032.22-.044.086-.013.241-.037.345-.056l.311-.055.323-.058.389-.068c.33-.056.429-.073.633-.11.11-.02.285-.051.39-.068.103-.018.234-.04.289-.051.055-.011.2-.037.322-.057.26-.044.554-.095.711-.124a96.605 96.605 0 011.012-.178c.248-.044.483-.085.685-.118a.97.97 0 00.24-.06c.035-.029.36-.571.66-1.1.26-.454.498-.936.77-1.556.023-.049.069-.164.102-.255a7.23 7.23 0 01.089-.234c.015-.037.055-.157.089-.267.034-.11.07-.22.08-.244.014-.025.027-.09.04-.145.014-.055.04-.185.064-.289.119-.531.155-.968.117-1.453a4.205 4.205 0 00-.044-.403 4.845 4.845 0 00-.21-.612c-.24-.522-.644-.955-1.198-1.287a5.092 5.092 0 00-.706-.345l-.09-.031-.128.192a5.485 5.485 0 01-.139.204c-.013.007-.142.206-.3.444-.158.239-.293.439-.301.445-.014.007-.018.022-.022.034 0 .013-.159.247-.343.519-.183.272-.378.564-.433.648-.387.592-1.612 2.413-1.627 2.42-.014.004-.018.016-.018.027 0 .018-.238.392-.267.42 0 .007-.147.217-.312.467-.356.539-.267.406-.795 1.191-.649.967-.673 1.003-.855 1.277-.092.141-.193.291-.224.334-.153.217-.46.695-.46.715 0 .027.277.005.338-.028zm41.397-2.879c.272-.121.619-.382.932-.702.217-.22.454-.564.523-.757.175-.486.205-.613.154-.633-.02-.007-.046-.005-.06.013a.106.106 0 01-.061.025c-.044 0-.246.059-.343.1a3.14 3.14 0 01-.175.065 3.398 3.398 0 00-.603.315 2.942 2.942 0 00-.273.233c-.523.503-.804 1.199-.563 1.394.06.048.305.021.47-.052zm5.35-.222a.379.379 0 00.107-.032.26.26 0 01.078-.03c.068-.016.35-.197.58-.37.297-.227.641-.612.766-.857a.375.375 0 01.06-.095c.015-.005.018-.02.018-.034a.29.29 0 01.029-.082c.049-.094.142-.359.17-.483.035-.152.021-.188-.06-.163a6.097 6.097 0 01-.197.057c-.073.02-.15.046-.171.057a.217.217 0 01-.082.022c-.024 0-.05.01-.057.02a.198.198 0 01-.08.035c-.111.024-.471.219-.646.349-.165.122-.405.36-.514.507-.112.152-.302.506-.33.613-.042.171-.04.378 0 .424.045.044.172.095.21.084a1.11 1.11 0 01.113-.022zm-12.192-6.513c.112-.091.29-.239.397-.328l.465-.384c.15-.122.313-.258.363-.302l.225-.2.311-.276c.198-.174.68-.652.9-.891a8.033 8.033 0 001.077-1.444 7.407 7.407 0 00.376-.767 1.2 1.2 0 00.07-.183c.014-.039.024-.086.034-.105.03-.053.087-.242.087-.286 0-.021.014-.049.02-.06.034-.036.062-.278.046-.362-.034-.146-.148-.19-.327-.128-.097.034-.53.31-.75.478-.412.315-.985.869-1.265 1.225l-.18.228c-.019.021-.033.048-.033.058 0 .011-.014.032-.033.047-.037.031-.42.603-.513.767l-.194.345a28.078 28.078 0 00-.826 1.653 8.118 8.118 0 00-.124.271c0 .01-.217.474-.246.528-.064.118-.126.283-.107.283.014 0 .115-.075.227-.167z"
            id="path155679"
            fill="#168198"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.022234}
          />
          <image
            width={34.415894}
            height={46.350998}
            preserveAspectRatio="none"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAACWCAYAAADKZDY/AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAA GXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAIABJREFUeJztnXmQZEed3z+Z76j7 6LtHMy2NNCNpNBI6EIcELJJAIAmW3dgDrfcwFmCz4rKJWCIWOxzWEPYfttcRDofXjg1HOOz1sRz2 LiJgzS5oYThkQBoYCWlGc199d3VVV3fd9Y70H1XvdVV33VU9Yh18Y3qq6r18mfny935n/jIf/AK/ wC+wG+L17kCv+PKXv6wdOXLkVtu237awsBAxTTMdCoWyoVAoG4lEspqmZZPJ5ObMzExJCOG+3v0d BD+3xFBKyQsXLkQLhcL9pVLp8bW1tcdXV1cPrayshAuFgpienla6rjuapjmGYdiGYTimaVqJRKI4 OTmZyWQy6Ww2mw6Hw+uGYawHg8H1YDCYNk0zrev6uqZpaV3XN0ulUjmRSDiHDx92jx8/rh5++GH3 9SLmzxUxVlZWIqlU6sZ8Pv+OQqHwxPr6+i+tra1NptNpdF0nEokQi8WIxWKYpulfJ0TtNlzXJZvN sry8TLFYJBKJEA6HkVIipUQIgaZpRCIRJiYmCIVC9tLSUr5YLG6Ew+GMYRgZTdM2DMPwiJYWQqzH 4/H1SCSSWVtb2zJNs5RMJiuO41QmJycrZ8+erT788MOOEEINe/+vKzGUUuL06dMzwL2ZTObRTCbz zvX19aOZTCaSy+UwDINkMkkymSQYDKJpGlJKTNPENE0cxyGfz5NKpUilUjiOQzweJ5lMEg6HMU2z iQiGYWDbNltbW6yurpLNZtF1nUQigWEYuG6NIYQQCCEIBAJMTU2h6zqrq6tqbW3NisViBcMw8rqu 5w3DyBmGkdM0bcM0zYxpmuvT09PrlmWlNzc3M4ZhbIXD4Vw4HM4BeSB/9OjRkhDCaTUe150YSil5 6dKlw/l8/tF0Ov3Y2trafalUajaTyRgAkUiEaDRKNBolHA6j6zpKKaSURCIRANLpNMvLy2xubuK6 LpFIhPHxcWKxGLqu126sPpjhcBjXdUmlUiwvL7OxsYGmaYyPj5NMJgkEAmiahhACKSW6rhMIBBBC kE6nWVhYIJvNEgqFSCaThEIhHMfBcbbHUwhBIpFgcnKS9fV1zp8/j6ZpKhQKWYZhVHRdr5imWTEM oxwIBAr79+/PBoPB9TNnzlz8rd/6rc95YnHPiaGU0s+cOZOwbfv+Uqn0vvX19feura3dsrS0ZBYK BREMBkkmkyQSCX9gdF3HMAx0XUfXdWzbJp1Os7S0xObmJsFgkEQiQTQaJRQKEQgEfCIABINBDMMg nU6zuLjI+vo6hmEQi8UYGxsjGo36RFZK+aIrEAiwubnJtWvXWF1dRQhBMplkamqKYDDoD7zHaaZp EgwGsW2ba9eusbCwAMD4+DhjY2MIIbBtG8dx/DbGx8dJpVJcvXoVy7LOfu5znzvi9VtnD3Dq1Kmo lPLmYrH40He+851HM5nMg6urq9Nra2u4rus/ZYcOHcI0TV88mKbpy/hSqcTKygrLy8sUCgWCwSBj Y2PMzs764scjmmEYaJqGUopcLsfly5fJZDLouk4sFuPgwYO+2DIMAyEEruv67VmWxcLCAktLS1iW RSgUYv/+/T7nQE0fKaUwTZNoNIqmaWxsbHD27FkymQzBYJDZ2VnGxsZ8QgOEQiHi8TiWZTE/P8+5 c+eoVqsAGIax0ThuIyPGK6+8MmMYxlu3trbeffny5Xek0+nbV1dXI/l8HikloVCIubk5/wZd18Vx HAzDIBQKoes6uVyOS5cukUqlKBaLhEIhJicnOXjwoE80IQS6rhMKhXyRkclkWFpaIp1OI4RgbGyM gwcP+pzmEczTH8FgEKUU6+vrnD59mnw+73NbLBZr0jeeSPIGtVqtMj8/z+LiIpVKhWg0ysGDB4nH 477eUUoRiUSIx+Pk83lee+011tbWsCxr57CNhhhKKXHp0qVbLct6fGNj4/GzZ8/et7q6OplKpXTL sggEAkSjUQ4fPuxbP67rYts2Qgji8TimaZJKpThz5gzpdJpisUg8HmdqaoqxsTEMw8BxHJ8I3kBp mubL5rW1NV9m33zzzb68b1T0nv4wTZNMJsOpU6dYX19H0zSSySQ33XQTgUDA/xNC4DgOlmURi8WI RqNkMhlOnjxJOp1GSkkymWT//v2+nvIeLs/ay2Qy/OQnP2FjY8Pn/J2QUmYGIsaJEyeMcDg8KYS4 f2tr67HnnnvuseXl5cMrKytiY2PDNz1nZmYYHx/3B822bZRSCCF89l5dXeX8+fOsr68DkEgkOHDg AOPj401Po/eEeZyTTqe5ePEiq6urKKVIJBIcOnSIUCgE1OS5YRiYpomu6z5XVCoVLl26xOLiol/n 3NwcwWDQJ4D3VNu27VtYmqYxPz/PCy+8QLlcJhqNcsMNN/iK3COaUopYLEYoFGJpaYmXXnqJfD7v 96kV6sd7J8apU6eigUDg9lwu90ubm5vvnp+ff8vKysr0+vq6L8cTiQS33XYb8XicQCCAUsq3NDyl Zds2KysrnD592rdmJiYmuPXWW4nFYkgpcV0X13X9ayKRCEopNjY2uHLlCqurq9i2TSKRYG5uzre0 dF33B98TYYFAgEqlQiqV8q0hz7fwuMtT+kopLMvCsizC4TDJZJJ8Ps+ZM2dYWlryxd7c3ByxWMzn VsuyME2TiYkJXNdlcXGRq1evUiqVen2+oZuY+uxn/yhy4crLbx6fSLzvm899+4nZmembNU1GioUi tm3hOA4HDhzwrRjANz01TSMcDmMYBrlcjsXFRVZXVykUCgQCAcbGxjh8+DChUAhN03bJc890zWQy XL58mbW1NWzbbhrIUChEMBj0rSdP73jH0uk0p06dIpOpPXSxWMwXlZ5F5A2mbduEQiFmZmZQSrG4 uMjJkyd94s3NzTE+Pu6LIY/Lo9EoyWSSYrHI2bNnWVlZ8ZVyI7pwBUKIJmL4pX/lV357ZiW9+K8E 2nsUTEpNM6WUaFKiaRJN0zFNg1Cwpgvi8RjJeJx4PE4sGiEWjTA7OwvA2toquVyOzc1NwuEw4XDY H8RAIICU0ieiR7xsNsvi4qJv0QQCAZLJJOPj40SjUd/jtizLNxU9+exxz/LyMqVSibGxMaanp33L xht8j3OCwSChUIhwOEwul+PChQtNXDA5OYlpmr7p6xHb830ymYwvZtvpg16IEQwGn/rsZz/7p95x nzOq1UIynUr9muuquH+RFEip1T1fzX+apbZ9TEiBFAIhJEIKDF0nGAwQqxMsHouRiMeJRMKEQyGC wQChUIiJiXGS8QSZTJorV66QzWaRUjI9Pc34+HiTTHZdl2q1immaJBIJIpEIxWKRhYUFfvSjH1Eo FIjFYj4BDMMAms1Rj3MancATJ06QyWQIh8NMT0/7hoZnAnuc63Hy+vo6J06cIJvN9jzgnY63VeCx WKAEwlW4vo2M47G0x0LC++fVjBQSUY/91P6aiSalhqwT1Suj6xoB0yQUChGLRTl0y0He+uY3ccO+ WRzHwbZtfyA98RAIBAgGg2xtbfGzn/2MhYUFXNf1LTaPezRNA/Cfas+qikajlEolzp07x7Vr17As i2g0yqFDh3zLziO+x0GJRAKAhYUFrl69Si6X62nAe4Wmaa11RnR2tsTLrzoAynURKBDS9zhV7Q5B gQvUDri4eGEBQa1vuwkmPM6pm5weoXRN5wPvf4y77zxKwDTY3Nz0LRPPA995w7qu88Y3vpG7777b H3Dbtn0l7BFR0zQ/5rSwsMCrr77qO4KTk5NEo9Emj9+Le8XjcaLRKJVKhQsXLjA/P0+lUhlq0Nsh EAi05ow3HTlS+t5ff9sBh2q5yLvf9QhCCLa2ttjc2iK3laNYKmE7Dq6rcJWLUjVRViOWqBGLBoIB NMpUIRDUn6j6d5RLKrXG2toq1WqVSqVCPB6vF98O8DV6242/PUvKC3E0KEeklCilmJqa8kWbN+ie 3vJ0UDgcJhaLkc/nOXHiBCsrKwPpg3bnWh2LRCKtOeMTn/hE+d/98Z/YjlNj8aNHj5LP59ENAwFc vHiR6elppJRUq1Ucx/Gtl0KhwFYuRyFfoFypUK1WqVoWlm0jqHFFA6vgOA7SEycNN+yZxY2D4DiO b6n44rMDGmNHskl8ym2ubPju/d7a2uLUqVOkUim/nX71QT+QUjrJZHKz8VijaevomixX66NmWRZn z57FMAweeughLl++7Fsrnmx97LHHOH78OI888ggLCwvMzMzw2muvoWkas7OzpNNplFIUikW2NjfJ 5fNcm1+gVLZRrkKI3ga4V3hiC2iKqjaeb3edZVlsbW3tOt7vwPdaXgiRX1paaoqPyIaTSghZ8OR9 LpfjgQce4Pbbb6eQzzMzPe1HSb1406VLl6hWq0gpefnll/nxj3/MyZMnuXLlCqdOneKnP/1pzVmr 2/SPvvvdzM5MAwqlXBSKneMzKHGGIWo7sTIKDuiA7PLyclOndzp9+ZoOFnzpS19i/5hkciyGER4j FE0SiY0xPTXF4UOHCEciJBIJwuEwjmNz//33s7a2xk033US5XGZlZYV4PE4ikeDixYtUKpWaaJCa 35hSoFRruSyE6GuA+y3fiEaO8uoaBP1cJ4TIHD16tD0xhCTf+Pvv3bXKXPAaFV2StySFqiS/orFx WeNaWSfvhrG0OOf0KBhxtGCS8fExYrE4kXAYBQgBhw8fZnl5ue5pN3S4PgitBrHVsWEGvBP6HMSR 1C+EyJ4+fboTZ4hC/RMAZQnGJkGGXRzl4jhgOzUDSbngqiy2s0TVllQcQcWV5IuSjYzGZsUg74Yo umGKbogyMS5cuMDq6mqt7nqL/YztMITodm03pd0NAzh/2WeeeUYdO3bMP9bMGULmvQqUUlgKDB00 E1Rdl/j3VLNgfVHjOuC4Do5Dw98mjguuqpmy//NUkmw2jNADuwZhFOjEOd6gdFLivdQ/Qmzs8qGa G6uLqXqZiiP9QQd8seN9NlXVWFPDfSkFylUoR3FD3G4+IdrrjFboJqZ6ffp/TrBB00jtIIYUosYZ NR6g4gpct8HkqkOIndXsLNDwIUDK2p+hecdV3UlsL6b2Sj+0QzczdpBznY7vjNjCTp0h6zqj/vhX HLF70MX2IHtQ7OCSxuLe9QK0BsfPv7YPsfF6Pdl7YeLuDBLCLjElcrUYk0C5iqorm55cQetBb3fc P0mNu3TZPJg1nXN9dEan8+0sul7bHOS4pmmdiaEJPd/IChVHoNy64q4f28kFPpfUT3QiluYRo6HC fqyYToM9rNM36oeiG6SUu8RUkzrw/Ayvsqpbr1Q1fdQL13WHaPi9/RVRr1x63wVoja3Vb95T4MMO Rh9hiKHa6aWubhYd7A6fww7O0A2j0Ph7p85oanoHi+ygyy7iSAV6Cz3jNnq+tTtpeSOjQr+c1Yl4 7ZR+N4JLKR3LsrZ2Hm8mhm7k6rUBULGlN4Wx3VCrDjd1ZJsrvD4JQGO3zoAdg+DNm7BbdPTqB1wv JT8khxWFELsyF5qIEdD0AnimbY0z3EYnr41OqF1TG3zZ8ClFg74Q28RorKPXJ3VYH6Mb2oVf+kUv Cl0IUdB1fVcGQzMxYsFmp8+uKfC2tmsLUQU1Qmhi+09S44yA1nitqnGCO5onedhA4XVGXim1K72w iRhTiUSTAi/bAscFYddL1h9zj8hNtBDbvz2FbcranybAAIJac3nobyD2UgyNot4+zNx8KBTqzBn7 9u0riLqJpJRiLa+TlQbBskPIVJhabUKIWpQEV2x/iobvVt101YCArP0FJYQMtolZp5yr3J4HuVuZ UYqyQb3xHsvmpZSdOcOyxotSai44UgHZss6/eOFWguSJ6iVmYy4H4hb7YzYzEYfpsMNEyCGqK9/T tl2wqc2Bl2woWzXuiGoQkMrXR/WuNjlce+1rdAsW7izXz7k+CZSLRCKdiXH16guWpmsVYRHyooHB xCwb6RTX0i6vrdUaDASCjI0nKWZXCOk24yGXG+I2B+IWB+I2NyVsDsYdZsIOSgfbBiyIhFTd2uoe Dml3w4N42I3YC6exXwghtp588sldEdImYqRSKVdAEUQI4J77H2Bm337S66tcPHOKjUy6dpFpogei 5Ks6WyVYzcFrayaJsXHGxxKklq5StRzGQi53zTm85X7FHXcoTvxIYGoCp8Hk9RS4YPio6ygU+F5Z UI3HhBAZWoRadwZkEUIWPPkfiycoFPJYtsPBW2sLbCKxOO/9wG9wx113c/9bHiQRj/rXBgKB2qyT kFiOIF3U+NlCkMlDT3Pve/+Ce97xm0QCAn+yT2w7farDTXW76VGgF9E1iIPXpq5d3jfsIMbU1JQS goInRaKxOFJKDt92hFg8CYBhmMTiCcrlMkpIxiZnAEiOTXDPmx7kjnvezOPvf4Jbbx5HAbarceXq VZ7/1l/z/W+/sutxuJ4xoWF0xSj60pDT1ZIYu7LQFRQ8u/N7z/0fzECQffvn2NqsXW9VK1w8d4ZK uVxbm0DNeTBMk4mpGdZTq+hGjMMHpzh/JcMNNx7kbe98nOmkRmziBp5/5U/Irxe322uTKDaoGdtt tu/nYYKpJ2Lceeed6vSZC3mPpoV8jkI+x0Y65Zcp5HP89EffJ55IohkG6bXV+vEtzp85RblcYnJy CjfvoBQsrqT5T3/2TaxKiWg0SraocJXyOcTtQ6GOwvwd9XWt0AOH7QqfQwvOEPUJpk4VlooFSsWm mCLFQoHXfvZTwtEoG+trpFeXAdB0nZm5w2iaTqVSRmo/bYoFNinPLnfQix8xiOl7PcRXg4hyhBCb rco0EePYsWPqrnve2JQh0g+KhTzFQlO2D+nUGs9+8b8RCocxzABb2QyIxtypZjHV6HP0+7SOwvTt t71eju2ApWlavtWJ3ZwhZH3qtbfOeAnEXvZ3K5RLRcqlbT0h9UZiNEdtWx4fIQZJjhtFmw2oKKWK rcrtJIainiEi23RC0zQOHz7M5uYmDzzwAOPj47iuSyG3xelTr3D2wiVsu+VuDC3htgkUthu0YTzw dvMPgxoKA6JsGEZPxED6SQnbnfcGQErJAw88QCKRwDQNXn31VQqFArquMz09zd333sf84hLhcISN jSyVFuvcmm6I/pPHruf06qDz213q7JkzEPV0HVlf/NKgeJiuJz8LITh9+jUuXrzo39z8/Dwvv/wy MzPT3HzzLUTCIb713N90XOOgxGAzb4MmqrXtxx6auy0IVEkkEr0RQxNaPYy+u3AwGEAom2uXr3Du 3DmSyST33XcfmqZRyOc4ffo02ewmW1tbhMNh7rn7bk6+9FKLZv35vJEP3CAD28/EUr+hjxbHSkeO HGm5PnlXOETTZdOchgfTNLn9tttZWbzG6TPnicViRKIR7rrrLs6fP8+hw7fyu7/7O2i65OKli1y9 dpUb9t9QW+7bhnsF+JzT6yC+niGTUUAIkXvTm960K2ILbWJT9Yuaju/fv5+Z2Vkcx0VIya/+6q8S iUR49tlnKZfLXLhwgR+98CLFQhFLVbCsKmcvnKFcKdcr3tFO/bPfRIBhOaMXBd4vQfsp3ypFxz+3 84Aht8VUYxOmaXLt6hXG4yESiQTJZJI7jhxhfn6e5eVlnn/+eV46+RJve/vbee/feRPXFq+gBST3 veUNCElzWg8NeVgNRkLjTe1VRuGgDl67QGE/9dSP9U4MXde3M9EbwnrFYhFN1xFSR0rJs88+y+nT r6FUbXlwqVzCdV1mZ2Y5ctsBwpOK2P5lnvp0ismbXYQGmpS7OMSbXBpVFvggYqqXLJRBUnnalE+3 O7ebMwwzD6J5UQuwubnJ3NyNoAfI5/O8612PcPSOI7XpVqXQpIbrunz961/n6tlVQnFBQKR589GT HL67wl0P3so7Hn2QiYnxpnrbWVvDyP5Br+309I+IK9oGCaGFNeW6laKU0hWukF7aPtTW+H3lK39B qVTGsixWlpbIri8hhfSXApumyXve8x5mZuOUNn7I2a0kx08+wqn/ewK3usBYokC5XNMh3XTGXi6M aVe+Wyb6KNAXMUpKVaQUFkoGGrM/lFJsbW3vEPDt48eRUvL4E0+glGJpeYlsNsvFixcheCPFtIsb 1PjusxHKG2A7ZYrZ5doNa4Zfcb8DMGzSQS9+yjDHu3CFArK7CtSx2wOvVi0hRFVBQOyeGfThOC7R aIyZmRlefPFFZmdnefihh/izL3yBc+fP4ViKyf1T/Pj7Jzh0282EokECZpCLZy6zvr7hR253Dsww gcLGG++H465jOMTVNK0tMXbpDKWUjRTlds6BZxRJAVtbm3zpi19ka3OTbHaD7x7/LsVCAata2+fv 0Xc/SiwW5dzpi5z52QUunrlCLudtilXvXYOfMQoRMUh6f7dI76iglHKAXTm2HnbP9KmIjdoqOwoc V2Fogmg8iVMtUCxVEQIMCaYGlguVSpmFhQXm6ztXerj33nu5du0ai4tLKKUoFUuUijXHU/jLj7dT da6Xs9YpANkK/fStm9gSQjjBYLDlXAa0IEYsJux0VpQRAl2CEJKxsQRB4MLV2v4JN00o/vAzWeYX dP74T2NkiruTx3/yk5/4uxTccfQI5Uq5tiVqKk2pvB1A7De52bvBUcenOrU16LGdUEq57SaWoIWY chzHQVB2laBqK26/8w389oc+zD/5w3dyw3jN89gq6wRiSX7z7+bYl2wOlwfDYQLBkL+/iFKKhfkF SoUy8WicWCy2s4P+Zzenr9dz/V47Kh+ih+udubm53jlj37599vzyahlqkxuBQIiLlxcYiwSJxySL GUVo+hb++qW7ybKIGT8JSxVfw8xOT+IInfnLl/w6c7k8E2aay+cXKVa1bTEl2s+BD/qE73XSwTAE klJac3NzLWf5oAVnJJNJG0TZa/Lca6+QWl3iz//3a5ybBxQkx6cwQrfw8sVfYjGX9JcNzO6f432/ 8bu8+32/TCTmb+iGAG6ZFRwY2z2/4WWHjGrSp11dnTAs8XptT0qZP3r0aMsgIbTgjImJCUfUF3JI KSjkc3z3m3/ZVOb0z05imCZKQWql5t0rIDk+wfLyKmMTE0xMz1DI1QyHw3fcxR3v/xXMi2c5/5Wv eLdQu24P5qxHwRnDJKx18Es2RIe3CezijGeeecaRUpZAEAxF0A0DXdPQpJcJqCiXirzwg+O8+Pxx HHt7of2Vi+dx7Cpb2Q0yqTX/+P65g6wup4iEkoQjdZ2hVG0Bzoht/FFEbfcC9VBLW+8bWs/0qXve +OZcSdR0huvW6CWkhqCeba5A4YLr1rI76sq3VCzwl3/+RYQU2A1bTV88d5rE2DhWtUq1Umpoa/g8 11Y3vRdiZxQxK6VUf8QAuPPIrf9wJZX67+l09pl0ev1ttcChBkKgahPXCCRomu8ASupLnJSLa7tN 1tH8lUssL87jOi6u6yCktwZ2b57Kfgmy17qpIRzSMnnNQ0tibGxsRNKZzT/MZDIPlopFKuUyrnLR pIZu6BiGiWGY9d05Ja5SuA0ST+oCHYXrOLiqlhtVrVre8DflX7ezpnq5wb0IMu6BOduIQYiRny4W Cm/VDUPE4kmiMRfHtuu7ZFaplMsU83kU1Db08rbANsz6ltaiphKEVp+k0jyZhFJuzbT10nkGDOyN kqP6zVAZFH3rDAAZ1KqqtpKvtjsnta1PjUCAoAqDAtetbR7v2LUtTCvlMoVCHuW6tb1rjfpmvab3 ohFv6rC277msr9DfyRntAofdjg2KQSe1BrGsWm1R0YiWxNANoyKorcZ0bKv+3gm5XbkATepoug6B AEGlfIWtlMJp4KBCPudvOF8jUACjvhmwamFN9TObNyqzd1SxsW51DKTAzWCwAlhKKarlEgHTqIt5 AaK+NaqsTWyL+sYvhmHWNy4SEAgQUhGUcv33Sti2hW1ZlMslCvktlAKpSSbH46260HSD/XLHqJLV 2p0b1N/Qdb1/zjCDwaoStPAUlS/3vV2ElUegurWFqFtKohZk1ISnUwL13ToVyiOQZaHrps85rQZx 1NbWzjZGFTXu5fqdr/XZiZbESE5OVgTCqs1ddGukTiDH9X7V+KO+RzpC1upo4CI8pR8I4ChFtVr1 X0jyemDY+e1ey4VCoY7E2OWBA9x2770VFJbvVwAoG4GDFPUVqx36JFCgHJRj4VplnGoJp1rGtaoo x961y3O75Oe9QD+cN2z4oxFSSrtarbaN2EIbYjx0221VqYmmqJ5t2XUz1UXiIJSDaUjGxxIEA0Zb AvkZF8pFuTauXcW1yii3ZtrWVruO1gvvhFEOcLtyrY5JKYuVSqVjJnhLMfXwww87mqaVwfKFlBD1 Hf4bUnjm5ua49777mJ4a4/LlK7x66jTBQJCl5VWKpRKC7Y1gds1VuC5IDY8UowyVXw/vewBsViqV jmslWhJDCKHubLWCSTXmiwgWFq5xbf4aj7/3UV5++RXm5g5w5Pbb2NzapFKu8pff+Ct0TfN1SG2a lSYWUjX7tuN06PUm1DBxqHacIoTI6rrecavSlmIKQMCuSZDt7D98y0gKsG2bfftmecMbjrKwuMiB /Tdw5uw5TO8tlJpE4IKykXJnf1STv3Edn9S+2hvGxK0jGwgEOhKj09vItnfypCZK9h+Yo1KpUCjk mZyYZHV1Bcex+dZz30ZKSSqVolQqUyyWWFpeRsrtNR6yPrvnKmjU17XBaK8zRh1/2uuZwFbt1bE5 NjY2GDGkFH5OjVIKXdd572NP1N+16lAsFrl69TK5rU1uvvkQ58+f5fz5c2hS44UXXgRA07Z3/6yH plpscdQUN2x5M4POefRyXS8RgFEofSFE9oMf/OCAnOFtxe3/FJw7e5bMxgZHjhyhWCwSiyW45ZbD LC4s4LiKt771QQ7M3cj3v3ec6ekZrly+hFKKSrWCU3+t2k6TS9GFGrQe2GEzC4dFv/pDSpnpNMsH HcVUw4bDKEDjB88/D8rl3Nkz7Nu3j1g8Ti63xc0330JmI0N6fR1XKe56wz3ceONNhMIRxsfGiUQj pNfXyWTSnDt3FrtY9lvZKaZGgX6INMjM4CCoSmQAAAAK2UlEQVTlusWloAMxNE00rGDylCuApGo5 XL22UKeT4qWXTqJcl6mpScbUBI5tk81mcR2X6elpTpx4kXvvu4+5G2/i1KlT9d7hf3pfR+VjdMur GjY1ZxCrqtMiGQ8dxNT2Tgk79xFsjpoKCoXaVOrVa4tcu7aAYej+W75yuU3SmQxbW7n6W78kjVEv 1bAh+l7nx/Za916g21wGdCCGXtMZSuwU8s0N+N990xRB1XLAcoAqp187gwCe/cpfMDMzjVVtjj8q pQbijFFZRZ0mlgY91sYD7xixhc6cURRCKIQQvTpKrZ/guhgSktW19Yby9bKKrrN9e43rEYJptcPz TrR1+kAUAdVPP704VJ8ZEz3V28/xUWAYTmlRxrFtu232uYe2xNANoyjArcdg+0avA9WLLbUXjt+w dfRSd8OxsmqzK0Ij2hLDlLKovCmkAfvbE5d0SVQeFMMkuu1BPwpA13eStiVGMBgqAm7HiYv+OtQS qgdK76WY6nWtxpCxqYJhGJ03UqEDMTTNKCFwRyWV23LJHinw6x1whI5WVSEYDA5OjLGxcFEoFEL0 9PT2ip1E8aLAvVy3FxgkO6VVuS7XFSKRyOBi6tZbb62JqVpTvfRvIKiG/zuWG2BOY1QYQbJCfmpq qu1SAA9tiXH06NGSkNLZDqDvEXrkjP6rHSzSO0i5bvpE07StRx55pOuOaB38DKq6ptVWVO4lLeiu xPdyYK+HJVcPhXRtqC0xPvjBD7pCyiLgZ4jshdxWOyY09tob7hcj6k/XUAh0mnYVQglBQQCo7U71 62H3gm6cMUh7vT7xw8amehRZXUMh0FlMAbJQ8zPaTrL30kZnKDWUaTuMD3K9Viz1Ej6HLsTwkxI6 JawNSZBh3YzruX5v0HsVXRbJeOjMGZJCx/PbjfVSrC32Ir92FJmC3dCjyHKBjpmEHjoSQyJa7lfY rmP93aQXQld7Yk31Ul+7hOdBuKIdYYQQluu6uVbX7ERnMeVzRu8TOX2H0BndPHRP7Y2IC/voU1VK 2ZOE6UwMIeovUey13aZrey476IAMIooaH5ZhEtj6KF/RNG14Ysh6hkj3ZQHtO7WXVs2gHNNpufMg yQZdUDV2vKa1HTorcI+iYji53Y0or0eEdZRtd7o3IUQlFosNTwytvhX3oJyxE+06/XoRY5Bswn45 SQhRvvPOO1vu8LwTnXJtkfWswprSGM2A+fK64diod0l4PdGCWLlOm7c0oos1JX2nby+f3Z1r7EZd 506M0qHrhLr3nRVd0jo9dCSGYZrbsk6NNi7VWMteZIh06muvyw/6FVFt2ku1OtgKHYmh69uZ6E1v Nx51oNDtLqZGtea7V4xgQgkhxEYoFPo3vV7TkRhhM7qtwHfc1yi5ZBjO2GtrqNcyLbjEMk3zDz72 sY+1emdFS3QkRigZbnj/0nDJwp0wyDqKXtrv1rdhEqC7ldF1/T+bpvk/ulbUeE2nk0YwWBBCKIEQ ncarX492J3rZWUeI/ndK6CUTfRQP0846dF3/YSwW+2cf+chHerKiPHTkDCeXK+ua5vSaITKo6LqO 6+q6tjeIr9EIKeViMBj82Ec+8pGeFbd/baeTS5WKJaSsdJBSLdHvAPU7KzdsP4bl5HZtCCGqwWDw Dz7+8Y+/OkhdHYlRXlS1zSMHePr64ZJhnL5RWlnDcoWu6/8xlUr9r7475F3f6aRprqtiRRRFj2Kq FdrJ+kY0ziuMcs13t2tGGYbRdf27k5OTz3zmM5/puIiyEzpyRjKZdBH1OY0h+t3+afKcL7dJoe61 Fz6ojmr3W0p5JR6P//7v/d7vdU3774SOxIhGowpEocYZw6GT2LqeG7k0YkQ6oxAMBv/gox/96Nlh 6+pIjEuXxpQQdF1X0A9aEeU6TfL0VbbXa03T/LdTU1Nf7atjbdCRGLfcsuEKmpMSRuZ5i22vvte0 /H6f5FHqhFbEMgzjW7Ozs//8ySef7P1lth3QkRh33nmnAo8zmokwdOym4fterWbt1bQdJNlASnkh kUh87Mknn+ya6t8rOhLjmWeeUUK2fyX1XsSmhglvdKq3H/TQTi4QCPyjD3/4w1cGaqANuiUkKOm9 H7zda39GILZ6nc/Yi2UBA/TdNU3zjz75yU/+Vb8XdkOX9E6gPvXavdjgRHFbhNCHUb7DoJsiN03z q29/+9v/pRBiYH+iHboSQzZkFQ4T6u6EVg5lP1xwvRS7lPJ8IpH4RLsX5Q6LrsQQPXLGjmv6IsrO 7TC8OvYK7RzLLvpqMxQK/f6HP/zhlb3qV1diaELvmxgeBolNXQ8M4NfYwWDw86lU6rt71im6xKYA hFHLRNfNAJVqGUPX6ttx9zbQvcamGssPU1e7elvV00u7Qgil6/oXZmZm/sOnP/3pkeuJRnQlRtAM ZYUQjqZpmhaK1Dakd12U626/O0N2ZrBuQUClmve5HVWwcJhQeYMh8crExMRnR+lPtENXYiSjoe/Y M7OPFQu591ct632VSvU2JWsLy5RS/l7nAhdZc4ba1rV7cBTsSKoeebLDcEkJuUgk8g8+9KEPrXUv PTy6EuNrX/taEfgb4NtPPPHEP7bgaHo984FyufqBSrVyhxIijKbViOO6WI4NykVKgWyjyHc+yYO+ 0MTDsHMabcxZ2zTNz3784x9/cajO9YGuxGiA+sY3vlEBTgInjx079i9++MOf3JFKp95brpTfZ1n2 3Tb2lCZNnzCO69TepyHYRZjGSPAo56N3YsCQvNJ1/b/s27fvv7K3+XtN0LoXaY3jx4+rixfPpZaX Fn70yU88/YVsJvfnhqGdFEJYAjHmKjcqpCakpoPQcBU1XaMUUN9UWNRe+zB3YD833TgHQLlcxqy/ X2MnhsnmaFWXbdstuULTtB+Hw+GPPfXUU0PNT/SLkT+KDx87pnP8+KRluW/Jlwq/VqlYj1eq1Vm/ gKq98sF1HDTdACF48K1v5p3veDtSSrLZLOFwGMMw+gog9htGcRyHUml3PrKUciMWiz3y9NNPv9zL /Y4SA3NGO1w5fty9cuVKfn7+6tnV5eWvfuqTH//32a38N5VyNlAiqVAREIbU6u/bAA7sv4GDN92I EIJSqeRzxihiXp2S3+yGdwxCLfEsHA7//U984hN/M1TDA6IfnTEQjh07VgV+APzgqaee+qfnryzc W8zlHy1VSk84tn2X7TjxvbKm+oxvKcMw/mR6enrghIJh8brl3z/11FPBc1euHK6WKg/de8/d73rr m9/0oFJq3+bmJqFQCMMwgP7W4PUj1mzbplze3l9X07TvBYPBX//Upz6VHuR+RoGfi8UQX/7ylzUg UalU7s/lcm8IBAKPOo7ziFLKUErtEqWj0CWNxJBSLkWj0ccGzXcaFX4uiNEKX/3qV2e2trbusyzr La7rPuA4zh3ApFIqopRq2e9+CGJZFpVKBSFEORwO/86nPvWpr7S49Lri55YYO/H1r399LJVKHQbe 4DjOA67r3uO67iFgXCnVcbvXTsQwTfNff+Yzn/mc6HFBy17ibw0xduJrX/taeHV1dUrTtNtc132z bduHhRBzSqk5pdQB13UjjeV3EsSyLFzXfS6RSPz6Rz/60Z4Wze81/tYSYyeUUuLzn/+8Nj4+rs3O zhrAVKlUOlitVg8qpW4BbnJd92bgRsdx4o7jrJmm+ctPP/30+de56z7+vyFGrzh27Ji8//77x0ul kv7kk0/u2UTRL/AL/AKjwv8D878gUVhPHWAAAAAASUVORK5CYII="
            id="image155681"
            x={308.8056}
            y={31.417492}
          />
          <text
            transform="scale(.56363 1.77421)"
            id="text12036"
            y={42.791161}
            x={548.51746}
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="20.4175px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.05464}
          >
            <tspan
              id="tspan12032"
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
              }}
              y={42.791161}
              x={548.51746}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="33.7354px"
              fontFamily="Franklin Gothic Medium"
              fill="#0f0"
              fillOpacity={0}
              strokeWidth={1.05464}
            >
              {
                <a href={url} target="_blank" style={{ fill: 'white' }}>
                  {'VAR'}{' '}
                </a>
              }
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
