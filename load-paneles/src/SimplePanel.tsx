import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  const styles = getStyles();
  console.log(data)
  let capacidad=[]
  let carga=[]
  let breaker=[]
  let rack=[]
  let load=[]
  let color=[]
  for(let i=1;i<=42;i++){
    capacidad[i]=validar(leerData("datos","CAPACIDAD",(i-1)))
    breaker[i]=validar(leerData("datos","breaker",(i-1)))
    rack[i]=leerData("datos","RACK",(i-1))
    carga[i]=validar(data.series.find(({ name }) => name === 'Average DATA.CURR_1.NOM.CH'+i+'.VALUE')?.fields[1].state?.calcs?.lastNotNull);
    load[i]=validar((carga[i]/capacidad[i])*100)
    if(load[i]>80 && load[i]<=100){
      color[i]="#BF0707"
    }
    if(load[i]>60 && load[i]<=80){
      color[i]="#f64043ff"
    }
    if(load[i]>40 && load[i]<=60){
      color[i]="#faa657ff"
    }
    if(load[i]>20 && load[i]<=40){
      color[i]="#fafe7eff"
    }
    if(load[i]>=0 && load[i]<=20){
      color[i]="#63be7bff"
    }
  }

  // Objeto para almacenar los grupos
let grupos:any = {};

// Iterar sobre los datos y agruparlos por rack
for (let i = 0; i < rack.length; i++) {
  let rack2 = rack[(i+1)];
  let breaker2 = breaker[(i+1)];
  console.log(rack2,breaker2)
  if (!grupos[rack2]) {
    grupos[rack2] = []; // Crear una nueva matriz si no existe
  }

  grupos[rack2].push(breaker2); // Agregar el breaker al grupo correspondiente
}
console.log(grupos)
// Mostrar los grupos resultantes
let agrupacion=[]
for (let rack2 in grupos) {
  if (grupos.hasOwnProperty(rack2)) {
    agrupacion.push([rack2,grupos[rack2]]);
  }
}
//console.log(agrupacion)
let labelRack=[]
let canales=[]
for (let j=1;j<=agrupacion.length;j++) {
  labelRack[j]=agrupacion[(j-1)][0]
  canales[j]="CH:"+agrupacion[(j-1)][1].toString()
}
//console.log(labelRack,canales)
  //FUNCIONES
  //LECTURA DE DATOS
  function leerData(equipo : any,dato:any, i:any){
    let variable= data.series.find(({ refId }) => refId === equipo)?.fields.find(({ name }) => name === dato)?.values?.buffer
    if(variable===null || variable===undefined){
      return 0
    }
    return variable[i]
  }
  // validar
  function validar (dato: any){
    if(dato===null||dato===undefined||dato===0){
      dato=0
    }else{
      dato=parseFloat(dato).toFixed(1)
    }
    return parseFloat(dato)
  }
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
     <svg
      width={"100%"}
      height={"100%"}
      viewBox="0 0 436.56249 211.66667"
      id="svg67790"
      xmlSpace="preserve"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      xmlns="http://www.w3.org/2000/svg"
      //{...props}
    >
      <defs id="defs67784">
        <clipPath id="clipPath3307-2-7-3-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-20" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-25"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-71" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-2-9-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-5-6-33"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68418" clipPathUnits="userSpaceOnUse">
          <path
            id="path68416"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68422" clipPathUnits="userSpaceOnUse">
          <path
            id="path68420"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68426" clipPathUnits="userSpaceOnUse">
          <path
            id="path68424"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68430" clipPathUnits="userSpaceOnUse">
          <path
            id="path68428"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68434" clipPathUnits="userSpaceOnUse">
          <path
            id="path68432"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68438" clipPathUnits="userSpaceOnUse">
          <path
            id="path68436"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-45" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-31" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-04"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-27"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-10" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-54" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-52"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68514" clipPathUnits="userSpaceOnUse">
          <path
            id="path68512"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68518" clipPathUnits="userSpaceOnUse">
          <path
            id="path68516"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68522" clipPathUnits="userSpaceOnUse">
          <path
            id="path68520"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68526" clipPathUnits="userSpaceOnUse">
          <path
            id="path68524"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68530" clipPathUnits="userSpaceOnUse">
          <path
            id="path68528"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68534" clipPathUnits="userSpaceOnUse">
          <path
            id="path68532"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-6-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-44-20-6"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-25-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-2-6-5-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-5-7-71-9"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-2-9-4-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-2-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-5-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68578">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68576"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68582">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68580"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68586">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68584"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68590">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68588"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-2-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-04-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-27-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-10-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-8-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-8-1-7-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-1-2-54-9"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-0-5-52-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-2-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-5-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68652">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68650"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68656">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68654"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-68" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-80"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-65" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-5-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-1-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-0-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68700" clipPathUnits="userSpaceOnUse">
          <path
            id="path68698"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68704" clipPathUnits="userSpaceOnUse">
          <path
            id="path68702"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68708" clipPathUnits="userSpaceOnUse">
          <path
            id="path68706"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68712" clipPathUnits="userSpaceOnUse">
          <path
            id="path68710"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-49" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-69"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-88" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-13"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-04"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-24" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-71"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-48-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-10-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-7-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-1-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68756" clipPathUnits="userSpaceOnUse">
          <path
            id="path68754"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68760" clipPathUnits="userSpaceOnUse">
          <path
            id="path68758"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68-7">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-80-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7-6">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-4-8"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65-3">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-2-6-9-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-5-7-0-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-2-9-7-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-1-7-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-0-7-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-6-7-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68804">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68802"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68808">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68806"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68812">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68810"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68816">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68814"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-69-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88-7">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-13-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-04-8"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-24-5"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-71-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-10-9-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-7-1-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-1-7-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68860">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68858"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68864">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path68862"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-7-70-5-1-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-7-5-04-8-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-24-5-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-71-6-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-0-9-48-3-9-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-04-9-10-9-9-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-7-1-4-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-1-7-0-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-48"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-42" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-90"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-3-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-0-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68912" clipPathUnits="userSpaceOnUse">
          <path
            id="path68910"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68916" clipPathUnits="userSpaceOnUse">
          <path
            id="path68914"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68934" clipPathUnits="userSpaceOnUse">
          <path
            id="path68932"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68938" clipPathUnits="userSpaceOnUse">
          <path
            id="path68936"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-47"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-32" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-18" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-07"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-3-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-0-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68982" clipPathUnits="userSpaceOnUse">
          <path
            id="path68980"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath68986" clipPathUnits="userSpaceOnUse">
          <path
            id="path68984"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-48-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-44-42-6"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-90-8"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-7-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-3-2-3"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-1-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69012">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path69010"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69016">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path69014"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69034">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path69032"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69038">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path69036"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-0-9-9-8-5-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-04-9-6-7-7-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-3-2-3-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-0-1-6-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69060" clipPathUnits="userSpaceOnUse">
          <path
            id="path69058"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69064" clipPathUnits="userSpaceOnUse">
          <path
            id="path69062"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69082" clipPathUnits="userSpaceOnUse">
          <path
            id="path69080"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69086" clipPathUnits="userSpaceOnUse">
          <path
            id="path69084"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69090" clipPathUnits="userSpaceOnUse">
          <path
            id="path69088"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69094" clipPathUnits="userSpaceOnUse">
          <path
            id="path69092"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69098" clipPathUnits="userSpaceOnUse">
          <path
            id="path69096"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69102" clipPathUnits="userSpaceOnUse">
          <path
            id="path69100"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-15" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-33"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-93" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-22" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-87"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-83"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-2-9-51" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-5-6-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69164" clipPathUnits="userSpaceOnUse">
          <path
            id="path69162"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69168" clipPathUnits="userSpaceOnUse">
          <path
            id="path69166"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69172" clipPathUnits="userSpaceOnUse">
          <path
            id="path69170"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69176" clipPathUnits="userSpaceOnUse">
          <path
            id="path69174"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69180" clipPathUnits="userSpaceOnUse">
          <path
            id="path69178"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69184" clipPathUnits="userSpaceOnUse">
          <path
            id="path69182"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-65" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-06"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-13" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-71"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-26"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-48-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-10-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-7-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-1-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69264" clipPathUnits="userSpaceOnUse">
          <path
            id="path69262"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69268" clipPathUnits="userSpaceOnUse">
          <path
            id="path69266"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69272" clipPathUnits="userSpaceOnUse">
          <path
            id="path69270"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69276" clipPathUnits="userSpaceOnUse">
          <path
            id="path69274"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69294" clipPathUnits="userSpaceOnUse">
          <path
            id="path69292"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69298" clipPathUnits="userSpaceOnUse">
          <path
            id="path69296"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69316" clipPathUnits="userSpaceOnUse">
          <path
            id="path69314"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69320" clipPathUnits="userSpaceOnUse">
          <path
            id="path69318"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69338" clipPathUnits="userSpaceOnUse">
          <path
            id="path69336"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69342" clipPathUnits="userSpaceOnUse">
          <path
            id="path69340"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69346" clipPathUnits="userSpaceOnUse">
          <path
            id="path69344"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69350" clipPathUnits="userSpaceOnUse">
          <path
            id="path69348"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69354" clipPathUnits="userSpaceOnUse">
          <path
            id="path69352"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69358" clipPathUnits="userSpaceOnUse">
          <path
            id="path69356"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69362" clipPathUnits="userSpaceOnUse">
          <path
            id="path69360"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69366" clipPathUnits="userSpaceOnUse">
          <path
            id="path69364"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69384" clipPathUnits="userSpaceOnUse">
          <path
            id="path69382"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69388" clipPathUnits="userSpaceOnUse">
          <path
            id="path69386"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69406" clipPathUnits="userSpaceOnUse">
          <path
            id="path69404"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69410" clipPathUnits="userSpaceOnUse">
          <path
            id="path69408"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69428" clipPathUnits="userSpaceOnUse">
          <path
            id="path69426"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69432" clipPathUnits="userSpaceOnUse">
          <path
            id="path69430"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69436" clipPathUnits="userSpaceOnUse">
          <path
            id="path69434"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69440" clipPathUnits="userSpaceOnUse">
          <path
            id="path69438"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69444" clipPathUnits="userSpaceOnUse">
          <path
            id="path69442"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69448" clipPathUnits="userSpaceOnUse">
          <path
            id="path69446"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-08" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-39"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-73" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-90"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-90"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-0-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-2-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-9-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-8-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69510" clipPathUnits="userSpaceOnUse">
          <path
            id="path69508"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69514" clipPathUnits="userSpaceOnUse">
          <path
            id="path69512"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69518" clipPathUnits="userSpaceOnUse">
          <path
            id="path69516"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69522" clipPathUnits="userSpaceOnUse">
          <path
            id="path69520"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69526" clipPathUnits="userSpaceOnUse">
          <path
            id="path69524"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69530" clipPathUnits="userSpaceOnUse">
          <path
            id="path69528"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69548" clipPathUnits="userSpaceOnUse">
          <path
            id="path69546"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69552" clipPathUnits="userSpaceOnUse">
          <path
            id="path69550"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-45" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-9-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-8-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-37"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-05" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-94"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-0-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-2-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69614" clipPathUnits="userSpaceOnUse">
          <path
            id="path69612"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69618" clipPathUnits="userSpaceOnUse">
          <path
            id="path69616"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69622" clipPathUnits="userSpaceOnUse">
          <path
            id="path69620"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69626" clipPathUnits="userSpaceOnUse">
          <path
            id="path69624"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69644" clipPathUnits="userSpaceOnUse">
          <path
            id="path69642"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69648" clipPathUnits="userSpaceOnUse">
          <path
            id="path69646"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69666" clipPathUnits="userSpaceOnUse">
          <path
            id="path69664"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69670" clipPathUnits="userSpaceOnUse">
          <path
            id="path69668"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69688" clipPathUnits="userSpaceOnUse">
          <path
            id="path69686"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69692" clipPathUnits="userSpaceOnUse">
          <path
            id="path69690"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69696" clipPathUnits="userSpaceOnUse">
          <path
            id="path69694"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69700" clipPathUnits="userSpaceOnUse">
          <path
            id="path69698"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69718" clipPathUnits="userSpaceOnUse">
          <path
            id="path69716"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69722" clipPathUnits="userSpaceOnUse">
          <path
            id="path69720"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69740" clipPathUnits="userSpaceOnUse">
          <path
            id="path69738"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69744" clipPathUnits="userSpaceOnUse">
          <path
            id="path69742"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69748" clipPathUnits="userSpaceOnUse">
          <path
            id="path69746"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69752" clipPathUnits="userSpaceOnUse">
          <path
            id="path69750"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69756" clipPathUnits="userSpaceOnUse">
          <path
            id="path69754"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69760" clipPathUnits="userSpaceOnUse">
          <path
            id="path69758"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69778" clipPathUnits="userSpaceOnUse">
          <path
            id="path69776"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69782" clipPathUnits="userSpaceOnUse">
          <path
            id="path69780"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69800" clipPathUnits="userSpaceOnUse">
          <path
            id="path69798"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69804" clipPathUnits="userSpaceOnUse">
          <path
            id="path69802"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69822" clipPathUnits="userSpaceOnUse">
          <path
            id="path69820"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69826" clipPathUnits="userSpaceOnUse">
          <path
            id="path69824"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69844" clipPathUnits="userSpaceOnUse">
          <path
            id="path69842"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69848" clipPathUnits="userSpaceOnUse">
          <path
            id="path69846"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69852" clipPathUnits="userSpaceOnUse">
          <path
            id="path69850"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69856" clipPathUnits="userSpaceOnUse">
          <path
            id="path69854"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69860" clipPathUnits="userSpaceOnUse">
          <path
            id="path69858"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69864" clipPathUnits="userSpaceOnUse">
          <path
            id="path69862"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69868" clipPathUnits="userSpaceOnUse">
          <path
            id="path69866"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69872" clipPathUnits="userSpaceOnUse">
          <path
            id="path69870"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69898" clipPathUnits="userSpaceOnUse">
          <path
            id="path69896"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69902" clipPathUnits="userSpaceOnUse">
          <path
            id="path69900"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-99" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-36"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-10" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69942" clipPathUnits="userSpaceOnUse">
          <path
            id="path69940"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath69946" clipPathUnits="userSpaceOnUse">
          <path
            id="path69944"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-98"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-0-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-2-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70008" clipPathUnits="userSpaceOnUse">
          <path
            id="path70006"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70012" clipPathUnits="userSpaceOnUse">
          <path
            id="path70010"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70016" clipPathUnits="userSpaceOnUse">
          <path
            id="path70014"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70020" clipPathUnits="userSpaceOnUse">
          <path
            id="path70018"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70024" clipPathUnits="userSpaceOnUse">
          <path
            id="path70022"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70028" clipPathUnits="userSpaceOnUse">
          <path
            id="path70026"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70046" clipPathUnits="userSpaceOnUse">
          <path
            id="path70044"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70050" clipPathUnits="userSpaceOnUse">
          <path
            id="path70048"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-18" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-57" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-48" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-10"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70130" clipPathUnits="userSpaceOnUse">
          <path
            id="path70128"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70134" clipPathUnits="userSpaceOnUse">
          <path
            id="path70132"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70138" clipPathUnits="userSpaceOnUse">
          <path
            id="path70136"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70142" clipPathUnits="userSpaceOnUse">
          <path
            id="path70140"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70146" clipPathUnits="userSpaceOnUse">
          <path
            id="path70144"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath70150" clipPathUnits="userSpaceOnUse">
          <path
            id="path70148"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <filter
          height={1.1046808}
          y={-0.052340379}
          width={1.0886487}
          x={-0.044324357}
          id="filter21611-1-1-4-7-3"
          colorInterpolationFilters="sRGB"
        >
          <feGaussianBlur
            id="feGaussianBlur21613-4-0-4-9-1"
            stdDeviation={0.05935181}
          />
        </filter>
        <clipPath id="clipPath3307-2-7-3-1-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-2-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-0-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-3-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-3-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-0-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84700" clipPathUnits="userSpaceOnUse">
          <path
            id="path84698"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84704" clipPathUnits="userSpaceOnUse">
          <path
            id="path84702"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-99-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-4-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-1-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-36-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-7-70-10-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-7-5-1-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-9-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-7-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84744" clipPathUnits="userSpaceOnUse">
          <path
            id="path84742"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84748" clipPathUnits="userSpaceOnUse">
          <path
            id="path84746"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-7-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-9-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-4-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-5-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-1-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-7-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-7-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-98-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-5-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-4-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-0-8-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-2-2-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-9-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-8-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84810" clipPathUnits="userSpaceOnUse">
          <path
            id="path84808"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84814" clipPathUnits="userSpaceOnUse">
          <path
            id="path84812"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84818" clipPathUnits="userSpaceOnUse">
          <path
            id="path84816"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84822" clipPathUnits="userSpaceOnUse">
          <path
            id="path84820"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84826" clipPathUnits="userSpaceOnUse">
          <path
            id="path84824"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84830" clipPathUnits="userSpaceOnUse">
          <path
            id="path84828"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84848" clipPathUnits="userSpaceOnUse">
          <path
            id="path84846"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84852" clipPathUnits="userSpaceOnUse">
          <path
            id="path84850"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-2-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-6-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-18-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-0-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-3-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-0-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-2-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-0-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-49-6-57-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-8-1-8-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-1-2-8-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-0-5-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-1-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-7-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-9-7-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-0-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-48-87" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-10-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-7-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-1-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84932" clipPathUnits="userSpaceOnUse">
          <path
            id="path84930"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84936" clipPathUnits="userSpaceOnUse">
          <path
            id="path84934"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84940" clipPathUnits="userSpaceOnUse">
          <path
            id="path84938"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84944" clipPathUnits="userSpaceOnUse">
          <path
            id="path84942"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84948" clipPathUnits="userSpaceOnUse">
          <path
            id="path84946"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath84952" clipPathUnits="userSpaceOnUse">
          <path
            id="path84950"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-4-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-8-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-08-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-39-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-4-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-3-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-73-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-90-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-0-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-90-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-0-0-2"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-2-7-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4-6-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5-1-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-9-9-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-8-1-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94010" clipPathUnits="userSpaceOnUse">
          <path
            id="path94008"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94014" clipPathUnits="userSpaceOnUse">
          <path
            id="path94012"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94018" clipPathUnits="userSpaceOnUse">
          <path
            id="path94016"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94022" clipPathUnits="userSpaceOnUse">
          <path
            id="path94020"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94026" clipPathUnits="userSpaceOnUse">
          <path
            id="path94024"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94030" clipPathUnits="userSpaceOnUse">
          <path
            id="path94028"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94048" clipPathUnits="userSpaceOnUse">
          <path
            id="path94046"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94052" clipPathUnits="userSpaceOnUse">
          <path
            id="path94050"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-45-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-4-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-9-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-1-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-4-4-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-5-3-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-9-5-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-8-8-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-9-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-37-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-2-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-8-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-05-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-94-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-0-2-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-2-9-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94114" clipPathUnits="userSpaceOnUse">
          <path
            id="path94112"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94118" clipPathUnits="userSpaceOnUse">
          <path
            id="path94116"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94122" clipPathUnits="userSpaceOnUse">
          <path
            id="path94120"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94126" clipPathUnits="userSpaceOnUse">
          <path
            id="path94124"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94144" clipPathUnits="userSpaceOnUse">
          <path
            id="path94142"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94148" clipPathUnits="userSpaceOnUse">
          <path
            id="path94146"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94166" clipPathUnits="userSpaceOnUse">
          <path
            id="path94164"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94170" clipPathUnits="userSpaceOnUse">
          <path
            id="path94168"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94188" clipPathUnits="userSpaceOnUse">
          <path
            id="path94186"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94192" clipPathUnits="userSpaceOnUse">
          <path
            id="path94190"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94196" clipPathUnits="userSpaceOnUse">
          <path
            id="path94194"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94200" clipPathUnits="userSpaceOnUse">
          <path
            id="path94198"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94218" clipPathUnits="userSpaceOnUse">
          <path
            id="path94216"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94222" clipPathUnits="userSpaceOnUse">
          <path
            id="path94220"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94240" clipPathUnits="userSpaceOnUse">
          <path
            id="path94238"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94244" clipPathUnits="userSpaceOnUse">
          <path
            id="path94242"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94248" clipPathUnits="userSpaceOnUse">
          <path
            id="path94246"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94252" clipPathUnits="userSpaceOnUse">
          <path
            id="path94250"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94256" clipPathUnits="userSpaceOnUse">
          <path
            id="path94254"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94260" clipPathUnits="userSpaceOnUse">
          <path
            id="path94258"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94278" clipPathUnits="userSpaceOnUse">
          <path
            id="path94276"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94282" clipPathUnits="userSpaceOnUse">
          <path
            id="path94280"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94300" clipPathUnits="userSpaceOnUse">
          <path
            id="path94298"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94304" clipPathUnits="userSpaceOnUse">
          <path
            id="path94302"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94322" clipPathUnits="userSpaceOnUse">
          <path
            id="path94320"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94326" clipPathUnits="userSpaceOnUse">
          <path
            id="path94324"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94344" clipPathUnits="userSpaceOnUse">
          <path
            id="path94342"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94348" clipPathUnits="userSpaceOnUse">
          <path
            id="path94346"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94352" clipPathUnits="userSpaceOnUse">
          <path
            id="path94350"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94356" clipPathUnits="userSpaceOnUse">
          <path
            id="path94354"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94360" clipPathUnits="userSpaceOnUse">
          <path
            id="path94358"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94364" clipPathUnits="userSpaceOnUse">
          <path
            id="path94362"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94368" clipPathUnits="userSpaceOnUse">
          <path
            id="path94366"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath94372" clipPathUnits="userSpaceOnUse">
          <path
            id="path94370"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-15-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-33-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-6-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-9-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-93-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-2-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-22-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-87-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-1-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-83-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-9-51-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-6-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-5-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-1-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-8-0-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-5-6-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97384" clipPathUnits="userSpaceOnUse">
          <path
            id="path97382"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97388" clipPathUnits="userSpaceOnUse">
          <path
            id="path97386"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97392" clipPathUnits="userSpaceOnUse">
          <path
            id="path97390"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97396" clipPathUnits="userSpaceOnUse">
          <path
            id="path97394"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97400" clipPathUnits="userSpaceOnUse">
          <path
            id="path97398"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97404" clipPathUnits="userSpaceOnUse">
          <path
            id="path97402"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-65-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-06-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-3-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-1-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-7-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-9-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-1-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-5-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-3-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-6-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-1-2-5-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-0-5-7-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-13-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-71-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-9-8-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-0-26-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-48-8-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-10-6-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-7-6-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-1-9-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97484" clipPathUnits="userSpaceOnUse">
          <path
            id="path97482"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97488" clipPathUnits="userSpaceOnUse">
          <path
            id="path97486"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97492" clipPathUnits="userSpaceOnUse">
          <path
            id="path97490"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97496" clipPathUnits="userSpaceOnUse">
          <path
            id="path97494"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97514" clipPathUnits="userSpaceOnUse">
          <path
            id="path97512"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97518" clipPathUnits="userSpaceOnUse">
          <path
            id="path97516"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97536" clipPathUnits="userSpaceOnUse">
          <path
            id="path97534"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97540" clipPathUnits="userSpaceOnUse">
          <path
            id="path97538"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97558" clipPathUnits="userSpaceOnUse">
          <path
            id="path97556"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97562" clipPathUnits="userSpaceOnUse">
          <path
            id="path97560"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97566" clipPathUnits="userSpaceOnUse">
          <path
            id="path97564"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97570" clipPathUnits="userSpaceOnUse">
          <path
            id="path97568"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97574" clipPathUnits="userSpaceOnUse">
          <path
            id="path97572"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97578" clipPathUnits="userSpaceOnUse">
          <path
            id="path97576"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97582" clipPathUnits="userSpaceOnUse">
          <path
            id="path97580"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97586" clipPathUnits="userSpaceOnUse">
          <path
            id="path97584"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97604" clipPathUnits="userSpaceOnUse">
          <path
            id="path97602"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97608" clipPathUnits="userSpaceOnUse">
          <path
            id="path97606"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97626" clipPathUnits="userSpaceOnUse">
          <path
            id="path97624"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97630" clipPathUnits="userSpaceOnUse">
          <path
            id="path97628"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97648" clipPathUnits="userSpaceOnUse">
          <path
            id="path97646"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97652" clipPathUnits="userSpaceOnUse">
          <path
            id="path97650"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97656" clipPathUnits="userSpaceOnUse">
          <path
            id="path97654"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97660" clipPathUnits="userSpaceOnUse">
          <path
            id="path97658"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97664" clipPathUnits="userSpaceOnUse">
          <path
            id="path97662"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath97668" clipPathUnits="userSpaceOnUse">
          <path
            id="path97666"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-48-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-44-42-5"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-90-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-6">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-7-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-3-2-8"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-1-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12709">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12707"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12713">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12711"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12731">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12729"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12735">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12733"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-4-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-47-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-0-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-9-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-7-7-70-32-6"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-7-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-18-5"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-07-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-4-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-3-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-3-1-9"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-8-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12779">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12777"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12783">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12781"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-3-9-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-48-3-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-42-6-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-90-8-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-0-9-9-8-5-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-04-9-6-7-7-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-3-2-3-18"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-0-1-6-48"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath12809" clipPathUnits="userSpaceOnUse">
          <path
            id="path12807"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath12813" clipPathUnits="userSpaceOnUse">
          <path
            id="path12811"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath12831" clipPathUnits="userSpaceOnUse">
          <path
            id="path12829"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath12835" clipPathUnits="userSpaceOnUse">
          <path
            id="path12833"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-0-9-9-8-5-1-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-7-7-6-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-3-2-3-1-0"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-1-6-4-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12857">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12855"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12861">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12859"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12879">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12877"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12883">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12881"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12887">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12885"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12891">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12889"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12895">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12893"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12899">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path12897"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-80-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-4-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-2-6-9-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-5-7-0-7"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-2-9-7-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-1-7-91"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-0-7-7"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-6-7-03"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14651">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14649"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14655">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14653"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14659">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14657"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14663">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14661"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49-8">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-69-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88-3">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-13-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-04-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-24-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-71-60"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-3">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-10-9-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-7-1-7"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-1-7-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14707">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14705"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14711">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path14709"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-68-7-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-80-1-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-7-6-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-4-8-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-3-65-3-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-6-9-3-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-0-2-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-7-6-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-0-9-5-6-4-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-04-9-1-7-9-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-0-7-4-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-6-7-0-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14755" clipPathUnits="userSpaceOnUse">
          <path
            id="path14753"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14759" clipPathUnits="userSpaceOnUse">
          <path
            id="path14757"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14763" clipPathUnits="userSpaceOnUse">
          <path
            id="path14761"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14767" clipPathUnits="userSpaceOnUse">
          <path
            id="path14765"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-49-5-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-69-0-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-88-7-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-13-4-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-7-70-5-1-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-7-5-04-8-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-24-5-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-71-6-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-0-9-48-3-9-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-04-9-10-9-9-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-7-1-4-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-1-7-0-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14811" clipPathUnits="userSpaceOnUse">
          <path
            id="path14809"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath14815" clipPathUnits="userSpaceOnUse">
          <path
            id="path14813"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-7-7-70-5-1-1-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-04-8-8-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-24-5-5-0"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-71-6-4-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-0-9-48-3-9-8-3"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-10-9-9-8-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-7-1-4-8-7"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-1-7-0-0-8"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9-59">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-6-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-44-20-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-25-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-2-6-5-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-5-7-71-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-2-9-4-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-9-8">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-5-8-3-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-2-9-0-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-5-6-33-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-2-72"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-5-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16961">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16959"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16965">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16963"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16969">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16967"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16973">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16971"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16977">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16975"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16981">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path16979"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-2-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31-3">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-04-56"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-27-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-10-1"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-8-47"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4-6">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-8-1-7-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-1-2-54-99"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-0-5-52-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-3-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-6-0-6-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-8-9-9-5"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-26-0-8-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17057">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17055"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17061">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17059"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17065">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17063"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17069">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17067"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17073">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17071"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17077">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path17075"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-9-5-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-6-7-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-20-6-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-25-0-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-3-2-1-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-6-5-0-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-71-9-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-4-2-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-6-2-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-2-3-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-5-4-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-5-0-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17121" clipPathUnits="userSpaceOnUse">
          <path
            id="path17119"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17125" clipPathUnits="userSpaceOnUse">
          <path
            id="path17123"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17129" clipPathUnits="userSpaceOnUse">
          <path
            id="path17127"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17133" clipPathUnits="userSpaceOnUse">
          <path
            id="path17131"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-45-2-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-2-4-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-31-9-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-04-5-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-7-70-2-0-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-7-5-27-1-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-10-4-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-8-4-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-49-6-4-2-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-8-1-7-7-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-1-2-54-9-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-0-5-52-5-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-6-1-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-2-7-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-5-2-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-5-9-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17195" clipPathUnits="userSpaceOnUse">
          <path
            id="path17193"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath17199" clipPathUnits="userSpaceOnUse">
          <path
            id="path17197"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-79"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-66" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-29"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-03" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-5-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-99"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-9-0-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-6-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-6-92" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-3-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-2-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-2-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-6-26" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-4-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-4-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-9-82"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-4-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-6-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-0-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-55"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-7-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-22"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-58" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-0-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-7-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-1-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-16"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-14" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-8-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-1-1-1-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-1-4-9-35"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-69"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-37"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-79"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-41"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-8-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-7-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-2-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-0-63"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-3-76"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-5-02"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-9-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-3-19"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-6-04"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-1-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-6-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-2-55"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-8-0-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-7-7-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-2-57-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-0-0-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-3-40-51"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-5-0-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-9-8-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-3-7-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-84-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-31-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-5-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-6-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-0-2"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-4-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-2-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-0-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-68" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-49" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-61"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-16" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-06"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-68"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-2-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-5-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-37"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-4-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-46" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-2-9-58" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-5-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-8-6-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-06-4-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-5-6-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-7-4-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-6-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-3-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-2-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-2-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-7-4-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-9-6-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-9-9-2"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-0-8-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-6-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-4-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-4-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-9-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-2-4-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-7-3-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-1-9-8-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-0-7-2-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-7-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-8-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-9-5-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-6-3-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-0-7-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-7-9-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-0-7-55"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-2-3-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-96"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-1-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-53"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-7-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-5" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-90"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-1-1-1-85" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-1-4-9-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-8-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-7-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-2-5"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-0-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-3-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-5-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-9-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-6-2"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-1-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-6-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-2-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-0-0-7-5-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-6-7-9-5-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-8-0-7-5-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-26-2-3-0-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-27" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-78"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-20" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-38"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-47" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-4-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-31"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-44-6-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-3-66"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-7" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-34"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-5-7-3" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-2-9-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-6-3-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-5-8-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-2-9-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-5-6-32"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-6-52" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-2-82"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-2-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-4-3-6-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-2-6-4-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-7-4-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-9-9-22"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-59" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-9" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-22"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-70-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-5-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-4-61" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-6-5-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-49-6-58" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-8-1-14"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-1-2-55" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-0-5-18"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-0-3-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-6-0-88"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-8-9-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-26-0-72"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-1-1-1-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-1-4-9-70"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-26"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-91"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-06"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-39"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-02"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-30"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-87"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-00"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-8-52"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-7-97"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-2-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-0-29"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-3-78"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-5-47"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-9-96"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-3-04"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-6-1-3-3-6-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-5-3-2-7-1-12"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-2-7-9-8-6-07"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-5-67-7-1-2-6"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-8-0-6"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-7-7-2"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-2-57-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-0-0-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-3-40-59"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-5-0-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-9-8-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-3-7-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-1-1-1-84-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-1-4-9-31-5"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-56-4-4-5-8"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-7-6-4-6-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-4-5-9-0-0-0"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-2-8-7-7-4-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-5-3-9-8-2-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-2-2-6-4-0-68"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-7-78" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-7-30"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-7-4-55" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-1-6-62"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient id="linearGradient3629">
          <stop offset={0} id="stop3625" stopColor="#fff" stopOpacity={1} />
          <stop offset={1} id="stop3627" stopColor="#37b4d7" stopOpacity={1} />
        </linearGradient>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-2-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-3-01"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30075">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30073"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30079">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30077"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-99-1">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-4-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-36-3"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-7-7-70-10-2"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-1-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-9-0"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-7-41"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30119">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30117"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30123">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30121"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-7-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-9-60"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-4-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-5-8"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-1-9">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-2-6-7-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-5-7-7-09"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-2-9-98-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-5-5">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-6-7-4-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-8-0-8-82"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-26-2-2-0"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-0">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-5-23"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-8-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30186">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30184"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30190">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30188"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30194">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30192"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30198">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30196"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30202">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30200"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30206">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30204"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30224">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30222"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30228">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30226"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-2-05">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-6-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-18-6">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-0-9"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-3-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-7-5-0-5"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-4-4-2-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-6-6-0-22"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-2-7-49-6-57-4"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-8-1-8-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-1-2-8-9"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-0-5-3-6"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-1-2">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-6-0-7-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-8-9-7-7"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-26-0-3-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-10-11"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-4">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-1-79"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30308">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30306"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30312">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30310"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30316">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30314"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30320">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30318"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30324">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30322"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30328">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path30326"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-1-9-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-2-7-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-0-5-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-3-01-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-1-6" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-4-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-3-4-4"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-0-3-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3692" clipPathUnits="userSpaceOnUse">
          <path
            id="path3690"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3696" clipPathUnits="userSpaceOnUse">
          <path
            id="path3694"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-99-1-4" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-4-0-8"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-6-6-2-1-0-0" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-3-3-2-36-3-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-1-2" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-4-10"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-3-4-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-0-3-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-2-7-7-70-10-2-7"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-1-7-7-5-1-0-9"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-4-4-9-0-3"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-6-6-7-41-4"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-7-3-1-9-1" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-7-72-2-7-0"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-7-44-0-5-1"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-1-3-3-01-7"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath3307-2-0-9-9-1-8" clipPathUnits="userSpaceOnUse">
          <path
            id="path3309-1-04-9-6-4-3"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          id="clipPath3307-6-6-2-8-3-4-9"
          clipPathUnits="userSpaceOnUse"
        >
          <path
            id="path3309-3-3-2-5-0-3-1"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath5148" clipPathUnits="userSpaceOnUse">
          <path
            id="path5146"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath5152" clipPathUnits="userSpaceOnUse">
          <path
            id="path5150"
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12070"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12072"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12074"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12076"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12078"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12080"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12082"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12084"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12086"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12088"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12090"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12092"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12094"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient12096"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1-7">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-04-9-6-4-7"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-2-8-3-4-8"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-2-5-0-3-2"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-9-3">
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-1-7-72-2-7-4"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath
          clipPathUnits="userSpaceOnUse"
          id="clipPath3307-6-6-7-44-0-5-5"
        >
          <path
            d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
            id="path3309-3-3-1-3-3-01-1"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient4253"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={463.64929}
          y1={41.939541}
          x2={464.38831}
          y2={41.939541}
        />
        <linearGradient
          xlinkHref="#linearGradient3629"
          id="linearGradient4255"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
          x1={503.52042}
          y1={39.562008}
          x2={504.38376}
          y2={39.562008}
        />
      </defs>
      <g id="layer1">
        <rect
          id="c26"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={126.62648}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c8"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={53.601437}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c2"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={29.259747}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="per2"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={29.259747}
          ry={0.015690081}
          fill={color[2]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <rect
          id="c37"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={175.3092}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c7"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={53.601437}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          opacity={1}
          fillOpacity={0.02887139}
        />
        <g
          transform="matrix(-1 0 0 1 411.619 104.474)"
          id="g11872"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-53.700035}
            cx={-55.325737}
            id="ellipse11848"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-45.604927}
            cx={-31.388998}
            id="ellipse11850"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11852"
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11854"
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11856"
            d="M34.547-58.566v9.736"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            id="path11858"
            d="M-19.191-59.766v7.38"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            transform="scale(-1 1)"
            id="rect11860"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            transform="scale(-1 1)"
            id="rect11862"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            id="path11864"
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11866"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11868"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11870"
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <g transform="matrix(-1 0 0 1 411.28 158.301)" id="g11900">
          <path
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            id="path11874"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            id="path11876"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse11878"
            cx={-55.190727}
            cy={-82.201485}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse11880"
            cx={-31.253988}
            cy={-74.106369}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            id="path11882"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M34.412-87.068v9.737"
            id="path11884"
            fill="none"
            stroke="url(#linearGradient12082)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            d="M-19.326-88.267v7.38"
            id="path11886"
            fill="none"
            stroke="url(#linearGradient12084)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            transform="scale(-1 1)"
            id="rect11888"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            transform="scale(-1 1)"
            id="rect11890"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            id="path11892"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11894"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11896"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            id="path11898"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <g id="g11928" transform="matrix(-1 0 0 1 411.28 109.618)">
          <path
            id="path11902"
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11904"
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-82.201485}
            cx={-55.190727}
            id="ellipse11906"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-74.106369}
            cx={-31.253988}
            id="ellipse11908"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11910"
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11912"
            d="M34.412-87.068v9.737"
            fill="none"
            stroke="url(#linearGradient12086)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path11914"
            d="M-19.326-88.267v7.38"
            fill="none"
            stroke="url(#linearGradient12088)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="rect11916"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            id="rect11918"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            id="path11920"
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11922"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11924"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11926"
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack9"
          y={27.468006}
          x={414.18912}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11930"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={27.468006}
            x={414.18912}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-02-B1"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.18912}
          y={50.293919}
          id="rack10"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={414.18912}
            y={50.293919}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11934"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-04-B1"}
          </tspan>
        </text>
        <text
          transform="scale(.9742 1.02648)"
          id="rack11"
          y={74.895081}
          x={414.18912}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11938"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={74.895081}
            x={414.18912}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-06-B1"}
          </tspan>
        </text>
        <g
          id="g11966"
          transform="matrix(-1 0 0 1 411.619 153.158)"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            id="ellipse11942"
            cx={-55.325737}
            cy={-53.700035}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            id="ellipse11944"
            cx={-31.388998}
            cy={-45.604927}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            id="path11946"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            id="path11948"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M34.547-58.566v9.736"
            id="path11950"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            d="M-19.191-59.766v7.38"
            id="path11952"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            id="rect11954"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            id="rect11956"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            id="path11958"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11960"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11962"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            id="path11964"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack12"
          y={97.721222}
          x={414.18912}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11968"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={97.721222}
            x={414.18912}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-08-B1"}
          </tspan>
        </text>
        <g id="g11998" transform="matrix(-1 0 0 1 411.28 205.927)">
          <path
            id="path11972"
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11974"
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-82.201485}
            cx={-55.190727}
            id="ellipse11976"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-74.106369}
            cx={-31.253988}
            id="ellipse11978"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11980"
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11982"
            d="M34.412-87.068v9.737"
            fill="none"
            stroke="url(#linearGradient12090)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path11984"
            d="M-19.326-88.267v7.38"
            fill="none"
            stroke="url(#linearGradient12092)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="rect11986"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            id="rect11988"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            id="path11990"
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11992"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11994"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11996"
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.18912}
          y={121.29177}
          id="rack13"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={414.18912}
            y={121.29177}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan12000"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-10-B1"}
          </tspan>
        </text>
        <g
          transform="matrix(-1 0 0 1 411.619 201.312)"
          id="g12028"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-53.700035}
            cx={-55.325737}
            id="ellipse12004"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-45.604927}
            cx={-31.388998}
            id="ellipse12006"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path12008"
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path12010"
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path12012"
            d="M34.547-58.566v9.736"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            id="path12014"
            d="M-19.191-59.766v7.38"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            transform="scale(-1 1)"
            id="rect12016"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            transform="scale(-1 1)"
            id="rect12018"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            id="path12020"
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path12022"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path12024"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path12026"
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.18912}
          y={144.63264}
          id="rack14"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={414.18912}
            y={144.63264}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan12030"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-15-B1"}
          </tspan>
        </text>
        <g transform="matrix(-1 0 0 1 411.28 254.61)" id="g12060">
          <path
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            id="path12034"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            id="path12036"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse12038"
            cx={-55.190727}
            cy={-82.201485}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse12040"
            cx={-31.253988}
            cy={-74.106369}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            id="path12042"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M34.412-87.068v9.737"
            id="path12044"
            fill="none"
            stroke="url(#linearGradient12094)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            d="M-19.326-88.267v7.38"
            id="path12046"
            fill="none"
            stroke="url(#linearGradient12096)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            transform="scale(-1 1)"
            id="rect12048"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            transform="scale(-1 1)"
            id="rect12050"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            id="path12052"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path12054"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path12056"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            id="path12058"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack15"
          y={168.7188}
          x={414.18912}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan12062"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={168.7188}
            x={414.18912}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-20-B1"}
          </tspan>
        </text>
        <g transform="matrix(-1 0 0 1 411.2 277.892)" id="g12060-2">
          <path
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            id="path12034-4"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            id="path12036-3"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse12038-3"
            cx={-55.190727}
            cy={-82.201485}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse12040-6"
            cx={-31.253988}
            cy={-74.106369}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            id="path12042-3"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M34.412-87.068v9.737"
            id="path12044-5"
            fill="none"
            stroke="url(#linearGradient4253)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            d="M-19.326-88.267v7.38"
            id="path12046-8"
            fill="none"
            stroke="url(#linearGradient4255)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            transform="scale(-1 1)"
            id="rect12048-9"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            transform="scale(-1 1)"
            id="rect12050-7"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            id="path12052-7"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path12054-4"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-7-3-1-9-3)"
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path12056-0"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-7-44-0-5-5)"
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            id="path12058-4"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack16"
          y={191.40025}
          x={414.18533}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54778px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan12062-1"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={191.40025}
            x={414.18533}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.7611px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"RESERVA"}
          </tspan>
        </text>
        <g transform="translate(26.999 133.658)" id="g11658">
          <path
            id="path11632"
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11634"
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-82.201485}
            cx={-55.190727}
            id="ellipse11636"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-74.106369}
            cx={-31.253988}
            id="ellipse11638"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11640"
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11642"
            d="M34.412-87.068v9.737"
            fill="none"
            stroke="url(#linearGradient12070)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path11644"
            d="M-19.326-88.267v7.38"
            fill="none"
            stroke="url(#linearGradient12072)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="rect11646"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            id="rect11648"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            id="path11650"
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11652"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11654"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11656"
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack2"
          y={50.663612}
          x={36.619556}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11660"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={50.663612}
            x={36.619556}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-02-B2"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.619556}
          y={27.48904}
          id="rack1"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.54777px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={36.619556}
            y={27.48904}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-01-B2"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.673565}
          y={32.439548}
          id="canal1"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.673565}
            y={32.439548}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:1-3-5"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.6273}
          y={55.487869}
          id="canal2"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.6273}
            y={55.487869}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:7-9-11"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.741081}
          y={80.239258}
          id="canal3"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.741081}
            y={80.239258}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-0"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:13-15-17"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.726078}
          y={103.06917}
          id="canal4"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.726078}
            y={103.06917}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-2"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:19-21-23"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.711075}
          y={126.67235}
          id="canal5"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.711075}
            y={126.67235}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-7"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:25-27-29"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.673565}
          y={149.77841}
          id="canal6"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.673565}
            y={149.77841}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-9"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:31-33-35"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.627304}
          y={173.86028}
          id="canal7"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.627304}
            y={173.86028}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-5"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:37-39-41"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.624802}
          y={195.86168}
          id="canal8"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={36.624802}
            y={195.86168}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-28"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:RESERVA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.43051}
          y={31.918802}
          id="canal9"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.43051}
            y={31.918802}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-1"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:2-4-6"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.47678}
          y={54.766609}
          id="canal10"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.47678}
            y={54.766609}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-4"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:8-10-12"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.41678}
          y={79.632317}
          id="canal11"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.41678}
            y={79.632317}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-17"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:14-16-18"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.43051}
          y={102.48536}
          id="canal12"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.43051}
            y={102.48536}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-3"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:20-22-24"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.43552}
          y={126.35935}
          id="canal13"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.43552}
            y={126.35935}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-178"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:26-28-30"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.43051}
          y={148.96832}
          id="canal14"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.43051}
            y={148.96832}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-70"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:32-34-36"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.47678}
          y={173.62093}
          id="canal15"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.47678}
            y={173.62093}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-6"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:38-40-42"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={414.36301}
          y={196.32196}
          id="canal16"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="4.32064px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.200734}
        >
          <tspan
            x={414.36301}
            y={196.32196}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11664-9-9-704"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.12128px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.200734}
          >
            {"CH:RESERVA"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.45092}
          y={75.278366}
          id="rack3"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={36.45092}
            y={75.278366}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11668"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-05-B1"}
          </tspan>
        </text>
        <g
          transform="translate(26.999 130.483)"
          id="g11696"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            id="ellipse11672"
            cx={-55.325737}
            cy={-53.700035}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            id="ellipse11674"
            cx={-31.388998}
            cy={-45.604927}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            id="path11676"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            id="path11678"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M34.547-58.566v9.736"
            id="path11680"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            d="M-19.191-59.766v7.38"
            id="path11682"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            id="rect11684"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            id="rect11686"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            id="path11688"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11690"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11692"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            id="path11694"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <g
          transform="translate(26.999 81.8)"
          id="g11722"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-53.700035}
            cx={-55.325737}
            id="ellipse11698"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-45.604927}
            cx={-31.388998}
            id="ellipse11700"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11702"
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11704"
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11706"
            d="M34.547-58.566v9.736"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            id="path11708"
            d="M-19.191-59.766v7.38"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            transform="scale(-1 1)"
            id="rect11710"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            transform="scale(-1 1)"
            id="rect11712"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            id="path11714"
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11716"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11718"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11720"
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <g id="g11750" transform="translate(26.999 182.342)">
          <path
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            id="path11724"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            id="path11726"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse11728"
            cx={-55.190727}
            cy={-82.201485}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            id="ellipse11730"
            cx={-31.253988}
            cy={-74.106369}
            rx={1.2687327}
            ry={1.0378371}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            id="path11732"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M34.412-87.068v9.737"
            id="path11734"
            fill="none"
            stroke="url(#linearGradient12074)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            d="M-19.326-88.267v7.38"
            id="path11736"
            fill="none"
            stroke="url(#linearGradient12076)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            transform="scale(-1 1)"
            id="rect11738"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            transform="scale(-1 1)"
            id="rect11740"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            id="path11742"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11744"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11746"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            id="path11748"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.450916}
          y={98.090675}
          id="rack4"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={36.450916}
            y={98.090675}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11752"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-07-B1"}
          </tspan>
        </text>
        <text
          transform="scale(.9742 1.02648)"
          id="rack5"
          y={121.67427}
          x={36.45092}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11756"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={121.67427}
            x={36.45092}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-09-B1"}
          </tspan>
        </text>
        <g
          id="g11784"
          transform="translate(26.999 178.108)"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-53.700035}
            cx={-55.325737}
            id="ellipse11760"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            transform="scale(-1 1)"
            ry={1.0378371}
            rx={1.2687327}
            cy={-45.604927}
            cx={-31.388998}
            id="ellipse11762"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11764"
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11766"
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11768"
            d="M34.547-58.566v9.736"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            id="path11770"
            d="M-19.191-59.766v7.38"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            transform="scale(-1 1)"
            id="rect11772"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            transform="scale(-1 1)"
            id="rect11774"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            id="path11776"
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11778"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11780"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11782"
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <g transform="translate(27.291 230.496)" id="g11812">
          <path
            id="path11786"
            d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11788"
            d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".233655px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-82.201485}
            cx={-55.190727}
            id="ellipse11790"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            ry={1.0378371}
            rx={1.2687327}
            cy={-74.106369}
            cx={-31.253988}
            id="ellipse11792"
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11794"
            d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path11796"
            d="M34.412-87.068v9.737"
            fill="none"
            stroke="url(#linearGradient12078)"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path11798"
            d="M-19.326-88.267v7.38"
            fill="none"
            stroke="url(#linearGradient12080)"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="rect11800"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
          />
          <path
            id="rect11802"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
          />
          <path
            id="path11804"
            d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
            clipPath="url(#clipPath3307-2-7-3-1-9)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11806"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
            clipPath="url(#clipPath3307-6-6-7-44-0-5)"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            id="path11808"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11810"
            d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
            fill="#3edce3"
            fillOpacity={1}
            stroke="#fff"
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </g>
        <text
          transform="scale(.9742 1.02648)"
          id="rack6"
          y={145.00233}
          x={36.45092}
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            id="tspan11814"
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            y={145.00233}
            x={36.45092}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-11-B1"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.45092}
          y={169.10184}
          id="rack7"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={36.45092}
            y={169.10184}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11818"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"F9-18-B1"}
          </tspan>
        </text>
        <g
          transform="translate(26.999 226.792)"
          id="g11846"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            id="ellipse11822"
            cx={-55.325737}
            cy={-53.700035}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            id="ellipse11824"
            cx={-31.388998}
            cy={-45.604927}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            id="path11826"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            id="path11828"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M34.547-58.566v9.736"
            id="path11830"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            d="M-19.191-59.766v7.38"
            id="path11832"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            id="rect11834"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            id="rect11836"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            id="path11838"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11840"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-0-9-9-1)"
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11842"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-2-8-3-4)"
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            id="path11844"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
            textAlign: "center"
          }}
          x={36.447128}
          y={190.85225}
          id="rack8"
          transform="scale(.9742 1.02648)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="6.35px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          textAnchor="middle"
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.304205}
        >
          <tspan
            x={36.447128}
            y={190.85225}
            style={{
              ////InkscapeFontSpecification: "'Franklin Gothic Medium, '",
              textAlign: "center"
            }}
            id="tspan11818-5"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Franklin Gothic Medium"
            textAnchor="middle"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.304205}
          >
            {"RESERVA"}
          </tspan>
        </text>
        <g
          transform="translate(26.533 249.118)"
          id="g11846-8"
          stroke="#fff"
          strokeOpacity={1}
        >
          <ellipse
            id="ellipse11822-9"
            cx={-55.325737}
            cy={-53.700035}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <ellipse
            id="ellipse11824-0"
            cx={-31.388998}
            cy={-45.604927}
            rx={1.2687327}
            ry={1.0378371}
            transform="scale(-1 1)"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.224436}
            paintOrder="stroke markers fill"
          />
          <path
            d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
            id="path11826-9"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
            id="path11828-3"
            fill="none"
            strokeWidth={0.441975}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            d="M34.547-58.566v9.736"
            id="path11830-6"
            fill="none"
            strokeWidth={0.857269}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".857269,.857269"
            strokeDashoffset={0}
          />
          <path
            d="M-19.191-59.766v7.38"
            id="path11832-1"
            fill="none"
            strokeWidth={1.00152}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".500759,.500759"
            strokeDashoffset={0}
          />
          <path
            id="rect11834-1"
            transform="scale(-1 1)"
            opacity={0.696078}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.209474}
            paintOrder="stroke markers fill"
            d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
          />
          <path
            id="rect11836-8"
            transform="scale(-1 1)"
            opacity={0.5}
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.153552}
            paintOrder="stroke markers fill"
            d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
          />
          <path
            d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
            id="path11838-9"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.116004}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
          <path
            id="path11840-7"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-2-0-9-9-1-7)"
            transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            id="path11842-2"
            d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
            clipPath="url(#clipPath3307-6-6-2-8-3-4-8)"
            transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.492842}
            paintOrder="stroke markers fill"
          />
          <path
            d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
            id="path11844-9"
            fill="#3edce3"
            fillOpacity={1}
            strokeWidth={0.0939632}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
          />
        </g>
        <rect
          id="rect30986"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={77.943123}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c34"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={158.90524}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c40"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={183.24661}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <path
          id="path1767"
          d="M95.926 55.938v-8.607l-3.294-2.573V22.893l-2.028-1.583v-2.573l7.73-5.936h18.756l5.196 4.155H326.37l4.816-3.76h11.279l4.182 3.068v4.056l-6.59 5.046v17.322l6.623 5.248v12.418l-3.987 2.903v135.67l2.106 1.363-.09 1.96-6.586 5.036h-28.179l-3.943-2.938H119.636l-2.24-1.889h-9.454l-2.285 1.924h-10.35l-2.285-1.924V57.94z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <rect
          id="c28"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={134.56389}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c10"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={61.538944}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c4"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={37.197254}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="per4"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={37.197254}
          ry={0.015690081}
          fill={color[4]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <rect
          id="c39"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={183.2466}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c9"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={61.538944}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          id="path1769"
          d="M92.086 187.525l-1.165-1.501v-9.89l2.844-2.9h4.755v12.065l-1.865 2.226z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1771"
          d="M93.987 43.676v-19.7l3.213 2.697v19.99z"
          display="inline"
          fill="#168198"
          fillOpacity={1}
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1773"
          d="M100.487 15.338l1.654-1.45h13.067l4.337 4.246h206.444l-.685.984H104.48z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1775"
          d="M346.398 21.733v8.069l-5.38 4.396V26.13z"
          display="inline"
          fill="#168198"
          fillOpacity={1}
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1777"
          d="M341.165 37.483v4.518l5.35 4.444v-4.71z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1779"
          d="M341.047 34.923v1.86l5.558 4.276v-4.88l-3.459-2.778z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1781"
          d="M346.605 35.382v-4.638l-2.897 2.295z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1783"
          d="M344.667 199.556l-1.208-.943.088-134.653 1.742-1.18v1.722l-.477.26z"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          id="path1785"
          d="M342.209 61.817l3.365-2.336V48.364l-3.194-2.378z"
          display="inline"
          fill="#168198"
          fillOpacity={1}
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M120.593 200.258h202.774l7.333 6.223h-3.53l-5.044-4.127h-9.061v-.938H121.902z"
          id="path1787"
          display="inline"
          fill="#168198"
          fillOpacity={1}
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M324.676 200.202h3.1l7.301 6.282-2.976.006z"
          id="path1789"
          display="inline"
          fill="none"
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <rect
          id="c1"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={29.259747}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c3"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={37.197254}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c5"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={45.302151}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          opacity={1}
          fillOpacity={0.02887139}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={132.74138}
          y={29.050831}
          id="text1799"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#008baf"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={132.74138}
            y={29.050831}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan1791"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#008baf"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"1"}
          </tspan>
          <tspan
            id="tspan1793"
            x={132.74138}
            y={35.781006}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#008baf"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"3"}
          </tspan>
          <tspan
            id="tspan1795"
            x={132.74138}
            y={42.511181}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#008baf"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"5"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur1"
          y={29.04401}
          x={190.79449}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={29.04401}
            x={190.79449}
            id="tspan1801"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[1]}
          </tspan>
        </text>
        <path
          id="path1807"
          d="M328.91 200.202h3l7.065 6.21-2.88.007z"
          display="inline"
          fill="#168198"
          fillOpacity={1}
          stroke="#3edce3"
          strokeWidth={0.405522}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
        />
        <path
          d="M54.82 213.162h3.214v2.721h-3.15z"
          id="path1809"
          transform="matrix(0 50.68693 -.32705 0 288.611 -2750.934)"
          display="inline"
          opacity={0.8}
          fill="#00b1d4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.175829}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray="none"
          strokeOpacity={1}
          filter="url(#filter21611-1-1-4-7-3)"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={35.906147}
          id="cur3"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1811"
            x={190.79449}
            y={35.906147}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[3]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={42.768269}
          id="cur5"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1817"
            x={190.79449}
            y={42.768269}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[5]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={49.630398}
          id="cur7"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1823"
            x={190.79449}
            y={49.630398}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[7]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={56.49255}
          id="cur9"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1829"
            x={190.79449}
            y={56.49255}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[9]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={63.35471}
          id="cur11"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1835"
            x={190.79449}
            y={63.35471}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[11]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={70.216881}
          id="cur13"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1841"
            x={190.79449}
            y={70.216881}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[13]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={77.079102}
          id="cur15"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1847"
            x={190.79449}
            y={77.079102}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[15]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={83.941284}
          id="cur17"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1853"
            x={190.79449}
            y={83.941284}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[17]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={90.803421}
          id="cur19"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1859"
            x={190.79449}
            y={90.803421}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[19]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={97.665588}
          id="cur21"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1865"
            x={190.79449}
            y={97.665588}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[21]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={104.52769}
          id="cur23"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1871"
            x={190.79449}
            y={104.52769}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
           {carga[23]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={111.38982}
          id="cur25"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1877"
            x={190.79449}
            y={111.38982}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[25]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={118.25198}
          id="cur27"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1883"
            x={190.79449}
            y={118.25198}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[27]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={125.11412}
          id="cur29"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1889"
            x={190.79449}
            y={125.11412}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[29]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={131.97623}
          id="cur31"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1895"
            x={190.79449}
            y={131.97623}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[31]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={138.83829}
          id="cur33"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1901"
            x={190.79449}
            y={138.83829}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[33]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={145.70039}
          id="cur35"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1907"
            x={190.79449}
            y={145.70039}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[35]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={152.56253}
          id="cur37"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1913"
            x={190.79449}
            y={152.56253}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[37]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={159.42468}
          id="cur39"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1919"
            x={190.79449}
            y={159.42468}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[39]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={190.79449}
          y={166.28683}
          id="cur41"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1925"
            x={190.79449}
            y={166.28683}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[41]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={158.43494}
          y={29.0229}
          id="cap1"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan1931"
            x={158.43494}
            y={29.0229}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[1]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap3"
          y={35.885036}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={35.885036}
            x={158.43494}
            id="tspan1937"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[3]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap5"
          y={42.74715}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={42.74715}
            x={158.43494}
            id="tspan1943"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[5]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap7"
          y={49.609272}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={49.609272}
            x={158.43494}
            id="tspan1949"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[7]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap9"
          y={56.471401}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={56.471401}
            x={158.43494}
            id="tspan1955"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
           {capacidad[9]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap11"
          y={63.333553}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={63.333553}
            x={158.43494}
            id="tspan1961"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[11]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap13"
          y={70.195702}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={70.195702}
            x={158.43494}
            id="tspan1967"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[13]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap15"
          y={77.05793}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={77.05793}
            x={158.43494}
            id="tspan1973"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[15]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap17"
          y={83.92012}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={83.92012}
            x={158.43494}
            id="tspan1979"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[17]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap19"
          y={90.782333}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={90.782333}
            x={158.43494}
            id="tspan1985"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[19]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap21"
          y={97.644516}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={97.644516}
            x={158.43494}
            id="tspan1991"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[21]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap23"
          y={104.50671}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={104.50671}
            x={158.43494}
            id="tspan1997"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[23]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap25"
          y={111.3689}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={111.3689}
            x={158.43494}
            id="tspan2003"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[25]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap27"
          y={118.23106}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={118.23106}
            x={158.43494}
            id="tspan2009"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[27]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap29"
          y={125.0932}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={125.0932}
            x={158.43494}
            id="tspan2015"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[29]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap31"
          y={131.95528}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={131.95528}
            x={158.43494}
            id="tspan2021"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[31]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap33"
          y={138.81732}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={138.81732}
            x={158.43494}
            id="tspan2027"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[33]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap35"
          y={145.67931}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={145.67931}
            x={158.43494}
            id="tspan2033"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[35]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap37"
          y={152.54131}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={152.54131}
            x={158.43494}
            id="tspan2039"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[37]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap39"
          y={159.40338}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={159.40338}
            x={158.43494}
            id="tspan2045"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[39]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap41"
          y={166.26544}
          x={158.43494}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={166.26544}
            x={158.43494}
            id="tspan2051"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[41]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={301.45044}
          y={28.912685}
          id="cap2"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2309"
            x={301.45044}
            y={28.912685}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[2]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap4"
          y={35.774822}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={35.774822}
            x={301.45044}
            id="tspan2315"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[4]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap6"
          y={42.636944}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={42.636944}
            x={301.45044}
            id="tspan2321"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[6]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap8"
          y={49.499081}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={49.499081}
            x={301.45044}
            id="tspan2327"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
           {capacidad[8]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap10"
          y={56.361225}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={56.361225}
            x={301.45044}
            id="tspan2333"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[10]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap12"
          y={63.223392}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={63.223392}
            x={301.45044}
            id="tspan2339"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[12]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap14"
          y={70.085556}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={70.085556}
            x={301.45044}
            id="tspan2346"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[14]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap16"
          y={76.947762}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={76.947762}
            x={301.45044}
            id="tspan2352"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[16]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap18"
          y={83.809937}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={83.809937}
            x={301.45044}
            id="tspan2358"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[18]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap20"
          y={90.672089}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={90.672089}
            x={301.45044}
            id="tspan2364"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[20]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap22"
          y={97.534256}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={97.534256}
            x={301.45044}
            id="tspan2370"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[22]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap24"
          y={104.39639}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={104.39639}
            x={301.45044}
            id="tspan2376"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[24]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap26"
          y={111.25853}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={111.25853}
            x={301.45044}
            id="tspan2382"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[26]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap28"
          y={118.12064}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={118.12064}
            x={301.45044}
            id="tspan2388"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[28]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap30"
          y={124.9828}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={124.9828}
            x={301.45044}
            id="tspan2394"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[30]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap32"
          y={131.84489}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={131.84489}
            x={301.45044}
            id="tspan2400"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[32]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap34"
          y={138.70699}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={138.70699}
            x={301.45044}
            id="tspan2406"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[34]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap36"
          y={145.56905}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00aad4"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={145.56905}
            x={301.45044}
            id="tspan2412"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00aad4"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[36]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap38"
          y={152.4312}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={152.4312}
            x={301.45044}
            id="tspan2418"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[38]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap40"
          y={159.29335}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={159.29335}
            x={301.45044}
            id="tspan2424"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[40]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cap42"
          y={166.15552}
          x={301.45044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={166.15552}
            x={301.45044}
            id="tspan2430"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {capacidad[42]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="l2"
          y={28.891567}
          x={368.87216}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={28.891567}
            x={368.87216}
            id="tspan2436"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[2]}
          </tspan>
        </text>
        <rect
          id="per6"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={45.302151}
          ry={0.015690081}
          fill={color[6]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={35.753704}
          id="l4"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2442"
            x={368.87216}
            y={35.753704}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[4]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={42.615833}
          id="l6"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2448"
            x={368.87216}
            y={42.615833}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[6]}
          </tspan>
        </text>
        <rect
          id="per8"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={54.130604}
          ry={0.015690081}
          fill={color[8]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={49.47794}
          id="l8"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2454"
            x={368.87216}
            y={49.47794}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[8]}
          </tspan>
        </text>
        <rect
          id="per10"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={62.068111}
          ry={0.015690081}
          fill={color[10]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={56.340084}
          id="l10"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2460"
            x={368.87216}
            y={56.340084}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[10]}
          </tspan>
        </text>
        <rect
          id="per12"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={70.173004}
          ry={0.015690081}
          fill={color[12]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={63.202236}
          id="l12"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2466"
            x={368.87216}
            y={63.202236}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[12]}
          </tspan>
        </text>
        <rect
          id="per14"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={78.47229}
          ry={0.015690081}
          fill={color[14]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={70.064369}
          id="l14"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2472"
            x={368.87216}
            y={70.064369}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[14]}
          </tspan>
        </text>
        <rect
          id="per16"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={86.409798}
          ry={0.015690081}
          fill={color[16]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={76.926598}
          id="l16"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2478"
            x={368.87216}
            y={76.926598}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[16]}
          </tspan>
        </text>
        <rect
          id="per18"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={94.514694}
          ry={0.015690081}
          fill={color[18]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={83.788788}
          id="l18"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2484"
            x={368.87216}
            y={83.788788}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[18]}
          </tspan>
        </text>
        <rect
          id="per20"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={102.81398}
          ry={0.015690081}
          fill={color[20]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={90.651001}
          id="l20"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2490"
            x={368.87216}
            y={90.651001}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[20]}
          </tspan>
        </text>
        <rect
          id="per22"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={110.75148}
          ry={0.015690081}
          fill={color[22]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={97.513184}
          id="l22"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2496"
            x={368.87216}
            y={97.513184}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[22]}
          </tspan>
        </text>
        <rect
          id="per24"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={118.85635}
          ry={0.015690081}
          fill={color[24]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={104.37541}
          id="l24"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2502"
            x={368.87216}
            y={104.37541}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[24]}
          </tspan>
        </text>
        <rect
          id="per26"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={126.62648}
          ry={0.015690081}
          fill={color[26]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={111.23756}
          id="l26"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2508"
            x={368.87216}
            y={111.23756}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[26]}
          </tspan>
        </text>
        <rect
          id="per28"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={134.5639}
          ry={0.015690081}
          fill={color[28]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={118.09973}
          id="l28"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2514"
            x={368.87216}
            y={118.09973}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[28]}
          </tspan>
        </text>
        <rect
          id="per30"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={142.66867}
          ry={0.015690081}
          fill={color[30]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={124.96188}
          id="l30"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2520"
            x={368.87216}
            y={124.96188}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[30]}
          </tspan>
        </text>
        <rect
          id="per32"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={150.96785}
          ry={0.015690081}
          opacity={0.949843}
          fill={color[32]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={131.82397}
          id="l32"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2526"
            x={368.87216}
            y={131.82397}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[32]}
          </tspan>
        </text>
        <rect
          id="per34"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={158.90526}
          ry={0.015690081}
          opacity={0.949843}
          fill={color[34]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={138.686}
          id="l34"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2532"
            x={368.87216}
            y={138.686}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[34]}
          </tspan>
        </text>
        <rect
          id="per36"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={167.01003}
          ry={0.015690081}
          opacity={0.949843}
          fill={color[36]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={145.54797}
          id="l36"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2538"
            x={368.87216}
            y={145.54797}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[36]}
          </tspan>
        </text>
        <rect
          id="per38"
          width={10.384028}
          height={5.6056557}
          x={311.64066}
          y={175.3092}
          ry={0.015690081}
          fill={color[38]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={152.40999}
          id="l38"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2544"
            x={368.87216}
            y={152.40999}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[38]}
          </tspan>
        </text>
        <rect
          id="per40"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={183.24661}
          ry={0.015690081}
          fill={color[40]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={159.27205}
          id="l40"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2550"
            x={368.87216}
            y={159.27205}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[40]}
          </tspan>
        </text>
        <rect
          id="per42"
          width={10.384028}
          height={5.6056557}
          x={311.64075}
          y={191.35138}
          ry={0.015690081}
          fill={color[42]}
          fillOpacity={1}
          strokeWidth={0.340385}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={368.87216}
          y={166.13411}
          id="l42"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2556"
            x={368.87216}
            y={166.13411}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[42]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text2816"
          y={21.818066}
          x={129.99599}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={21.818066}
            x={129.99599}
            id="tspan2814"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"CH"}
          </tspan>
        </text>
        <path
          d="M103.83 52.571h110.583"
          id="path2822"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908822}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908822,.908822"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          id="path2824"
          d="M102.695 76.87H213.28"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908823}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908823,.908823"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <rect
          id="c11"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={69.643837}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          d="M102.695 101.211H213.28"
          id="path2826"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908823}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908823,.908823"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <rect
          id="c13"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={77.943123}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          id="path2828"
          d="M102.695 125.553H213.28"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908823}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908823,.908823"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <rect
          id="c14"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={85.88063}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          d="M102.695 149.895H213.28"
          id="path2830"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908823}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908823,.908823"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <rect
          id="c17"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={93.985527}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          id="path2832"
          d="M102.695 174.236H213.28"
          fill="none"
          stroke="#757575"
          strokeWidth={0.908823}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".908823,.908823"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <rect
          id="c19"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={102.28481}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <path
          id="path2834"
          d="M222.288 52.528h115.12"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927273}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927273,.927273"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          d="M222.288 76.87h115.12"
          id="path2836"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927275}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927275,.927275"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          id="path2838"
          d="M222.288 101.211h115.12"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927274}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927274,.927274"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          d="M222.288 125.553h115.12"
          id="path2840"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927275}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927275,.927275"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          id="path2842"
          d="M222.288 149.895h115.12"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927273}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927273,.927273"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <path
          d="M222.288 174.237h115.12"
          id="path2844"
          fill="none"
          stroke="#757575"
          strokeWidth={0.927274}
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeMiterlimit={4}
          strokeDasharray=".927274,.927274"
          strokeDashoffset={0}
          strokeOpacity={0.996711}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2854"
          y={49.675762}
          x={132.74138}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2846"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={49.675762}
            x={132.74138}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"7"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={56.405937}
            x={132.74138}
            id="tspan2848"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"9"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={63.136112}
            x={132.74138}
            id="tspan2850"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"11"}
          </tspan>
        </text>
        <rect
          id="c21"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={110.22231}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={132.74138}
          y={70.300766}
          id="text2864"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={132.74138}
            y={70.300766}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2856"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"13"}
          </tspan>
          <tspan
            id="tspan2858"
            x={132.74138}
            y={77.030945}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"15"}
          </tspan>
          <tspan
            id="tspan2860"
            x={132.74138}
            y={83.761116}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"17"}
          </tspan>
        </text>
        <rect
          id="c23"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={118.32719}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2874"
          y={90.925888}
          x={132.74138}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2866"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={90.925888}
            x={132.74138}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"19"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={97.656067}
            x={132.74138}
            id="tspan2868"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"21"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={104.38624}
            x={132.74138}
            id="tspan2870"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"23"}
          </tspan>
        </text>
        <rect
          id="c25"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={126.62648}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c29"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={142.66866}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c27"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={134.56389}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={132.74138}
          y={111.55093}
          id="text2884"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={132.74138}
            y={111.55093}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2876"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"25"}
          </tspan>
          <tspan
            id="tspan2878"
            x={132.74138}
            y={118.2811}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"27"}
          </tspan>
          <tspan
            id="tspan2880"
            x={132.74138}
            y={125.01128}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"29"}
          </tspan>
        </text>
        <rect
          id="c35"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={167.01003}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c33"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={158.90526}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <rect
          id="c31"
          width={6.9844527}
          height={5.6056557}
          x={111.79743}
          y={150.96786}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2894"
          y={132.17586}
          x={132.74138}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2886"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={132.17586}
            x={132.74138}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"31"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={138.90604}
            x={132.74138}
            id="tspan2888"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"33"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={145.6362}
            x={132.74138}
            id="tspan2890"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"35"}
          </tspan>
        </text>
        <rect
          id="c41"
          width={6.9844527}
          height={5.6056557}
          x={111.78073}
          y={191.35136}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.02887139}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={132.74138}
          y={152.80061}
          id="text2904"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={132.74138}
            y={152.80061}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2896"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"37"}
          </tspan>
          <tspan
            id="tspan2898"
            x={132.74138}
            y={159.53079}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"39"}
          </tspan>
          <tspan
            id="tspan2900"
            x={132.74138}
            y={166.26096}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"41"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={119.79497}
          y={35.751202}
          id="text2910"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2906"
            x={119.79497}
            y={35.751202}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B1"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text2916"
          y={56.376133}
          x={119.79497}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={56.376133}
            x={119.79497}
            id="tspan2912"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B3"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={119.79497}
          y={77.001198}
          id="text2922"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2918"
            x={119.79497}
            y={77.001198}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B5"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text2928"
          y={97.626312}
          x={119.79497}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={97.626312}
            x={119.79497}
            id="tspan2924"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B7"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={119.79497}
          y={118.25136}
          id="text2934"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2930"
            x={119.79497}
            y={118.25136}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B9"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text2940"
          y={138.87619}
          x={119.17044}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={138.87619}
            x={119.17044}
            id="tspan2936"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B11"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={119.17044}
          y={159.50095}
          id="text2946"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2942"
            x={119.17044}
            y={159.50095}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B13"}
          </tspan>
        </text>
        <rect
          id="c6"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={45.302151}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2956"
          y={29.050831}
          x={273.88287}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2948"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={29.050831}
            x={273.88287}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"2"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={35.781006}
            x={273.88287}
            id="tspan2950"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"4"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={42.511181}
            x={273.88287}
            id="tspan2952"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"6"}
          </tspan>
        </text>
        <rect
          id="c12"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={69.643837}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={273.88287}
          y={49.675762}
          id="text2966"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={273.88287}
            y={49.675762}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2958"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"8"}
          </tspan>
          <tspan
            id="tspan2960"
            x={273.88287}
            y={56.405937}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"10"}
          </tspan>
          <tspan
            id="tspan2962"
            x={273.88287}
            y={63.136112}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"12"}
          </tspan>
        </text>
        <rect
          id="c16"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={85.88063}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c18"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={93.985527}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2976"
          y={70.300766}
          x={273.88287}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2968"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={70.300766}
            x={273.88287}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"14"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={77.030945}
            x={273.88287}
            id="tspan2970"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"16"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={83.761116}
            x={273.88287}
            id="tspan2972"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"18"}
          </tspan>
        </text>
        <rect
          id="c20"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={102.28481}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c22"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={110.22231}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c24"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={118.32719}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={273.88287}
          y={90.925888}
          id="text2986"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={273.88287}
            y={90.925888}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2978"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"20"}
          </tspan>
          <tspan
            id="tspan2980"
            x={273.88287}
            y={97.656067}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"22"}
          </tspan>
          <tspan
            id="tspan2982"
            x={273.88287}
            y={104.38624}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"24"}
          </tspan>
        </text>
        <rect
          id="c30"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={142.66867}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text2996"
          y={111.55093}
          x={273.88287}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2988"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={111.55093}
            x={273.88287}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"26"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={118.2811}
            x={273.88287}
            id="tspan2990"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"28"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={125.01128}
            x={273.88287}
            id="tspan2992"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"30"}
          </tspan>
        </text>
        <rect
          id="c36"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={167.01003}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c32"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={150.96785}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={273.88287}
          y={132.17586}
          id="text3006"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            x={273.88287}
            y={132.17586}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            id="tspan2998"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"32"}
          </tspan>
          <tspan
            id="tspan3000"
            x={273.88287}
            y={138.90604}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"34"}
          </tspan>
          <tspan
            id="tspan3002"
            x={273.88287}
            y={145.6362}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"36"}
          </tspan>
        </text>
        <rect
          id="c42"
          width={6.9844527}
          height={5.6056557}
          x={231.37103}
          y={191.35139}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <rect
          id="c38"
          width={6.9844527}
          height={5.6056557}
          x={231.3877}
          y={175.30922}
          ry={0.015690081}
          fill="#0f0"
          strokeWidth={0.27916}
          fillOpacity={0.03412073}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="text3016"
          y={152.80061}
          x={273.88287}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3008"
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={152.80061}
            x={273.88287}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"38"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={159.53079}
            x={273.88287}
            id="tspan3010"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"40"}
          </tspan>
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={166.26096}
            x={273.88287}
            id="tspan3012"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"42"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text3022"
          y={35.751202}
          x={260.93674}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={35.751202}
            x={260.93674}
            id="tspan3018"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B2"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={260.93674}
          y={56.376133}
          id="text3028"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3024"
            x={260.93674}
            y={56.376133}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B4"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text3034"
          y={77.001198}
          x={260.93674}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={77.001198}
            x={260.93674}
            id="tspan3030"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B6"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={260.93674}
          y={97.626312}
          id="text3040"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3036"
            x={260.93674}
            y={97.626312}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B8"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text3046"
          y={118.25136}
          x={260.31223}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={118.25136}
            x={260.31223}
            id="tspan3042"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B10"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={260.31226}
          y={138.87619}
          id="text3052"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan3048"
            x={260.31226}
            y={138.87619}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B12"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text3058"
          y={159.50095}
          x={260.31226}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="gray"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={159.50095}
            x={260.31226}
            id="tspan3054"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="gray"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"B14"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={242.63895}
          y={10.208264}
          id="text3280"
          transform="scale(.83112 1.20319)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.28127px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.259528}
        >
          <tspan
            id="tspan3276"
            x={242.63895}
            y={10.208264}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.28127px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.259528}
          >
            <tspan
              id="tspan3274"
              fontSize="9.87777px"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.259528}
            >
              {"PANEL 1"}
            </tspan>
          </tspan>
        </text>
        <path
          id="rect3705"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 53.460804H118.909421V75.400139H111.65446z"
        />
        <path
          id="rect3707"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 29.119104H118.909421V51.058439H111.65446z"
        />
        <path
          id="rect3709"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 77.802475H118.909421V99.74181H111.65446z"
        />
        <path
          id="rect3711"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 102.14417H118.909421V124.083505H111.65446z"
        />
        <path
          id="rect3713"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 126.48582H118.909421V148.42515500000002H111.65446z"
        />
        <path
          id="rect3715"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 150.82716H118.909421V172.766495H111.65446z"
        />
        <path
          id="rect3717"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M111.65446 175.16849H118.909421V197.107825H111.65446z"
        />
        <path
          id="rect3719"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 53.460804H238.499771V75.400139H231.24481z"
        />
        <path
          id="rect3721"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 29.119104H238.499771V51.058439H231.24481z"
        />
        <path
          id="rect3723"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 77.802475H238.499771V99.74181H231.24481z"
        />
        <path
          id="rect3725"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 102.14417H238.499771V124.083505H231.24481z"
        />
        <path
          id="rect3727"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 126.48582H238.499771V148.42515500000002H231.24481z"
        />
        <path
          id="rect3729"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 150.82716H238.499771V172.766495H231.24481z"
        />
        <path
          id="rect3731"
          fill="none"
          fillRule="evenodd"
          stroke="#3fdbe4"
          strokeWidth={0.285663}
          strokeOpacity={1}
          d="M231.24481 175.16849H238.499771V197.107825H231.24481z"
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={194.32234}
          y={21.818066}
          id="text84174"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84172"
            x={194.32234}
            y={21.818066}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"A"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text84178"
          y={21.818066}
          x={214.93172}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={21.818066}
            x={214.93172}
            id="tspan84176"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"% LOAD"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={149.35631}
          y={21.818066}
          id="text84182"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84180"
            x={149.35631}
            y={21.818066}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"CAPACIDAD"}
          </tspan>
        </text>
        <rect
          id="per1"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={29.259747}
          ry={0.015690081}
          fill={color[1]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          transform="scale(.84731 1.1802)"
          id="l1"
          y={29.0229}
          x={219.29469}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={29.0229}
            x={219.29469}
            id="tspan84192"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[1]}
          </tspan>
        </text>
        <rect
          id="per3"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={37.197254}
          ry={0.015690081}
          fill={color[3]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={35.885036}
          id="l3"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84198"
            x={219.29469}
            y={35.885036}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[3]}
          </tspan>
        </text>
        <rect
          id="per5"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={45.302151}
          ry={0.015690081}
          fill={color[5]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={42.74715}
          id="l5"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84204"
            x={219.29469}
            y={42.74715}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[5]}
          </tspan>
        </text>
        <rect
          id="per7"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={54.130604}
          ry={0.015690081}
          fill={color[7]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={49.609272}
          id="l7"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84210"
            x={219.29469}
            y={49.609272}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[7]}
          </tspan>
        </text>
        <rect
          id="per9"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={62.068111}
          ry={0.015690081}
          fill={color[9]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={56.471401}
          id="l9"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84216"
            x={219.29469}
            y={56.471401}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[9]}
          </tspan>
        </text>
        <rect
          id="per11"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={70.173004}
          ry={0.015690081}
          fill={color[11]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={63.333553}
          id="l11"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84222"
            x={219.29469}
            y={63.333553}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[11]}
          </tspan>
        </text>
        <rect
          id="per13"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={78.47229}
          ry={0.015690081}
          fill={color[13]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={70.195702}
          id="l13"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84228"
            x={219.29469}
            y={70.195702}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[13]}
          </tspan>
        </text>
        <rect
          id="per15"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={86.409798}
          ry={0.015690081}
          fill={color[15]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={77.05793}
          id="l15"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84234"
            x={219.29469}
            y={77.05793}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[15]}
          </tspan>
        </text>
        <rect
          id="per17"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={94.514694}
          ry={0.015690081}
          fill={color[17]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={83.92012}
          id="l17"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84240"
            x={219.29469}
            y={83.92012}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[17]}
          </tspan>
        </text>
        <rect
          id="per19"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={102.81398}
          ry={0.015690081}
          fill={color[19]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={90.782333}
          id="l19"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84246"
            x={219.29469}
            y={90.782333}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[19]}
          </tspan>
        </text>
        <rect
          id="per21"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={110.75148}
          ry={0.015690081}
          fill={color[21]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={97.644516}
          id="l21"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84252"
            x={219.29469}
            y={97.644516}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[21]}
          </tspan>
        </text>
        <rect
          id="per23"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={118.85635}
          ry={0.015690081}
          fill={color[23]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={104.50671}
          id="l23"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84258"
            x={219.29469}
            y={104.50671}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[23]}
          </tspan>
        </text>
        <rect
          id="per25"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={126.62648}
          ry={0.015690081}
          fill={color[25]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={111.3689}
          id="l25"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84264"
            x={219.29469}
            y={111.3689}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[25]}
          </tspan>
        </text>
        <rect
          id="per27"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={134.5639}
          ry={0.015690081}
          fill={color[27]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={118.23106}
          id="l27"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84270"
            x={219.29469}
            y={118.23106}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[27]}
          </tspan>
        </text>
        <rect
          id="per29"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={142.66867}
          ry={0.015690081}
          fill={color[29]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={125.0932}
          id="l29"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84276"
            x={219.29469}
            y={125.0932}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[29]}
          </tspan>
        </text>
        <rect
          id="per31"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={150.96785}
          ry={0.015690081}
          fill={color[31]}
          strokeWidth={0.340385}
          opacity={0.94984326}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={131.95528}
          id="l31"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84282"
            x={219.29469}
            y={131.95528}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[31]}
          </tspan>
        </text>
        <rect
          id="per33"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={158.90526}
          ry={0.015690081}
          fill={color[33]}
          strokeWidth={0.340385}
          opacity={0.94984326}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={138.81732}
          id="l33"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84288"
            x={219.29469}
            y={138.81732}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[33]}
          </tspan>
        </text>
        <rect
          id="per35"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={167.01003}
          ry={0.015690081}
          fill={color[35]}
          strokeWidth={0.340385}
          opacity={0.94984326}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={145.67931}
          id="l35"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84294"
            x={219.29469}
            y={145.67931}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[35]}
          </tspan>
        </text>
        <rect
          id="per37"
          width={10.384028}
          height={5.6056557}
          x={185.16991}
          y={175.3092}
          ry={0.015690081}
          fill={color[37]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={152.54131}
          id="l37"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84300"
            x={219.29469}
            y={152.54131}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[37]}
          </tspan>
        </text>
        <rect
          id="per39"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={183.24661}
          ry={0.015690081}
          fill={color[39]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={159.40338}
          id="l39"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84306"
            x={219.29469}
            y={159.40338}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[39]}
          </tspan>
        </text>
        <rect
          id="per41"
          width={10.384028}
          height={5.6056557}
          x={185.17}
          y={191.35138}
          ry={0.015690081}
          fill={color[41]}
          strokeWidth={0.340385}
          fillOpacity={1}
        />
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={219.29469}
          y={166.26544}
          id="l41"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#000"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84312"
            x={219.29469}
            y={166.26544}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#000"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {load[41]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          x={337.45218}
          y={29.0229}
          id="cur2"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84521"
            x={337.45218}
            y={29.0229}
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[2]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur4"
          y={35.885036}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={35.885036}
            x={337.45218}
            id="tspan84527"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[4]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur6"
          y={42.74715}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={42.74715}
            x={337.45218}
            id="tspan84533"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[6]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur8"
          y={49.609272}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={49.609272}
            x={337.45218}
            id="tspan84539"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[8]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur10"
          y={56.471401}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={56.471401}
            x={337.45218}
            id="tspan84545"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[10]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur12"
          y={63.333553}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={63.333553}
            x={337.45218}
            id="tspan84551"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[12]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur14"
          y={70.195702}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={70.195702}
            x={337.45218}
            id="tspan84557"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[14]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur16"
          y={77.05793}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={77.05793}
            x={337.45218}
            id="tspan84563"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[16]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur18"
          y={83.92012}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={83.92012}
            x={337.45218}
            id="tspan84569"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
           {carga[18]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur20"
          y={90.782333}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={90.782333}
            x={337.45218}
            id="tspan84575"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[20]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur22"
          y={97.644516}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={97.644516}
            x={337.45218}
            id="tspan84581"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[22]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur24"
          y={104.50671}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={104.50671}
            x={337.45218}
            id="tspan84587"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[24]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur26"
          y={111.3689}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={111.3689}
            x={337.45218}
            id="tspan84593"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[26]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur28"
          y={118.23106}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={118.23106}
            x={337.45218}
            id="tspan84599"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[28]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur30"
          y={125.0932}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={125.0932}
            x={337.45218}
            id="tspan84605"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[30]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur32"
          y={131.95528}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={131.95528}
            x={337.45218}
            id="tspan84611"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[32]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur34"
          y={138.81732}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={138.81732}
            x={337.45218}
            id="tspan84617"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[34]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur36"
          y={145.67931}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={145.67931}
            x={337.45218}
            id="tspan84623"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[36]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur38"
          y={152.54131}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={152.54131}
            x={337.45218}
            id="tspan84629"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[38]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur40"
          y={159.40338}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={159.40338}
            x={337.45218}
            id="tspan84635"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[40]}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="cur42"
          y={166.26544}
          x={337.45218}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, '"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.38414px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#00abd6"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            //style={{ //InkscapeFontSpecification: "'Franklin Gothic Medium, '" }}
            y={166.26544}
            x={337.45218}
            id="tspan84641"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            fill="#00abd6"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {carga[42]}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={271.13821}
          y={21.818066}
          id="text84649"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84647"
            x={271.13821}
            y={21.818066}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"CH"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text84653"
          y={21.818066}
          x={291.74728}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={21.818066}
            x={291.74728}
            id="tspan84651"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"CAPACIDAD"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          x={341.08447}
          y={21.818066}
          id="text84657"
          transform="scale(.84731 1.1802)"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan84655"
            x={341.08447}
            y={21.818066}
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"A"}
          </tspan>
        </text>
        <text
          transform="scale(.84731 1.1802)"
          id="text84661"
          y={21.818066}
          x={363.56705}
          style={{
            lineHeight: 1.25,
            //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
            fontVariantLigatures: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantEastAsian: "normal"
          }}
          xmlSpace="preserve"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="5.64444px"
          fontFamily="Franklin Gothic Medium"
          letterSpacing={0}
          wordSpacing={0}
          display="inline"
          fill="#d4aa00"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            style={{
              //InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: "normal",
              fontVariantCaps: "normal",
              fontVariantNumeric: "normal",
              fontVariantEastAsian: "normal"
            }}
            y={21.818066}
            x={363.56705}
            id="tspan84659"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            fill="#d4aa00"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {"% LOAD"}
          </tspan>
        </text>
      </g>
    </svg>
    </div>
 );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
