import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  //LINKS
  let ger_url = 'https://bmsclouduio.i.telconet.net/d/-MP4guXMk/gerencia?orgId=1&refresh=5s';
  let dse_url = 'https://bmsclouduio.i.telconet.net/d/kDsk0H3Gz/dse-7420?orgId=1&refresh=5s';
  let dc_url = 'https://bmsclouduio.i.telconet.net/d/bz40p9_Mz/principal?orgId=1&refresh=5s';
  let elec_url = 'https://bmsclouduio.i.telconet.net/d/GqPfZd9Mz/unifilar-electrico?orgId=1&refresh=5s';
  let aacc_url = 'https://bmsclouduio.i.telconet.net/d/WiGOxcXMz/aacc?orgId=1&refresh=5s';
  let sis_url = 'https://bmsclouduio.i.telconet.net/d/ruoLmnrMk/sistema-contraincendios?orgId=1&refresh=5s';
  let arq_url = 'https://bmsclouduio.i.telconet.net/d/YE3CT_qMk/arquitectura?orgId=1&refresh=5s';
  let mv_url = 'https://bmsclouduio.i.telconet.net/d/reOkkQ6Gz/grupal-mvs?orgId=1&refresh=5s';
  let swt_url = 'https://bmsclouduio.i.telconet.net/d/aKTaOleMz/grupal-sw?orgId=1&refresh=5s';
  let plc_url = 'https://bmsclouduio.i.telconet.net/d/-B6_1qeGz/grupal-plc?orgId=1&refresh=5s';
  let gtw_url = 'https://bmsclouduio.i.telconet.net/d/ga490G6Gz/grupal-gtw?orgId=1&refresh=5s';
  let relec_url = 'https://bmsclouduio.i.telconet.net/d/pl_nX_XGz/reporte-electrico?orgId=1&refresh=5s';
  let raacc_url = 'https://bmsclouduio.i.telconet.net/d/pl_nX_XGz/reporte-electrico?orgId=1&refresh=5s';
  let delec_url = 'http://172.30.128.208:1880/ui/#!/1?socketid=953mpJJRWzyDbgyJAAAq';
  let daacc_url = 'http://172.30.128.208:1880/ui/#!/2?socketid=953mpJJRWzyDbgyJAAAq';

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        id="svg1462"
        viewBox="0 0 507.99999 44.979168"
        height={'100%'}
        width={'100%'}
        //{...props}
      >
        <defs id="defs1456">
          <clipPath id="clipPath1069" clipPathUnits="userSpaceOnUse">
            <ellipse
              ry={92.971107}
              rx={51.450279}
              cy={346.14044}
              cx={-248.095}
              id="ellipse1071"
              opacity={1}
              fill="olive"
              fillOpacity={1}
              fillRule="evenodd"
              stroke="none"
              strokeWidth={7.53068}
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
              paintOrder="stroke fill markers"
            />
          </clipPath>
          <clipPath id="clipPath5494" clipPathUnits="userSpaceOnUse">
            <rect
              ry={1.2665578}
              y={373.44049}
              x={1007.6845}
              height={226.78572}
              width={102.80952}
              id="rect5496"
              fill="none"
              fillRule="evenodd"
              stroke="#61625b"
              strokeWidth={4.99999}
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeOpacity={1}
            />
          </clipPath>
          <filter
            id="filter14519"
            x={-0.017834665}
            width={1.0356693}
            y={-0.009041911}
            height={1.0180838}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.58931073} id="feGaussianBlur14521" />
          </filter>
          <filter
            id="filter14519-4"
            x={-0.017834665}
            width={1.0356693}
            y={-0.009041911}
            height={1.0180838}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.58931073} id="feGaussianBlur14521-0" />
          </filter>
          <filter
            id="filter14519-8"
            x={-0.017834665}
            width={1.0356693}
            y={-0.009041911}
            height={1.0180838}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.58931073} id="feGaussianBlur14521-8" />
          </filter>
          <linearGradient
            gradientTransform="matrix(.74967 0 0 .65243 47.062 -70.7)"
            gradientUnits="userSpaceOnUse"
            y2={145.97466}
            x2={260.96414}
            y1={172.82849}
            x1={260.96414}
            id="linearGradient1158"
            xlinkHref="#linearGradient1156"
          />
          <linearGradient id="linearGradient1156">
            <stop id="stop1152" offset={0} stopColor="#09a8ae" stopOpacity={1} />
            <stop id="stop1154" offset={1} stopColor="#09a8ae" stopOpacity={0} />
          </linearGradient>
          <filter
            height={1.1713483}
            y={-0.085674152}
            width={1.1634072}
            x={-0.081703611}
            id="filter2091-9-7"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur2093-4-3" stdDeviation={0.2879845} />
          </filter>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2072">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path2074"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath343">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path341"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath347">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path345"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath351">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path349"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath355">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path353"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath359">
            <path
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              id="path357"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
        </defs>
        <g id="layer1">
          <path
            d="M41.759 7.448v12.16H88.13l6.548-5.745V1.903H48.107z"
            id="path1095"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.499999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1097"
            d="M42.08 11.698V7.671l6.122-5.42h4.681"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1099"
            d="M163.711 6.487l-.385.323.022 1.335-.86-.434-.26.156v.408l1.08.614v1.04l-.816-.448-.017.905-.874-.47V8.602l-.314-.215-.3.166v.986l-1.282-.748-.359.206.036.515 1.152.619-.735.457-.01.367.409.229 1.066-.596.73.466-.64.47.668.467-.811.47-1.05-.618-.371.179.004.41.743.43-1.155.666v.488l.404.211 1.201-.708v.887l.314.198.322-.238v-1.224l.928-.448v.87l.758-.511.004 1.062-1.045.592v.367l.31.22.793-.457v1.33l.394.323.296-.331v-1.313l.825.461.282-.188-.013-.515-1.013-.511v-1.031l.793.493v-.83l.883.485v1.192l.323.237.34-.246v-.941l1.143.735.417-.188-.027-.476-1.134-.627.722-.47v-.39l-.292-.184-1.062.555-.829-.434.695-.48-.686-.403.798-.57 1.116.624.318-.198-.027-.403-.757-.435 1.174-.654-.045-.506-.354-.162-1.174.623-.014-.91-.345-.197-.367.229V9.88l-.892.457v-.919l-.73.516v-1.02l1.113-.643v-.386l-.339-.196-.836.483v-1.48zm.376 4.43l.37.592-.36.595-.733.005-.37-.59.361-.597z"
            display="inline"
            opacity={1}
            fill="#3fbefa"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0177471px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            clipPath="url(#clipPath1069)"
            transform="matrix(.0645 0 0 .0645 190.573 -11.849)"
            id="g1105"
            display="inline"
            opacity={0.8}
          >
            <image
              y={394.28033}
              x={933.47174}
              width={243.41667}
              height={169.33333}
              preserveAspectRatio="none"
              xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5gAAAKACAYAAAD5Otf/AAAgAElEQVR4XuzdTawm13kf+Lomm/De CGwhDhgBBuyVrbEkUAlJbRIkMSI2BQRhAwqz6GA8YEReMhvBgA2thBlgwE3ISzLMOABnQREggwBs xoFjw56Fu5mYkOWRvbIBA44RCbQn8D7opvgOziWrVV1dH6fOW1XvOVW/u+mPW6fqnN9T9/b991Mf Z5UPAgQIECBAgAABAgQIECAwg8DZDPuwCwIECBAgQIAAAQIECBAgUAmYTgICBAgQIECAAAECBAgQ mEVAwJyF0U4IECBAgAABAgQIECBAQMB0DhAgQIAAAQIECBAgQIDALAIC5iyMdkKAAAECBAgQIECA AAECAqZzgAABAgQIECBAgAABAgRmERAwZ2G0EwIECBAgQIAAAQIECBAQMJ0DBAgQIECAAAECBAgQ IDCLgIA5C6OdECBAgAABAgQIECBAgICA6RwgQIAAAQIECBAgQIAAgVkEBMxZGO2EAAECBAgQIECA AAECBARM5wABAgQIECBAgAABAgQIzCIgYM7CaCcECBAgQIAAAQIECBAgIGA6BwgQIECAAAECBAgQ IEBgFgEBcxZGOyFAgAABAgQIECBAgAABAdM5QIAAAQIECBAgQIAAAQKzCAiYszDaCQECBAgQIECA AAECBAgImM4BAgQIECBAgAABAgQIEJhFQMCchdFOCBAgQIAAAQIECBAgQEDAdA4QIECAAAECBAgQ IECAwCwCAuYsjHZCgAABAgQIECBAgAABAgKmc4AAAQIECBAgQIAAAQIEZhEQMGdhtBMCBAgQIECA AAECBAgQEDCdAwQIECBAgAABAgQIECAwi4CAOQujnRAgQIAAAQIECBAgQICAgOkcIECAAAECBAgQ IECAAIFZBATMWRjthAABAgQIECBAgAABAgQETOcAAQIECBAgQIAAAQIECMwiIGDOwmgnBAgQIECA AAECBAgQICBgOgcIECBAgAABAgQIECBAYBYBAXMWRjshQIAAAQIECBAgQIAAAQHTOUCAAAECBAgQ IECAAAECswgImLMw2gkBAgQIECBAgAABAgQICJjOAQIECBAgQIAAAQIECBCYRUDAnIXRTggQIECA AAECBAgQIEBAwHQOECBAgAABAgQIECBAgMAsAgLmLIx2QoAAAQIECBAgQIAAAQICpnOAAAECBAgQ IJCZwAvPPXu4ePW16t0bN6qrV6/6eS2z+pgOAQL9Ar5hOTsIECBAgAABAicWqANlexoC5okL4/AE CEwWEDAnkxlAgAABAgQIEDhO4L333ju8+OKL1fu3bg3uSMA8ztloAgTWFxAw1zd3RAIECBAgQGCH An1dykBxraqqJ8/Oqqc+dXnwcLj8nYC5wxPFkgkULiBgFl5A0ydAgAABAgTyFBjrUoZQ+e2z7h/F BMw8a2pWBAiMCwiY40a2IECAAAECBAiMCsQEymaXcmiHAuYotw0IEMhUQMDMtDCmRYAAAQIECOQt EALl7/72b1Xhaa99H281LnsdWs3ZlSvV7aefrq688cblZgJm3rU3OwIE+gUETGcHAQIECBAgQCBS YM4u5Z3r16uHHnmk+uiLX6we/NznLmdweOCBewLmx4eDn9Uia2MzAgTyEPBNK486mAUBAgQIECCQ oUBKoAzdyMOdO/etpu5S/thzz90NlM2NPvzww+qnfvqnBcwMzwNTIkAgXkDAjLeyJQECBAhsVGDo 6Z7NJT/62GPVzZs3/du50fOgXtbFxcXhheef71xleDBP+Oh7OE97UN2l/PArX6k+85nPDMoJmBs/ sSyPwE4E/CO5k0JbJgECBAjcK/D4448fxt5BOGQWwuZTTz1VnZ+f+7e08JNr7D8Y2q8Q6Vtu3aEM l71Wv/zLSSoukU1iM4gAgYwE/KOYUTFMhQABAgSWFxgLliE4/uIv/Hz1Mz/7c9Wf/emfVH/4R398 OamhMOo+ueXrNucRxh7O0w6UITiGj5TLXqfMWwdzipZtCRDIVUDAzLUy5kWAAAECswr0danOn/16 9Su/+mujly/Wk3nllVeqd955pzNwCpqzlmzWnQ11KWM7lGFCY/dRHjNpAfMYPWMJEMhFQMDMpRLm QYAAAQKLCfzY2dmhvfN3b9yonnjiid5jhh/2h+6ZC5+/du3afUEz7Pfq1av+fV2smnE7Tnk4T9ee 6+5ldXFRxdxHGTe77q0EzGP0jCVAIBcB/wDmUgnzIECAAIFFBNrhsi9Y1oGyHSz7/r6e7H/8j/+x evHFF+8Jmi+9/LJ7Mxep5vBOhy5/zqVLOcbiHswxIZ8nQCB3AQEz9wqZHwECBAgkCzTDZbi38vd+ 7/fu21dfp7Lr75thM+yo2eH8V+fPVRevvnZ3/+HS25deedW/s8nVGx849LTXMDqEypinvS552ev4 Kn60hQ7mFC3bEiCQq4B/+HKtjHkRIECAwFECzXAZwt6/vnhlcH9jHczm4HbQrMNmuD+z+XoLl8se VcLOwVO6lH3voww7rl8f8tEXv9j5Tsr5Zz6+RwFz3MgWBAjkLyBg5l8jMyRAgACBiQJD4bIdJGM7 mF3j2tMKHc1wyexXn3zy7qeEzInFa2weOpR9D1QKm8Ve9lrfR3n76aerv/7Wt6If6JQ+87SRAmaa m1EECOQlIGDmVQ+zIUCAAIEjBZpPC+27LLZ9iLHuZTOEht+Pffyf/8f/fs/lsp4uOyb2o8+Hh/M0 A3p7ZGyoDONCl/LHnnuu+h8/+ZPZhsr2+tyDGX+u2JIAgTwFBMw862JWBAgQIJAg0A4nP/z443v2 ktq9DDuJ6WA2D9YMmSHo3rx507+5HTWd2qXsu+y1vo/yoUceqapf/uWEs+f0Q3QwT18DMyBA4HgB /9gdb2gPBAgQIJCJQPPS2PbTYscuhe17emxzaUPdyz/4gz+ovvCFL9wj8dN/82/e/bMu5icU4T8B /uIv/uKee1Xbp89bZ2fVUxHnVC4P54mYatQmH33ve9UDn//85bYPHj55s47zJorORgQIZCQgYGZU DFMhQIAAgXSBZvey+VCfsWDZPmL7cthwX2Vs97IOmfWv/+E//Ie7QWrPXcxw2fIf/tEf3/fO0GAf LnkNH09GhMotdCmHzjcBM/3r30gCBPIREDDzqYWZECBAgMARAs3uZfvS2LDbKfdZ9oWAmPsv2yHz 2rVrd4PVXrpR4bLXP/vTP7nnPtSmaex9lFsIlO3zLoTIcE9o+Pgbf/VXl0+wDX9X/9ruYHpI1BHf FAwlQOAkAgLmSdgdlAABAgTmFqgDZvuVJH0dzGboHAqU7Q5me9t2oOz6c/3Qmi2/G3PonZR1oAx2 Q5e+1k97rS4uqpxeHxJ7rjbDZFd4DPtpB8r2vgXMWG3bESCQq4CAmWtlzIsAAQIEogWa4aZ972Uz SKZ2MWM6l+E4fWFzi/diDl32GixCqPz22fiPGSXcR9nXhQwhMnQjw6/tjzpIRp/En24oYE4Vsz0B ArkJjH/nz23G5kOAAAECBFoCfZfHDnUvm8GzDdr3tNmuoDnUwQz7DQ/+2cplss1XwLTNplz2ejk2 wy5lV5BsdxzrQNn++7HOZMwX7YPf+U51eOaZy03rh/y4RDZGzjYECOQkIGDmVA1zIUCAAIEkgTpg dr33cuzVJH1dzWYAjelgNoNmHSy7Hvbz0ssvV+fn50X8+ztHoAwWl5e+njhQDoXHdmisT8K+7mTS SdoYFILk5SXAHb8KmMfqGk+AwKkFivgH7tRIjk+AAAECeQvUATOEt+eee+5ysqndy/bYsXA51sGs 5er7MHMOmOFJvC+++GLn017DOmK7lGHbO9evVz/23HOX9xyu/THlXsgwt/blrKmXt9aB8XKfAyGy HS7D9vXfCZhrny2OR4DA3AIC5tyi9keAAAECqwvUATPm/suuADn0KpL6c+1FNd972RUy6+3DJbLh 83XAzOl1JXMFyubTXj/8yleqYLbUx5T7IVODYszcpwbImH2GbQTMWCnbESCQq4CAmWtlzIsAAQIE ogSaD/j5/g9+cE+46epijnU22wdN6WCGfdTBst5fHTDDn0/5upLg9c477wx2KccezlM/7fX2008v 0qUcCpHN0Ni8hLX593MFy2M6knXd685k+HPz930nt4AZ9WVvIwIEMhYQMDMujqkRIECAwLhA8z7B +v2X7RA5FDSnPNCnns3YZbHtcBn+fKonycYEyifPzu6+PiSEx8OdO/fBz/2019q9+Q7Ivnsh57qE tets6upENsNgOxTGhMTxs7Z7i9sffFBdeeONy096yE+qonEECJxaQMA8dQUcnwABAgSOEnj88ccP 79+6dbmPOmDWO2wGy6GH+bQn0Nx2bHJDl8fWY9cMmOGy19/97d+qLl59rXPqU++jfOiRR6rql395 jOG+z7cDZN0Jbr4f8jJIfe5zVQiZ7d93/XnyJBoDpgTJrhA5V7AMIfLStKqq+vfNXwXMY6psLAEC OQgImDlUwRwIECBAIFmgq4M5trOhrmXfPZf1Pqd2L+txzYfnzH2J7Ng7Kd/69H2UT43ATH3aa8xr PU4dIMOShx6qM3aupH6+KzyGYNn19+EYdz/35puXHWQdzFR54wgQOLWAgHnqCjg+AQIECBwl0NXB HAuQfQfsGjc0uZjuZRg/dwdz6mWvXWuIuY+y2QGuL2UNfxe6kH2Xsx5VzJHBfV3IU4TIvk5kvYR2 mIx10cGMlbIdAQK5CgiYuVbGvAgQIEAgSmBqBzPmvZh9B07tXob9HdvBbD7MqD2/lMteL7t6Ha8Q Gbo3cihUHvNgnb6H6TTXmUsXMsypKzymBsp2LQXMqC97GxEgkLGAgJlxcUyNAAECBMYF+u7BHAuS zT2377kcu0w2jI3tXtbHaT5FdnxV41vEXPY61qVshsnmE1nHj37cFs1OZNjTUHic697HsRk3L10d C5Fj+zrm8wLmMXrGEiCQg4CAmUMVzIEAAQIEkgWmdDDHQufQw32O6V6GxdUdzJdefrl6+OGHB9cb Lqm9du3aPa8SSelStt9J2b68tWsSqZ3IsVd6NI/VFSiTT4CRgX33PLaHzdWBnLKOh958swqvemn+ Gsa7B3OKom0JEMhNQMDMrSLmQ4AAAQKTBGKeIjv27sspT40Nk5vavQxj6g7muzdu3H1HZv06k65f 64D50acP6BlCuXP9+uVlm+3LXutAOQk0YuOueyHbobHr/Y9rdCPHOpFDD9qJWPrkTerwGAZ2BcoQ MJufa3cw534g1OQFGECAAIGJAgLmRDCbEyBAgEBeAkMP+WnPdKiDWb9Go2t1x3YvmwHz+z/4wWVA rT/a78wMf193MH/61q3q2x0Bs/lOynBpa7ikN3zUr/s4pkIxncg1O5D1WqZ2Ii9D26dPbT3GI2Zs X3BM+fv6HaT1U2QFzJgK2IYAgZwEBMycqmEuBAgQIDBZYOwezLDDoQ7mUk+ObS8kdDAffeyx6u23 374bMNvhstnJDB3MOmDWgbLdpZyjQznUjbwMreFhQN/5zicBtvH79vrm6k6OvsajERrXuqx1ahcy JlgGv/ry2KalgDn5W4ABBAhkJiBgZlYQ0yFAgACBaQJDl8i2w2U7TDY/39fBHOtehn10dSGbq/jd 3/6t6uLV16r68tj6c/W+u/4cAmb4+H9efvnylSDhI7weJPZjLDjW+1kjNF6GqQ8+GHz6aj2frq7j 0p3IsUAY5ja0TT33rvsp+4JkO1w2/9y8B1MHM/aMtx0BArkICJi5VMI8CBAgQCBJIKaD2bfjY+69 jAmW9XFD9/L82a9Xv/Krv3b5V0PBMnw+XPL65S9/+XLb0PHsC5Z974VMvR/y2C7k0Lshh17tcRng Tnw5692A2/PgnWYA7Pp9Vzey6+9iTnIBM0bJNgQI5CogYOZaGfMiQIAAgSiB2A5mV/cyHKDuXHa9 mmSoeznWtQz7/ou/+Ivqheefv7w09vd+7/fuHmtsYc2AGTqY4WPKqz3G9p/6+ZgAeTeodVzKmnrc mHFjl7GGfbQ7jEOhsKsbOdR1jJlj7DYCZqyU7QgQyFFAwMyxKuZEgACBmQTee++9Qwg5fR9jr8u4 evXqWdhH+HWmKR29m3o+9a9DD/kZu/cyTGYoWNaTTXlqbBhb33cZwmX90dU1bc+hHTD7wuXRmD07 iLmctdlx7Pv9UvOLeRrr2GWvMZe81vNP7UROXX99/2UY5yE/U/VsT4BALgLZ/MCQC4h5ECBAYAsC P3Z2dtjCOqau4Ycff9w5JOXey7Cj5qWszZAZPjfWwewKl1PW86/On6v+8I/++PIezPohO1PG923b FR7bl6eO/XmOefTto68TGbbv60COXbI6NSBO3X4uDwFzLkn7IUDglAIC5in1HZsAAQILCLzw3LOH 8ECZPX60A2ZXB3PqU2ObQbN+vUhMuAzj+gJvTG3qezB/75//85jNe7cZCpRjQXLJ+yKndhjDAse6 jjGXsMZc+noUeMLgZrCsh+tgJkAaQoBAFgICZhZlMAkCBAjMJ9C8ZHS+vZaxpzrQtYNlX9DsW9Ux 916++OKL1fu3bh0VLsO8Qgfz/3v1teqtf/tvJ4XH9sZLhsS+iU0Nj2PBsT5OX2dxSrDM6UzuCpYC Zk4VMhcCBFIEBMwUNWMIECCQscBeL48NJenqGDbD5Rzdy6HS1w/1+f4PfnB5b+cxH+2A2exE1vsd ejLr0sFy7KE6IfSFjylhM2w/drnrWNjsMw9hLrxPNIePoWApYOZQIXMgQOAYAQHzGD1jCRAgkKGA gPlJUfoe8DP2apKY7mXfJbL160j+9cUrR58ZzYDZDpdLh8e7AbbjlR3NS0ynBshT3dsY5tkMdWsF zZgg2XeiuET26C8hOyBA4EQCAuaJ4B2WAAECSwkImPfLHtu5HLvnMhxxzu5l2F8dMP/vf/EvljpV Ovc7teO46uSOONhY2Auhs9nlrH9fj2t/vp5Ke7sjpnjZYa2PJ2AeI2ksAQKnFBAwT6nv2AQIEFhA QMD8Ufey717M+t2Xbf6h7mXYdihohnsvw0fzlSTHlHeugNkMjH3zie3uxW7XDF/tY67RPRwLk8fU Za2xAuZa0o5DgMDcAgLm3KL2R4AAgRMKhHdDhss09/ox9NTWsUtjh8LmWLgMnw/uL738cvXcc8/N wt8MmDEhMRy0K1iNBbqhMNbsqE0NiilzaR+jvY+++QzNc5ZiLLiTvrkLmAui2zUBAosKCJiL8to5 AQIE1hXY8xNkg3QzYPa9+7KvIjH3Xg5VMwTMd2/cqJ544onLzcYeLhQeAtR16W79cKA6YH777Ef/ VPeFxbGO3VDIHBs7tOau/cbsb6n5rPvVNn60Y4JvHTDDOXX16lU/r41z24IAgUwEfMPKpBCmQYAA gWMF9vz+y9purIMZtquD3ZSgGVObEDDrp8f2XYIbs596ju2AeWwoSw2DMXM+JkgdM3Ys/MaE3Zj1 zb1NzJoFzLnV7Y8AgbUEBMy1pB2HAAECCwvs+d7LdsDs6wwuce9lfey6gxnu0zz2I4TgL3/5y9VP 37pVNTuYYb/NoDg1QMUEm2Pn3jxG1/HWmEPXGtrHjZlb7Fxjtxuz7XrIjw7mmJrPEyCQm4CAmVtF zIcAAQIJAsLlJ2hD78GM7So2L5UN+xx6sE94cuzDDz98eez6Hsx/8k/+yd0Kxlx2W++/+WvYwbVr 1zoDZsLpMXnIWEisg+7UgBszkbH7PtsPGlpiDjHzXHobHcylhe2fAIGlBATMpWTtlwABAisJCJc/ gu66BzN8dihcxoTAoVLWIbN+B+av/OqvVWGfzXDaFSLHjnvKgLnEqRvTMWwfd2pncOr2S6yzK3xP Cc31nATMpapjvwQILC0gYC4tbP8ECBBYSGDvD/TpYu3rYIZth+69HHs9SUwJw2tK3r916/I+zDpg xozr2iYE0qUDZswlo1MCXy7hrstzbG5jn6/3mWI2xbB5nAdu3778o0tkU7+KjCNA4FQCAuap5B2X AAECiQJ7fxXJEFu7gxkbLMN27Utj6+P0XSJbdy6bv77w/POXryqpL5tNLPHlsBBY2/dgpnQBw75i A9Qx822OTTlezJjU9U+ZW8zlwXM5De1HB3MNZccgQGAJAQFzCVX7JECAwEICFxcXhxBifHQLDHUw h8yO6WCGgBk+QqgMl8k++thj1Te+8Y2jS9QVMI/eqR0UIyBgFlMqEyVAoCUgYDolCBAgUIiAS2LH C1UHzOZTZLtGjd3/OHaJa7NrWYfLOmiG/wA4f/br1d/7B/+wqv+uvc3YSkJYPSZgxnQD++Ywx2Wg zX2n7G/O+Y9ZH3OssO+luqoC5ljlfJ4AgVwFBMxcK2NeBAgQaAh4kE/c6TD1Etmw166wGf5+6Omx 4fPtkFnP8J133rm8F7O+VLYZMuNW8Uk3dCxgHhOMUkJfe+5z7KMvoI0dK3ZcrPeU7Y5xj513OIZ7 MKdUxbYECOQkIGDmVA1zIUCAQEvA/ZbTTok5OphjRxzqXoax9aWy4fchZKZ+hKDa9R7Mrv2lPKU0 ZV7HhquUYx4zZo7u4jHHr8emuOlgziFvHwQInEJAwDyFumMSIEAgQsD9lhFIrU26Ophje0m5/7Kv e9k8Vt3JDJfL/szP/lz1Z3/6J2NTufv5sH1XwIwJKjHbxExkjv2k7GOtsDxmkDL3sX1O+byAOUXL tgQI5CQgYOZUDXMhQIDApwIvPPfs4eLV13hMFGh2MLuGpoTJ5n66nhw7NMX6gUwhZIaPZtAcC51/ +Ed/HN3BjGWa67LW5vFi97nU01mX2u/QGuvPxa69rz5DIVbAjD2rbUeAQG4CAmZuFTEfAgR2LyBc pp8CsZfI1kdICZwx3cvQrQwBMny0Q2bs6sJ/MFyrqurbZ2n/VKd04PrGpOwrdp1hu6W7ljHrWmKN qft0D+aUs8e2BAjkJpD2r1ZuqzAfAgQIbETglOEyvF4jfPziL/z85a91QAr3FKY8qCbso/k+yKtX ry7yb07TbMkOZmz3sg6XzV9DWAy+tW3oTobf1792nb59Hcw1OnZDX06poWnOL9FTzuHYY8eO18Gc 84yxLwIE1hRY5B/7NRfgWAQIENiSwJpPi63f17hU8FurLs3Xt4wFzDCnY15REtO9DMcYCpl1iG+G zLZVCJ/HdjDrfaZcxhkbgmJqPHVfU7fvm8Ox+4kdH7tdjFVzmzpgfnw4+FltKp7tCRA4qYBvWifl d3ACBAj8SGCtcBmC5c2bNzfz/b8dMMM7MNsfdais/74ZMmPOwa5g2dXZbV4a23yoT+gG15fK1p3i rlAZQmf9EV51MuUS2aWCzpBPSjc1dZ6p42Lqe+ptutYmYJ66Ko5PgECqwGZ+wEgFMI4AAQI5CKwR LsODZl565dXNfd+f0sE8pnsZzpOYDmZX9zImYLbPw6kB89jzeCzAjX0+9fjH3H+ZMqeUMalrO2ac gHmMnrEECJxSYHM/aJwS07EJECCQItAMSCnjx8ZsNVjW6276ff8HP+jkaHYwU7qX9U777kftCpXN icwZMGMDUtd2Uy+XjT1WGz3m2GPn7dTPzzXXqUbNeY6NjZljvY2AOfUMsD0BArkICJi5VMI8CBDY pcB77713+OqTTy6y9q0Hy66AGe7B7LpENmx7bPcy7GPsYUd9QbMZMKcUO1wi++TZWfXUwKCY0DLl mDHbLnnMJfa9xD5jnMa2GZqXgDmm5/MECOQqIGDmWhnzIkBg8wJLhcuXXn65Oj8/383396EO5hz3 XsaciGMdzLCPOd5r+tZI2IyZ69g2U8NY6iWuU46Tcq/n2DqP/fyU+accS8BMUTOGAIEcBHbzA0gO 2OZAgACBpsDc912+e+NGVfoTYVPOkCmXyKa897Ke09j9l0Mhs93BjHkyaPgPiBdffLEK92J2ffR1 N2MuT00JRzFjYrbpWssS48b2Ofb5lHMxdUzXXATMVE3jCBA4tYCAeeoKOD4BArsUmPN9lzFhZcvI sR3MY+697POL6VzWY5sdzJSajXW8Q3czfAxdTjtnuJt6Tk0JdFO2recRM6Zvm2PunZzawY2ZZ1iT gDn1DLM9AQK5CAiYuVTCPAgQ2JXAHN3LvXYs2yfKWAcz5d7LulvZPFbq/ZdhHykdzKEviBA2f/e3 f6v3stuYezd39QW34mJjA+RY2BcwVyyaQxEgMKuAgDkrp50RIEBgXGCOcJnSARufWZlbxHQwp3Yv g8RYoAzbrNnBHAucQ5fTTulujnXzwjxiQlTMNrFn3Jz7ij1m33ZrzUXAPLZSxhMgcCoBAfNU8o5L gMAuBS4uLg4vPP988toffeyx6ubNm753NwS7AubQw32+8IUv3PNE2XYxmt3Lofsu63BZB83wa+hU hr9vf8zdwRw7gYYuwV6qu5kSvGLCbNdax4419vkxvyU+P3VOAuYSVbBPAgTWEPBDyhrKjkGAAIFP BY7pXgqX3afREh3MsQf61DPJpYM59AWW+rCgOb9oU4JkbCCL3S6sJ2YeU/Y3p1F7XwLmkrr2TYDA kgIC5pK69k2AAIGGwDHhMuzGZbHxAbPrvssweqx7GbZphsuHH36491LZKeFy7Q7m0BfeWBd9jVeh lPSNISZwxmzTteahcQJmSWeJuRIg0BQQMJ0PBAgQWEFg7AmgY1MQLvuF5uxgLtG5rGd+7FNkx86R lM/P0d2M6QqmzK09JiXEpYyJnevUp8dO3a+AGStmOwIEchMQMHOriPkQILBJgWO6l54WO3xK9AXM vi5mzAkWEzSbHcywz777L+vPNe+9zfU/DJbobk4NeTHbLxXuxs6NmLmNBePYfQiYY9XweQIEchUQ MHOtjHkRILAZgWO6l+67HD8Nmg+0+f4PfnA5YOwS2a69Tg2VdaAcCpbN4+TYwRzTbYb39rYpDwuK fQ/l2Lyan48NbGOd1tj9TJlb7LZdx64Dpv9gilW0HQECuQgImLlUwjwIENiswDHdy1w7XTkVa+gp siFoho/63suhecfee1nvo93BHNp3TvdgpttVJZkAACAASURBVNZu7HLaKa9CmTKHUwW/Kcedsm3s 2gXMWCnbESCQm4CAmVtFzIcAgU0JHNO91LmIOxWmdDD79hjTvQxj61eQtLuWMV3MEjuYfV7hvP7d 3/6tqrmm5rYx3c0lQlncGVPGVgJmGXUySwIE7hcQMJ0VBAgQWFBg6H2EQ4c9f/br1UuvvOp7dERt YjqYYTdTu5h9h57y9Nh6H1voYA6VYqy7GRM4I0p9d5PSw+nY/MPnH7h9+3K9/qNpyplhWwIEchDw w0sOVTAHAgQ2K5B6eaxLY+NPiWM7mKF7WX/0vZYkJVS2V7ClDuZYdYYeFjQUNo99eE9McDvcuTM2 /Xs+P7bPvp2ljqv3p4M5qUw2JkAgIwEBM6NimAoBAtsTSAmYwuW08yDmKbKxe4y5VLYOm2Gfsfdh br2DOeQbwuY777xTvX/rVudmqYEztqZTQ+vUYDh1+655e8hPbDVtR4BACQICZglVMkcCBIoUSLk8 9qWXX67Oz899b55Q8aZzuJww5lLY5u6bHcyhw8aGyb597KmDORY4m69saW8bHhb0VGT95wh3kYc6 arOUeepgHkVuMAECJxTwQ8wJ8R2aAIFtCwy94qFv5bqX98sc86Ck0s6wvdV/7Xs35zofpnZFpx7X PZhTxWxPgEBOAgJmTtUwFwIENiUw9fJYD/PoLn8zYIb3goZLLZu/Nke1P1dflllvn/sJtreA2a7H XIFzyvs2u7Zt/l1K93HoPBt7H2c9Vgcz969W8yNAoE9AwHRuECBAYAGBlK7b3sNFXxlqy+//4AfV Zz7zmdFqffjhh73bhPdixlxCO3TZbP2qkq6D/OEf/XH1i7/w81X4deyjGYbrbZ0D96oNXWY+95Np x+rV+4PUlStV34ODjgmnAmZqRYwjQODUAgLmqSvg+AQIbFJg6v2X7r3sPw3GAmYIlCF49gXLECrr j6Fw2RcqY+69jA2WXaGyuXIBc/g8ePHFF5MeFlTiNxkBs8SqmTMBAkFAwHQeECBAYAGBqZfHChbH Bcy+0XXHMny+GTSb28c+OXboNGkGzLEOZt9lvGH/zoP4L8ZTdDeP6UhOGesezPjzwJYECOQnIGDm VxMzIkBgAwJTAqbu5XDBhzqYfd3L5qWwza5lXwczpnsZXjXSvjx2rmBZCwiY6V/8Qw/VGrucdkr4 65vhHPto7lsHM/1cMJIAgdMKCJin9Xd0AgQ2KDD1/kuhYvgkCO9RDK+16LsHc+jS2DpQDgXLhx9+ uOrrYqZcHtvVway7lmGlOpjLf9HHvHvz22fH/Qg0R6Ac2oeAufx54ggECCwjcNx312XmZK8ECBAo WmDq60kEzLSAOXTvZV8Hs+tIQ5fINgNmVwcz7C+1i9k1F+fC/F/6cz2Zdv6Zde+xDp0C5lrijkOA wNwCAubcovZHgMDuBaZcHnv+7Nerl1551ffigbMmtYNZ77K+93LuB/xMCZZ9ncv261MEzOW/faR0 N1O7lWOvQBlabR0wnRPLnxOOQIDAvAJ+qJnX094IECBQTQmYfngcP2H6AmbMpbFh7zHBsr5Mtjmb mMtjw/YxQXPostjmMZ0P4+fDnFuE7ubv/vZvVRevvta527F7N8Og1PDZPGDXPgTMOSttXwQIrCkg YK6p7VgECGxeoA5DsQsVKMalatMffvzx+MafbjH26pKwWd9TZcPnlnwPpnswo8u4+oY5XU4rYK5e fgckQGAmAQFzJki7IUCAQBCYcv9luDzy5s2bvg+PnDp1wAxec3w0H7jT3F9X8Gtfwhq27/q7OeYV 9uGJwnNJzrOfU7wKpZ65gDlPDe2FAIH1Bfxgs765IxIgsGGBKZfHvnvjRnX16lXfh3vOh7Fu0oZP o8uluT83vwof8yqUqasRMKeK2Z4AgVwE/GCTSyXMgwCBTQhMCZguj+0u+ZQu8CZOmohF+M+ICKSV Nxl7WNBbZ2fVU0fMScA8As9QAgROKiBgnpTfwQkQ2JKA91+mV3PqvavpRyp7pMuq863fHN3N5sN+ BMx8a21mBAgMCwiYzhACBAjMJDCl8+byx0/QBcu0k0/QTHNba9TY5d3h6bTfPhv+EUzAXKtajkOA wNwCAubcovZHgMBuBVweG196wTLeamhLQXMexyX3EhM2n+y4nFbAXLIq9k2AwJICAuaSuvZNgMCu BATM8XJP6fKO780WTQH39JZxPsQEztDdFDDLqKdZEiBwv4CA6awgQIDATAICZj+kjuVMJ9nIbnQ0 13Ge8yhDr0IJx/EfB3Nq2xcBAmsICJhrKDsGAQKbF5jygJ+93X85JXh3nSjhfrVwCeG1K1c2fx4d 7typ/tnhUL195Eq9T/NIwBMN7+puCpgnKobDEiCQLCBgJtMZSIAAgR8JjHUhmlZ7eeXEsZfDhtc8 7CFUdn0dhaAZPo4Nm8JJ2d+lwveVl1551c9qZZfR7AnsTsA3rd2V3IIJEFhCYEqXbus/9B9zOWz9 dM3wuoa9f4SQWTu8fedO9bXDIYnEZbNJbAYRIECAQKKAgJkIZxgBAgSaAgJmVU25TLh99jSDZTNY Ocuqqu5mhrB5TNDc26XZzh0CBAgQOI2AgHkad0clQGBjAnsPmFMuEW6Wfu2OZR3Wjjn9cuiuHhM0 t95BP6a2xhIgQIDA8QIC5vGG9kCAwM4FpoSrLV6uOCVc16dKCJZvPfTQomdOX5hMDYhj4TR1v8cg pAZNDwE6Rt1YAgQIEBgSEDCdHwQIEDhSYMrDbLb0gJ/US2I/Oju7e2/hkfSXw4eCXwh9t59++nK7 hx55pProi1+875D/4yd/8u7ffeYzn6k+/PDDu3/+G3/1V/ds/+B3vnP3z7c/+KB66M03R48/xxrH 9vG127cnP3nWJbNjqj5PgAABAikCAmaKmjEECBBoCEzp4G3l8sQpobqmqp8KO8c9ll2h8s7163eD 5Idf+UoVwmL9UYfGZmAMwbIdINsndh0+m/vqO/k/+t73qhBA6+BZh9+1OpuprzjZyjnpmxIBAgQI 5CEgYOZRB7MgQKBggb0FzCnrDWWd63LYdqisu5M/9txzl2dPsxM5Fhz7gmT4+7qLGX4NoTF81J3L EB6bH6GDWX/UndK+zy99itc+71TV5CfOCplLV8f+CRAgsB8BAXM/tbZSAgQWEpgSuEr+QT7lkthj L4dth8rQpawvdX3wc5+7eznrlEBZdy7Dr3Vnsu4+hlOkDpHNy1+bXcjmnOq/H7tMd6FTb3C3Uy+b 3eL9wadwd0wCBAjsXUDA3PsZYP0ECBwtEBswS77nbeq7LevLYVNx7wtxFxeX90/WXcqYQNm8BLb+ fR1K6/Efv/LK3fsoQ1jsCopdl7h2XeY790OFUu2a46Y+BEjInEPdPggQILBvAQFz3/W3egIEjhSY 8gTZUgPm1M5larhsh8pwyWm4/HVKqAzlbN9bWXcqQ5cyBMrwUXcnxzqQKfdPNt9beeTpNcvwqfdm CpmzsNsJAQIEdisgYO629BZOgMAcAlMedlPiE2SnBOhj7rVshrIQLP/6W9+6LM9Qp7KrQ1nXtBlK L++fPD+/7E62u5QpAXKO8+YU+5jazSz5cu5T+DomAQIECHwiIGA6EwgQIHCEQOzlseEQpf3APuWy 2JRwed9De15//e5lsDGXwLbLds8TX3/91+8+zTX2stcjToPsh9bhWsjMvlQmSIAAgeIFBMziS2gB BAicUmCrAXNKZzblQT73XEZ6cVGF14qEj65gOdSpDGPal8BeeeON+06JPXUqY74epjwAqLT/GIlZ v20IECBAYDkBAXM5W3smQGAHAlsMmEuGy/alsPU9lsd0LMPY5sN6wmlXXworWPZ/EQqZO/gGZYkE CBA4gYCAeQJ0hyRAYBsCU+5PLOXBKVPC5Q8feujufY1jFW1ephpeNTIWLNsP6qk7lSFMdnUsm/dW CpVj1fjR54XMeCtbEiBAgECcgIAZ52QrAgQI3CcwJYyV8ATZ2MA89X7Le17p8enlsCkdy2bIDB3L 5qWwQmX6F+iU+zJdLpvubCQBAgT2IiBg7qXS1kmAwOwCW7o8dolwOaVr2dWxbHctw58/8xu/cfeJ sOHPguXxp3Wo0ztVVX3tcIjamZAZxWQjAgQI7FZAwNxt6S2cAIFjBbYSMGOfFjulc3lM17LrPZah 4/ngl750eUlu/SFcHnsG3zs+tpNZyuXe8+rYGwECBAjECgiYsVK2I0CAQEtgCwHzvffeO3z1ySdH azs1XNbdxY9+//cv75nsuyR27HNhPz/xzW9eXg4rUI6W6egNhMyjCe2AAAECuxcQMHd/CgAgQCBV oPSAuVS4DEHw9tNPV3/9rW/1Bssx8zp4NruWAuaY2jyfD13iByMul9XJnMfbXggQILA1AQFzaxW1 HgIEVhGIvWexnkyO963FBOTYzmXXey2ndi1DqKw/wr2Wh2eeuftn4XKV0/ryIFPuyXz3xo3q6tWr fpZYrzyORIAAgewF/KOQfYlMkACBHAWmPEE2x05PTLgM7uFVJGMfzfstxy6JHdpX3bVsPiFWsBzT X+7zsZfL5vifJ8up2DMBAgQIjAkImGNCPk+AAIEOgdiAFobm9oqS2O5rbLgMawxBcChcjr3XMuzD g3zy+1KLfU+mkJlf7cyIAAECpxIQME8l77gECBQtMCVg5nYZYczcx8LllFeQjBU6hM/6ktgQVO95 Au3YYJ9fXCAmZOb2nyiLozgAAQIECPQKCJhODgIECCQIxIS0erc5dXdi5v3R2dnoE1vrgHnn+vXJ D/Opu5n1PZf1U2LrTmhCOQxZWCAmZL708svV+fm5nysWroXdEyBAIHcB/xDkXiHzI0AgO4HYp6/m FjBj7ht96+ysunblyuXU+zqJU8Nl36tIwt8Ll9md3r0TeuD27dHJ5vSfKaOTtQEBAgQILCIgYC7C aqcECGxZ4OLi4vDC889HLzGHH7pj7ruMeWLs1HDZRmp2L4XL6FMoiw099CeLMpgEAQIEshcQMLMv kQkSIJCbQExYa845h4AZc2ls7H2X4bLYH3vuucGy9HUtwyCdy9zO6Pj5xFwq637MeE9bEiBAYIsC AuYWq2pNBAgsKhAT1uoJ5PCKkpj5jt13eUznstm1bD8p1mtIFj1VF9l5TMjM4T9VFlm8nRIgQIDA qICAOUpkAwIECNwrEBPYcgmYMfeLNu+77Kp1+z2XfefDUNcyjNG53MZXUjgfHjwcBheTw3+sbEPb KggQIFCegIBZXs3MmACBEwtMCZinfkXJ2Fxj77vse8/l2KWwoWPpabEnPmEXOHxMyPRU2QXg7ZIA AQIFCAiYBRTJFAkQyEtgLLQ1Z3vKSwVj7hUduu+y+a7LH373u5dBMQTGqR86l1PF8t6+7mi7VDbv OpkdAQIETiUgYJ5K3nEJEChSIOaS01wC5lgQjrk0Nqzl7PXXqw+/8pXecPmb//2/V7/0t/7W3WU3 g2j4/Wd+4zeqwzPPXH7ePZdFnva9kx57dYku5rbqbTUECBCIERAwY5RsQ4AAgU8FYt4lmUPAHAuX Y5fGHvNQn3r9wuX2v2xiXl1y6svEt18FKyRAgEBeAgJmXvUwGwIEMhcoIWDGdFljnhobuo0f/vmf 93Yu25fMtjuX4XLaBz7/+bsV1b3M/OROnN7YpbIe+JMIaxgBAgQKFRAwCy2caRMgcBqBsc5ge1an uAdzbI4xl8b2PdQnRr0Omg9+6UtV8wm0MWNtU5ZA3ekee6qsLmZZdTVbAgQIHCMgYB6jZywBArsT GAtvpw6YMd3L+sE+XeGvDgxj910219nVyfyJb36zuvLGG+653MlXiC7mTgptmQQIEIgQEDAjkGxC gACBWmBKwDzFpYFj8xvqXk6577LvibLuu9zv18rYA39O0c3fbzWsnAABAqcTEDBPZ+/IBAgUJhDT HWwu6fzZr1cvvfLqat9nx+YX+2Cfoe7lULAMaw/3Xbo0trATe6bpjj3w5xT/4TLT0uyGAAECBCYI rPaDz4Q52ZQAAQJZClxcXBxeeP756Lmt/YqGse7l0IN9pnQv+wAuu5ef/az7LqPPkO1tqIu5vZpa EQECBKYKCJhTxWxPgMBuBXJ+guyU7mXfvZf1g31iC9z3vktPi40V3N52upjbq6kVESBAYKqAgDlV zPYECOxWYKxD2IZZ856zsbnFdC9TLo0Na66DZv1KEgFzt18ilwvXxdx3/a2eAAECAqZzgAABApEC YyHuVAFzrHsZ5hWeHDv01Ng7169Xf/2tb/W+87K5Nk+NjTxhdrqZLuZOC2/ZBAgQ+FRAwHQqECBA IFIg14A5Nq+YJ8f2dS/7HupTk3lqbOTJs7PNdDF3VnDLJUCAQENAwHQ6ECBAIFJgLMidqoM5NK+Y J8fGdC+7gmb4u/DhwT6RJ9CONhvrYq79hOUd0VsqAQIETi4gYJ68BCZAgEAJAi889+zh4tXXoqe6 1g/QY5fHxnQvf/jd7969j3JsgX0P9gnj3Hs5prevz+ti7qveVkuAAIFaQMB0LhAgQCBCYOoTZNd6 RclYVzXce9n3Ud+T+eGf//l9917GXBrrnZcRJ86ON/na7dvV2wPrX/MhWDsug6UTIEBgdQEBc3Vy ByRAoESBsSDXXtO7N25UV69eXfx7bOrlsXcf+HNxUX34la9EPdynuUb3XpZ4Fq8753COPXg49B50 rS7/uqt2NAIECBBY/IcfxAQIENiCwNSAuUZ3Zuyy3bFXk4RLWru6l+0gGTqV4aPZ1Xzwc5+rqh// 8c4n026h3tYwj4DLZOdxtBcCBAiUJCBgllQtcyVA4GQCOQbMsTn1BcyY7uXQJbK6lyc7DYs6cDjP 3qmq6msDXcy1Ov1FwZksAQIEChcQMAsvoOkTILCOwFiYa89ijQ7m0JzGHu7T1b0cu++y2cV88Etf 0r1c59Qr+ihjl8k++thj1c2bN/0sUnSVTZ4AAQL3Cvim7owgQIDAiMDYk1q7hi8dMMceOjT0cJ8w 39tPP1399be+FXXvpSfH+hJJFQgB858dDh72kwpoHAECBAoUEDALLJopEyCwrsDUgLlGV+aYh/sE vbPXX68++uIX74Mc62K693Ldc28LRxt7J+bS/xmzBUNrIECAQEkCAmZJ1TJXAgROIjD2MJ32pNZ4 RUnq5bH1XNsP9+kLlu3upVeTnOQULP6gQw/7WeM/ZIoHtAACBAgUJCBgFlQsUyVA4DQCud1/eXFx cXjh+ed7MUafHvv666OvJukKnOHvfuKb36yuvPFGFe7h9EEgVmDonZgCZqyi7QgQIFCGgIBZRp3M kgCBEwpMCZhr/LCcev9luB8ufPzwu9+9R3Pssth648unx372sx7uc8JzscRDxzxN1mWyJVbWnAkQ INAtIGA6MwgQIDAiEO7BfPHFF6OcvvGNb1RXr15d9HvrMfdf3rl+ffDhPu2wWf+5/vWBz39e9zLq TLBRU2DsPkyvK3G+ECBAYDsCi/4QtB0mKyFAgEA+Aqn3X4ZOUjNgxnYuw8rDw31u/6//q8tj8zkN ipvJ0H2Y589+vXrplVf9TFJcVU2YAAEC9wv4Zu6sIECAQEECYw8c6ns9SfPy2L77K8MDfMJHX/D0 7suCTpQMpzp0H2aYrstkMyyaKREgQCBBQMBMQDOEAAECpxI49v7Lv/z+96Pefdlc3+WrSX7916vD M89c/rUH/Jyq+uUe132Y5dbOzAkQIDBVQMCcKmZ7AgQInFBg7vsvu15DUi+vef+lp8eesOgbOXQI mQ8eDr2rcR/mRgptGQQI7F5AwNz9KQCAAIGSBFLuv6wvjz2LeD1Jl8VlB/PHf9zTY0s6UTKdq/sw My2MaREgQGBGAQFzRky7IkCAwNICQwEz5v7L5vxi78UMAfPwwAMujV26uDvYv/dh7qDIlkiAwO4F BMzdnwIACBAoRSC8LuWrTz7ZO92+gFkP+Oj3f7/3AT5jT5T1epJSzpK85+lBP3nXx+wIECAwh4CA OYeifRAgQGAFgYuLi8MLzz8/OWC2X08SdjAUKJuf83qSFQq7o0OMvQ/Tk2R3dDJYKgECmxUQMDdb WgsjQGBrAikP+Knvv2y+/7LPpSt0uv9ya2fRadcjYJ7W39EJECCwhoCAuYayYxAgQGAGgZQH/ITD dnUwh7qYIVR+9L3v3e1yev/lDMWzi0uBsSfJ6mA6UQgQIFC+gIBZfg2tgACBnQikBsxLnouL6sOv fKX3HZhDl8yG+y/Dh/df7uREW3iZQ0+S9aqShfHtngABAisICJgrIDsEAQIE5hAYCpgfnZ11BsD6 Etkffve790yhL1DW3cuw8eXlsb/+69XhmWeEyzkKaB+XAkMB86WXX67Oz8/9bOJcIUCAQMECvokX XDxTJ0BgXwLHBMyzH/7w8rLXro9mqGx+PoTQz/zGbwiY+zrNFl/t0JNkz5/9evXSK6/62WTxKjgA AQIElhPwTXw5W3smQIDAbALHPEH28tLW//k/7wbMsVeS1JP2BNnZymdHDQGvKnE6ECBAYNsCAua2 62t1BAhsROCF5549XLz6Wu9qxt6B2QyY9U7aryOpO5x1R9MTZDdy8mS2DE+SzawgpkOAAIGZBQTM mUHtjgABAksIDAXMa1VVvfXQQ72Hvf3009VD/+7f9V4i2zcwBMzDAw+4/3KJgu54nwLmjotv6QQI 7EJAwNxFmS2SAIHSBVICZvMdmM2A2X6QT/PezPbnBMzSz5z85i9g5lcTMyJAgMCcAgLmnJr2RYAA gYUEHn/88cP7t2517n2og1m/A3Osg9l+0E/95/CKEq8nWaioO92tgLnTwls2AQK7ERAwd1NqCyVA oGSB1IAZ1hwukf3rb33rnndgjj3oxxNkSz5b8p173VV/8HDoneTHh4OfTfItoZkRIEBgVMA38VEi GxAgQOD0AkOvKBnrYJ69/nr14Ve+ck/ADCsau1TWOzBPX/ctzkAHc4tVtSYCBAj8SEDAdDYQIECg AIGUgFl3i9oBs++9l02G0MH8iW9+s7ryxhsukS3g/ChpiuG81MEsqWLmSoAAgWkCAuY0L1sTIEDg JAJDAfOts7PqWnjXZcdH+GG+r4PZ7mI2h3sH5knKvIuDCpi7KLNFEiCwYwEBc8fFt3QCBMoRSA2Y lyu8uOi8RLYdKJtPk728B/Ozn60uA2pPeC1Hz0xzEXAPZi6VMA8CBAgsJyBgLmdrzwQIEJhNIDVg tjuY9cN9Yi6TffBLXxIwZ6ugHdUCOpjOBQIECGxbQMDcdn2tjgCBjQikBszYDmYXk4C5kZMno2Xo YGZUDFMhQIDAQgIC5kKwdkuAAIE5BVIDZtc9mDHdyzB3AXPOCtqXDqZzgAABAvsQEDD3UWerJECg cIHUgJnawXzwO9+pDs88czncPZiFnzwZTV8HM6NimAoBAgQWEhAwF4K1WwIECMwpkBowx54i2zfH OmAKl3NW0b6CgHswnQcECBDYtoCAue36Wh0BAhsRSA2Y7Q5m9OWxn3YwBcyNnECZLEMHM5NCmAYB AgQWFBAwF8S1awIECMwlkBowUzuYH7/ySnXljTdcHjtXAe3nroAOppOBAAEC2xYQMLddX6sjQGAj AqkBs93BjOUQMGOlbDdFQAdzipZtCRAgUKaAgFlm3cyaAIGdCaQGzNQOpifI7uwEW3G5OpgrYjsU AQIETiAgYJ4A3SEJECAwVSA1YNYdzI+++MXoQ/6Pn/zJ6jOf/ezlw1jcgxnNZsMIAR3MCCSbECBA oHABAbPwApo+AQL7EEgNmHUHcyxghqfGNrd54POfv4QVMPdxfq25Sh3MNbUdiwABAusLCJjrmzsi AQIEJgukBsyUDmYYI2BOLpEBEQI6mBFINiFAgEDhAgJm4QU0fQIE9iGQGjCHOph117Lr18Mzz+hg 7uPUWn2VOpirkzsgAQIEVhUQMFfldjACBAikCaQGzJQOZgicAmZanYwaFtDBdIYQIEBg+wIC5vZr bIUECGxAIDVg9nUwm/dctjuY9StKApt7MDdw8mS2BB3MzApiOgQIEJhZQMCcGdTuCBAgsITA3AGz nmP74T7h770Dc4kK2mcQ0MF0HhAgQGD7AgLm9mtshQQIbEBgzoDZDpV9HUzdyw2cOBkuQQczw6KY EgECBGYUEDBnxLQrAgQILCUwZ8Ds6l7ec8nsl77kHZhLFXLn+9XB3PkJYPkECOxCQMDcRZktkgCB 0gXmCph93cvgc7eTKWCWfrpkPX8dzKzLY3IECBA4WkDAPJrQDggQILC8wFwBs929bF8eexk0Bczl C7rTI+hg7rTwlk2AwK4EBMxdldtiCRAoVWCugDn09NjwucuP83OXyJZ6ohQwbx3MAopkigQIEDhC QMA8As9QAgQIrCUwV8Ac62DW78D0gJ+1Kruv4+hg7qveVkuAwD4FBMx91t2qCRAoTGCugNl5Sex3 vlN99MUvXt6DGT4Ozzzj/ZeFnR8lTVcHs6RqmSsBAgSmCwiY082MIECAwOoCcwTMdrisA2UdLutf BczVy7ubA+pg7qbUFkqAwI4FBMwdF9/SCRAoR2COgFmvdihofvzKK9WVN97QwSzn1ChupjqYxZXM hAkQIDBJQMCcxGVjAgQInEZgroDZFS6bHUwB8zT13ctRdTD3UmnrJEBgzwIC5p6rb+0ECBQjMEfA HAqXAeIyaHpFSTHnRKkT1cEstXLmTYAAgTgBATPOyVYECBA4qUBKwKy7RWevv373IT7t+y3vBsv6 QT8C5knrvPWD62BuvcLWR4AAgaoSMJ0FBAgQKEAgJWCGZYUf6LsCZjNY1svXwSzgRNjAFHUwN1BE SyBAgMCAgIDp9CBAgEABAikBs6uD2Q6W93U0z88/CaVXrhSgYoqlCehgllYx8yVAgMB0AQFzupkR BAgQWF0gJWAOdTCb7728ZzEC5uq13dsBdTD3VnHrJUBgbwIC5t4qbr0ECBQpkBIwh+7BbCI0w2Z4 B2b40MEs8jTJftI6mNmXyAQJECBw9ADNyQAAIABJREFUtICAeTShHRAgQGB5gZSA2dXBDH/X96Af ryhZvo6O8Ml9wQ8eDr0UHx8OfjZxohAgQKBgAd/ECy6eqRMgsB+BlIAZ28GsFW9/8EF15Y03dC/3 c1qtvlIdzNXJHZAAAQKrCwiYq5M7IAECBKYLpATMvg5ms4tZzyR0NXUwp9fFiOkCOpjTzYwgQIBA SQICZknVMlcCBHYrkBIwYzqY9zzsxwN+dnt+rbVwHcy1pB2HAAECpxMQME9n78gECBCIFkgJmEMd zPaBvQMzuhQ2PFJAB/NIQMMJECCQuYCAmXmBTI8AAQJBICVg9r0HM+yv8zUlOphOtoUFdDAXBrZ7 AgQIZCAgYGZQBFMgQIDAmMCxAbPef+/7L8MG5+dj0/B5AkcL6GAeTWgHBAgQyFpAwMy6PCZHgACB TwSODZiDwfJT5PAOTO+/dMYtKaCDuaSufRMgQCAPAQEzjzqYBQECBAYFUgJm2GH4gf7s9dfjdDfa wQwG78QJ2GoBgaeq6p7/uNDBXADZLgkQIJCRgICZUTFMhQABAn0CKQFz6B7M9nG2+g7MOlx+7XBw cp1I4KOzs7sBUwfzREVwWAIECKwoIGCuiO1QBAgQSBVICZh1B/PO9evVQ488MnjorQbMsOi379yp BMzUM+/4cc2AWZ+TDw4E/o8PBz+bHM9uDwQIEDiZgG/iJ6N3YAIECMQLpAbMcITbTz8tYH4aaM6f /Xr1K7/6a9Uf/MEfxOPbcpLAF77wheratWvV+7duXY7TwZzEZ2MCBAgULyBgFl9CCyBAYA8CSwfM 8ATZy/s1r1zZHGezg/nujRvVE088sbk15ragL3/5y50BM8zTPZi5Vct8CBAgMK+AgDmvp70RIEBg EQEBM51VwEy3Sx3ZFzDdg5kqahwBAgTKERAwy6mVmRIgsGMBATO9+AJmul3qSB3MVDnjCBAgUL6A gFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBgyYAZniD70JtvugdzB+fR WkvUwVxL2nEIECCQn4CAmV9NzIgAAQL3CcwdMC9D5aevLqlfURIO6iE/Tr45BHQw51C0DwIECJQp IGCWWTezJkBgZwJzB8wm35bfgRnW6RLZ9b9YdDDXN3dEAgQI5CIgYOZSCfMgQIDAgMBcAbPduQxd TAHTqTe3gA7m3KL2R4AAgXIEBMxyamWmBAjsWCA1YIbXQty5fv3u5bA1Ydclslu8PFYH8zRfNDqY p3F3VAIECOQgIGDmUAVzIECAwIhAasAMu7399NP33G9Z33t595Dn55t9wI+AeZovLR3M07g7KgEC BHIQEDBzqII5ECBAYKGAGdPBrARM59/MAjqYM4PaHQECBAoSEDALKpapEiCwX4G5O5jNS2QFzP2e V0utXAdzKVn7JUCAQP4CAmb+NTJDAgQIVKkBM6aDeXjmmUth92A60eYS0MGcS9J+CBAgUJ6AgFle zcyYAIEdCqQGzEBV34NZdy3bv4aAudVwGdbvNSXrf8HoYK5v7ogECBDIRUDAzKUS5kGAAIEBgdSA 2e5g3nNp7KfHEzCdenML6GDOLWp/BAgQKEdAwCynVmZKgMCOBVID5lgHM3z+yhtv6GDu+NxaYuk6 mEuo2icBAgTKEBAwy6iTWRIgsHOB1IDZ18FsXiYrYO785Fpg+TqYC6DaJQECBAoREDALKZRpEiCw b4HUgNnsYF7+/oMPLt+JKWDu+3xaevU6mEsL2z8BAgTyFRAw862NmREgQOCuQGrAbHYw2+Hycucb fwdmWKKH/Kz/haSDub65IxIgQCAXAQEzl0qYBwECBAYE5giYXR1MAdNpt4SADuYSqvZJgACBMgQE zDLqZJYECOxcYI6A2fmakjffrEKX02tKdn6Czbx8HcyZQe2OAAECBQkImAUVy1QJENivwNwBM0iG ezF1MPd7Ti25ch3MJXXtmwABAnkLCJh518fsCBAgcCkwd8C8+6AfHUxn2AICOpgLoNolAQIEChEQ MAsplGkSILBvgbkCZt25DJfLho/wipLw4RLZfZ9fc69eB3NuUfsjQIBAOQICZjm1MlMCBHYsMFfA rDuXNeVDb765eVVPkV2/xDqY65s7IgECBHIREDBzqYR5ECBAYEBgroB5N1h++i7M0MHccvcyrFfA XP9LSwdzfXNHJECAQC4CAmYulTAPAgQIrBAw2x1MAdNpt4SADuYSqvZJgACBMgQEzDLqZJYECOxc YIkO5h6eIKuDeZovHB3M07g7KgECBHIQEDBzqII5ECBAYETg2IBZ777ZwQz3X279HZgC5mm+tHQw T+PuqAQIEMhBQMDMoQrmQIAAgQUCZgiP4ePO9euX77ysnxwb/m4v78AUME/zpaWDeRp3RyVAgEAO AgJmDlUwBwIECCwQMMMuQ8gMAbP9EQLm4ZlnNv+AHwHzNF9aOpincXdUAgQI5CAgYOZQBXMgQIDA AgGz2cGsu5bNLuYeHvAjYJ7mS0sH8zTujkqAAIEcBATMHKpgDgQIEFggYA51MC8D5w7egSlgnuZL SwfzNO6OSoAAgRwEBMwcqmAOBAgQWCBgDt2DKWA65ZYU0MFcUte+CRAgkLeAgJl3fcyOAAEClwJz PUX2Mlh++sCfcIls+Di7cmXTym/fuVN97XC4XOO7N25UTzzxxKbXm8PidDBzqII5ECBA4DQCAuZp 3B2VAAECkwRSAmb7Hsz2Ad2DOakENp4goIM5AcumBAgQ2JiAgLmxgloOAQLbFEgJmEGi9ymyO3kH ZjDQwVz/a0IHc31zRyRAgEAuAgJmLpUwDwIECAwIpATMvg7mnt6BKWCe5stKB/M07o5KgACBHAQE zByqYA4ECBAYEUgJmDqYn6DqYK7/5aWDub65IxIgQCAXAQEzl0qYBwECBBbsYNYP9qkPEV5REjqc W3/Aj4B5mi8rHczTuDsqAQIEchAQMHOogjkQIEBggQ7m0EN+BEyn3JICOphL6to3AQIE8hYQMPOu j9kRIEDgUiDlElkB85OTxyWy638R6WCub+6IBAgQyEVAwMylEuZBgACBAYGUgBl2136KbH2pbOhg 7uVDwFy/0jqY65s7IgECBHIREDBzqYR5ECBAYOaAOdTB3Ms7MHUwT/NlpYN5GndHJUCAQA4CAmYO VTAHAgQIjAjM1cGsDyNgOuWWFNDBXFLXvgkQIJC3gICZd33MjgABApcCKQGzq4O5t3dg6mCe5gtI B/M07o5KgACBHAQEzByqYA4ECBBYsYO5pyfICpin+dLSwTyNu6MSIEAgBwEBM4cqmAMBAgQWCJg6 mJ+gesjP+l9eOpjrmzsiAQIEchEQMHOphHkQIEBgQCDlEtmwu66nyB6eeebySGdXruzCXMBcv8w6 mOubOyIBAgRyERAwc6mEeRAgQGDmgNn3FNnwgB8B0+m2pIAO5pK69k2AAIG8BQTMvOtjdgQIELgU mKuDGfa1p3dghvXqYK7/RaSDub65IxIgQCAXAQEzl0qYBwECBBbsYIanx97+4IPLI+zpFSUC5mm+ rHQwT+PuqAQIEMhBQMDMoQrmQIAAgRGBOTuYAuYTzreFBXQwFwa2ewIECGQsIGBmXBxTI0CAQC2Q EjCH7sHcywN+dDBP8zWkg3kad0clQIBADgICZg5VMAcCBAjoYC52DrgHczHa3h3rYK5v7ogECBDI RUDAzKUS5kGAAIEBgWM7mPU9mOEBP6GzqYPpdFtSQAdzSV37JkCAQN4CAmbe9TE7AgQIXAocGzBr RgHzRvXEE+7BXPrLSgdzaWH7J0CAQL4CAma+tTEzAgQI3BUQMNNPBpfIptuljtTBTJUzjgABAuUL CJjl19AKCBDYgUBKwAws4XLYO9ev3xXSwdTBXOPLRQdzDWXHIECAQJ4CAmaedTErAgQI3COQEjCb T5F1D+bh0vPdGwLmGl9aOphrKDsGAQIE8hQQMPOsi1kRIEDg6IDZ18HcG61LZNevuA7m+uaOSIAA gVwEBMxcKmEeBAgQGBA4toNZ7/rKG2/s6gmyYd0C5vpfWjqY65s7IgECBHIREDBzqYR5ECBAYOaA 2dXBFDBdIrvGF5oO5hrKjkGAAIE8BQTMPOtiVgQIELhHQAcz/YTQwUy3Sx2pg5kqZxwBAgTKFxAw y6+hFRAgsAOBlIDZ7GCGh/xU5+eXT5U9u3JlB2I/WqKAuX65dTDXN3dEAgQI5CIgYOZSCfMgQIDA gEBKwGw/RVbA9BTZtb7IdDDXknYcAgQI5CcgYOZXEzMiQIDAfQIpAVMH8xNGHcz1v6B0MNc3d0QC BAjkIiBg5lIJ8yBAgMCCHcyw64fefNMlst6DucrXmQ7mKswOQoAAgSwFBMwsy2JSBAgQuFfg2A6m gHm4BH1XwFzlS0sHcxVmByFAgECWAgJmlmUxKQIECBwfMJv3YAqYAuaaX1M6mGtqOxYBAgTyEhAw 86qH2RAgQKBTYI4O5h7fgRkw3YO5/heVDub65o5IgACBXAQEzFwqYR4ECBAYEEgJmO0OpoDpEtm1 vsh0MNeSdhwCBAjkJyBg5lcTMyJAgMB9AikBM+wkhMw7169f7k/AFDDX+tLSwVxL2nEIECCQn4CA mV9NzIgAAQKzB8y9PkE2QLpEdv0vKB3M9c0dkQABArkICJi5VMI8CBAgMCBwbAdTwPSQnzW/wHQw 19R2LAIECOQlIGDmVQ+zIUCAQKeAgJl+YuhgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5k ATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4Ejg2Ye33ATzg1BMz1v0B0MNc3d0QCBAjkIiBg5lIJ 8yBAgMCAgICZfnoImOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiCQEjCb78HUwfSQnzW/THQw19R2 LAIECOQlIGDmVQ+zIUCAQKdASsAMO6rfgylgCphrfmnpYK6p7VgECBDIS0DAzKseZkOAAIFZA2a9 sxA0z65c2aWuS2TXL7sO5vrmjkiAAIFcBATMXCphHgQIEBgQSO1gCpge8nOKLywdzFOoOyYBAgTy EBAw86iDWRAgQGBQ4NiAuWdeHcz1q6+Dub65IxIgQCAXAQEzl0qYBwECBBbsYO4ZV8Bcv/o6mOub OyIBAgRyERAwc6mEeRAgQEDAXOQcEDAXYR3cqQ7m+uaOSIAAgVwEBMxcKmEeBAgQEDAXOQcEzEVY kwJmGBQeOPXg4ZOn+nZ9fHw4+Nlk/ZI5IgECBGYT8E18Nko7IkCAwHIC7sFMtxUw0+1SR+pgpsoZ R4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzrSPZipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptul jtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0 IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAg sAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM 8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEg QKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQO ZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nA TLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQvoCAWX4NrYAAgR0I CJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNVzjgCBAiULyBgll9D KyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbbpY7UwUyVM44AAQLl CwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM9CILmOl2qSN1MFPl jCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQILADAQEzvcgCZrpd 6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUEzPJraAUECOxAQMBM L7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYRIECgfAEBs/waWgEB AjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWkDmaqnHEECBAoX0DA LL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwB AgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfq YKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZfQysgQGAHAgJmepEF zHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC5QsImOXX0AoIENiB gICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1 tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6XepIHcxUOeMIECBQ voCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDATC+ygJlulzpSBzNV zjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloBAQI7EBAw04ssYKbb pY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9AwCy/hlZAgMAOBATM 9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmccAQIEyhcQMMuvoRUQ ILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH6mCmyhlHgACB8gUE zPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqRBcx0u9SROpipcsYR IECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDYgYCAmV5kATPdLnWk DmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5NbQCAgR2ICBgphdZ wEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAgUL6AgFl+Da2AAIEd CAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6UgczVc44AgQIlC8gYJZf QysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm26WO1MFMlTOOAAEC 5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQEzPQiC5jpdqkjdTBT 5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EVECCwAwEBM73IAma6 XepIHcxUOeMIECBQvoCAWX4NrYAAgR0ICJjpRRYw0+1SR+pgpsoZR4AAgfIFBMzya2gFBAjsQEDA TC+ygJlulzpSBzNVzjgCBAiULyBgll9DKyBAYAcCAmZ6kQXMdLvUkTqYqXLGESBAoHwBAbP8GloB AQI7EBAw04ssYKbbpY7UwUyVM44AAQLlCwiY5dfQCggQ2IGAgJleZAEz3S51pA5mqpxxBAgQKF9A wCy/hlZAgMAOBATM9CILmOl2qSN1MFPljCNAgED5AgJm+TW0AgIEdiAgYKYXWcBMt0sdqYOZKmcc AQIEyhcQMMuvoRUQILADAQEzvcgCZrpd6kgdzFQ54wgQIFC+gIBZfg2tgACBHQgImOlFFjDT7VJH 6mCmyhlHgACB8gUEzPJraAUECOxAQMBML7KAmW6XOlIHM1XOOAIECJQvIGCWX0MrIEBgBwICZnqR Bcx0u9SROpipcsYRIECgfAEBs/waWgEBAjsQEDDTiyxgptuljtTBTJUzjgABAuULCJjl19AKCBDY gYCAmV5kATPdLnWkDmaqnHEECBAoX0DALL+GVkCAwA4EBMz0IguY6XapI3UwU+WMI0CAQPkCAmb5 NbQCAgR2ICBgphdZwEy3Sx2pg5kqZxwBAgTKFxAwy6+hFRAgsAMBATO9yAJmul3qSB3MVDnjCBAg UL6AgFl+Da2AAIEdCAiY6UUWMNPtUkfqYKbKGUeAAIHyBQTM8mtoBQQI7EBAwEwvsoCZbpc6Ugcz Vc44AgQIlC8gYJZfQysgQGAHAgJmepEFzHS71JE6mKlyxhEgQKB8AQGz/BpaAQECOxAQMNOLLGCm 26WO1MFMlTOOAAEC5QsImOXX0AoIENiBgICZXmQBM90udaQOZqqccQQIEChfQMAsv4ZWQIDADgQE zPQiC5jpdqkjdTBT5YwjQIBA+QICZvk1tAICBHYgIGCmF1nATLdLHamDmSpnHAECBMoXEDDLr6EV ECCwAwEBM73IzYAZ9vLoY49d7uz9W7cuf+/XeR1q27piH52dVWdXrlz+8XDnzuWvDx4OvQX9+HDw s0n66W4kAQIETi7gm/jJS2ACBAgQGBcQMMeN+rZoB8z0PRmZItAMmHXIFDBTJI0hQIBAGQICZhl1 MksCBHYuIGCmnwACZrrdHCN1MOdQtA8CBAiUIyBgllMrMyVAYMcCAuaOi5/J0sPlrXeuX68eeuSR yxnd/uCDy9/Xv9Z/d+WNN+5eEts19bAfHcxMimoaBAgQWEBAwFwA1S4JECAwt4CAObeo/U0RqO+d DAFz7GMoYLoHc0zP5wkQIFC+gIBZfg2tgACBHQgImDsocsZLnCtghiXqYGZcaFMjQIDADAIC5gyI dkGAAIGlBQTMpYXtf0gghMLwJNjbTz89CqWDOUpkAwIECGxaQMDcdHktjgCBrQgImFupZJnr6AqY 9f2X7RW5B7PMGps1AQIE5hIQMOeStB8CBAgsKCBgLohr19ECOpjRVDYkQIDAbgUEzN2W3sIJEChJ QMAsqVrbm2vMJbKXT5c9Px9dvHswR4lsQIAAgaIFBMyiy2fyBAjsRUDA3Eul815n6GA2L43tukz2 oTff7F2Ep8jmXV+zI0CAwBwCAuYcivZBgACBhQUEzIWB7X5UIOby2LAT92COUtqAAAECmxYQMDdd XosjQGArAgLmVipZ3jqmvKJkLGDqYJZXfzMmQIDAVAEBc6qY7QkQIHACAQHzBOgOeSnQFzA9RdYJ QoAAAQJdAgKm84IAAQIFCAiYBRRpw1MMIfPO9etRK/QezCgmGxEgQGCzAgLmZktrYQQIbElAwNxS NctaS7uD2flgn0ceqW5/8MHlwtyDWVZ9zZYAAQJzCwiYc4vaHwECBBYQEDAXQLXLSQJzPOTHPZiT yG1MgACBIgUEzCLLZtIECOxNQMDcW8XzWW/MOzCbs9XBzKd2ZkKAAIFTCAiYp1B3TAIECEwUEDAn gtl8doGuDmbX5bLuwZyd3g4JECBQlICAWVS5TJYAgb0KCJh7rXw+664DZt/TY+uZ6mDmUzMzIUCA wCkEBMxTqDsmAQIEJgoImBPBbD67gHswZye1QwIECGxSQMDcZFktigCBrQkImFuraFnriQ2XYVU6 mGXV1mwJECAwt4CAObeo/REgQGABAQFzAVS7HBVovqKk79LY9t+7B3OU1QYECBDYtICAuenyWhwB AlsREDC3Usny1hFC5p3r1++Z+NB9mDqY5dXYjAkQIDCngIA5p6Z9ESBAYCEBAXMhWLsdFGh2MGOp dDBjpWxHgACBbQoImNusq1URILAxAQFzYwUtZDlzB8yw7LDPBw+HXoGPDwc/mxRyfpgmAQIEugR8 E3deECBAoAABAbOAIm1wiiEMnl25UsW+oiQQ6GBu8ESwJAIECEwQEDAnYNmUAAECpxIQME8lv+/j tgNmU6PvPkz3YO77nLF6AgQICJjOAQIECBQgIGAWUKQNT3Gu15TUl9y6RHbDJ4ulESCwewEBc/en AAACBEoQEDBLqNJ25zhXwAxC7sHc7nliZQQIEAgCAqbzgAABAgUICJgFFGmDU2y/oqR9WWzXZbLu wdzgiWBJBAgQmCAgYE7AsikBAgROJSBgnkp+38ftegfmmIh7MMeEfJ4AAQLbFhAwt11fqyNAYCMC AuZGClnQMlJeURKWp4NZUJFNlQABAgsICJgLoNolAQIE5hYQMOcWtb8xgaGA2fcE2bGAGT7vHswx eZ8nQIBA2QICZtn1M3sCBHYiIGDupNAZLjM84GcoULanrIOZYRFNiQABAisKCJgrYjsUAQIEUgUE zFQ541IFht6BObRP92CmihtHgACBbQgImNuoo1UQILBxAQFz4wXOeHlDryjxFNmMC2dqBAgQOJGA gHkieIclQIDAFAEBc4qWbecUaAbMmEtldTDn1LcvAgQIlCcgYJZXMzMmQGCHAgLmDoueyZKHOphd U3QPZiaFMw0CBAicSEDAPBG8wxIgQGCKgIA5Rcu2cwlMDZfhuDqYc+nbDwECBMoUEDDLrJtZEyCw MwEBc2cFP/Fym68oibkstjl2c3t8AAAeAUlEQVRdHcwTF8/hCRAgcGIBAfPEBXB4AgQIxAgImDFK tplLYOwdmO/fuFE9+uST1e0PPrjnkN/5y7+s/u5v/mZ1duVK71S8B3OuKtkPAQIE8hQQMPOsi1kR IEDgHgEB0wmxtkAIgneuX598WB3MyWQGECBAYFMCAuamymkxBAhsVUDA3Gpl813XEgEzrFYHM9+a mxkBAgTmEBAw51C0DwIECCwsIGAuDGz39wjUl8ievf765WWwU+7D1MF0MhEgQGDfAgLmvutv9QQI FCIgYBZSqI1MMwTMcB/l1KfIugdzIyeAZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOM0BaYGzDBWB9M5 RIAAgX0LCJj7rr/VEyBQiICAWUihNjbNuQNm4HEP5sZOEsshQIBAS0DAdEoQIECgAAEBs4AibWSK x7wDUwdzIyeBZRAgQOAIAQHzCDxDCRAgsJaAgLmWtOPUXcaUV5S4B9P5Q4AAAQICpnOAAAECBQgI mAUUaSNTbHYwU5bkHswUNWMIECCwHQEBczu1tBICBDYsIGBuuLiZLe2YgKmDmVkxTYcAAQInEBAw T4DukAQIEJgqIGBOFbN9qkDqK0rq4+lgpsobR4AAgW0ICJjbqKNVECCwcQEBc+MFzmh5XQHzoUce qW5/8MHoLHUwR4lsQIAAgc0LCJibL7EFEiCwBQEBcwtVLGsNzVeUhOD4xZ/6qd4FND+vg1lWnc2W AAECcwsImHOL2h8BAgQWEBAwF0C1y04BHUwnBgECBAgcIyBgHqNnLAECBFYSEDBXgnaYuwLNDmaT JXQrH33yyer9GzfudjV1MJ04BAgQIFALCJjOBQIECBQgIGAWUKQNTbEvXHYtsRku3YO5oZPAUggQ IJAoIGAmwhlGgACBNQWGAua1qqreeuihNafjWBsVmPqKknY386E336zqS2y7iOr9P3g49Ap+fDj4 2WSj55dlESCwDwHfxPdRZ6skQKBwgccff/zw/q1bnasQMAsvbkbTHwuYdbey3bWsHwAU08F8+86d 6msCZkZVNxUCBAjMKyBgzutpbwQIEFhEQMBchNVOOwRCyLxz/fqgTXhtSdc9mGMdzLBTAdNpR4AA gW0LCJjbrq/VESCwEQEBcyOFLGAZXQGzq2PZ7maGP4ePv/ubv1mdXbnSu1IBs4CTwBQJECBwhICA eQSeoQQIEFhLQMBcS9pxgkD9kJ//63Ofq/63733vPpT2ezHrP+tgOn8IECBAQMB0DhAgQKAAgRee e/Zw8eprvTP9oYf8FFDF/KfY9Q7MMOu+ey/D58L9lwJm/rU1QwIECKwlIGCuJe04BAgQOEJAwDwC z9DJAmOvKem7ZPbR3/mdwafIhol87fbt6u2BGXmK7ORyGUCAAIGsBATMrMphMgQIEOgWuLi4OLzw /PO9PDqYzpy5BJrhsus+y2bHsvn5cPwQMMc+BMwxIZ8nQIBA2QICZtn1M3sCBHYkMPQuzI/OzgYf rLIjJktNFBh7RUnYbd+DferQGRMwH7h9u3eG589+vXrplVf9bJJYQ8MIECCQg4Bv4jlUwRwIECAQ ISBgRiDZJFmgef/llM5lHS7DgY8NmO/euFFdvXrVzybJVTSQAAECpxfwTfz0NTADAgQIRAkMBcy3 zs6qawOvhog6gI12LRD7gJ++LmbM/ZcBeKiD6f7LXZ+CFk+AwEYEBMyNFNIyCBDYvsBQwLxWVdVb niS7/ZNgwRUOdTDDYccujw3bjL0DMxzjwcOhdxUC5oIFtmsCBAisJCBgrgTtMAQIEDhWYOhJsgLm sbrG1wLtJ8jGBMswNqaD+fadO9XXBEwnGwECBDYtIGBuurwWR4DAlgQ8SXZL1cxzLSFcDt1/GWbd vOey+eeYgOkJsnnW3awIECAwp4CAOaemfREgQGBBgffee+/w1Sef7D2CV5UsiL/xXXfdfzk1aMY8 4GcoYD762GPVzZs3/Vyy8XPN8ggQ2L6Ab+Tbr7EVEiCwIQFPkt1QMTNaytgTZNudy3YXM3x+7P7L sI1XlGRUdFMhQIDAQgIC5kKwdkuAAIElBDxJdglV+wwB87/80i/dvfy1DpB9v7bFYi6PHXvAj1eU OA8JECCwDQEBcxt1tAoCBHYi4EmyOyn0issMwS983Ll+vfdJse3ptDuYMQHTA35WLKpDESBA4IQC AuYJ8R2aAAECUwUef/zxw/u3bvUOcx/mVFHbt++/HLr3suvS2CB47P2XYR9eUeJcJECAwDYEBMxt 1NEqCBDYkYD7MHdU7BWW2nf/ZX3ovlBZfz4mXIZth+6/9ICfFQrtEAQIEFhJQMBcCdphCBAgMJeA +zDnkrSfsctjx8Jl3b2sQ2qfqPsvnWsECBDYj4CAuZ9aWykBAhsRcB/mRgqZwTKGupfNcDkUNGM6 mO6/zKDYpkCAAIGVBATMlaAdhgABAnMJvPDcs4eLV1/r3Z37MOeS3vZ+6u5leHps+Gg/MTZ29TGv Jxl6/2U4jvsvY7VtR4AAgfwFBMz8a2SGBAgQuEfgvffeO3z1ySd7Vd46O6uuXblCjcCoQAiZ9dNj mxvHXBobto95emzYzv2Xo6WwAQECBDYjIGBuppQWQoDAngTch7mnai+71vf//t8f7F4Ohc2Y7qX7 L5etn70TIEAgNwEBM7eKmA8BAgQiBIYCZhjuMtkIxJ1vEoJfuDy2HSDn7l66PHbnJ5rlEyCwOwEB c3clt2ACBLYgMHYf5kdnZ9WZy2S3UOpF1xC6l/VHbLCst5/j8tiwL/dfLlpiOydAgMDqAgLm6uQO SIAAgXkEPE12Hsc97qXdvUzpYsZcHjv29NjzZ79evfTKq34W2eNJaM0ECGxWwDf1zZbWwggQ2LqA y2S3XuFl1le/mqTZvZx6pBAuw8dYl9zlsVNlbU+AAIHyBQTM8mtoBQQI7FTg4uLi8MLzz/eu3tNk d3pijCx7rHsZoxbTvQz7GXp6bPi8y2NjtG1DgACBsgQEzLLqZbYECBC4R8Blsk6IFIGu7mXsPZix 3cuxy2PfvXGjunr1qp9DUgpoDAECBDIW8I094+KYGgECBMYEHn/88cP7t271buZpsmOC+/p83b0M q44NlLXQ93/zN6uf/qVfqmK7ly6P3de5ZbUECBCoBQRM5wIBAgQKF9DFLLyAK0//mHsvY58cO9a9 fPSxx6qbN2/6GWTl2jscAQIE1hDwzX0NZccgQIDAggIe9rMg7kZ2HTqX4SO893JK97LuWtYMc3Uv X3r55er8/NzPIBs5vyyDAAECTQHf3J0PBAgQKFxg7DJZD/spvMAzTv+Y7mXsvZchzD54OAzO2sN9 ZiyqXREgQCAzAQEzs4KYDgECBFIEdDFT1PYxJvW1JM3uZfj9tStXosDG7r3UvYxitBEBAgSKFRAw iy2diRMgQOBHAi889+zh4tXXekl0Mfd5trQvjQ0KYw/3aV8WG8bE3nupe7nP88yqCRAg0BQQMJ0P BAgQ2IiALuZGCjnjMlK7l2EKddAMvz5VVdVZRAdT93LG4tkVAQIEChUQMAstnGkTIECgLeBeTOdE U+CYcNncT2z3Mox54PbtwSK499I5SoAAge0LCJjbr7EVEiCwIwFdzB0Ve2CpXZfGjsk0O5bhfZfh I/bBPmFb3csxYZ8nQIDAPgQEzH3U2SoJENiJgC7mTgoducyYp8Z23XMZdj/l0lj3XkYWxGYECBDY gYCAuYMiWyIBAvsSGOtifnR2FnU/3b7UtrPa1Etj2x3M2HdeBrmxS2PPn/169dIrr/qZYzunmZUQ IECgV8A3eycHAQIENiYw1sW8VlXVWw89tLFVW04QmBou+7qXUy6NffvOnepr3nvpBCRAgACBTwUE TKcCAQIENigw1sX02pLtFT3lvstaodm9DH8X+9RY3cvtnUdWRIAAgWMFBMxjBY0nQIBAhgJj78UM U/6hLmaGlUubUkq47HqoTzh6eGps7MfYg33Cfjw5NlbTdgQIENiGgIC5jTpaBQECBO4TGOtiulR2 GydNHS7DeypTH+rz7z/7t6u/89q/uexcho+Yd17GPNjn3Rs3qqtXr/pZYxunmlUQIEAgSsA3/Sgm GxEgQKA8gffee+/w1SefHJy4S2XLq2vfjGPCZXNsu4M55X2XYT9jD/Z59LHHqps3b/o5YzunmJUQ IEAgSsA3/igmGxEgQKBMgbEuZliVS2XLrG2YdexDfZoP8+l6sM+Uh/qE43qwT7nnjJkTIEBgaQEB c2lh+ydAgMCJBcZCpktlT1yghMPXwTIMndq5DGNCyPyvX/+Xky+LjQ2XL738cnV+fu5njITaGkKA AIHSBXzzL72C5k+AAIERgYuLi8MLzz8/uJVLZcs5jVLvuezqXIa/m/LE2KA0dmls2MaDfco5n8yU AAECcwsImHOL2h8BAgQyFBjrYoYpf3R2FvVwlwyXt8sppXQuA1QdNKdeFhsbLj3YZ5eno0UTIEDg roCA6WQgQIDATgRiQqb7MfM9GaZ0LtsP8On689TOZcwrSVwam+/5Y2YECBBYS0DAXEvacQgQIHBi gZhLZd2PeeIi9Rw+5T2XXbsKryP517/+7+4+HCh2tTEP9Qn7cmlsrKjtCBAgsF0BAXO7tbUyAgQI 3Cfw+OOPH96/dWtQxv2YeZ04U8Nl1xNjj7ksNuZ9l8JlXueM2RAgQOCUAgLmKfUdmwABAicQiLlU Vsg8QWFah5xySWzXbOtQGbqW//TP/1uVcs9lbLh03+XpzxczIECAQC4CAmYulTAPAgQIrCggZK6I nXCoqeFy6D2X9ZNiwzTOrlyZNJuYJ8a673ISqY0JECCweQEBc/MltkACBAjcL/Dee+8dvvrkk6M0 niw7SjTrBnWwDDv9L7/0S5P23X4NSf3nR3/ndybfcxkOHPNQn7Cd+y4nlcnGBAgQ2LyAgLn5Elsg AQIEugVi7scMI4XMdc6gqV3LvlmFYPlfv/4vq7/z2r+5fMdl+FiicylcrnNeOAoBAgRKExAwS6uY +RIgQGBGgZhLZcPhvL5kRvSOXYVwWYfAqe+3bL+CJOw+tWsZxupcLltreydAgMDWBQTMrVfY+ggQ IDAiEBsydTLnP5VSu5bty2HDzMLDfELX8tqVK0mXxAqX89fXHgkQILBHAQFzj1W3ZgIECLQEhMx1 T4nUYNk1y/a9lmGbqZfECpfr1t/RCBAgsGUBAXPL1bU2AgQITBCIDZleYTIBtbVp8yE+IQTWl8N+ 8ad+qvrOX/5l1I67upcpryBpHiz2stjzZ79evfTKq352iKqUjQgQILBPAf9I7LPuVk2AAIH7BGKf LBsGCpnTTqBmxzKMnHqfZdfRmq8fCZ9P6VrGvucy7F+4nFZzWxMgQGCvAgLmXitv3QQIEOgQEDLn PS3GOpaxncuuV5CEey3DR/MBQVNmPyVcPvrYY9XNmzf9zDAF2LYECBDYqYB/LHZaeMsmQIBAn8CU kHktdDMfeghmQ6AvVIZNYgNlH2iza5nSsawDafj1wcMhqm7CZRSTjQgQIEDgUwEB06lAgAABAvcJ XFxcHF54/vkoGSHzky5i86O+v7IOlO1gOTVohmB5zNNhm3N7+86d6mvCZdS5bSMCBAgQmC4gYE43 M4IAAQK7EYh98E8A2dtrTNqdymAQ7q2cK1SG/dUdy9RuZftEjX2YTxjnnsvdfJlbKAECBGYVEDBn 5bQzAgQIbE9gSsjc8sN/2l3KUOm+TmWzQzm1Wxn2G54KO1eoDPubcr9l2P6ll1+uzs/P/YywvS9n KyJAgMDiAv7xWJzYAQgQIFC+wJSQWfols11Bsg6TdSVvP/30PUUNrxjp6lxODZf160bqnc8RMqdc EhuO++6NG9XVq1f9fFD+l60VECBA4CQC/gE5CbuDEiBAoDyBxx9//PD+rVvREz9lN7MvJMZMvh3q 2mEy7KN+Z2Xs5bBDQfPR3/mde+7hnCNU1uuccklsGPPx4eDngpiTxDYECBAg0CvgHxInBwECBAhE C0wNmafuZoZw+NCbb/aur/58V4hsD2p3Keug2Q6ZMV3LECrDR/P9mO13ZUYXpWPDqV1L4fIYbWMJ ECBAoCkgYDofCBAgQGCSwJTXmNQ7PmU3sx0eH3rkkctp3f7gg8tfw5/r33dB1MGyHSjDn4fCZPNz 7UAZxoZOZeo7LPsKlhIsPcxn0ulvYwIECBAYERAwnSIECBAgkCQw5b7M+gBrP2l2qCvY17XsCpzN kFmvpeu+yzpIhm26Xl0yd6BsFm7q5bBhrHCZdOobRIAAAQIDAgKm04MAAQIEkgVeeO7Zw8Wrr00a Hy6b/fbZ2axPSR2bQFew6wqAdTCceh/knJe3jq2l/fmUrmXYh/stp0rbngABAgRiBATMGCXbECBA gECvwMXFxeGF55+fLHTKy2brIFlPug6UqUFxyc5kH2wIljcOh+rtifKPPvZYdfPmTf/+T3SzOQEC BAjECfgHJs7JVgQIECAwIpByyWzYZQiaT316TyLkcYHUjmXYs1eQjPvaggABAgSOExAwj/MzmgAB AgQaAimXzNbDT3HpbEnFOyZY6lqWVGlzJUCAQNkCAmbZ9TN7AgQIZCkw9XUmzUUImj/SCJfevlNV 1dcOh+Q6n+pey9SOdvJCDSxa4FTnadFoJk8gUwEBM9PCmBYBAgRKF0h5nUl7zXu9fDb1/sqm36mf EBsCptBQ+lfxOvN3rqzj7CgE1hIQMNeSdhwCBAjsVCD1IUDtruaTZ2fVtStXNqsYupX/LOGhPV0g OQQ7oWGzp+rsC3OuzE5qhwROKiBgnpTfwQkQILAfgWMum+0Km6U/GKi+/DXlSbB9Z01OD/ERGvbz tX3sSp0rxwoaTyAvAQEzr3qYDQECBDYvMFfQrKHCPZuldDeXCJXBIadgWddFaNj8l/JsC3SuzEZp RwSyEBAwsyiDSRAgQGB/AnMHzXbgDB3O8FG/43Jt4TpMhuPO2aWs15H7k2GFhrXPuHKP51wpt3Zm TqBLQMB0XhAgQIDASQWOebVJ7MRDlzN8hE7n3JfWLh0k22vMPVjW8xUaYs9O2zlXnAMEtiUgYG6r nlZDgACBYgXmeBjQsYuvg2jfft4+9gBHjD/1U2GnTl1omCq23+2dK/utvZVvU0DA3GZdrYoAAQJF C6zR1SwBKHQrv/GNb1RXr14t7t9roaGEMyyPOTpX8qiDWRCYS6C4f7DmWrj9ECBAgEAZAkvdq5nz 6l96+eXq/Pzcv9E5F8ncZhMQMGejtCMCWQj4xyuLMpgEAQIECIwJvPfee4cXX3yxev/WrbFNi/t8 6FQ+9dRTQmUBlQvd9ZdeefVkPz+d6vhLHlfALODEN0UCEwRO9g1ywhxtSoAAAQIE7hMo/TLa0u6p nHoKLhlIps7F9nkLCJh518fsCEwVEDCnitmeAAECBLIUCA8Jeuedd7LtcG49UGZ5UrQmNUfojdlH zDZ9XseMLaEGXXMUMEutnHkT6BYQMJ0ZBAgQILBZgXBZ7e/+9m9Vf/hHf7xK8AyXuv7iL/x89TM/ +3PVww8/XOTDeeY6GfYYlGLtcrSZY06p+xAwY88c2xEoQ0DALKNOZkmAAAECCwqEIBp2X4fR9qFC aPx7/+AfXv51iU90XZBud7tODVG7g5qwYAFzApZNCRQgIGAWUCRTJECAAAECBAhsVUDA3GplrWuv AgLmXitv3QQIECBAYEGB8HqZ0PltfoSnr4YOYPuwQ39/7BRjO45hvjdv3hz9uSh2f8fOuz1+juPW 9sc+BXeOuTTXJ2DOfbbYH4HTCox+Iz3t9BydAAECBAgQKFHg2NBw7PgpZiFchu1jAuaU/ea+bWpQ TB3XF5rXrHXuNTE/AlsQEDC3UEVrIECAAAECmQmUEhpCWAoPgYoNmHOFq6nlOtVxu+Y511zq/ZRy rkytme0J7FVAwNxr5a2bAAECBAgsKLBWaDgm7DTDZWzAXJBscNfHrHNszlP3PXX7seOvda6MzcPn CRCYR0DAnMfRXggQIECAAIGGwClDQ2wAqi+Nracdc4ls7L7nPhlOddyudcw1Fx3Muc8S+yOQh4CA mUcdzIIAAQIECGxKIDVgTgkvXdvGjm+Hy4AfEzBPVaTYda0xv7nmImCuUS3HILC+gIC5vrkjEiBA gACBzQukBsw1YNqXxm6hgxm8H33ssUG+uQK0gLnGWeoYBMoVEDDLrZ2ZEyBAgACBbAVSAuZcwWUI pS9chleqHPv6jiWLMWbT1ZFtzyc2YI7VbmwuUx3Gjjd1f7YnQOC0AgLmaf0dnQABAgQIbFJg7dAQ G3qGglhMAIs9ztxFHTvunAFzbO5jcxkbX3/eJbKxUrYjUJaAgFlWvcyWAAECBAgUIbBmwIwNPGMh LCZgngp/bI1jawvznmt9Y3OJNRIwY6VsR6AsAQGzrHqZLQECBAgQKEJgzYAZAzJXAJsrXMXMubnN 2HHnWl/MvMbmErOPsI2AGStlOwJlCQiYZdXLbAkQIECAQBECuQXMItAGJjkW6up7S8O9pF0fc95f OjaXqdbOlalitieQt4CAmXd9zI4AAQIECBQpEBsajgkrx4xdAzXMb+px+oLg0mudsv8p2w6tXwdz 6tlhewJlCAiYZdTJLAkQIECAQFECsQHzmEXNFXSOmcNYgPqnf/7fJu3+sf/0nzp/NstprXPNRcCc dGrYmEAxAgJmMaUyUQIECBAgUI5ATMCcElS6tp0y/lRyt/7xPz78+8/+7SoEzfBr/fEzP/tz1Z/9 6Z9U7V9P1cGc4jOXu4A5Rd22BMoREDDLqZWZEiBAgACBYgRiAuaxi5kr6Bw7j77xYX51B7MOmUPH CtusGTBT/VLH9a19jXNlqRrbLwEC9wsImM4KAgQIECBAYHaBNULD3EFnboQ6YHZ1MMOxml3N+vdr Bczw1Nn3b92qPj4cJv8sOJe7DubcZ5z9EchDYPI3lTymbRYECBAgQIBAzgIC5ifVCZfIhl/bHcz/ 9x/9o+p/+c//uWr/uqd7MOvzd41zJeevFXMjsDUBAXNrFbUeAgQIECCQgcBaoaF+Umvo/B37+5hX edRdt/avXeRdHcy6U9n+tZ7/Wh3MNU6R/7+9O7xpGwzCAMwETNBFGIFR2g0YgKIu0I7CAPnBIigD MECDHNWVFTmJP5Jz7LvnF61wLr7nrlJfvpCcO+l0gjnHFDwHgfkFBMz5zT0jAQIECBBIL3AsYJ4L HUOYlmuXCjp2gjl8yezwpbGn+p3bYs7nm+uHEUvdEfdFIJuAgJltovohQIAAAQILEMgaGlqC19gJ 5sPvP3dvP77vf//y6ePj7uX+fv91s9nsT2BvcYLZ0lPEamXdlQgrNQmsQUDAXMOU3CMBAgQIEFiZ wFhoaAkyLdcumaY7wXx/ff0fKrt7nfsEc4rllGuinAXMKFl1CdxGQMC8jbtnJUCAAAECqQWyhoaW IHbsBPPb4+M+ZPanmf3XbiGiTjBb7nu4mF99XMtyZ92VFgPXEsgkIGBmmqZeCBAgQIDAQgTWFhqi glT/O5j9WA5PL4cnmt2fb/kuslEG51Zybbtyrh/fJ1BdQMCsvgH6J0CAAAECAQLXeJOfgNu6qOR2 u939+vl89JTxsHh/gnkYIod/P/yMzKgTzFONTw2WU68bczj1Dr0C5kVr6cEEFicgYC5uJG6IAAEC BAisX2BqaPhqaIkSWtr9RPXZ1b201/5jYcbuccpHvvSPm7orkRZqEyBwPQEB83qWKhEgQIAAAQL/ BFpCQ8u1gPMJmH++meqotoCAWXv+uidAgAABAiECQkMIa8qidiXlWDVVWEDALDx8rRMgQIAAgSgB oSFKNl9du5JvpjqqLSBg1p6/7gkQIECAQIiA0BDCmrKoXUk5Vk0VFhAwCw9f6wQIECBAIEpAaIiS zVfXruSbqY5qCwiYteevewIECBAgECIgNISwpixqV1KOVVOFBQTMwsPXOgECBAgQiBIQGqJk89W1 K/lmqqPaAgJm7fnrngABAgQIhAh0oSGksKIpBf7udv5PmnKymqoo4B9zxanrmQABAgQIECBAgAAB AgECAmYAqpIECBAgQIAAAQIECBCoKCBgVpy6ngkQIECAAAECBAgQIBAgIGAGoCpJgAABAgQIECBA gACBigICZsWp65kAAQIECBAgQIAAAQIBAgJmAKqSBAgQIECAAAECBAgQqCggYFacup4JECBAgAAB AgQIECAQICBgBqAqSYAAAQIECBAgQIAAgYoCAmbFqeuZAAECBAgQIECAAAECAQICZgCqkgQIECBA gAABAgQIEKgo8AnOCS0ENzfoqAAAAABJRU5ErkJggg=="
              id="image1101"
              clipPath="url(#clipPath5494)"
              transform="translate(-1302.83 -112.597)"
            />
            <rect
              id="rect1103"
              width={36.968048}
              height={15.486614}
              x={-229.30182}
              y={433.12564}
              ry={0.045448378}
              fill="#fff"
              fillRule="evenodd"
              strokeWidth={0.288906}
            />
          </g>
          <path
            d="M94.396 9.64v4.244L88.2 19.596h-4.737"
            id="path1107"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.03278}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M79.73 14.753h8.417V8.658l-.685-.803-1.606 1.607-1.265-.34-2.28 3.95-1.627-1.628-.852.852v2.48"
            id="path1109"
            display="inline"
            fill="#02728e"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M79.912 11.64l.818-.818 1.504 1.504L84.32 8.71l1.472.395 1.79-1.679"
            id="path1111"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth={0.264999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1113"
            d="M49.952 28.832l-1.166-.791 1.269-.614z"
            transform="scale(.6992) rotate(13.076 163.51 349.186)"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264999}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <path
            id="path1115"
            d="M99.702 7.463V19.57h90.693l6.733-5.72V1.943h-91.516z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.570934}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1119"
            d="M343.915 7.448v12.16h46.371l6.548-5.745V1.903h-46.572z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.499999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M344.235 11.698V7.671l6.123-5.42h4.681"
            id="path1121"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1123"
            d="M396.552 9.64v4.244l-6.197 5.712h-4.737"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.03278}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1125"
            d="M99.817 11.717V7.573l4.755-5.58h3.636"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.06037}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M196.84 9.544v4.273l-5.377 5.752h-4.11"
            id="path1127"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.14489}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M405.535 7.448v12.16h69.239l5.14-5.745V1.903h-69.867z"
            id="path1129"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.499999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M405.647 11.442V7.445l4.386-5.38h3.353"
            id="path1131"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1133"
            d="M479.714 9.54v4.285l-4.69 5.767h-3.586"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.0708}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={45.249626}
            y={13.0406}
            id="text1147"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1145"
              x={45.249626}
              y={13.0406}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'GERENCIAL'}
            </tspan>
          </text>
          <text
            id="text1151"
            y={13.0406}
            x={105.57463}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={105.57463}
              id="tspan1149"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'MAIN'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={122.50797}
            y={13.0406}
            id="text1155"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1153"
              x={122.50797}
              y={13.0406}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={dc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DC'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1163"
            y={13.0406}
            x={208.76154}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={208.76154}
              id="tspan1161"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'GRUPAL'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={234.69034}
            y={13.0406}
            id="text1167"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1165"
              x={234.69034}
              y={13.0406}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={mv_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {"MV'S"}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1171"
            y={13.0406}
            x={251.09428}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={251.09428}
              id="tspan1169"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={swt_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'SWITCHS'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1179"
            y={13.0406}
            x={299.24872}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={299.24872}
              id="tspan1177"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={gtw_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'GATEWAYS'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1187"
            y={13.0406}
            x={346.87387}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={346.87387}
              id="tspan1185"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'REPORTE'}
            </tspan>
          </text>
          <path
            d="M390.725 6.487l-.385.323.022 1.335-.86-.434-.26.156v.408l1.08.614v1.04l-.816-.448-.018.905-.873-.47V8.602l-.314-.215-.3.166v.986l-1.282-.748-.359.206.036.515 1.152.619-.735.457-.01.367.409.229 1.066-.596.73.466-.64.47.668.467-.811.47-1.05-.618-.371.179.004.41.743.43-1.155.666v.488l.404.211L388 13.52v.887l.314.198.322-.238v-1.224l.928-.448v.87l.758-.511.004 1.062-1.045.592v.367l.31.22.793-.457v1.33l.394.323.296-.331v-1.313l.825.461.282-.188-.013-.515-1.013-.511v-1.031l.793.493v-.83l.883.485v1.192l.323.237.34-.246v-.941l1.143.735.417-.188-.027-.476-1.134-.627.722-.47v-.39l-.292-.184-1.062.555-.829-.434.695-.48-.686-.403.798-.57 1.116.624.318-.198-.027-.403-.757-.435 1.174-.654-.045-.506-.354-.162-1.174.623-.014-.91-.345-.197-.367.229V9.88l-.892.457v-.919l-.73.516v-1.02l1.113-.643v-.386l-.339-.196-.836.483v-1.48zm.376 4.43l.37.592-.36.595-.733.005-.37-.59.361-.597z"
            id="path1189"
            display="inline"
            opacity={1}
            fill="#3fbefa"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0177471px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1191"
            d="M472.747 6.487l-.386.323.023 1.335-.86-.434-.26.156v.408l1.08.614v1.04l-.816-.448-.018.905-.874-.47V8.602l-.314-.215-.3.166v.986l-1.282-.748-.359.206.036.515 1.152.619-.735.457-.009.367.408.229 1.067-.596.73.466-.64.47.667.467-.811.47-1.049-.618-.372.179.004.41.744.43-1.155.666v.488l.403.211 1.201-.708v.887l.314.198.323-.238v-1.224l.927-.448v.87l.758-.511.004 1.062-1.044.592v.367l.309.22.793-.457v1.33l.394.323.296-.331v-1.313l.825.461.282-.188-.013-.515-1.013-.511v-1.031l.793.493v-.83l.883.485v1.192l.323.237.34-.246v-.941l1.143.735.417-.188-.027-.476-1.134-.627.722-.47v-.39l-.291-.184-1.063.555-.829-.434.695-.48-.686-.403.798-.57 1.116.624.318-.198-.027-.403-.757-.435 1.174-.654-.045-.506-.354-.162-1.174.623-.014-.91-.345-.197-.367.229V9.88l-.892.457v-.919l-.73.516v-1.02l1.113-.643v-.386l-.338-.196-.837.483v-1.48zm.376 4.43l.37.592-.361.595-.732.005-.37-.59.36-.597z"
            display="inline"
            opacity={1}
            fill="#3fbefa"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0177471px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={409.3165}
            y={13.0406}
            id="text1195"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1193"
              x={409.3165}
              y={13.0406}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'DISPONIBILIDAD'}
            </tspan>
          </text>
          <path
            id="path1203"
            d="M148.214 11.472h-.86l-.004-.847.784-.077z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M148.287 10.035l-.775-.37.361-.767.74.269z"
            id="path1205"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1207"
            d="M149.002 8.674l-.586-.63.618-.58.59.522z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M150.14 7.708l-.297-.806.794-.297.343.709z"
            id="path1209"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1211"
            d="M151.545 7.237l.057-.86.847.016.058.786z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M153.05 7.375l.362-.78.785.32-.23.754z"
            id="path1213"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1215"
            d="M154.375 8.037l.644-.572.591.608-.51.599z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M155.35 9.19l.805-.305.337.778-.69.379z"
            id="path1217"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1219"
            d="M155.876 10.525l.859.067-.028.847-.886.036z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M149.398 11.593l3.936-6.199-1.448 4.14 2.44-.67.202.215-4.68 8.106 1.767-5.863z"
            id="path1221"
            display="inline"
            opacity={0.998}
            fill="none"
            stroke="#fd6425"
            strokeWidth={0.299283}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter10264)"
          />
          <path
            d="M84.667 115.017l53.015-81.377-17.784 51.835 44.072-11.908L97.724 190.06l24.348-79.938z"
            id="path1223"
            transform="matrix(.05986 0 0 .05986 144.51 4.625)"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.271779}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".271779,.543559"
            strokeDashoffset={0}
            strokeOpacity={1}
            filter="url(#filter14519)"
          />
          <path
            id="path1225"
            d="M148.72 13.473c.898 2.35 5.458 2.631 6.76-.244"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1227"
            d="M375.988 11.578h-.86l-.004-.848.784-.076z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M376.062 10.14l-.776-.37.362-.767.74.269z"
            id="path1229"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1231"
            d="M376.777 8.78l-.586-.63.617-.58.59.521z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M377.915 7.814l-.298-.807.794-.297.343.71z"
            id="path1233"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1235"
            d="M379.32 7.342l.056-.86.847.017.058.785z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M380.823 7.48l.364-.78.784.32-.23.754z"
            id="path1237"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1239"
            d="M382.149 8.142l.644-.571.591.607-.51.6z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M383.124 9.296l.806-.305.337.777-.691.38z"
            id="path1241"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1243"
            d="M383.65 10.63l.859.068-.027.847-.887.036z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M377.172 11.699l3.937-6.2-1.448 4.14 2.439-.67.202.216-4.68 8.105 1.767-5.863z"
            id="path1245"
            display="inline"
            opacity={0.998}
            fill="none"
            stroke="#fd6425"
            strokeWidth={0.299283}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter10264)"
          />
          <path
            d="M84.667 115.017l53.015-81.377-17.784 51.835 44.072-11.908L97.724 190.06l24.348-79.938z"
            id="path1247"
            transform="matrix(.05986 0 0 .05986 372.285 4.73)"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.271779}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".271779,.543559"
            strokeDashoffset={0}
            strokeOpacity={1}
            filter="url(#filter14519-4)"
          />
          <path
            id="path1249"
            d="M376.495 13.579c.898 2.35 5.457 2.63 6.759-.244"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1251"
            d="M458.234 11.775h-.86l-.003-.848.783-.076z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M458.308 10.337l-.776-.37.362-.767.74.27z"
            id="path1253"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1255"
            d="M459.023 8.976l-.586-.629.617-.58.59.521z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M460.16 8.01l-.296-.806.793-.297.343.71z"
            id="path1257"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1259"
            d="M461.566 7.54l.056-.86.848.016.058.785z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M463.07 7.678l.363-.781.784.32-.23.754z"
            id="path1261"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1263"
            d="M464.395 8.34l.645-.572.59.607-.51.6z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M465.37 9.493l.806-.305.337.777-.69.38z"
            id="path1265"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1267"
            d="M465.896 10.827l.859.068-.027.847-.886.036z"
            display="inline"
            opacity={0.998}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".0159077px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M459.418 11.896l3.937-6.2-1.448 4.14 2.44-.67.201.216-4.68 8.105 1.767-5.863z"
            id="path1269"
            display="inline"
            opacity={0.998}
            fill="none"
            stroke="#fd6425"
            strokeWidth={0.299283}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter10264)"
          />
          <path
            d="M84.667 115.017l53.015-81.377-17.784 51.835 44.072-11.908L97.724 190.06l24.348-79.938z"
            id="path1271"
            transform="matrix(.05986 0 0 .05986 454.531 4.927)"
            display="inline"
            opacity={0.998}
            fill="#ff0"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.271779}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".271779,.543559"
            strokeDashoffset={0}
            strokeOpacity={1}
            filter="url(#filter14519-8)"
          />
          <path
            id="path1273"
            d="M458.741 13.776c.898 2.35 5.457 2.63 6.759-.244"
            display="inline"
            fill="none"
            stroke="#fff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            id="text1277"
            y={14.930607}
            x={80.402992}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={14.930607} x={80.402992} id="tspan1275" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={ger_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'G'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1281"
            y={15.159718}
            x={147.58551}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={15.159718} x={147.58551} id="tspan1279" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={elec_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'E'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1285"
            y={14.881001}
            x={160.12787}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={14.881001} x={160.12787} id="tspan1283" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={aacc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'A'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1289"
            y={14.881001}
            x={170.99791}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={14.881001} x={170.99791} id="tspan1287" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={sis_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'I'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1293"
            y={15.304249}
            x={374.68924}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="8.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={15.304249} x={374.68924} id="tspan1291" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={relec_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'RE'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1297"
            y={15.582967}
            x={385.30884}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="8.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={15.582967} x={385.30884} id="tspan1295" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={raacc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'RA'}{' '}
              </a>
            </tspan>
          </text>
          <text
            transform="scale(1.10647 .90378)"
            id="text1301"
            y={16.31682}
            x={410.4043}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.7101px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.292751}
          >
            <tspan y={16.31682} x={410.4043} id="tspan1299" fill="#0ff" strokeWidth={0.292751} fillOpacity={0}>
              <a href={delec_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DE'}{' '}
              </a>
            </tspan>
          </text>
          <text
            id="text1305"
            y={15.025524}
            x={467.37759}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="8.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan y={15.025524} x={467.37759} id="tspan1303" fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={daacc_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DA'}{' '}
              </a>
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={133.22632}
            y={13.0406}
            id="text1313"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1311"
              x={133.22632}
              y={13.0406}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={dse_url} target="_blank" style={{ fill: 'white' }}>
                {' '}
                {'DSE'}{' '}
              </a>
            </tspan>
          </text>
          <image
            id="image1321"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAToAAAE7CAYAAABNHk35AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz AAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURB VHic7d15eGRlnS/w7+89VUllqSTdSWXrTjcNNIhcF8AFFEFZHEEHvO4ouI5zFXG/3rnqODouKOrj vtyLcllFxyuM4IJwVdQBFRBEAWURGmhSWSqV5aSSSqrOOb/5I9190yepJUlVnaqT7+d5+tG871vn /For3z7b+x7BBk1NTXU5jvM8ETkRwFEAdgPoA9AKoHmj26e65AGYATAL4GEReRDAbY7j/Lqvr++R YEsjWknW86E9e/bE4vH4K1T1dSJyOgCrwnVR4/oLgKsBXNbT0zMcdDFEwBqDLplMtjY1Nb0TwHsA 9FenJAqJHICrXNe9sK+v7+Ggi6HNreygm5iYeBmALwLYUb1yKIQWAXwum81eODQ0lA26GNqcSgbd +Ph4uzHmGwDOKzF0UkR+73ne/caYvZ7nzRtjpipTJtUTVW0D0KKq/caYI1T1WABHlPjYfcaY12zd uvXeGpRIdJCiQTc1NbXTdd0bsHSTYTVjInKV53lX9/T03C0iXuVLpEYwMTGxDcDLAbwewHEFhmUB nNvT03NtzQojQpGgS6fTT1bV/wdgcJXuEVX9eCaTuWzXrl0L1SuPGlEqlXq+Mebjqvq8VbpdVb0g kUj8r5oXRpvWqkE3PT29y3GcW7Ay5BTAN0TkQ93d3XbVq6OGpaqSTqfPBfAVAF3+bgBv6OnpubL2 ldFmtCLoJicnOz3Pux0rr7nMquq5iUTi+tqURmEwNTV1iOu6P8DK01lHVV+YSCRuDqIu2lzM8h9U VTzPuwQrQ25cVZ/PkKO12rJly6Oe5z0fwE2+roiIXJ1KpQYCKIs2mYOCbt+pxst9Y2ZU9YxEInFX 7cqiMOnt7c1ks9mXishvfF39IvLNQIqiTeXAqev09PQWx3HuB9C7rN8TkRd3d3f/rPalUdjMzMxs zefzdwI4ZHm7qp7NswWqpgNHdI7jvB8HhxwAfI4hR5XS2dk5KSLnAHCXt4vIp1XVFPgY0YYZYGli PoALfH0PZ7PZf619SRRm3d3dvwfwDV/zkycnJ/2XTIgqxgCA67rnAuhc3qGqH+GUHaqGaDT6MSyt fHKAqp4fTDW0Gew/XXi9r/3hnp6e79e6GNoc9p3C+h8YPnlqauqQIOqh8DOpVGoQwDN97ZeKiLva B4gqwfO8b2PpweH9xHXds4Kqh8LNiMgp/kbXdb8XRDG0eSQSiQcB3O1rPjWIWij8DIATfG2Pcf0w qgUR+YWv6fhACqHQMwCe5Gu7PYhCaFPyf9d6Z2ZmtgZSCYWaAXDY8gZVvT+gWmiTEZEH/G35fP7w IGqhcDPwrSwhImMB1UKbTD6fX/FdE5HO1cYSbUQEQJuvLbORDaZSqXg+FvsCRKIb2Q7Vt6jqZYl4 /Fcb2UYul7NbWlr8zR0b2SbRaiL7/iy3ocdKWlpa2kZFzronFvNPJ6OQaPc8HLOwYAP41Ua2s337 diedTvub/d9Hog2rypfKEXHmDKcuhhXfbUmNhmlERKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9 Bh0RhR6DjohCj0FHRKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKHHoCOi 0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKEXCboAIqofqhp5/PHH4wAQjUYXBwcH 54OuqRIYdESb1OTkZGc2m32RMea5qnosgMNGRkb6otGo7B+TTCanAewFcLeI3GaM+VlfX9/DgRW9 Tgw6ok1EVc3o6OgZnue9bWFh4YUi0qSqxT7Ste/PU1T1PNd1kUwm/6SqlzQ1NV2WSCRma1P5xvAa HdEmMTw8fPbIyMg9qvpjEXkJgKZ1buppIvKVfD6/N5lMfjiZTLZWss5qYNARhVwymdyRTCZvFJEf AnhyBTfdCeCTAO4fHh4+vYLbrTgGHVGIDQ8Pnw3gHgAvrOJuhkTkxmQy+QVVrcvLYQw6opAaHh7+ HyJyLYCOGuxOALx3ZGTkR6lUKl6D/a0Jg44ohIaHh/9JRC5C7X/HX5TP539Wb2FXl4eZRLR+w8PD 7xSRz5Q7XkTQHI0ialmIWBZElp4uUVW4noe84yDnOHA9r9xNPiefz1+jqmeKiLOOv0LFMeiIQmR4 ePg0EflCOWMjloX2lhbEotED4VZMznEwt7CAhVyunM2fPjIy8jkA7y1ncLXx1JUoJPbu3bvVGHM5 ShzAiAg629qQ6OxES1NTWSEHAE2RCLa0t6OnowMRyyrnI+8ZGRk5o6yNVxmDjigkLMv6oqoOFhsT sSwkOjvR2ty87v1EIxH0dHSUtQ1Vvbgertcx6IhCIJlMHgvg3GJjopaFno4OWGbjv/b7jwrbW1pK Dd2ey+X++4Z3uEEMOqJw+ASK/D5HLAtb4/GyT1PLFW9pKXlkJyLvfeKJJ7oruuM1YtARNbjh4eEj ARS8FiYi2NLeDlOBI7nVdLS2IhopelkwblnWm6uy8zIx6IganIi8FUsP7K6qPRYr9+bBevePztaS 013/sWoFlIFBR9T4Xlaow4igLRaregHRSAQtRU5hVfXwvXv3PrXqhRTAoCNqYCMjI08GsKtQf2ss VvHrcoW0lbhWZ1nWi2tSyCoYdEQNzPO8Zxfrb2la70pMaxeNREqdIhettZoYdESN7bhCHZYxVb02 t5rmaLRY97G1qsOPU8CooT3xxBPnNTc3f8KyLDfoWtbLdV1rcXHxI9u3b79yrZ81xuwotEJwiTuh VVFin9vvu+++pqOPPrqsOWSVxKCjRtfa1ta2s1bXoapBVbG4uLiuVXqLzYSoxIPBa1Vin9LV1dWH pXdQ1BRPXYkaW8GpCSaA8C+1TxFpr1EpB2HQETW2RvsdLvomnmpptP+RiOhgBd+7GkSirLJPXfaf aoxZrGU9+/EaHTU0VZ2dm5t7xBjTsDcjPM+zVHW9rw0cLdSxhoUyK8ZxVqyzKcv+U2dnZ5O1rWgJ g44a2tDQ0NUArg66jgA9VqgjvzJ0qi7vFv33ZnT37t2BHNHx1JWosf2xUIfjunCKB0/FLebzBftU 9c4alnIQBh1RA3Nd97Zi/WUue14ReccpGqzGmDtqVox/30HtmIg2bvv27fcAeLxQ//ziIgo9UFxp 84vFz0pV9Sc1KWQVDDqiBiYiCuCHhfpdz8PcwkLV63Bct1TQPTYwMHBX1QspgEFH1OCMMd8q1p9Z WKj6tbrpTKbUkG/vC+VAMOiIGlx/f/+9qvrLQv2qium5uaqdwtrz86Xuti4YYy6uys7LxKAjCoeP FOvMOw6mMpmKh93cwkLJU2NV/Wp/f/94RXe8Rgw6ohDYtm3bbwH8oNiYxXweaduGV6Gws+fnYc8X nJix33gul7uwIjvcAAYdUUhYlnUBgHSxMXnXRWp6ekOPnTiui4mZmXJvcrx9165d0+veWYUw6IhC oq+vb0xV3wyg6NwvTxVTmQzStr2mwMs7DqYzGaRmZkpdk9vvksHBwWvL3kEVcQoYUYhs27bt+mQy +VEsvee1qJzjIJfJwIigORpFUzQKy5gDSy0plh5PyTsOFvP5td65vWVubu4d6/pLVAGDjihkBgcH P5lMJrsBvKec8Z4qsrkcspWbRXGX67pnBzWvdTXhDzpVeGVcS1ARWGW8Fs7LZJBLll6AwcTjaBoY KDnOXViAV/qCLkwsBqv0uzOJAAADAwPvGxkZyQL4YC33KyK/y+VyL965c+dULfdbSuiDbnF4GL13 3om29uILm+5Np4FXvark9vL33IMTWwou6nrA3Y89BqeMoPNuuw1HlbHk9YMLC8Dpp5ccRwQcmDHx oZGRkQdU9RsAavGv5CWZTOYd9XQkt1/ogw6eh0MGB7F169aiwzKqsMvcZDweLznGmp5GOYvkWJEI hvr7S457ZHi4+BVmolUMDAxcPjw8/HsRuRzVe93gOIDzBwcHr6nS9jeMd12JQm7btm0PDAwMPEdE 3oDKvpgmKyKfzefzT6rnkAM2wxEdEUFEPABXqOp3k8nkK0TkHQBOwPoOdh4HcLFlWd/u6+sbq2ih VcKgI9pERCQP4LsAvvv4448PRiKRvwfwHADPAHAoAP8dOQUwLCJ/BHCHqv50YGDgriAn6K9H6IPO NDfjvmQSzdPFH86eymTQVM72Egn8fqz0P2LzsRjKeUe6G4vhd3v2lBy32NKCou9AJ1qjHTt2JAH8 731/AAAjIyMJY0wcABYXFxeGhobGRaT2a7JXWOiDLtrXB+fss0veGCgn5AAgevjhyB5+eMlx5YQc AESPOw7lTKRhyFEtDAwMpACkgq6j0ngzgohCj0FHRKHHoCOi0GPQEVHoMeiIKPQMsOKGZOjvxFJ9 GBkZWXEzWVULvwGZaJ0MAP/re4rPfieqkEgk0rFKc7lTjonKZlTVv5xK6SU3iCpjxWoGqhr4stsU PkZE/uZrOzKQSmjTEZEn+dsikYj/+0i0YREA9wNYvtDZ8RvZYDabnetrbh7+u7m50Q1VRnVLVE3E 8/6w4e2I+L9rI1u2bOERHVVcRERuVdV3LmvbNjExcVRPT89f17PBRCIxC+BZlSmPQu4038+/DaQK Cr1IPp+/ORKJKADZ36iq5wD4l+DKorCbnJx8qud5R/uafxFIMYS//e1v73Rd9xW+ZolGo1859NBD i74vthFE+vv7x1Op1K0icuL+RhF5o6p+Yt+SLkQV53neW31NrqpeF0gxBFXdOT8/f9LyNhGBZVmh +P/EAIAx5nJf+9Dk5OTrA6iHNoHx8fF+AG/xNf88kUiUfusQ0ToYAHBd93uqOrG8Q1X/NZ1Or/ac E9GGGGMuBHDQG4ZU9WsBlUObgAGA3t7ejIh8yde3TVU/HUBNFGITExOnAnijr/munp6enwRQDm0S B+a6ep73Zax8ccb56XT6lbUticIqlUoNALgKy2587fOBRluamxrLgaDr7e3NAHi3f4CqXjY5Ofnc mlZFoZNOpztE5MdYORviOz09Pb8MoibaPA5avaSnp+ffAfwf35hWz/N+PD4+fhKI1sG27W5VvQHA sb6uR6PR6LuCqIk2lxXLNGWz2QtE5G5fc5cx5saJiYk31qYsCot0On10Lpe7FUtvmlpuAcCrOzs7 JwMoizaZFUE3NDSU9TzvTBF5xNcVA3BpOp2+emxsrK825VGjUtXIxMTEu1T1DqycP+2o6qt7enpu D6I22nxWXXgzkUiMWJZ1GoCH/X2qeo5lWfdPTEx8aHJysrPqFVJDUVUzMTHxsnQ6fReAL8P3GAmA nKqel0gkrg+gPNqkCq4w3NXVtcdxnOcAWO1f3S4An/I87/GJiYlLJiYmTtuzZ4//xbe0SaiqTE5O PiWdTn80nU4/AOAaAE9ZZeg0gDMTicT3alshbXZFVxPu7+8ff+ihh07q6ur6jIi8GysfC+gA8GYA b47H4wsTExN/BPBXVd2LpQU9uYhiCIlIDEsLtPYDOCKdTh8LIFHiY7+3LOucLVu2PFrt+oj8Si6b vnv37kUA702lUteLyFcA/JcCQ2MATgBwgog/D2kTs0XkY1u3bv1qGN74To2p7JfjJBKJm7u7u48R kX8A8FAVa6JwmBWRz7uue0R3d/cXGXIUpDW9CGffl/USVb10YmLiDGPM61T1LABt1SmPGoynqr8T ke9EIpHvdXV1+ZfpJwrEut74JSIegJ8A+ImqNk1NTT3L87wTARwF4HAR6VfVdgAr3vJEq1NV8Txv 1SNsY4xXb1OkRGTK87xZEXkYwIMAbotGo7/hc3FUjzb8akMRyQG4Zd+furX3scd+JJa129/uOo5G m5qOGxwcnA+irv0eeeSRc1tbWy9f7fpmJpP51q5du94WQFlEobBp3uEqIrFIJLLai3/GpqamAn+R t2VZiEQiZrWgsyyL17eINiDwX3Aiompj0BFR6DHoiCj0GHREFHoMOiIKPQYdEYUeg46IQo9BR0Sh x6AjotBj0BFR6DHoiCj0GHREFHqbZlK/53kR13VTK9pdN9/d3R34Ekiqms5ms3cbY1ZM4HddNx1E TURh0dBBNz09fYfjOOW+elFUNbeitalJROSvExMTlS2ugpqbm980MTHxpnLGRqPRizs7Oz9Z7ZqI GklDB53jOFhYWBgKuo56Eo1Gm4Ougaje8BodEYUeg46IQo9BR0Sh19DX6CKRyHwsFvtL0HXUE8uy +OYtIp+GDrqurq6Tg66BiOofT12JKPQYdEQUegw6Igo9Bh0RhR6DjohCj0FHRKHHoCOi0GPQEVHo MeiIKPQYdEQUeg09BQwA9uzZ83YAz/e3i8g9hxxyCBegJCrPXCwWe2h5g6oaAHZA9VRUwwed53lP A/Aqf7uI8GiVqEy7d+/+KICPBl1HtTAMiCj0GHREFHoMOiIKPQYdEYUeg46IQo9BR0Shx6AjotBj 0BFR6DHoiCj0GHREFHoNPwWMNpepqakuEZGg66DK6+zsXBCRbDW2zaCjuqOq1szMzDHGmJMBPBvA EQAOA9AebGVUTbOzs7BtWwGMAXgIwF9V9ZZoNHpza2vrExvZNoOO6sbs7OxRqvqW2dnZc4wxg0HX Q4EQAP37/jxPRP7RcRzYtv17Vf2O4zhXdHd3r3lFFV6jo8BNT08fa9v2D1X1XgDvB8CQI7/jReSr 0Wj0Mdu2PzU1NdW1lg8z6Cgw09PTW2ZmZr5pjLkDwNng95FK6wLwIcuyHrBt+43lfoinrhQI27af KyJXq+qOIsMcAHeq6l0AHgQwJiLzxpi52lRJtaSqWzzPazHG7ABwlKo+B8AhBYb3ArjUtu2zVfUt nZ2dk8W2zaCjmrNt+x8AfFNVV/v+qYj83PO8K3K53HWJRGK21vVR/Zienj7MsqxzVPVNAA5dZchL ReRptm2f0dHR8UCh7fBUgWrKtu0PArgYq/8je50x5ph4PP7Czs7Oqxhy1NXV9XA8Hv9kPB4/QlVf C+CRVYbtAnDL9PT0Mwpth0FHNTMzM/MuABdi6c7aciOq+uKOjo6Xtre3/ymA0qjOiYjb2dn53Xg8 fjSWvkOub0iPMeaG2dnZo1b7PIOOasK27bNF5EurdN0sIk/r7Oz8ac2LooYjIgsdHR0fFpHTAaR9 3T2qeoNt293+zzHoqOqmp6d3AbgUK4/kfhCPx8+Ix+OpAMqiBhaPx28GcKKq7vV17QRwhaoe9F1j 0FHVici3AGzxtd0Uj8dfJyKLAZVFDa6jo+N+AKdgaSbFcmfOzs6+ZXkDg46qamZm5lwROdXX/Jds NvtyEckFUhSFRmdn599E5NVYehRpuYts2+7Z/wODjqpGVaMi8nFfc05EXt3b25sJpCgKnXg8/mtV /ZSveSuAD+z/gUFHVTM7O/taLN36X+7z8Xj83iDqofDq6Oj4NAD/c3Tn778xwaCjanq77+fJxcXF zwRSCYWaiCyKyD/7mttV9XUAg46qxLbtI7G0xNIBqvp1PgRM1dLe3n4tfEd1xpg3AAw6qp6zfD+r ql4WRCG0OYiIh6XHmA5Q1WPn5+e3M+ioWl7g+/n2rq6u1abvEFWM53nf97fl8/lTGHRULcf7fv5F IFXQptLV1bUHK+fDHs+go4qbnZ3the8BYQB/CKIW2nxU9aDvmogcyaCjivM8b8VyOiJScAkdogrz f9cO43p0VA2d/gZVHa/Gjtra2vqamrxTsHIeLTUO9bymn83MzExVaHv+udMdDDqqOGNMm6oe1BaP x6uyKrDjOE99zeneFcce6fK73KDufshyr77RvATAzyqxPRHxvzyHQUdVsdr3yr9+WMXsGvC8447y qrV5qrKZOdHSo8qnqq7v1b8Wr9ERUegx6Igo9Bh0RBR6DDoiCj0GHRGFHoOOiEKPQUdEocegI6LQ Y9ARUegx6Igo9Bh0RBR6DDoiCj0GHRGFXsOvXmKMmQdwv79dRBYCKIeI6lDDB92uXbveB+B9QddB RPWLp65EFHoMOiIKPQYdEYUeg46IQo9BR0Shx6AjotBj0BFR6DHoiCj0GHREFHoMOiIKPQYdEYUe g46IQq+hJ/WnUqkBAMe6rnu4iAwA2AIAqrogIsOqutfzvLsHBgYeEBEv2GqJKCgNF3RjY2MnAHgl gLM8zzsMAETkoDH7fxYRWJaF8fHxmbGxsZtU9dp8Pn/d0NBQttZ1E1FwGiLoVNWMj4+/FsD7ATx9 HZvoBPBKEXllU1NTenR09GLP874wODg4UdlKiage1f01ulQqdfL4+PgfAVyJ9YWcX7eIfNCyrIfG xsY+oKoNEfZEtH51G3R79uyJjY6OftXzvJsBPLUKu+gC8Nnx8fFbU6nUEVXYPhHViboMulQqNdDa 2nqziFwAQEp+YGOe5XnenaOjo39f5f0QUUDqLuhGRkYO8TzvtwCOr+Fu20Xk38fHx99Qw30SUY3U VdCl0+ntxphfAzhkLZ/bf3c1GokgGokgYlkwsuYDQUtVLxkdHT1nrR8kovpWNxfiR0dH2xzHuR7A jnLGG2MQa2pCNBKBZVmrjvFUkXcc5HI55B2nnM1aInLp2NjYo319fb8rv3oiqmd1c0QnIl8HcEyp ccYYtLe2oiseR6y5uWDIAYARQXM0inhbG7ricTRFo+WU0gzgB8lksqfs4omortVF0I2Ojp4NoOT1 sVhzMzrb28sNrIPsD8j21lYYU/KvPWhZ1lfWvBMiqkuBB92ePXtiIlIyVNpbW9Eai62YBbFWTdEo Otraygm7c8bGxk7f0M6IqC4EHnStra0XoMR1ubaWlnUdxRVijEG8rQ1WibBT1QtVtdqPtxBRlQUa dPfdd18TgPcVG9MSi6G5qani+7b2ncoWO0IUkWekUqm/q/jOiaimAg267u7uVwAYKNQfiUTQ0txc tf1bllVy+6r69qoVQEQ1EWjQGWOKPrPWGotVvYZYc3Op63Vn7t27d2vVCyGiqgks6MbHx9tV9bRC /fsf/K2FEkd1kaampjNqUggRVUVgQed53rMBFDxkq8Z1uUKaotGi1+pU9ZSaFUNEFRfYzAhjzDNV ddU+EUE0UrvS9u8vl88X6j9urdu88cYb22Kx2NM2XFwd8Dzvzy94wQsyQddBtF6BBZ2qPqlQn2XM hp+XW6uIZRUMOgBPVlWzluXYY7HYyf39/T8pNnOjEXieh/Hx8dcA+LegayFaryDnum4r1FHGw7wV VyKQomNjYz0Axte6zWgFn/8LglPeHGGiuhbkXdeCc0mDOAoq4+HhRI1KIaIKCzLoCqdZgWt3QRKR wGeRENH6BPnLW/h6V42vz61qKWz3J64u++9E1GCCvEaXKtThebV/Bavr3+dS2O5PXDHGrOmNYSIy MzMzc69lWQXvcDQCz/MirutOBV0H0UYEGXQjhTpc161lHQBKhmsukUis6UbESSeddCuAp2yoKCKq iMBOXUXkL4X6XM9DoWfsqqXECsQPruXREiKqL4EFnareWaSv3KXPK1VL0f2p6u01K4aIKi7IoPst gIVC/Yu5XM1qyTtO0SNIY8xvalYMEVVcYEHX398/p6q/LNSfdxw4NbpWN79QMG8BIL+4uPijmhRC RFUR9LNhVxXrLBFAFbGwuFj0RoSI3Dg0NDRZ9UKIqGoCDbp0On0NikyrchwH2cXFqu3fdd2S21fV r1WtACKqiUCD7uijj84B+HyxMdmFhapcr/NUkZmfL3V39+7e3t6bKr5zIqqpoE9dkcvlvgbgiWJj 5rJZLBZeWWTNPM+DncmsfEh4pQ+ICGdEEDW4wINuaGgoq6oXlBo3Nz+P+YWFDT9fl8vnYc/NlZx9 oarf7+vr+/mGdkZEdSHwoAOA/v7+6wBcWWrcwuIiZjKZYuvGFeR5HjLz88jMz5czxWzUcZzz17wT IqpLQU4BO4jrum+zLOspAJ5ebNz+wDLGINbUhGgkUnBZJ08V+XweecdZSzguAnjF9u3b02v6CxBR 3aqboBscHJxPJpNnW5b1GwA7S433PO/A4yciAmMMzL5VT1QVqlrONbgVmwXwlr6+vlvX+kEiql91 E3QAMDg4+PjY2NipAH4JYEe5n1NVuK6LDT5e7InIm3t7e7+zsc1Q2HlqMLFwFCDFF4gVOEjECk7p PmDe2YLx3Eklx0UxiW2t/1FyXNbtxlTuySXHNcs4umMPlBwXBnUVdADQ19f3cCqVOl5Vr1PVZ9Zo t7MAXt/b2/vDGu2PGtii0wL0fxgD23cXHbfnwduAfMn7bMjkh7DzmP+J1tbWouMe+vM1gFs66MYX jsOhz/pUyXGP3HM1usGgC0wikRjZu3fvydFo9CIRuQD/f124ilPVP0QikfN6enrur9Y+KHysiIWm Eq/ktCwDBLQaYanaACCAV7MEpm7/qkNDQ9n+/v53eZ53KoA/V2EXMwD+qa+v7wSGHFG41W3Q7Tcw MHBzb2/vsSLyRgB/qsAmJwFclM/nD+vr6/usiPA1V0QhV5enrn4i4gK4HMDlY2NjzxWRV6jq2QB2 lbmJDICbAFyXy+X+79DQULZatRJR/WmIoFtu36MftwJ4byqVGnBd95kADhWRIQDtAKCqOREZB/Co qt7d19f3Vx65UaVETB6TT/wCmfFfFR2XzS7s+0YW1x55Asl7P4dSsw2NMwXESm+vI/oo7r/9SyXH We4TZW0vDBou6JZLJBIjAK4Pug7aXKJWDrtbLi49sIyQA4DW6CRacW3pgWX+tm5pehBb8GDpgY39 bvU1qftrdEREG8WgI6LQY9ARUegx6Igo9Bh0RBR6DDqqOFVdsfb92NjYJrrHR0ESEf93Lcego4oz xmT8be3t7WU+bEG0Mara6WuyGXRUcY7jTK3Sti2IWmjzEZFBX9NUQz8wTPXJdd2HjG9pDBF5EoC7 Kr0vY8zslTdEJ677j0j1XwJMVWHPSczzPLuCmzzS9/NDVVv+iDY327afAHDgKE5EvhaPx98ZYEm0 CaiqzM7OjgFILGv+Ak9dqSpU9Vbfz6cHVQttHtPT00/HwSEHALcw6KgqVJGwyAAAA/RJREFUjDG/ 8DUdOT09fWwgxdCmYVnWa31Nrud5v2LQUVWIyHUADloxxhjz3wIqhzYBVW0BcN7yNhH5ZVdX1xSD jqqivb19DMDPfM1vmJ+f3x5EPRR+tm2/FUDf8jbP864A+MAwVZHneV/1NTW7rntRIMVQqNm23S0i /+xrHuno6LgGYNBRFXV1dd0E4Lblbar62pmZmRcHVBKFlIh8Gb6bEKp6kYhkgSq+XYsIAGzbPhHA b3Dwdy3tuu5xW7ZseSygsihE9p2y+ldCfTAejz9VRBYBHtFRlXV0dNwC4FJfc7dlWTfYtt0TRE0U HjMzMy8C8HVfsxpj3rE/5AAGHdVAPp9/L7Bibe+jAPx8bm5uIICSKARs236JMeYarFwU/kvt7e0/ X97AU1eqiUwm81TP827FyjcpPKaqr+rs7Lw9iLqo8eyb/fA+ABcBsHzdt8Tj8VNE5KBXh/OIjmqi vb39z8aY/wpg0de1U0RumZ2d/RdV3STvpKL1mp6ePnR2dvanAD6PlSF3j+d5Z/lDDuARHdWYbdsv EZF/U9XWVbofAfCpeDx+lYisWNOONq+5ublB13XfD+B8rP6Sxj8ZY17U3t4+utrnGXRUc7ZtnwDg hwB6CwwZFZHvqeq18Xj8Nobe5mTbdo+InKqqrwFwJoCm1caJyE25XO6V3d3dBVdAYdBRIObm5gYc x7lSRE4tNk5E5lX1XizdzBgVkYyqckmmcOoQkXZV3YGlm1VHoHhGOSLysfb29k+LiFdswww6Coyq im3b54nIZ+GbukNUwh2qekG5N7EYdBS4ycnJzmg0eoGqvhsrl9ghWu4OABfG4/HrRETL/RCDjuqG qrbYtv1SEXkdgFOx+kVnWiPV1fNApGF+/UdV9RoR+U5HR8fv1rOBhvmb0uaiqrFMJnO8qj4bwG4A h4lIXFW3BF3bctlsdmsul2vxt6uqtLa2TjU1Nc0FUdcyYtv2kOM4Kx4li0ajuXg8ngyiqALy+67B jorIQwD+iqXn4v6y0Q0z6Ig24NFHH/3a4uLiO/ztIoJYLPamHTt2XBZAWQeMjo62ZbPZv7mu2+/v i0ajv9i5c+dpQdRVa3xgmIhCj0FHRKHHoCOi0GPQEVHoMeiIKPQYdEQUegw6Igo9Bh0RhR6DjohC j0FHRKHHoCOi0GPQEVHoRYIugKjBGWPMaisei6oWXfW2FvL5vBeJRKZFZMbfZ4zxv6gotLh6CZFP KpW62HGcE8scbkTE/zYqAICqOgDKXhyynhlj/tDX1/f6oOtYLx7REfl4ntfquu5RQddRTyzLujfo GjaC1+iIKPQYdEQUegw6Igo9XqMjWoVlWXuDrqHONPSNy/8EScVb89IAHxkAAAAASUVORK5CYII="
            preserveAspectRatio="none"
            height={9.577652}
            width={8.8197317}
            x={180.98192}
            y={6.8226967}
            display="inline"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={180.5869}
            y={15.025524}
            id="text1329"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.5833px"
            fontFamily="sans-serif"
            display="inline"
            fill="#0ff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan id="tspan1327" x={180.5869} y={15.025524} fill="#0ff" strokeWidth={0.264583} fillOpacity={0}>
              <a href={arq_url} target="_blank" style={{ fill: 'white' }}>
                {'R'}{' '}
              </a>
            </tspan>
          </text>
          <path
            d="M205.02 7.472v12.074H326l8.982-5.705V1.966l-124.989.125z"
            id="path1333"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.658552}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M205.118 11.416V7.441l4.875-5.35h3.727"
            id="path1335"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.05149}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1337"
            d="M334.597 9.547v4.262l-7.172 5.737h-5.483"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.32058}
            strokeLinecap="round"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            id="text19495"
            y={13.0406}
            x={280.19876}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={13.0406}
              x={280.19876}
              id="tspan19493"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              <a href={plc_url} target="_blank" style={{ fill: 'white' }}>
                {"PLC'S"}{' '}
              </a>
            </tspan>
          </text>
          <path
            d="M456.468 29.19l-6.948-6.288-388.86-.044-2.406 1.878v.873l2.351 1.66c-.037 2.777-.154 4.236.02 6.172l2.604 3.1v1.66l-1.421 1.178V41.3h380.06l1.694-1.135 2.952 2.401h6.178l3.612-2.481z"
            id="path1142"
            opacity={0.2}
            fill="url(#linearGradient1158)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.419615}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path829-0"
            d="M456.53 29.255l-6.95-6.352-388.92-.045-2.406 1.897v.883l2.351 1.676c-.037 2.807.02 3.729.02 6.127l2.605 3.242v1.676l-1.422 1.19v1.942h380.118l1.695-1.147 2.953 2.426h6.179l3.613-2.507z"
            opacity={0.75}
            fill="none"
            fillOpacity={1}
            stroke="#04e6f4"
            strokeWidth={0.34636}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path831-8"
            d="M456.629 29.531l.037-1.95-5.105-4.844-2.4-.038z"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".251848px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path833-1"
            d="M61.533 27.214l-.117 6.227.795-.42V27.52z"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".121051px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            transform="matrix(1.26642 0 0 1.26654 -179.6 -87.605)"
            id="g6103"
            display="inline"
            strokeWidth={1.39987}
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M260.444 88.374l-12.62 12.458"
              id="path852-9"
              display="inline"
              strokeWidth={0.864463}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path d="M255.17 89.828l-6.212 5.99" id="path852-0-1" display="inline" strokeWidth=".404818px" />
            <path d="M257.861 94.466l-4.455 4.302" id="path852-0-7-2" display="inline" strokeWidth=".404818px" />
          </g>
          <path
            id="rect2077-1"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M66.550186 37.199219H71.830685V39.4765215H66.550186z"
          />
          <path
            id="rect2077-7"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M66.550186 34.11528H71.830685V36.392582499999996H66.550186z"
          />
          <path
            id="rect2077-1-2"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M66.550186 31.031359H71.830685V33.3086615H66.550186z"
          />
          <path
            id="rect2077-9"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M66.550186 27.947443H71.830685V30.2247455H66.550186z"
          />
          <path
            id="rect2077-1-5"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M66.550186 24.863512H71.830685V27.1408145H66.550186z"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={74.996452}
            y={34.973381}
            id="text1049"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1047"
              x={74.996452}
              y={34.973381}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ESTADOS EQUIPOS'}
            </tspan>
          </text>
          <circle
            id="st_chill1"
            cx={192.76047}
            cy={32.950371}
            r={3.75}
            display="inline"
            opacity={1}
            fill="#1aea78"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            id="ellipse2176"
            cx={39.309525}
            cy={175.58884}
            rx={4.2296953}
            ry={4.0336714}
            transform="matrix(.50256 0 0 .44492 173.097 -46.91)"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            id="text1066"
            y={34.973381}
            x={155.4294}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={34.973381}
              x={155.4294}
              id="tspan1064"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'OPERATIVO'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={212.04965}
            y={34.973381}
            id="text1086"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1084"
              x={212.04965}
              y={34.973381}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ALARMA PREVENTIVA'}
            </tspan>
          </text>
          <circle
            r={3.75}
            cy={32.950371}
            cx={279.01443}
            id="circle1088"
            display="inline"
            opacity={1}
            fill="#fc0"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            transform="matrix(.50256 0 0 .44492 259.351 -46.91)"
            ry={4.0336714}
            rx={4.2296953}
            cy={175.58884}
            cx={39.309525}
            id="ellipse1090"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            id="text1094"
            y={34.973381}
            x={296.18716}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={34.973381}
              x={296.18716}
              id="tspan1092"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ALARMA EMERGENTE'}
            </tspan>
          </text>
          <circle
            id="circle1096"
            cx={363.15323}
            cy={32.950371}
            r={3.75}
            display="inline"
            opacity={1}
            fill="red"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            id="ellipse1098"
            cx={39.309525}
            cy={175.58884}
            rx={4.2296953}
            ry={4.0336714}
            transform="matrix(.50256 0 0 .44492 343.49 -46.91)"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.79678}
            y={34.973381}
            id="text1102"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.87777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1100"
              x={379.79678}
              y={34.973381}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'APAGADO/STAND-BY'}
            </tspan>
          </text>
          <circle
            r={3.75}
            cy={32.950371}
            cx={447.82123}
            id="circle1104"
            display="inline"
            opacity={1}
            fill="#4d4d4d"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.0833686}
            strokeLinecap="round"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
          <ellipse
            transform="matrix(.50256 0 0 .44492 428.156 -46.91)"
            ry={4.0336714}
            rx={4.2296953}
            cy={175.58884}
            cx={39.309525}
            id="ellipse1106"
            display="inline"
            opacity={0.29}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.01348}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2091-9-7)"
          />
          <path
            id="path1112"
            d="M201.62 22.881l6.314 2.353h98.488l6.744-2.513"
            fill="#0ceef7"
            fillOpacity={0.74902}
            stroke="none"
            strokeWidth=".21253px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path1140"
            clipPath="url(#clipPath2072)"
            transform="matrix(.259 0 0 .26502 92.726 17.371)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(.259 0 0 .26502 178.981 17.371)"
            clipPath="url(#clipPath2072)"
            id="path1166"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path1168"
            clipPath="url(#clipPath2072)"
            transform="matrix(.259 0 0 .26502 263.12 17.371)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            transform="matrix(.259 0 0 .26502 347.786 17.371)"
            clipPath="url(#clipPath2072)"
            id="path1170"
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#0deff7"
            strokeWidth={1.03457}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
