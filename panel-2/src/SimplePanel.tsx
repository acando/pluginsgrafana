import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
//import styleEstado from 'elementos/Estado';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  //nombre
  let data_repla: any = data.series.find(({ refId }) => refId === 'G')?.name;
  let nom_on: any = data_repla.replace(/[.*+?^${}()|[\]\\]/g, '');
  console.log(data_repla, nom_on);

  ////LINKS
  let url = '';
  switch (nom_on) {
    case 'PDU-01B-F5':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-01B-F6':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-02B-F5':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-02B-F6':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    default:
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
  }

  /*let cur = [];
  for (let i = 1; i <= 42; i++) {
    cur[i] = data.series.find(
      ({ name }) => name === 'Average DATA.CURR_1.NOM.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (cur[i] === null || cur[i] === 0) {
      cur[i] = 0;
    } else {
      cur[i] = cur[i].toFixed(1);
    }
  }

  let kva = [];
  for (let i = 1; i <= 42; i++) {
    kva[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KVA_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kva[i] === null || kva[i] === 0) {
      kva[i] = 0;
    } else {
      kva[i] = kva[i].toFixed(1);
    }
  }

  let kw = [];
  for (let i = 1; i <= 42; i++) {
    kw[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KW_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kw[i] === null || kw[i] === 0) {
      kw[i] = 0;
    } else {
      kw[i] = kw[i].toFixed(1);
    }
  }

  let kwh = [];
  for (let i = 1; i <= 42; i++) {
    kwh[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KWH_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kwh[i] === null || kwh[i] === 0) {
      kwh[i] = 0;
    } else {
      kwh[i] = (kwh[i] ).toFixed(1);
    }
  }


  let v = [];
  for (let i = 1; i <= 42; i++){
    if(cur[i]===0){
      v[i]=0;
    }else{
    v [i] = (kva [i]/cur[i]).toFixed(1);
    }
  }
  //console.log(kw[32],kwh[32]);*/

  let cur2 = [];
  for (let i = 1; i <= 42; i++) {
    cur2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.CURR_2.NOM.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (cur2[i] === null || cur2[i] === 0) {
      cur2[i] = 0;
    } else {
      cur2[i] = cur2[i].toFixed(1);
    }
  }

  let kvat2 = [];
  for (let i = 1; i <= 42; i++) {
    kvat2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KVA_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kvat2[i] === null || kvat2[i] === 0) {
      kvat2[i] = 0;
    } else {
      kvat2[i] = (kvat2[i] * 100).toFixed(1);
    }
  }

  let kw2 = [];
  for (let i = 1; i <= 42; i++) {
    kw2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KW_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kw2[i] === null || kw2[i] === 0) {
      kw2[i] = 0;
    } else {
      kw2[i] = (kw2[i] / 10).toFixed(1);
    }
  }

  /*for (let i = 1; i <= 42; i++) {
    kwh2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KWH_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kwh2[i] === null || kwh2[i] === 0 || kwh2[i] < 0) {
      kwh2[i] = 0;
    } else {
      kwh2[i] = (kwh2[i]).toFixed(1);
    }
  }*/

  let kwh2 = [];
  for (let i = 1; i <= 42; i++) {
    if (kwh2[i] === null || kwh2[i] === 0 || kwh2[i] < 0) {
      kwh2[i] = 0;
    } else {
      kwh2[i] = (kw2[i] * 60).toFixed(1);
    }
  }

  let vt2 = [];
  let v2 = [];
  let kva2 = [];
  for (let i = 1; i <= 42; i++) {
    if (cur2[i] === 0) {
      vt2[i] = 0;
    } else {
      vt2[i] = (kvat2[i] / cur2[i]).toFixed(1);
    }
    kva2[i] = (kvat2[i] / 1000).toFixed(1);
    v2[i] = vt2[i];
    if (v2[i] > 127) {
      v2[i] = (126).toFixed(1);
    }
  }

  ////// ETIQUETAS
  let etq1 = 'RESERVA';
  let etq2 = 'RESERVA';
  let etq3 = 'RESERVA';
  let etq4 = 'RESERVA';
  let etq5 = 'RESERVA';
  let etq6 = 'RESERVA';
  let etq7 = 'RESERVA';
  let etq8 = 'RESERVA';
  let etq9 = 'RESERVA';
  let etq10 = 'RESERVA';
  let etq11 = 'RESERVA';
  let etq12 = 'RESERVA';
  let etq13 = 'RESERVA';
  let etq14 = 'RESERVA';

  if (nom_on === 'PDU-01B-F3') {
    etq1 = 'F3-10-A2';
    etq2 = 'F3-11-A1';
    etq3 = 'F3-13-A1';
    etq4 = 'F3-15-A1';
    etq5 = 'F3-17-A1';
    etq8 = 'F3-12-A1';
    etq9 = 'F3-12-A2';
    etq10 = 'F3-14-A1';
    etq11 = 'F3-16-A1';
    etq12 = 'F1-03-A2';
  }

  if (nom_on === 'PDU-01B-F4') {
    etq1 = 'F4-07-A1';
    etq2 = 'F4-05-A1';
    etq8 = 'F4-06-A1';
    etq9 = 'F4-04-A1';
    etq11 = 'F4-15-A3';
  }

  if (nom_on === 'PDU-01B-F5') {
    etq1 = 'F5-01-A1';
    etq2 = 'F5-03-A1';
    etq3 = 'F5-08-A1';
    etq4 = 'F5-05-A1';
    etq7 = 'F4-09-A1';
    etq8 = 'F5-02-A1';
    etq10 = 'F5-04-A1';
    etq11 = 'F5-15-A1';
  }

  if (nom_on === 'PDU-01B-F6') {
    etq1 = 'F6-01-A1';
    etq2 = 'F6-03-A3';
    etq3 = 'F6-05-A1';
    etq4 = 'F6-03-A2';
    etq5 = 'F6-14-A1';
    etq8 = 'F6-02-A1';
    etq9 = 'F6-03-A1';
    etq11 = 'F6-04-A1';
  }

  if (nom_on === 'PDU-01B-F9') {
    etq1 = 'F9-01-A1';
    etq2 = 'F9-03-A3';
    etq3 = 'F9-05-A1';
    etq4 = 'F9-07-A1';
    etq5 = 'F9-09-A1';
    etq6 = 'F9-14-A1';
    etq7 = 'F9-15-A1';
    etq8 = 'F9-02-A1';
    etq9 = 'F9-04-A1';
    etq10 = 'F9-06-A1';
    etq11 = 'F9-08-A1';
    etq12 = 'F9-10-A1';
    etq13 = 'F9-11-A1';
    etq14 = 'F9-19-A1';
  }

  ///// SISTEMA 2 //////
  if (nom_on === 'PDU-02B-F3') {
    etq1 = 'F3-08-A1';
    etq2 = 'F2-13-A1';
    etq3 = 'F3-09-A1';
    etq4 = 'F3-07-A1';
    etq5 = 'F1-03-A2';
    etq6 = 'F3-10-A1';
    etq7 = 'F2-02-A1';
    etq8 = 'F3-16-A1';
    etq9 = 'F3-14-A1';
    etq10 = 'F3-12-A2';
    etq11 = 'F3-16-A1';
    etq12 = 'F2-01-A1';
    etq13 = 'F2-02-A1';
    etq14 = 'F2-03-A1';
  }

  if (nom_on === 'PDU-02B-F4') {
    etq1 = 'F4-07-A1';
    etq2 = 'F4-05-A1';
    etq8 = 'F4-06-A1';
    etq9 = 'F4-04-A1';
    etq11 = 'F4-15-A3';
  }

  if (nom_on === 'PDU-02B-F5') {
    etq3 = 'F5-08-A1';
    etq10 = 'F5-04-A1';
    etq11 = 'F5-05-A1';
    etq12 = '';
    etq13 = 'F5-14-A1';
    etq14 = 'F5-15-A1';
  }

  if (nom_on === 'PDU-02B-F6') {
    etq4 = 'F6-05-A1';
    etq5 = 'F6-14-A1';
    etq10 = 'F6-04-A1';
  }

  if (nom_on === 'PDU-02B-F9') {
    etq1 = 'F9-01-A2';
    etq2 = 'F9-02-A2';
    etq3 = 'F9-05-A1';
    etq4 = 'F9-07-A1';
    etq5 = 'F9-09-A1';
    etq6 = 'F9-11-A1';
    etq7 = 'F9-18-A1';
    etq8 = 'F9-02-A1';
    etq9 = 'F9-04-A1';
    etq10 = 'F9-06-A1';
    etq11 = 'F9-08-A1';
    etq12 = 'F9-10-A1';
    etq13 = 'F9-15-A1';
    etq14 = 'F9-20-A1';
  }

  /*let imagen = [];
  let imagen1 = 0;
  let equipo = [
    'PDU-01B-F2',
    'PDU-01B-F3',
    'PDU-01B-F4',
    'PDU-01B-F5',
    'PDU-01B-F6',
    'PDU-01B-F7',
    'PDU-01B-F8',
    'PDU-01B-F9',
    'PDU-02B-F2',
    'PDU-02B-F3',
    'PDU-02B-F4',
    'PDU-02B-F5',
    'PDU-02B-F6',
    'PDU-02B-F7',
    'PDU-02B-F8',
    'PDU-02B-F9',
  ];

  for (let i = 0; i <= equipo.length; i++) {
    console.log(nom_on, equipo[i]);
    if (nom_on === equipo[i]) {
      imagen[i] = 1;
      console.log('entro');
    } else {
      imagen[i] = 0;
    }
    if (nom_on === 'PDU-01B-F7' || nom_on === 'PDU-01B-F8' || nom_on === 'PDU-02B-F7' || nom_on === 'PDU-02B-F8') {
      imagen1 = 1;
    }
  }*/

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        id="svg67790"
        viewBox="0 0 436.56249 211.66667"
        height={'100%'}
        width={'100%'}
        //{...props}
      >
        <defs id="defs67784">
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-20">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-25"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-71">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-33"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68418">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68416"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68422">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68420"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68426">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68424"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68430">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68428"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68434">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68432"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68438">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68436"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-04"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-27"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-10">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-54">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-52"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68514">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68512"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68518">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68516"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68522">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68520"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68526">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68524"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68530">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68528"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68534">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68532"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-20-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-25-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-2-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-5-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-71-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-4-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68578" clipPathUnits="userSpaceOnUse">
            <path
              id="path68576"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68582" clipPathUnits="userSpaceOnUse">
            <path
              id="path68580"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68586" clipPathUnits="userSpaceOnUse">
            <path
              id="path68584"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68590" clipPathUnits="userSpaceOnUse">
            <path
              id="path68588"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-45-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-2-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-31-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-04-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-27-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-10-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-4-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-7-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-54-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-52-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68652" clipPathUnits="userSpaceOnUse">
            <path
              id="path68650"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68656" clipPathUnits="userSpaceOnUse">
            <path
              id="path68654"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-80"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68700">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68698"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68704">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68702"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68708">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68706"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68712">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68710"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-69"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-13"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68756">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68754"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68760">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68758"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-68-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-80-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-7-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-4-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-65-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5-6-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0-7-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6-7-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68804" clipPathUnits="userSpaceOnUse">
            <path
              id="path68802"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68808" clipPathUnits="userSpaceOnUse">
            <path
              id="path68806"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68812" clipPathUnits="userSpaceOnUse">
            <path
              id="path68810"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68816" clipPathUnits="userSpaceOnUse">
            <path
              id="path68814"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-49-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-69-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-88-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-13-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68860" clipPathUnits="userSpaceOnUse">
            <path
              id="path68858"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68864" clipPathUnits="userSpaceOnUse">
            <path
              id="path68862"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-1-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04-8-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24-5-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71-6-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-9-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9-9-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1-4-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7-0-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-48"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-42">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-90"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68912">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68910"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68916">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68914"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68934">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68932"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68938">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68936"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-47"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-32">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-18">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-07"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68982">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68980"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68986">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68984"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-3-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-48-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-42-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-90-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69012" clipPathUnits="userSpaceOnUse">
            <path
              id="path69010"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69016" clipPathUnits="userSpaceOnUse">
            <path
              id="path69014"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69034" clipPathUnits="userSpaceOnUse">
            <path
              id="path69032"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69038" clipPathUnits="userSpaceOnUse">
            <path
              id="path69036"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1-6-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69060">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69058"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69064">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69062"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69082">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69080"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69086">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69084"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69090">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69088"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69094">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69092"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69098">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69096"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69102">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69100"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-15">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-33"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-93">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-22">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-87"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-83"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-51">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69164">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69162"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69168">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69166"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69172">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69170"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69176">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69174"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69180">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69178"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69184">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69182"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-65">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-06"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-13">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-71"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-26"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69264">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69262"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69268">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69266"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69272">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69270"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69276">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69274"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69294">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69292"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69298">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69296"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69316">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69314"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69320">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69318"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69338">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69336"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69342">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69340"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69346">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69344"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69350">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69348"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69354">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69352"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69358">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69356"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69362">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69360"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69366">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69364"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69384">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69382"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69388">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69386"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69406">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69404"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69410">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69408"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69428">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69426"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69432">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69430"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69436">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69434"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69440">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69438"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69444">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69442"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69448">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69446"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-08">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-39"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-73">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-90"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-90"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69510">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69508"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69514">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69512"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69518">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69516"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69522">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69520"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69526">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69524"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69530">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69528"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69548">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69546"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69552">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69550"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-45">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-37"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-05">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-94"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69614">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69612"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69618">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69616"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69622">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69620"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69626">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69624"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69644">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69642"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69648">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69646"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69666">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69664"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69670">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69668"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69688">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69686"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69692">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69690"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69696">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69694"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69700">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69698"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69718">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69716"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69722">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69720"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69740">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69738"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69744">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69742"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69748">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69746"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69752">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69750"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69756">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69754"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69760">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69758"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69778">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69776"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69782">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69780"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69800">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69798"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69804">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69802"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69822">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69820"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69826">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69824"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69844">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69842"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69848">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69846"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69852">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69850"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69856">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69854"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69860">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69858"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69864">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69862"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69868">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69866"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69872">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69870"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69898">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69896"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69902">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69900"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-99">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-36"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-10">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69942">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69940"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69946">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69944"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-98"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70008">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70006"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70012">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70010"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70016">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70014"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70020">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70018"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70024">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70022"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70028">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70026"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70046">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70044"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70050">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70048"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-18">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-57">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70130">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70128"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70134">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70132"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70138">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70136"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70142">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70140"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70146">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70144"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath70150">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path70148"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <filter
            id="filter21611-1-1-4-7-3"
            x={-0.044324357}
            width={1.0886487}
            y={-0.052340381}
            height={1.1046808}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.05935181} id="feGaussianBlur21613-4-0-4-9-1" />
          </filter>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-2-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-3-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84700">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84698"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84704">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84702"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-99-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-36-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-10-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-1-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84744">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84742"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84748">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84746"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-7-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-9-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-4-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-5-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-1-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-7-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-98-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-5-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-4-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-8-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-2-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84810">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84808"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84814">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84812"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84818">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84816"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84822">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84820"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84826">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84824"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84830">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84828"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84848">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84846"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84852">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84850"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-6-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-18-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-0-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-0-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-0-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-57-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-8-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-1-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-7-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-87">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84932">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84930"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84936">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84934"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84940">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84938"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84944">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84942"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84948">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84946"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath84952">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path84950"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-8-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-08-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-39-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-4-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-3-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-73-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-90-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-0-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-90-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-7-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-6-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-1-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-9-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-1-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94010">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94008"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94014">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94012"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94018">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94016"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94022">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94020"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94026">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94024"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94030">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94028"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94048">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94046"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94052">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94050"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-45-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-4-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-9-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-1-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-4-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-3-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-5-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-8-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-9-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-37-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-2-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-8-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-05-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-94-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-2-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94114">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94112"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94118">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94116"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94122">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94120"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94126">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94124"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94144">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94142"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94148">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94146"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94166">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94164"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94170">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94168"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94188">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94186"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94192">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94190"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94196">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94194"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94200">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94198"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94218">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94216"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94222">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94220"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94240">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94238"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94244">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94242"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94248">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94246"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94252">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94250"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94256">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94254"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94260">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94258"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94278">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94276"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94282">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94280"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94300">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94298"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94304">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94302"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94322">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94320"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94326">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94324"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94344">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94342"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94348">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94346"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94352">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94350"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94356">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94354"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94360">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94358"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94364">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94362"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94368">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94366"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath94372">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path94370"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-15-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-33-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-6-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-9-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-93-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-2-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-22-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-87-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-1-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-83-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-51-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97384">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97382"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97388">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97386"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97392">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97390"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97396">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97394"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97400">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97398"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97404">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97402"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-65-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-06-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-3-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-1-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-7-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-9-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-1-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-5-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-6-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-13-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-71-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-8-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-26-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-8-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-6-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-6-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-9-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97484">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97482"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97488">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97486"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97492">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97490"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97496">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97494"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97514">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97512"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97518">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97516"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97536">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97534"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97540">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97538"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97558">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97556"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97562">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97560"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97566">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97564"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97570">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97568"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97574">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97572"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97578">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97576"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97582">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97580"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97586">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97584"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97604">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97602"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97608">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97606"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97626">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97624"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97630">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97628"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97648">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97646"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97652">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97650"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97656">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97654"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97660">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97658"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97664">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97662"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath97668">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path97666"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-3-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-48-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-42-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-90-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12709" clipPathUnits="userSpaceOnUse">
            <path
              id="path12707"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12713" clipPathUnits="userSpaceOnUse">
            <path
              id="path12711"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12731" clipPathUnits="userSpaceOnUse">
            <path
              id="path12729"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12735" clipPathUnits="userSpaceOnUse">
            <path
              id="path12733"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-4-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-47-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-9-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-32-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-18-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-07-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-1-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12779" clipPathUnits="userSpaceOnUse">
            <path
              id="path12777"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12783" clipPathUnits="userSpaceOnUse">
            <path
              id="path12781"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3-9-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-48-3-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-42-6-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-90-8-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-5-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7-7-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2-3-18">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1-6-48"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12809">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12807"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12813">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12811"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12831">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12829"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12835">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12833"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8-5-1-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7-7-6-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2-3-1-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1-6-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12857" clipPathUnits="userSpaceOnUse">
            <path
              id="path12855"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12861" clipPathUnits="userSpaceOnUse">
            <path
              id="path12859"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12879" clipPathUnits="userSpaceOnUse">
            <path
              id="path12877"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12883" clipPathUnits="userSpaceOnUse">
            <path
              id="path12881"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12887" clipPathUnits="userSpaceOnUse">
            <path
              id="path12885"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12891" clipPathUnits="userSpaceOnUse">
            <path
              id="path12889"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12895" clipPathUnits="userSpaceOnUse">
            <path
              id="path12893"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12899" clipPathUnits="userSpaceOnUse">
            <path
              id="path12897"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-68-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-80-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-7-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-4-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-65-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-9-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-0-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5-6-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1-7-91"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0-7-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6-7-03"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14651" clipPathUnits="userSpaceOnUse">
            <path
              id="path14649"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14655" clipPathUnits="userSpaceOnUse">
            <path
              id="path14653"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14659" clipPathUnits="userSpaceOnUse">
            <path
              id="path14657"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14663" clipPathUnits="userSpaceOnUse">
            <path
              id="path14661"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-49-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-69-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-88-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-13-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71-60"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14707" clipPathUnits="userSpaceOnUse">
            <path
              id="path14705"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14711" clipPathUnits="userSpaceOnUse">
            <path
              id="path14709"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68-7-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-80-1-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7-6-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-4-8-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65-3-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-9-3-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-0-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-7-6-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6-4-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1-7-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0-7-4-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6-7-0-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14755">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14753"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14759">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14757"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14763">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14761"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14767">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14765"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-69-0-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88-7-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-13-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-1-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24-5-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71-6-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-9-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9-9-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7-0-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14811">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14809"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14815">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14813"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5-1-1-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04-8-8-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24-5-5-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71-6-4-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3-9-8-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9-9-8-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1-4-8-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7-0-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-9-59" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-6-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-20-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-25-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-2-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-5-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-71-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-9-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-3-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-33-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2-72"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16961" clipPathUnits="userSpaceOnUse">
            <path
              id="path16959"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16965" clipPathUnits="userSpaceOnUse">
            <path
              id="path16963"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16969" clipPathUnits="userSpaceOnUse">
            <path
              id="path16967"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16973" clipPathUnits="userSpaceOnUse">
            <path
              id="path16971"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16977" clipPathUnits="userSpaceOnUse">
            <path
              id="path16975"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath16981" clipPathUnits="userSpaceOnUse">
            <path
              id="path16979"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-45-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-2-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-31-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-04-56"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-2-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-27-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-10-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-8-47"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-4-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-54-99" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-52-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-3-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-6-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-8-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17057" clipPathUnits="userSpaceOnUse">
            <path
              id="path17055"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17061" clipPathUnits="userSpaceOnUse">
            <path
              id="path17059"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17065" clipPathUnits="userSpaceOnUse">
            <path
              id="path17063"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17069" clipPathUnits="userSpaceOnUse">
            <path
              id="path17067"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17073" clipPathUnits="userSpaceOnUse">
            <path
              id="path17071"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17077" clipPathUnits="userSpaceOnUse">
            <path
              id="path17075"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9-5-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-6-7-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-20-6-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-25-0-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2-1-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-5-0-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-71-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-4-2-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2-3-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5-0-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17121">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17119"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17125">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17123"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17129">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17127"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17133">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17131"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-2-4-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31-9-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-04-5-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-27-1-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-10-4-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-8-4-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4-2-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-7-7-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-54-9-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-52-5-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-1-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5-9-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17195">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17193"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17199">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17197"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-79"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-66">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-29"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-03">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-5-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-99"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-0-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-6-92">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-3-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-2-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-2-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-6-26">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-4-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-4-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-9-82"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-4-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-6-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-55"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-7-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-22"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-58">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-0-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-1-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-16"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-14">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-8-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-35"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-69"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-37">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-79">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-41">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-8-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-7-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-2-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-0-63"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-3-76">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-5-02"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-3-19"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-6-04">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-1-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-6-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-2-55"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-8-0-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-7-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-2-57-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-0-0-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-3-40-51">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-5-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-9-8-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-3-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-84-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-31-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-5-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-6-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-0-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-68">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-49">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-61"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-16">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-06"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-68"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-37"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-4-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-46">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-58">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-8-6-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-06-4-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-5-6-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-7-4-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-6-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-3-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-2-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-2-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-7-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-9-6-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-9-9-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-0-8-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-6-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-4-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-2-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-7-3-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-9-8-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-7-2-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-7-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-8-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-5-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-3-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-7-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-9-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-7-55">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-3-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-96"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-53"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-7-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-90"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-85">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-8-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-2-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-0-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-5-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-9-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-6-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-1-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-6-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-2-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-7-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-9-5-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-7-5-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-3-0-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-27">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-78"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-20">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-38"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-47">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-4-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-31"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-6-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-66"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-34"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-32"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-6-52">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-2-82">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-2-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-6-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-4-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-4-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-9-22"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-59">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-22"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-61">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-5-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-58">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-14"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-55">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-18"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-88"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-72"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-70"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-26">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-91"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-06"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-39">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-02"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-30">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-87"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-00"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-8-52">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-7-97"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-0-29"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-3-78">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-5-47"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-9-96">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-3-04"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-1-3-3-6-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-3-2-7-1-12"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-7-9-8-6-07">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-67-7-1-2-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-8-0-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-7-7-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-2-57-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-0-0-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-3-40-59">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-5-0-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-9-8-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-3-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-1-1-1-84-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-1-4-9-31-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-56-4-4-5-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-7-6-4-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-5-9-0-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-8-7-7-4-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-3-9-8-2-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-2-6-4-0-68"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-78">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-30"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-55">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-62"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient id="linearGradient3629">
            <stop id="stop3625" offset={0} stopColor="#fff" stopOpacity={1} />
            <stop id="stop3627" offset={1} stopColor="#37b4d7" stopOpacity={1} />
          </linearGradient>
          <clipPath id="clipPath3307-2-7-3-1-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-2-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-0-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-3-01"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30075" clipPathUnits="userSpaceOnUse">
            <path
              id="path30073"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30079" clipPathUnits="userSpaceOnUse">
            <path
              id="path30077"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-99-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-4-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-1-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-36-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-10-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-1-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-9-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-7-41"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30119" clipPathUnits="userSpaceOnUse">
            <path
              id="path30117"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30123" clipPathUnits="userSpaceOnUse">
            <path
              id="path30121"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-7-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-9-60"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-4-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-5-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-1-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-7-09" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-98-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-5-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-4-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-8-82" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-2-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-23"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30186" clipPathUnits="userSpaceOnUse">
            <path
              id="path30184"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30190" clipPathUnits="userSpaceOnUse">
            <path
              id="path30188"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30194" clipPathUnits="userSpaceOnUse">
            <path
              id="path30192"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30198" clipPathUnits="userSpaceOnUse">
            <path
              id="path30196"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30202" clipPathUnits="userSpaceOnUse">
            <path
              id="path30200"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30206" clipPathUnits="userSpaceOnUse">
            <path
              id="path30204"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30224" clipPathUnits="userSpaceOnUse">
            <path
              id="path30222"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30228" clipPathUnits="userSpaceOnUse">
            <path
              id="path30226"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-2-05" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-18-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-0-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-3-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-0-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-2-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-0-22"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-57-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-8-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-8-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-3-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-1-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-7-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-7-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-3-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-11"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-79"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30308" clipPathUnits="userSpaceOnUse">
            <path
              id="path30306"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30312" clipPathUnits="userSpaceOnUse">
            <path
              id="path30310"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30316" clipPathUnits="userSpaceOnUse">
            <path
              id="path30314"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30320" clipPathUnits="userSpaceOnUse">
            <path
              id="path30318"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30324" clipPathUnits="userSpaceOnUse">
            <path
              id="path30322"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath30328" clipPathUnits="userSpaceOnUse">
            <path
              id="path30326"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient33323"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient33325"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-9-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-2-7-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0-5-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-3-01-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-4-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-4-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-3-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3692">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3690"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3696">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3694"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-99-1-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-4-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-36-3-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-4-10"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-4-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-3-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-10-2-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-1-0-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-9-0-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-7-41-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-9-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-2-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-3-01-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-4-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath5148">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path5146"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath5152">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path5150"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient5780"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient5782"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient5812"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient5814"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6506"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6508"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6510"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6512"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6514"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6516"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6709"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6711"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6811"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient6813"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient7298"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient7300"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient7364"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient7366"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8408"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8410"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8412"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8414"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8416"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8418"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8424"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8426"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8428"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8430"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8432"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient8434"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10008"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10010"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10012"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10014"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10020"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10022"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10024"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient10026"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11021"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11023"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11025"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11027"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11033"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11035"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11037"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient11039"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12070"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12072"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12074"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12076"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12082"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12084"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12086"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12088"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12090"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient12092"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13523"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13525"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13527"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13529"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13535"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13537"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13539"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13541"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13543"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient13545"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14974"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14976"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14978"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14980"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14982"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient14984"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient15181"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient15183"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient15382"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient15384"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16362"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16364"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16366"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16368"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16370"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16372"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16374"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16376"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16378"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16380"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16382"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16384"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16386"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient16388"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17568"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17570"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17572"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17574"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17580"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17582"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17584"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17586"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17592"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient17594"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18773"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18775"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18785"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18787"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18789"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient18791"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19878"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19880"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19882"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19884"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19890"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19892"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19894"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient19896"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31411"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31413"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31514"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31516"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31588"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31590"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31715"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31717"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31789"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient31791"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient35576"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient35578"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient47288"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient47290"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient48646"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient48648"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient49544"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient49546"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
        </defs>
        <g id="layer1">
          <g id="1b_f9" transform="translate(8.996)" opacity={1}>
            <g id="g14535">
              <g id="g13111" transform="translate(15.357 145.64)">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13085"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13087"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13089"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13091"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13093"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13095"
                  fill="none"
                  stroke="url(#linearGradient13523)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13097"
                  fill="none"
                  stroke="url(#linearGradient13525)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13099"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13101"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13103"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13105"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13107"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13109"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.6437988}
                y={64.398384}
                id="etiqueta2"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.6437988}
                  y={64.398384}
                  style={{}}
                  id="etiqueta2"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq2}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="etiqueta1"
                y={41.455215}
                x={9.6433334}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54777px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta1"
                  style={{}}
                  y={41.455215}
                  x={9.6433334}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq1}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13123"
                y={89.013039}
                x={9.6433334}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta3"
                  style={{}}
                  y={89.013039}
                  x={9.6433334}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq3}
                </tspan>
              </text>
              <g id="g13149" transform="translate(15.357 142.465)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13125"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13127"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13129"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13131"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13133"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13135"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13137"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13139"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13141"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13143"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13145"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13147"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13175" transform="translate(15.357 93.782)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13151"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13153"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13155"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13157"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13159"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13161"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13163"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13165"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13167"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13169"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13171"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13173"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g transform="translate(15.357 194.324)" id="g13203">
                <path
                  id="path13177"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13179"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13181"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13183"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13185"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13187"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13527)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13189"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13529)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13191"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13193"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13195"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13197"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13199"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13201"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13207"
                y={111.82534}
                x={9.6437988}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta4"
                  style={{}}
                  y={111.82534}
                  x={9.6437988}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq4}
                </tspan>
              </text>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.6433334}
                y={135.40907}
                id="text13211"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.6433334}
                  y={135.40907}
                  style={{}}
                  id="etiqueta5"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq5}
                </tspan>
              </text>
              <g transform="translate(15.357 190.09)" id="g13237" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13213"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13215"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13217"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13219"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13221"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13223"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13225"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13227"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13229"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13231"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13233"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13235"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13265" transform="translate(15.65 242.478)">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13239"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13241"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13243"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13245"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13247"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13249"
                  fill="none"
                  stroke="url(#linearGradient13531)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13251"
                  fill="none"
                  stroke="url(#linearGradient13533)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13253"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13255"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13257"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13259"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13261"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13263"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.9439859}
                y={158.73724}
                id="text13269"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.9439859}
                  y={158.73724}
                  style={{}}
                  id="etiqueta6"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq6}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13273"
                y={182.83675}
                x={9.943778}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta7"
                  style={{}}
                  y={182.83675}
                  x={9.943778}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq7}
                </tspan>
              </text>
              <g id="g13299" transform="translate(15.357 238.774)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13275"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13277"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13279"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13281"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13283"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13285"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13287"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13289"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13291"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13293"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13295"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13297"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
            </g>
            <g id="g14425">
              <g id="g13325" transform="matrix(-1 0 0 1 404.74 117.139)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13301"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13303"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13305"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13307"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13309"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13311"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13313"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13315"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13317"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13319"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13321"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13323"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13353" transform="matrix(-1 0 0 1 404.4 170.966)">
                <path
                  id="path13327"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13329"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13331"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13333"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13335"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13337"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13535)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13339"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13537)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13341"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13343"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13345"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13347"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13349"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13351"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <g transform="matrix(-1 0 0 1 404.4 122.283)" id="g13381">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13355"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13357"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13359"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13361"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13363"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13365"
                  fill="none"
                  stroke="url(#linearGradient13539)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13367"
                  fill="none"
                  stroke="url(#linearGradient13541)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13369"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13371"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13373"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13375"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13377"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13379"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78024}
                y={41.868366}
                id="text13385"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78024}
                  y={41.868366}
                  style={{}}
                  id="etiqueta8"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq8}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13389"
                y={64.694275}
                x={391.78024}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta9"
                  style={{}}
                  y={64.694275}
                  x={391.78024}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq9}
                </tspan>
              </text>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.7803}
                y={89.295418}
                id="text13393"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.7803}
                  y={89.295418}
                  style={{}}
                  id="etiqueta10"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq10}
                </tspan>
              </text>
              <g transform="matrix(-1 0 0 1 404.74 165.822)" id="g13419" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13395"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13397"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13399"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13401"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13403"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13405"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13407"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13409"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13411"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13413"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13415"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13417"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78024}
                y={112.12059}
                id="text13423"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78024}
                  y={112.12059}
                  style={{}}
                  id="etiqueta11"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq11}
                </tspan>
              </text>
              <g transform="matrix(-1 0 0 1 404.4 218.591)" id="g13451">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13425"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13427"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13429"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13431"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13433"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13435"
                  fill="none"
                  stroke="url(#linearGradient13543)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13437"
                  fill="none"
                  stroke="url(#linearGradient13545)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13439"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13441"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13443"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13445"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13447"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13449"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13455"
                y={135.69116}
                x={391.7803}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta12"
                  style={{}}
                  y={135.69116}
                  x={391.7803}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq12}
                </tspan>
              </text>
              <g id="g13481" transform="matrix(-1 0 0 1 404.74 213.977)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13457"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13459"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13461"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13463"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13465"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13467"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13469"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13471"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13473"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13475"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13477"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13479"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13485"
                y={159.03304}
                x={391.78024}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta13"
                  style={{}}
                  y={159.03304}
                  x={391.78024}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq13}
                </tspan>
              </text>
              <g id="g13513" transform="matrix(-1 0 0 1 404.4 267.275)">
                <path
                  id="path13487"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13489"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13491"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13493"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13495"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13497"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13547)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13499"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13549)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13501"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13503"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13505"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13507"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13509"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13511"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78061}
                y={183.11919}
                id="text13517"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78061}
                  y={183.11919}
                  style={{}}
                  id="etiqueta14"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq14}
                </tspan>
              </text>
            </g>
          </g>
          <g
            transform="translate(8.996) matrix(.0028 -.83112 1.20318 .00405 0 0)"
            id="deshabilitado"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.28128px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.259528}
            opacity={0}
          >
            <text
              id="text3280-3"
              y={40.470913}
              x={-190.32112}
              style={{
                lineHeight: 1.25,
              }}
              xmlSpace="preserve"
              display="inline"
            >
              <tspan
                style={{}}
                y={40.470913}
                x={-190.32112}
                id="tspan3276-8"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="11.2889px"
                fontFamily="Franklin Gothic Medium"
                fill="#00abd6"
                fillOpacity={1}
                strokeWidth={0.259528}
              >
                {'EQUIPO DES HABILITADO'}
              </tspan>
            </text>
            <text
              xmlSpace="preserve"
              style={{
                lineHeight: 1.25,
              }}
              x={-189.01515}
              y={308.91052}
              id="text30036"
              display="inline"
            >
              <tspan
                id="tspan30032"
                x={-189.01515}
                y={308.91052}
                style={{}}
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="11.2889px"
                fontFamily="Franklin Gothic Medium"
                fill="#00abd6"
                fillOpacity={1}
                strokeWidth={0.259528}
              >
                {'EQUIPO DES HABILITADO'}
              </tspan>
            </text>
          </g>
          <path
            d="M95.926 55.938v-8.607l-3.294-2.573V22.893l-2.028-1.583v-2.573l7.73-5.936h18.756l5.196 4.155H326.37l4.816-3.76h11.279l4.182 3.068v4.056l-6.59 5.046v17.322l6.623 5.248v12.418l-3.987 2.903v135.67l2.106 1.363-.09 1.96-6.586 5.036h-28.179l-3.943-2.938H119.636l-2.24-1.889h-9.454l-2.285 1.924h-10.35l-2.285-1.924V57.94z"
            id="path1767"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M92.086 187.525l-1.165-1.501v-9.89l2.844-2.9h4.755v12.065l-1.865 2.226z"
            id="path1769"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M93.987 43.676v-19.7l3.213 2.697v19.99z"
            id="path1771"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M100.487 15.338l1.654-1.45h13.067l4.337 4.246h206.444l-.685.984H104.48z"
            id="path1773"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M346.398 21.733v8.069l-5.38 4.396V26.13z"
            id="path1775"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M341.165 37.483v4.518l5.35 4.444v-4.71z"
            id="path1777"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M341.047 34.923v1.86l5.558 4.276v-4.88l-3.459-2.778z"
            id="path1779"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M346.605 35.382v-4.638l-2.897 2.295z"
            id="path1781"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M344.667 199.556l-1.208-.943.088-134.653 1.742-1.18v1.722l-.477.26z"
            id="path1783"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M342.209 61.817l3.365-2.336V48.364l-3.194-2.378z"
            id="path1785"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1787"
            d="M120.593 200.258h202.774l7.333 6.223h-3.53l-5.044-4.127h-9.061v-.938H121.902z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1789"
            d="M324.676 200.202h3.1l7.301 6.282-2.976.006z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            transform="scale(.84731 1.1802)"
            id="text1799"
            y={29.050831}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1791"
              style={{}}
              y={29.050831}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'1'}
            </tspan>
            <tspan
              style={{}}
              y={35.781006}
              x={132.74138}
              id="tspan1793"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'3'}
            </tspan>
            <tspan
              style={{}}
              y={42.511181}
              x={132.74138}
              id="tspan1795"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'5'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={29.04401}
            id="pn1_cur1"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1801"
              x={147.70213}
              y={29.04401}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[1]}
            </tspan>
          </text>
          <path
            d="M328.91 200.202h3l7.065 6.21-2.88.007z"
            id="path1807"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            transform="matrix(0 50.68693 -.32705 0 288.611 -2750.934)"
            id="path1809"
            d="M54.82 213.162h3.214v2.721h-3.15z"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.175829}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4-7-3)"
          />
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur3"
            y={35.906147}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.906147}
              x={147.70213}
              id="tspan1811"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[3]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur5"
            y={42.768269}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.768269}
              x={147.70213}
              id="tspan1817"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[5]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur7"
            y={49.630398}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.630398}
              x={147.70213}
              id="tspan1823"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[7]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur9"
            y={56.49255}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.49255}
              x={147.70213}
              id="tspan1829"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[9]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur11"
            y={63.35471}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.35471}
              x={147.70213}
              id="tspan1835"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[1]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur13"
            y={70.216881}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.216881}
              x={147.70213}
              id="tspan1841"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[13]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur15"
            y={77.079102}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.079102}
              x={147.70213}
              id="tspan1847"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[15]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur17"
            y={83.941284}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.941284}
              x={147.70213}
              id="tspan1853"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[17]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur19"
            y={90.803421}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.803421}
              x={147.70213}
              id="tspan1859"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[19]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur21"
            y={97.665588}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.665588}
              x={147.70213}
              id="tspan1865"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[21]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur23"
            y={104.52769}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.52769}
              x={147.70213}
              id="tspan1871"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[23]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur25"
            y={111.38982}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.38982}
              x={147.70213}
              id="tspan1877"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[25]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur27"
            y={118.25198}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.25198}
              x={147.70213}
              id="tspan1883"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[27]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur29"
            y={125.11412}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.11412}
              x={147.70213}
              id="tspan1889"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[29]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur31"
            y={131.97623}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.97623}
              x={147.70213}
              id="tspan1895"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[31]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur33"
            y={138.83829}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.83829}
              x={147.70213}
              id="tspan1901"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[33]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur35"
            y={145.70039}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.70039}
              x={147.70213}
              id="tspan1907"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[35]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur37"
            y={152.56253}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.56253}
              x={147.70213}
              id="tspan1913"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[37]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur39"
            y={159.42468}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.42468}
              x={147.70213}
              id="tspan1919"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[39]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur41"
            y={166.28683}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.28683}
              x={147.70213}
              id="tspan1925"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[41]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva1"
            y={29.0229}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={29.0229}
              x={193.1982}
              id="tspan1931"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[1]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={35.885036}
            id="pn1_kva3"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1937"
              x={193.1982}
              y={35.885036}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[3]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={42.74715}
            id="pn1_kva5"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1943"
              x={193.1982}
              y={42.74715}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[5]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={49.609272}
            id="pn1_kva7"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1949"
              x={193.1982}
              y={49.609272}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[7]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={56.471401}
            id="pn1_kva9"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1955"
              x={193.1982}
              y={56.471401}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[9]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={63.333553}
            id="pn1_kva11"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1961"
              x={193.1982}
              y={63.333553}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[11]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={70.195702}
            id="pn1_kva13"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1967"
              x={193.1982}
              y={70.195702}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[13]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={77.05793}
            id="pn1_kva15"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1973"
              x={193.1982}
              y={77.05793}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[15]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={83.92012}
            id="pn1_kva17"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1979"
              x={193.1982}
              y={83.92012}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[17]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={90.782333}
            id="pn1_kva19"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1985"
              x={193.1982}
              y={90.782333}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[19]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={97.644516}
            id="pn1_kva21"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1991"
              x={193.1982}
              y={97.644516}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[21]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={104.50671}
            id="pn1_kva23"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1997"
              x={193.1982}
              y={104.50671}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[23]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={111.3689}
            id="pn1_kva25"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2003"
              x={193.1982}
              y={111.3689}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[25]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={118.23106}
            id="pn1_kva27"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2009"
              x={193.1982}
              y={118.23106}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[27]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={125.0932}
            id="pn1_kva29"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2015"
              x={193.1982}
              y={125.0932}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[29]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={131.95528}
            id="pn1_kva31"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2021"
              x={193.1982}
              y={131.95528}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[31]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={138.81732}
            id="pn1_kva33"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2027"
              x={193.1982}
              y={138.81732}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[33]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={145.67931}
            id="pn1_kva35"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2033"
              x={193.1982}
              y={145.67931}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[35]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={152.54131}
            id="pn1_kva37"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2039"
              x={193.1982}
              y={152.54131}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[37]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={159.40338}
            id="pn1_kva39"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2045"
              x={193.1982}
              y={159.40338}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[39]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={166.26544}
            id="pn1_kva41"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2051"
              x={193.1982}
              y={166.26544}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[41]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw1"
            y={28.998089}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={28.998089}
              x={215.80942}
              id="tspan2057"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[1]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={35.860226}
            id="pn1_kw3"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2063"
              x={215.80942}
              y={35.860226}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[3]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={42.72234}
            id="pn1_kw5"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2069"
              x={215.80942}
              y={42.72234}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[5]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={49.584476}
            id="pn1_kw7"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2075"
              x={215.80942}
              y={49.584476}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[7]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={56.446621}
            id="pn1_kw9"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2081"
              x={215.80942}
              y={56.446621}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[9]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={63.308796}
            id="pn1_kw11"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2087"
              x={215.80942}
              y={63.308796}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[11]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={70.170952}
            id="pn1_kw13"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2093"
              x={215.80942}
              y={70.170952}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[13]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={77.03318}
            id="pn1_kw15"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2099"
              x={215.80942}
              y={77.03318}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[15]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={83.89534}
            id="pn1_kw17"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2105"
              x={215.80942}
              y={83.89534}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[17]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={90.757507}
            id="pn1_kw19"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2111"
              x={215.80942}
              y={90.757507}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[19]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={97.619644}
            id="pn1_kw21"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2117"
              x={215.80942}
              y={97.619644}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[21]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={104.48181}
            id="pn1_kw23"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2123"
              x={215.80942}
              y={104.48181}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[23]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={111.34392}
            id="pn1_kw25"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2129"
              x={215.80942}
              y={111.34392}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[25]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={118.20607}
            id="pn1_kw27"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2135"
              x={215.80942}
              y={118.20607}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[27]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={125.06822}
            id="pn1_kw29"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2141"
              x={215.80942}
              y={125.06822}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[29]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={131.93028}
            id="pn1_kw31"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2147"
              x={215.80942}
              y={131.93028}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[31]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={138.79234}
            id="pn1_kw33"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2153"
              x={215.80942}
              y={138.79234}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[33]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={145.65446}
            id="pn1_kw35"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2159"
              x={215.80942}
              y={145.65446}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[35]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={152.51659}
            id="pn1_kw37"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2165"
              x={215.80942}
              y={152.51659}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[37]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={159.37874}
            id="pn1_kw39"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2171"
              x={215.80942}
              y={159.37874}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[39]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={166.24089}
            id="pn1_kw41"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2177"
              x={215.80942}
              y={166.24089}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[41]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh1"
            y={29.106876}
            x={237.5686}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={29.106876}
              x={237.5686}
              id="tspan2183"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[1]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={35.969013}
            id="pn1_kwh3"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2189"
              x={237.5686}
              y={35.969013}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[3]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={42.831142}
            id="pn1_kwh5"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2195"
              x={237.5686}
              y={42.831142}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[5]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={49.693272}
            id="pn1_kwh7"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2201"
              x={237.5686}
              y={49.693272}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[7]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={56.555416}
            id="pn1_kwh9"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2207"
              x={237.5686}
              y={56.555416}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[9]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={63.417591}
            id="pn1_kwh11"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2213"
              x={237.5686}
              y={63.417591}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[11]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={70.279747}
            id="pn1_kwh13"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2219"
              x={237.5686}
              y={70.279747}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[13]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={77.14196}
            id="pn1_kwh15"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2225"
              x={237.5686}
              y={77.14196}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[15]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={84.004135}
            id="pn1_kwh17"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2231"
              x={237.5686}
              y={84.004135}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[17]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={90.866287}
            id="pn1_kwh19"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2237"
              x={237.5686}
              y={90.866287}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[19]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={97.728424}
            id="pn1_kwh21"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2243"
              x={237.5686}
              y={97.728424}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[21]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={104.59058}
            id="pn1_kwh23"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2249"
              x={237.5686}
              y={104.59058}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[23]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={111.4527}
            id="pn1_kwh25"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2255"
              x={237.5686}
              y={111.4527}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[25]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={118.31485}
            id="pn1_kwh27"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2261"
              x={237.5686}
              y={118.31485}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[27]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={125.177}
            id="pn1_kwh29"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2267"
              x={237.5686}
              y={125.177}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[29]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={132.03908}
            id="pn1_kwh31"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2273"
              x={237.5686}
              y={132.03908}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[31]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={138.90115}
            id="pn1_kwh33"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2279"
              x={237.5686}
              y={138.90115}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[33]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={145.76328}
            id="pn1_kwh35"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2285"
              x={237.5686}
              y={145.76328}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[35]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={152.6254}
            id="pn1_kwh37"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2291"
              x={237.5686}
              y={152.6254}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[37]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={159.48756}
            id="pn1_kwh39"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2297"
              x={237.5686}
              y={159.48756}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[39]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={237.5686}
            y={166.3497}
            id="pn1_kwh41"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2303"
              x={237.5686}
              y={166.3497}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[41]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur2"
            y={28.912685}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={28.912685}
              x={288.24213}
              id="tspan2309"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[2]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={35.774822}
            id="pn1_cur4"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2315"
              x={288.24213}
              y={35.774822}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[4]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={42.636944}
            id="pn1_cur6"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2321"
              x={288.24213}
              y={42.636944}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[6]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={49.499081}
            id="pn1_cur8"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2327"
              x={288.24213}
              y={49.499081}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[8]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={56.361225}
            id="pn1_cur10"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2333"
              x={288.24213}
              y={56.361225}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[10]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={63.223392}
            id="pn1_cur12"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2339"
              x={288.24213}
              y={63.223392}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[12]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={70.085556}
            id="pn1_cur14"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2346"
              x={288.24213}
              y={70.085556}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[14]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={76.947762}
            id="pn1_cur16"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2352"
              x={288.24213}
              y={76.947762}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[16]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={83.809937}
            id="pn1_cur18"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2358"
              x={288.24213}
              y={83.809937}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[18]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={90.672089}
            id="pn1_cur20"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2364"
              x={288.24213}
              y={90.672089}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[20]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={97.534256}
            id="pn1_cur22"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2370"
              x={288.24213}
              y={97.534256}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[22]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={104.39639}
            id="pn1_cur24"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2376"
              x={288.24213}
              y={104.39639}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[24]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={111.25853}
            id="pn1_cur26"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2382"
              x={288.24213}
              y={111.25853}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[26]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={118.12064}
            id="pn1_cur28"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2388"
              x={288.24213}
              y={118.12064}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[28]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={124.9828}
            id="pn1_cur30"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2394"
              x={288.24213}
              y={124.9828}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[30]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={131.84489}
            id="pn1_cur32"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2400"
              x={288.24213}
              y={131.84489}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[32]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={138.70699}
            id="pn1_cur34"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2406"
              x={288.24213}
              y={138.70699}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[34]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={145.56905}
            id="pn1_cur36"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2412"
              x={288.24213}
              y={145.56905}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[36]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={152.4312}
            id="pn1_cur38"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2418"
              x={288.24213}
              y={152.4312}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[38]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={159.29335}
            id="pn1_cur40"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2424"
              x={288.24213}
              y={159.29335}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[40]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={166.15552}
            id="pn1_cur42"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2430"
              x={288.24213}
              y={166.15552}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur2[42]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={28.891567}
            id="pn1_kva2"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2436"
              x={333.73782}
              y={28.891567}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[2]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva4"
            y={35.753704}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.753704}
              x={333.73782}
              id="tspan2442"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[4]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva6"
            y={42.615833}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.615833}
              x={333.73782}
              id="tspan2448"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[6]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva8"
            y={49.47794}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.47794}
              x={333.73782}
              id="tspan2454"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[8]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva10"
            y={56.340084}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.340084}
              x={333.73782}
              id="tspan2460"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[10]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva12"
            y={63.202236}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.202236}
              x={333.73782}
              id="tspan2466"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[12]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva14"
            y={70.064369}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.064369}
              x={333.73782}
              id="tspan2472"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[14]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva16"
            y={76.926598}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={76.926598}
              x={333.73782}
              id="tspan2478"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[16]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva18"
            y={83.788788}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.788788}
              x={333.73782}
              id="tspan2484"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[18]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva20"
            y={90.651001}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.651001}
              x={333.73782}
              id="tspan2490"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[20]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva22"
            y={97.513184}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.513184}
              x={333.73782}
              id="tspan2496"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[22]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva24"
            y={104.37541}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.37541}
              x={333.73782}
              id="tspan2502"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[24]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva26"
            y={111.23756}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.23756}
              x={333.73782}
              id="tspan2508"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[26]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva28"
            y={118.09973}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.09973}
              x={333.73782}
              id="tspan2514"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[28]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva30"
            y={124.96188}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={124.96188}
              x={333.73782}
              id="tspan2520"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[30]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva32"
            y={131.82397}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.82397}
              x={333.73782}
              id="tspan2526"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[32]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva34"
            y={138.686}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.686}
              x={333.73782}
              id="tspan2532"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[34]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva36"
            y={145.54797}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.54797}
              x={333.73782}
              id="tspan2538"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[36]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva38"
            y={152.40999}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.40999}
              x={333.73782}
              id="tspan2544"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[38]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva40"
            y={159.27205}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.27205}
              x={333.73782}
              id="tspan2550"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[40]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva42"
            y={166.13411}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.13411}
              x={333.73782}
              id="tspan2556"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva2[42]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={28.866756}
            id="pn1_kw2"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2562"
              x={357.59726}
              y={28.866756}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[2]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw4"
            y={35.728893}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.728893}
              x={357.59726}
              id="tspan2568"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[4]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw6"
            y={42.591015}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.591015}
              x={357.59726}
              id="tspan2574"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[6]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw8"
            y={49.453144}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.453144}
              x={357.59726}
              id="tspan2580"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[8]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw10"
            y={56.315304}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.315304}
              x={357.59726}
              id="tspan2586"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[10]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw12"
            y={63.177464}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.177464}
              x={357.59726}
              id="tspan2592"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[12]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw14"
            y={70.039612}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.039612}
              x={357.59726}
              id="tspan2598"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[14]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw16"
            y={76.901855}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={76.901855}
              x={357.59726}
              id="tspan2604"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[16]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw18"
            y={83.764008}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.764008}
              x={357.59726}
              id="tspan2610"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[18]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw20"
            y={90.626175}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.626175}
              x={357.59726}
              id="tspan2616"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[20]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw22"
            y={97.488327}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.488327}
              x={357.59726}
              id="tspan2622"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[22]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw24"
            y={104.35046}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.35046}
              x={357.59726}
              id="tspan2628"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[24]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw26"
            y={111.21257}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.21257}
              x={357.59726}
              id="tspan2634"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[26]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw28"
            y={118.07472}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.07472}
              x={357.59726}
              id="tspan2640"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[28]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw30"
            y={124.93687}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={124.93687}
              x={357.59726}
              id="tspan2646"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[30]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw32"
            y={131.79897}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.79897}
              x={357.59726}
              id="tspan2652"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[32]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw34"
            y={138.66103}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.66103}
              x={357.59726}
              id="tspan2658"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[34]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw36"
            y={145.5231}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.5231}
              x={357.59726}
              id="tspan2664"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[36]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw38"
            y={152.38525}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.38525}
              x={357.59726}
              id="tspan2670"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[38]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw40"
            y={159.24741}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.24741}
              x={357.59726}
              id="tspan2676"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[40]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw42"
            y={166.10957}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.10957}
              x={357.59726}
              id="tspan2682"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw2[42]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={28.975544}
            id="pn1_kwh2"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2688"
              x={379.3558}
              y={28.975544}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[2]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh4"
            y={35.837681}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.837681}
              x={379.3558}
              id="tspan2694"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[4]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh6"
            y={42.69981}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.69981}
              x={379.3558}
              id="tspan2700"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[6]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh8"
            y={49.561939}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.561939}
              x={379.3558}
              id="tspan2706"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[8]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh10"
            y={56.424107}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.424107}
              x={379.3558}
              id="tspan2712"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[10]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh12"
            y={63.286266}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.286266}
              x={379.3558}
              id="tspan2718"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[12]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh14"
            y={70.148407}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.148407}
              x={379.3558}
              id="tspan2724"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[14]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh16"
            y={77.010628}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.010628}
              x={379.3558}
              id="tspan2730"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[16]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh18"
            y={83.872787}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.872787}
              x={379.3558}
              id="tspan2736"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[18]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh20"
            y={90.73497}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.73497}
              x={379.3558}
              id="tspan2742"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[20]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh22"
            y={97.597107}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.597107}
              x={379.3558}
              id="tspan2748"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[22]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh24"
            y={104.45924}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.45924}
              x={379.3558}
              id="tspan2754"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[24]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh26"
            y={111.32135}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.32135}
              x={379.3558}
              id="tspan2760"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[26]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh28"
            y={118.1835}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.1835}
              x={379.3558}
              id="tspan2766"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[28]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh30"
            y={125.04565}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.04565}
              x={379.3558}
              id="tspan2772"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[30]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh32"
            y={131.90778}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.90778}
              x={379.3558}
              id="tspan2778"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[32]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh34"
            y={138.76984}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.76984}
              x={379.3558}
              id="tspan2784"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[34]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh36"
            y={145.63191}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.63191}
              x={379.3558}
              id="tspan2790"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[36]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh38"
            y={152.49408}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.49408}
              x={379.3558}
              id="tspan2796"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[38]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh40"
            y={159.35622}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.35622}
              x={379.3558}
              id="tspan2802"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[40]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh42"
            y={166.21838}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.21838}
              x={379.3558}
              id="tspan2808"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh2[42]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={130.62051}
            y={21.818066}
            id="text2816"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2814"
              x={130.62051}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'CH'}
            </tspan>
          </text>
          <path
            id="path2822"
            d="M103.83 52.571h110.583"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908822}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908822,.908822"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M102.695 76.87H213.28"
            id="path2824"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2826"
            d="M102.695 101.211H213.28"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M102.695 125.553H213.28"
            id="path2828"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2830"
            d="M102.695 149.895H213.28"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M102.695 174.236H213.28"
            id="path2832"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 52.528h115.12"
            id="path2834"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927273}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927273,.927273"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2836"
            d="M222.288 76.87h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927275}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927275,.927275"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 101.211h115.12"
            id="path2838"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927274}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927274,.927274"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2840"
            d="M222.288 125.553h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927275}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927275,.927275"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 149.895h115.12"
            id="path2842"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927273}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927273,.927273"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2844"
            d="M222.288 174.237h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927274}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927274,.927274"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={49.675762}
            id="text2854"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={49.675762}
              style={{}}
              id="tspan2846"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'7'}
            </tspan>
            <tspan
              id="tspan2848"
              x={132.74138}
              y={56.405937}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'9'}
            </tspan>
            <tspan
              id="tspan2850"
              x={132.74138}
              y={63.136112}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'11'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2864"
            y={70.300766}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2856"
              style={{}}
              y={70.300766}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'13'}
            </tspan>
            <tspan
              style={{}}
              y={77.030945}
              x={132.74138}
              id="tspan2858"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'15'}
            </tspan>
            <tspan
              style={{}}
              y={83.761116}
              x={132.74138}
              id="tspan2860"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'17'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={90.925888}
            id="text2874"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={90.925888}
              style={{}}
              id="tspan2866"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'19'}
            </tspan>
            <tspan
              id="tspan2868"
              x={132.74138}
              y={97.656067}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'21'}
            </tspan>
            <tspan
              id="tspan2870"
              x={132.74138}
              y={104.38624}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'23'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2884"
            y={111.55093}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2876"
              style={{}}
              y={111.55093}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'25'}
            </tspan>
            <tspan
              style={{}}
              y={118.2811}
              x={132.74138}
              id="tspan2878"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'27'}
            </tspan>
            <tspan
              style={{}}
              y={125.01128}
              x={132.74138}
              id="tspan2880"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'29'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={132.17586}
            id="text2894"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={132.17586}
              style={{}}
              id="tspan2886"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'31'}
            </tspan>
            <tspan
              id="tspan2888"
              x={132.74138}
              y={138.90604}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'33'}
            </tspan>
            <tspan
              id="tspan2890"
              x={132.74138}
              y={145.6362}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'35'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2904"
            y={152.80061}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2896"
              style={{}}
              y={152.80061}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'37'}
            </tspan>
            <tspan
              style={{}}
              y={159.53079}
              x={132.74138}
              id="tspan2898"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'39'}
            </tspan>
            <tspan
              style={{}}
              y={166.26096}
              x={132.74138}
              id="tspan2900"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'41'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2910"
            y={35.751202}
            x={119.79497}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.751202}
              x={119.79497}
              id="tspan2906"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B1'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.79497}
            y={56.376133}
            id="text2916"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2912"
              x={119.79497}
              y={56.376133}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B3'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2922"
            y={77.001198}
            x={119.79497}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.001198}
              x={119.79497}
              id="tspan2918"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B5'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.79497}
            y={97.626312}
            id="text2928"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2924"
              x={119.79497}
              y={97.626312}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B7'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2934"
            y={118.25136}
            x={119.79497}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.25136}
              x={119.79497}
              id="tspan2930"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B9'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.17044}
            y={138.87619}
            id="text2940"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2936"
              x={119.17044}
              y={138.87619}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B11'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2946"
            y={159.50095}
            x={119.17044}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.50095}
              x={119.17044}
              id="tspan2942"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B13'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={29.050831}
            id="text2956"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={29.050831}
              style={{}}
              id="tspan2948"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'2'}
            </tspan>
            <tspan
              id="tspan2950"
              x={273.88287}
              y={35.781006}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'4'}
            </tspan>
            <tspan
              id="tspan2952"
              x={273.88287}
              y={42.511181}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'6'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2966"
            y={49.675762}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2958"
              style={{}}
              y={49.675762}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'8'}
            </tspan>
            <tspan
              style={{}}
              y={56.405937}
              x={273.88287}
              id="tspan2960"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'10'}
            </tspan>
            <tspan
              style={{}}
              y={63.136112}
              x={273.88287}
              id="tspan2962"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'12'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={70.300766}
            id="text2976"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={70.300766}
              style={{}}
              id="tspan2968"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'14'}
            </tspan>
            <tspan
              id="tspan2970"
              x={273.88287}
              y={77.030945}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'16'}
            </tspan>
            <tspan
              id="tspan2972"
              x={273.88287}
              y={83.761116}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'18'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2986"
            y={90.925888}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2978"
              style={{}}
              y={90.925888}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'20'}
            </tspan>
            <tspan
              style={{}}
              y={97.656067}
              x={273.88287}
              id="tspan2980"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'22'}
            </tspan>
            <tspan
              style={{}}
              y={104.38624}
              x={273.88287}
              id="tspan2982"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'24'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={111.55093}
            id="text2996"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={111.55093}
              style={{}}
              id="tspan2988"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'26'}
            </tspan>
            <tspan
              id="tspan2990"
              x={273.88287}
              y={118.2811}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'28'}
            </tspan>
            <tspan
              id="tspan2992"
              x={273.88287}
              y={125.01128}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'30'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3006"
            y={132.17586}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2998"
              style={{}}
              y={132.17586}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'32'}
            </tspan>
            <tspan
              style={{}}
              y={138.90604}
              x={273.88287}
              id="tspan3000"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'34'}
            </tspan>
            <tspan
              style={{}}
              y={145.6362}
              x={273.88287}
              id="tspan3002"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'36'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={152.80061}
            id="text3016"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={152.80061}
              style={{}}
              id="tspan3008"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'38'}
            </tspan>
            <tspan
              id="tspan3010"
              x={273.88287}
              y={159.53079}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'40'}
            </tspan>
            <tspan
              id="tspan3012"
              x={273.88287}
              y={166.26096}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'42'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.93674}
            y={35.751202}
            id="text3022"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3018"
              x={260.93674}
              y={35.751202}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B2'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3028"
            y={56.376133}
            x={260.93674}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.376133}
              x={260.93674}
              id="tspan3024"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B4'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.93674}
            y={77.001198}
            id="text3034"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3030"
              x={260.93674}
              y={77.001198}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B6'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3040"
            y={97.626312}
            x={260.93674}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.626312}
              x={260.93674}
              id="tspan3036"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B8'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.31223}
            y={118.25136}
            id="text3046"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3042"
              x={260.31223}
              y={118.25136}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B10'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3052"
            y={138.87619}
            x={260.31226}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.87619}
              x={260.31226}
              id="tspan3048"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B12'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.31226}
            y={159.50095}
            id="text3058"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3054"
              x={260.31226}
              y={159.50095}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B14'}
            </tspan>
          </text>
          <text
            transform="scale(.83112 1.20319)"
            id="text3280"
            y={10.208264}
            x={242.63895}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.28127px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.259528}
          >
            <tspan
              id="tspan3278"
              style={{}}
              y={10.208264}
              x={242.63895}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87777px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.259528}
            >
              <tspan
                id="tspan31213"
                style={{}}
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="5.28127px"
                fontFamily="Franklin Gothic Medium"
                fill="#00abd6"
                fillOpacity={1}
                strokeWidth={0.259528}
              />
              {'PANEL 2'}
            </tspan>
          </text>
          <path
            id="rect3705"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 53.460804H118.909421V75.400139H111.65446z"
          />
          <path
            id="rect3707"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 29.119104H118.909421V51.058439H111.65446z"
          />
          <path
            id="rect3709"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 77.802475H118.909421V99.74181H111.65446z"
          />
          <path
            id="rect3711"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 102.14417H118.909421V124.083505H111.65446z"
          />
          <path
            id="rect3713"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 126.48582H118.909421V148.42515500000002H111.65446z"
          />
          <path
            id="rect3715"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 150.82716H118.909421V172.766495H111.65446z"
          />
          <path
            id="rect3717"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 175.16849H118.909421V197.107825H111.65446z"
          />
          <path
            id="rect3719"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 53.460804H238.499771V75.400139H231.24481z"
          />
          <path
            id="rect3721"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 29.119104H238.499771V51.058439H231.24481z"
          />
          <path
            id="rect3723"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 77.802475H238.499771V99.74181H231.24481z"
          />
          <path
            id="rect3725"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 102.14417H238.499771V124.083505H231.24481z"
          />
          <path
            id="rect3727"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 126.48582H238.499771V148.42515500000002H231.24481z"
          />
          <path
            id="rect3729"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 150.82716H238.499771V172.766495H231.24481z"
          />
          <path
            id="rect3731"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 175.16849H238.499771V197.107825H231.24481z"
          />
          <text
            transform="scale(.84731 1.1802)"
            id="text84174"
            y={21.818066}
            x={150.60545}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={150.60545}
              id="tspan84172"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'A'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={172.46388}
            y={21.818066}
            id="text84178"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84176"
              x={172.46388}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'V'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84182"
            y={21.818066}
            x={192.44867}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={192.44867}
              id="tspan84180"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KVA'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={216.80522}
            y={21.818066}
            id="text84186"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84184"
              x={216.80522}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KW'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84190"
            y={21.818066}
            x={237.41446}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={237.41446}
              id="tspan84188"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KWH'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={29.0229}
            id="pn1_v1"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84192"
              x={169.46625}
              y={29.0229}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[1]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v3"
            y={35.885036}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.885036}
              x={169.46625}
              id="tspan84198"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[3]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v5"
            y={42.74715}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.74715}
              x={169.46625}
              id="tspan84204"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[5]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v7"
            y={49.609272}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.609272}
              x={169.46625}
              id="tspan84210"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[7]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v9"
            y={56.471401}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.471401}
              x={169.46625}
              id="tspan84216"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[9]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v11"
            y={63.333553}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.333553}
              x={169.46625}
              id="tspan84222"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[11]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v13"
            y={70.195702}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.195702}
              x={169.46625}
              id="tspan84228"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[13]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v15"
            y={77.05793}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.05793}
              x={169.46625}
              id="tspan84234"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[15]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v17"
            y={83.92012}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.92012}
              x={169.46625}
              id="tspan84240"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[17]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v19"
            y={90.782333}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.782333}
              x={169.46625}
              id="tspan84246"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[19]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v21"
            y={97.644516}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.644516}
              x={169.46625}
              id="tspan84252"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[21]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v23"
            y={104.50671}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.50671}
              x={169.46625}
              id="tspan84258"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[23]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v25"
            y={111.3689}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.3689}
              x={169.46625}
              id="tspan84264"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[25]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v27"
            y={118.23106}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.23106}
              x={169.46625}
              id="tspan84270"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[27]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v29"
            y={125.0932}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.0932}
              x={169.46625}
              id="tspan84276"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[29]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v31"
            y={131.95528}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.95528}
              x={169.46625}
              id="tspan84282"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[31]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v33"
            y={138.81732}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.81732}
              x={169.46625}
              id="tspan84288"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[33]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v35"
            y={145.67931}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.67931}
              x={169.46625}
              id="tspan84294"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[35]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v37"
            y={152.54131}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.54131}
              x={169.46625}
              id="tspan84300"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[37]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v39"
            y={159.40338}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.40338}
              x={169.46625}
              id="tspan84306"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[39]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v41"
            y={166.26544}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.26544}
              x={169.46625}
              id="tspan84312"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[41]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v2"
            y={29.0229}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={29.0229}
              x={309.98337}
              id="tspan84521"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[2]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={35.885036}
            id="pn1_v4"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84527"
              x={309.98337}
              y={35.885036}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[4]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={42.74715}
            id="pn1_v6"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84533"
              x={309.98337}
              y={42.74715}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[6]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={49.609272}
            id="pn1_v8"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84539"
              x={309.98337}
              y={49.609272}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[8]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={56.471401}
            id="pn1_v10"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84545"
              x={309.98337}
              y={56.471401}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[10]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={63.333553}
            id="pn1_v12"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84551"
              x={309.98337}
              y={63.333553}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[12]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={70.195702}
            id="pn1_v14"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84557"
              x={309.98337}
              y={70.195702}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[14]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={77.05793}
            id="pn1_v16"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84563"
              x={309.98337}
              y={77.05793}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[16]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={83.92012}
            id="pn1_v18"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84569"
              x={309.98337}
              y={83.92012}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[18]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={90.782333}
            id="pn1_v20"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84575"
              x={309.98337}
              y={90.782333}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[20]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={97.644516}
            id="pn1_v22"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84581"
              x={309.98337}
              y={97.644516}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[22]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={104.50671}
            id="pn1_v24"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84587"
              x={309.98337}
              y={104.50671}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[24]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={111.3689}
            id="pn1_v26"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84593"
              x={309.98337}
              y={111.3689}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[26]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={118.23106}
            id="pn1_v28"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84599"
              x={309.98337}
              y={118.23106}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[28]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={125.0932}
            id="pn1_v30"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84605"
              x={309.98337}
              y={125.0932}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[30]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={131.95528}
            id="pn1_v32"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84611"
              x={309.98337}
              y={131.95528}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[32]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={138.81732}
            id="pn1_v34"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84617"
              x={309.98337}
              y={138.81732}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[34]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={145.67931}
            id="pn1_v36"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84623"
              x={309.98337}
              y={145.67931}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[36]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={152.54131}
            id="pn1_v38"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84629"
              x={309.98337}
              y={152.54131}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[38]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={159.40338}
            id="pn1_v40"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84635"
              x={309.98337}
              y={159.40338}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[40]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={166.26544}
            id="pn1_v42"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84641"
              x={309.98337}
              y={166.26544}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v2[42]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84649"
            y={21.818066}
            x={271.13821}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={271.13821}
              id="tspan84647"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'CH'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={291.74728}
            y={21.818066}
            id="text84653"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84651"
              x={291.74728}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'A'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84657"
            y={21.818066}
            x={312.98093}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={312.98093}
              id="tspan84655"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'V'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={332.96545}
            y={21.818066}
            id="text84661"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84659"
              x={332.96545}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KVA'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84665"
            y={21.818066}
            x={357.94592}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={357.94592}
              id="tspan84663"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KW'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={379.17932}
            y={21.818066}
            id="text84669"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84667"
              x={379.17932}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KWH'}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
