import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
//import { stylesFactory, useTheme } from '@grafana/ui';
import { stylesFactory } from '@grafana/ui';
//import BotonEst from 'componentes/BotonEst';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  let valor1 = data.series.find(({ name }) => name === 'MAX')?.fields[1].state?.calcs?.max;
  let valor2 = data.series.find(({ name }) => name === 'NOM')?.fields[1].state?.calcs?.mean;
  let valor3 = data.series.find(({ name }) => name === 'MIN')?.fields[1].state?.calcs?.min;
  let lec_unidad: any = data.series.find(({ refId }) => refId === 'UNIDAD')?.name;
  let unidad: any = lec_unidad.replace(/[.*+?^{}()|[\]\\]/g, '');
  let lec_titulo: any = data.series.find(({ refId }) => refId === 'TITULO')?.name;
  let titulo: any = lec_titulo.replace(/[.*+?^${}()|[\]\\]/g, '');
  let lec_deci: any = data.series.find(({ refId }) => refId === 'DECIMAL')?.name;
  let deci: any = lec_deci.replace(/[.*+?^{}()|[\]\\]/g, '');
  let lec_op: any = data.series.find(({ refId }) => refId === 'OPCION')?.name;
  let op: any = lec_op.replace(/[.*+?^{}()|[\]\\]/g, '');
  let decimal=0
  let dato1=''
  let dato2=''
  let dato3=''

  
  if (deci === null) {
    decimal = 1;
  }else{
    decimal=parseInt(deci)
  }

  if (valor3 === null) {
    valor3 = 0;
  }else{
    valor3=valor3.toFixed(decimal)
  }
  if (valor2 === null) {
    valor2 = 0;
  }else{
    valor2=valor2.toFixed(decimal)
  }
  if (valor1 === null) {
    valor1 = 0;
  }else{
    valor1=valor1.toFixed(decimal)
  }

  switch (op) {
    case '1':
      dato1='MAX'
      dato2='NOM'
      dato3='MIN'
      break;
    case '2':
      dato1='MAX'
      dato2='MIN'
      dato3='TOT'
      valor2 = data.series.find(({ name }) => name === 'MAX')?.fields[1].state?.calcs?.firstNotNull;
      valor1 = data.series.find(({ name }) => name === 'MAX')?.fields[1].state?.calcs?.lastNotNull;
      if (valor2 === null) {
        valor2 = 0;
      }else{
        valor2=valor2.toFixed(decimal)
      }
      if (valor1 === null) {
        valor1 = 0;
      }else{
        valor1=valor1.toFixed(decimal)
      }
      valor3 = (parseFloat(valor1)-parseFloat(valor2)).toFixed(decimal)
      break;
    case '3':
      dato1='MAX'
      dato2='MIN'
      dato3='TOT'
      valor2 = data.series.find(({ name }) => name === 'MAX')?.fields[1].state?.calcs?.firstNotNull;
      valor1 = data.series.find(({ name }) => name === 'MAX')?.fields[1].state?.calcs?.lastNotNull;
      let nom1 = data.series.find(({ name }) => name === 'NOM')?.fields[1].state?.calcs?.firstNotNull;
      let max1 = data.series.find(({ name }) => name === 'NOM')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }else{
        valor1=valor1.toFixed(decimal)
      }
      if (valor2 === null) {
        valor2 = 0;
      }else{
        valor2=valor2.toFixed(decimal)
      }
      if (nom1 === null) {
        nom1 = 0;
      }else{
        nom1=nom1.toFixed(decimal)
      }
      if (max1 === null) {
        max1 = 0;
      }else{
        max1=max1.toFixed(decimal)
      }
      valor2 = (parseFloat(valor2)+parseFloat(nom1)).toFixed(decimal)
      valor1 = (parseFloat(valor1)+parseFloat(max1)).toFixed(decimal)
      valor3 = (parseFloat(valor1)-parseFloat(valor2)).toFixed(decimal)
      console.log("datosaaaa",valor2,valor1,nom1,max1)
      break;
    default:
      break;
  }
  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
    <svg
      width={"100%"}
      height={"100%"}
      viewBox="0 0 134.93748 71.437509"
      id="svg5"
      xmlns="http://www.w3.org/2000/svg"
      //{...props}
    >
      <defs id="defs2">
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath55423">
          <path
            d="M-6.879-250.07l-50.58-4.047-7.284 7.284h-324.12l-8.903-8.903-50.985 4.856V-6.474l51.794 3.237 6.88-8.093h325.334l7.688 8.497L-6.07-6.879z"
            id="path55425"
            fill="none"
            stroke="#000"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath id="clipPath1069" clipPathUnits="userSpaceOnUse">
          <ellipse
            ry={92.971107}
            rx={51.450279}
            cy={346.14044}
            cx={-248.095}
            id="ellipse1071"
            opacity={1}
            fill="olive"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={7.53068}
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="stroke fill markers"
          />
        </clipPath>
        <clipPath id="clipPath5494" clipPathUnits="userSpaceOnUse">
          <rect
            ry={1.2665578}
            y={373.44049}
            x={1007.6845}
            height={226.78572}
            width={102.80952}
            id="rect5496"
            fill="none"
            fillRule="evenodd"
            stroke="#61625b"
            strokeWidth={4.99999}
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
        </clipPath>
        <clipPath clipPathUnits="userSpaceOnUse" id="clipPath200219">
          <circle
            id="circle200221"
            cx={130.47343}
            cy={121.66941}
            r={76.152832}
            opacity={0.701923}
            fill="red"
            fillOpacity={1}
            strokeWidth={0.328671}
          />
        </clipPath>
      </defs>
      <g id="layer1">
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={15.617236}
          y={31.58062}
          id="text2745"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87778px"
          fontFamily="sans-serif"
          fill="#27647c"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={15.617236}
            y={31.58062}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            fill="#27647c"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {dato1+":"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={108.13866}
          y={31.701199}
          id="text2745-3"
          fontStyle="normal"
          fontWeight={400}
          fontSize="7.76111px"
          fontFamily="sans-serif"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-5"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={108.13866}
            y={31.701199}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Arial"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {unidad}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={108.13866}
          y={45.470467}
          id="text2745-3-5"
          fontStyle="normal"
          fontWeight={400}
          fontSize="7.76111px"
          fontFamily="sans-serif"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-5-5"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={108.13866}
            y={45.470467}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Arial"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {unidad}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={108.13866}
          y={58.832504}
          id="text2745-3-5-9"
          fontStyle="normal"
          fontWeight={400}
          fontSize="7.76111px"
          fontFamily="sans-serif"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-5-5-9"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={108.13866}
            y={58.832504}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="7.76111px"
            fontFamily="Arial"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {unidad}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={68.250061}
          y={17.98605}
          id="titulo"
          fontStyle="normal"
          fontWeight={400}
          fontSize="10.5833px"
          fontFamily="sans-serif"
          fill="#27647c"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-2"
            style={{
              //InkscapeFontSpecification: "'sans-serif Bold'",
              textAlign: "center"
            }}
            x={68.250061}
            y={17.98605}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={700}
            fontStretch="normal"
            fontFamily="sans-serif"
            textAnchor="middle"
            fill="#27647c"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {titulo}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={15.882541}
          y={45.349888}
          id="text2745-7"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87778px"
          fontFamily="sans-serif"
          fill="#27647c"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-8"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={15.882541}
            y={45.349888}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            fill="#27647c"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {dato2+":"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={16.170504}
          y={58.714512}
          id="text2745-7-3"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87778px"
          fontFamily="sans-serif"
          fill="#27647c"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-8-6"
            style={{
              //InkscapeFontSpecification: "Arial"
            }}
            x={16.170504}
            y={58.714512}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            fill="#27647c"
            fillOpacity={1}
            strokeWidth={0.264583}
          >
            {dato3+":"}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 0.45,
            //InkscapeFontSpecification: "Arial"
          }}
          x={105.27489}
          y={31.58062}
          id="max"
          fontStyle="normal"
          fontVariant="normal"
          fontWeight={400}
          fontStretch="normal"
          fontSize="9.87778px"
          fontFamily="Arial"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-1"
            style={{
              //InkscapeFontSpecification: "Arial",
              textAlign: "end"
            }}
            x={105.27489}
            y={31.58062}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {valor1}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={104.94595}
          y={45.349888}
          id="nom"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87778px"
          fontFamily="sans-serif"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-8-3"
            style={{
              //InkscapeFontSpecification: "Arial",
              textAlign: "end"
            }}
            x={104.94595}
            y={45.349888}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {valor2}
          </tspan>
        </text>
        <text
          xmlSpace="preserve"
          style={{
            lineHeight: 1.25
          }}
          x={105.22359}
          y={58.711926}
          id="min"
          fontStyle="normal"
          fontWeight={400}
          fontSize="9.87778px"
          fontFamily="sans-serif"
          fill="#fff"
          fillOpacity={1}
          stroke="none"
          strokeWidth={0.264583}
        >
          <tspan
            id="tspan2743-8-6-6"
            style={{
              //InkscapeFontSpecification: "Arial",
              textAlign: "end"
            }}
            x={105.22359}
            y={58.711926}
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="9.87778px"
            fontFamily="Arial"
            textAnchor="end"
            fill="#fff"
            strokeWidth={0.264583}
          >
            {valor3}
          </tspan>
        </text>
        <path
          d="M65.032 65.931l-2.439 2.439h-16.05l-2.237-2.238H18.958l-2.62 2.565-14.558-.109-.073-16.923 2.548-2.329v19.216V17.31L18.958 2.607h24.456l-2.475 2.475H16.63l2.51-2.511h36.066l2.675 2.674"
          id="path915"
          fill="none"
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M65.217 68.617l2.574-2.574h14.925l1.482 2.566h22.605l2.107-2.106h21.98V45.868l2.213-2.213V4.076l-2.341 2.342v21.127l2.226 2.226"
          id="path1618"
          fill="none"
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M82.922 2.635l2.265 2.47h7l2.47-2.161h37.16l-1.75 1.955h-18.735l-2.264-2.058"
          id="path1620"
          fill="none"
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.7 50.246l1.5-1.29v-12.94l-1.534-.944z"
          id="path1622"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M7.166 58.289v5.677h5.969z"
          id="path1624"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M18.084 68.697l1.238-1.383H43.85l1.383 1.383z"
          id="path1626"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M46.303 65.982h1.853l.962 1.667h-1.837z"
          id="path1630"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M49.432 65.982h1.853l.962 1.667H50.41z"
          id="path1630-6"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M52.56 65.982h1.854l.963 1.667h-1.838z"
          id="path1630-0"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M55.69 65.982h1.853l.963 1.667h-1.838z"
          id="path1630-8"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M58.819 65.982h1.853l.963 1.667h-1.838z"
          id="path1630-2"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M59.2 2.484h3.477l1.806 3.13h-3.448z"
          id="path1630-00"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth="1.01024px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M64.936 2.485h3.476l1.806 3.128H66.77z"
          id="path1630-6-7"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth="1.01024px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M70.671 2.484h3.476l1.807 3.13h-3.448z"
          id="path1630-0-5"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth="1.01024px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M76.406 2.485h3.477l1.806 3.128h-3.448z"
          id="path1630-8-4"
          fill="#ffb900"
          fillOpacity={0.886918}
          stroke="none"
          strokeWidth="1.01024px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.577 21.935l1.622.789v-4.417L1.58 19.54z"
          id="path1928"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.602 24.071v-1.286l1.597.922v1.342z"
          id="path1930"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.602 26.508V25.22l1.597.923v1.342z"
          id="path1930-8"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.602 28.944v-1.286l1.597.922v1.342z"
          id="path1930-8-6"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.602 31.38v-1.286l1.597.923v1.342z"
          id="path1930-8-7"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M1.602 33.817v-1.286l1.597.922v1.342z"
          id="path1930-8-3"
          fill="#e3a400"
          fillOpacity={1}
          stroke="none"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M123.137 7.168l5.101 5.513V7.112z"
          id="path1628-7"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
        <path
          d="M132.331 42.337V30.942l-1.955-1.853v15.132z"
          id="path3856"
          fill="#27647c"
          fillOpacity={1}
          stroke="#0ceef7"
          strokeWidth=".538449px"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeOpacity={0.85098}
        />
      </g>
    </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
