import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> { }

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  let fecha: any = data.timeRange.raw.from;
  let tiempo = '';
  let unidades = '';
  let h = 1;
  let d = 1;
  let data_repla: any = data.series.find(({ refId }) => refId === 'UNIDAD')?.name;
  let unidad: any = data_repla.replace(/[.*+?^{}()|[\]\\]/g, '');
  let data_repla2: any = data.series.find(({ refId }) => refId === 'OPCION')?.name;
  let opcion: any = data_repla2.replace(/[.*+?^${}()|[\]\\]/g, '');
  let trafo = 7.6705;
  let lectura = 26.613;
  /////***** FECHA
  if (typeof fecha === 'string') {
    tiempo = fecha.replace(/[-now.mdyh*+?^${}()|[\]\\]/g, '');
    unidades = fecha.replace(/[-now.1234567890*+?^${}()|[\]\\]/g, '');
  }
  if (typeof fecha === 'object') {
    let fecha: any = data.timeRange.raw;
    let fecha_in = fecha.from._d;
    let fecha_out = fecha.to._d;
    var difference = Math.abs(fecha_in - fecha_out);
    console.log('UNO', fecha_in, fecha_out, difference / (1000 * 3600 * 24));
    tiempo = '' + difference / (1000 * 3600 * 24);
    unidades = 'd';
  }
  let tiempo2 = parseInt(tiempo);

  if (unidades === 'm') {
    h = 1;
    d = 1;
  } else if (unidades === 'h') {
    h = tiempo2;
    d = 1;
  } else if (unidades === 'd') {
    h = tiempo2;
    d = 24;
  }
  trafo = trafo * h * d;
  lectura = lectura * h * d;
  console.log(trafo);
  let valor1 = 0.0;
  let valor2 = 0.0;
  let valor3 = 0.0;
  let valor4 = 0.0;
  let valor = 0.0;
  let valor5 = 0.0;
  let data_repla3: any = data.series.find(({ refId }) => refId === 'PRECIO')?.name;
  switch (opcion) {
    case '1': //diferencia entre el ultimo valor y el primero "consumo"
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = valor2 - valor1;
      break;

    case '2': // suma de consumos + trafo + lectura
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.firstNotNull;
      valor4 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.lastNotNull;

      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }

      valor = (valor2 - valor1) + (valor4 - valor3) + trafo + lectura;
      console.log(valor2 - valor1, valor4 - valor3);

      break;

    case '3': //promedio de 3 valores
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = (valor1 + valor2 + valor3) / 3;

      break;

    case '4': // precio de consumo 
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;

      let precio: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = (valor2 - valor1) * precio;
      break;

    case '5': // precio de la suma de consumos
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.firstNotNull;
      valor4 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.lastNotNull;


      let data_repla4: any = data.series.find(({ refId }) => refId === 'PRECIO')?.name;
      let precio2: any = data_repla4.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      if (valor4 === null) {
        valor4 = 0;
      }
      valor = ((valor2 - valor1) + trafo + lectura) * 0.0842 + (valor4 - valor3) * 0.0779;

      console.log((valor2 - valor1) * 0.0842, (valor4 - valor3) * 0.0779);
      break;
    case '6': //operacion cualquiera
      let data_repla5: any = data.series.find(({ refId }) => refId === 'OPERACION')?.fields[1].state?.calcs
        ?.lastNotNull;
      let dato = parseFloat(data_repla5);

      if (dato === null) {
        dato = 0;
      }

      valor = dato;
      break;

    case '7': // valor multiplicado por @timestamp
      valor5 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.mean;
      valor = valor5 * h * d;
      valor = parseFloat(valor.toFixed(1));

      break;

    case '8': // precio multiplicado por @timestamp
      valor5 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.mean;
      let precio1: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      valor = valor5 * h * d * precio1;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '9': //SUMA DE minimo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.min;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.min;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.min;
      valor = valor1 + valor2 + valor3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '10': //SUMA DE promedio
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.mean;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.mean;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.mean;
      valor = valor1 + valor2 + valor3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '11': //SUMA DE maximo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.max;
      valor = valor1 + valor2 + valor3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '12': //PROMEDIO DE minimo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.min;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.min;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.min;
      valor = (valor1 + valor2 + valor3) / 3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '13': //PROMEDIO DE promedio
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.mean;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.mean;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.mean;
      valor = (valor1 + valor2 + valor3) / 3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '14': //PROMEDIO DE maximo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.max;
      valor = (valor1 + valor2 + valor3) / 3;
      valor = parseFloat(valor.toFixed(1));

      break;
    case '15': // minimo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.min;
      valor = parseFloat(valor1.toFixed(1));

      break;
    case '16': // promedio
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.mean;
      valor = parseFloat(valor1.toFixed(1));

      break;
    case '17': // maximo
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      valor = parseFloat(valor1.toFixed(1));

      break;
    case '18': // minimo 2 DECIMALES
      valor = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.min;

      break;
    case '19': // promedio 2 DECIMALES
      valor = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.mean;

      break;
    case '20': // maximo 2 DECIMALES
      valor = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      break;
    case '21': // consumo + trafo
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;

      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = valor2 - valor1 + trafo;
      break;
    case '22': // precion del consumo + trafo
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;

      let precio: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      console.log('valores kw', valor2, valor1, valor2 - valor1, trafo, h, d, precio);
      valor = (valor2 - valor1 + trafo) * precio;
      console.log(valor);
      break;
    case '23': // precio del promedio de maximos
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.max;
      let precio: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = (valor1 + valor2 + valor3) * precio;
      valor = parseFloat(valor.toFixed(2));
      break;
    case '24':
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = valor2 - valor1 + trafo + potMax
      break;
    case '25':
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      let precio: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = (valor2 - valor1) * precio;
      break;
    case '26': // transporte de energia
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = (valor2 - valor1) * 0.0007
      break;
    case '27': // transporte de potencia
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.max;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.max;
      data_repla3 = data.series.find(({ refId }) => refId === 'PRECIO3')?.name;
      let precio3: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = (valor1 + valor2 + valor3) * precio3;
      valor = parseFloat(valor.toFixed(1))
      break;
    case '28': //alumbrado publico
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      data_repla3 = data.series.find(({ refId }) => refId === 'PRECIO3')?.name;
      let precio3: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = (((valor2 - valor1)) * 0.0007 + (valor3 * precio3) + 0.79) * 0.2
      break;
    case '29': //TASA DE BASURA
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      data_repla3 = data.series.find(({ refId }) => refId === 'PRECIO3')?.name;
      let precio3: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = (((valor2 - valor1)) * 0.0007 + (valor3 * precio3) + 0.79) * 0.213
      break;
    case '30': //TOTAL CONSUMO BP
      valor1 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR1')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.max;
      let precio: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      data_repla3 = data.series.find(({ refId }) => refId === 'PRECIO2')?.name;
      let precio2: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');
      data_repla3 = data.series.find(({ refId }) => refId === 'PRECIO3')?.name;
      let precio3: any = data_repla3.replace(/[*+?^${}()|[\]\\]/g, '');

      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      let consumo = ((valor2 - valor1)) * precio
      let peajeTrans = valor3 * precio2
      let peajeEqq = ((valor2 - valor1)) * 0.0007
      let transEqq = valor3 * precio3
      let comercial = 0.79
      let alumbrado = (((valor2 - valor1)) * 0.0007 + (valor3 * precio3) + 0.79) * 0.2
      let basura = (((valor2 - valor1)) * 0.0007 + (valor3 * precio3) + 0.79) * 0.213
      let impuestos = 14.94
      valor = consumo + peajeTrans + peajeEqq + transEqq + comercial + alumbrado + basura + impuestos
      console.log("datos total ->", consumo, peajeTrans, peajeEqq, transEqq, comercial, alumbrado, basura, impuestos)
      break;
    case '31':
      let tamanio = data.series.find(({ name }) => name === 'CANTIDAD')?.fields[1].state?.calcs?.lastNotNull;
      let suma = 0;
      for (let i = 0; i < tamanio; i++) {
        let dato = data.series.find(({ name }) => name === 'VALOR' + (i + 1))?.fields[1].state?.calcs?.lastNotNull;
        suma += dato
      }
      valor = suma / tamanio
      break;
    case '32': //potencia kva biess
      let consumo = localStorage.getItem("biessConsumo");

      if (consumo === null || consumo === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      break;
    case '33': //potencia kva bpc
      let consumo = localStorage.getItem("bpcConsumo");

      if (consumo === null || consumo === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      break;
    case '34': //potencia kwbiess
      let consumo = localStorage.getItem("biessConsumo");

      if (valor === null || valor === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      valor = valor * 0.9
      break;
    case '35': //potencia kw bpc
      let consumo = localStorage.getItem("bpcConsumo");

      if (consumo === null || consumo === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      valor = valor * 0.9
      break;
    case '36': //energia biess
      let consumo = localStorage.getItem("biessConsumo");

      if (valor === null || valor === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      valor = valor * h * d
      break;
    case '37': //energia bpc
      let consumo = localStorage.getItem("bpcConsumo");

      if (consumo === null || consumo === undefined) {
        valor = 0
      } else {
        valor = parseFloat(consumo)
      }
      valor = valor * h * d
      break;
    case '100': //ultimo dato
      valor = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor === null||valor === undefined) {
        valor = 0;
      }else{
        valor=parseFloat(valor.toFixed(3))
      }
      break;
      case '101': //ultimo dato
      valor = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor === null||valor === undefined) {
        valor = 0;
      }else{
        valor=parseFloat(valor.toFixed(0))
      }
      break;
    default:
      valor = 0;
      break;
  }

  let ajuste = data.series.find(({ name }) => name === 'AJUSTE')?.fields[1].state?.calcs?.lastNotNull;
  if (ajuste === 1) {
    if (valor === null || valor === undefined || isNaN(valor)) {
      valor = 0;
    } else {
      valor = parseFloat(valor.toFixed(1));
    }
    valor = parseFloat((valor * 1.0225).toFixed(1));
  }

  if (opcion === '18' || opcion === '19' || opcion === '20') {
    if (valor === null || valor === undefined || isNaN(valor)) {
      valor = 0;
    } else {
      valor = parseFloat(valor.toFixed(2));
    }
  } else {
    if (valor === null || valor === undefined || isNaN(valor)) {
      valor = 0;
    } else {
      valor = parseFloat(valor.toFixed(1));
    }
  }
  if (opcion === '101') {
    if (valor === null || valor === undefined || isNaN(valor)) {
      valor = 0;
    } else {
      valor = parseFloat(valor.toFixed(0));
    }
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        id="svg8"
        viewBox="0 0 40.999999 25"
        height={'100%'}
        width={'100%'}
      //{...props}
      >
        <defs id="defs2">
          <linearGradient id="linearGradient877">
            <stop id="stop873" offset={0} stopColor="#9a7c00" stopOpacity={1} />
            <stop id="stop875" offset={1} stopColor="#000" stopOpacity={0} />
          </linearGradient>
          <linearGradient id="linearGradient949">
            <stop id="stop945" offset={0} stopColor="#00465f" stopOpacity={1} />
            <stop id="stop947" offset={1} stopColor="#00758d" stopOpacity={1} />
          </linearGradient>
          <linearGradient
            gradientTransform="matrix(1.24042 0 0 1.24042 .078 -.633)"
            gradientUnits="userSpaceOnUse"
            y2={10.616492}
            x2={32.234886}
            y1={10.616492}
            x1={10.256792}
            id="linearGradient935"
            xlinkHref="#linearGradient949"
          />
          <linearGradient
            gradientTransform="matrix(1.24042 0 0 1.24042 .078 -.633)"
            gradientUnits="userSpaceOnUse"
            y2={16.727802}
            x2={16.142069}
            y1={1.9272544}
            x1={16.494652}
            id="linearGradient951"
            xlinkHref="#linearGradient949"
          />
          <linearGradient
            gradientUnits="userSpaceOnUse"
            y2={21.899397}
            x2={39.088169}
            y1={21.899397}
            x1={26.843884}
            id="linearGradient879"
            xlinkHref="#linearGradient877"
          />
        </defs>
        <g id="layer1" stroke="none">
          <path
            d="M2.009 24.441l-1.07-1.004V9.107l.908-1.053V6.466l-.739.421V.657h5.756v.775l1.92.156.96-.96H39.11l1.035 1.034V7.25L37.603 9.79v5.093l2.63 2.63v5.827l-1.048 1.047H24.131l-1.211-1.21H10.234l-.745.745h-3.61l-.674.389z"
            id="path849"
            fill="url(#linearGradient935)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
            opacity={1}
            fillOpacity={1}
          />
          <path
            d="M2.844 7.388V5.424l2.92-2.92H7.65z"
            id="path851"
            fill="#002e4c"
            fillOpacity={1}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
            opacity={1}
          />
          <path
            d="M3.182 20.72v-6.139l-.909-.909V9.644l7.922-7.922h26.032v12.94l2.766 3.26v1.14H23.396l-1.634 1.634z"
            id="path853"
            fill="url(#linearGradient951)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M6.56 23.21h2.925l.81-.707H21.93v-.47H8.063z"
            id="path855"
            fill="#002e4c"
            fillOpacity={1}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g871"
            fillOpacity={1}
            fill="url(#linearGradient879)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path id="path857" d="M23.301 20.426h1.22l2.707 3.069h-1.342z" />
            <path id="path857-3" d="M25.628 20.426h1.22l2.707 3.069h-1.342z" />
            <path id="path857-3-6" d="M27.955 20.426h1.22l2.706 3.069H30.54z" />
            <path id="path857-3-7" d="M30.282 20.426h1.22l2.706 3.069h-1.342z" />
            <path id="path857-3-5" d="M32.608 20.426h1.22l2.707 3.069h-1.342z" />
          </g>
          <path
            d="M37.73 14.871l.385-.45V9.984l-.335-.193.764-.765v6.767z"
            id="path917"
            fill="#00738b"
            fillOpacity={0.95294118}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M39.026 16.285l.443-.444V8.73l-.542-.184 1.239-1.251v10.009z"
            id="path919"
            fill="#00728b"
            fillOpacity={0.95294118}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            id="valor"
            y={11.675325}
            x={20.287453}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="7.0015px"
            fontFamily="sans-serif"
            fill="#5df"
            fillOpacity={1}
            strokeWidth={0.328195}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={11.675325}
              x={20.287455}
              id="tspan1006"
              fontSize="7.0015px"
              textAnchor="middle"
              fill="#5df"
              strokeWidth={0.328195}
            >
              {valor}
            </tspan>
          </text>
          <text
            id="unidades"
            y={17.356113}
            x={20.884413}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="5.25112px"
            fontFamily="sans-serif"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.328195}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={17.356113}
              x={20.884413}
              id="tspan1006-5"
              fontSize="5.25112px"
              textAnchor="middle"
              fill="#fff"
              strokeWidth={0.328195}
            >
              {unidad}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
