import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  //nombre
  //let data_repla: any = data.series.find(({ refId }) => refId === 'E')?.name;
  //let nom_on: any = data_repla.replace(/[.*+?^${}|[\]\\]/g, '');
  //console.log(data_repla, nom_on);
  //let data_repla: any = data.series.find(({ refId }) => refId === 'UNIDAD')?.name;
  //let unidad: any = data_repla.replace(/[.*+?^{}()|[\]\\]/g, '');
  let data_repla2: any = data.series.find(({ refId }) => refId === 'OPCION')?.name;
  let opcion: any = data_repla2.replace(/[.*+?^${}()|[\]\\]/g, '');

  let fun_79 = [1, 2, 4, 8, 16, 32, 64, 128, 256];
  let st_evento = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  let st_st = [
    'Reposo',
    'T. Reenganche',
    'Abierto',
    'T. Espera',
    'T. Cierre',
    'T. Reposición',
    'Lockout',
    'T. Seguridad',
    'T. Apertura Final',
  ];

  let fun_input = [1, 2, 4, 8, 16, 32];
  let st_in = [17, 18, 19, 20, 21, 22];
  let in_st = ['Entrada 1', 'Entrada 2', 'Entrada 3', 'Entrada 4', 'Entrada 5', 'Entrada 6'];

  let fun_output = [0, 256, 512, 1024, 2048];
  let st_out = [0, 1, 2, 3, 4];
  let out_st = ['---', 'Salida 1', 'Salida 2', 'Salida 3', 'Salida 4'];

  let fun_46BC = [0, 16, 4096];
  let st_dt = [0, 1, 2];
  let dt_st = ['---', 'Pickup', 'Trip'];

  let fun_52 = [
    1,
    2,
    4,
    8,
    16,
    32,
    64,
    128,
    256,
    512,
    1024,
    2048,
    4096,
    2050,
    2052,
    2056,
    2064,
    2080,
    4098,
    4100,
    4104,
    4112,
    4128,
  ];
  let st_int = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, '--', '--', '--', '--', '--', '--', '--', '--', '--', '--'];
  let int_st = [
    'Inicio',
    'Error',
    'Abierto',
    'T. Apertura',
    'Fallo de apertura',
    'Cerrado',
    'T. Cierre',
    'Fallo de cierre',
    'Exceso de aperturas',
    'Exc. amp acum',
    'Exc. aperturas',
    '52a',
    '52b',
    '52a Error',
    '52a Abierto',
    '52a T. Apertura',
    '52a Fallo de Apertura',
    '52a Cerrado',
    '52b Error',
    '52b Abierto',
    '52b T. Apertura',
    '52b Fallo de Apertura',
    '52b Cerrado',
  ];

  let num_st = '';
  let estado_st = '';

  let evento = '';
  let num_even = '';
  let evento1 = 0;
  let evento2 = 0;
  let evento3 = 0;
  let evento4 = 0;
  let evento5 = 0;

  switch (opcion) {
    case '1':
      evento1 = data.series.find(({ name }) => name === 'EVENTO')?.fields[1].state?.calcs?.firstNotNull;
      for (let i = 0; i < fun_79.length; i++) {
        if (evento1 === fun_79[i]) {
          num_st = '' + st_evento[i];
          estado_st = st_st[i];
        }
      }
      evento = estado_st;
      num_even = num_st;
      break;
    case '2':
      evento2 = data.series.find(({ name }) => name === 'EVENTO')?.fields[1].state?.calcs?.firstNotNull;
      for (let i = 0; i < fun_input.length; i++) {
        if (evento2 === fun_input[i]) {
          num_st = '' + st_in[i];
          estado_st = in_st[i];
        }
      }
      evento = estado_st;
      num_even = num_st;

      break;
    case '3':
      evento3 = data.series.find(({ name }) => name === 'EVENTO')?.fields[1].state?.calcs?.firstNotNull;
      for (let i = 0; i < fun_output.length; i++) {
        if (evento3 === fun_output[i]) {
          num_st = '' + st_out[i];
          estado_st = out_st[i];
        }
      }
      evento = estado_st;
      num_even = num_st;

      break;

    case '4':
      evento4 = data.series.find(({ name }) => name === 'EVENTO')?.fields[1].state?.calcs?.firstNotNull;
      for (let i = 0; i < fun_46BC.length; i++) {
        if (evento4 === fun_46BC[i]) {
          num_st = '' + st_dt[i];
          estado_st = dt_st[i];
        }
      }
      evento = estado_st;
      num_even = num_st;

      break;

    case '5':
      evento5 = data.series.find(({ name }) => name === 'EVENTO')?.fields[1].state?.calcs?.firstNotNull;
      for (let i = 0; i < fun_52.length; i++) {
        if (evento5 === fun_52[i]) {
          num_st = '' + st_int[i];
          estado_st = int_st[i];
        }
      }
      evento = estado_st;
      num_even = num_st;

      break;
    default:
      evento = '---';
      break;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        id="svg8"
        viewBox="0 0 40.999999 25"
        height={'100%'}
        width={'100%'}
        //{...props}
      >
        <defs id="defs2">
          <linearGradient id="linearGradient877">
            <stop id="stop873" offset={0} stopColor="#9a7c00" stopOpacity={1} />
            <stop id="stop875" offset={1} stopColor="#000" stopOpacity={0} />
          </linearGradient>
          <linearGradient id="linearGradient949">
            <stop id="stop945" offset={0} stopColor="#00465f" stopOpacity={1} />
            <stop id="stop947" offset={1} stopColor="#00758d" stopOpacity={1} />
          </linearGradient>
          <linearGradient
            gradientTransform="matrix(1.24042 0 0 1.24042 .078 -.633)"
            gradientUnits="userSpaceOnUse"
            y2={10.616492}
            x2={32.234886}
            y1={10.616492}
            x1={10.256792}
            id="linearGradient935"
            xlinkHref="#linearGradient949"
          />
          <linearGradient
            gradientTransform="matrix(1.24042 0 0 1.24042 .078 -.633)"
            gradientUnits="userSpaceOnUse"
            y2={16.727802}
            x2={16.142069}
            y1={1.9272544}
            x1={16.494652}
            id="linearGradient951"
            xlinkHref="#linearGradient949"
          />
          <linearGradient
            gradientUnits="userSpaceOnUse"
            y2={21.899397}
            x2={39.088169}
            y1={21.899397}
            x1={26.843884}
            id="linearGradient879"
            xlinkHref="#linearGradient877"
          />
        </defs>
        <g id="layer1" stroke="none">
          <path
            d="M2.009 24.441l-1.07-1.004V9.107l.908-1.053V6.466l-.739.421V.657h5.756v.775l1.92.156.96-.96H39.11l1.035 1.034V7.25L37.603 9.79v5.093l2.63 2.63v5.827l-1.048 1.047H24.131l-1.211-1.21H10.234l-.745.745h-3.61l-.674.389z"
            id="path849"
            fill="url(#linearGradient935)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
            opacity={1}
            fillOpacity={1}
          />
          <path
            d="M2.844 7.388V5.424l2.92-2.92H7.65z"
            id="path851"
            fill="#002e4c"
            fillOpacity={1}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
            opacity={1}
          />
          <path
            d="M3.182 20.72v-6.139l-.909-.909V9.644l7.922-7.922h26.032v12.94l2.766 3.26v1.14H23.396l-1.634 1.634z"
            id="path853"
            fill="url(#linearGradient951)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M6.56 23.21h2.925l.81-.707H21.93v-.47H8.063z"
            id="path855"
            fill="#002e4c"
            fillOpacity={1}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g871"
            fillOpacity={1}
            fill="url(#linearGradient879)"
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path id="path857" d="M23.301 20.426h1.22l2.707 3.069h-1.342z" />
            <path id="path857-3" d="M25.628 20.426h1.22l2.707 3.069h-1.342z" />
            <path id="path857-3-6" d="M27.955 20.426h1.22l2.706 3.069H30.54z" />
            <path id="path857-3-7" d="M30.282 20.426h1.22l2.706 3.069h-1.342z" />
            <path id="path857-3-5" d="M32.608 20.426h1.22l2.707 3.069h-1.342z" />
          </g>
          <path
            d="M37.73 14.871l.385-.45V9.984l-.335-.193.764-.765v6.767z"
            id="path917"
            fill="#00738b"
            fillOpacity={0.95294118}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M39.026 16.285l.443-.444V8.73l-.542-.184 1.239-1.251v10.009z"
            id="path919"
            fill="#00728b"
            fillOpacity={0.95294118}
            strokeWidth=".328195px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            id="valor"
            y={11.675325}
            x={20.287453}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="7.0015px"
            fontFamily="sans-serif"
            fill="#5df"
            fillOpacity={1}
            strokeWidth={0.328195}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={11.675325}
              x={20.287455}
              id="tspan1006"
              fontSize="5.0015px"
              textAnchor="middle"
              fill="#5df"
              strokeWidth={0.328195}
            >
              {evento}
            </tspan>
          </text>
          <text
            id="unidades"
            y={17.356113}
            x={20.884413}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="5.25112px"
            fontFamily="sans-serif"
            fill="#fff"
            fillOpacity={1}
            strokeWidth={0.328195}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={17.356113}
              x={20.884413}
              id="tspan1006-5"
              fontSize="3.25112px"
              textAnchor="middle"
              fill="#fff"
              strokeWidth={0.328195}
            >
              Evento:{num_even}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
