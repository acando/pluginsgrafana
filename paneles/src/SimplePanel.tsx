import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
//import styleEstado from 'elementos/Estado';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();
  console.log(data);
  //nombre
  let data_repla: any = data.series.find(({ refId }) => refId === 'G')?.name;
  let nom_on: any = data_repla.replace(/[.*+?^${}()|[\]\\]/g, '');
  console.log(data_repla, nom_on);

  ////LINKS
  /*let url = '';
  switch (nom_on) {
    case 'PDU-01B-F5':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-01B-F6':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-02B-F5':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    case 'PDU-02B-F6':
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
      break;
    default:
      url = 'http://bmscloud.i.telconet.net:32308/d/QoYPPPz7z/rack-banco-pacifico?orgId=1&refresh=5s';
  }*/

  let cur = [];
  for (let i = 1; i <= 42; i++) {
    cur[i] = data.series.find(
      ({ name }) => name === 'Average DATA.CURR_1.NOM.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (cur[i] === null || cur[i] === 0) {
      cur[i] = 0;
    } else {
      cur[i] = cur[i].toFixed(1);
    }
  }

  let kvat = [];
  for (let i = 1; i <= 42; i++) {
    kvat[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KVA_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kvat[i] === null || kvat[i] === 0) {
      kvat[i] = 0;
    } else {
      kvat[i] = (kvat[i] * 100).toFixed(1);
    }
  }

  let kw = [];
  for (let i = 1; i <= 42; i++) {
    kw[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KW_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kw[i] === null || kw[i] === 0) {
      kw[i] = 0;
    } else {
      kw[i] = (kw[i] / 10).toFixed(1);
    }
  }

  /*let kwh = [];
  for (let i = 1; i <= 42; i++) {
    kwh[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KWH_1.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kwh[i] === null || kwh[i] === 0) {
      kwh[i] = 0;
    } else {
      kwh[i] = (kwh[i] ).toFixed(1);
    }
  }*/
  let kwh = [];
  for (let i = 1; i <= 42; i++) {
    kwh[i] = kw[i] * 60;
    if (kwh[i] === null || kwh[i] === 0) {
      kwh[i] = 0;
    } else {
      kwh[i] = kwh[i].toFixed(1);
    }
  }

  let vt = [];
  let kva = [];
  let v = [];
  for (let i = 1; i <= 42; i++) {
    if (cur[i] === 0) {
      vt[i] = 0;
    } else {
      vt[i] = (kvat[i] / cur[i]).toFixed(1);
    }
    kva[i] = (kvat[i] / 1000).toFixed(1);
    v[i] = vt[i];
    if (v[i] > 127) {
      v[i] = (126).toFixed(1);
    }
  }

  console.log(vt[13]);

  /*let cur2 = [];
  for (let i = 1; i <= 42; i++) {
    cur2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.CURR_2.NOM.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (cur2[i] === null || cur2[i] === 0) {
      cur2[i] = 0;
    } else {
      cur2[i] = cur2[i].toFixed(1);
    }
  }

  let kva2 = [];
  for (let i = 1; i <= 42; i++) {
    kva2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KVA_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kva2[i] === null || kva2[i] === 0) {
      kva2[i] = 0;
    } else {
      kva2[i] = kva2[i].toFixed(1);
    }
  }

  let kw2 = [];
  for (let i = 1; i <= 42; i++) {
    kw2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KWH_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kw2[i] === null || kw2[i] === 0) {
      kw2[i] = 0;
    } else {
      kw2[i] = (kw2[i] / 3600).toFixed(1);
    }
  }

  let kwh2 = [];
  for (let i = 1; i <= 42; i++) {
    kwh2[i] = data.series.find(
      ({ name }) => name === 'Average DATA.KWH_2.CH' + i + '.VALUE'
    )?.fields[1].state?.calcs?.lastNotNull;
    if (kwh2[i] === null || kwh2[i] === 0) {
      kwh2[i] = 0;
    } else {
      kwh2[i] = (kwh2[i] / 10).toFixed(1);
    }
  }
  let v2 = [];
  for (let i = 1; i <= 42; i++){
    if(cur2[i]===0){
      v2[i]=0;
    }else{
    v2 [i] = (kva2 [i]/cur2[i]).toFixed(2);
    }
  }*/

  ////// ETIQUETAS
  /*let imagen = [];
  let imagen1 = 0;
  let equipo = [
    'PDU-01B-F2',
    'PDU-01B-F3',
    'PDU-01B-F4',
    'PDU-01B-F5',
    'PDU-01B-F6',
    'PDU-01B-F7',
    'PDU-01B-F8',
    'PDU-01B-F9',
    'PDU-02B-F2',
    'PDU-02B-F3',
    'PDU-02B-F4',
    'PDU-02B-F5',
    'PDU-02B-F6',
    'PDU-02B-F7',
    'PDU-02B-F8',
    'PDU-02B-F9',
  ];

  for (let i = 0; i <= equipo.length; i++) {
    console.log(nom_on, equipo[i]);
    if (nom_on === equipo[i]) {
      imagen[i] = 1;
      console.log('entro');
    } else {
      imagen[i] = 0;
    }
    if (nom_on === 'PDU-01B-F7' || nom_on === 'PDU-01B-F8' || nom_on === 'PDU-02B-F7' || nom_on === 'PDU-02B-F8') {
      imagen1 = 1;
    }
  }*/

  let etq1 = 'RESERVA';
  let etq2 = 'RESERVA';
  let etq3 = 'RESERVA';
  let etq4 = 'RESERVA';
  let etq5 = 'RESERVA';
  let etq6 = 'RESERVA';
  let etq7 = 'RESERVA';
  let etq8 = 'RESERVA';
  let etq9 = 'RESERVA';
  let etq10 = 'RESERVA';
  let etq11 = 'RESERVA';
  let etq12 = 'RESERVA';
  let etq13 = 'RESERVA';
  let etq14 = 'RESERVA';

  if (nom_on === 'PDU-01B-F3') {
    etq1 = 'F2-20-A1';
    etq2 = 'F2-19-A1';
    etq3 = 'F3-05-A1';
    etq4 = 'F3-07-A1';
    etq5 = 'F3-09-A1';
    etq8 = 'F2-21-A1';
    etq9 = 'F3-04-A1';
    etq10 = 'F3-06-A1';
    etq11 = 'F3-08-A1';
    etq12 = 'F3-10-A1';
  }

  if (nom_on === 'PDU-01B-F4') {
    etq1 = 'F4-21-A1';
    etq2 = 'F4-19-A1';
    etq3 = 'F4-17-A1';
    etq4 = 'F4-15-A1';
    etq5 = 'F4-13-A1';
    etq6 = 'F4-11-A1';
    etq7 = 'F4-09-A1';
    etq8 = 'F4-20-A1';
    etq9 = 'F4-18-A1';
    etq10 = 'F4-16-A1';
    etq11 = 'F4-14-A1';
    etq12 = 'F4-12-A1';
    etq13 = 'F4-10-A1';
    etq14 = 'F4-08-A1';
  }

  if (nom_on === 'PDU-01B-F5') {
    etq1 = 'F5-01-A1';
    etq2 = 'F5-03-A1';
    etq3 = 'F5-08-A1';
    etq4 = 'F5-05-A1';
    etq7 = 'F4-09-A1';
    etq8 = 'F5-02-A1';
    etq10 = 'F5-04-A1';
    etq11 = 'F5-15-A1';
  }

  if (nom_on === 'PDU-01B-F6') {
    etq1 = 'F6-01-A1';
    etq2 = 'F6-03-A3';
    etq3 = 'F6-05-A1';
    etq4 = 'F6-03-A2';
    etq5 = 'F6-14-A1';
    etq8 = 'F6-02-A1';
    etq9 = 'F6-03-A1';
    etq11 = 'F6-04-A1';
  }

  if (nom_on === 'PDU-01B-F9') {
    etq1 = 'F9-01-A1';
    etq2 = 'F9-03-A3';
    etq3 = 'F9-05-A1';
    etq4 = 'F9-07-A1';
    etq5 = 'F9-09-A1';
    etq6 = 'F9-14-A1';
    etq7 = 'F9-15-A1';
    etq8 = 'F9-02-A1';
    etq9 = 'F9-04-A1';
    etq10 = 'F9-06-A1';
    etq11 = 'F9-08-A1';
    etq12 = 'F9-10-A1';
    etq13 = 'F9-11-A1';
    etq14 = 'F9-19-A1';
  }

  ///// SISTEMA 2 //////
  if (nom_on === 'PDU-02B-F3') {
    etq1 = 'F2-21-A1';
    etq2 = 'F2-19-A1';
    etq3 = 'F3-05-A1';
    etq4 = 'F3-07-A1';
    etq5 = 'F3-11-A1';
    etq6 = 'F3-10-A1';
    etq7 = 'RESERVA';
    etq8 = 'F3-17-A1';
    etq9 = 'F3-04-A1';
    etq10 = 'F3-06-A1';
    etq11 = 'F3-15-A1';
    etq12 = 'F3-12-A1';
    etq13 = 'F3-10-A2';
    etq14 = 'F2-20-A2';
  }

  if (nom_on === 'PDU-02B-F4') {
    etq1 = 'F4-02-A1';
    etq2 = 'F4-20-A1';
    etq3 = 'F4-18-A1';
    etq4 = 'F4-15-A1';
    etq5 = 'F4-16-A3';
    etq6 = 'F4-12-A1';
    etq7 = 'F4-10-A1';
    etq8 = 'F4-21-A1';
    etq9 = 'F4-11-A2';
    etq10 = 'F4-18-A2';
    etq11 = 'F4-15-A1';
    etq12 = 'F4-13-A1';
    etq13 = 'F4-11-A1';
    etq14 = 'F4-09-A1';
  }

  if (nom_on === 'PDU-02B-F5') {
    etq3 = 'F5-08-A1';
    etq10 = 'F5-04-A1';
    etq11 = 'F5-05-A1';
    etq12 = '';
    etq13 = 'F5-14-A1';
    etq14 = 'F5-15-A1';
  }

  if (nom_on === 'PDU-02B-F6') {
    etq4 = 'F6-05-A1';
    etq5 = 'F6-14-A1';
    etq10 = 'F6-04-A1';
  }

  if (nom_on === 'PDU-02B-F9') {
    etq1 = 'F9-01-A2';
    etq2 = 'F9-02-A2';
    etq3 = 'F9-05-A1';
    etq4 = 'F9-07-A1';
    etq5 = 'F9-09-A1';
    etq6 = 'F9-11-A1';
    etq7 = 'F9-18-A1';
    etq8 = 'F9-02-A1';
    etq9 = 'F9-04-A1';
    etq10 = 'F9-06-A1';
    etq11 = 'F9-08-A1';
    etq12 = 'F9-10-A1';
    etq13 = 'F9-15-A1';
    etq14 = 'F9-20-A1';
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        width={'100%'}
        height={'100%'}
        viewBox="0 0 436.56249 211.66667"
        id="svg67790"
        //{...props}
      >
        <defs id="defs67784">
          <clipPath id="clipPath3307-2-7-3-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-20" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-25"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-71" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-33"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68418" clipPathUnits="userSpaceOnUse">
            <path
              id="path68416"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68422" clipPathUnits="userSpaceOnUse">
            <path
              id="path68420"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68426" clipPathUnits="userSpaceOnUse">
            <path
              id="path68424"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68430" clipPathUnits="userSpaceOnUse">
            <path
              id="path68428"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68434" clipPathUnits="userSpaceOnUse">
            <path
              id="path68432"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68438" clipPathUnits="userSpaceOnUse">
            <path
              id="path68436"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-45" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-31" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-04"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-27"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-10" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-54" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-52"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68514" clipPathUnits="userSpaceOnUse">
            <path
              id="path68512"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68518" clipPathUnits="userSpaceOnUse">
            <path
              id="path68516"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68522" clipPathUnits="userSpaceOnUse">
            <path
              id="path68520"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68526" clipPathUnits="userSpaceOnUse">
            <path
              id="path68524"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68530" clipPathUnits="userSpaceOnUse">
            <path
              id="path68528"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68534" clipPathUnits="userSpaceOnUse">
            <path
              id="path68532"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-20-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-25-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-5-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-71-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-4-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68578">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68576"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68582">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68580"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68586">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68584"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68590">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68588"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-2-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-04-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-27-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-10-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-7-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-54-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-52-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68652">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68650"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68656">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68654"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-68" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-80"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-65" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68700" clipPathUnits="userSpaceOnUse">
            <path
              id="path68698"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68704" clipPathUnits="userSpaceOnUse">
            <path
              id="path68702"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68708" clipPathUnits="userSpaceOnUse">
            <path
              id="path68706"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68712" clipPathUnits="userSpaceOnUse">
            <path
              id="path68710"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-49" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-69"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-88" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-13"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68756" clipPathUnits="userSpaceOnUse">
            <path
              id="path68754"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68760" clipPathUnits="userSpaceOnUse">
            <path
              id="path68758"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-80-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-4-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-9-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1-7-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0-7-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68804">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68802"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68808">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68806"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68812">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68810"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68816">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68814"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-69-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-13-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68860">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68858"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath68864">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path68862"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5-1-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04-8-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24-5-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71-6-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3-9-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9-9-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1-4-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7-0-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-48"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-42" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-90"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68912" clipPathUnits="userSpaceOnUse">
            <path
              id="path68910"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68916" clipPathUnits="userSpaceOnUse">
            <path
              id="path68914"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68934" clipPathUnits="userSpaceOnUse">
            <path
              id="path68932"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68938" clipPathUnits="userSpaceOnUse">
            <path
              id="path68936"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-47"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-32" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-18" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-07"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68982" clipPathUnits="userSpaceOnUse">
            <path
              id="path68980"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath68986" clipPathUnits="userSpaceOnUse">
            <path
              id="path68984"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-48-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-42-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-90-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69012">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69010"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69016">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69014"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69034">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69032"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath69038">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path69036"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1-6-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69060" clipPathUnits="userSpaceOnUse">
            <path
              id="path69058"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69064" clipPathUnits="userSpaceOnUse">
            <path
              id="path69062"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69082" clipPathUnits="userSpaceOnUse">
            <path
              id="path69080"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69086" clipPathUnits="userSpaceOnUse">
            <path
              id="path69084"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69090" clipPathUnits="userSpaceOnUse">
            <path
              id="path69088"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69094" clipPathUnits="userSpaceOnUse">
            <path
              id="path69092"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69098" clipPathUnits="userSpaceOnUse">
            <path
              id="path69096"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69102" clipPathUnits="userSpaceOnUse">
            <path
              id="path69100"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-15" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-33"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-93" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-22" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-87"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-83"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-51" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69164" clipPathUnits="userSpaceOnUse">
            <path
              id="path69162"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69168" clipPathUnits="userSpaceOnUse">
            <path
              id="path69166"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69172" clipPathUnits="userSpaceOnUse">
            <path
              id="path69170"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69176" clipPathUnits="userSpaceOnUse">
            <path
              id="path69174"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69180" clipPathUnits="userSpaceOnUse">
            <path
              id="path69178"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69184" clipPathUnits="userSpaceOnUse">
            <path
              id="path69182"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-65" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-06"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-13" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-71"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-26"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69264" clipPathUnits="userSpaceOnUse">
            <path
              id="path69262"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69268" clipPathUnits="userSpaceOnUse">
            <path
              id="path69266"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69272" clipPathUnits="userSpaceOnUse">
            <path
              id="path69270"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69276" clipPathUnits="userSpaceOnUse">
            <path
              id="path69274"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69294" clipPathUnits="userSpaceOnUse">
            <path
              id="path69292"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69298" clipPathUnits="userSpaceOnUse">
            <path
              id="path69296"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69316" clipPathUnits="userSpaceOnUse">
            <path
              id="path69314"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69320" clipPathUnits="userSpaceOnUse">
            <path
              id="path69318"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69338" clipPathUnits="userSpaceOnUse">
            <path
              id="path69336"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69342" clipPathUnits="userSpaceOnUse">
            <path
              id="path69340"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69346" clipPathUnits="userSpaceOnUse">
            <path
              id="path69344"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69350" clipPathUnits="userSpaceOnUse">
            <path
              id="path69348"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69354" clipPathUnits="userSpaceOnUse">
            <path
              id="path69352"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69358" clipPathUnits="userSpaceOnUse">
            <path
              id="path69356"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69362" clipPathUnits="userSpaceOnUse">
            <path
              id="path69360"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69366" clipPathUnits="userSpaceOnUse">
            <path
              id="path69364"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69384" clipPathUnits="userSpaceOnUse">
            <path
              id="path69382"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69388" clipPathUnits="userSpaceOnUse">
            <path
              id="path69386"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69406" clipPathUnits="userSpaceOnUse">
            <path
              id="path69404"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69410" clipPathUnits="userSpaceOnUse">
            <path
              id="path69408"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69428" clipPathUnits="userSpaceOnUse">
            <path
              id="path69426"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69432" clipPathUnits="userSpaceOnUse">
            <path
              id="path69430"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69436" clipPathUnits="userSpaceOnUse">
            <path
              id="path69434"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69440" clipPathUnits="userSpaceOnUse">
            <path
              id="path69438"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69444" clipPathUnits="userSpaceOnUse">
            <path
              id="path69442"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69448" clipPathUnits="userSpaceOnUse">
            <path
              id="path69446"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-08" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-39"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-73" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-90"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-90"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69510" clipPathUnits="userSpaceOnUse">
            <path
              id="path69508"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69514" clipPathUnits="userSpaceOnUse">
            <path
              id="path69512"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69518" clipPathUnits="userSpaceOnUse">
            <path
              id="path69516"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69522" clipPathUnits="userSpaceOnUse">
            <path
              id="path69520"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69526" clipPathUnits="userSpaceOnUse">
            <path
              id="path69524"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69530" clipPathUnits="userSpaceOnUse">
            <path
              id="path69528"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69548" clipPathUnits="userSpaceOnUse">
            <path
              id="path69546"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69552" clipPathUnits="userSpaceOnUse">
            <path
              id="path69550"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-45" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-37"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-05" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-94"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69614" clipPathUnits="userSpaceOnUse">
            <path
              id="path69612"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69618" clipPathUnits="userSpaceOnUse">
            <path
              id="path69616"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69622" clipPathUnits="userSpaceOnUse">
            <path
              id="path69620"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69626" clipPathUnits="userSpaceOnUse">
            <path
              id="path69624"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69644" clipPathUnits="userSpaceOnUse">
            <path
              id="path69642"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69648" clipPathUnits="userSpaceOnUse">
            <path
              id="path69646"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69666" clipPathUnits="userSpaceOnUse">
            <path
              id="path69664"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69670" clipPathUnits="userSpaceOnUse">
            <path
              id="path69668"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69688" clipPathUnits="userSpaceOnUse">
            <path
              id="path69686"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69692" clipPathUnits="userSpaceOnUse">
            <path
              id="path69690"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69696" clipPathUnits="userSpaceOnUse">
            <path
              id="path69694"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69700" clipPathUnits="userSpaceOnUse">
            <path
              id="path69698"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69718" clipPathUnits="userSpaceOnUse">
            <path
              id="path69716"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69722" clipPathUnits="userSpaceOnUse">
            <path
              id="path69720"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69740" clipPathUnits="userSpaceOnUse">
            <path
              id="path69738"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69744" clipPathUnits="userSpaceOnUse">
            <path
              id="path69742"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69748" clipPathUnits="userSpaceOnUse">
            <path
              id="path69746"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69752" clipPathUnits="userSpaceOnUse">
            <path
              id="path69750"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69756" clipPathUnits="userSpaceOnUse">
            <path
              id="path69754"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69760" clipPathUnits="userSpaceOnUse">
            <path
              id="path69758"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69778" clipPathUnits="userSpaceOnUse">
            <path
              id="path69776"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69782" clipPathUnits="userSpaceOnUse">
            <path
              id="path69780"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69800" clipPathUnits="userSpaceOnUse">
            <path
              id="path69798"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69804" clipPathUnits="userSpaceOnUse">
            <path
              id="path69802"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69822" clipPathUnits="userSpaceOnUse">
            <path
              id="path69820"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69826" clipPathUnits="userSpaceOnUse">
            <path
              id="path69824"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69844" clipPathUnits="userSpaceOnUse">
            <path
              id="path69842"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69848" clipPathUnits="userSpaceOnUse">
            <path
              id="path69846"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69852" clipPathUnits="userSpaceOnUse">
            <path
              id="path69850"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69856" clipPathUnits="userSpaceOnUse">
            <path
              id="path69854"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69860" clipPathUnits="userSpaceOnUse">
            <path
              id="path69858"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69864" clipPathUnits="userSpaceOnUse">
            <path
              id="path69862"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69868" clipPathUnits="userSpaceOnUse">
            <path
              id="path69866"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69872" clipPathUnits="userSpaceOnUse">
            <path
              id="path69870"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69898" clipPathUnits="userSpaceOnUse">
            <path
              id="path69896"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69902" clipPathUnits="userSpaceOnUse">
            <path
              id="path69900"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-99" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-36"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-10" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69942" clipPathUnits="userSpaceOnUse">
            <path
              id="path69940"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath69946" clipPathUnits="userSpaceOnUse">
            <path
              id="path69944"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-98"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70008" clipPathUnits="userSpaceOnUse">
            <path
              id="path70006"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70012" clipPathUnits="userSpaceOnUse">
            <path
              id="path70010"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70016" clipPathUnits="userSpaceOnUse">
            <path
              id="path70014"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70020" clipPathUnits="userSpaceOnUse">
            <path
              id="path70018"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70024" clipPathUnits="userSpaceOnUse">
            <path
              id="path70022"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70028" clipPathUnits="userSpaceOnUse">
            <path
              id="path70026"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70046" clipPathUnits="userSpaceOnUse">
            <path
              id="path70044"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70050" clipPathUnits="userSpaceOnUse">
            <path
              id="path70048"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-18" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-57" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70130" clipPathUnits="userSpaceOnUse">
            <path
              id="path70128"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70134" clipPathUnits="userSpaceOnUse">
            <path
              id="path70132"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70138" clipPathUnits="userSpaceOnUse">
            <path
              id="path70136"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70142" clipPathUnits="userSpaceOnUse">
            <path
              id="path70140"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70146" clipPathUnits="userSpaceOnUse">
            <path
              id="path70144"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath70150" clipPathUnits="userSpaceOnUse">
            <path
              id="path70148"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <filter
            height={1.1046808}
            y={-0.052340381}
            width={1.0886487}
            x={-0.044324357}
            id="filter21611-1-1-4-7-3"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21613-4-0-4-9-1" stdDeviation={0.05935181} />
          </filter>
          <clipPath id="clipPath3307-2-7-3-1-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-2-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-0-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-3-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84700" clipPathUnits="userSpaceOnUse">
            <path
              id="path84698"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84704" clipPathUnits="userSpaceOnUse">
            <path
              id="path84702"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-99-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-1-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-36-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-10-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-1-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84744" clipPathUnits="userSpaceOnUse">
            <path
              id="path84742"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84748" clipPathUnits="userSpaceOnUse">
            <path
              id="path84746"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-7-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-9-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-4-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-5-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-1-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-7-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-7-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-98-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-5-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-4-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-8-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-2-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84810" clipPathUnits="userSpaceOnUse">
            <path
              id="path84808"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84814" clipPathUnits="userSpaceOnUse">
            <path
              id="path84812"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84818" clipPathUnits="userSpaceOnUse">
            <path
              id="path84816"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84822" clipPathUnits="userSpaceOnUse">
            <path
              id="path84820"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84826" clipPathUnits="userSpaceOnUse">
            <path
              id="path84824"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84830" clipPathUnits="userSpaceOnUse">
            <path
              id="path84828"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84848" clipPathUnits="userSpaceOnUse">
            <path
              id="path84846"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84852" clipPathUnits="userSpaceOnUse">
            <path
              id="path84850"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-6-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-18-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-0-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-0-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-0-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-57-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-8-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-1-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-7-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-87" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84932" clipPathUnits="userSpaceOnUse">
            <path
              id="path84930"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84936" clipPathUnits="userSpaceOnUse">
            <path
              id="path84934"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84940" clipPathUnits="userSpaceOnUse">
            <path
              id="path84938"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84944" clipPathUnits="userSpaceOnUse">
            <path
              id="path84942"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84948" clipPathUnits="userSpaceOnUse">
            <path
              id="path84946"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath84952" clipPathUnits="userSpaceOnUse">
            <path
              id="path84950"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-8-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-08-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-39-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-4-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-3-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-73-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-90-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-0-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-90-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-7-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-6-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-1-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-9-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-1-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94010" clipPathUnits="userSpaceOnUse">
            <path
              id="path94008"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94014" clipPathUnits="userSpaceOnUse">
            <path
              id="path94012"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94018" clipPathUnits="userSpaceOnUse">
            <path
              id="path94016"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94022" clipPathUnits="userSpaceOnUse">
            <path
              id="path94020"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94026" clipPathUnits="userSpaceOnUse">
            <path
              id="path94024"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94030" clipPathUnits="userSpaceOnUse">
            <path
              id="path94028"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94048" clipPathUnits="userSpaceOnUse">
            <path
              id="path94046"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94052" clipPathUnits="userSpaceOnUse">
            <path
              id="path94050"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-45-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-4-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-9-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-1-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-4-4-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-5-3-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-9-5-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-8-8-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-9-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-37-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-2-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-8-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-05-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-94-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-2-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94114" clipPathUnits="userSpaceOnUse">
            <path
              id="path94112"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94118" clipPathUnits="userSpaceOnUse">
            <path
              id="path94116"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94122" clipPathUnits="userSpaceOnUse">
            <path
              id="path94120"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94126" clipPathUnits="userSpaceOnUse">
            <path
              id="path94124"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94144" clipPathUnits="userSpaceOnUse">
            <path
              id="path94142"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94148" clipPathUnits="userSpaceOnUse">
            <path
              id="path94146"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94166" clipPathUnits="userSpaceOnUse">
            <path
              id="path94164"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94170" clipPathUnits="userSpaceOnUse">
            <path
              id="path94168"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94188" clipPathUnits="userSpaceOnUse">
            <path
              id="path94186"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94192" clipPathUnits="userSpaceOnUse">
            <path
              id="path94190"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94196" clipPathUnits="userSpaceOnUse">
            <path
              id="path94194"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94200" clipPathUnits="userSpaceOnUse">
            <path
              id="path94198"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94218" clipPathUnits="userSpaceOnUse">
            <path
              id="path94216"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94222" clipPathUnits="userSpaceOnUse">
            <path
              id="path94220"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94240" clipPathUnits="userSpaceOnUse">
            <path
              id="path94238"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94244" clipPathUnits="userSpaceOnUse">
            <path
              id="path94242"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94248" clipPathUnits="userSpaceOnUse">
            <path
              id="path94246"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94252" clipPathUnits="userSpaceOnUse">
            <path
              id="path94250"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94256" clipPathUnits="userSpaceOnUse">
            <path
              id="path94254"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94260" clipPathUnits="userSpaceOnUse">
            <path
              id="path94258"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94278" clipPathUnits="userSpaceOnUse">
            <path
              id="path94276"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94282" clipPathUnits="userSpaceOnUse">
            <path
              id="path94280"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94300" clipPathUnits="userSpaceOnUse">
            <path
              id="path94298"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94304" clipPathUnits="userSpaceOnUse">
            <path
              id="path94302"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94322" clipPathUnits="userSpaceOnUse">
            <path
              id="path94320"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94326" clipPathUnits="userSpaceOnUse">
            <path
              id="path94324"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94344" clipPathUnits="userSpaceOnUse">
            <path
              id="path94342"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94348" clipPathUnits="userSpaceOnUse">
            <path
              id="path94346"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94352" clipPathUnits="userSpaceOnUse">
            <path
              id="path94350"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94356" clipPathUnits="userSpaceOnUse">
            <path
              id="path94354"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94360" clipPathUnits="userSpaceOnUse">
            <path
              id="path94358"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94364" clipPathUnits="userSpaceOnUse">
            <path
              id="path94362"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94368" clipPathUnits="userSpaceOnUse">
            <path
              id="path94366"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath94372" clipPathUnits="userSpaceOnUse">
            <path
              id="path94370"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-15-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-33-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-6-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-9-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-93-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-2-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-22-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-87-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-1-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-83-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-51-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97384" clipPathUnits="userSpaceOnUse">
            <path
              id="path97382"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97388" clipPathUnits="userSpaceOnUse">
            <path
              id="path97386"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97392" clipPathUnits="userSpaceOnUse">
            <path
              id="path97390"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97396" clipPathUnits="userSpaceOnUse">
            <path
              id="path97394"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97400" clipPathUnits="userSpaceOnUse">
            <path
              id="path97398"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97404" clipPathUnits="userSpaceOnUse">
            <path
              id="path97402"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-65-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-06-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-3-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-1-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-7-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-9-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-1-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-5-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-6-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-13-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-71-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-8-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-26-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-8-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-6-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-6-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-9-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97484" clipPathUnits="userSpaceOnUse">
            <path
              id="path97482"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97488" clipPathUnits="userSpaceOnUse">
            <path
              id="path97486"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97492" clipPathUnits="userSpaceOnUse">
            <path
              id="path97490"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97496" clipPathUnits="userSpaceOnUse">
            <path
              id="path97494"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97514" clipPathUnits="userSpaceOnUse">
            <path
              id="path97512"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97518" clipPathUnits="userSpaceOnUse">
            <path
              id="path97516"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97536" clipPathUnits="userSpaceOnUse">
            <path
              id="path97534"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97540" clipPathUnits="userSpaceOnUse">
            <path
              id="path97538"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97558" clipPathUnits="userSpaceOnUse">
            <path
              id="path97556"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97562" clipPathUnits="userSpaceOnUse">
            <path
              id="path97560"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97566" clipPathUnits="userSpaceOnUse">
            <path
              id="path97564"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97570" clipPathUnits="userSpaceOnUse">
            <path
              id="path97568"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97574" clipPathUnits="userSpaceOnUse">
            <path
              id="path97572"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97578" clipPathUnits="userSpaceOnUse">
            <path
              id="path97576"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97582" clipPathUnits="userSpaceOnUse">
            <path
              id="path97580"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97586" clipPathUnits="userSpaceOnUse">
            <path
              id="path97584"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97604" clipPathUnits="userSpaceOnUse">
            <path
              id="path97602"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97608" clipPathUnits="userSpaceOnUse">
            <path
              id="path97606"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97626" clipPathUnits="userSpaceOnUse">
            <path
              id="path97624"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97630" clipPathUnits="userSpaceOnUse">
            <path
              id="path97628"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97648" clipPathUnits="userSpaceOnUse">
            <path
              id="path97646"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97652" clipPathUnits="userSpaceOnUse">
            <path
              id="path97650"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97656" clipPathUnits="userSpaceOnUse">
            <path
              id="path97654"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97660" clipPathUnits="userSpaceOnUse">
            <path
              id="path97658"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97664" clipPathUnits="userSpaceOnUse">
            <path
              id="path97662"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath97668" clipPathUnits="userSpaceOnUse">
            <path
              id="path97666"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-3-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-48-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-42-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-90-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12709">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12707"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12713">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12711"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12731">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12729"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12735">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12733"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-4-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-47-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-0-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-9-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-32-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-18-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-07-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-4-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-3-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-1-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-8-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12779">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12777"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12783">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12781"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-3-9-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-48-3-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-42-6-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-90-8-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-8-5-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-7-7-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-2-3-18" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-1-6-48"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12809" clipPathUnits="userSpaceOnUse">
            <path
              id="path12807"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12813" clipPathUnits="userSpaceOnUse">
            <path
              id="path12811"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12831" clipPathUnits="userSpaceOnUse">
            <path
              id="path12829"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath12835" clipPathUnits="userSpaceOnUse">
            <path
              id="path12833"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-8-5-1-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-7-7-6-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-2-3-1-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-1-6-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12857">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12855"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12861">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12859"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12879">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12877"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12883">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12881"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12887">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12885"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12891">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12889"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12895">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12893"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath12899">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path12897"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-68-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-80-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-7-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-4-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-65-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-9-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-0-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-5-6-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-1-7-91"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-0-7-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-6-7-03"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14651">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14649"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14655">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14653"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14659">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14657"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14663">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14661"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-49-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-69-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-88-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-13-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71-60"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14707">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14705"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath14711">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path14709"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-68-7-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-80-1-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-7-6-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-4-8-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-65-3-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-9-3-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-0-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-7-6-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-5-6-4-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-1-7-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-0-7-4-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-6-7-0-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14755" clipPathUnits="userSpaceOnUse">
            <path
              id="path14753"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14759" clipPathUnits="userSpaceOnUse">
            <path
              id="path14757"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14763" clipPathUnits="userSpaceOnUse">
            <path
              id="path14761"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14767" clipPathUnits="userSpaceOnUse">
            <path
              id="path14765"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-49-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-69-0-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-88-7-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-13-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-5-1-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-04-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-24-5-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-71-6-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-48-3-9-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-10-9-9-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-7-1-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-1-7-0-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14811" clipPathUnits="userSpaceOnUse">
            <path
              id="path14809"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath14815" clipPathUnits="userSpaceOnUse">
            <path
              id="path14813"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-5-1-1-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-04-8-8-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-24-5-5-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-71-6-4-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-3-9-8-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-9-9-8-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-1-4-8-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-7-0-0-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-9-59">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-6-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-20-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-25-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-2-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-5-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-71-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-4-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-6-3-9-8">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-5-8-3-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-2-9-0-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-5-6-33-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-6-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-2-72"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-5-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-5-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16961">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16959"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16965">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16963"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16969">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16967"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16973">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16971"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16977">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16975"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath16981">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path16979"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-45-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-2-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-31-3">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-04-56"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-2-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-27-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-10-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-8-47"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-4-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-7-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-54-99">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-52-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-3-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-6-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-9-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-8-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17057">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17055"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17061">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17059"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17065">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17063"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17069">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17067"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17073">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17071"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath17077">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path17075"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-9-5-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-6-7-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-20-6-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-25-0-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-2-1-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-5-0-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-71-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-4-2-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2-3-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5-0-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17121" clipPathUnits="userSpaceOnUse">
            <path
              id="path17119"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17125" clipPathUnits="userSpaceOnUse">
            <path
              id="path17123"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17129" clipPathUnits="userSpaceOnUse">
            <path
              id="path17127"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17133" clipPathUnits="userSpaceOnUse">
            <path
              id="path17131"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-45-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-2-4-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-31-9-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-04-5-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-2-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-27-1-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-10-4-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-8-4-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-4-2-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-7-7-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-54-9-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-52-5-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-6-1-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-2-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-5-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-5-9-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17195" clipPathUnits="userSpaceOnUse">
            <path
              id="path17193"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath17199" clipPathUnits="userSpaceOnUse">
            <path
              id="path17197"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-79"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-66" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-29"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-03" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-5-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-99"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-0-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-6-92" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-3-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-2-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-2-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-6-26" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-4-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-4-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-9-82"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-4-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-6-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-55"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-7-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-22"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-58" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-0-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-7-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-1-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-16"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-14" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-8-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-35"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-69"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-37" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-79" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-41" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-8-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-7-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-2-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-0-63"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-3-76" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-5-02"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-9-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-3-19"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-6-04" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-1-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-6-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-2-55"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-8-0-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-7-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-2-57-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-0-0-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-3-40-51" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-5-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-9-8-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-3-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-84-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-31-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-5-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-6-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-0-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-4-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-0-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-68" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-49" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-61"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-16" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-06"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-68"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-37"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-4-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-46" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-58" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-8-6-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-06-4-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-5-6-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-7-4-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-6-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-3-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-2-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-2-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-7-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-9-6-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-9-9-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-0-8-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-6-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-4-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-2-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-7-3-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-9-8-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-7-2-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-7-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-8-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-5-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-3-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-7-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-9-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-7-55" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-3-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-96"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-1-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-53"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-7-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-90"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-85" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-8-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-7-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-2-5" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-0-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-3-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-5-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-9-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-6-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-1-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-6-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-2-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-0-7-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-7-9-5-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-0-7-5-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-2-3-0-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-27" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-78"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-20" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-38"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-47" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-4-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-31"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-6-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-66"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-34"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-3-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-8-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-9-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-6-32"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-6-52" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-2-82" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-2-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-3-6-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-6-4-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-7-4-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-9-9-22"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-59" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-22"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-61" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-5-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-49-6-58" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-8-1-14"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-1-2-55" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-0-5-18"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-0-3-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-6-0-88"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-8-9-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-26-0-72"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-70"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-26" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-91"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-06"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-39" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-02"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-30" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-87"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-00"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-8-52" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-7-97"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-2-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-0-29"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-3-78" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-5-47"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-9-96" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-3-04"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-6-1-3-3-6-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-5-3-2-7-1-12"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-2-7-9-8-6-07" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-5-67-7-1-2-6"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-8-0-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-7-7-2"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-2-57-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-0-0-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-3-40-59" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-5-0-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-9-8-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-3-7-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-1-1-1-84-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-1-4-9-31-5"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-56-4-4-5-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-7-6-4-6-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-4-5-9-0-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-2-8-7-7-4-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-5-3-9-8-2-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-2-2-6-4-0-68"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-78" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-30"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-55" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-62"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient id="linearGradient3629">
            <stop offset={0} id="stop3625" stopColor="#fff" stopOpacity={1} />
            <stop offset={1} id="stop3627" stopColor="#37b4d7" stopOpacity={1} />
          </linearGradient>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-1-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-2-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-0-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-3-01"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-9-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-6-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-3-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-0-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30075">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30073"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30079">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30077"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-99-1">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-4-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-1-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-36-3"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-10-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-1-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-9-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-7-41"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30119">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30117"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30123">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30121"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-3-7-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-72-9-60"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-44-4-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-3-5-8"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-4-3-1-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-2-6-7-4"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-5-7-7-09">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-2-9-98-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-0-5-5">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-7-4-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-0-8-82">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-2-2-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-4-0">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-5-23"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-9-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-8-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30186">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30184"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30190">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30188"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30194">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30192"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30198">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30196"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30202">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30200"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30206">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30204"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30224">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30222"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30228">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30226"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-2-05">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-6-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-18-6">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-0-9"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-7-70-3-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-7-5-0-5"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-4-4-2-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-6-6-0-22"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-49-6-57-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-8-1-8-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-1-2-8-9">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-0-5-3-6"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-7-0-3-1-2">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-7-6-0-7-1"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-7-8-9-7-7">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-1-26-0-3-7"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-2-0-9-48-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-1-04-9-10-11"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath3307-6-6-2-8-7-4">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path3309-3-3-2-5-1-79"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30308">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30306"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30312">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30310"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30316">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30314"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30320">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30318"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30324">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30322"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath30328">
            <path
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              id="path30326"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            y2={41.939541}
            x2={464.38831}
            y1={41.939541}
            x1={463.64929}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient33323"
            xlinkHref="#linearGradient3629"
          />
          <linearGradient
            y2={39.562008}
            x2={504.38376}
            y1={39.562008}
            x1={503.52042}
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient33325"
            xlinkHref="#linearGradient3629"
          />
          <clipPath id="clipPath3307-2-7-3-1-9-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-2-7-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-0-5-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-3-01-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-1-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-4-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-4-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-3-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3692" clipPathUnits="userSpaceOnUse">
            <path
              id="path3690"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3696" clipPathUnits="userSpaceOnUse">
            <path
              id="path3694"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-99-1-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-4-0-8"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-1-0-0" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-36-3-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-1-2" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-4-10"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-4-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-3-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-7-70-10-2-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-7-5-1-0-9"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-4-4-9-0-3" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-6-6-7-41-4"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-7-3-1-9-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-7-72-2-7-0"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-7-44-0-5-1" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-1-3-3-01-7"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-2-0-9-9-1-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-1-04-9-6-4-3"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3307-6-6-2-8-3-4-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path3309-3-3-2-5-0-3-1"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath5148" clipPathUnits="userSpaceOnUse">
            <path
              id="path5146"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath5152" clipPathUnits="userSpaceOnUse">
            <path
              id="path5150"
              d="M215.13 299.215l1.959.546.228 1.96-1.868.728 1.959 2.186 3.097-1.002.866-1.73.045-2.734-1.548-1.64s-2.005-.136-2.232-.136c-.228 0-2.506 1.822-2.506 1.822z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient5780"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient5782"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient5812"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient5814"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6506"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6508"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6510"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6512"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6514"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6516"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6709"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6711"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6811"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6813"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6877"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient6879"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient7298"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient7300"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient7364"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient7366"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8408"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8410"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8412"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8414"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8416"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8418"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8424"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8426"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8428"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8430"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8432"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8434"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8436"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient8438"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10008"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10010"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10012"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10014"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10020"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10022"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10024"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient10026"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11021"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11023"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11025"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11027"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11033"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11035"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11037"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient11039"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12070"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12072"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12074"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12076"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12078"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12080"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12082"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12084"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12086"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12088"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12090"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12092"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12094"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient12096"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13523"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13525"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13527"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13529"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13531"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13533"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13535"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13537"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13539"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13541"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13543"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13545"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13547"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient13549"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14974"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14976"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14978"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14980"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14982"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient14984"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15181"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15183"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15255"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15257"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15382"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15384"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15456"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient15458"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16362"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16364"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16366"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16368"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16370"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16372"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16374"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16376"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16378"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16380"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16382"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16384"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16386"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient16388"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17568"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17570"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17572"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17574"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17576"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17578"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17580"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17582"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17584"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17586"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17588"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17590"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17592"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient17594"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18773"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18775"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18785"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18787"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18789"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18791"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18793"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient18795"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19878"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19880"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19882"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19884"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19890"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19892"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19894"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={463.64929}
            y1={41.939541}
            x2={464.38831}
            y2={41.939541}
          />
          <linearGradient
            xlinkHref="#linearGradient3629"
            id="linearGradient19896"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.3457 0 0 1 658.838 -124.139)"
            x1={503.52042}
            y1={39.562008}
            x2={504.38376}
            y2={39.562008}
          />
        </defs>
        <g id="layer1">
          <g id="1b_f9" transform="translate(8.996)" opacity={1}>
            <g id="g14535">
              <g id="g13111" transform="translate(15.357 145.64)">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13085"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13087"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13089"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13091"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13093"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13095"
                  fill="none"
                  stroke="url(#linearGradient13523)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13097"
                  fill="none"
                  stroke="url(#linearGradient13525)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13099"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13101"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13103"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13105"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13107"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13109"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.6437988}
                y={64.398384}
                id="etiqueta2"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.6437988}
                  y={64.398384}
                  style={{}}
                  id="etiqueta2"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq2}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="etiqueta1"
                y={41.455215}
                x={9.6433334}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54777px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta1"
                  style={{}}
                  y={41.455215}
                  x={9.6433334}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq1}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13123"
                y={89.013039}
                x={9.6433334}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta3"
                  style={{}}
                  y={89.013039}
                  x={9.6433334}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq3}
                </tspan>
              </text>
              <g id="g13149" transform="translate(15.357 142.465)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13125"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13127"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13129"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13131"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13133"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13135"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13137"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13139"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13141"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13143"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13145"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13147"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13175" transform="translate(15.357 93.782)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13151"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13153"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13155"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13157"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13159"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13161"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13163"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13165"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13167"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13169"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13171"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13173"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g transform="translate(15.357 194.324)" id="g13203">
                <path
                  id="path13177"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13179"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13181"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13183"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13185"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13187"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13527)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13189"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13529)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13191"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13193"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13195"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13197"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13199"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13201"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13207"
                y={111.82534}
                x={9.6437988}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta4"
                  style={{}}
                  y={111.82534}
                  x={9.6437988}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq4}
                </tspan>
              </text>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.6433334}
                y={135.40907}
                id="text13211"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.6433334}
                  y={135.40907}
                  style={{}}
                  id="etiqueta5"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq5}
                </tspan>
              </text>
              <g transform="translate(15.357 190.09)" id="g13237" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13213"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13215"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13217"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13219"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13221"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13223"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13225"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13227"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13229"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13231"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13233"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13235"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13265" transform="translate(15.65 242.478)">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13239"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13241"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13243"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13245"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13247"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13249"
                  fill="none"
                  stroke="url(#linearGradient13531)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13251"
                  fill="none"
                  stroke="url(#linearGradient13533)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13253"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13255"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13257"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13259"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13261"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13263"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={9.9439859}
                y={158.73724}
                id="text13269"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={9.9439859}
                  y={158.73724}
                  style={{}}
                  id="etiqueta6"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq6}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13273"
                y={182.83675}
                x={9.943778}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.35px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta7"
                  style={{}}
                  y={182.83675}
                  x={9.943778}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.76111px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq7}
                </tspan>
              </text>
              <g id="g13299" transform="translate(15.357 238.774)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13275"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13277"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13279"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13281"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13283"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13285"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13287"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13289"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13291"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13293"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13295"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13297"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
            </g>
            <g id="g14425">
              <g id="g13325" transform="matrix(-1 0 0 1 404.74 117.139)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13301"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13303"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13305"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13307"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13309"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13311"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13313"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13315"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13317"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13319"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13321"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13323"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <g id="g13353" transform="matrix(-1 0 0 1 404.4 170.966)">
                <path
                  id="path13327"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13329"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13331"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13333"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13335"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13337"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13535)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13339"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13537)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13341"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13343"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13345"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13347"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13349"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13351"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <g transform="matrix(-1 0 0 1 404.4 122.283)" id="g13381">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13355"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13357"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13359"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13361"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13363"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13365"
                  fill="none"
                  stroke="url(#linearGradient13539)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13367"
                  fill="none"
                  stroke="url(#linearGradient13541)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13369"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13371"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13373"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13375"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13377"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13379"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78024}
                y={41.868366}
                id="text13385"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78024}
                  y={41.868366}
                  style={{}}
                  id="etiqueta8"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq8}
                </tspan>
              </text>
              <text
                transform="scale(.9742 1.02648)"
                id="text13389"
                y={64.694275}
                x={391.78024}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta9"
                  style={{}}
                  y={64.694275}
                  x={391.78024}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq9}
                </tspan>
              </text>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.7803}
                y={89.295418}
                id="text13393"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.7803}
                  y={89.295418}
                  style={{}}
                  id="etiqueta10"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq10}
                </tspan>
              </text>
              <g transform="matrix(-1 0 0 1 404.74 165.822)" id="g13419" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-53.700035}
                  cx={-55.325737}
                  id="ellipse13395"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-45.604927}
                  cx={-31.388998}
                  id="ellipse13397"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13399"
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13401"
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13403"
                  d="M34.547-58.566v9.736"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  id="path13405"
                  d="M-19.191-59.766v7.38"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13407"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13409"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  id="path13411"
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13413"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13415"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13417"
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78024}
                y={112.12059}
                id="text13423"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78024}
                  y={112.12059}
                  style={{}}
                  id="etiqueta11"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq11}
                </tspan>
              </text>
              <g transform="matrix(-1 0 0 1 404.4 218.591)" id="g13451">
                <path
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  id="path13425"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  id="path13427"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13429"
                  cx={-55.190727}
                  cy={-82.201485}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  transform="scale(-1 1)"
                  id="ellipse13431"
                  cx={-31.253988}
                  cy={-74.106369}
                  rx={1.2687327}
                  ry={1.0378371}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  id="path13433"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  d="M34.412-87.068v9.737"
                  id="path13435"
                  fill="none"
                  stroke="url(#linearGradient13543)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  d="M-19.326-88.267v7.38"
                  id="path13437"
                  fill="none"
                  stroke="url(#linearGradient13545)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13439"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  transform="scale(-1 1)"
                  id="rect13441"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  id="path13443"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13445"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13447"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  id="path13449"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13455"
                y={135.69116}
                x={391.7803}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta12"
                  style={{}}
                  y={135.69116}
                  x={391.7803}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq12}
                </tspan>
              </text>
              <g id="g13481" transform="matrix(-1 0 0 1 404.74 213.977)" stroke="#fff" strokeOpacity={1}>
                <ellipse
                  id="ellipse13457"
                  cx={-55.325737}
                  cy={-53.700035}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  id="ellipse13459"
                  cx={-31.388998}
                  cy={-45.604927}
                  rx={1.2687327}
                  ry={1.0378371}
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M33.404-48.95v-10.372c-.047-.553-.242-1.032-1.316-1.076h-52.393v11.546l11.843 4.958h35.725"
                  id="path13461"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M54.395-53.677h-8.909l-8.233 8.21h-5.47"
                  id="path13463"
                  fill="none"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  d="M34.547-58.566v9.736"
                  id="path13465"
                  fill="none"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                />
                <path
                  d="M-19.191-59.766v7.38"
                  id="path13467"
                  fill="none"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                />
                <path
                  id="rect13469"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -47.315445H5.009535V-46.623553509999994H-25.947546z"
                />
                <path
                  id="rect13471"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.947546 -46.070038H-9.313049V-45.37814650999999H-25.947546z"
                />
                <path
                  d="M51.113-50.037h-1.1c.027-1.255.377-1.38-1.578-1.384l.084-.945c2.632.022 2.644-.113 2.594 2.329z"
                  id="path13473"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
                <path
                  id="path13475"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-2-0-9-9-1)"
                  transform="matrix(-.8279 0 0 .67722 211.89 -249.276)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13477"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  clipPath="url(#clipPath3307-6-6-2-8-3-4)"
                  transform="matrix(.8279 0 0 .67722 -125.046 -257.315)"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  d="M46.583-50.212h1.1c-.027 1.255-.376 1.38 1.579 1.384l-.085.945c-2.632-.022-2.643.113-2.594-2.33z"
                  id="path13479"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                />
              </g>
              <text
                transform="scale(.9742 1.02648)"
                id="text13485"
                y={159.03304}
                x={391.78024}
                style={{
                  lineHeight: 1.25,
                }}
                xmlSpace="preserve"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  id="etiqueta13"
                  style={{}}
                  y={159.03304}
                  x={391.78024}
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq13}
                </tspan>
              </text>
              <g id="g13513" transform="matrix(-1 0 0 1 404.4 267.275)">
                <path
                  id="path13487"
                  d="M33.269-77.451v-10.372c-.047-.553-.242-1.032-1.316-1.076H-20.44v11.546l11.843 4.957h35.725"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13489"
                  d="M-17.223-75.621l.378-.576-3.172-1.46v-1.485l-.801.198v1.683z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="none"
                  strokeWidth=".233655px"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity={1}
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-82.201485}
                  cx={-55.190727}
                  id="ellipse13491"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <ellipse
                  ry={1.0378371}
                  rx={1.2687327}
                  cy={-74.106369}
                  cx={-31.253988}
                  id="ellipse13493"
                  transform="scale(-1 1)"
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.224436}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13495"
                  d="M54.26-82.178h-8.909l-8.233 8.21h-5.47"
                  fill="none"
                  stroke="#3edce3"
                  strokeWidth={0.441975}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  id="path13497"
                  d="M34.412-87.068v9.737"
                  fill="none"
                  stroke="url(#linearGradient13547)"
                  strokeWidth={0.857269}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".857269,.857269"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="path13499"
                  d="M-19.326-88.267v7.38"
                  fill="none"
                  stroke="url(#linearGradient13549)"
                  strokeWidth={1.00152}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray=".500759,.500759"
                  strokeDashoffset={0}
                  strokeOpacity={1}
                />
                <path
                  id="rect13501"
                  transform="scale(-1 1)"
                  opacity={0.696078}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.209474}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -75.816887H5.144544999999997V-75.12499550999999H-25.812536z"
                />
                <path
                  id="rect13503"
                  transform="scale(-1 1)"
                  opacity={0.5}
                  fill="#3edce3"
                  fillOpacity={1}
                  strokeWidth={0.153552}
                  paintOrder="stroke markers fill"
                  d="M-25.812536 -74.57148H-9.178039000000002V-73.87958850999999H-25.812536z"
                />
                <path
                  id="path13505"
                  d="M50.978-78.538h-1.1c.027-1.256.377-1.381-1.578-1.384l.084-.946c2.632.023 2.644-.112 2.594 2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.116004}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
                <path
                  transform="matrix(-.8279 0 0 .67722 211.754 -277.777)"
                  clipPath="url(#clipPath3307-2-7-3-1-9)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13507"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  transform="matrix(.8279 0 0 .67722 -125.181 -285.817)"
                  clipPath="url(#clipPath3307-6-6-7-44-0-5)"
                  d="M217.978 298.221a2.52 2.52 0 00-2.475 2.52 2.52 2.52 0 002.52 2.52 2.52 2.52 0 002.52-2.52 2.52 2.52 0 00-2.52-2.52 2.52 2.52 0 00-.045 0zm.045.25a2.27 2.27 0 012.269 2.27 2.27 2.27 0 01-2.27 2.269 2.27 2.27 0 01-2.268-2.27 2.27 2.27 0 012.269-2.269z"
                  id="path13509"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.492842}
                  paintOrder="stroke markers fill"
                />
                <path
                  id="path13511"
                  d="M46.448-78.714h1.1c-.027 1.256-.376 1.381 1.579 1.384l-.085.946c-2.632-.023-2.643.112-2.594-2.33z"
                  fill="#3edce3"
                  fillOpacity={1}
                  stroke="#fff"
                  strokeWidth={0.0939632}
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeMiterlimit={4}
                  strokeDasharray="none"
                  strokeOpacity={1}
                />
              </g>
              <text
                xmlSpace="preserve"
                style={{
                  lineHeight: 1.25,
                }}
                x={391.78061}
                y={183.11919}
                id="text13517"
                transform="scale(.9742 1.02648)"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="6.54778px"
                fontFamily="Franklin Gothic Medium"
                letterSpacing={0}
                wordSpacing={0}
                display="inline"
                fill="#fff"
                fillOpacity={1}
                stroke="none"
                strokeWidth={0.304205}
              >
                <tspan
                  x={391.78061}
                  y={183.11919}
                  style={{}}
                  id="etiqueta14"
                  fontStyle="normal"
                  fontVariant="normal"
                  fontWeight={400}
                  fontStretch="normal"
                  fontSize="7.7611px"
                  fontFamily="Franklin Gothic Medium"
                  fill="#fff"
                  fillOpacity={1}
                  strokeWidth={0.304205}
                >
                  {etq14}
                </tspan>
              </text>
            </g>
          </g>

          <path
            id="path1767"
            d="M95.926 55.938v-8.607l-3.294-2.573V22.893l-2.028-1.583v-2.573l7.73-5.936h18.756l5.196 4.155H326.37l4.816-3.76h11.279l4.182 3.068v4.056l-6.59 5.046v17.322l6.623 5.248v12.418l-3.987 2.903v135.67l2.106 1.363-.09 1.96-6.586 5.036h-28.179l-3.943-2.938H119.636l-2.24-1.889h-9.454l-2.285 1.924h-10.35l-2.285-1.924V57.94z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1769"
            d="M92.086 187.525l-1.165-1.501v-9.89l2.844-2.9h4.755v12.065l-1.865 2.226z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1771"
            d="M93.987 43.676v-19.7l3.213 2.697v19.99z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1773"
            d="M100.487 15.338l1.654-1.45h13.067l4.337 4.246h206.444l-.685.984H104.48z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1775"
            d="M346.398 21.733v8.069l-5.38 4.396V26.13z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1777"
            d="M341.165 37.483v4.518l5.35 4.444v-4.71z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1779"
            d="M341.047 34.923v1.86l5.558 4.276v-4.88l-3.459-2.778z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1781"
            d="M346.605 35.382v-4.638l-2.897 2.295z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1783"
            d="M344.667 199.556l-1.208-.943.088-134.653 1.742-1.18v1.722l-.477.26z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1785"
            d="M342.209 61.817l3.365-2.336V48.364l-3.194-2.378z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M120.593 200.258h202.774l7.333 6.223h-3.53l-5.044-4.127h-9.061v-.938H121.902z"
            id="path1787"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M324.676 200.202h3.1l7.301 6.282-2.976.006z"
            id="path1789"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={29.050831}
            id="text1799"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={29.050831}
              style={{}}
              id="tspan1791"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'1'}
            </tspan>
            <tspan
              id="tspan1793"
              x={132.74138}
              y={35.781006}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'3'}
            </tspan>
            <tspan
              id="tspan1795"
              x={132.74138}
              y={42.511181}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'5'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur1"
            y={29.04401}
            x={147.70213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={29.04401}
              x={147.70213}
              id="tspan1801"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[1]}
            </tspan>
          </text>
          <path
            id="path1807"
            d="M328.91 200.202h3l7.065 6.21-2.88.007z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.405522}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M54.82 213.162h3.214v2.721h-3.15z"
            id="path1809"
            transform="matrix(0 50.68693 -.32705 0 288.611 -2750.934)"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.175829}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4-7-3)"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={35.906147}
            id="pn1_cur3"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1811"
              x={147.70213}
              y={35.906147}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[3]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={42.768269}
            id="pn1_cur5"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1817"
              x={147.70213}
              y={42.768269}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[5]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={49.630398}
            id="pn1_cur7"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1823"
              x={147.70213}
              y={49.630398}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[7]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={56.49255}
            id="pn1_cur9"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1829"
              x={147.70213}
              y={56.49255}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[9]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={63.35471}
            id="pn1_cur11"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1835"
              x={147.70213}
              y={63.35471}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[11]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={70.216881}
            id="pn1_cur13"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1841"
              x={147.70213}
              y={70.216881}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[13]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={77.079102}
            id="pn1_cur15"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1847"
              x={147.70213}
              y={77.079102}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[15]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={83.941284}
            id="pn1_cur17"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1853"
              x={147.70213}
              y={83.941284}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[17]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={90.803421}
            id="pn1_cur19"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1859"
              x={147.70213}
              y={90.803421}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[19]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={97.665588}
            id="pn1_cur21"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1865"
              x={147.70213}
              y={97.665588}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[21]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={104.52769}
            id="pn1_cur23"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1871"
              x={147.70213}
              y={104.52769}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[23]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={111.38982}
            id="pn1_cur25"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1877"
              x={147.70213}
              y={111.38982}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[25]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={118.25198}
            id="pn1_cur27"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1883"
              x={147.70213}
              y={118.25198}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[27]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={125.11412}
            id="pn1_cur29"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1889"
              x={147.70213}
              y={125.11412}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[29]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={131.97623}
            id="pn1_cur31"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1895"
              x={147.70213}
              y={131.97623}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[31]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={138.83829}
            id="pn1_cur33"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1901"
              x={147.70213}
              y={138.83829}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[33]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={145.70039}
            id="pn1_cur35"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1907"
              x={147.70213}
              y={145.70039}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[35]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={152.56253}
            id="pn1_cur37"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1913"
              x={147.70213}
              y={152.56253}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[37]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={159.42468}
            id="pn1_cur39"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1919"
              x={147.70213}
              y={159.42468}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[39]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={147.70213}
            y={166.28683}
            id="pn1_cur41"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1925"
              x={147.70213}
              y={166.28683}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[41]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={193.1982}
            y={29.0229}
            id="pn1_kva1"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1931"
              x={193.1982}
              y={29.0229}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[1]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva3"
            y={35.885036}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.885036}
              x={193.1982}
              id="tspan1937"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[3]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva5"
            y={42.74715}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.74715}
              x={193.1982}
              id="tspan1943"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[5]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva7"
            y={49.609272}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.609272}
              x={193.1982}
              id="tspan1949"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[7]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva9"
            y={56.471401}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.471401}
              x={193.1982}
              id="tspan1955"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[9]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva11"
            y={63.333553}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.333553}
              x={193.1982}
              id="tspan1961"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[11]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva13"
            y={70.195702}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.195702}
              x={193.1982}
              id="tspan1967"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[13]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva15"
            y={77.05793}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.05793}
              x={193.1982}
              id="tspan1973"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[15]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva17"
            y={83.92012}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.92012}
              x={193.1982}
              id="tspan1979"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[17]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva19"
            y={90.782333}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.782333}
              x={193.1982}
              id="tspan1985"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[19]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva21"
            y={97.644516}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.644516}
              x={193.1982}
              id="tspan1991"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[21]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva23"
            y={104.50671}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.50671}
              x={193.1982}
              id="tspan1997"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[23]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva25"
            y={111.3689}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.3689}
              x={193.1982}
              id="tspan2003"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[25]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva27"
            y={118.23106}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.23106}
              x={193.1982}
              id="tspan2009"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[27]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva29"
            y={125.0932}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.0932}
              x={193.1982}
              id="tspan2015"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[29]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva31"
            y={131.95528}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.95528}
              x={193.1982}
              id="tspan2021"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[31]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva33"
            y={138.81732}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.81732}
              x={193.1982}
              id="tspan2027"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[33]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva35"
            y={145.67931}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.67931}
              x={193.1982}
              id="tspan2033"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[35]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva37"
            y={152.54131}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.54131}
              x={193.1982}
              id="tspan2039"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[37]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva39"
            y={159.40338}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.40338}
              x={193.1982}
              id="tspan2045"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[39]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva41"
            y={166.26544}
            x={193.1982}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.26544}
              x={193.1982}
              id="tspan2051"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[41]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={215.80942}
            y={28.998089}
            id="pn1_kw1"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2057"
              x={215.80942}
              y={28.998089}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[1]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw3"
            y={35.860226}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.860226}
              x={215.80942}
              id="tspan2063"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[3]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw5"
            y={42.72234}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.72234}
              x={215.80942}
              id="tspan2069"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[5]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw7"
            y={49.584476}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.584476}
              x={215.80942}
              id="tspan2075"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[7]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw9"
            y={56.446621}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.446621}
              x={215.80942}
              id="tspan2081"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[9]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw11"
            y={63.308796}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.308796}
              x={215.80942}
              id="tspan2087"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[11]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw13"
            y={70.170952}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.170952}
              x={215.80942}
              id="tspan2093"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[13]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw15"
            y={77.03318}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.03318}
              x={215.80942}
              id="tspan2099"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[15]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw17"
            y={83.89534}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.89534}
              x={215.80942}
              id="tspan2105"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[17]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw19"
            y={90.757507}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.757507}
              x={215.80942}
              id="tspan2111"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[19]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw21"
            y={97.619644}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.619644}
              x={215.80942}
              id="tspan2117"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[21]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw23"
            y={104.48181}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.48181}
              x={215.80942}
              id="tspan2123"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[23]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw25"
            y={111.34392}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.34392}
              x={215.80942}
              id="tspan2129"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[25]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw27"
            y={118.20607}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.20607}
              x={215.80942}
              id="tspan2135"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[27]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw29"
            y={125.06822}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.06822}
              x={215.80942}
              id="tspan2141"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[29]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw31"
            y={131.93028}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.93028}
              x={215.80942}
              id="tspan2147"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[31]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw33"
            y={138.79234}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.79234}
              x={215.80942}
              id="tspan2153"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[33]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw35"
            y={145.65446}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.65446}
              x={215.80942}
              id="tspan2159"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[35]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw37"
            y={152.51659}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.51659}
              x={215.80942}
              id="tspan2165"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[37]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw39"
            y={159.37874}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.37874}
              x={215.80942}
              id="tspan2171"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[39]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw41"
            y={166.24089}
            x={215.80942}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.24089}
              x={215.80942}
              id="tspan2177"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[41]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={236.94408}
            y={29.106876}
            id="pn1_kwh1"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2183"
              x={236.94408}
              y={29.106876}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[1]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh3"
            y={35.969013}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.969013}
              x={236.94408}
              id="tspan2189"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[3]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh5"
            y={42.831142}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.831142}
              x={236.94408}
              id="tspan2195"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[5]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh7"
            y={49.693272}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.693272}
              x={236.94408}
              id="tspan2201"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[7]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh9"
            y={56.555416}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.555416}
              x={236.94408}
              id="tspan2207"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[9]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh11"
            y={63.417591}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.417591}
              x={236.94408}
              id="tspan2213"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[11]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh13"
            y={70.279747}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.279747}
              x={236.94408}
              id="tspan2219"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[13]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh15"
            y={77.14196}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.14196}
              x={236.94408}
              id="tspan2225"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[15]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh17"
            y={84.004135}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={84.004135}
              x={236.94408}
              id="tspan2231"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[17]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh19"
            y={90.866287}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.866287}
              x={236.94408}
              id="tspan2237"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[19]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh21"
            y={97.728424}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.728424}
              x={236.94408}
              id="tspan2243"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[21]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh23"
            y={104.59058}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.59058}
              x={236.94408}
              id="tspan2249"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[23]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh25"
            y={111.4527}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.4527}
              x={236.94408}
              id="tspan2255"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[25]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh27"
            y={118.31485}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.31485}
              x={236.94408}
              id="tspan2261"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[27]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh29"
            y={125.177}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.177}
              x={236.94408}
              id="tspan2267"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[29]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh31"
            y={132.03908}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={132.03908}
              x={236.94408}
              id="tspan2273"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[31]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh33"
            y={138.90115}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.90115}
              x={236.94408}
              id="tspan2279"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[33]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh35"
            y={145.76328}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.76328}
              x={236.94408}
              id="tspan2285"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[35]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh37"
            y={152.6254}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.6254}
              x={236.94408}
              id="tspan2291"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[37]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh39"
            y={159.48756}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.48756}
              x={236.94408}
              id="tspan2297"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[39]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh41"
            y={166.3497}
            x={236.94408}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.3497}
              x={236.94408}
              id="tspan2303"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[41]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={288.24213}
            y={28.912685}
            id="pn1_cur2"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2309"
              x={288.24213}
              y={28.912685}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[2]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur4"
            y={35.774822}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.774822}
              x={288.24213}
              id="tspan2315"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[4]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur6"
            y={42.636944}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.636944}
              x={288.24213}
              id="tspan2321"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[6]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur8"
            y={49.499081}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.499081}
              x={288.24213}
              id="tspan2327"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[8]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur10"
            y={56.361225}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.361225}
              x={288.24213}
              id="tspan2333"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[10]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur12"
            y={63.223392}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.223392}
              x={288.24213}
              id="tspan2339"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[12]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur14"
            y={70.085556}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.085556}
              x={288.24213}
              id="tspan2346"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[14]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur16"
            y={76.947762}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={76.947762}
              x={288.24213}
              id="tspan2352"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[16]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur18"
            y={83.809937}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.809937}
              x={288.24213}
              id="tspan2358"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[18]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur20"
            y={90.672089}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.672089}
              x={288.24213}
              id="tspan2364"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[20]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur22"
            y={97.534256}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.534256}
              x={288.24213}
              id="tspan2370"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[22]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur24"
            y={104.39639}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.39639}
              x={288.24213}
              id="tspan2376"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[24]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur26"
            y={111.25853}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.25853}
              x={288.24213}
              id="tspan2382"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[26]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur28"
            y={118.12064}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.12064}
              x={288.24213}
              id="tspan2388"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[28]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur30"
            y={124.9828}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={124.9828}
              x={288.24213}
              id="tspan2394"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[30]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur32"
            y={131.84489}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.84489}
              x={288.24213}
              id="tspan2400"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[32]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur34"
            y={138.70699}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.70699}
              x={288.24213}
              id="tspan2406"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[34]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur36"
            y={145.56905}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.56905}
              x={288.24213}
              id="tspan2412"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[36]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur38"
            y={152.4312}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.4312}
              x={288.24213}
              id="tspan2418"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[38]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur40"
            y={159.29335}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.29335}
              x={288.24213}
              id="tspan2424"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[40]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_cur42"
            y={166.15552}
            x={288.24213}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.15552}
              x={288.24213}
              id="tspan2430"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {cur[42]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kva2"
            y={28.891567}
            x={333.73782}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={28.891567}
              x={333.73782}
              id="tspan2436"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[2]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={35.753704}
            id="pn1_kva4"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2442"
              x={333.73782}
              y={35.753704}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[4]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={42.615833}
            id="pn1_kva6"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2448"
              x={333.73782}
              y={42.615833}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[6]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={49.47794}
            id="pn1_kva8"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2454"
              x={333.73782}
              y={49.47794}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[8]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={56.340084}
            id="pn1_kva10"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2460"
              x={333.73782}
              y={56.340084}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[10]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={63.202236}
            id="pn1_kva12"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2466"
              x={333.73782}
              y={63.202236}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[12]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={70.064369}
            id="pn1_kva14"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2472"
              x={333.73782}
              y={70.064369}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[14]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={76.926598}
            id="pn1_kva16"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2478"
              x={333.73782}
              y={76.926598}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[16]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={83.788788}
            id="pn1_kva18"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2484"
              x={333.73782}
              y={83.788788}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[18]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={90.651001}
            id="pn1_kva20"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2490"
              x={333.73782}
              y={90.651001}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[20]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={97.513184}
            id="pn1_kva22"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2496"
              x={333.73782}
              y={97.513184}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[22]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={104.37541}
            id="pn1_kva24"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2502"
              x={333.73782}
              y={104.37541}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[24]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={111.23756}
            id="pn1_kva26"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2508"
              x={333.73782}
              y={111.23756}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[26]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={118.09973}
            id="pn1_kva28"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2514"
              x={333.73782}
              y={118.09973}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[28]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={124.96188}
            id="pn1_kva30"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2520"
              x={333.73782}
              y={124.96188}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[30]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={131.82397}
            id="pn1_kva32"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2526"
              x={333.73782}
              y={131.82397}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[32]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={138.686}
            id="pn1_kva34"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2532"
              x={333.73782}
              y={138.686}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[34]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={145.54797}
            id="pn1_kva36"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2538"
              x={333.73782}
              y={145.54797}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[36]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={152.40999}
            id="pn1_kva38"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2544"
              x={333.73782}
              y={152.40999}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[38]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={159.27205}
            id="pn1_kva40"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2550"
              x={333.73782}
              y={159.27205}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[40]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={333.73782}
            y={166.13411}
            id="pn1_kva42"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2556"
              x={333.73782}
              y={166.13411}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kva[42]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kw2"
            y={28.866756}
            x={357.59726}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={28.866756}
              x={357.59726}
              id="tspan2562"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[2]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={35.728893}
            id="pn1_kw4"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2568"
              x={357.59726}
              y={35.728893}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[4]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={42.591015}
            id="pn1_kw6"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2574"
              x={357.59726}
              y={42.591015}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[6]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={49.453144}
            id="pn1_kw8"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2580"
              x={357.59726}
              y={49.453144}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[8]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={56.315304}
            id="pn1_kw10"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2586"
              x={357.59726}
              y={56.315304}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[10]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={63.177464}
            id="pn1_kw12"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2592"
              x={357.59726}
              y={63.177464}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[12]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={70.039612}
            id="pn1_kw14"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2598"
              x={357.59726}
              y={70.039612}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[14]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={76.901855}
            id="pn1_kw16"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2604"
              x={357.59726}
              y={76.901855}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[16]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={83.764008}
            id="pn1_kw18"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2610"
              x={357.59726}
              y={83.764008}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[18]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={90.626175}
            id="pn1_kw20"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2616"
              x={357.59726}
              y={90.626175}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[20]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={97.488327}
            id="pn1_kw22"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2622"
              x={357.59726}
              y={97.488327}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[22]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={104.35046}
            id="pn1_kw24"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2628"
              x={357.59726}
              y={104.35046}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[24]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={111.21257}
            id="pn1_kw26"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2634"
              x={357.59726}
              y={111.21257}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[26]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={118.07472}
            id="pn1_kw28"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2640"
              x={357.59726}
              y={118.07472}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[28]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={124.93687}
            id="pn1_kw30"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2646"
              x={357.59726}
              y={124.93687}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[30]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={131.79897}
            id="pn1_kw32"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2652"
              x={357.59726}
              y={131.79897}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[32]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={138.66103}
            id="pn1_kw34"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2658"
              x={357.59726}
              y={138.66103}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[34]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={145.5231}
            id="pn1_kw36"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2664"
              x={357.59726}
              y={145.5231}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[36]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={152.38525}
            id="pn1_kw38"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2670"
              x={357.59726}
              y={152.38525}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[38]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={159.24741}
            id="pn1_kw40"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2676"
              x={357.59726}
              y={159.24741}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[40]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={357.59726}
            y={166.10957}
            id="pn1_kw42"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2682"
              x={357.59726}
              y={166.10957}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kw[42]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_kwh2"
            y={28.975544}
            x={379.3558}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={28.975544}
              x={379.3558}
              id="tspan2688"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[2]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={35.837681}
            id="pn1_kwh4"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2694"
              x={379.3558}
              y={35.837681}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[4]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={42.69981}
            id="pn1_kwh6"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2700"
              x={379.3558}
              y={42.69981}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[6]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={49.561939}
            id="pn1_kwh8"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2706"
              x={379.3558}
              y={49.561939}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[8]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={56.424107}
            id="pn1_kwh10"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2712"
              x={379.3558}
              y={56.424107}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[10]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={63.286266}
            id="pn1_kwh12"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2718"
              x={379.3558}
              y={63.286266}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[12]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={70.148407}
            id="pn1_kwh14"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2724"
              x={379.3558}
              y={70.148407}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[14]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={77.010628}
            id="pn1_kwh16"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2730"
              x={379.3558}
              y={77.010628}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[16]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={83.872787}
            id="pn1_kwh18"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2736"
              x={379.3558}
              y={83.872787}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[18]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={90.73497}
            id="pn1_kwh20"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2742"
              x={379.3558}
              y={90.73497}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[20]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={97.597107}
            id="pn1_kwh22"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2748"
              x={379.3558}
              y={97.597107}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[22]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={104.45924}
            id="pn1_kwh24"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2754"
              x={379.3558}
              y={104.45924}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[24]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={111.32135}
            id="pn1_kwh26"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2760"
              x={379.3558}
              y={111.32135}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[26]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={118.1835}
            id="pn1_kwh28"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2766"
              x={379.3558}
              y={118.1835}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[28]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={125.04565}
            id="pn1_kwh30"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2772"
              x={379.3558}
              y={125.04565}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[30]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={131.90778}
            id="pn1_kwh32"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2778"
              x={379.3558}
              y={131.90778}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[32]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={138.76984}
            id="pn1_kwh34"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2784"
              x={379.3558}
              y={138.76984}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[34]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={145.63191}
            id="pn1_kwh36"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2790"
              x={379.3558}
              y={145.63191}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[36]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={152.49408}
            id="pn1_kwh38"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2796"
              x={379.3558}
              y={152.49408}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[38]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={159.35622}
            id="pn1_kwh40"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2802"
              x={379.3558}
              y={159.35622}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[40]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={379.3558}
            y={166.21838}
            id="pn1_kwh42"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2808"
              x={379.3558}
              y={166.21838}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {kwh[42]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2816"
            y={21.818066}
            x={129.99599}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={129.99599}
              id="tspan2814"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'CH'}
            </tspan>
          </text>
          <path
            d="M103.83 52.571h110.583"
            id="path2822"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908822}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908822,.908822"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2824"
            d="M102.695 76.87H213.28"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M102.695 101.211H213.28"
            id="path2826"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2828"
            d="M102.695 125.553H213.28"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M102.695 149.895H213.28"
            id="path2830"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2832"
            d="M102.695 174.236H213.28"
            fill="none"
            stroke="#757575"
            strokeWidth={0.908823}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".908823,.908823"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2834"
            d="M222.288 52.528h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927273}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927273,.927273"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 76.87h115.12"
            id="path2836"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927275}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927275,.927275"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2838"
            d="M222.288 101.211h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927274}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927274,.927274"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 125.553h115.12"
            id="path2840"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927275}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927275,.927275"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            id="path2842"
            d="M222.288 149.895h115.12"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927273}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927273,.927273"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <path
            d="M222.288 174.237h115.12"
            id="path2844"
            fill="none"
            stroke="#757575"
            strokeWidth={0.927274}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".927274,.927274"
            strokeDashoffset={0}
            strokeOpacity={0.996711}
          />
          <text
            transform="scale(.84731 1.1802)"
            id="text2854"
            y={49.675762}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2846"
              style={{}}
              y={49.675762}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'7'}
            </tspan>
            <tspan
              style={{}}
              y={56.405937}
              x={132.74138}
              id="tspan2848"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'9'}
            </tspan>
            <tspan
              style={{}}
              y={63.136112}
              x={132.74138}
              id="tspan2850"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'11'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={70.300766}
            id="text2864"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={70.300766}
              style={{}}
              id="tspan2856"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'13'}
            </tspan>
            <tspan
              id="tspan2858"
              x={132.74138}
              y={77.030945}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'15'}
            </tspan>
            <tspan
              id="tspan2860"
              x={132.74138}
              y={83.761116}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'17'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2874"
            y={90.925888}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2866"
              style={{}}
              y={90.925888}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'19'}
            </tspan>
            <tspan
              style={{}}
              y={97.656067}
              x={132.74138}
              id="tspan2868"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'21'}
            </tspan>
            <tspan
              style={{}}
              y={104.38624}
              x={132.74138}
              id="tspan2870"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'23'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={111.55093}
            id="text2884"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={111.55093}
              style={{}}
              id="tspan2876"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'25'}
            </tspan>
            <tspan
              id="tspan2878"
              x={132.74138}
              y={118.2811}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'27'}
            </tspan>
            <tspan
              id="tspan2880"
              x={132.74138}
              y={125.01128}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'29'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2894"
            y={132.17586}
            x={132.74138}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2886"
              style={{}}
              y={132.17586}
              x={132.74138}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'31'}
            </tspan>
            <tspan
              style={{}}
              y={138.90604}
              x={132.74138}
              id="tspan2888"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'33'}
            </tspan>
            <tspan
              style={{}}
              y={145.6362}
              x={132.74138}
              id="tspan2890"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'35'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={132.74138}
            y={152.80061}
            id="text2904"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={132.74138}
              y={152.80061}
              style={{}}
              id="tspan2896"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'37'}
            </tspan>
            <tspan
              id="tspan2898"
              x={132.74138}
              y={159.53079}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'39'}
            </tspan>
            <tspan
              id="tspan2900"
              x={132.74138}
              y={166.26096}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'41'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.79497}
            y={35.751202}
            id="text2910"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2906"
              x={119.79497}
              y={35.751202}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B1'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2916"
            y={56.376133}
            x={119.79497}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.376133}
              x={119.79497}
              id="tspan2912"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B3'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.79497}
            y={77.001198}
            id="text2922"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2918"
              x={119.79497}
              y={77.001198}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B5'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2928"
            y={97.626312}
            x={119.79497}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.626312}
              x={119.79497}
              id="tspan2924"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B7'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.79497}
            y={118.25136}
            id="text2934"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2930"
              x={119.79497}
              y={118.25136}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B9'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2940"
            y={138.87619}
            x={119.17044}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.87619}
              x={119.17044}
              id="tspan2936"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B11'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={119.17044}
            y={159.50095}
            id="text2946"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2942"
              x={119.17044}
              y={159.50095}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B13'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2956"
            y={29.050831}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2948"
              style={{}}
              y={29.050831}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'2'}
            </tspan>
            <tspan
              style={{}}
              y={35.781006}
              x={273.88287}
              id="tspan2950"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'4'}
            </tspan>
            <tspan
              style={{}}
              y={42.511181}
              x={273.88287}
              id="tspan2952"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'6'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={49.675762}
            id="text2966"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={49.675762}
              style={{}}
              id="tspan2958"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'8'}
            </tspan>
            <tspan
              id="tspan2960"
              x={273.88287}
              y={56.405937}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'10'}
            </tspan>
            <tspan
              id="tspan2962"
              x={273.88287}
              y={63.136112}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'12'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2976"
            y={70.300766}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2968"
              style={{}}
              y={70.300766}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'14'}
            </tspan>
            <tspan
              style={{}}
              y={77.030945}
              x={273.88287}
              id="tspan2970"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'16'}
            </tspan>
            <tspan
              style={{}}
              y={83.761116}
              x={273.88287}
              id="tspan2972"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'18'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={90.925888}
            id="text2986"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={90.925888}
              style={{}}
              id="tspan2978"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'20'}
            </tspan>
            <tspan
              id="tspan2980"
              x={273.88287}
              y={97.656067}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'22'}
            </tspan>
            <tspan
              id="tspan2982"
              x={273.88287}
              y={104.38624}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'24'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text2996"
            y={111.55093}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan2988"
              style={{}}
              y={111.55093}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'26'}
            </tspan>
            <tspan
              style={{}}
              y={118.2811}
              x={273.88287}
              id="tspan2990"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'28'}
            </tspan>
            <tspan
              style={{}}
              y={125.01128}
              x={273.88287}
              id="tspan2992"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'30'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={273.88287}
            y={132.17586}
            id="text3006"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={273.88287}
              y={132.17586}
              style={{}}
              id="tspan2998"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'32'}
            </tspan>
            <tspan
              id="tspan3000"
              x={273.88287}
              y={138.90604}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'34'}
            </tspan>
            <tspan
              id="tspan3002"
              x={273.88287}
              y={145.6362}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'36'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3016"
            y={152.80061}
            x={273.88287}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3008"
              style={{}}
              y={152.80061}
              x={273.88287}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'38'}
            </tspan>
            <tspan
              style={{}}
              y={159.53079}
              x={273.88287}
              id="tspan3010"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'40'}
            </tspan>
            <tspan
              style={{}}
              y={166.26096}
              x={273.88287}
              id="tspan3012"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'42'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3022"
            y={35.751202}
            x={260.93674}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.751202}
              x={260.93674}
              id="tspan3018"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B2'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.93674}
            y={56.376133}
            id="text3028"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3024"
              x={260.93674}
              y={56.376133}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B4'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3034"
            y={77.001198}
            x={260.93674}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.001198}
              x={260.93674}
              id="tspan3030"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B6'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.93674}
            y={97.626312}
            id="text3040"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3036"
              x={260.93674}
              y={97.626312}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B8'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3046"
            y={118.25136}
            x={260.31223}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.25136}
              x={260.31223}
              id="tspan3042"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B10'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={260.31226}
            y={138.87619}
            id="text3052"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3048"
              x={260.31226}
              y={138.87619}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B12'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text3058"
            y={159.50095}
            x={260.31226}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="gray"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.50095}
              x={260.31226}
              id="tspan3054"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="gray"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'B14'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={242.63895}
            y={10.208264}
            id="text3280"
            transform="scale(.83112 1.20319)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.28127px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.259528}
          >
            <tspan
              id="tspan3276"
              x={242.63895}
              y={10.208264}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.28127px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.259528}
            >
              <tspan id="tspan3274" fontSize="9.87777px" fill="#00abd6" fillOpacity={1} strokeWidth={0.259528}>
                {'PANEL 1'}
              </tspan>
            </tspan>
          </text>
          <path
            id="rect3705"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 53.460804H118.909421V75.400139H111.65446z"
          />
          <path
            id="rect3707"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 29.119104H118.909421V51.058439H111.65446z"
          />
          <path
            id="rect3709"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 77.802475H118.909421V99.74181H111.65446z"
          />
          <path
            id="rect3711"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 102.14417H118.909421V124.083505H111.65446z"
          />
          <path
            id="rect3713"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 126.48582H118.909421V148.42515500000002H111.65446z"
          />
          <path
            id="rect3715"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 150.82716H118.909421V172.766495H111.65446z"
          />
          <path
            id="rect3717"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M111.65446 175.16849H118.909421V197.107825H111.65446z"
          />
          <path
            id="rect3719"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 53.460804H238.499771V75.400139H231.24481z"
          />
          <path
            id="rect3721"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 29.119104H238.499771V51.058439H231.24481z"
          />
          <path
            id="rect3723"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 77.802475H238.499771V99.74181H231.24481z"
          />
          <path
            id="rect3725"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 102.14417H238.499771V124.083505H231.24481z"
          />
          <path
            id="rect3727"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 126.48582H238.499771V148.42515500000002H231.24481z"
          />
          <path
            id="rect3729"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 150.82716H238.499771V172.766495H231.24481z"
          />
          <path
            id="rect3731"
            fill="none"
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={0.285663}
            strokeOpacity={1}
            d="M231.24481 175.16849H238.499771V197.107825H231.24481z"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={149.98093}
            y={21.818066}
            id="text84174"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84172"
              x={149.98093}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'A'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84178"
            y={21.818066}
            x={171.21483}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={171.21483}
              id="tspan84176"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'V'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={191.19962}
            y={21.818066}
            id="text84182"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84180"
              x={191.19962}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KVA'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84186"
            y={21.818066}
            x={216.80522}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={216.80522}
              id="tspan84184"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KW'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={238.03899}
            y={21.818066}
            id="text84190"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84188"
              x={238.03899}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KWH'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v1"
            y={29.0229}
            x={169.46625}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={29.0229}
              x={169.46625}
              id="tspan84192"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[1]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={35.885036}
            id="pn1_v3"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84198"
              x={169.46625}
              y={35.885036}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[3]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={42.74715}
            id="pn1_v5"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84204"
              x={169.46625}
              y={42.74715}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[5]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={49.609272}
            id="pn1_v7"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84210"
              x={169.46625}
              y={49.609272}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[7]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={56.471401}
            id="pn1_v9"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84216"
              x={169.46625}
              y={56.471401}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[9]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={63.333553}
            id="pn1_v11"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84222"
              x={169.46625}
              y={63.333553}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[11]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={70.195702}
            id="pn1_v13"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84228"
              x={169.46625}
              y={70.195702}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[13]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={77.05793}
            id="pn1_v15"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84234"
              x={169.46625}
              y={77.05793}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[15]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={83.92012}
            id="pn1_v17"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84240"
              x={169.46625}
              y={83.92012}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[17]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={90.782333}
            id="pn1_v19"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84246"
              x={169.46625}
              y={90.782333}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[19]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={97.644516}
            id="pn1_v21"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84252"
              x={169.46625}
              y={97.644516}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[21]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={104.50671}
            id="pn1_v23"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84258"
              x={169.46625}
              y={104.50671}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[23]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={111.3689}
            id="pn1_v25"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84264"
              x={169.46625}
              y={111.3689}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[25]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={118.23106}
            id="pn1_v27"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84270"
              x={169.46625}
              y={118.23106}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[27]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={125.0932}
            id="pn1_v29"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84276"
              x={169.46625}
              y={125.0932}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[29]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={131.95528}
            id="pn1_v31"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84282"
              x={169.46625}
              y={131.95528}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[31]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={138.81732}
            id="pn1_v33"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84288"
              x={169.46625}
              y={138.81732}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[33]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={145.67931}
            id="pn1_v35"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84294"
              x={169.46625}
              y={145.67931}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[35]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={152.54131}
            id="pn1_v37"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84300"
              x={169.46625}
              y={152.54131}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[37]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={159.40338}
            id="pn1_v39"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84306"
              x={169.46625}
              y={159.40338}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[39]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={169.46625}
            y={166.26544}
            id="pn1_v41"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84312"
              x={169.46625}
              y={166.26544}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[41]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={309.98337}
            y={29.0229}
            id="pn1_v2"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84521"
              x={309.98337}
              y={29.0229}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[2]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v4"
            y={35.885036}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={35.885036}
              x={309.98337}
              id="tspan84527"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[4]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v6"
            y={42.74715}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={42.74715}
              x={309.98337}
              id="tspan84533"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[6]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v8"
            y={49.609272}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={49.609272}
              x={309.98337}
              id="tspan84539"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[8]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v10"
            y={56.471401}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={56.471401}
              x={309.98337}
              id="tspan84545"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[10]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v12"
            y={63.333553}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={63.333553}
              x={309.98337}
              id="tspan84551"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[12]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v14"
            y={70.195702}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={70.195702}
              x={309.98337}
              id="tspan84557"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[14]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v16"
            y={77.05793}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={77.05793}
              x={309.98337}
              id="tspan84563"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[16]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v18"
            y={83.92012}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={83.92012}
              x={309.98337}
              id="tspan84569"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[18]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v20"
            y={90.782333}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={90.782333}
              x={309.98337}
              id="tspan84575"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[20]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v22"
            y={97.644516}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={97.644516}
              x={309.98337}
              id="tspan84581"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[22]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v24"
            y={104.50671}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={104.50671}
              x={309.98337}
              id="tspan84587"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[24]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v26"
            y={111.3689}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.3689}
              x={309.98337}
              id="tspan84593"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[26]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v28"
            y={118.23106}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={118.23106}
              x={309.98337}
              id="tspan84599"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[28]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v30"
            y={125.0932}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={125.0932}
              x={309.98337}
              id="tspan84605"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[30]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v32"
            y={131.95528}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={131.95528}
              x={309.98337}
              id="tspan84611"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[32]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v34"
            y={138.81732}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={138.81732}
              x={309.98337}
              id="tspan84617"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[34]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v36"
            y={145.67931}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={145.67931}
              x={309.98337}
              id="tspan84623"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[36]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v38"
            y={152.54131}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={152.54131}
              x={309.98337}
              id="tspan84629"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[38]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v40"
            y={159.40338}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={159.40338}
              x={309.98337}
              id="tspan84635"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[40]}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="pn1_v42"
            y={166.26544}
            x={309.98337}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.38414px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00abd6"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={166.26544}
              x={309.98337}
              id="tspan84641"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.38414px"
              fontFamily="Franklin Gothic Medium"
              fill="#00abd6"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {v[42]}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={271.13821}
            y={21.818066}
            id="text84649"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84647"
              x={271.13821}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'CH'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84653"
            y={21.818066}
            x={291.74728}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={291.74728}
              id="tspan84651"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'A'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={312.98093}
            y={21.818066}
            id="text84657"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84655"
              x={312.98093}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'V'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84661"
            y={21.818066}
            x={332.96545}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={332.96545}
              id="tspan84659"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KVA'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            x={357.94592}
            y={21.818066}
            id="text84665"
            transform="scale(.84731 1.1802)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan84663"
              x={357.94592}
              y={21.818066}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KW'}
            </tspan>
          </text>
          <text
            transform="scale(.84731 1.1802)"
            id="text84669"
            y={21.818066}
            x={379.17932}
            style={{
              lineHeight: 1.25,

              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
              }}
              y={21.818066}
              x={379.17932}
              id="tspan84667"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'KWH'}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
