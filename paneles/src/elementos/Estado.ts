import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    generalOn: css`
      fill: #1aea78;
    `,
    generalOff: css`
      fill: gray;
    `,
  };
});

const estado = getStyles();

export default estado;
