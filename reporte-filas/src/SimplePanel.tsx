import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx, injectGlobal } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  console.log(data);
  // LISTA
  let ListaUma = [
    { id: 0, descripcion: 'Equipo Apagado', clase: 'apagado' },
    { id: 1, descripcion: 'Equipo encendido', clase: 'operativo' },
    { id: 2, descripcion: 'Umbral de temperatura de retorno', clase: 'almpreventiva' },
    { id: 3, descripcion: 'Umbral de temperatura de suministro', clase: 'almpreventiva' },
    { id: 4, descripcion: 'Alarma de filtro de aire', clase: 'alarma' },
    { id: 5, descripcion: 'Alarma de inundación', clase: 'alarma' },
    { id: 6, descripcion: 'Alarma de perdida de aire', clase: 'alarma' },
    { id: 7, descripcion: 'Alarma de Sobrecalentamiento', clase: 'alarma' },
    { id: 8, descripcion: 'Alarma de detección de humo', clase: 'alarma' },
    { id: 9, descripcion: 'Alarma de humificador', clase: 'alarma' },
    { id: 10, descripcion: 'Alarma general', clase: 'alarma' },
    { id: 11, descripcion: 'Equipo Apagado por bajo voltaje EEQ', clase: 'apagado' },
    { id: 12, descripcion: 'Equipo Inhabilitado', clase: 'deshabilitado' },
    { id: 13, descripcion: 'Equipo en Mantenimiento', clase: 'mantenimiento' },
  ];

  let tabla_apa = [];
  let tabla_ope = [];
  let tabla_almp = [];
  let tabla_alm = [];
  let tabla_man = [];
  let tabla_des = [];
  let tiempo = new Date();
  let fecha =
    tiempo.getUTCDate() +
    '/' +
    (tiempo.getUTCMonth() + 1) +
    '/' +
    tiempo.getFullYear() +
    ' ' +
    tiempo.getHours() +
    ':' +
    tiempo.getMinutes();

  function datostablaups(fecha: any, sistema: any, tipo: any, equipo: any, dato: any, detalle: any) {
    let tabla = (
      <tr>
        <td>{fecha}</td>
        <td>B</td>
        <td>{sistema}</td>
        <td>{tipo}</td>
        <td>{equipo}</td>
        <td id={dato}> {dato.toUpperCase()} </td>
        <td> {detalle} </td>
      </tr>
    );
    return tabla;
  }

  let eventoEquipo: any;
  let eventoEquipo2: any;
  let evento = 0;
  let fechaDse = new Date();
  let fechaTrans = new Date();
  let detalle;
  let dato;

  let minutos = 0;
  let min = 0;
  let st_dse: any = data.series.find(({ name }) => name === 'EVENTO_DSE')?.fields[1].state?.calcs?.lastNotNull;

  for (let i = 1; i <= 10; i++) {
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_UMA' + i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_UMA' + i)?.fields[1].state?.calcs?.lastNotNull;

    if (eventoEquipo === 0 || eventoEquipo === null || eventoEquipo === undefined) {
      dato = 'apagado';
      evento = 0;
      fechaTrans = new Date();
    } else {
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring(eventoEquipo.length - 3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, eventoEquipo.length - 3)));
      console.log('transformados', evento, fechaTrans);
    }
    if (eventoEquipo === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 11;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 13;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    }
    console.log('fecha dse222', st_dse, evento, fechaDse);
    console.log(st_dse, fechaDse.getTime(), fechaTrans.getTime(), min, minutos, '¡¡¡MINUTOS....!!!');
    console.log('Datos eventos', eventoEquipo, evento, fechaTrans);
    fecha =
      fechaTrans.getUTCDate() +
      '/' +
      (fechaTrans.getUTCMonth() + 1) +
      '/' +
      fechaTrans.getFullYear() +
      ' ' +
      fechaTrans.getHours() +
      ':' +
      fechaTrans.getMinutes();
    detalle = ListaUma.find(o => o.id === parseInt('' + evento));
    console.log('detalles', detalle?.descripcion, detalle?.clase);
    if (detalle?.clase === 'apagado') {
      tabla_apa.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'operativo') {
      tabla_ope.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'almpreventiva') {
      tabla_almp.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'alarma') {
      tabla_alm.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'deshabilitado') {
      tabla_des.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'mantenimiento') {
      tabla_man.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${'100%'}px;
          height: ${'100%'}px;
        `
      )}
    >
      <div id="table-scroll">
        <table className="table">
          <thead>
            <tr>
              <th>RACK</th>
              <th>SISTEMA A</th>
              <th>SISTEMA B</th>
              <th>TOTAL</th>
            </tr>
          </thead>
          <tbody>
            {tabla_alm}
            {tabla_almp}
            {tabla_man}
            {tabla_des}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
