import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  console.log(data);

  let listaGen=[
    {"id":0,descripcion:"Equipo Apagado",clase:"apagado"},
    {"id":1,descripcion:"Equipo encendido",clase:"operativo"},
    {"id":2,descripcion:"Umbral nivel de combustible",clase:"almpreventiva"},
    {"id":3,descripcion:"Parado de emergencia Electrical-Trip",clase:"alarma"},
    {"id":4,descripcion:"Parado de emergencia Shutdown",clase:"alarma"},
    {"id":5,descripcion:"Parado de emergencia Warning",clase:"alarma"},
    {"id":6,descripcion:"Falla de arranque Electrical-Trip",clase:"alarma"},
    {"id":7,descripcion:"Falla de arranque Shutdown",clase:"alarma"},
    {"id":8,descripcion:"Falla de arranque Warning",clase:"alarma"},
    {"id":9,descripcion:"Alta tension del generador Electrical-Trip",clase:"alarma"},
    {"id":10,descripcion:"Alta tension del generador Shutdown",clase:"alarma"},
    {"id":11,descripcion:"Alta tension del generador Warning",clase:"alarma"},
    {"id":12,descripcion:"Baja tension del generador Electrical-Trip",clase:"alarma"},
    {"id":13,descripcion:"Baja tension del generador Shutdown",clase:"alarma"},
    {"id":14,descripcion:"Baja tension del generador Warning",clase:"alarma"},
    {"id":15,descripcion:"Alta temperatura del refrigerante Electrical-Trip",clase:"alarma"},
    {"id":16,descripcion:"Alta temperatura del refrigerante Shutdown",clase:"alarma"},
    {"id":17,descripcion:"Alta temperatura del refrigerante Warning",clase:"alarma"},
    {"id":18,descripcion:"Alta temperatura de aceite Electrical-Trip",clase:"alarma"},
    {"id":19,descripcion:"Alta temperatura de aceite Shutdown",clase:"alarma"},
    {"id":20,descripcion:"Alta temperatura de aceite Warning",clase:"alarma"},
    {"id":21,descripcion:"Baja presion de aceite Electrical-Trip",clase:"alarma"},
    {"id":22,descripcion:"Baja presion de aceite Shutdown",clase:"alarma"},
    {"id":23,descripcion:"Baja presion de aceite Warning",clase:"alarma"},
    {"id":24,descripcion:"Sobrevelocidad Electrical-Trip",clase:"alarma"},
    {"id":25,descripcion:"Sobrevelocidad Shutdown",clase:"alarma"},
    {"id":26,descripcion:"Sobrevelocidad Warning",clase:"alarma"},
    {"id":27,descripcion:"Baja velocidad Electrical-Trip",clase:"alarma"},
    {"id":28,descripcion:"Baja velocidad Shutdown",clase:"alarma"},
    {"id":29,descripcion:"Baja velocidad Warning",clase:"alarma"},
    {"id":30,descripcion:"Equipo Apagado por bajo voltaje EEQ",clase:"apagado"},
    {"id":31,descripcion:"Equipo Inhabilitado",clase:"deshabilitado"},
    {"id":32,descripcion:"Equipo en Mantenimiento",clase:"mantenimiento"},
    ];

  let listaUps=[
    {"id":0,descripcion:"Equipo Apagado",clase:"apagado"},
    {"id":1,descripcion:"Equipo encendido",clase:"operativo"},
    {"id":2,descripcion:"Carga estimada restante baja",clase:"almpreventiva"},
    {"id":3,descripcion:"Umbral de voltaje en bateria",clase:"almpreventiva"},
    {"id":4,descripcion:"Umbral voltaje de entrada L1",clase:"almpreventiva"},
    {"id":5,descripcion:"Umbral voltaje de entrada L2",clase:"almpreventiva"},
    {"id":6,descripcion:"Umbral voltaje de entrada L3",clase:"almpreventiva"},
    {"id":7,descripcion:"Umbral voltaje de salida L1",clase:"almpreventiva"},
    {"id":8,descripcion:"Umbral voltaje de salida L2",clase:"almpreventiva"},
    {"id":9,descripcion:"Umbral voltaje de salida L3",clase:"almpreventiva"},
    {"id":10,descripcion:"Umbral corriente de salida L1",clase:"almpreventiva"},
    {"id":11,descripcion:"Umbral corriente de salida L2",clase:"almpreventiva"},
    {"id":12,descripcion:"Umbral corriente de salida L3",clase:"almpreventiva"},
    {"id":13,descripcion:"Umbral potencia de salida L1",clase:"almpreventiva"},
    {"id":14,descripcion:"Umbral potencia de salida L2",clase:"almpreventiva"},
    {"id":15,descripcion:"Umbral potencia de salida L3",clase:"almpreventiva"},
    {"id":16,descripcion:"Umbral salida porcentaje de carga L1",clase:"almpreventiva"},
    {"id":17,descripcion:"Umbral salida porcentaje de carga L2",clase:"almpreventiva"},
    {"id":18,descripcion:"Umbral salida porcentaje de carga L3",clase:"almpreventiva"},
    {"id":20,descripcion:"Equipo Apagado por bajo voltaje EEQ",clase:"apagado"},
    {"id":21,descripcion:"Equipo Inhabilitado",clase:"deshabilitado"},
    {"id":22,descripcion:"Equipo en Mantenimiento",clase:"mantenimiento"},
    ];

  let listaPdu=[
      {"id":0,descripcion:"Equipo Apagado",clase:"apagado"},
      {"id":1,descripcion:"Equipo encendido",clase:"operativo"},
      {"id":2,descripcion:"Error de Comunicacion",clase:"alarma"},
      {"id":3,descripcion:"Puerta Abierta",clase:"alarma"},
      {"id":4,descripcion:"Alarma General",clase:"alarma"},
      {"id":5,descripcion:"Voltaje alto de Entrada 1",clase:"alarma"},
      {"id":6,descripcion:"Voltaje bajo de Entrada 1",clase:"alarma"},
      {"id":7,descripcion:"Voltaje alto de Salida 1",clase:"alarma"},
      {"id":8,descripcion:"Voltaje bajo de Salida 1",clase:"alarma"},
      {"id":9,descripcion:"Alarma Trip de Interruptor Principal",clase:"alarma"},
      {"id":10,descripcion:"Estado Modbus",clase:"alarma"},
      {"id":11,descripcion:"Umbral nivel Voltaje de Entrada",clase:"almpreventiva"},
      {"id":12,descripcion:"Umbral nivel Voltajde de Salida",clase:"almpreventiva"},
      {"id":20,descripcion:"Equipo Apagado por bajo voltaje EEQ",clase:"apagado"},
      {"id":21,descripcion:"Equipo Inhabilitado",clase:"deshabilitado"},
      {"id":22,descripcion:"Equipo en Mantenimiento",clase:"mantenimiento"},
    ];

  let listaAts=[
      {"id":0,descripcion:"Equipo Apagado",clase:"apagado"},
      {"id":1,descripcion:"Equipo encendido",clase:"operativo"},
      {"id":2,descripcion:"Perdida de Comunicacion",clase:"alarma"},
      {"id":3,descripcion:"Equipo Apagado por bajo voltaje EEQ",clase:"apagado"},
      {"id":4,descripcion:"Equipo Inhabilitado",clase:"deshabilitado"},
      {"id":5,descripcion:"Equipo en Mantenimiento",clase:"mantenimiento"},
    ];

  let listaTableros=[
      {"id":0,descripcion:"Equipo Apagado",clase:"apagado"},
      {"id":1,descripcion:"Equipo encendido",clase:"operativo"},
      {"id":2,descripcion:"Alarma General",clase:"alarma"},
      {"id":3,descripcion:"Perdida de Comunicacion",clase:"alarma"},
      {"id":4,descripcion:"Estado Trip",clase:"alarma"},
      {"id":5,descripcion:"Umbral nivel de Voltaje",clase:"almpreventiva"},
      {"id":6,descripcion:"Umbral nivel de Corriente",clase:"almpreventiva"},
      {"id":10,descripcion:"Equipo Apagado por bajo voltaje EEQ",clase:"apagado"},
      {"id":11,descripcion:"Equipo Inhabilitado",clase:"deshabilitado"},
      {"id":12,descripcion:"Equipo en Mantenimiento",clase:"mantenimiento"},
    ];


  let tiempo = new Date();
  let fecha = tiempo.getUTCDate() +'/' +(tiempo.getUTCMonth() + 1) +'/' +tiempo.getFullYear() +' ' +tiempo.getHours() +':' +tiempo.getMinutes();
  
  let tabla_apa = [];
  let tabla_ope = [];
  let tabla_almp = [];
  let tabla_alm = [];
  let tabla_man = [];
  let tabla_des = [];
  
  /*0 apagado
  1 encendido
  2 alm general
  3 modbus
  4 trip
  5 umbral voltaje
  6 umbral corriente

  */
  let eventoEquipo:any
  let eventoEquipo2:any
  let evento=0
  let fechaTrans=new Date()
  let fechaDse=new Date();
  let detalle;
  let minutos = 0;
  let min = 0;
  let st_dse: any = data.series.find(({ name }) => name === 'EVENTO_DSE')?.fields[1].state?.calcs?.lastNotNull;
  let eventoDse = 1;
  if (st_dse > 0) {
    st_dse = st_dse.toString();
    fechaDse = new Date(parseFloat(st_dse.substring(0, st_dse.length - 3)));
    eventoDse = parseInt(st_dse.substring(st_dse.length - 3,st_dse.length))
  }
  let dato="apagado"
  let datoinformado = [];
  let generador=[4,5,6,10,11,12]
  for (let i = 0; i < generador.length; i++) {
    let sistema=1;
    if(i>3)sistema=2    
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_GEN'+generador[i])?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_GEN'+generador[i])?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 30;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 31;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 32;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    console.log('Datos eventos', eventoEquipo, evento, fechaTrans);
    fecha =
      fechaTrans.getUTCDate() +
      '/' +
      (fechaTrans.getUTCMonth() + 1) +
      '/' +
      fechaTrans.getFullYear() +
      ' ' +
      fechaTrans.getHours() +
      ':' +
      fechaTrans.getMinutes();
    console.log("Datos eventos----",eventoEquipo,evento,fechaTrans)
    //fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaGen.find(o => o.id === parseInt(""+evento));
    console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN-' + generador[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN-' + generador[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN-' + generador[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN-' + generador[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN' + generador[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'GEN-' + generador[i], detalle?.clase, detalle?.descripcion));
    
  }
  // UPS
  for (let i = 1; i <=6; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_UPS1B'+i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_UPS1B'+i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 20;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 21;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 22;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    console.log("Datos eventos: UPS-SIS:1",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaUps.find(o => o.id === parseInt(""+evento));
    console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-1-'+i+'B', detalle?.clase, detalle?.descripcion)); 
  }
  for (let i = 1; i <=6; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_UPS2B'+i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_UPS2B'+i)?.fields[1].state?.calcs?.lastNotNull;
    
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      console.log("transformados",evento,fechaTrans)
    }
    
    if (eventoEquipo === 0 && eventoDse === 0) {
        min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
        minutos = min / (1000 * 60 * 60);
        if (minutos < 4) {
          dato = 'apagado';
          evento = 20;
        }
      }
      if (eventoEquipo2 === 1) {
        dato = 'inhabilitado';
        evento = 21;
        fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
      } else if (eventoEquipo2 === 2) {
        dato = 'mantenimiento';
        evento = 22;
        fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    }
    if(i===3){
      console.log("UPS-2-4B",eventoEquipo,evento,fechaTrans)
    }
    //console.log("Datos eventos:UPS-SIS-2",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaUps.find(o => o.id === parseInt(""+evento));
    console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'UPS-2-'+i+'B', detalle?.clase, detalle?.descripcion));
    
  }
  // ATS

  let ats = [3, 7, 9, 4, 8, 10, 11];
  let ats1 = ['03B', 'CHI-07B', 'PDU-09B', '04B', 'CHI-08B', 'PDU-10B', 'SG-11B'];

  for (let i = 0; i < ats.length; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_ATS'+ ats[i])?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_ATS'+ ats[i])?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 3;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 4;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 5;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaAts.find(o => o.id === parseInt(""+evento));
    //console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+ats1[i], detalle?.clase, detalle?.descripcion));
  }

  //PDU

  for (let i = 2; i < 9; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_PDU1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_PDU1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 20;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 21;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 22;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaPdu.find(o => o.id === parseInt(""+evento));
    //console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-01B-F'+i, detalle?.clase, detalle?.descripcion));
    
  }

  for (let i = 1; i < 9; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_PDU2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_PDU1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 20;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 21;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 22;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaPdu.find(o => o.id === parseInt(""+evento));
    //console.log("detalles",detalle?.descripcion,detalle?.clase)
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'PDU-02B-F'+i, detalle?.clase, detalle?.descripcion));
    
  }
  //TDB
  for (let i = 0; i < 3; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TDB'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TDB'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDB-0'+i, detalle?.clase, detalle?.descripcion));
    
  }
  //TATS
  let tats = ["3_0", "3_1", "3_2", "4_0", "4_1", "4_2"];
  let tats1 = ['03B-EEQ', '03B-A', '03B-B', '04B-EEQ', '04B-A', '04B-B'];
  for (let i = 0; i < tats.length; i++) {
    let sistema=1;
    if(i>2)sistema=2   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TATS'+ tats[i])?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TATS'+ tats[i])?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'ATS-'+tats1[i], detalle?.clase, detalle?.descripcion));
  }
  
  //TDP
  for (let i = 0; i <= 8; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TDP1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TDP1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-01B-'+i, detalle?.clase, detalle?.descripcion));
  }
  for (let i = 0; i <= 9; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TDP2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'TDP-02B-'+i, detalle?.clase, detalle?.descripcion));
  }

  //TUPSIN
  for (let i = 0; i <= 6; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TUPSIN1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TUPSIN1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-01B-'+i, detalle?.clase, detalle?.descripcion));
  }
  for (let i = 0; i <= 6; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TUPSIN2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TUPSIN2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-IN-02B-'+i, detalle?.clase, detalle?.descripcion));
  }

  //TUPSOUT
  for (let i = 1; i <= 8; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TUPSOUT1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TUPSOUT1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-01B-'+i, detalle?.clase, detalle?.descripcion));
  }
  for (let i = 1; i <= 8; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TUPSOUT2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TUPSOUT2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-UPS-OUT-02B-'+i, detalle?.clase, detalle?.descripcion));
  }
  
  
  //TPDU
  for (let i = 0; i < 12; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TPDU1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TPDU1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-01B-'+i, detalle?.clase, detalle?.descripcion));
  }
  for (let i = 0; i < 12; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TPDU2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-PDU-02B-'+i, detalle?.clase, detalle?.descripcion));
  }

  //TCHI
  for (let i = 0; i <= 22; i++) {
    let sistema=1;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TCHI1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TCHI1B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-01B-'+i, detalle?.clase, detalle?.descripcion));
  }
  for (let i = 0; i <= 22; i++) {
    let sistema=2;   
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_TCHI2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_TCHI2B'+ i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null|| eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato="apagado"
      evento=0
      fechaTrans = new Date()
    } else{
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring((eventoEquipo.length)-3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, (eventoEquipo.length)-3)));
      //console.log("transformados",evento,fechaTrans)
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 10;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 11;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } 
    //console.log("Datos eventos",eventoEquipo,evento,fechaTrans)
    fecha =fechaTrans.getUTCDate() +'/' +(fechaTrans.getUTCMonth() + 1) +'/' +fechaTrans.getFullYear() +' ' +fechaTrans.getHours() +':' +fechaTrans.getMinutes();
    detalle=listaTableros.find(o => o.id === parseInt(""+evento));
    if(detalle?.clase==="apagado")tabla_apa.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="operativo")tabla_ope.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="almpreventiva")tabla_almp.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="alarma")tabla_alm.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="deshabilitado")tabla_des.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
    if(detalle?.clase==="mantenimiento")tabla_man.push(datosTabla(fecha, sistema, 'ELÉCTRICO', 'T-CHI-02B-'+i, detalle?.clase, detalle?.descripcion));
  }
  // FUNCION
  function datosTabla(fecha: any, sistema: any, tipo: any, equipo: any, dato: any, detalle: any) {
    let tabla = (
      <tr>
        <td>{fecha}</td>
        <td>B</td>
        <td>{sistema}</td>
        <td>{tipo}</td> 
        <td>{equipo}</td>
        <td id={dato}> {dato.toUpperCase()} </td>
        <td> {detalle} </td>
      </tr>
    );
    return tabla;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${'100%'}px;
          height: ${'100%'}px;
        `
      )}
    >
      <div id="table-scroll">
        <table className="table">
          <thead>
            <tr>
              <th>FECHA </th>
              <th>FASE</th>
              <th>SISTEMA</th>
              <th>TIPO DE SISTEMA</th>
              <th>EQUIPO </th>
              <th>ESTADO </th>
              <th>DETALLE </th>
            </tr>
          </thead>
          <tbody>
            {tabla_alm}
            {tabla_almp}
            {tabla_man}
            {tabla_des}
            {tabla_ope}
            {tabla_apa}
            {datoinformado}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
