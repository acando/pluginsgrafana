import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx, injectGlobal } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  console.log(data);
  // LISTA
  let ListaUma = [
    { id: 0, descripcion: 'Equipo Apagado', clase: 'apagado' },
    { id: 1, descripcion: 'Equipo encendido', clase: 'operativo' },
    { id: 2, descripcion: 'Umbral de temperatura de retorno', clase: 'almpreventiva' },
    { id: 3, descripcion: 'Umbral de temperatura de suministro', clase: 'almpreventiva' },
    { id: 4, descripcion: 'Alarma de filtro de aire', clase: 'alarma' },
    { id: 5, descripcion: 'Alarma de inundación', clase: 'alarma' },
    { id: 6, descripcion: 'Alarma de perdida de aire', clase: 'alarma' },
    { id: 7, descripcion: 'Alarma de Sobrecalentamiento', clase: 'alarma' },
    { id: 8, descripcion: 'Alarma de detección de humo', clase: 'alarma' },
    { id: 9, descripcion: 'Alarma de humificador', clase: 'alarma' },
    { id: 10, descripcion: 'Alarma general', clase: 'alarma' },
    { id: 11, descripcion: 'Equipo Apagado por bajo voltaje EEQ', clase: 'apagado' },
    { id: 12, descripcion: 'Equipo Inhabilitado', clase: 'deshabilitado' },
    { id: 13, descripcion: 'Equipo en Mantenimiento', clase: 'mantenimiento' },
  ];

  let ListaChill = [
    { id: 0, descripcion: 'Equipo Apagado', clase: 'apagado' },
    { id: 1, descripcion: 'Equipo encendido', clase: 'operativo' },
    { id: 2, descripcion: 'Umbral de temperatura de retorno', clase: 'almpreventiva' },
    { id: 3, descripcion: 'Umbral de temperatura de suministro', clase: 'almpreventiva' },
    { id: 101, descripcion: 'Baja temperatura ambiental Compresor 1', clase: 'alarma' },
    { id: 102, descripcion: 'Alta temperatura ambiental Compresor 1', clase: 'alarma' },
    { id: 103, descripcion: 'Baja temperatura del líquido refrigerante Compresor 1', clase: 'alarma' },
    { id: 105, descripcion: 'Bajo voltaje de batería RTC Compresor 1', clase: 'alarma' },
    { id: 106, descripcion: 'Número inválido de recursos de compensación Compresor 1', clase: 'alarma' },
    { id: 107, descripcion: 'Fallo de comunicaciones VSD Compresor 1', clase: 'alarma' },
    { id: 108, descripcion: 'Pre-carga bajo voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 109, descripcion: 'Pre-carga desequilibrio voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 110, descripcion: 'Alto voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 111, descripcion: 'Bajo voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 112, descripcion: 'Desequilibrio de voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 113, descripcion: 'Alta temperatura ambiental VSD Compresor 1', clase: 'alarma' },
    { id: 114, descripcion: 'Entrada monofásica Compresor 1', clase: 'alarma' },
    { id: 115, descripcion: 'Fallo fuente de alimentación VSD Compresor 1', clase: 'alarma' },
    { id: 116, descripcion: 'Falla placa lógica VSD Compresor 1', clase: 'alarma' },
    { id: 117, descripcion: 'Sobrecarga corriente del motor (Hardware) Compresor 1', clase: 'alarma' },
    { id: 118, descripcion: 'Fallo del enchufe CT Compresor 1', clase: 'alarma' },
    { id: 127, descripcion: 'Alta presión de descarga (Software) Compresor 1', clase: 'alarma' },
    { id: 128, descripcion: 'Alta presión diferencial de aceite Compresor 1', clase: 'alarma' },
    { id: 129, descripcion: 'Baja presión diferencial de aceite Compresor 1', clase: 'alarma' },
    { id: 130, descripcion: 'Baja presión de succión Compresor 1', clase: 'alarma' },
    { id: 131, descripcion: 'Alta temperatura de descarga Compresor 1', clase: 'alarma' },
    { id: 132, descripcion: 'Alta temperatura de aceite Compresor 1', clase: 'alarma' },
    { id: 133, descripcion: 'Sobrecalentamiento de baja succión Compresor 1', clase: 'alarma' },
    { id: 134, descripcion: 'Fallo de sensor Compresor 1', clase: 'alarma' },
    { id: 135, descripcion: 'Corriente baja del motor Compresor 1', clase: 'alarma' },
    { id: 136, descripcion: 'Alta temperatura del motor Compresor 1', clase: 'alarma' },
    { id: 137, descripcion: 'Pre-carga bajo voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 138, descripcion: 'Pre-carga desequilibrio de voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 139, descripcion: 'Alto voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 140, descripcion: 'Bajo voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 141, descripcion: 'Desequilibrio de voltaje bus DC Compresor 1', clase: 'alarma' },
    { id: 142, descripcion: 'Corriente alta del motor Compresor 1', clase: 'alarma' },
    { id: 143, descripcion: 'Sobrecarga corriente del motor (Software) Compresor 1', clase: 'alarma' },
    { id: 144, descripcion: 'Fallo controlador puerta del IGBT Compresor 1', clase: 'alarma' },
    { id: 145, descripcion: 'Alta temperatura de placa base Compresor 1', clase: 'alarma' },
    { id: 146, descripcion: 'Alta temperatura de placa base Compresor 1', clase: 'alarma' },
    { id: 147, descripcion: 'Fallo señal de funcionamiento VSD Compresor 1', clase: 'alarma' },
    { id: 148, descripcion: 'Alta presión de descarga (Software) Compresor 1', clase: 'alarma' },
    { id: 149, descripcion: 'Descarga de presión alta (Hardware-HPCQ) Compresor 1', clase: 'alarma' },
    { id: 150, descripcion: 'Fallo en control de voltaje Compresor 1', clase: 'alarma' },
    { id: 151, descripcion: 'Baja descarga de sobrecalentamiento Compresor 1', clase: 'alarma' },
    { id: 201, descripcion: 'Baja temperatura ambiental Compresor 2', clase: 'alarma' },
    { id: 202, descripcion: 'Alta temperatura ambiental Compresor 2', clase: 'alarma' },
    { id: 203, descripcion: 'Baja temperatura del líquido refrigerante Compresor 2', clase: 'alarma' },
    { id: 205, descripcion: 'Bajo voltaje de batería RTC Compresor 2', clase: 'alarma' },
    { id: 206, descripcion: 'Número inválido de recursos de compensación Compresor 2', clase: 'alarma' },
    { id: 207, descripcion: 'Fallo de comunicaciones VSD Compresor 2', clase: 'alarma' },
    { id: 208, descripcion: 'Pre-carga bajo voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 209, descripcion: 'Pre-carga desequilibrio voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 210, descripcion: 'Alto voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 211, descripcion: 'Bajo voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 212, descripcion: 'Desequilibrio de voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 213, descripcion: 'Alta temperatura ambiental VSD Compresor 2', clase: 'alarma' },
    { id: 214, descripcion: 'Entrada monofásica Compresor 2', clase: 'alarma' },
    { id: 215, descripcion: 'Fallo fuente de alimentación VSD Compresor 2', clase: 'alarma' },
    { id: 216, descripcion: 'Falla placa lógica VSD Compresor 2', clase: 'alarma' },
    { id: 217, descripcion: 'Sobrecarga corriente del motor (Hardware) Compresor 2', clase: 'alarma' },
    { id: 218, descripcion: 'Fallo del enchufe CT Compresor 2', clase: 'alarma' },
    { id: 227, descripcion: 'Alta presión de descarga (Software) Compresor 2', clase: 'alarma' },
    { id: 228, descripcion: 'Alta presión diferencial de aceite Compresor 2', clase: 'alarma' },
    { id: 229, descripcion: 'Baja presión diferencial de aceite Compresor 2', clase: 'alarma' },
    { id: 230, descripcion: 'Baja presión de succión Compresor 2', clase: 'alarma' },
    { id: 231, descripcion: 'Alta temperatura de descarga Compresor 2', clase: 'alarma' },
    { id: 232, descripcion: 'Alta temperatura de aceite Compresor 2', clase: 'alarma' },
    { id: 233, descripcion: 'Sobrecalentamiento de baja succión Compresor 2', clase: 'alarma' },
    { id: 234, descripcion: 'Fallo de sensor Compresor 2', clase: 'alarma' },
    { id: 235, descripcion: 'Corriente baja del motor Compresor 2', clase: 'alarma' },
    { id: 236, descripcion: 'Alta temperatura del motor Compresor 2', clase: 'alarma' },
    { id: 237, descripcion: 'Pre-carga bajo voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 238, descripcion: 'Pre-carga desequilibrio de voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 239, descripcion: 'Alto voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 240, descripcion: 'Bajo voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 241, descripcion: 'Desequilibrio de voltaje bus DC Compresor 2', clase: 'alarma' },
    { id: 242, descripcion: 'Corriente alta del motor Compresor 2', clase: 'alarma' },
    { id: 243, descripcion: 'Sobrecarga corriente del motor (Software) Compresor 2', clase: 'alarma' },
    { id: 244, descripcion: 'Fallo controlador puerta del IGBT Compresor 2', clase: 'alarma' },
    { id: 245, descripcion: 'Alta temperatura de placa base Compresor 2', clase: 'alarma' },
    { id: 246, descripcion: 'Alta temperatura de placa base Compresor 2', clase: 'alarma' },
    { id: 247, descripcion: 'Fallo señal de funcionamiento VSD Compresor 2', clase: 'alarma' },
    { id: 248, descripcion: 'Alta presión de descarga (Software) Compresor 2', clase: 'alarma' },
    { id: 249, descripcion: 'Descarga de presión alta (Hardware-HPCQ) Compresor 2', clase: 'alarma' },
    { id: 250, descripcion: 'Fallo en control de voltaje Compresor 2', clase: 'alarma' },
    { id: 251, descripcion: 'Baja descarga de sobrecalentamiento Compresor 2', clase: 'alarma' },
    { id: 300, descripcion: 'Equipo Apagado por bajo voltaje EEQ', clase: 'apagado' },
    { id: 301, descripcion: 'Equipo Inhabilitado', clase: 'deshabilitado' },
    { id: 302, descripcion: 'Equipo en Mantenimiento', clase: 'mantenimiento' },
  ];

  let ListaVar = [
    { id: 0, descripcion: 'Equipo Apagado', clase: 'apagado' },
    { id: 1, descripcion: 'Equipo encendido', clase: 'operativo' },
    { id: 2, descripcion: 'Umbral de frecuencia', clase: 'almpreventiva' },
    { id: 34, descripcion: 'Pérdida de comunicación', clase: 'alarma' },
    { id: 17, descripcion: 'Pérdida de comunicación', clase: 'alarma' },
    { id: 13, descripcion: 'Corriente Alta', clase: 'alarma' },
    { id: 40, descripcion: 'Corriente Alta', clase: 'alarma' },
    { id: 41, descripcion: 'Corriente Alta', clase: 'alarma' },
    { id: 42, descripcion: 'Corriente Alta', clase: 'alarma' },
    { id: 59, descripcion: 'Corriente Alta', clase: 'alarma' },
    { id: 11, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 29, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 65, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 69, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 74, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 244, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 245, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 247, descripcion: 'Temperatura alta', clase: 'alarma' },
    { id: 49, descripcion: 'Velocidad de inversor alta', clase: 'alarma' },
    { id: 62, descripcion: 'Velocidad de inversor alta', clase: 'alarma' },
    { id: 5, descripcion: 'Voltaje de DC BUS alto', clase: 'alarma' },
    { id: 7, descripcion: 'Voltaje de DC BUS alto', clase: 'alarma' },
    { id: 64, descripcion: 'Voltaje de DC BUS alto', clase: 'alarma' },
    { id: 1, descripcion: 'Voltaje de DC BUS bajo', clase: 'alarma' },
    { id: 6, descripcion: 'Voltaje de DC BUS bajo', clase: 'alarma' },
    { id: 8, descripcion: 'Voltaje de DC BUS bajo', clase: 'alarma' },
    { id: 16, descripcion: 'Cortocircuito en salida del inversor', clase: 'alarma' },
    { id: 14, descripcion: 'Corriente a tierra alta', clase: 'alarma' },
    { id: 10, descripcion: 'Sobrecarga del motor', clase: 'alarma' },
    { id: 50, descripcion: 'Sobrecarga del motor', clase: 'alarma' },
    { id: 58, descripcion: 'Sobrecarga del motor', clase: 'alarma' },
    { id: 222, descripcion: 'Sobrecarga del motor', clase: 'alarma' },
    { id: 9, descripcion: 'Sobrecarga del inversor', clase: 'alarma' },
    { id: 12, descripcion: 'Limite de torque', clase: 'alarma' },
    { id: 142, descripcion: 'Falla externa de inversor', clase: 'alarma' },
    { id: 13, descripcion: 'Error operativo de interfaz', clase: 'alarma' },
    { id: 3, descripcion: 'Perdida de carga', clase: 'alarma' },
    { id: 95, descripcion: 'Perdida de carga', clase: 'alarma' },
    { id: 229, descripcion: 'Perdida de carga', clase: 'alarma' },
    { id: 70, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 76, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 79, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 81, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 82, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 91, descripcion: 'Error de configuración', clase: 'alarma' },
    { id: 60, descripcion: 'Falla de señal/sensor', clase: 'alarma' },
    { id: 90, descripcion: 'Falla de señal/sensor', clase: 'alarma' },
    { id: 192, descripcion: 'Falla de señal/sensor', clase: 'alarma' },
    { id: 30, descripcion: 'Falla de salida del inversor', clase: 'alarma' },
    { id: 31, descripcion: 'Falla de salida del inversor', clase: 'alarma' },
    { id: 32, descripcion: 'Falla de salida del inversor', clase: 'alarma' },
    { id: 99, descripcion: 'Parada del motor', clase: 'alarma' },
    { id: 4, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 33, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 36, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 37, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 46, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 228, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 246, descripcion: 'Error potencia del inversor', clase: 'alarma' },
    { id: 20, descripcion: 'Bajo voltaje', clase: 'alarma' },
    { id: 23, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 27, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 38, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 39, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 47, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 48, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 73, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 85, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 86, descripcion: 'Falla de la unidad interna', clase: 'alarma' },
    { id: 500, descripcion: 'Equipo Apagado por bajo voltaje EEQ', clase: 'apagado' },
    { id: 501, descripcion: 'Equipo Inhabilitado', clase: 'deshabilitado' },
    { id: 502, descripcion: 'Equipo en Mantenimiento', clase: 'mantenimiento' },
  ];

  let tabla_apa = [];
  let tabla_ope = [];
  let tabla_almp = [];
  let tabla_alm = [];
  let tabla_man = [];
  let tabla_des = [];
  let tiempo = new Date();
  let fecha =
    tiempo.getUTCDate() +
    '/' +
    (tiempo.getUTCMonth() + 1) +
    '/' +
    tiempo.getFullYear() +
    ' ' +
    tiempo.getHours() +
    ':' +
    tiempo.getMinutes();

  function datostablaups(fecha: any, sistema: any, tipo: any, equipo: any, dato: any, detalle: any) {
    let tabla = (
      <tr>
        <td>{fecha}</td>
        <td>B</td>
        <td>{sistema}</td>
        <td>{tipo}</td>
        <td>{equipo}</td>
        <td id={dato}> {dato.toUpperCase()} </td>
        <td> {detalle} </td>
      </tr>
    );
    return tabla;
  }

  let eventoEquipo: any;
  let eventoEquipo2: any;
  let evento = 0;
  let fechaDse = new Date();
  let fechaTrans = new Date();
  let detalle;
  let dato;

  let minutos = 0;
  let min = 0;
  let st_dse: any = data.series.find(({ name }) => name === 'EVENTO_DSE')?.fields[1].state?.calcs?.lastNotNull;
  let eventoDse = 1;
  if (st_dse > 0) {
    st_dse = st_dse.toString();
    fechaDse = new Date(parseFloat(st_dse.substring(0, st_dse.length - 3)));
    eventoDse = parseInt(st_dse.substring(st_dse.length - 3, st_dse.length));
  }
  for (let i = 1; i <= 10; i++) {
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_UMA' + i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_UMA' + i)?.fields[1].state?.calcs?.lastNotNull;

    if (eventoEquipo === 0 || eventoEquipo === null || eventoEquipo === undefined) {
      dato = 'apagado';
      evento = 0;
      fechaTrans = new Date();
    } else {
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring(eventoEquipo.length - 3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, eventoEquipo.length - 3)));
      console.log('transformados', evento, fechaTrans);
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 11;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 12;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 13;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    }
    console.log('fecha dse222', st_dse, evento, fechaDse);
    console.log(st_dse, fechaDse.getTime(), fechaTrans.getTime(), min, minutos, '¡¡¡MINUTOS....!!!');
    console.log('Datos eventos', eventoEquipo, evento, fechaTrans);
    fecha =
      fechaTrans.getUTCDate() +
      '/' +
      (fechaTrans.getUTCMonth() + 1) +
      '/' +
      fechaTrans.getFullYear() +
      ' ' +
      fechaTrans.getHours() +
      ':' +
      fechaTrans.getMinutes();
    detalle = ListaUma.find(o => o.id === parseInt('' + evento));
    console.log('detalles', detalle?.descripcion, detalle?.clase);
    if (detalle?.clase === 'apagado') {
      tabla_apa.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'operativo') {
      tabla_ope.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'almpreventiva') {
      tabla_almp.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'alarma') {
      tabla_alm.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'deshabilitado') {
      tabla_des.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'mantenimiento') {
      tabla_man.push(datostablaups(fecha, '1&2', 'AACC', '2/UMA-' + i, detalle?.clase, detalle?.descripcion));
    }
  }

  for (let i = 1; i <= 4; i++) {
    let sistema = 1;
    if (i > 2) {
      sistema = 2;
    }
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_CHI' + i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_CHI' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null || eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato = 'apagado';
      evento = 0;
      fechaTrans = new Date();
    } else {
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring(eventoEquipo.length - 3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, eventoEquipo.length - 3)));
      console.log('transformados', evento, fechaTrans);
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 300;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 301;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 302;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    }
    console.log('Datos eventos', eventoEquipo, evento, fechaTrans);
    fecha =
      fechaTrans.getUTCDate() +
      '/' +
      (fechaTrans.getUTCMonth() + 1) +
      '/' +
      fechaTrans.getFullYear() +
      ' ' +
      fechaTrans.getHours() +
      ':' +
      fechaTrans.getMinutes();
    detalle = ListaChill.find(o => o.id === parseInt('' + evento));
    console.log('detalles', detalle?.descripcion, detalle?.clase);
    if (detalle?.clase === 'apagado') {
      tabla_apa.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'operativo') {
      tabla_ope.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'almpreventiva') {
      tabla_almp.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'alarma') {
      tabla_alm.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'deshabilitado') {
      tabla_des.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'mantenimiento') {
      tabla_man.push(datostablaups(fecha, sistema, 'AACC', '2/EA(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
  }

  ////// VARIADORES
  for (let i = 1; i <= 6; i++) {
    let sistema = 1;
    if (i > 3) {
      sistema = 2;
    }
    eventoEquipo = data.series.find(({ name }) => name === 'EVENTO_VAR' + i)?.fields[1].state?.calcs?.lastNotNull;
    eventoEquipo2 = data.series.find(({ name }) => name === 'EVENTO2_VAR' + i)?.fields[1].state?.calcs?.lastNotNull;
    if (eventoEquipo === 0 || eventoEquipo === null || eventoEquipo === undefined) {
      detalle = 'Equipo Apagado';
      dato = 'apagado';
      evento = 0;
      fechaTrans = new Date();
    } else {
      eventoEquipo = eventoEquipo.toString();
      evento = parseFloat(eventoEquipo.substring(eventoEquipo.length - 3, eventoEquipo.length));
      fechaTrans = new Date(parseFloat(eventoEquipo.substring(0, eventoEquipo.length - 3)));
      console.log('transformados', evento, fechaTrans);
    }
    if (eventoEquipo === 0 && eventoDse === 0) {
      min = Math.abs(fechaDse.getTime() - fechaTrans.getTime());
      minutos = min / (1000 * 60 * 60);
      if (minutos < 4) {
        dato = 'apagado';
        evento = 500;
      }
    }
    if (eventoEquipo2 === 1) {
      dato = 'inhabilitado';
      evento = 501;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    } else if (eventoEquipo2 === 2) {
      dato = 'mantenimiento';
      evento = 502;
      fechaTrans = new Date(parseFloat(eventoEquipo2.substring(0, eventoEquipo2.length - 3)));
    }
    console.log('Datos eventos', eventoEquipo, evento, fechaTrans);
    fecha =
      fechaTrans.getUTCDate() +
      '/' +
      (fechaTrans.getUTCMonth() + 1) +
      '/' +
      fechaTrans.getFullYear() +
      ' ' +
      fechaTrans.getHours() +
      ':' +
      fechaTrans.getMinutes();
    detalle = ListaVar.find(o => o.id === parseInt('' + evento));
    console.log('detalles', detalle?.descripcion, detalle?.clase);
    if (detalle?.clase === 'apagado') {
      tabla_apa.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'operativo') {
      tabla_ope.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'almpreventiva') {
      tabla_almp.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'alarma') {
      tabla_alm.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'deshabilitado') {
      tabla_des.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
    if (detalle?.clase === 'mantenimiento') {
      tabla_man.push(datostablaups(fecha, sistema, 'AACC', '2/B1(' + i + ')', detalle?.clase, detalle?.descripcion));
    }
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${'100%'}px;
          height: ${'100%'}px;
        `
      )}
    >
      <div id="table-scroll">
        <table className="table">
          <thead>
            <tr>
              <th>FECHA</th>
              <th>FASE</th>
              <th>SISTEMA</th>
              <th>TIPO DE SISTEMA</th>
              <th>EQUIPO </th>
              <th>ESTADO </th>
              <th>DETALLE </th>
            </tr>
          </thead>
          <tbody>
            {tabla_alm}
            {tabla_almp}
            {tabla_man}
            {tabla_des}
            {tabla_ope}
            {tabla_apa}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
