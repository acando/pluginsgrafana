import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx, injectGlobal } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/styles.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  console.log(data);
  // LISTA

  let chi_s1 = data.series.find(({ name }) => name === 'CHILL_S1')?.fields[1].state?.calcs?.lastNotNull;
  let chi_s2 = data.series.find(({ name }) => name === 'CHILL_S2')?.fields[1].state?.calcs?.lastNotNull;
  let ups_s1 = data.series.find(({ name }) => name === 'UPS_S1')?.fields[1].state?.calcs?.lastNotNull;
  let ups_10 = data.series.find(({ name }) => name === 'UPS_10')?.fields[1].state?.calcs?.lastNotNull;
  let ups_s2 = data.series.find(({ name }) => name === 'UPS_S2')?.fields[1].state?.calcs?.lastNotNull;
  let pdu_s1 = data.series.find(({ name }) => name === 'PDU_S1')?.fields[1].state?.calcs?.lastNotNull;
  let pdu_s2 = data.series.find(({ name }) => name === 'PDU_S2')?.fields[1].state?.calcs?.lastNotNull;
  let gen_s1 = data.series.find(({ name }) => name === 'GEN_S1')?.fields[1].state?.calcs?.lastNotNull;
  let gen_s2 = data.series.find(({ name }) => name === 'GEN_S2')?.fields[1].state?.calcs?.lastNotNull;
  let umas_s1 = data.series.find(({ name }) => name === 'UMAS_S1')?.fields[1].state?.calcs?.lastNotNull;
  let var_s1 = data.series.find(({ name }) => name === 'VAR_S1')?.fields[1].state?.calcs?.lastNotNull;
  let var_s2 = data.series.find(({ name }) => name === 'VAR_S2')?.fields[1].state?.calcs?.lastNotNull;
  let pump_s1 = data.series.find(({ name }) => name === 'BOMBA_S1')?.fields[1].state?.calcs?.lastNotNull;
  let pump_s2 = data.series.find(({ name }) => name === 'BOMBA_S2')?.fields[1].state?.calcs?.lastNotNull;
  let trafo_s1 = data.series.find(({ name }) => name === 'TRAFO_S1')?.fields[1].state?.calcs?.lastNotNull;
  let trafo_s2 = data.series.find(({ name }) => name === 'TRAFO_S2')?.fields[1].state?.calcs?.lastNotNull;
  let ats_s1 = data.series.find(({ name }) => name === 'ATS_S1')?.fields[1].state?.calcs?.lastNotNull;
  let ats_s2 = data.series.find(({ name }) => name === 'ATS_S2')?.fields[1].state?.calcs?.lastNotNull;

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${'100%'}px;
          height: ${'100%'}px;
        `
      )}
    >
      <div>
        <table className='table'>
<tbody>
<tr >
<td id="encabezado_prin" colSpan={4} >FASE B</td>
</tr>
<tr >
<td id="encabezado" >Equipo</td>
<td id="encabezado" >Disponibilidad M&iacute;nima</td>
<td id="encabezado" >Sistema 1</td>
<td id="encabezado" >Sistema 2</td>
</tr>
<tr >
<td id="encabezado" >Transformador</td>
<td >50%</td>
<td >{trafo_s1} %</td>
<td >{trafo_s2} %</td>
</tr>
<tr >
<td id="encabezado" >Tableros</td>
<td >50%</td>
<td >{ats_s1} %</td>
<td >{ats_s2} %</td>
</tr>
<tr >
<td id="encabezado" >UPS 200KVA</td>
<td >50%</td>
<td >{ups_s1} %</td>
<td >{ups_s2} %</td>
</tr>
<tr >
<td id="encabezado" >UPS 10 KVA</td>
<td >50%</td>
<td >{ups_10} %</td>
<td >{ups_10} %</td>
</tr>
<tr >
<td id="encabezado" >PDU</td>
<td >50%</td>
<td >{pdu_s1} %</td>
<td >{pdu_s2} %</td>
</tr>
<tr >
<td id="encabezado" >Generador</td>
<td >50%</td>
<td >{gen_s1} %</td>
<td >{gen_s2} %</td>
</tr>
<tr >
<td id="encabezado" >Variador</td>
<td >50%</td>
<td >{var_s1} %</td>
<td >{var_s2} %</td>
</tr>
<tr>
<td id="encabezado" >UMAS</td>
<td >50%</td>
<td >{umas_s1} %</td>
<td >{umas_s1} %</td>
</tr>
<tr>
<td id="encabezado" >CHILLERS</td>
<td >50%</td>
<td >{chi_s1} %</td>
<td >{chi_s2} %</td>
</tr>
<tr>
<td id="encabezado" >BOMBAS</td>
<td >50%</td>
<td >{pump_s1} %</td>
<td >{pump_s2} %</td>
</tr>
</tbody>
</table>
      </div>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
