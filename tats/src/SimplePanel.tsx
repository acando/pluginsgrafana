import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
//import styleEstado from 'elementos/Estado';
import './css/stylesPop.css';
const swal = require('sweetalert');

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  const d = new Date();
  d.setTime(d.getTime() + 30 * 60 * 1000);
  let expira = ';expires=' + d.toUTCString();
  let alm_gen = 0;
  let salarma = 'http://172.30.128.202:1880/uimedia/audio/alarma.mp4'; //rojo
  let imgTabAdv = 'http://172.30.128.202:1880/uimedia/icon/tab400_adv.png';
  let imgTabAlm = 'http://172.30.128.202:1880/uimedia/icon/tab400_alm.png';
  let msgEstado = '';
  let colorEstado = '';
  let imgTab = '';

  //nombre
  let data_repla: any = data.series.find(({ refId }) => refId === 'B')?.name;
  let nom_on: any = data_repla.replace(/[.*+?^${}()|[\]\\]/g, '');
  let sistema = 1;
  let ubicacion = 'TABLEROS - 2A';

  if (nom_on === 'ATS-4B-EEQ' || nom_on === 'ATS-4B-A' || nom_on === 'ATS-4B-B') {
    sistema = 2;
    ubicacion = 'TABLEROS - 2B';
  }

  let fp = data.series.find(({ name }) => name === 'Average DATA.POW_FACT_TOTAL.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let fac;
  if (fp === 0 || fp === null) {
    fac = (0).toFixed(2);
  } else {
    fac = (fp / 100 - 0.01).toFixed(2);
  }

  let vol_a = data.series.find(({ name }) => name === 'Average DATA.V_FASE_A.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (vol_a === null || vol_a === 0) {
    vol_a = 0;
  } else {
    vol_a = (vol_a * 1.73).toFixed(1);
  }

  let vol_b = data.series.find(({ name }) => name === 'Average DATA.V_FASE_B.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (vol_b === null || vol_b === 0) {
    vol_b = 0;
  } else {
    vol_b = (vol_b * 1.73).toFixed(1);
  }

  let vol_c = data.series.find(({ name }) => name === 'Average DATA.V_FASE_C.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (vol_c === null || vol_c === 0) {
    vol_c = 0;
  } else {
    vol_c = (vol_c * 1.73).toFixed(1);
  }

  let vol = ((parseFloat(vol_a) + parseFloat(vol_b) + parseFloat(vol_c)) / 3).toFixed(0);

  let cur_a = data.series.find(({ name }) => name === 'Average DATA.CUR_FASE_A.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (cur_a === null || cur_a === 0) {
    cur_a = 0;
  } else {
    cur_a = cur_a.toFixed(1);
  }

  let cur_b = data.series.find(({ name }) => name === 'Average DATA.CUR_FASE_B.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (cur_b === null || cur_b === 0) {
    cur_b = 0;
  } else {
    cur_b = cur_b.toFixed(1);
  }

  let cur_c = data.series.find(({ name }) => name === 'Average DATA.CUR_FASE_C.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (cur_c === null || cur_c === 0) {
    cur_c = 0;
  } else {
    cur_c = cur_c.toFixed(1);
  }
  let cur = ((parseFloat(cur_a) + parseFloat(cur_b) + parseFloat(cur_c)) / 3).toFixed(1);

  let pow_real = data.series.find(({ name }) => name === 'Average DATA.POW_REAL_TOTAL.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (pow_real === null || pow_real === 0) {
    pow_real = 0;
  } else {
    pow_real = (pow_real / 10).toFixed(1);
  }

  let pow_appt = data.series.find(({ name }) => name === 'Average DATA.POW_APPRT_TOTAL.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  if (pow_appt === null || pow_appt === 0) {
    pow_appt = 0;
  } else {
    pow_appt = (pow_appt / 10).toFixed(1);
  }

  //////***** ALARMAS *****/////
  let st = '';
  let led;
  let adv_vol = 0;
  let adv_cur = 0;
  let stEquipo = 0;
  let pos = data.series.find(({ name }) => name === 'Average DATA.POS_DEC.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let mant = data.series.find(({ name }) => name === 'Average DATA.ST_MANT.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let hab = data.series.find(({ name }) => name === 'Average DATA.ST_HABIL.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  if (pos === 0 || pos === null) {
    led = '#4d4d4d';
    st = 'STAND-BY';
    msgEstado = 'apagado';
    colorEstado = 'alarma';
    imgTab = imgTabAlm;
    stEquipo = 0;
  } else {
    led = '#1aea78';
    st = 'ON';
    msgEstado = 'encendido';
    colorEstado = 'advertencia';
    imgTab = imgTabAdv;
    stEquipo = 1;
  }
  if (mant === 1) {
    led = '#9955ff';
    st = 'MANT';
  }
  if (hab === 0) {
    led = '#ff9955';
    st = 'INHABIL';
  }

  if ((pos === 1 && parseFloat(vol) >= 472.3) || (pos === 1 && parseFloat(vol) <= 391)) {
    adv_vol = 1;
  } else {
    adv_vol = 0;
  }
  if (pos === 1 && parseFloat(cur) >= 1280) {
    adv_cur = 1;
  } else {
    adv_cur = 0;
  }
  let st_alerta_vol = revisar_data_status1(adv_vol);
  let st_alerta_cur = revisar_data_status1(adv_cur);

  //let classAlm;
  let alm: any = revisar_data_status(
    data.series.find(({ name }) => name === 'Average DATA.GEN_ALM_DEC.VALUE')?.fields[1].state?.calcs?.lastNotNull
  );

  //let classMod;
  let mod: any = revisar_data_status(
    data.series.find(({ name }) => name === 'Average DATA.MODBUS_ST_DEC.VALUE')?.fields[1].state?.calcs?.lastNotNull
  );

  function reproducir(sonido: any) {
    const audio = new Audio(sonido);
    audio.play();
  }

  function revisar_data_status(stringdata: any) {
    let data_ret_string = [];
    if (stringdata === null || stringdata === 0) {
      data_ret_string[0] = '#4d4d4d';
      data_ret_string[1] = '1s';
    } else {
      data_ret_string[0] = 'red';
      data_ret_string[1] = '1s';
      alm_gen = alm_gen + 1;
      //reproducir(salarma);
    }
    return data_ret_string;
  }
  function revisar_data_status1(adv: any) {
    let data_ret_string = [];
    if (adv === null || adv === 0) {
      data_ret_string[0] = '#fff';
      data_ret_string[1] = '1s';
    } else {
      data_ret_string[0] = '#ccff00'; //color amarillo
      data_ret_string[1] = '1.5s';
      //reproducir(sadvertencia);
    }
    return data_ret_string;
  }

  function PopUp(cookieVar: any, equipo: any, variable: any, nomCookie: any) {
    if (cookieVar === '') {
      document.cookie = 'cookie_' + nomCookie + '=' + variable + expira;
    } else {
      if (cookieVar !== '' + variable) {
        reproducir(salarma);
        swal({
          className: colorEstado,
          title: equipo,
          text: 'Equipo ' + msgEstado,
          icon: imgTab,
        }).then((value: any) => {
          document.cookie = 'cookie_' + nomCookie + '=' + variable + expira;
        });
      }
    }
  }

  function getCookie(cname: any) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }
  let cookieEstado = getCookie('cookie_' + nom_on);
  let cookieAlm = getCookie('cookie_' + nom_on + 'alm');
  let cookieAdv = getCookie('cookie_' + nom_on + 'adv');
  PopUp(cookieEstado, nom_on, stEquipo, nom_on);

  /*if ((adv_vol === 1 && mant === 0 && hab === 1) || (adv_cur === 1 && mant === 0 && hab === 1)) {
    reproducir(sadvertencia);
  }
  if ((alm[0] === 'red' && mant === 0 && hab === 1) || (mod[0] === 'red' && mant === 0 && hab === 1)) {
    reproducir(salarma);
  }*/
  if ((adv_cur === 1 || adv_vol === 1) && mant === 0 && hab === 1) {
    msgEstado = 'alarmado';
    colorEstado = 'advertencia';
    imgTab = imgTabAdv;
    PopUp(cookieAdv, nom_on, 1, nom_on + 'adv');
  } else {
    document.cookie = 'cookie_' + nom_on + 'adv' + '=' + 0 + expira;
  }
  if (alm_gen > 0 && mant === 0 && hab === 1) {
    msgEstado = 'alarmado';
    colorEstado = 'alarma';
    imgTab = imgTabAlm;
    PopUp(cookieAlm, nom_on, 1, nom_on + 'alm');
  } else {
    document.cookie = 'cookie_' + nom_on + 'alm' + '=' + 0 + expira;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        id="svg760"
        viewBox="0 0 507.99999 198.43751"
        height={'100%'}
        width={'100%'}
        //{...props}
      >
        <defs id="defs754">
          <filter
            height={1.0816041}
            y={-0.040802043}
            width={1.0644186}
            x={-0.032209255}
            id="filter21601-3-5"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21603-0-7" stdDeviation={0.046267815} />
          </filter>
          <filter
            height={1.1046808}
            y={-0.052340381}
            width={1.0886487}
            x={-0.044324357}
            id="filter21611-1-1"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21613-4-0" stdDeviation={0.05935181} />
          </filter>
          <filter
            id="filter21601-3-5-4"
            x={-0.032209255}
            width={1.0644186}
            y={-0.040802043}
            height={1.0816041}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.046267815} id="feGaussianBlur21603-0-7-9" />
          </filter>
          <filter
            id="filter21611-1-1-4"
            x={-0.044324357}
            width={1.0886487}
            y={-0.052340381}
            height={1.1046808}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.05935181} id="feGaussianBlur21613-4-0-4" />
          </filter>
          <filter
            height={1.1046808}
            y={-0.052340381}
            width={1.0886487}
            x={-0.044324357}
            id="filter21611-1-1-4-7"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21613-4-0-4-9" stdDeviation={0.05935181} />
          </filter>
          <filter
            id="filter21611-1-1-4-7-6"
            x={-0.044324357}
            width={1.0886487}
            y={-0.052340381}
            height={1.1046808}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.05935181} id="feGaussianBlur21613-4-0-4-9-9" />
          </filter>
          <filter
            height={1.0816041}
            y={-0.040802043}
            width={1.0644186}
            x={-0.032209255}
            id="filter21601"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21603" stdDeviation={0.046267815} />
          </filter>
          <filter
            height={1.1046808}
            y={-0.052340381}
            width={1.0886487}
            x={-0.044324357}
            id="filter21611"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21613" stdDeviation={0.05935181} />
          </filter>
          <filter
            id="filter21601-1"
            x={-0.032209255}
            width={1.0644186}
            y={-0.040802043}
            height={1.0816041}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.046267815} id="feGaussianBlur21603-9" />
          </filter>
          <filter
            id="filter21611-4"
            x={-0.044324357}
            width={1.0886487}
            y={-0.052340381}
            height={1.1046808}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.05935181} id="feGaussianBlur21613-2" />
          </filter>
          <filter
            height={1.0816041}
            y={-0.040802043}
            width={1.0644186}
            x={-0.032209255}
            id="filter21601-1-7"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21603-9-4" stdDeviation={0.046267815} />
          </filter>
          <filter
            height={1.1046808}
            y={-0.052340381}
            width={1.0886487}
            x={-0.044324357}
            id="filter21611-4-9"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21613-2-1" stdDeviation={0.05935181} />
          </filter>
          <filter
            height={1.0227143}
            y={-0.011357153}
            width={1.02544}
            x={-0.012719987}
            id="filter21324-6"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21326-5" stdDeviation={0.008764315} />
          </filter>
          <filter
            height={1.0271899}
            y={-0.013594938}
            width={1.2045712}
            x={-0.10228567}
            id="filter21328-6"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21330-7" stdDeviation={0.059200515} />
          </filter>
          <filter
            height={1.1710345}
            y={-0.085517257}
            width={1.0279174}
            x={-0.013958724}
            id="filter21336-5"
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur id="feGaussianBlur21338-9" stdDeviation={0.06344668} />
          </filter>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath835-87-6-6">
            <path
              d="M96.618 96.28l-37.15-13.897V92.94H44.9l-4.96 27.807 32.334 7.637z"
              id="path837-76-3-7"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#fff"
              strokeWidth={0.565}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={21.3543}
              strokeOpacity={1}
              paintOrder="markers stroke fill"
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient8861"
            id="linearGradient8863"
            x1={258.93073}
            y1={223.38387}
            x2={258.93073}
            y2={273.52362}
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(-27.423 -123.608)"
          />
          <linearGradient id="linearGradient8861">
            <stop offset={0} id="stop8857" stopColor="#178299" stopOpacity={1} />
            <stop offset={1} id="stop8859" stopColor="#178299" stopOpacity={0} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient8841"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(1.01602 0 0 1 -304.943 -210.586)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <linearGradient id="linearGradient15175">
            <stop offset={0} id="stop15171" stopColor="#178497" stopOpacity={1} />
            <stop offset={1} id="stop15173" stopColor="#178497" stopOpacity={0} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient15175"
            id="linearGradient3292"
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(-1.01602 0 0 1 566.91 -210.446)"
            x1={524.13776}
            y1={291.75107}
            x2={525.07324}
            y2={216.72852}
          />
          <linearGradient
            xlinkHref="#linearGradient15375"
            id="linearGradient15377"
            x1={476.53326}
            y1={236.85876}
            x2={524.95203}
            y2={237.25563}
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(-269.212 -57.737)"
          />
          <linearGradient id="linearGradient15375">
            <stop offset={0} id="stop15371" stopColor="#168498" stopOpacity={1} />
            <stop offset={1} id="stop15373" stopColor="#168498" stopOpacity={0} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient4372"
            id="linearGradient4374"
            x1={582.47876}
            y1={50.31768}
            x2={650.33972}
            y2={50.324806}
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(1 0 0 .598 -270.668 .167)"
          />
          <linearGradient id="linearGradient4372">
            <stop offset={0} id="stop4368" stopColor="#ff0" stopOpacity={1} />
            <stop offset={1} id="stop4370" stopColor="#000" stopOpacity={1} />
          </linearGradient>
          <linearGradient
            gradientTransform="matrix(1.00215 0 0 1 5.852 48.576)"
            gradientUnits="userSpaceOnUse"
            y2={-45.765266}
            x2={120.25919}
            y1={-16.749292}
            x1={119.72682}
            id="linearGradient1239"
            xlinkHref="#linearGradient1237"
          />
          <linearGradient id="linearGradient1237">
            <stop id="stop1233" offset={0} stopColor="#0e0e0e" stopOpacity={0.91} />
            <stop id="stop1235" offset={1} stopColor="#000" stopOpacity={0} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient4372"
            id="linearGradient4374-9"
            x1={582.47876}
            y1={50.31768}
            x2={650.33972}
            y2={50.324806}
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(1 0 0 .598 -271.07 87.222)"
          />
          <linearGradient
            xlinkHref="#linearGradient1260"
            id="linearGradient4374-9-7"
            x1={671.33923}
            y1={50.441502}
            x2={587.38568}
            y2={50.083946}
            gradientUnits="userSpaceOnUse"
            gradientTransform="matrix(.89198 0 0 -.598 -106.618 72.394)"
          />
          <linearGradient id="linearGradient1260">
            <stop offset={0} id="stop1258" stopColor="#000" stopOpacity={1} />
            <stop offset={1} id="stop1256" stopColor="#ff0" stopOpacity={1} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient14947"
            id="linearGradient14967"
            gradientUnits="userSpaceOnUse"
            x1={79.872833}
            y1={114.53431}
            x2={81.360344}
            y2={36.041458}
            gradientTransform="matrix(-1.07806 0 0 .99767 317.807 -27.717)"
          />
          <linearGradient id="linearGradient14947">
            <stop offset={0} id="stop14943" stopColor="#002746" stopOpacity={1} />
            <stop offset={1} id="stop14945" stopColor="#002746" stopOpacity={0} />
          </linearGradient>
          <clipPath id="clipPath1984" clipPathUnits="userSpaceOnUse">
            <path
              id="path1986"
              d="M371.505 79.284l5.412-6.681s11.359 3.474 11.693 3.474c.334 0 9.087-6.548 9.087-6.548s3.408-7.016 3.541-7.35c.134-.334.334-5.813.334-5.813s-2.338-2.673-2.806-2.94c-.468-.267-6.481-3.34-6.815-3.675-.334-.334-4.01-3.809-4.277-4.076-.267-.267-.6-2.272-.6-2.272l.266-7.216 11.827 3.675 5.813 4.945s5.88 8.552 6.014 9.554c.133 1.003 2.205 8.486 1.403 10.424-.802 1.938-3.541 8.62-4.343 9.555-.802.935-6.749 6.28-7.283 6.615-.535.334-6.348 3.073-7.818 3.541-1.47.468-10.49 1.604-11.827 1.537-1.336-.067-5.68-2.806-6.548-3.14-.868-.335-3.073-3.609-3.073-3.609z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath2072" clipPathUnits="userSpaceOnUse">
            <path
              id="path2074"
              d="M387.609 46.155l-.035-11.264 10.34 1.678 5.765 5.103s3.78 4.63 4.157 5.48c.378.851 3.875 6.332 3.875 6.332l1.606 13.229s-3.307 6.236-3.685 7.37c-.378 1.134-6.71 8.221-7.087 8.41-.378.19-12.757 1.796-13.89 2.268-1.135.473-7.371.945-7.371.945l-6.899-2.457-4.63-3.307 5.953-7.56s4.82-4.724 6.143-5.102c1.322-.378 6.898-.945 6.898-.945z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath1163" clipPathUnits="userSpaceOnUse">
            <path
              id="path1165"
              d="M131.347 106.211v-6.425l-23.718 6.614s8.126 31.75 8.504 32.79c.162.444 21.545-1.323 21.261-.095-.283 1.229 10.017-4.819 9.639-4.819h-3.213l-5.197-6.615z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <filter
            id="filter2963"
            x={-0.10655243}
            width={1.2131048}
            y={-0.13087419}
            height={1.2617484}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.16760617} id="feGaussianBlur2965" />
          </filter>
          <filter
            id="filter2703"
            x={-0.093270496}
            width={1.186541}
            y={-0.022304475}
            height={1.044609}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.10552047} id="feGaussianBlur2705" />
          </filter>
          <filter
            id="filter2663"
            x={-0.011369487}
            width={1.0227391}
            y={-0.012704551}
            height={1.0254091}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={0.089972254} id="feGaussianBlur2665" />
          </filter>
          <clipPath id="clipPath374-4" clipPathUnits="userSpaceOnUse">
            <path
              id="path376-4"
              d="M-86.935-109.613L-102.81-317.5l94.495-55.94h260.047l-7.56 250.22-38.553 52.16z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            y2={36.041458}
            x2={81.360344}
            y1={114.53431}
            x1={79.872833}
            gradientTransform="matrix(-1.07806 0 0 .99767 317.807 -27.717)"
            gradientUnits="userSpaceOnUse"
            id="linearGradient1486"
            xlinkHref="#linearGradient14947"
          />
          <clipPath id="clipPath40132" clipPathUnits="userSpaceOnUse">
            <path
              d="M245.934 114.315h29.795v30.098h-29.795zm4.564 9.03l11.675 6.5 3.125-2.992-.534-.2-.167-4.51-12.903-2.828z"
              className="powerclip"
              id="lpe_path-effect40136"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <linearGradient
            xlinkHref="#linearGradient15375"
            id="linearGradient7984"
            x1={448.90973}
            y1={9.5249996}
            x2={448.90973}
            y2={34.219444}
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(-.53 -1.058)"
          />
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath2974">
            <path
              d="M-791.118-496.052l-34.21-457.566 188.157-128.289 662.829 17.105-34.21 500.329-119.737 188.157z"
              id="path2976"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath2974-7" clipPathUnits="userSpaceOnUse">
            <path
              id="path2976-8"
              d="M-791.118-496.052l-34.21-457.566 188.157-128.289 662.829 17.105-34.21 500.329-119.737 188.157z"
              fill="none"
              stroke="#000"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath3209" clipPathUnits="userSpaceOnUse">
            <path
              id="path3211"
              d="M-764.262-507.124l-15.573-48.135-25.483-312.876 8.494-93.438 70.787-9.91-2.832-24.067 63.708-5.663-2.831-35.393h101.932l5.663-39.64L-39.409-1072l-7.079 142.988H.232l-31.854 351.1-16.99 38.933-27.606.708-74.326 137.326-610.886-99.809z"
              fill="none"
              stroke="#000"
              strokeWidth="1.4015px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
        </defs>
        <g id="layer1">
          <path
            d="M271.448 152.743V141.56l-2.71-3.342v-28.41l-1.667-2.057v-3.342l6.357-7.713h15.422l4.273 5.4h76.8l3.96-4.886h9.274l3.44 3.985v5.27l-5.42 6.557v22.506l5.447 6.818v16.135l-3.28 3.772v15.953l1.732 1.772-.073 2.546-5.416 6.544H346.65l-3.242-3.817h-52.464l-1.842-2.455h-7.774l-1.879 2.5h-8.51l-1.88-2.5v-27.451z"
            id="path1746-1"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M268.29 178.774l-.958-1.95v-12.851l2.338-3.768h3.91v15.676l-1.533 2.893z"
            id="path1748-8"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M269.854 136.81v-25.596l2.641 3.504v25.973z"
            id="path1750-1"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M275.2 99.992l1.51-1.884h11.932l3.96 5.517h76.592l-.626 1.278h-89.721z"
            id="path1752-7"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M297.604 180.523h60.648l5.545 7.59h-2.67l-3.813-5.033h-6.852v-1.144h-51.868z"
            id="path1754-2"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M359.242 180.456h2.345l5.52 7.66-2.25.008z"
            id="path1756-0"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M386.391 108.3v10.484l-4.424 5.712v-10.483z"
            id="path1781-2"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M382.089 128.765v5.869l4.4 5.775v-6.12z"
            id="path1783-9"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M381.992 125.438v2.416l4.57 5.556v-6.34l-2.844-3.61z"
            id="path1785-5"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M386.562 126.034v-6.026l-2.383 2.981z"
            id="path1787-7"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M385.041 178.332l-.994-1.225v-13.942l1.433-1.532v2.237l-.393.337z"
            id="path1789-4"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1756-1-9"
            d="M362.885 180.456h2.345l5.52 7.66-2.25.008z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1756-3-1"
            d="M366.262 180.456h2.345l5.52 7.66-2.25.008z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M382.947 160.38l2.767-3.034v-14.445l-2.627-3.089z"
            id="path1814-1"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1746-1-7"
            d="M271.637 60.13V49.634l-2.71-3.137V19.83l-1.667-1.93v-3.138l6.357-7.24h15.422l4.273 5.069h76.8l3.96-4.586h9.274l3.44 3.74v4.948l-5.42 6.154v21.125l5.447 6.4v15.143l-3.28 3.541V84.03l1.732 1.664-.073 2.389-5.416 6.143h-32.937l-3.242-3.584h-52.464l-1.842-2.304h-7.774l-1.88 2.347h-8.51l-1.879-2.347V62.572z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1748-8-6"
            d="M268.48 84.564l-.96-1.831V70.67l2.34-3.536h3.91V81.85l-1.534 2.715z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1750-1-8"
            d="M270.043 45.176V21.15l2.641 3.29v24.378z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1752-7-2"
            d="M275.388 10.617L276.9 8.85h11.932l3.96 5.178h76.592l-.626 1.2h-89.721z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1754-2-9"
            d="M297.793 86.206h60.648l5.545 7.124h-2.67l-3.813-4.724h-6.852v-1.074h-51.868z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1756-0-8"
            d="M359.43 86.143h2.346l5.52 7.19-2.25.007z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1781-2-2"
            d="M386.58 18.416v9.84l-4.424 5.361v-9.84z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1783-9-6"
            d="M382.278 37.624v5.51l4.4 5.42v-5.745z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1785-5-9"
            d="M382.18 34.501v2.269l4.57 5.214v-5.95l-2.843-3.389z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1787-7-9"
            d="M386.75 35.061v-5.656l-2.382 2.798z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1789-4-5"
            d="M385.23 84.15l-.994-1.151V69.913l1.433-1.438v2.1l-.393.316z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M363.074 86.143h2.345l5.52 7.19-2.25.007z"
            id="path1756-1-9-5"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M366.45 86.143h2.346l5.52 7.19-2.25.007z"
            id="path1756-3-1-3"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1814-1-4"
            d="M383.136 67.3l2.767-2.849V50.893l-2.627-2.9z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M394.503 79.939v-9.32l-2.491-2.785V44.159l-1.533-1.714V39.66l5.845-6.427h14.181l3.929 4.499h70.62l3.64-4.07h8.529l3.162 3.32v4.392l-4.983 5.463v18.756l5.009 5.681v13.445l-3.016 3.144v71.683l1.593 1.477-.068 2.121-4.98 5.454h-30.287l-2.98-3.181H412.43l-1.694-2.046h-7.148l-1.728 2.083h-7.825l-1.728-2.083V82.106z"
            id="path1746-1-7-4"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M391.6 147.202l-.882-1.626v-10.708l2.15-3.14h3.596v13.063l-1.41 2.41z"
            id="path1748-8-6-8"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M393.037 66.662v-21.33l2.43 2.92v21.643z"
            id="path1750-1-8-9"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M397.953 35.98l1.39-1.57h10.97l3.642 4.597h70.428l-.575 1.066h-82.501z"
            id="path1752-7-2-6"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M500.197 42.904v8.736l-4.068 4.76v-8.736z"
            id="path1781-2-2-7"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M496.24 59.957v4.891l4.046 4.813v-5.1z"
            id="path1783-9-6-6"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M496.151 57.185v2.013l4.202 4.63v-5.283l-2.615-3.008z"
            id="path1785-5-9-9"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M500.353 57.681V52.66l-2.19 2.484z"
            id="path1787-7-9-7"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M498.888 160.228l-.913-1.021.066-70.583 1.318-1.277v1.864l-.361.28z"
            id="path1789-4-5-6"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M497.03 86.303l2.544-2.529V71.737l-2.415-2.574z"
            id="path1814-1-4-8"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1754-2-7"
            d="M413.154 160.988h60.648l5.545 6.738h-2.669l-3.814-4.469h-6.851v-1.015h-51.869z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1756-0-5"
            d="M474.792 160.928h2.345l5.52 6.801-2.25.006z"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M478.435 160.928h2.345l5.52 6.801-2.25.006z"
            id="path1756-1-9-8"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M481.812 160.928h2.345l5.52 6.801-2.25.006z"
            id="path1756-3-1-8"
            display="inline"
            fill="none"
            stroke="#3edce3"
            strokeWidth={0.349999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1448-8-2-3"
            d="M98.272 26.744v-10.59l-3.488-1.466v-3.15h67.358v3.53l-3.368 1.412v10.481l3.248 1.575v3.204H95.024v-3.476z"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1376-3-6-1-7"
            d="M96.507 30.873h1.978V28.13l-2.11.715z"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1376-3-3-6-1-1"
            d="M160.533 30.873h-1.978V28.13l2.11.715z"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1376-3-5-5-1-2"
            d="M96.507 12.418h1.978v2.742l-2.11-.716z"
            display="inline"
            opacity={0.75}
            fill="#192e4f"
            fillOpacity={1}
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1376-3-3-3-3-6-6"
            d="M160.533 12.418h-1.978v2.742l2.11-.716z"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1325-6-9-3-2"
            d="M95.478 26.586v-9.938"
            display="inline"
            opacity={0.75}
            fill="#00e4e8"
            fillOpacity={1}
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1325-6-3-1-9-0"
            d="M161.983 26.667V16.73"
            display="inline"
            opacity={0.75}
            fill="#00e4e8"
            fillOpacity={1}
            stroke="#00afd4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="rect4837-8-8"
            display="inline"
            opacity={0.25}
            fill="none"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#00fbff"
            strokeWidth={0.564999}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={5.65}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M99.577759 12.733377H157.612911V30.784303H99.577759z"
          />
          <path
            d="M395.893 5.824h3.214v2.722h-3.15z"
            id="path21607-0-0"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601-3-5)"
          />
          <path
            id="path21605-0-4"
            d="M400.127 5.824h3.213v2.722h-3.15z"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601-3-5)"
          />
          <path
            d="M54.82 213.162h3.214v2.721h-3.15z"
            id="path21609-4-0"
            transform="matrix(.91623 0 0 1 354.401 -207.337)"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1)"
          />
          <path
            id="path21607-0-0-2"
            d="M9.222 5.579h3.214V8.3h-3.15z"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601-3-5-4)"
          />
          <path
            d="M54.82 213.162h3.214v2.721h-3.15z"
            id="path21615-1-6-7"
            transform="matrix(17.62592 0 0 .71592 -942.243 -146.61)"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4)"
          />
          <path
            d="M13.456 5.579h3.213V8.3h-3.15z"
            id="path21605-0-4-8"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601-3-5-4)"
          />
          <path
            transform="matrix(.91623 0 0 1 -32.27 -207.583)"
            id="path21609-4-0-7"
            d="M54.82 213.162h3.214v2.721h-3.15z"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4)"
          />
          <path
            transform="matrix(23.8868 0 0 .71592 -1133.624 -145.53)"
            id="path21615-1-6-7-4"
            d="M54.82 213.162h3.214v2.721h-3.15z"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.173116}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4-7)"
          />
          <path
            d="M54.82 213.162h3.214v2.721h-3.15z"
            id="path21615-1-6-7-4-5"
            transform="matrix(5.05763 0 0 .71592 210.705 -143.968)"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.376221}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611-1-1-4-7-6)"
          />
          <path
            id="path2425"
            d="M82.043 10.762l6.998-6.196h74.083l7.56 6.047 85.8.19 7.337-6.053 122.657-.022 5.152 4.911"
            display="inline"
            fill="none"
            stroke="#00bec4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path2427"
            d="M82.15 10.454H2.688"
            display="inline"
            fill="none"
            stroke="#00bec4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path2610"
            d="M396.895 192.143v-3.591l16.63-12.662h75.485l11.225 7.715v9.889h-7.24l-6.047-4.092h-72.572l-3.78 3.213z"
            display="inline"
            fill="#000"
            fillOpacity={0}
            stroke="#00fbff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path2612"
            d="M487.61 188.741l5.763 4.063h3.97l-7.56-5.291h-78.336l-5.197 4.063h4.347l3.118-2.74z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#00fbff"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="rect2614-4"
            d="M403.684 184.908l2.773-.034-2.138 1.537-2.874.067z"
            display="inline"
            opacity={1}
            fill="#168198"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#00fbff"
            strokeWidth={0.349999}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="rect2614-5"
            d="M407.603 182.015l2.773-.034-2.138 1.537-2.873.067z"
            display="inline"
            opacity={0.75}
            fill="#04e6ed"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#00fbff"
            strokeWidth={0.349999}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="rect2614-53"
            d="M411.364 178.922l2.773-.034-2.138 1.537-2.873.067z"
            display="inline"
            opacity={1}
            fill="#168198"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="#00fbff"
            strokeWidth={0.349999}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <g
            transform="translate(-1.965 .263)"
            id="g2683"
            display="inline"
            fill="#000"
            fillOpacity={0}
            stroke="#00fbff"
            strokeOpacity={1}
            fillRule="evenodd"
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            paintOrder="markers stroke fill"
          >
            <path
              id="rect2644-7"
              display="inline"
              opacity={0.25}
              strokeWidth={0.349999}
              d="M420.65417 177.13287H437.49215000000004V178.3355828H420.65417z"
            />
            <path
              id="rect2644-1"
              display="inline"
              opacity={0.25}
              strokeWidth={0.35}
              d="M420.68222 179.53831H477.744266V180.7410228H420.68222z"
            />
            <path
              id="rect2644-8"
              display="inline"
              opacity={0.25}
              strokeWidth={0.349999}
              d="M420.68222 182.07738H484.426002V183.2800928H420.68222z"
            />
            <path
              id="rect2644-17"
              display="inline"
              opacity={0.25}
              strokeWidth={0.349999}
              d="M420.74899 184.44939H490.105435V185.6521028H420.74899z"
            />
          </g>
          <path
            id="path21605"
            d="M193.574 189.94h3.213v2.721h-3.15z"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601)"
          />
          <path
            d="M189.34 189.94h3.214v2.721h-3.15z"
            id="path21607"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21601)"
          />
          <path
            d="M54.82 213.162h3.214v2.721h-3.15z"
            id="path21609"
            transform="matrix(.91623 0 0 1 147.848 -23.222)"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611)"
          />
          <path
            transform="matrix(21.44472 0 0 .6693 -971.523 47.75)"
            id="path21615"
            d="M54.82 213.162h3.214v2.721h-3.15z"
            display="inline"
            opacity={0.8}
            fill="#00b1d4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            filter="url(#filter21611)"
          />
          <path
            d="M94.597 190.841h88.427"
            id="path21770"
            display="inline"
            opacity={0.8}
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.4}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path2927"
            d="M5.587 184.818v-7.04l4.188-1.519v-31.398l-4.091-1.418v-66.19l3.117 1.317v60.315l1.851.912v-52.72l15.391 5.774v61.835L22.05 155.8v-25.321l-7.013-2.583v53.883z"
            display="inline"
            fill="#123952"
            fillOpacity={0.992157}
            stroke="#1bfff8"
            strokeWidth={0.264999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path2929"
            d="M22.118 155.058v18.143l10.392 6.221"
            display="inline"
            fill="none"
            stroke="#1bfff8"
            strokeWidth={0.264999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path2931"
            d="M14.89 181.869l12.685 8.414 37.976.331"
            display="inline"
            fill="none"
            stroke="#1bfff8"
            strokeWidth={0.264999}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path3043"
            d="M10.564 87.02l2.94 1.136v52.92l-2.94-1.337z"
            display="inline"
            fill="#1bfff8"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            transform="translate(-1.965 .792)"
            id="g1243"
            display="inline"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          >
            <path
              id="path21605-1"
              d="M78.889 188.572h3.213v2.722h-3.15z"
              display="inline"
              opacity={0.8}
              fill="none"
              stroke="#0deff7"
              filter="url(#filter21601-1)"
            />
            <path
              d="M74.655 188.572h3.214v2.722h-3.15z"
              id="path21607-2"
              display="inline"
              opacity={0.8}
              fill="none"
              stroke="#0deff7"
              filter="url(#filter21601-1)"
            />
            <path
              d="M54.82 213.162h3.214v2.721h-3.15z"
              id="path21609-1"
              transform="matrix(.91623 0 0 1 33.163 -24.59)"
              display="inline"
              opacity={0.8}
              fill="#00b1d4"
              fillOpacity={1}
              stroke="none"
              filter="url(#filter21611-4)"
            />
          </g>
          <path
            id="path3045"
            d="M22.084 130.344v-16.017"
            display="inline"
            fill="none"
            stroke="#00f8ed"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g1243-7"
            transform="rotate(-90 40.713 205.41)"
            display="inline"
            strokeWidth={0.20153}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          >
            <path
              d="M78.889 188.572h3.213v2.722h-3.15z"
              id="path21605-1-5"
              display="inline"
              opacity={0.8}
              fill="none"
              stroke="#0deff7"
              filter="url(#filter21601-1-7)"
            />
            <path
              id="path21607-2-7"
              d="M74.655 188.572h3.214v2.722h-3.15z"
              display="inline"
              opacity={0.8}
              fill="none"
              stroke="#0deff7"
              filter="url(#filter21601-1-7)"
            />
            <path
              transform="matrix(.91623 0 0 1 33.163 -24.59)"
              id="path21609-1-9"
              d="M54.82 213.162h3.214v2.721h-3.15z"
              display="inline"
              opacity={0.8}
              fill="#00b1d4"
              fillOpacity={1}
              stroke="none"
              filter="url(#filter21611-4-9)"
            />
          </g>
          <g
            transform="matrix(1.12618 0 0 .99883 8.24 -2.204)"
            id="g21353-7"
            display="inline"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          >
            <path
              transform="matrix(.96447 0 0 .96923 .693 3.515)"
              id="path21306-8"
              d="M22.357 113.308v1.852h1.654v-1.852z"
              opacity={0.8}
              fill="#00aad4"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.2}
              filter="url(#filter21324-6)"
            />
            <path
              id="path21308-1"
              d="M21.657 122.7l-.093 10.452h1.389l-.047-10.443z"
              opacity={0.8}
              fill="#00aad4"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.2}
              filter="url(#filter21328-6)"
            />
            <path id="path21310-1" d="M22.357 115.16v7.507" fill="none" stroke="#00aad4" strokeWidth={0.2} />
            <path
              id="path21312-5"
              d="M22.423 133.151v25.928l-4.413 2.488v11.986l3.413 2.373.083 16.878h5.481"
              fill="none"
              fillOpacity={1}
              stroke="#00aad4"
              strokeWidth={0.200312}
            />
            <path
              id="path21314-3"
              d="M37.899 192.024v1.78H26.99v-1.78z"
              opacity={0.8}
              fill="#00aad4"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.270336}
              filter="url(#filter21336-5)"
            />
          </g>
          <g id="g6676" transform="translate(10.87 -117.783)" display="inline" strokeOpacity={1}>
            <circle
              transform="matrix(1.0715 0 0 1.0715 151.513 129.649)"
              clipPath="url(#clipPath835-87-6-6)"
              r={19.377043}
              cy={105.63503}
              cx={64.011055}
              id="path829-8-9-1"
              display="inline"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#3addfa"
              strokeWidth={0.527302}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={5.65}
              paintOrder="markers stroke fill"
            />
            <path
              id="path847-0-3"
              d="M194.71 248.77l-2.026.254c-.44-2.785-.602-5.57-.1-8.353l-1.013-.152c.273-2.826 1.192-5.266 2.126-7.695l2.38 1.012c-1.115 3.99-2.11 8.16-1.368 14.935z"
              display="inline"
              fill="#168198"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              id="path849-1-0"
              d="M212.782 268.059l-.556 2.278c1.539.534 3.236.816 5.113.81l.1-.912c4.896.081 9.75-.21 14.378-2.53l.608 1.012c7.14-2.813 11.056-8.473 14.124-14.884l-.658-.253c-.902 2.253-2.026 4.348-3.594 6.126l-1.924-1.367c-7.287 8.816-16.942 10.72-27.59 9.72z"
              display="inline"
              fill="#168198"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.264999}
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path
              id="path853-1-6"
              d="M227.92 218.649l.759-2.633c10.39 2.603 15.771 10.216 19.035 19.946l-.76.152a36.804 36.804 0 00-6.123-11.116l-1.673 1.548z"
              display="inline"
              fill="#168198"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <circle
              r={22.910192}
              cy={243.16536}
              cx={220.05162}
              id="path839-1-6"
              display="inline"
              opacity={0.75}
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#fc0"
              strokeWidth={0.564999}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray=".564999,3.39"
              strokeDashoffset={0}
              paintOrder="markers stroke fill"
            />
            <ellipse
              ry={25.273825}
              rx={25.273794}
              cy={243.27106}
              cx={219.90883}
              id="path843-5-8"
              display="inline"
              fill="none"
              fillOpacity={0.996078}
              fillRule="evenodd"
              stroke="#168498"
              strokeWidth={1.065}
              strokeLinecap="square"
              strokeLinejoin="round"
              strokeMiterlimit={4}
              strokeDasharray="none"
              strokeDashoffset={0}
              paintOrder="markers stroke fill"
            />
            <path
              id="path1180-1"
              d="M192.583 240.67l-1.012-.151c.221-2.62 1.112-5.164 2.126-7.695l1.152.466c-1.15 2.385-1.775 4.741-2.266 7.38z"
              fill="#bf9900"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              d="M245.89 253.58c-1.021 1.937-2.121 4.037-3.617 6.129l.703.507c1.209-1.756 2.401-3.853 3.573-6.383z"
              id="path1184-4"
              fill="#bf9900"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              id="path1194-0"
              d="M217.44 270.235l-.101.912c-1.758.1-3.444-.293-5.113-.81l.204-.834c1.645.347 3.54.742 5.01.732z"
              fill="#bf9900"
              fillOpacity={1}
              stroke="none"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
          </g>
          <ellipse
            id="path8847"
            cx={231.39238}
            cy={124.91325}
            rx={25.360931}
            ry={25.427353}
            display="inline"
            opacity={0.4}
            fill="url(#linearGradient8863)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01038}
            strokeLinejoin="round"
            strokeDasharray="1.01038,1.01038"
            paintOrder="stroke fill markers"
          />
          <path
            id="path8839"
            d="M258.58 39.62l-20.805-20.835-33.017-.094-4.554 4.053v1.886l4.45 3.582v24.606l-1.862 1.885v2.357l6.83 6.693v3.583l-2.69 2.545v4.148h16.353l3.209-2.45 5.589 5.184h20.493l5.693-5.373z"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient8841)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.29065}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M3.387 39.76L24.19 18.925l33.017-.094 4.554 4.054v1.885l-4.45 3.583v24.605l1.862 1.886V57.2l-6.83 6.693v3.583l2.69 2.545v4.148H38.681l-3.209-2.451-5.589 5.185H9.39L3.697 71.53z"
            id="path3290"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient3292)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.29065}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M219.3 172.032l4.282-2.176 31.213.064 2.778 1.318v6.624l-4.224 2.038h-34.16z"
            id="path15362"
            display="inline"
            opacity={0.3}
            fill="url(#linearGradient15377)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.445942}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path6170"
            d="M362.99 24.233l-3.422.036-3.176 1.378 3.347-.035zm-4.306.045l-3.897.041-3.176 1.378 3.821-.04zm-4.78.05l-3.652.039-3.175 1.378 3.577-.037zm-4.532.048l-3.654.038-3.176 1.379 3.579-.038zm16.096.044c-.01-.007.01.008.028.024-.01-.008-.017-.016-.028-.024zm-20.63.003l-3.484.037-3.175 1.378 3.409-.036zm-4.364.046l-3.368.035-3.175 1.378 3.293-.034zm-4.25.044l-3.427.036-3.175 1.379 3.352-.035zm-4.308.046l-3.01.031-3.176 1.379 2.936-.031zm32.825.009c.008.006.026.014.032.02l.005-.02h-.036zm-36.717.031l-3.565.038-3.174 1.378 3.488-.036zm-4.44.047l-3.314.035-3.174 1.378 3.237-.034zm-4.194.044l-3.37.035-3.175 1.379 3.294-.035zm-4.25.045l-3.358.035-3.175 1.378 3.281-.034zm-4.238.044l-3.144.033-3.175 1.379 3.067-.033zm-4.027.043l-2.831.03-3.175 1.378 2.755-.029zm-3.71.04l-2.671.027-3.175 1.378 2.594-.027zm-3.552.036l-2.684.029-3.174 1.378 2.607-.027zm-3.565.038l-2.607.027-3.175 1.378 2.532-.026zm67.659.413l-.013.005.014-.001v-.004z"
            display="inline"
            opacity={1}
            fill="url(#linearGradient4374)"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.403902}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="estado_eq"
            display="inline"
            fill={led}
            fillOpacity={1}
            fillRule="evenodd"
            strokeWidth={0.262189}
            d="M99.320915 12.923059H157.356064V30.973993H99.320915z"
          />
          <path
            id="color_st"
            display="inline"
            opacity={1}
            fill="url(#linearGradient1239)"
            fillOpacity={1}
            fillRule="evenodd"
            strokeWidth={0.26247}
            d="M99.320915 12.923063H157.48078900000002V30.973997000000004H99.320915z"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={103.38667}
            y={24.652231}
            id="nom_off"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.70997px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.364122}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={303.13593}
            y={22.552736}
            id="text3596"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.52777px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan3594"
              x={303.13593}
              y={22.552736}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'DATOS GENERALES'}
            </tspan>
          </text>
          <text
            id="text3600"
            y={42.393055}
            x={288.61978}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              style={{}}
              y={42.393055}
              x={288.61978}
              id="tspan3598"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'FASE:'}
            </tspan>
            <tspan
              style={{}}
              y={50.330555}
              x={288.61978}
              id="tspan3602"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'SISTEMA:'}
            </tspan>
            <tspan
              style={{}}
              y={58.268055}
              x={288.61978}
              id="tspan2416"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'MARCA:'}
            </tspan>
            <tspan
              style={{}}
              y={66.205559}
              x={288.61978}
              id="tspan3606"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'MODELO:'}
            </tspan>
            <tspan
              style={{}}
              y={74.143059}
              x={288.61978}
              id="tspan3608"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'UBICACI\xD3N:'}
            </tspan>
          </text>
          <text
            id="text3705"
            y={111.02153}
            x={314.72089}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.52777px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={111.02153}
              x={314.72089}
              id="tspan3703"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ALARMAS'}
            </tspan>
          </text>
          <text
            id="text5731"
            y={50.119507}
            x={433.84299}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="3.52777px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={50.119507}
              x={433.84299}
              id="tspan5729"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ESTADOS'}
            </tspan>
          </text>
          <text
            transform="scale(1.05085 .95161)"
            id="volt"
            y={51.168686}
            x={22.066082}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="8.46667px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.289184}
          >
            <tspan
              style={{}}
              y={51.168686}
              x={22.066082}
              id="tspan5844"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={700}
              fontStretch="normal"
              fontSize="9.87777px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.289184}
            >
              {vol}
              <animate
                attributeName="fill"
                from="#fff"
                to={st_alerta_vol[0]}
                dur={st_alerta_vol[1]}
                repeatCount="indefinite"
              />
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              textAlign: 'center',
            }}
            x={238.41194}
            y={176.5748}
            id="st"
            fontStyle="normal"
            fontWeight={400}
            fontSize="3.52777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            textAnchor="middle"
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              dy={0}
              id="tspan5848"
              x={238.41194}
              y={176.5748}
              style={{
                textAlign: 'center',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="7.05556px"
              fontFamily="Franklin Gothic Medium"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {st}
            </tspan>
          </text>
          <text
            transform="scale(1.05085 .95161)"
            id="curr"
            y={134.85446}
            x={219.50011}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="12.4912px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.289184}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={134.85446}
              x={219.50011}
              id="tspan5927"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={700}
              fontStretch="normal"
              fontSize="12.4912px"
              fontFamily="Franklin Gothic Medium"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.289184}
            >
              {cur}
              <animate
                attributeName="fill"
                from="#fff"
                to={st_alerta_cur[0]}
                dur={st_alerta_cur[1]}
                repeatCount="indefinite"
              />
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={219.8568}
            y={93.529266}
            id="text5947"
            fontStyle="normal"
            fontWeight={400}
            fontSize="3.52777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan5945"
              x={219.8568}
              y={93.529266}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.5861px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'CORRIENTE'}
            </tspan>
          </text>
          <path
            d="M282.02 191.317h111.8"
            id="path6140"
            display="inline"
            fill="none"
            stroke="#00aad4"
            strokeWidth={0.598704}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M204.218 186.907h43.27"
            id="path6144"
            display="inline"
            fill="#e3ff00"
            fillOpacity={1}
            stroke="#b37c05"
            strokeWidth={0.2195}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".2195,.2195"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path6170-7"
            d="M364.704 112.346l-3.422.036-3.175 1.378 3.346-.035zm-4.306.045l-3.897.04-3.175 1.38 3.82-.04zm-4.78.05l-3.652.038-3.175 1.379 3.577-.038zm-4.532.048l-3.654.038-3.176 1.378 3.579-.037zm16.097.044c-.011-.007.009.008.027.024-.01-.008-.016-.017-.027-.024zm-20.63.003l-3.485.036-3.175 1.379 3.409-.036zm-4.365.046l-3.368.035-3.175 1.378 3.293-.034zm-4.25.044l-3.427.036-3.174 1.378 3.35-.035zm-4.307.045l-3.012.032-3.175 1.378 2.936-.03zm32.824.01c.008.005.026.013.032.02l.005-.02h-.036zm-36.717.031l-3.564.037-3.175 1.379 3.488-.037zm-4.44.047l-3.314.035-3.174 1.378 3.237-.034zm-4.194.044l-3.37.035-3.175 1.378 3.294-.034zm-4.25.044l-3.358.036-3.175 1.378 3.281-.035zm-4.238.045l-3.144.033-3.175 1.378 3.067-.032zm-4.027.043l-2.831.03-3.175 1.378 2.755-.03zm-3.71.039l-2.67.028-3.175 1.378 2.593-.027zm-3.552.037l-2.683.028-3.175 1.379 2.607-.028zm-3.565.038l-2.607.027-3.175 1.378 2.532-.026zm67.659.413l-.013.005.014-.001v-.004z"
            display="inline"
            opacity={1}
            fill="url(#linearGradient4374-9)"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.403902}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="path6170-7-8"
            d="M482.705 54.054l-3.052-.036-2.833-1.378 2.985.035zm-3.84-.045l-3.477-.04-2.832-1.38 3.408.04zm-4.264-.05l-3.258-.038-2.832-1.379 3.19.038zm-4.043-.048l-3.26-.038-2.832-1.378 3.192.037zm14.358-.044c-.01.007.008-.008.024-.024-.008.008-.014.017-.024.024zm-18.401-.003l-3.109-.036-2.832-1.379 3.04.036zm-3.894-.046l-3.004-.035-2.832-1.378 2.937.034zm-3.79-.044l-3.057-.036-2.832-1.378 2.99.035zm-3.843-.045l-2.686-.032-2.832-1.378 2.619.03zm29.28-.01c.006-.005.022-.013.028-.02.003.008.003.014.004.02h-.033zm-32.752-.031l-3.18-.037-2.83-1.379 3.11.037zm-3.96-.047l-2.956-.035-2.832-1.378 2.888.034zm-3.741-.044l-3.007-.035-2.831-1.378 2.938.034zm-3.792-.044l-2.994-.036-2.832-1.378 2.927.035zm-3.78-.045l-2.803-.033-2.832-1.378 2.735.032zm-3.591-.043l-2.526-.03-2.831-1.378 2.457.03zm-3.31-.039l-2.382-.028-2.831-1.378 2.313.027zm-3.168-.037l-2.393-.028-2.832-1.379 2.326.028zm-3.18-.038l-2.325-.027-2.832-1.378 2.258.026zm60.35-.413l-.011-.005.012.001v.004z"
            display="inline"
            opacity={1}
            fill="url(#linearGradient4374-9-7)"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.381463}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <text
            id="text3705-8"
            y={185.65208}
            x={219.57692}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="3.52777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={185.65208}
              x={219.57692}
              id="tspan3703-1"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.9389px"
              fontFamily="BankGothic Lt BT"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'ESTADO'}
            </tspan>
          </text>
          <path
            d="M204.064 173.212l-.032-4.977 5.663.002"
            id="path15285-8-5-8"
            display="inline"
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.678307}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path6142-3"
            d="M203.67 184.592h14.784"
            display="inline"
            fill="none"
            stroke="#00aad4"
            strokeWidth={0.300305}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="1.20122,.300305"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <g
            transform="matrix(.52638 0 0 .48302 172.552 163.542)"
            id="g15358"
            display="inline"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          >
            <path
              d="M88.487 12.957l7.519.05-7.356 4.114z"
              id="path15352"
              fill="#17d8fb"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.964112}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              d="M89.194 17.992l8.133-4.504 59.298.132 5.214-.072.095 20.77-8.055-.036H88.982z"
              id="path15354"
              fill="none"
              stroke="#0deff7"
              strokeWidth={0.8844}
              strokeLinecap="butt"
              strokeLinejoin="miter"
            />
            <path
              d="M89.27 30.147c-.136 0-.246.072-.246.16v3.618c0 .088.11.16.246.16h14.66l7.759.185-4.853-4.123-.072.047a.32.32 0 00-.173-.047z"
              id="path15356"
              opacity={1}
              fill="#17d8fb"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.96697}
              strokeLinejoin="round"
            />
          </g>
          <path
            d="M250.642 32.314l.102-4.525-8.863-9.15-4.483-.017z"
            id="path14959"
            display="inline"
            fill="url(#linearGradient14967)"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".287896px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M203.12 26.799l-.288 27.85 1.969-1.878V28.166z"
            id="path14961"
            display="inline"
            fill="url(#linearGradient1486)"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".287896px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M386.1 38.143a22.25 22.248 0 01.495.026l-.025 4.723a17.518 17.517 0 00-.47-.017 17.518 17.517 0 00-17.519 17.516 17.518 17.517 0 006.337 13.466l-2.697 3.896a22.25 22.248 0 01-8.371-17.362 22.25 22.248 0 0122.25-22.248zm1.66.087a22.25 22.248 0 0112.97 5.444l-3.744 3.013a17.518 17.517 0 00-9.25-3.718zm13.831 6.248a22.25 22.248 0 016.125 10.75l-4.823.209a17.518 17.517 0 00-5.013-7.972zm6.384 11.907a22.25 22.248 0 01.374 4.006 22.25 22.248 0 01-1.586 8.153l-4.246-2.106a17.518 17.517 0 001.101-6.047 17.518 17.517 0 00-.431-3.8zm-5.898 11.137l4.225 2.094a22.25 22.248 0 01-7.292 8.852l-2.52-4a17.518 17.517 0 005.587-6.946zm-26.232 7.045a17.518 17.517 0 009.558 3.314l-.212 4.711a22.25 22.248 0 01-12.04-4.133zm19.675.55l2.536 4.025a22.25 22.248 0 01-11.7 3.487l.214-4.745a17.518 17.517 0 008.95-2.767z"
            id="path884-8-0"
            clipPath="url(#clipPath1984)"
            transform="matrix(.69053 0 0 .62532 -35.892 11.993)"
            display="inline"
            opacity={0.75}
            fill="#fc0"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.939752}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            d="M258.409 39.776l-20.794-20.702-33-.094-4.552 4.028v1.874l4.449 3.56V52.89l-1.863 1.873v2.342l6.828 6.65v3.56l-2.69 2.53v4.121h16.345l3.207-2.435 5.587 5.152h20.483l5.69-5.34z"
            id="path829-0"
            display="inline"
            opacity={0.75}
            fill="none"
            fillOpacity={1}
            stroke="#04e6f4"
            strokeWidth={0.289637}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M251.064 32.563l.103-4.497-9-9.092-4.552-.016z"
            id="path831-8"
            display="inline"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".289184px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M202.805 27.083l-.293 27.672 2-1.865V28.44z"
            id="path833-1"
            display="inline"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".289184px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g6103"
            transform="translate(-4.347 -26.107)"
            display="inline"
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              id="path852-9"
              d="M260.444 88.374l-12.62 12.458"
              display="inline"
              strokeWidth={0.61753}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <path id="path852-0-1" d="M255.17 89.828l-6.212 5.99" display="inline" strokeWidth=".289183px" />
            <path id="path852-0-7-2" d="M257.861 94.466l-4.455 4.302" display="inline" strokeWidth=".289183px" />
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              textAlign: 'center',
            }}
            x={218.70462}
            y={53.982666}
            id="fp_tot"
            transform="scale(1.05085 .95161)"
            fontStyle="normal"
            fontWeight={400}
            fontSize="5.89545px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            textAnchor="middle"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.289184}
          >
            <tspan
              id="tspan1960-4"
              x={218.70462}
              y={53.982666}
              style={{
                textAlign: 'center',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={700}
              fontStretch="normal"
              fontSize="9.87777px"
              fontFamily="Franklin Gothic Medium"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.289184}
            >
              {fac}
            </tspan>
          </text>
          <path
            d="M386.207 36.256a23.653 23.65 0 00-23.653 23.651 23.653 23.65 0 0023.653 23.65 23.653 23.65 0 0023.652-23.65 23.653 23.65 0 00-23.652-23.65zm0 3.546a20.179 20.106 0 0120.178 20.105 20.179 20.106 0 01-20.178 20.106 20.179 20.106 0 01-20.18-20.106 20.179 20.106 0 0120.18-20.105z"
            id="path2051"
            clipPath="url(#clipPath2072)"
            transform="matrix(.87806 0 0 .79515 -108.61 2.269)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.739048}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <path
            id="rect2077"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 54.6856H215.3636607V57.8733716H207.97127z"
          />
          <path
            id="rect2077-1"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 50.368729H215.3636607V53.5565003H207.97127z"
          />
          <path
            id="rect2077-7"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 46.051842H215.3636607V49.2396133H207.97127z"
          />
          <path
            id="rect2077-1-2"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 41.734962H215.3636607V44.922733300000004H207.97127z"
          />
          <path
            id="rect2077-9"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 37.41806H215.3636607V40.6058313H207.97127z"
          />
          <path
            id="rect2077-1-5"
            display="inline"
            opacity={0.75}
            fill="#10677d"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.61753}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M207.97127 33.101196H215.3636607V36.2889673H207.97127z"
          />
          <text
            id="text21805"
            y={24.757998}
            x={214.56966}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="3.52777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={24.757998}
              x={214.56966}
              id="tspan21803"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.5861px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'FP TOTAL'}
            </tspan>
          </text>
          <path
            d="M240.134 33.078l2.14-2.071-5.196-4.952H208.25l-3.769-4.592"
            id="path15126"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.281789}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".84537,.84537"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            id="path15379"
            d="M204.406 178.423l-.032 4.977 5.663-.002"
            display="inline"
            fill="none"
            stroke="#0deff7"
            strokeWidth={0.678307}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M3.666 39.592l20.476-20.834 32.496-.095 4.483 4.054v1.886l-4.38 3.582v24.606l1.833 1.885v2.357l-6.724 6.693v3.583l2.649 2.545v4.148H38.403l-3.157-2.45-5.501 5.184H9.575L3.97 71.363z"
            id="path829"
            display="inline"
            opacity={0.75}
            fill="none"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth={0.288348}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M10.899 32.333l-.102-4.525 8.862-9.15 4.483-.017z"
            id="path831"
            display="inline"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth=".287896px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M58.42 26.818l.289 27.85-1.969-1.877V28.185z"
            id="path833"
            display="inline"
            opacity={0.75}
            fill="#0cedf7"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth=".287896px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M8.372 61.97L20.8 73.946"
            id="path852"
            display="inline"
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth={0.600797}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M13.564 63.369l6.118 5.757"
            id="path852-0"
            display="inline"
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth=".281347px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M10.915 67.826l4.387 4.135"
            id="path852-0-7"
            display="inline"
            fill="#168498"
            fillOpacity={1}
            stroke="#0deff7"
            strokeWidth=".281347px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M18.703 33.935l-2.14-2.07 5.196-4.952h28.83l3.768-4.592"
            id="path975"
            display="inline"
            opacity={0.75}
            fill="none"
            stroke="#fcfcfc"
            strokeWidth={0.281789}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray=".84537,.84537"
            strokeDashoffset={0}
            strokeOpacity={1}
          />
          <path
            d="M132.164 101.266a17.845 17.845 0 00-.397.02l.02 3.789a14.05 14.05 0 01.377-.014 14.05 14.05 0 0114.051 14.05 14.05 14.05 0 01-5.082 10.801l2.163 3.126a17.845 17.845 0 006.714-13.927 17.845 17.845 0 00-17.846-17.845zm-1.331.07a17.845 17.845 0 00-10.402 4.366l3.003 2.417a14.05 14.05 0 017.418-2.982zm-11.093 5.011a17.845 17.845 0 00-4.913 8.624l3.869.166a14.05 14.05 0 014.02-6.394zm-5.12 9.55a17.845 17.845 0 00-.3 3.214 17.845 17.845 0 001.272 6.54l3.405-1.69a14.05 14.05 0 01-.883-4.85 14.05 14.05 0 01.346-3.048zm4.73 8.934l-3.389 1.68a17.845 17.845 0 005.849 7.1l2.021-3.208a14.05 14.05 0 01-4.48-5.572zm21.04 5.65a14.05 14.05 0 01-7.667 2.66l.17 3.778a17.845 17.845 0 009.658-3.315zm-15.78.442l-2.035 3.228a17.845 17.845 0 009.384 2.798l-.172-3.806a14.05 14.05 0 01-7.178-2.22z"
            id="path884-8-0-4"
            clipPath="url(#clipPath1163)"
            transform="matrix(.9957 0 0 .96932 -100.842 -68.299)"
            display="inline"
            opacity={0.75}
            fill="#168498"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.611547}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={29.685022}
            y={25.452135}
            id="text21801"
            fontStyle="normal"
            fontWeight={400}
            fontSize="3.52777px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan21799"
              x={29.685022}
              y={25.452135}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="4.5861px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'VOLTAJE'}
            </tspan>
          </text>
          <path
            id="path2477"
            d="M29.505 35.167a12.042 12.042 0 00-1.47.238l.436 1.429a10.554 10.554 0 011.07-.174zm-2.013.375a12.042 12.042 0 00-1.477.512l.767 1.295a10.554 10.554 0 011.106-.372zm-1.994.745a12.042 12.042 0 00-1.565.898l1.017 1.113a10.554 10.554 0 011.274-.707zm-2.025 1.227a12.042 12.042 0 00-1.41 1.24l1.27.835a10.554 10.554 0 011.117-.947zm-1.797 1.656a12.042 12.042 0 00-1.165 1.553l.08-.087 1.273.735a10.554 10.554 0 011.043-1.343zm-1.394 1.932a12.042 12.042 0 00-.832 1.738l1.436.413a10.554 10.554 0 01.664-1.372zm-1.035 2.31a12.042 12.042 0 00-.445 1.842l1.48.176a10.554 10.554 0 01.409-1.644zm-.525 2.441a12.042 12.042 0 00-.07 1.298 12.042 12.042 0 00.014.591l1.485-.118a10.554 10.554 0 01-.01-.473 10.554 10.554 0 01.063-1.161zm1.47 2.332l-1.48.158a12.042 12.042 0 00.36 1.958l1.407-.499a10.554 10.554 0 01-.288-1.617zm.444 2.158l-1.392.536a12.042 12.042 0 00.68 1.657l1.285-.76a10.554 10.554 0 01-.573-1.433zm.832 1.93l-1.262.794a12.042 12.042 0 00.958 1.445l1.178-.912a10.554 10.554 0 01-.874-1.326zm1.23 1.763l-1.154.943a12.042 12.042 0 001.4 1.388l.872-1.213a10.554 10.554 0 01-1.119-1.118zm14.464 1.453a10.554 10.554 0 01-1.608 1.03l.711 1.308a12.042 12.042 0 001.78-1.138zm-12.906.023l-.838 1.233a12.042 12.042 0 001.506.975l.791-1.264a10.554 10.554 0 01-1.46-.944zm1.966 1.197l-.76 1.288a12.042 12.042 0 002.18.802l.387-1.437a10.554 10.554 0 01-1.807-.653zm8.837.05a10.554 10.554 0 01-1.799.628l.435 1.426a12.042 12.042 0 002.086-.747zm-6.48.731l-.348 1.448a12.042 12.042 0 002.04.247l.064-1.487a10.554 10.554 0 01-1.756-.208zm4.131.02a10.554 10.554 0 01-1.81.192l-.025 1.49a12.042 12.042 0 002.232-.244z"
            display="inline"
            fill="#c29f05"
            fillOpacity={0.988235}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.264999}
            strokeLinecap="square"
            strokeLinejoin="round"
            paintOrder="markers stroke fill"
          />
          <path
            d="M27.83 36.119c11.976-5.163 23.5 14.12 8.675 21.221"
            id="path2639"
            display="inline"
            fill="none"
            stroke="none"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path2446"
            d="M391.757 9.679h18.007l4.945-4.878 67.485-.133 4.945 8.953h18.174"
            display="inline"
            fill="none"
            stroke="#00bec4"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            id="fase"
            y={40.462254}
            x={329.01462}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              style={{}}
              y={40.462254}
              x={329.01462}
              id="tspan3614"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'B'}
            </tspan>
          </text>
          <text
            id="sistema"
            y={48.651157}
            x={329.01462}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              y={48.651157}
              x={329.01462}
              id="tspan3618-0"
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {sistema}
            </tspan>
          </text>
          <text
            id="marca"
            y={57.114574}
            x={329.01462}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              y={57.114574}
              x={329.01462}
              id="tspan3618-1"
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.470567}
            >
              {'ENTELLIGUARD'}
            </tspan>
          </text>
          <text
            id="modelo"
            y={65.246124}
            x={329.01462}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              style={{}}
              y={65.246124}
              x={329.01462}
              id="tspan3614-5"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'POWER BREAK II'}
            </tspan>
          </text>
          <text
            id="ubicacion"
            y={73.875504}
            x={329.01462}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              style={{}}
              y={73.875504}
              x={329.01462}
              id="tspan3614-8"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {ubicacion}
            </tspan>
          </text>
          <text
            id="text3659"
            y={128.74823}
            x={288.62207}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            transform="scale(.9853 1.01493)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.27422px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.470567}
          >
            <tspan
              id="tspan1627"
              style={{}}
              y={128.74823}
              x={288.62207}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'GENERAL:'}
            </tspan>
            <tspan
              style={{}}
              y={144.43378}
              x={288.62207}
              id="tspan2982"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.470567}
            >
              {'MODBUS STATUS:'}
            </tspan>
          </text>
          <ellipse
            ry={2.4136531}
            rx={2.5725989}
            cy={128.66548}
            cx={364.80881}
            id="alam_gen"
            display="inline"
            opacity={0.88}
            fill="#1bea77"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.501687}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <animate attributeName="fill" from="#4d4d4d" to={alm[0]} dur={alm[1]} repeatCount="indefinite" />
          </ellipse>
          <ellipse
            transform="matrix(.74208 0 0 .6085 35.223 106.193)"
            ry={1.5367997}
            rx={1.8875909}
            cy={34.583458}
            cx={444.15182}
            id="path2448-1"
            display="inline"
            opacity={0.592}
            fill="#fff"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.746581}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2963)"
          />
          <text
            transform="scale(.84337 1.18572)"
            id="text825-6-5"
            y={51.097401}
            x={479.90582}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="6.25586px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.271989}
          >
            <tspan
              id="tspan5817"
              style={{}}
              y={59.257092}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'VOLT. AB:'}
            </tspan>
            <tspan
              id="tspan5821"
              style={{}}
              y={67.416779}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'VOLT. BC:'}
            </tspan>
            <tspan
              id="tspan3048"
              style={{}}
              y={75.576469}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'VOLT. CA:'}
            </tspan>
            <tspan
              id="tspan2427"
              style={{}}
              y={83.736153}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'CUR A:'}
            </tspan>
            <tspan
              id="tspan3050"
              style={{}}
              y={91.895836}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'CUR B:'}
            </tspan>
            <tspan
              id="tspan6734"
              style={{}}
              y={100.05553}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'CUR C:'}
            </tspan>
            <tspan
              id="tspan1629"
              style={{}}
              y={108.21522}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'POTENCIA REAL TOT:'}
            </tspan>
            <tspan
              id="tspan1631"
              style={{}}
              y={116.3749}
              x={479.90582}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="6.52775px"
              fontFamily="Franklin Gothic Medium"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={0.271989}
            >
              {'POTENCIA APPT TOT:'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={568.1593}
            y={97.433777}
            id="cur_c"
            transform="scale(.8204 1.21891)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={568.1593}
              y={97.433777}
              style={{}}
              id="tspan15409"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {cur_c} A
            </tspan>
          </text>
          <g transform="translate(-1.681 .994)" id="st2" display="inline" fill="red" fillOpacity={1}>
            <g
              id="ats_st2"
              transform="translate(-.035 -.19)"
              display="inline"
              fill={led}
              fillOpacity={1}
              stroke="none"
              strokeOpacity={1}
            >
              <path
                d="M213.683 169.146a5.86 5.86 0 00-4.066 1.662 5.7 5.7 0 00-1.684 4.01 5.7 5.7 0 001.684 4.01 5.86 5.86 0 004.066 1.662 5.86 5.86 0 004.066-1.662 5.7 5.7 0 001.685-4.01 5.7 5.7 0 00-1.685-4.01 5.86 5.86 0 00-4.066-1.662zm0 .835a5.03 5.03 0 013.475 1.42 4.877 4.877 0 011.435 3.417 4.879 4.879 0 01-1.435 3.418 5.03 5.03 0 01-3.475 1.419 5.03 5.03 0 01-3.475-1.42 4.879 4.879 0 01-1.435-3.417c0-1.259.531-2.526 1.435-3.418a5.03 5.03 0 013.475-1.42z"
                id="path15781"
                strokeWidth={0.0168493}
                strokeLinejoin="round"
                strokeMiterlimit={4}
                strokeDasharray=".0336987,.0168493"
                strokeDashoffset={0}
              />
              <path
                transform="matrix(.14885 0 0 .14885 123.933 125.484)"
                id="path2489"
                d="M601.873 320.96c.063-1.311 2.784-1.358 2.69.07-.033.121 0 9.402 0 9.402-.347 1.077-2.153 1.292-2.713 0z"
                strokeWidth=".264583px"
                strokeLinecap="butt"
                strokeLinejoin="miter"
                filter="url(#filter2703)"
              />
              <path
                transform="matrix(.14885 0 0 .14885 123.933 125.484)"
                id="path2649"
                d="M597.565 325.633c1.698.334 1.59 1.335 1.404 2.364-1.342 1.49-2.997 2.762-2.868 5.287.434 2.218.972 4.389 3.417 5.673 2.377 1.244 4.814 1.155 6.562.177 1.889-1.079 3.77-3.085 3.728-5.85-.088-2.4-1.275-4.226-3.313-5.607.085-.983-.099-2.122 2.023-1.917 2.272 1.959 4.04 4.21 3.95 7.536-.001 3.557-1.8 6.426-5.654 8.51-2.77 1.343-5.445.865-8.09-.168-2.57-1.48-5.569-3.356-5.217-9.528.748-3.306 2.154-5.363 4.058-6.477z"
                strokeWidth=".264583px"
                strokeLinecap="butt"
                strokeLinejoin="miter"
                filter="url(#filter2663)"
              />
            </g>
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={30.988453}
            y={55.318169}
            id="text9359"
            fontStyle="normal"
            fontWeight={400}
            fontSize="6.35px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan9357"
              x={30.988453}
              y={55.318169}
              style={{}}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={700}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'V'}
            </tspan>
          </text>
          <text
            transform="scale(.8204 1.21891)"
            id="pow_appt"
            y={113.35474}
            x={568.20898}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan6736"
              style={{}}
              y={113.35474}
              x={568.20898}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {pow_appt} KVA
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              textAlign: 'center',
            }}
            x={128.12933}
            y={24.87211}
            id="text1545"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.70997px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            textAnchor="middle"
            display="inline"
            fill="#000"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.364122}
          >
            <tspan
              id="tspan1543"
              x={128.12933}
              y={24.87211}
              style={{
                textAlign: 'center',
              }}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87777px"
              fontFamily="Franklin Gothic Medium"
              textAnchor="middle"
              fill="#000"
              fillOpacity={1}
              strokeWidth={0.364122}
            >
              {nom_on}
            </tspan>
          </text>
          <text
            id="nom_on"
            y={25.930445}
            x={128.12933}
            style={{
              lineHeight: 1.25,
              textAlign: 'center',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="9.70997px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            textAnchor="middle"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.364122}
          >
            <tspan
              style={{
                textAlign: 'center',
              }}
              y={25.930445}
              x={128.12933}
              id="tspan6769"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="9.87777px"
              fontFamily="Franklin Gothic Medium"
              textAnchor="middle"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.364122}
            >
              {nom_on}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={568.20898}
            y={105.39427}
            id="pow_real"
            transform="scale(.8204 1.21891)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={568.20898}
              y={105.39427}
              style={{}}
              id="tspan6736-3"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {pow_real} KW
            </tspan>
          </text>
          <text
            transform="scale(.8204 1.21891)"
            id="cur_b"
            y={89.473236}
            x={568.1593}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan15409-5"
              style={{}}
              y={89.473236}
              x={568.1593}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {cur_b} A
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={568.1593}
            y={81.57058}
            id="cur_a"
            transform="scale(.8204 1.21891)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={568.1593}
              y={81.57058}
              style={{}}
              id="tspan15409-5-4"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {cur_a} A
            </tspan>
          </text>
          <text
            transform="scale(.8204 1.21891)"
            id="vol_vab"
            y={57.689144}
            x={568.1593}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan15409-5-4-2"
              style={{}}
              y={57.689144}
              x={568.1593}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {vol_a} V
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={568.1593}
            y={65.591736}
            id="vol_vbc"
            transform="scale(.8204 1.21891)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              x={568.1593}
              y={65.591736}
              style={{}}
              id="tspan1765"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {vol_b} V
            </tspan>
          </text>
          <text
            transform="scale(.8204 1.21891)"
            id="vol_vca"
            y={73.552231}
            x={568.1593}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="5.64444px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              id="tspan1765-8"
              style={{}}
              y={73.552231}
              x={568.1593}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="5.64444px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              strokeWidth={0.264583}
            >
              {vol_c} V
            </tspan>
          </text>
          <ellipse
            id="alm_modbus"
            cx={364.80865}
            cy={144.54027}
            rx={2.5725989}
            ry={2.4136531}
            display="inline"
            opacity={0.88}
            fill="#1bea77"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.501687}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <animate attributeName="fill" from="#4d4d4d" to={mod[0]} dur={mod[1]} repeatCount="indefinite" />
          </ellipse>
          <ellipse
            id="ellipse2986"
            cx={444.15182}
            cy={34.583458}
            rx={1.8875909}
            ry={1.5367997}
            transform="matrix(.74208 0 0 .6085 35.222 122.068)"
            display="inline"
            opacity={0.592}
            fill="#fff"
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.746581}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeDashoffset={0}
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter2963)"
          />
          <text
            id="text2997"
            y={136.58304}
            x={230.18196}
            style={{
              lineHeight: 1.25,
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontWeight={400}
            fontSize="6.35px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.264583}
          >
            <tspan
              style={{}}
              y={136.58304}
              x={230.18196}
              id="tspan2995"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={700}
              fontStretch="normal"
              fontSize="6.35px"
              fontFamily="Franklin Gothic Medium"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.264583}
            >
              {'A'}
            </tspan>
          </text>
          <path
            transform="translate(-12.144 -21.082)"
            clipPath="url(#clipPath40132)"
            d="M250.934 121.948l.76-2.633c10.39 2.603 15.771 10.216 19.035 19.946l-.76.152a36.804 36.804 0 00-6.123-11.116l-1.673 1.548z"
            id="path37087"
            display="inline"
            fill="#bf9900"
            fillOpacity={1}
            stroke="none"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g8008"
            transform="translate(-.778 3.635)"
            display="inline"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path
              d="M417.764 14.632l5.709-5.717h8.404l4.087 4.094h26.49l4.152-4.159h8.986l5.754 5.764v8.81l-5.487 5.496h-3.25l-2.486-2.49h-11.398l-2.4 2.405H442.72l-2.443-2.447h-11.383l-2.25 2.253h-3.2l-5.852-5.217z"
              id="path4700"
              opacity={0.3}
              fill="url(#linearGradient7984)"
              fillOpacity={1}
              stroke="none"
              strokeWidth={0.4}
              strokeMiterlimit={4}
              strokeDasharray="none"
            />
            <g transform="translate(-2.646 29.81)" id="g7995">
              <path
                id="path4702"
                d="M420.399-8.206h-.618l.163-7.09 5.679-5.81h3.407v.569h-3.184l-5.365 5.373z"
                fill="#0ff"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4704"
                d="M482.933-8.206h.618l-.163-7.09-5.679-5.81h-3.407v.569h3.184l5.365 5.373z"
                fill="#0ff"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4706"
                d="M419.94-15.418l5.708-5.717h8.404l4.088 4.094h26.49l4.152-4.159h8.985l5.755 5.764v8.81l-5.488 5.496h-3.25l-2.486-2.49h-11.397l-2.4 2.405h-13.606l-2.443-2.447H431.07L428.82-1.41h-3.2l-5.852-5.217z"
                fill="none"
                stroke="#168498"
                strokeWidth={0.4}
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
              <path
                id="path4708"
                d="M430.897-1.052h12.019l-1.176-1.265h-9.667z"
                fill="#fc0"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4710"
                d="M460.754-1.052h12.018l-1.176-1.265h-9.666z"
                fill="#fc0"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4712"
                d="M436.347-21.049h30.635l-2.811 2.816h-25.644z"
                fill="#0ff"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4714"
                d="M468.13-18.348l1.462-1.466h7.487l5.105 4.741v7.93L477.51-2.46l-2.209.086-1.663-1.609h3.556l3.615-3.62v-6.493l-4.102-4.109z"
                fill="#168498"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4716"
                d="M435.453-18.348l-1.463-1.466h-7.487l-5.105 4.741v7.93l4.675 4.683 2.209.086 1.663-1.609h-3.556l-3.615-3.62v-6.493l4.102-4.109z"
                fill="#168498"
                fillOpacity={1}
                stroke="none"
                strokeWidth=".264582px"
              />
              <path
                id="path4718"
                d="M419.94-15.418l5.708-5.717h8.404l4.088 4.094h26.49l4.152-4.159h8.985l5.755 5.764v8.81l-5.488 5.496h-3.25l-2.486-2.49h-11.397l-2.4 2.405h-13.606l-2.443-2.447H431.07L428.82-1.41h-3.2l-5.852-5.217z"
                fill="none"
                stroke="#168498"
                strokeWidth={0.4}
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
            </g>
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={376.8981}
            y={29.830231}
            id="text4728"
            transform="scale(1.14175 .87585)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="condensed"
            fontSize="8.661px"
            fontFamily="Franklin Gothic Medium Cond"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            opacity={1}
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.43534}
          >
            <tspan
              x={376.8981}
              y={29.830231}
              style={{
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              id="tspan4724"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="condensed"
              fontSize="8.661px"
              fontFamily="Franklin Gothic Medium Cond"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={0.43534}
            >
              <tspan
                id="tspan4733"
                style={{}}
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="10.3932px"
                fontFamily="Franklin Gothic Medium"
                strokeWidth={0.43534}
              >
                {'DC-UIO'}
              </tspan>
            </tspan>
          </text>
          <image
            id="image3237"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj8AAAHiCAMAAAAwOl1pAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5r c2NhcGUub3Jnm+48GgAAAAlwSFlzAAAOwwAADsMBx2+oZAAAAnxQTFRFR3BMZmVkqJ6VX15drKmm dnV0sKuk4+LhycnHvbq4WldVSUZDT1BQT0xKoqKbQUJBnZyUXltZoop7mpqSk5SMOjg2ooJskJGI o4h3k3JdjWpUpYZxjI2EkXFealZKl5iQlpaOkpOMnH1qooRyMjU1MTAvvcPBKi0th4iAtbq5LC4u emFSn6KcjI6Hh35zLTAwLjExLjEwLTAvICIhcLEiLzIxs7q4sLe2JScne4GAJikoIyUkISMjAAAA dXp4LC8veoB/kGdOpauoKy0to6ilc3h2d3x6jWRLr7Wzh4h+lGlQVVlYYmdmXGBfcHZ0rLOwUFRT X2NiYWVkWV1cqK2qoaaje4J+eH59TFBPPD8+QENCMjU0Zmtqn6SggoN7SUxLb3Rymp+ca3FvqrCt mW5Uam9tlG1TZGlobXJxQ0ZFKSsrRklIODs6mJyZaG1rNTg3h3BjimBIm3JYnXZcoXtghW1gfYSD lZqXiYqBnqGdnKKfgmpdf4x+fW5khYuJgYeGkZmXLjIxk5aSjJKQjHJljpWTgHJpiI+NeId4n6up ucC9mnxqiHVpCAkHiGxdpK+tioN4k3hndmxjpX9ngmdZm6ellJ2ccIBuDw8PhJeAl6KgjW9fjXtt hVpDkJCIf4B3andpgqNzenpyhnxykYByfJdxd3FqFRYWj7R5i6WBwcjHeVRAGxwcaWhimcN9o9B+ UDQkc4xqQise4fyJnoNzaqkgYnNfa007Y1pQDhkEemNWwet70fOEU4YYo5KHN1oPb19UcY+NRG8T s95/YJsc9f6cKkYMs+JeGCkGWGpTITcJa4NiW1FJndFGWz8uhcMxYoGAcpyXTWlo1fai733fhwAA AC90Uk5TAGhhfjZJTQYRIqXduMx67JSUc6rh8ubviuXy0PjSubzJ1MauotmEu/DDgdbhu/C5eAlB AAGG2UlEQVR42uy9+29b17Ut3LiOH02TpknTJidN2yQ4Fz04+A43t2DJtnhEk6ZMmUnMOCHjJA4V K6SZxLFsiZKs1ydZ1cuopeodo4hegGS4QBQDhmFAhX8w8gcE/pu+MeZca+1NSe6p7nfSk3vdTYlv kRTX2GOOOeZca//oR//c/rn9c/vn9s/tn9s/t3/Etn///gN2239g/yO3f35T/9x22A6+/EvZJnj2 kvnlhbl8GSf52bK99vJr4e2V117H+W9+85vn/k7UPvfci9u25/S0w7aHPziFtif37Nm3Z9++fU/q 75P7nnzyyb27Qfn+AwdD24GDB/iz8/Y396rHe8/aO9+Jbd78VDrlZkdHpaN8f17u6uiUraOTN/lA B653yNbZUSnhdrlUSWzMd5Qq5Tt37vzk7xy7zlJnuVIulfEylXKlUirhN5EpVUrlUqKcKScSHZ0b 6UqlUpYtg3sy5TIesT+JcjnNSzyTz07jOQ/vPrWbsdz30r9UbQv/sm17SX7w+5Js//7SS6Err+hp +/baK6/8+48fG/6Z75wX1AAgRAQv5gmRSvsC8TOPG/OKHcKnQ/CEMS8RSYQT70zfl+uJvxs/PzrQ qcAh/joESAAhgFIqJ0oZ4Ie46dzcIH4AKd2IILnkb0IuM3LG88Su8bOHf84/Bgzbl9NpvAZwiF9c SeNmhmdywtbOO9o3S+n29uX29vY0z5dxb3u73LLX7PWHP3l88NNRmRcGkhMR0qlYKS8vKDOBlcx9 RI48Vumo8If8gIvyJsFQ2QV+9h+Yr3QAguUS6atE7gJwCB/BR0bQUurYaK8IksqJStmiSJ4g8LHP SwgbJcq7xM9z+PMMyQtDLxyG10gngh8CiC+ekZvAEJ4jAGoXsNhfIM9da7cgevjzxwc/JkoJ93R2 Kj4ENeX7CwY3ErnMg5UK8SPnpJ9SpbO8gTtBJOXK388/B0lYiFnkMf5tSZEjgQlnaWGUSufmcpnk lDH0kxCUJQzh4JmZhBIRqSnzv4MfEkmaKOI789X0jNSTIHrkDdKy8cOlCaB0wDT4SfAlEg46vDvd fuexiV8HCJ+KBCiBx3yH1ThA0MICo1knYxhBQyYCgET6dMr487mJzXkCCvDZTfzC6zAE4iSUUill GM0kRGl84uABQO0lMk8pY4KXjWEKHsavTBoII+iAn6d3g58XEbba24WEhG3kNXgSBJFyMoaL0vLD K6X2zUqmPeAZx0YBeuT3zhOPD340Ys1b2rFCR4C0kFBpRIbio/NGPJOOKsJGnR2bgBWDWEX082/+ Xv7p7FDtDeHTAVKhcM4QKuQC3kthnCh1biwLnggYUlQlozdliEVn8yb+AKN+5+7Tu/nPX0TY0tdJ E4UGN2mlHxfLEgqedEYglCGA0mHIpC3lOPHD7THCzy+JDUmz5pVWOgU6gpeO0kbCYUqvVDoN7ZBy qH03OgU8ZRLJruIXQiD1E6QPBZCInxLgkckIqzCnwohqCLOcY1Sz5Z2Eoqec0Fj2cJf4kdwtQwGt YOQtxDINXo542oWe2kUBEVjLm2WJXIkQfvRsWWFF/Nx98rHCj4EQM62OjYd3/v3FX9p0rLxRRgom 9GMkEpmns7N9c/OVl4mcjc5OkdHU07vhn07+AdN1QlGCVxnZO2MSpQjoJE1KAY46LIAkcolYFrww Yxe5xKEnnsA/v9nNf/66vgBILqNJl2LIcA34B8DJlDX90vDVLshaFgay4DHEk0478PC+xwk/UDjz EqVw0ZG+c/fuTw786MCz85puASAVBZDNviCDIHTu3v3xgf17X+7YgHhSGQM5syv8iJdUqQiR0fvZ uPPUnoN7XqOermRUSoveqXRsppnFG+bBIG9s/mbPc68piviUtAihcma3+EmUDPQ0GErESrz0EkQ1 AdQuFCTXDXBMLCsLgBKBAAql76qo25fv7n1syhe/nDfimYBpBzB+IyL0GepkGeSNsj5oUnf8pO/e vSsBfv/rUN4mlafluDv8SN7VicgF8xBvDNjSl+4AD6mrI6SSyHR2btITKhsBvXH3X7lz79/3ipKG iCDBwG7xo6JcUSjZV/vmT/bClX7ulYSTPkYQtadt/MK1zPKGZGH/8vq+vfuee021T8JIH8XR3YOP D35sXKK4AQAe6r++/3UjkwmgeSORVDtXHMh+tP9l6wRZ/fN340fCIJ2kDpg7G2A9kzvtKZUzFcnQ JYhQRM9vthM6JcJk8+5TZmwOvuYkkSRPZUBwN//462JU0sdWqZXYeMFEnQMvioMILmHuLkBqT5vA RiEEBiqlN35jPse+V1wSljan5RcOPDb4ebmzQ/0fIGMDAPh5YCwKeiCRv2UO32HqHB2dmwDZXlf+ kDy+nOncpf5R2qKHWOrsAHU8dSDIq4VrRNpg1ICaEtN4AUpm8+4dFxoOviTMkymbOLY7/PzoNVFa lbLqdbz0C4FoeQ76S8OX2IeWf+j2EFCZjc3Nnziv4MBrLnyJT51OPFb4MXUJYughALDP8bvk6Eyy KveN/Sy5WSdRst/txRUGuoqWI3YXv0R4UziVNiGnAmv6pUympGWtsiTVZWogLVm0373783AFwjrR jG2ZXfLPa3iXjMn9JS4+ESanhDGBMuIkprWGIRkZMZTZ/GnIajrwSsKm8ZrKb/xs/+ODH2NAz8NF vPPQhi/ug0zBxPEpd9yvaFmMo54OkRTGsKLFMLGAdsU/RBw5rdSBkX94MJwYJVRAlzQ+4RoARKBU wvSDcXtJE3iiCGC4c3c3VYP9v1TDKVMSdzJRbV4f/JcM0ZCxqZc5JQwPbbxQJXD2pY0JlNFkrf2p x6co/7KmVsRQGfjZdMy7D5gS9inT5emwdfqOZaDE7ar79wr1UAtXdsk/tJ/LoryX71QN3h6UT8k7 JdXQHDa8/EZaQlTVyLyu6lf0TyJxd1f4+dEvSWyJskpz6Kpqy+91us+we9KSx2tYMjSEyzvV77T/ lbQJXny8VHn49I8eI/x0mOaMzo4q/tlHh7kiIggCZ6PTSCDBT9Dls7dDuaeTMmg3/rMU3ks0EEsb d++Ev/C9ZfpC6kCnlYGQxlc2MZh3q1Os54xvI/l7IhQE/x7+eUkyugqLI0Drwy0p03NUXomEUTQi iy1+UCnd6u+8GPjR6Uxpc/Pxwc/+1yV8Qc7inPrHfTN7eKfaxIANRLRJ35er4tde7eJQL/rh34uf /QeF20BtnaAgyPYq/GipQpRzhvUoIqncWdpMtG/Fj6ljSJkzvTv8HPhliXGL/Ub0lu7+rFry7qNw BtVk2jPGOJRSqTjP6Ydb9fGedtPkkU5X0puVzZ8+PvzzulZHKZ/nH4ah8aKRxZ2qrxHCRPN2lPCk oFC5BwX4kto/u+KfinQOVSQDQ/YeDkv71PyhCipJOVNENDhwE/h5egtJaP2TmVr7LvED+Z6mAw0Q I35trd3v06SLgifRblqA9C5cPtyqb/YlKH1gAmVKy5vzmcen/Qf8I40/UmOfJ7VsHnSJfaVDK15I sZHIl+93appPlDkV+3pFa2XiIu+Cf+al5KbONZRvWBa/SO1TSkghTEoWag2jSrZ5p5omXmejR0I6 FhOl9rt3d1O1PCBVE5jmmQpsnvJW/tlDTkubVo60iiGngbbhZ09CrehEZXmjA8Hw548P/zwvTYbS PDbPAbpjjI09Vld3aLsqEqaNefGBIHddAk8fECqY3UJAwsNd5F+dBj3Ez8Ow9N3/sgBHjRk5mYoq Ih2cyyfD+VfJ4gs/y7vDz8FOvKBW1lgAu7ul5PBiwpEPlY+NTwY/W+PXi+QlPA/tAh2Jx6n9B/tw p/T4SIm0swMjeefHxMbeX2qXjzrOFXb/dJQ3pUU6LU+yLrW6OGI1/v3xa//BinrPFWk9xNC/sDdw dah6lFQyofIE7mTl5Df7w4laWuMcBVP7bvFD7xIhspQpQeY8rA5+sAYyJlkvq3g29QmGqfbNrfr5 NS1sVDbbO/hnj0/7xo9+9Oy84kcjGAF09zcH9+/7ZQdvu46OiiRjCGHzaKzvhN93lyjb/6w61B3S jVjZDf90avd9WZpgaSA+fcC6ygKHEtVtWbuhafJkGGTKfOcnAv85YeVzOdNR2SV+9nbwLbT4hUHf CDngAk1xv9PWPUwYBW1Kplucyr0irgGfREU00t3HCT+dCiDpUcXF8sM7dzdfZt88EdSh3c/KRTIp I4F5GR0bCCRPPbHvZdMRZHqBOv5+/OydN72rFe0vBGyfFgY6+LJYwomy63iWLg6MNTtIywh1NkM8 8Jr2XOC5TPjbN+7uqmtir75DRlN4vvDPA2YTaBr5DE1MG0E7NEw72cOqaLf/NahnPG0TkgoPlh6j 9h+mWUo+Gr90Zg7NYbm3w07Vsd3R5c37kql1pOHnSYeZFjYqkk/trn5RKgsFSXdzBqN/9ydPHnzu l5p80UI0kyO0tb1UliIn2oGglSjx975mHqHaBi9Q/+xm2PYRdNJ2VAFAIKLvCKUaaKa1iTVhql4Z kdDtpo4KIN3514NV6getrZhFUNIo1/544Ye98J2mxaej0/QC2WZDBRX9RRFBiY1O125fkcuKcX+Y pt0JG4F/c87q3o6yyKYyWw8rYiS2b2wsS++PJO9sJ6MBzQqDNswzguEmNdCd37z4ekVrXwSWTL7J 7BY/OrkM6kcuSmWQyk+VVfa+ou2HmXYTwtpN4GIAE/wklu88ZRlo/4sb+CTgvwrb8aXM8X9b+w+a Wvbu2/PMM89w4uY+bkGsf0ZdZuobYESayOalJUhrqh0dZhKPgATwMUCrSAJekiyq3GkaWhHVnt5/ AG/17PO/+4Vsr7766u+w/Zbb87I9q5vs/Aw8YjXLjUoGEa2U4C/jlZRFSwmxElFjLUkvB252lje1 aTUjxh9a4LWTbNf8Q+aqaM91mTEqgdbLp3/8xJ7XRflwYk9ZglVZGzjUDjL5fHp584Wfc7rrfvDg xka5Y2MDNRdJ0P5vaf/Zf/Dgvn3PPMOh/NWve06ePDkzs74+OdnWNjq1MLUn5OFCpJYqFW3WMJ30 ZJ55nXw63xlM7klszM93ulYOdvBU5nU+RokhbHbzu+/m/qO5+WRo6zm5dZtZXbt1ax2Ri/3OMvVU J+hINyv5RBCl6plOdAfrC8yxhYhQHkhsspjKiTwMLWjRaZdejw2TghPA2PZyc7OTw9OQjULG21RU osv0nYwkfJwKpvkefUvr90gKL7Mxylr/Igstbz7867+/8hLv3MA2Lx0fwFD7/8HtP4Zonn32tw40 60DN0tLoZFvXwsLU1MJC/8IizkP4YYe4Sg5GolCb4bzBiDaydtKCNhOdO+1MDI1tE9zu3bu3tDQ5 ML42s9p8csaBpyfAEPA7uTS7sPDXv347Ny5TlSlzZOYy+YVw0guJaaKAIE3g8KFJ0fQDcVJY+0YH GgBxtT2tnfQ8ldLsQdt7cN8zz//i1w66OGE72fPrnp5fY/sVN8OKr/5SUCqvAPKRYm0iYbtZiaG0 iB6NX2o/CwNpBpY2DWaiqMuVh8sV6Oz2jNY8/g/DjwXN8wqaHo4UQAOuAdl0tfX3j472T7UBPV1T /QsLbVPEUQg/kB3LVBD4/krix5S0HGq9n06jc6B9NE0zqdrE7MTE0s2b927eNGdLS+vra+vrqzNk oBmLmx5+GiBrcXRxcWpxaur+t9/+9a/AD8paxEumJDghcNiGhkKpzIEv8VGSEUZXGAp3IXhlMsRO R2dik12DpnO5JBZieeG7b/9jdbW5mv522mZWb91aWxvAhGlJ7TIlKZXq5EVp4KerJJX3RNh0lm4y 4EXRpNJICqaJNFr89e6M3Lf8r/v/DwLNq7/4dY+AppmgWR8fb2sbaJsc7e8CeJbuj/a3dY124bxt dPR+/9TCIsADBD0Xxg8BBAjRa2Eos3OTsddXKvOVTtM4RveQadrELMEyYzZc4S3+rstpZsYOIFFM CC+ajeiZXZy6T/4ZIByw3xMnQjsdpBphBAKH0wkrooEkwxabmT5fZr69vcJ6w/1NbVmWDH9xcXZ2 YmlyEvhdbZ5x7LfTtro+2Y9P8O3aaltal2AQf8DGq4SSUNrMHsxktHu1LOwjiVciKMW3G3QlHqJo mtYODj69feMH3/7zv/6ttq6YzSfzqXyypRWgmVwaX5ocHxDCmZzsAoC6uiYBmdEuIGeyv22qrX9h FODpxyAigE0F+NmzsawAaudE3gSHscRaRFlK71AfMjOs0jHbtXTPwWZ95lEbxo+PLy3hzRf78U4K nSk5w7bYD/oB/1SkLpoRDV1W9snIhHjBkcGSJGGy0WmGCOpcYKAAWXQsb3LNjanFUaJ5ScmP8G2e OWnYb5sCW0cMx66zsPAt8LM2JV3vJTtnUPqcy3a+eyJYPiGtd4m4sYUMDV2qlRPtD+lNtVthjfs3 n96n215VYZBhPzT8fP3119ezddeb6q5fr00OADjjk+PjQM9k2/gA6QcQ6urH99XVNjvaBUxBNHeB eUYZvCiDAvw8SfG3oatKYEzTlKWcA8EcCJJ4dgKouWfAcTKMlJNy6gnfBaU1yTft7yeOu6baFkcF MwqixfIiBhyjB/6ZLCWUgVR6AbGidSqKlYyIorKqIYRVPkC7rxMflFEGH67zftvSkhLf0s11kJ/h Psav7RGMRNgv8g97z/1v/3r71toC1jtYFsoQIGW0AkG8KIAyOo1Hp6VqH5nOLRT93C4AoyWECakV ne6TKbezCp9J3B8gkCWQivzr6fntDyx4/a+vr1//GqfrdfjNDkC2jmPgACAAZ7JNEDRK1HSBBdoW +rsWoZ4JHoCojfQz9WKQx25IADPt3xwtfmXtFcmO7vX1JXN9xUJLX7avmO9tbUnlU63dLb241tvd 2z083L0yjPOVHhAOgQPQgPK6ILcYPhfBdotTbVNE7OKsYmiB+hn8Q43M+CgIKknyRSuR/ThQPMaa KYu8FkjJtc1NJmqLo5NLAWTDFwxeW6MXg2jX/SlKvylhnynFzyIxIC1fCVvbsi0aZrUEmXeq3GP6 yKSTXrP7hORgUO2bJUnTKKVpkSfKU/h06zOr2MFOKpSBoed/aPxTex3E83Vdbe3XxdrroJ4lIAjC RyVzV9c4iAgDOImxxDY627WwCPFMMtCdcLEKPyp/mHhK1JeGPKkKlG4mC8VCqu9LwKYv+cdUMl/M 96X+WOhuTbbkW/ry0ykgK5nMJvNtA0ANAQu93tYmwG2bWsT5LJFjGYjn9xm/bo+X6PkqYsBCvMjo 6lFKQ4obKY6KxCYrljfuz84uOSJs3iFyGvDMONpZH+gHG3b1k3WJHYuftbVFM2tdl+BIa4giljQS 0W9qd/eb1kOmqu2mriGQg/VTKekcjbT44OnRmzfXDRHOWCbs6Xn2h4Yfsk9tEzBUW1dbHB8YIO2Q f9rGu9oIHlxj5FqalAFdHF3omppqG+0SHUI2CPCzd2NjExBqT2uPDYNDKY0vosyRvZdNJlvzfa3J ZKqlr1DXl0qmUslCMpnvSzYVU0lcTWWLhb58LyQWKAdKneCZxHuOIuuT2CWyGe8K/ikzlGn8GheQ ZAxyFD7ETkYYCINHX5gUBXR1CHgqHZOtuUKhNVVoHc4XWoZbh4eHW/CzsjLcChYcXhleWelZwdbT w1+GLKK5i7gG6/VL+JLdhwIe+kfdGolMunxPIuGa5SXdahe4SDADzNoFIVK+0FbndvwlVriSGRos 04OGpiY1jK4SzScVPfQQTv7g8FNbCwTVXq9raqqrq6X8ge7pGh9YIgHxh9RDOcSQgkjWBTZYaOvn NziL38WFKvwsQ0Fjz1suiREiFUI0ddJ3vTddBOekiJhkbjo5XaxrIfG0TvcV8n3ZVF8W4QwPFwsD AzJYAJG8W9fC6OIEQDO6CP5ZWLACaLEi6lXy90pJeaYsINKCe0ZSeUntSzan1xYfCO3NyXwul8PH KQyncnjjHPBbh/wBiE4Ws7m6bDKXz/N6sZgtZlcGRqnC2gic/v77XZp2kn2IH/APC6MZu2AGpR8g olmWVr0qFNC8SyZVtCeMRMIzJQsTxtlc1h5p0na6vLg+M6MSXrSP5IE9Ap+Tz/zA8LOvFuAB+dTy NwXQMPfC71Lb5FKbxg9q6a6uWbDCItADC6h/incqIdx/PYSfTRPBWHkSLsBXRlMEUJooADt9edIO MZRK1oJy8slCNpXNZ6d5vdDHx/oG+mkbiOLqGp1E3jeK90ICNjuqYUv0T4XXkX99J/5hRjo3GLQI G3nzjoyxf0iElQwa07QGxua0zcSDXA6cVyjkmnLFZKEplcM753Gzrw9Xs7lUXSrV3ZttyvJzdq90 TTL1lNjVL+QzdZ/sRxRJ/NLOsHYlIUWS5ubtGdPrLCZPSTGlmVeIfDg3dRM8TVYS4X0TMnlFjJST vHRpn3ioPzj8CHzqrtddL15vyq5Pji91DWCjhh6VDB6ksyRqBAoIkQypEIYTOOqCicgoEsaPwEea w/FloA5EFOGLYzS5ly9OtwI9LQhUhXw+n5xOpYrJVuAGNwu4N5/PInxlu8f7AZ5+MpAid5TvPDUq xCPwWbBJvIlfmlV1dGQ0Rhnfp5SR1TiVeDoqWlohNaEzq3yPxJIDy6SKTQXSTCqbKpKAEEvzvDOJ W0VcFACzma62BckEu2iAie8u/gEoSPlHwEO2QWRa1mhl1/CxBQrlm7KZdKF3qg0kcWyzLNpH5HNp FnIC2XBtFqoi24ShySZDCNr3g8PPdWTwtV8L/zQBP8i/xieRd3UtiX5lCBPnh0YixdAov0OM6AIC GXORAD8HjX/YrmZGJqM5tDQgp+8li4BMHqPVOk2aKWD/T00XukVH5/uQnfXh7pZk9zh97gEilu/L HGwUoWy0bVaVj2Zfs1OCn+/++u3tSWIElS2uYZehzSM2sxYyJHxlKrJGiybxmcwGFgq7mSzWJVO5 Yo4YaSrmuNWlck0p3FtMMpxl6/LFuiL0fDG3AtKh9sOZCudRRC4YClOCn7m1tXLC5OoZkk9iOdFu qp+Z5fZMOu1aN7hXtUvhK621d6mlJjKYAy9moygfRPq6IkaiNsu9+msGhmzTSWUhctIPDT97mbvj c2YBd/LPgEBG9n0wEHJ5RhKGEKbTQgaQP0zmxcSDgq7CzzIARBrmntS+LCvvyMSHTGYWKdc0okWq KZ9NteAnhS05jaSrUOhLFUROJwv5lgKTP5N7Cf0JYKGgSTp0nY0DPSXy9bvbt8k/CQlgJVkFkRGr ovWMkswrZvhUdUTrR+a/PwAB5uqKfUBKNl8LmNTxk4GRIPIFVYBUMtlU28RoNgP1QzLEubxrv4rn EP/gf0TgWpZwras+8ZKSmTmZgKmckavLrn/DzQdLbGyCfMpGbANfD7gjX9eN2hS5cRDEen5g/Rz7 DxI9FNC11/F9rQ+MI/8S9wfbACPYQJvAhuM4ObpAOdJFZYsQs9BPEL0W4GdZ7R+GLFafRDaKBYzz iTqonb48CAjjhGiVS/YVmnpzENMYsSKCWDKbzeURL6CfJ2XAmPjh3fqp2tumZoFYamjNw2AgLkr6 I/UvMZoQwlTyEEt8dzJSgi6mIArJPNfd2BRv+GYKxALQ5vLFXC9EGMJpUz7VhE+Yz2XBScVcNgch nZRtBoGLoYssLHU/qnhRP/cN/9DDIX1U6OXI7iIoyRiaSWjfT6KUMev/KIYk70Levtwhf8I1EWV+ 4z3kMU21tUWRpTIwqZ6giPtD858P1tJ4hn2IgAv+oX0IE1rCFnSI2oijEreUEiCgOa6zLIUtYkwX AvwcIPswq2AGom4el/lLSNiY6CvC35lGlp6czvYliy1JqNQ+SGnkXqkkpfM0BjNV7Jvs75rsMsaP uojEDm4swkZok0zMMBDj119R/1K2gcohUDvYNKZ3paUQJfai5PEJhQ8G8EEt8JNsyqXyOXyebLIW wj5XzBdak8VCNpsUXZ1CasbCTnGGbhQpEWnnQr9aP11SOjbxK01+NaucSWmdUho4Smh6xeQsbRo4 xGjU7h9xq9PlTczS0bWB2GTC13hQm0Mw4IAwjNU21dXmggrKr3+g+KmrawJ8ijPjLFuwcAHEEC+j bTaAjbapD90vOppZEV3o/hD/HGhX/mHOTtRoDkZViTHtx+BgdJLECpRPC/Z/QQ04ifErOV1omS5O 51PdtAwkdRfjme9LAxHUM0sRPTsrGRiMAzCB5O+TFVn1WXs3kH9pKiZStSThDKyD+IbohfmckNUc 0gdQpsWmpkJR2a9IpDQh6YKMhuDJ1uZyyMOosZN1tVnhHy2kIGVYRPYJO1MyeDYAAD8SKBPCL8yh 2HrKPQcxLK3OD5saJZvPaDOi5mBl7GslCacyN5Y2mfjSD7K5bF1drq6prqk22ZStrc329jgB/asf Wj/HAQI820T01GZzKxDP8H+RgAFB/dTMo+I8M3jNsog6S/MZGVmXZPDcFV8JVq/5FwpoGTdGD07I ZEcwv6HE5v0C7J6WQiGVz0L6fJlM9k2DAlI0FaGAJKNH6pNtTY639Q/0i28JJgLxTC0SqeC6rkVV QcaCXjD5O+rvaW3S4AJAnSqE0opdVUE0nxnY0ptShcIAP4CsQbxMMkoVyUW5JhAhGBhxqzZZrK0j NWVrYQwhmK2reSj6eUoT90UR0pZ/zNTWklRsZAnEkrSf6iLQ6YzU3221K5HW4qAUMEqb/Oj8rJ1C 1eSqzAMkXblawBtRIQUQNeVaTfELP7/6gdXj9+9Hhvj1dUTcIv2OnvHxgTkWUScnddeXxAvdG7Mk nMnRWd61QCmJkhRyImigavww/5JvMaPN5CWpw8NdnZ/O5/vy3N1zqFgkC9dBRdA7LUi+gJy+JsQR yBAYMQOmeKEbIQTczCL9wptTSI9KBYzch/j1HflH+o0wDlz9hZSTll4crV6YyqmsKY19HcPFtcju kf3oGyIhhALLoxhXQBRL0uFsyrOMgk8DbmrCKCJ/RwcLvWcgiEwo4nmKAGYBZW5t3Nre0tPI3hXq MX4F6UxQAEsL+Shy6CayQ7V9MyM92rpWdJoUhMveJjREMPOCp4ukBldagxrcL35w3T9Gq2WZdsBr hfyZZNalpYvJyVG1oNs4etL/IwFllHXURfLR6JJrINsv/CMrP4qnJw05VLKl5Y3KBIJF3XQOChqG M8yeuroimKhYaMqDkhAtCggYfbl8Njku/vNkvxa/6FviDSGcR9k1xo8xO6vVLwyf6h+MVcX0/3DQ RC1L149t2RAiQvBiKYM6BfxTl0fIKiD/yiH/ShVb8tTS00gFMWLFpjwTsGwxnyOglwTNXQviANH+ ws+ilP7IP+gfk3dIkPQSmmtaSOi68+am6CBpRtQ4l6kAPvyM0j6g+4AwU+p6lvGg7rp6uohfrZq6 M4r97gfXAHRdPiWgXocUCPgZIP0MDEj600YpO9vPUSN0JJBBxwI2bBm8N3tv4t7E7D03feDfBT/L /KJIyh2aSmfS7AjvYIG0r6+1DyZhoVAoZqdTfdOUPsksXEMqoGye49U7OU7uGRDxTB00KlprliYm IDw7ixim5sGiwQ/lMkiOIOL0T9HSbAAS6FbUVcSkLqy9qiOKmw+KUrbIQs2nCrleWj5AC+Ipqafp Okwh+tOQ1CDL1E0JXqOUY1MQPwt0o6boIfaDf74F/5TEvMnI4i3amy+ISpdMH5lBkFmQngtDlzjD FMdQYZuQfEMocbBTUp6OzyX2z9eavuO8NWjd/e0Pjn/+rY4NHAi5CCjTKwMDsH9hwbDkLspZ45fY P6AiAc69iZtsUubPBDqW51/eb/Ej5VOTgGSog/htYZHRDIQJNM40cq1p4Ke1COmcLbYi50p9mWJe xmiC8jxK88N4NzQcDcxOinnIMwatRXbOQj238WxUZYjEL9TfgROOSLok4BDDmTqaxoGIIY7sxqaA mkjCZS+UMgonTfncdVRQWAtD1EpJxg5SgmgFIcKDRkoGT3FJSqejakgtCPX0SxPHQhC/OCO6JKtO l7SSS0+IZXRZEzojjUhcATqjCzogVJU2NuCKa4qoq1aJUsMHhO8ELq7Nopxdl2R2Uxzu6bHli+d/ ePiBgEYHWbY43ZSsW5kD/wyMY/dnGQFxTBKhttGlyaUlNlmhPRknaXGfuDfP0wR+9zn+IYA2GMA4 lnI4kfYS+lpY+s4J8yCDh3Bm/RR4qkvWIXYUcnXs6SjSFgKCBtDyCARJC1AblMdo14QUwaifZyWZ n+XN+yimCv+MS4pHwSqldpWjJRbASlISY8s8lkJJyPxlPgaf5gFrXogQoByEiuJ1quliXR0y+rps DqlELW7BUcWjqCLcRChdIJRx2S9mJr0oiOeF/vsGPx1qgZNyJCBRksm+U8koLAhx2aP0+BiAzcZy yRCTfmJO8imJNMqhESJXBzvu6yYau19/XTdski8g6NkfXPz6N5BPE9wzaNlUvpXoGVgHiNr6x++D gwbW2Qy8hM48NrZP3LwnyBHscKrEPAlo4nnHP5ubG7YJelmaYdDCu4lvsD1Tyefh8+ZZ0J6mf0gv qNDEzGsa0rkXzWUQrXWgBWTvdJ/E9KWAbutfGhW/iTREIiIj4bwLuz/5ZyAhXq9mOeLCVUS9au2k IrJ2eZNjRBuYGgP8kyyK3mPhC/iB8YQ7silWw+g7X8/V5bJNcGGKMKmL421iHvazkcS2IIiLeJ8B 9BbwU1H9LvJKL/QnYeGhHdeqb1TpbLZnKkYYydL1zFgTMuMxARlK3qktmrpkbd1KMAHlh1Y+3X/g 3/JNdYUvC199NTR0o2VlfUByd7ZxgLDH12dGtK9TpkYIeCbMBSPXxCy6UicmTAAT/bOhTRzLWsvA 2cOM5K4ViEK4PSmyD2uUHLAsCIcUwJI3SpV9WSQehXVqd1TAukg96n2TcqZErI8K+0DU35yE+lD9 0y4dNzR2tOHRpEKqm9kAyeCVljTatgM8yMFaQdxioQuKvphCDl+ACd6XKxBUkEEpuIkp+nj5IkkY 4ZvqGdidmJJpBMTTIvznb2+tTQrXEKqgF5ZsK9I+YiKZJvUdElJlE6RsLnfIg2lVRhUtf0lja7qY u54FfKQpIktnt85k7xTRz/xgcHNw755nnv/tL7pHxoaGRsZGRq6NXBsa62Hz4Th67ZjCIxdb7xke VvSAeiCYJ+RXfu4BOrOLuDo6+h97dVrd79BtgO4rbGjFWnmwgp8HMysPHjxAzHuAARnuo9XcgmpB K4BDgzfPwcrCKMuiAoaIArMD/dbQYJO0W0albZ/afVRil+ogDCfCKfS04R852toyW5B5JZNRO840 qdP43dyQ/go8XJLV3eEfEiFZltlrYfZCtcMIghddTFFHN3GD15ljUEOSONCmhjgBvUj7YqGNvSyj 8C/vs311MiFOu6jlUiggkXIkCVWoACPtyks4qGDCPEEayeSZvCrTNcrXi7TkpKMGKGItY0XhQwba 8z/u92CyzjPP/vbV3/c0n7x6bXBs5Nq1ayODV0cGB987896JBpl1hdYlxjEAaXWwu2UF+JmAZEa2 RcjoD4DDhhyWM9valk7qlLqbsImWZg1PETYPZFt50Pugt7UwXUTRvRUe0DRGD9lOS10ePWUoGGBn RwhD+jfNYRtnz+zSgDTcIFfuWhD8CHJGxcAcXby5NIqI2jY39913390eWN5ol6xPetdL2sJlfDop cW9sSiyVvgBz1ssqF+RyNsVeMfbSwjCkmUhfEcIH2WiWshoBLZeDAz/apikEy3LSEclssL+L/SPE j5QvEoZyNOGSpnn1FK0EEkuM9fb2TXmOHo4nI360OM/S/pzowKdA7KR2hpVQhLuS7bHZ1/9g+Z1z cTHH69VfnWxeXV09ceLElauDOF0dvHb1yJUTDY3nTn3y2SefnD/HiV+caDo3MAf8rL03PDxyk5iY Bd+AA0g3o7OsQYkNNImpC0ts0j3Zw9lbS5PE1oQEtdlFBLh5cwvBrjBdl2TzIXgIbfTstWHTTZEV 72wd5DNtGCjW4tKANBqzZNAlLfQgoKn+rllRzdLIMTuF5ki82/r4bez+41Nds2Lolblkjha9zFwI KYNvbrYvayjVX0bWAhimDj1kdXi/JMxm8l4Rn6AI7QzINAFKeZqIgHW+aRKpIFrHJGiNSv8cynCT o0td/QigwM8o3oWeRUYaxNhDxg/B0jERIhhWABFJuHdj08y7yJiuD+1mNQuUtS8CutQ++CTAsDRn WfAgfu37HwhUugjB74GbE6trJ9bWGhsb1041NDSAb95rONHYeO6zz84BOuc/+eTcpfMgIIzMEkZx 4Nb4WvPVwcHhe7NLS0DBpAAIBEBTERun2UGHYM4L4HOzhxNgMHWC/t7ohEDNsZXgB5GhD18N/J5p qp6+aTou0B7Y29g2ARMIPYkwXlB+Yw44QPqh4zvLvZ8TX5F/dbGHXqLa6OQ4qhfonkCncmt3a6HQ i1kchd5UoYCSaL4VP70PHvT2Cvvdu/eA6n/pplTQkH6nh2GAN6F6UgsOStF4R70gC9UM2YzkHfUv KLN8lgkYCFLquQSzdJLQll+iN7WEEPtX4qdfWp5NN+GyWaSXk98ND8qCmGmLaVjxBj2ZjGmUKrWb pjOCqX02y9SPHuJ1mdqAWBZ0b/zj2jecwCFuVtca1k4QNvg9dQqIOffJuVOnzoFwPjsH3PD0yflL OJ26NbeOdAsRa3yOEaxncKT32kzb0uzSLL6+SQlqS0uc4oO8DFO0ZIqATgW8J9NH2TZN5HBTJAkj QTdhkkVqehr78/R0inWvFHCUb0FSBtmaJ/dMs4aQzI/zXaCe+yWE9Y+aXiRQEauonCA6S/cQ+fT9 ufsof9HdBNfdU10vMdPETdlSvalcL8AExU5Xh0UTwLa2iXKngDvrCqSiFFMx3IMSDoQ0vGd4UijQ 89m9SEUnZe52G99VDU1MMRqYXB9n/vW712zCKTnnsrJde3pZ232WRZvxrF2uYcUGmey+bJo42O2b 0Gx1WY7FfO865uJl2U0jzT8oX9S59p9/WPvG3p8/9bPLH7x9blXpBqA5dY7bJ+c+49l5xKrz589/ dv7SpfPEDX4+u3Tr1q1Lc3O3xwECUNDS3K31W2u3mq8MX2tGP/Q6c/rxdfYm4sYkT5zky43zjntu Mn7N3FzSbQK0NDkBLSSbsNBEax+TGtTYp5N/TH5ZQLcGGzkwc4dleDR1TNP7/TJZkArcHKc50HCR oj8mvVIAYRLqIt1vIIbdJZybhlBHmM4iUvJ3ohNn9BSwEsO8XpuXA4fz2ixIsANnCMU5lJiIJTT6 QKlCgNXB+2piPTebzbMzkcWLXnpBaGGVyjvPllSBSSrICLo+vnb7FgMoLUUOP2WYwdJGCFHmTEqD DzeIIp0jZ1SZbMu25XX5XpHYQUpxnWdoDK1LnnT6+df/kPL7/h9fPsrt8IWPzyluzt36BEHqPFBD 5HxG0GC79BF/L92+dP7WpVu35/B7+/bcHGGBLwdwWls9AY10ZXDw6rUTze8NXm3uaW5ma3fzCqcE NM/0UPVwcoCbT7rO+b831WvUywkEP8y1mpgskHDqspivk50uSuWd/RIMEFnI5hzqCKiIoUA2PkkP 8/7cACY8sGZA9cy6Jcu3o/CCJtEIIO1thNAAAU1Vxg0IAgkJaMxJN+NUCYDkfDYpVfYc+YZCOlVH 5SyCA5r1a1hC4Cq0JHL86nLSOc+2FelfEfNwFqgdvbk+N3eL0316UQJBYRiNaGjlZvtiCqRX6HUU eG9J5OMEzPv7o5hLUkqYeT7ty0pPVdtNlNwp7VGVRO7Fi1RQPv2HtG/s/8nxCxcEP0frP7hEYSNg OYcQJTJH8PPRJXLP+bnPiJlbty/dNts7t28tgZkHxm/N3Rq/hUUoeppXe042N6+sDl5dPdEDQdR8 9dp7710dfG/wzOC11muDg2eujLx3DQkcLIDB97px1rzaPEiYyeyBlfWeHsjxB+sFZuoF5OlZaTBO ar8oSj2iMoApTOsBoHIz48pA9++P378vJjQqGdRa49LLT9kl6ou3xxktcfUmiyp0pmbFoJIbBlP3 +NCssTpF3ON6IQVfJ8esD58kJzZCls4PSxg0hmAcoraKnmhooFbOiKObKetH3CT/dHGyPC1M9ipM ttHMMKHzXhA7Uw96ASP2N/ZithDdSnSlNcGlyKVwjveCVuvNQ7f1tkKkzays3LynRkcTtBiaf4Bp dJQQw8Pae0j++cU/on3jieOCHkHQ4YuXzn/CCAWwfHLp3EfnP/oUcevTjz7FtfOXLGzeFOC8885b p9966zRggw1n42tgoLXV9TVsXI1jXZflgOI5SdpZPSkTxIGulZWTgyM9IyfHmgcFRmOD18hb145d vXoNmHpv5NqVKydyqHc1TdeCf2DTwfRtQn86uydYtWRGX8suVtQv1imfv50DAQ3c/xb1L/hBnJBP N2p8/Kbo9iViCObUTdVqIDjSnVTlZo09dU8sqnt6jwJq1mFqtlcdS3QjoXLbpH1I05zTA0gBN5RG 4EXMa6xLJXsHdDYRFwCgCETrCGA8McuKKuqElHs0xWTh6+pNFxnRRfxQ5sFTBGX37jmFZsEGod/b mxrubSW8anPXsS/lm5quswMIW69zf3pe/Ud0hP3swgXDP9jqPwZ2Lt3+6NJ5/nyKH1DPR7ht2Ob2 6bd4eusdnLidRkgH9fAX9Ly2ttq8ukYnaFw3XJ+hwag4Ip44+/dkeOEDmUEuM2+5JsLJlZPYf2Z6 VrpbEC1q0RCRrytMY8en9QJNm01N99VN1zGMoRyGm3ynWwNzc3NQ8MAv0Nmzit+etR6+ZM+MW9xl Xa5LlDQWpwTNWf4sEUcEzc0JQznqKej5BNrFUvJmYKE8Z6BJMz8mYMiEC+oeFDTyKRoO08NzFNDK P+JkTi1KIx35b0nob1QN1Yl5jZ3zRoOFLifmt8ILK9lMdMhSoxPzuswWMCaHZmzt6+5DroFYmCrW cVmCJosfIOgf0b7xxAWDH4DncH390bfevP3pRx+9eelTbJcIG569eRrbW4TO7bfeegMXp3n1ndOi gG7JiRs0UPPqLQQzZl6MKsyNBsRcnGuTXmmIaju5tnkdhVddswP0QJIYvym1M+EI9BmiJwtpFmgZ /lzuel12epoBYjqFhnFk8xg6sPZ1tmCP39L3XrvSc/UkKGwQcfQafq8O9gzi5rVrzd2tfSMouHS3 XBu+1t1dGIHjvTKjAROuN85pJ6yw6CILUxFETCJFzM8u0XdCSMEsK/RoZOuyLdAc7HlmwQsQz6Ft 9TqKdZzpmO0eGL/fPym9SBRAsL9nYV8G/+PSkk0xHcEF8VMAJIHTwAvbPfkVcMnWqeLM/iwN93UX 0OnCeXIgRySHw4F9+CrWnPy+U/ifEj0Xjl847hNB9fWfv/kmAtSnb37Ky0uIUthOn35H6OZdnvEW IthHn35GdH1E8iH3DMzJNSRh0pJoYDNADGmNnnUy3LOuawxAR49DtNzv79dpmjrdrl9W0sDk0VGM c2tLS+tQ8suWFnQCtfyxgFYOtBxC+LCbYpplVLSr5xAp5zSEYm1DrEK2ppSmq6lwHu/qCq73DA9e Y9FlcKy7u/vaGE6D3VfHxsZaxwZHUI4ZHhxrGbs20jL25UhL99AQ5Nnw4ErrMNE1gxPaMmDsFjlb kEVSfIIc5wsWYCGyd+I64msKj9PGK3bzf2ZFTjr6dU730rhCR7oSZm8uiWHBqHnP8JylvNlAwOu1 e6bmLHJeHjeO6/zsbAfzw4X+7pZcSx59UpjMBBWO0s6wi1/wftdWv+cWjgMvgHsuxC/6ly9+cAHw qT8Obnnzo3duAzJgnTdPK2re5TnuO/3mO+8AVnCeIawbPzt/+5LlnjkZxrU1+f4koEhvh4BnYFwb PdAsaPDDZcvW+zElXLrN29CdLA4ya41tMulnpe/GjZavWoa6b4y1ftkycqOvZWispRunIdyV6h6S u4eGvqL0Av+QhNZ5dVxWQmO4nFRnfEaXdzIhVGOZWTyIax6smLUKOQkYgBkZAY6uDaJMMzY0MkQu u3Z1KEeg5thpmGMRrKk2nwILYc4giQf30n/OUQHBT2zhfALZZ1S6L9003unSugGQaWyZuKkgEjiw Pjgxq7I+TE6GaaTz7p6NqMKKtCGwokD/TF83VrlpzXGGHHgo35RaCWYwN6+tft8tHAcuMnZd/OCD D2J/+Yb4qX8bceodnN4i57xL4JyWgIX7oKMvfQRTqAGm4pVGVDVuXVLy0RBmhDTAw6Ci9CPfpb2B 38l1u8ZJ8/ik2DboM+eMrS4zeV4nUYyudPfdaOn+6sYQtpYvhwstN74CdXzZN/RlC0r/CPqAUcuN G2PNRO6A4Jc4Vcgiqef7zkFKs8N2/CYnyio7ji9phLypMshoId2WbgZ3MJICaz1EGvzDJk674NTl WhJQE7gmS+qh9qkjJzFLhEIr1g0Dw3PrSCUGB3uGR1aGm7kyB9YpWunRBToo/iDFVmZ6oIbN285y gTXiyXLRrDEYDFEpbhx0iJ4JuqAg76XWFGJzKxRZX6GlF0Td1LQSTIFnLvw9l+APnkX0uvDhh59f PPyXP8cAn9jnGqnefVcFMhiHp0/f/IhJ2blzjahkHLl67OqJ965cee/cnMJHJbRcQ/zCCN4S4CgX ySplZhtYX2+2yyuhb3GBC2e06XyF/rZ+bWBk//LocN9w4cbQV91fdQ99+dWNsRtjQzdav7pxA6w0 1k0KGhu7AQC1tNwaUPqDDrtPDMk2OS7Rsn8Oxfk2LjJDNTbHDAAGtKzr0m/WteuaEpU7SruPy+4x zrRNqgTTcwwwcpw6dI2xwJVlmT15naUwpPFo/cuhdfQ6qhlAFWdj1KGhdg2Z6OrqSs/gyrWTg8OD wyeHe5p7rjWfHOlBdefatWEkncMFjnp3C0Jny9C1lr7hkdaekW5ZCga/MysUZD0z926aJUL5URRi k0aXkXzuY6G34XxLN2JXX75vuHd4BFoo37Ti1g8VH3HP94+foxc+/+Ljz736v9QAPrGzH3/88fvv v/3GG+8qEb35JvT0Jal3nTu11vje4JUzV69evXIFZfj31jTzuiUBSwIYA8n4wK1bSgYavmSSPMHD x9btWkvUz9K1IwuHMYh1yWJCbLoanVz5qmWMaGkZgvzpBlq++rJlbGisD9TT3T3yVd/Q2A1w09jQ mqCW4BkQsWUQxDVl+nlBHLVRfSmA5gakLDXF1clY3ZK5Nm1mvR5UHDj/sI0rhvQrpEWOjfQV/tgK jfplYQhFFUxmpqmQxBx85s9wOcFKOVlgAQ7eCgMpWQ/nDKXrQY65enJ1BkhqBjqG81xArRUj3to9 giWFWkeGh661DncPtgyOdHcPjg22DiOMjo30DbUMDo+NdLdeGxkbvAIBh5QA5lrPTDOmssAn6O3j fII8QERxB7istBRXLHZ0Dvy+71v/gH4ufAjAfHjo+IVYsNWTjOoPH4a2jh86e/nixQ8+BMreZ3R7 E5nZuYaGxjMNa9jvbeRiBV4iGLSOgZHEMZvLm4zexi98oeNdZvEnLT7QvZElhDCISyvdwy3dUDsQ QTfwNX71VXcfpdAYYlYr9rgb3d24ACc1zwloxiWKCXLIe10DbYpfq79Egwl+ZCKslMT4o22vOkm2 TVbe1L4LLLYnORQBNjnc3Y0lG1IMqPnuVF/LcDLZ0vflUN9X+fxX6MRs7UvdwEIh6FXC03rwBYB+ b427f51VnHW3sey3LjUfoTY5uynFHKiy9ZkVIZ8ZCXcrrWyQAlkNX2sdaQU+RI1docHfzLmI387h K8LqWnl8FSNECxf/uVbo6alaw3Pf92//HL1w6I2333j37feBmVjVZm/WxGJbH6g38AK+ooqvDz78 EABDkw36/L799va3t1WaDFjorNMP0vg1o0tL6soHZvDaOAGR1vEk1y9boVhGhgSOAf10Yx8dgoRG TtZ3oxvR7MaXN/q6h8a+Gl6lfyAmwpzSjyUg05k9Lkai0M8tmg3jXFSFU+O7ODO1a6rLTZVl9xAJ CIbNVJtoecY2Qhzck7/R1/dHvjc+AIA8hMVnu6HJEIRayJCpltYv4eZhslGPUDHfblzBbD8PrsML wW7WpQsCaGomWdmk0dVLNmbaKzdD5R2cEWPilN360GfUOH753UF8gtaREeZaPc2yjQQrv/b8A1ZQ 2P8z8Q4vIly9cagaNDsgp6amXn6r7q/RM7mor6kRcOkLKbyOW3iBvz7+7u136Rqp7hb1LOnWqMYK bfSAPlzqGbuBhGusewx7/RCFEIA0BunzVV8LghhZCGxwY2j4JF9nQIE6pwJo3MHHiHbxnSiUYHYK ftgTjbX1+iV46YwfgfCUrBjC/g+7AB3mz3bBYoFeB5y//HLoBqYUIbBirwc5tv6RXJjkRNl8Yail j0uknVTwDKh+7+cJ1le/iZ/4t7HGZ7+sR8tF2fqld0kveE28jC5xMdq6rBrUmXVLBmmTS3MfHCV6 GDcuHH9/pAVKndRzpbn5PdSCVk4G5XdW4r/nEvz+p46KeXjh4uULO2GmpiY4V2jVbGejHfC05Za8 gJzV24eFvgiwC4fOnmUCSIB9Z+jr/hqrGWPolEUaPQawDEMpdHdDPY90gwqGbgxDUff1jY2tSfxU ABE8t4ziInRY71LqmxTtxTg7Pil4WeQMZ2k3E+rRaY5cNGRKpq1DwoudIKbC5Ap8KKSDrTQRhsA4 4vnmW4a+7B6CGQn7t6UAZvxqCA/0IZ4O3HJQnnPsw7tuM863cV2yqSmztguw1N+mYgszDrtkqUTi CBN+uth0L6vgyfJ3Zi2G/oXvZIcngmS73DDYIwuucnsPOAran8lA33sJ/mmpfNXXW7hwc4SyA1Zq goua6hvubwz91Dz6L2Ohd4jpO7ofRZiGx6NHo3Evevbixc8/fB/a623khW++c+r8qSsnzgxCDNy4 cXVN6efWnHW75yx85HfcRU9qs/MwimRx80XT4tolRt/ApJuAvTjaZapXMgFSVvKZHCYwwDvdfa2p G9Cq6D9rafmyQDcKmgfCCN7Ljb4hhLbW7p5xA+U5y0KcIicImptj+GJzB1fSQkO9oZsuNg6YKc+A lJwtCIb6FT28ECwRTd+ZWqUDUPSSgue9VQ1gJ3vCK9h97yX4n7JyoWPozvRKTG44BnEs4kbf/Y3j p5rQg7GqR0LoqdkOwOBp4SfVhIFm/6Y+xF5oGWCA9BEgJUJ+/gUjJPgLDAb9dXtOwaTCXT3qcUzP IPXIrGpJ+8YpRMZ1jZA2mXTdJaGsi+qZ5zfhVMKigwHV9xUt8b6WVigxgAWk1NrafSMPJOF6N5BV GDppoqkGsDkTSsfFVST/cL71VL+yTBcpSCDapb47QQRYCYSQAzLUaWeT4R/8fHdU/2vFkGxn15rD W9Wxh77PEjx6Dp/88U9eqK+mgxrHQpaLwrByyLA0FauOaAGcaoKbIdQ4VNSbYBg8UqMvvSUIhjBU /wgtXyXra8ISX8Pj8ehZK8CgwCQ8Qt/fl63/PieKDkzqyo0SwcRR0OWEdFsfhmlA4AxTPVPkFMBB QyNURQBRH6xfqOhhEBSI6qThH9ZzAyE/qR0u54kf0Iws7IJVabmolqx21yUr8EkcI2C6hHq6ugRF DFvCQ8TRtxcOH7YAumC3L5pV+oToR3vocfUX3w9y9j7x458+9YJ/WHdl83VvTbvqt8vpWMwiwoGq Gm01NUEgsxymt2uq5FTAUDvrqO0iqua/wI7Faf3WP9r2gIUwNsHXIQr8DxRfRn8RYN+yOocxnVmB 0mlF6g7TAJ43lhHuzg+N0F1ozXf35VuGu/um+/6INYqG+1qHm28Z2plzVcABdb4s/xEnPCLRFM2B fpk20mVJSES0CqAujWX9Ard+gRWQ9OFh0YwKHtHQvHYe4FkN0Y/Lv3r+u0vwoJwnfv7Tpy5LqYub fo8uJsSqsOQerjc4qnfE8eghrAJWFbjCZGb5pjqDC4XBmppqTRVI8ZgLizsrrJrYNraLbdNcsa3Q VUzXh/5YBdhRhMfLH3z+8Rfvf/w2TA7YqZc++uxU49WrR66ydwk+1A1ELuRjrWSn7pO3jI+g6Tus sAHjXWhxeVzmWo926SpT7NvuF4+8bUCCqSJIgKOpmCkvaxTr/1aabLD5l88eOuRbCnrfoYf4MUtn qoP42/8+ykGwevpnhw4LHuoVFfXmRtUdgid7S7Fkb8Sq7603IKt3yIttt5Dqtw3TVsLasoWlVU3A d1VB0lGYJRfzeAhtTo7vlCVWg6c+DOWaLQlo6D9xuSP/q8NirvrH44yOn38I1/7tN95+9/2PTQoJ BlMHzHgIqsDWZLnPfl1eqmtKYTSpGqxNF77jA5rQCxktWDRRQX+h6Dl69sMPL4I0j2vvzYWzq1Xy x2ofbs//9wUryyYOJfV2cMP3BQ/VbwVVfWAcWvSEHgw9YkEXs3QWhML67ebkVvZ6BJ6qRjhEItWw cuq9Ss27+0Jaa4u6CnulNVuVfOxRnkXNI0Kq2bOIsAtxBMjLlxkfPxT9ZQLkfdFg/XP9snobDz4z boOYIEedIMm5bAZ2/6Li59DH73/wlz//+cLZ44aAGqvx0xMcP+rZ///B6oWjW9CxdcxjjlvqDa9s fUqIo6pRFPzEtjNX8OfuuiGkencyr1dfv/MQbCOuHRFWE1L1VSlfcJMP1VvMVQGvpsrrqqn5G6jY gZpq/rYmc+9RHzhhtGDN11xPfB33oyrAPgfAvrMOGDfgC+t2LLgeqfu+4ufzt9+v+TO2i2cNfj5r 3hK/7AIc/3sz4DWzevpnZw/rWIYhEduKo1jAFkE8ewQnbb2nip2Ce0yYi7knbHk580Ym9sVCnywW hmUoGtY/mrJcFvcIXG0R9rHAc6rZSZeH8sWAx8IpQIh2tvxpfc0OgHwUO9U4XNkny79OmJuv4LDN H31bIPrAhC84Yd8QPx98oPr5wqUQfnqqjsK6W/zs37vnRSAHoVG6CbfRzjb4uOHbDrHYNszFtsSq +iroxWI7PDsWippWf1fTW7UaCx4OvZwDVIixtmut+h2oy8mk/1JxxQJZH7DdVoRZErQvXFMfKP8q xR/bYmjtBKVte0O98/lrwl9TMEDsMcYGoRVF/PrLFx8q/Ry9tIP9I31xu23fOPj/vHfixBlMWF9r PHXuE7Quow/s3Tfefvvjjz//8IMPLkK1+4iZh7dhq0pPx6pRtUPoi1WTxZZzJ7S2QXZHmFbRUkBc LvXbznyxID6GMO6AHIJYkELunDjWVDmnsR0wZlG1xfV0hFaz1X+vIp6a/8qzqndsFxLnIa9h6xfF oUP8euP9s7h4/6LqZyTw4fhlzR+Wv3ZXfj/wKpoCTzSckA2T13G1AWBadSfc1wBkncNknNtE1htv v//xF587ZB11yNoZD3avCNIxyw6xMNWEBjoWYDK2HTWxnWJiEFVDqmwrUcWqsRwLc1YslBwEfxqw YLUR4aLtIyLjTlExti02OvIJmQs1Tm1tj2mBlKr/m9gKrgYQqo+yOwtj93Hc6J9zj8i/doufZ7HO wQm3NTYYKPFO+WnQ60RUAwHGK7iQM5ntjknuQNY7pwVZ739hSCsa5xyxamjVV2voR4ihWJVPsEOS F9vyOrHqLC6EvFgYybEt8TH8xrGtL1uFqC0oDKRaOBzWVxkQO2Kryoh/RHAMia6amrCfGqoS7miQ hqFV7+4Ivtizb3B7/6yhn+NrW+KXpZ9dtm8c/P2ZEw1nQhRkrjQIhgyW7GMNcpf8GlCtmV85l7sE WKAsTosXaH0E0npXSQvYunz5UFyAdbh+Zy1VJZuryTg00jsx3iM2g5XYjrRV9VKxbZItzIXhtNHZ VlWfKhYmtyr7ob5+B19rC/JqYrH/wuOqCdd2aqoYqSZU+dnqVJnPeAHJ2mXVzjiPr+2Qv+vZrto3 9rCx9MgV+eF27MiZM/zFBlV0Qn8bHHYMKTm4NQTgUsoKUMhtzT3dPQJ8NRBZGg7fgtAisj50Quto ldCKBdEu1IP2N7K8WH39f/FgKGUM3M3w8D9KyNVXGaKxkIKqZs5YwHDhYFdfBblY2EiLVen8v+HT PwJWsVg1e22xAuqrekO1iHHoEf7hbldQeH5QFnnCKjxc6clcv3oFmELb45UrBlrE1hUBlEDqTIim BCchUBlmkgt3Zp7hTg1riqiGBoPNVSUuPNCI9V6g4YWzQtCKh5FVpZl29A22RbywntqGzkCrBRDb ljGG742F9Fv9Vm9rG6+G/n6L5Au9aixkg8SCrPNRpLW9wvgon6smXLoW90gq8Kt6+HBTP+1x7vPu DmCw/9UjV3gCYK7yR9CDllmBk73OS85rsk/gs7EJc5GxQFdX8NtwjMgKxFRDKCZaqnJMZs4MtsLn DmX6fI2QeKBR0kPV8IKsDxkNz0aPHw/Fw9jWlGtr+l8fzvBjO0WyHe6J1W+1Gqr0eFX1r/5vEWC1 LAu7puFPWaXUqnR8fSxWLef/S59rG6gMgC6H2jck+Wo26fvu2jf2n5ZZE6ff+fQ8KnyNAMAZRjHh nCuKE4MaszlccTML1PF+wgvnV4iuI1iw7urVM8eunDly5pgJhSfO2KB2wrGOE+cNjsYcghx76R2C oDWLKEGV/KzhnjVZropzpk+DtKARBVpCWsfZJ1k9kLFH66S/E01bXii27b5YfTWWtiQJ1VZVrMqk D7Aa2xLA62Pb3zDkxFflibFHN0Oow3j06Ac7hS+hoF2V3w98EIlGInE/EvUiHpoS2FXFpiqZjqO4 wkJiDSfOHDt2zKLqqsXW1S3AEtZy8OJNWSVD4uEg+U0lFsjqCl7P4OpEWEc1OIVu8z9LVlWxsKFh C22tyelEg0WoITqSVgOMh0uYU0Tn4f33P9ZwiBLm0aNbHa3tgbCaNx6p1rfHrJ0VVGz7k2NhTzS2 zc3YwklVfmlVnhoS64ELEgu5YwZRRw/HpEPg6IfV6VdP0MOxq/L7wctR4Cfq+1FunoeziBeN+D7P cAvAOoS7D1lccTrOG28QV5eAK7DKMaGr7XCykApt1wyyTJC8ek1vCR6PHKPkUlCdcerdZnkBbk40 VHFYdegzF438AS2JgJcfg6+1QOuvNZjsMGRpAVkXt1ha9VVjuk3RVLuS4dAWewS7xQIRFQtldcHV arM/FqvKG2M78GSsfud3jYXCt0PTUeJH/Ogvdiyf7rr8vvcs8QMERQQ6OOEHMIrYuwEsD0iKRPyI bn6EN6KeH/EPRQ+dDU3z4uzBdzDR/bNTDYyCV0UhWbyYeDc4uCUWDoafc/WqQyH++IiF1REGQpxE up9R/e7i3Imt2HEY2xIFzdYY3Foz2BJgNUgspLSnW8r0UH145SwJh8edhI+FMPRIB2kneVWtpKoV +faK9RYjKnwWCwtv51EEle1YWFGZfUCIx8xx+bh5xwAGDtpV+f1JzyNUPKIj6glohIAQ0qKAji/3 y4lPifIp5KQ42cqPmsfk0hcai5O2NBI6XEkgfPOj8581NpwRpjLqCaukyDbI9VIGg03ZyWp5vYrw d2VQACm4lMQQkfCYqnd6DiYanrF0Zc4bnYYKdHs4EjbqWWOjBjteDeFtTW40GgLDphIeK9JUZYdR kfBV5LRzMNxSfq66PxbbydVyca3KVdpiNGzpXQjxWjjtE/gcFvygHhYun/a45B0/u8LPEwITEgqp BqjxiQf8+hElIJ6TjhRYijJ5tqApogjjw+AlpS1fURURSSV/jkfIWgSmEJYILCEsTHXG6mWNnB4v gBJIYQbOtTHOw+F1/gxadIX0utoMVwxVXTl2VX2GK1RXxBWAdQXh8BiwdYLoOnEmkPAnGgxOGqrA 0hDS8gG6GkPMJb8i5ZW11hokTiq0WOG5ZMLh229/ofHw8lm4pYe3J3N/g6NcL1S1Kb8dYrEdvIgd bVeFlVkvzjQZvbED/wiAdld+/3HEjDfoxBMYRL2oHX/fPuZbiGhU80UkEWkikUg/8pyoPNezHBYV XSX4EXBGzcvyqfZNoodEa/1BpDvapb4wgRB89cm5xmMIX1exMsoI1kjBURDGdGoXlkkZk8Mh8IgI 1wLWcpHPbIASguARBkEj/anUjhjBhRTxSqC1TgBh1ZKqGleP2IivRmEv84PTmhFdjc5vPdNw6tyl Ty69CbfUOQ8XLx/yL1w4erh+V9tOYie2tbUmrLachV4v03eAnxo1Et99ZP61q/L7z8kS2OJKNIIm /jAhE+EDJDDC2cAlIIq4pwpP+b7eKYjyBHES30hFfiSOk0EjAYUn/MEqLcGavFpcQBaNRi13CZDx alWB8A0s8EFcnTpxjIgBfMa4VgvmmsrpxldDvH1jBBjDmjwmKlrNJfg5dsTgCbyE2CcwE4nFsHh1 EA/yMTwReMLjx06cOHLijLE4+du4HVONDSGOajTM1hjirsYtqstESA2oWJVfSOt0SMNfVg2/U+Hw b9dsYlsCYqDQ6t38Hcs/p4mc/9fgR3/MDMJdlU9/ItEKGPFMtPJl/DzqY0FWXEZeApFHYe1ZpFgp bZQRZY8gRBW3QkEZx4sY6okKMDXGRSMGucpnfkTpT97I14/E1xccS1Lo3lDOD2kglOnzkhC++en5 U6dOQLUDNeQqAyksufEVVwSSWfH8HRpzEdHRlbDTMbVCj4lsPyLkZVhMTDCxWSm0jhyTaHgmXKYJ 4mEAJqOpzE1VVo2GouxTGhsN4KzyorNlQqMr8QQa/oPLh6JVGn5rdh/b0USIxQL4kH/k7tuWfIR/ XBF1t/j5KYZImEGjkcYeI21wv2fVcUQ5SGNa1NyrAshTkeMZ5gL3xD3J/yMGQhGjlkBykYhBiYGU H4kaxSTPQ1CMHNI4KUCN6l8qanmvr9zomQd9+1kjFmGHZGLNRXR1qoN1+jSN0QbAgvIKCGIU5DIu gq4bvIaZ8rjJgChiXh0GNbmII3GsmAEKOdEEww2NiYNXlMwEd/QeroT993Dip1cbt4S+raHRMVdj IOWp4RsDAwKnc42nPgkjy1Z4LLJi27v7AvY5Wm/wc95VL8LlLwJoN+X3/U8LKVi+kDHyjCCOGGSY oGKUtIlCvoteBibKWiYh80SUR+1d5C1fApKqLY41I6Rv6UdCZSRq+IqQNn6B7xnisgQWDeR61OJY N3NFPptDrPkbfADvrBLW55awPjUJIat+PLLUkKzcQVhZ8hqS407pgacEWYPWOiVkjtFSFYlOjY4A SPwIkgRzdN8lBB4RWKlbKsaDMUuFdcL01dgYIqtQmFTOMnhStSWHiVjTv1qzmmsNxxzRLq3bVdnh 5QsB+xwlfgRS5yx6lH9OBk0cuym/73/KDoDnOaR4KnCoTpQtfIciSxxmGJlhyXNM0mYoRV5Eznwr ueMCJc3X/MAa8H3zUp4wk3AQ/shXiymqrqYQjwZGk/NZpeRFLX8Z8Ec0wEXVz/IJRN9Ti8L8E+b/ 8OXpyAdNIPyAhCVOwzui3M9QX2s2OGLUu5DUGDcCS/lKk0bJCq9oBqiWAus2kFB4lRPHxGMVtJ0h fwUO6RH1ss40nAlyw5BVWk1X1SqrMRBaVsIH5wooefoa77l4tBo/QlBrLnwZANnZp7sqvx/4mQyM ZtcyKL6xcsy+6ysnmPjhOcNHT8SCcoSzHHWoHcQMMxgzUgVUxChreR2CNCqvY+OdIkOYLuruocKO mlfTvM4oMN+9g2MiLxqwkogzL8RQJkIzNqvMiguSo0YAmldR4R6yRmm5f4RSDiqENA0GDXLEYhAs jXEZTTUdKO3Dwv2qbWE4ph0yxyTWacQjxI4JPcnNY5anzgSYUlw1GgVeram2MVZjwxY/q6Hxkl18 w+GH2diaFT/Uzs2mekoI7Wr1hAMfqmQVIeFHDUn4KlGEICLG0/HMAEsyHjVhxTMRxuRkvrWxPesg ebrf+5LvW/VsIqRnSEqEs29EdTRORsKQ+oZWLA0FMUzAocTlWWAaxotG4wYhuivoBxVJLh46+VCD LGGj0Zlh1fzDAmUvsET1k/v6r8unOOSJwLKBUC2sTy+d/wyMdeyI2FHWHB3Uxb+sTaoXhq2MdrcN Muqxq4SXwGj8UG5U60JluHrC3XfGtmY5g90p88YtrlXDu0er45cA6MJq1foJGr9E/+yq/H7woqDD ixqlI1LEUYHdU22ipGaPQkOpyrMmY1QTemsdRfSZpDYTfSTUmb8SheLexPCMZyomnrjYBoS+Z8pz nkROz+Vglk/UcYqbiCUwEYbzApvBkJOSTOi/iTgvIqKCTGWX+4hxX8HoGzj5ftSSmMk/jfNBhhO+ MoHwC8HVu0gJUSMEbRw7csSVBwdNLeeadeAt0JxBetVZWVJxNi0QR9RwF09UMkDLUoqsYwIpo7KC tj/ZPj66Q/zyccitAD6rJ5udgN7VAQz2HtI90468ZzRs1NOIYstevmbXUadyzDcYVTFiJIVkYHIu 6XxEEq4gCYvaeBP3I0F49E12buOkZwbK2xb+oqHAZVwE3wh3L+I+rRc1UkrLwVaVO1PUyG75Xz3P vY/VeMZhj/uWOM1/b8hI9blnP0fIoTB7mOSKZnfkwyGBpYGQ3uglNsugqQGAcEgJlQQtf4VKOoNh UpO+hqAWfdWwmPCXMtYxgyzi6oMq/MQ0fh3icUfWVlcFRjxzRdTf7a785dJzfD1x8414TopYNokY 5WOgFXd+TjTwbGxObwKg7znH2XNvYQZuKzlEfetLR4IAF5GhiEfsbm8+qoqlqHChF7Fj5fRNVEQ7 IRWNGlpSQolb1rOZnmfB4Zu0wKm9qNKXZpiWK12Rx9Nkw7jsvjXSoyZ11YgpUdenbBPrVf9JLTIK D4KvPG2WEQvrfcUVIuG5U43aLBNqargSFANNeLzq0HXNVH2M1scy+Ror9UH+8dkwfA6bntbLa9KX h8JLA3Akm8Kp+ffPP//ss88888weHMhg3969B3ng4v2PimlPhPeiaDyw9ny7h3uyuwXFB93RNZfS Xg/PpVsRazIaIjOetuEAxQjvi3u+eScvsLPN3/jBe5mIhriCOw9pfJKR9UPxdcvoGKvbsIGCUXEg wZTukqc5oOFaqdeov+CUj8Q/Pl3+Vi12IwPjVhP6EWOQeYaGBaOubcEkiWKuKiqjzvySb8FTivb9 aBXLGr4KERY893c+/egTEhbVtu3su2I6QK+a7oZrlp74M2KUPS+vxrfhB7bQRWT73Br1QgFkCIko kuimUe33v//Vr37x6qu/++1vn99mLf7YkK9xeiPue4yGsiyjlO2XEvmDaeeQ78DoAsWd8JLnKMQz GZIZGJOkyYCpuuboxSOa5IuMEKEedUmcFvVVfdjr5i1C4UZeNB43oxSoGxt9VEpFLNC8sHhSoWZc qmgkFLJFosv+YXDm+6bwZx0P9eKNdekHEDHOl8qoqKNoPwj/+pe+sz30K/N9c5PXvGB30F1reyA8 LU0NpwisY0eC5r4jQScWyerEhRB6HP98iLlXCh64RjhZ+gkAFCrPGyQ1Nz+zvfwVMWSj6ZBvs+6o yZy8qEqfqB1i+4X4zqp2OiVK41l0UBXfWAsnaosfZLq4oRDPhsC4AYyOgx+oavWRvIip08aNDHeJ uI6zi8K+E2bGvPR9k5tFjEkQNXiUN/WcdSr7jekmUJ+b/0nc0phvvE8mcepmqMUu/6wV3fJ3ngFM 1OYWvhYXfSOy7GfwDGNqXhhocycULKg0r+HbSp1Qm/3M909YbXEa3lWr4bNGle6Xjm7hHwHQF++9 d0KOdIyWlFvh+GXxY62hUIbfvH1m809MOhFRV04oyMoBKy6ioX3d2oFe4LMgE/airnQlL+BH7ShF jWXkm3qr0d6+wUlgNxKmhqY8vduzXUX4aIdEUmmhRXdhI2pM6JQo6qmTJe7kIbNXuMTS2em2gKdR 2fQORN3eYGJd0C0n8kjtTuSFFmeeH3FeazTq8jsJbxGbFkacc+YbWW2wpXuH574l7w9GRslZ3O5T rqxj/A0JhUZfuEjvPDLP/4Nv9gsyqmcD4eVq/Jh5GG+MjGgNEAcPFZisNQtuFEW6NplS0ExgVG+f mfpTNc58W0fwjGUSsdUHS+q+MktcTZO47wKcJvNSIbAYC3ZkV/sIQoWTAdHAIPBsZ1HEYVPDleeY zcipaJAmiX6IWkPJFuucSBcTyWLAGDy8jIeMaOtDqGbSf81UWWxUpQT245Z7jGcaDQKgdI9HTWOB tSciruriaWuCeEz2mwhapIz6840hLmNhiM58dM+KsQBejti0NcKqBIGpwbpnEhL5GMe34EcF0PkU Vsbm0ozDBRxvr4hDgeFglzikeD6Fdde7u4dbcWQhHmpIZ/Y06+GztpfGnjbDajNdq5BDo2/J3JWe 9NsOcidL70FibdDoO7EYMfumZ9oyTKk2qlUxYRsFclS70FydzH0a88m0V0QB6LlXt3an9pr4lkck BYobjpGBdhWzgBtdwLaSR1uZXBNcxJRsDRg9bTbwjFKJu4hnWMImC1HXiOcpQePv4q4EbXhIP4Kp X/sawz1TE3YMKGXnqMsWIpbExL0wskCMU/MBnBdj/urCTvxT/2krD2qG5fqxGjUWisVx7Yu9OD4i 1vnEQV1wiE0cXCovB0zkgaYALxy1o7vn1we3lb9sV6FTmdHA9gj2dJNqK294bgRMMNB2IV/lt3EN VRL4wQhbQlY/MUoqs+lyxAsGUJ0CzwvIwXdVFM8PrCDlCzPcRuBamaN+uU2MJPrJk3zbhBQNTAtr UngGXDZoReKu3SROAMWdHWZUsuNL+Wd8Sz6++248z6mqIBtUB4CRVGOk+y4jhu+9IGKZTgfnWLkS oO9ZHyTI88T1igffYjQSNxDaih/tpj8P/OA4Pt3D3UMFHEOuBceSy+M32ZLq7U0WWngQUN6Dg1zi WPY49eZyTdvxc+Bn1gpU/erb2qnn25ZC5RFf1LU2YGi/qxeNOPL3bKJieqc9QyiRoA/fjwb9RbYZ VqxC8xWZ4Ym7lll1BjRRMyI3avojfX2PuK+EbYnaGM/WZ4h6QcJsQoehrWiQLduvPmKCcoTLPnCN /+NxhC3JHTWY+CL67Z7kRW2w/oPR0MbCj1rJFLUVIedjRFz3rxcS/F7I1xCq94OArQotolXGiBVr 1uCQhjvP7sLRuEkkIqHpDvJaVfg5bJcLakTwwjF8sCQ1DseDY4G34DC++SzWGU6SiPI4NivAU+CB G3HAoFQOz8gVti8sfuAFk1i7Bg0DaLvX+DbZ1CtRkyioYehHvaAgFfGdWKl246Rx0XeMTiHl6Yv5 7plOFBqHwDOx0vNDmiHqVEXEFUD8oIstnLhHjBEuPrI84pkqSjRq63rGYfdNzukE3eG/YPvzn//0 Z9lw/Ztvvqkx836PKzn77pPbYkqoTcp060ZtnmgrP/aKxj73ffu2ABl1CWnEFviceaFIibpisPvW lbTUUY1GTQe6pxa5+aoubClf1Miy2GeGhnHsBBysqLU7h2NK9RYLjFq5FI4vjoM74SCtuA4KQvhK Mowlc9mm/PbSGMpfpv1C65XhApDUIkPiP2o654WErJoLeoWMEPIcHXsRoxktEF3SE42G44YoHtNG rX0k4gB51gaOusq+HxTaTM4etdjwzfyPiO1bsx9Wfkxt3UgqX+WbIR6diRSK24e/Efj86U//yVP1 9qfDUTtNwFaQTcDwfE2uIy53lE9r3YaQo6Y5p4mGnmaxggtbCbKT8ey35KuHZmPcIV/TmqhrwHO9 dNb+Nj2eWsbbAT/QP1ewbjmWo8bS5n08dnyhFtDBsRBxWO8kjyOfx4E2m3ADvxBETTjQLw4lvb00 tvesLYCbUGRtP/2HXLZjhyzqut8932Y91o+NGrz4undaV840e5lMy2Ypxu42qawRPPw64qZMqy2t 0UhVUTZiirCeZ7rewgJa8ylfM2h9bT8AtPybrlDhO1IwUcez3oQfrf/mG8XPVvBwOxp0aBtb0Q+a QkKOmCnRWNfQ0KiN+UoPFreuhOu58BSxwdy4FiZDN2XpSGieg2+scs27fEuEvuFy3D4aHPwihB8c IQzgGRseHmlB0tWLo3HgQNCpphRpB5DJ5Qo4WCKRxcNHZ6/XZpuKLT3bZqY+eUijVzwoRnlB6ij2 sB+1rfJOeWrO7Ed9p3xthIm6BMJ8UZ4txnoRV50FgXmu/UykdsR3ClvlskGx7ypWnlXx5lvyIs4D tHA05rZv/Wlfy54KTksVdmeNxoP0m5TqqRzjTYuf//zTdv75zwtquJvP6JvWKbVAfOdfWYvSpu/a 0G3f3ouGuhto95jvzjNNScHeEvVcXdBqzrjnhXZJuzN6Ou/FRi1Xq+TXcHQr/8gs+JHusREQ0DBO vb29ckRCyGcI6d58dyHVi5wsX2gRBVTM5fIFxK9sy/aZqU8ELfOeGR3X+um5Omok7gzBiLMlvIhV x27ftZZfVSeWZwO5souTGi6tslV8M7xB+43V1dHwt21Qps1kfiB3TG+aFcl2Pw/UsxokdvYR0RZ3 JQzPTkHiXBHi5y+P4J8/HfcjvlU9tp9OPqYXdYiuwnw0ZNJH/eA79MMPSZHRd+lnmHdtbdAqhKjh Kd9ZX1Fnb4Sm4gWN5/GjO8Wv4zhsghyXuo+H8AbZ5JJN0DupPAgIqEHajqysmGuFku7tbSpAWyez w9tnFj5BwB8KzHLR/54N4ebze5qLR0OTml0M8nwXijX19LygHm3rX7asY1WGF+oa0nGOO4D4dppQ 4EVHo2E82sqKyWKN0yseimmvMIUuSap9z6pY33UjuSaBiOv90YAdiTPpijn8bFdAf4rbNignbY0/ GbH5gmHruBeKZU4+m2Q35Iu5/jni7Q8moPnWBLVWome/GosUm5lEHFfza42H7FerUavsw8OWf+Jy pE8elqy1N9+bzyNmIVjBPcTx6pty+RQO9YsD1+eSySLMxSIglc1mV7bPLPyx64gxu4NrKLZ+XSTc 7OwqRa64GQlildSw5dvwo0FpMygwGu8nYrPsiM4LC7UkRzSUBXmJ3b+D/j/XLOTEVdD0YdvRbGeQ jqFt6/aMDtGZsFGzR9h2Q6OqA/wYAb2Vf+JauDM7jVelejRF1ERE0w9TvNGZlq4nyPz7cS88LcDS f5DCayQL2dY2NoX8S8/upXFXNTJR3rHeFvu5XhftvDwG6QOfGccI6ssle5MthSYeJjqZ6k1yo/LJ pYCcbA4JGXR0Llnb1LodPz8x370f+L2u8mLNNusxe5FDQS+X6Sv0vKgZdHape7a9yrQwR8wEVVtC tkLKNyrYFTtM/Tnikt5oVWiyrY6+b4fdmmOeqyrYtv6oJRg/ak0jP4i90VCR1eSTZt4ZaZKGT/x4 gJ/tAezPip+o78SgzblNY4CZqWuDZ9QWCeMR51VFXOtZJKho+GYCQTD9xfSl+HH7BhFtxtRijMkd A7KP2tzVGjL2C6zGz1GzSOJFHMiFEnpkDMeWyvMwrEAMIhaOy/zZ6cb3TgxeK/T2FnK9OVQ3sjxQ fTIP/tlWPv2paTqVFNgofd91n0YCHnFZkxke385vtpahYyk30Vn/IO5itw3TRgi7sobpZXAzdcxD QYeWGRg7GyQeDfW+RtxcaOE/zePjGvc8NRA9M2/Meka+lsZMj6XNBeIyVqCfCzV/g3/+ovjxrSjR 785jvTbgS9f9ZhNDMw3KmLGIUvrNObHkTEZfNZl+q8aCVQzGndmjyTKbbX1b/LV5V9QzBGsnB/Cz HD+6vX2sJvY5piwN4whkYCAcABpYaQV2qJkL+XffPn3qs89OAEmpAp1nVsbkYofjej9tmy2dReGZ zjudtx4oCquavXBpxTYpBCmnyTeCAqpvJ9FHDEhN2dvVQr2oFyq0+rbipVTkakDiHni2sdnMFrPF oYidquEe8ZzNG7FZou2Bc42TxvEKinXsq4zHL3wj+defJQX7zy0QAn5UQrn5aWp1KvUaB8HUFTzx nfygjcqwoopE09Rvm3iNA6V3elXT2mz/XsQ2XEasfiZwPdc/FEyns+6B7OSB/XPBHTWyJvbx4JjU T4eHW1igSPUW+3KFXlZS8+++cel24ycn8jkUvZKtKGAkkZXlIIS2zyzc/5RvhYEXN+0MniWZiBWl 1mT1w3lWyLI1vo+rpvuBRIoGbYiu2m5aQqPBv24nCgaa2rNWiZmbEfScapJrO8OcC+AmrQUeuN27 vUPB/KGIHwnmz3q2YKa6WryjePxojdCPEtDW7ZvjLMabyOsSCt82Qyk/EOgW+8ap0JqvmgRmqpB1 3l2hww8kRMT6KFGb0cejQfOZKkTf9Au4EpJk9pZmg/mc28qnop/fx5wjiB/Ap6U32Up8QDHTiM4m 33j33DmsZ5qFAdSEu1HPgLBONWWLO5W/orbTx7TQ2SqL6arxbKeADqXnSMjWEa09F9d2PNP/biZ4 WZg518UoJC/UUuxqYb51613ntemVcQizvWG+6xfV3ER3Qt9OrtYP45kOJVs10ImE0SCCmtqsvApx I7/x+PF6E78MhKox9E38uDbWa0+dH3F2gX4cu4KEHzQBREK4DrkyShNGF7tuTOufhiaysYs7EglW SbEuR9S5c8EcAdcSESoQb8cPAfT24Njw0PAIjgzdKkk77B9W31Fmz791+tSJM43N2XyxNw/XJ0X8 wJduyu6AnxcirkjluQJVJBK0+NmkzLeTWiStCPWH2QRWm/nCTWY2Saia2mdagExHTqjCHJ6F7Nuq httfbYwybr9rO7Mf1Hc5bCQIkJ4R865S4eaxmpYxX2uzMjyUPoTP8XqNX3+pQbXrwmEslIwjS9iQ 9qeYYMx9XtdGEnU5rMZ+3zPpRcTU7Vy3XdBD7gemmIni8iHjno1kqm/8Q1XTAyzV+2Z30LUHfHWN 4iasHooGIrW6fHrYHMUM7WNjQzz8YXd3Lw9Fn08WYBUWYPwMnzrf+N57pwZTecjmll5UTlHY6M03 JYvby6cHL6qTGursUc/WJFY2NJiqSrDGTyTosrExzNMFhHxXBPYiXtAcEiTzTm+Zvcp3X64faPSw hRE0FvmmA8/kiNFQkVsFTNQoDGveeTKOETfHJK6KVESmfE5bQUHapeg5fvwwwVJz/DKPzszjH+EM PXyXz57140cP43EKoLhn7QCXKuuabK7c77lpKO571Vm0UvjRhD5iWzGMxeDHD9laqdkDoq7/07Sm hzqhHIfHDRy59BdFiOfimvxvO8QvAOgt4GcYB4TGoTSThSLaN9Cgkc+j/N4yfP782tXVtRGKohS8 oZZelucBoe7t5VOUv3xbVpH5LpFgbqgXCYrqvumls10udkqK7WoPGmeiVfODTaoSDZpeo8HSZFbC anHZjLIXCSW+oR4/1x5r561KYhU16Z0hO8+CVRr5zaIyVdOIhBT+YGbN+n+QIKgvESeCiJ+ab2ou cya8bJgI+D5WuPj4C7OaGJ/hmzmrlDZx7cs0ZU03C996F6EuRTdtmwmc7zwo7S63dqjbrVyRznX5 hNqGTYbqVdWujcKwzSx2JkF0i/1s+ecdzM0YRgoPCME97O0tkoFy+WRrsqXh3OqVE6vdhSLyLuTw dIN6C63ZfO/JV7eXv1xfl37zYlN4pmFS9lM3ir6dpRx17pWtWboGY5tYRe0EQN/sJvGQAnD2si3v 2N6cQP7Y7y/Uex31XB+V0YueOtl20ikb0uJB/c23MS7KXuhIaAE1a/6acqUYP76v7IOYdfntdz76 6NP/j7F36Ykzy9KFu7q6qrqzT1V3qUo6kyN19+ToG3zS3vtNbbCpbL+IwBYWYO4JEWHMTQgTEDDj NrCJsCzLlloeOVXyAEseZEuf2j8i/0FV/aLvWdf9YmM78R1jDMSKvdd6bmtvj37MLOIFOxZmOVYa gRahxRSHPsrRBQFKXhVMoujl9HOtpYF2cqHMrIUy8vMr6shbOYDGVjZ1XupVVzEKRrJxBgPskYqq rNJ3fb1+bt3SVWSbz9i3j+PnAmcLup+JHWzzHfZXVp49uXPv0fff99dJB4Sjp38OQh7d0WT/2b99 Sn9p72ly4srICI9fMQ2maEfVws7xPjJNBoP8NECBn0fmbE/JHPHqBfXH1X1mISou5vgr/64uShgW 5DcGeUeNajeW+YUYjTa5pqWVD9XziUwfWEUzyeDo57On19uHbwqxiuN3KL2JXo4eHh0dPXx49BDF tLBBsjI9LKUh5xO3UrTP+hwXJBRlYWwonByiMM8bn+g5Reegq9Lg6Tzs7Ac/ALU5GZWxVvNDI/iG j7Qaz5z6E/qC569dMiA+g36sfzFBOkOMXlv3TobPEPxw9ejpu0eInCF8eghp6+TpzuQ5NdL9P9zg HhTg0zp6h+bVexELYljkE0ooFzVcYJ4plcBDH8Qrd67WPvT7d4WTUipeLcG9YlF7eNJZ5Q5Pc+Br bJG9O/n4a/XSEFjNNa6AVVCK1LCbKAZtJVRp9Gr1aHfL/CheRkZHRkZHMbFOTLzZQgoQSgq5LHfu nLSpfoJGGamfX+IeHO8067JXivPj0QQO0bGzxkHuck/lR0KBzXJycETurEFQtx69h5qLuDLjtKMg Ubrq2iqnSZ9++93u9xQRMqT5fb1PP02eDx8gsOYRokOePsL5c28Tf/tiCIx62L8YPgME1D//hD79 fSU1K5FLSgaIxjBpLFkDlQlGz1dm6Eo6s2qzGDXlgA8H8VYVLWMpzlibPk/+zFL6WpmJWs+qWDWB DHtAHGQwtMehZ8PV3BJU6bWRgp08psKp3OOvY5BfXmNj8yOj8jLiP/HPVE8vnmRSIFaugC8mElHz RBkjzIImqmh7YiYfzE3zXSebrwQyFsarEC1R5buq/az8SGPQtDKio4qpyHSVha+jdZutT+l3OoDu wXePFmgL5w8wZ/TPkxdPkKZ15+Tpne8fv39379HmHcqDRGLbKYiy0z4UHJPrn7oHU9FRyVxUVdar as8sH3+D+XVCsOm1UO2ucYtVUSZyKZWGIXmCR/Mgr6yllmY3NtyhQv0nJ+SrRuyHIYNV9XGbrgKz pBRLsCNJnAyDmIqGRokvlj2P3Zov5TPy0e+mXpy0SBedFOSp5ONVaSk5MWo3z8t5XhXTN3Uu0YfL ZM9Fi5Ao2qDATy9tCKNh9kKsa/lV7qNX417y+7Aq3YYOsZ/Sp1Q/Y3duU0zkcGvllMAfMOwY3Pco 1mEXQbFYOfn9A05HQ9TH90+efE8ZWv3l/g30qTX9Ip5UMLEy344GEVZa+0oa6Whe4g+0pJwzjEai V2rXE42BqsrVFM48VWgox2KR9fKJnCwgT3syU5PHhn+CkwjU0+PSMMn90BmRb5qoD7fU34DQRRmb BAMKMrujgLx+7NhpHEdzz26PjdH45QcPdSImmjAZfqU0ohlw5J6NtcZsN7w+qcBl8oViho9EHGrh NUVKKpITgyKqKjQt3RJ3KshsZXKym+BDO39ubd49OaEGaGtI5bOMCWvldGF6cQmZigsP720+fTS+ ubf7cHP3wfbt8c0nd05OcK9d/f1N4avBumWKYpb2RTr75NeuW3GiTgaxqD4Y93FsMVmHKuqMaGaf aybnqlwi0WNYr3l8TJJpClFvj/ncHshHHC1cUfiCEjUlfj4Pskqc6FCZFdhyqS2XRj5WboDo/BkZ naKzZtR+KlfZ3Is73/FibPqk6dyp5UGsPR5AriCVI7pc1bS9CuMb9iPqERNCqsnQ0M6q4UCV91y7 NkMn8xzseRM8LKbYM5TFTbH3CX1KBdS7d0KnyouL/uk5EV0QHq4/XVo62kMa1vSHB3exMOfh0sL0 7BFCjGh6QDrW6vin9Ok32uxFBSWc+I7uMDddgvroTDMQS8KORETHlErr74ka3BBVVVElah9QiV9V bISqeq30a5NqN+9oMrDhTw3hmsnWXGGmBhdpk/lUr1Nt8rzmGef2X0s05+G90gtsnqpmZKp5BtnL 3IvN77D2seGXcCmP3a6uCGgIr2W2Zr5c6ZiijYrFdefSj+gZAwoy8QXrJ2tyab4DHXbNSZNeuVcp 3gA/q/s9nYyvolFGa9MH5kxj+0T/ZHbvCBEys4tHD77fvrdL0MXu0eLRNn53RCG2j+//ww3ud94t wJBP9mCIGNzR0pAXXE9zMpGvTthF5RM84Kfid21MRhWu43haSbGQxkHvN/c6ixvcwgndglAEnyWR lcH86EZ8bl0NOjQ5hDTMeK4o6G5+Grrt9AKT+hm9Xj5aRFMvdr/F/cU8QSi3kaRv6URVqz89NbAe C/Oz9AlPPasaybWugZDZqWo6gtx0WhUNTNBAAL0MFMjPlhNSZv6xm+Sr3x5y9Fn/+WUf+M96HzDh 1uTdhcUFYBR7S7vYH/P+weLDvaOHD5AV/ODowZ3Hd1a/f/cJ/fV33whAlRUDlhkw2oRZWf1UxRHf GBPN2GOEVypiLD2dG9iwA9G1wxqVAh7aPztiIqmcOiJV0doJO9Pk1dky6UoRWmqRZpGp/Dg18oA8 4SpV/lFV7BDTBmhsrNH/oGJG+BKb0uvsxcKfekyfNjwbnCcktFXUx5Mv2mhCMV0RUhHsraoc1ejY GG9qoYY3vqpiVSz30XThKiBLqaE2KGtKPHjPc43pvx37JH2D6qf9ApP76SW0P32gg3CcwuAF+BBu jBezsx++v7f9/Z3de9sEe+3evXd38zYtGD27oX5ohrF49yi3qrG7ljUvkEWUGyo6bhOLcTMVHURs 8NuxJMPp3zW8c8F3HohVThetpMrj6ENxQjEqUilHGKoCkBlB51ClqxX97jVE0VlJziqU4Vl16bES 8rTgPyMjH51BU3x/Td/q1RYXkpN9mLEOMRSZRq20emXptdzv5dTIpSoW6GTuXXwUNSsJgjfDyTxK phUKpll1pTd/ALW/Y0NPLJuJPraPzh9ZsfvdxtNnz0/BX/Thf8f4vgKBIch36A8n19FAb24//n6C zBfr/S2wHIR/PXv29IZg1m8UsZAOVON5RJUkalTkdfm4o1Iz091aMkq8lsSc1FareQzFeBk1XSXF hoba4UarSzqeann2VSWiQNOEYmX+ZUuUDpJKoJdgash/RK9oxIfKK8symJQsK8ucC5XcX2uolamR G2+wuRdr1P44xRfFNO+J1K48VOGjdiGGrlFIi6/kC46XJFMzNoKsOeyHcTNZ6Fd5zqcHcVc2/5eN NKUPq4pfur7h/EH9HCDm7hnkP8/gPyWvzim8OuSAn1wfLkx/2Pxx89EyuNMVkpYBmZ7bgfxn8oZg 1m+iDQV6O1HdiINdNhAkmX7VDMp9c4wxNsUImjwlOG9sGLe1LfLSC2ZQN1NCI56hLpGuoYFHp8pF PNK+NCQmSm9Hbx8rV7TL17fSmcAOSEOI+U3qqMeYHXk1F1A5f5qds/XP+zh/aj0A+PONJeAumORP lQRVKF4+k4upljZKz9RMgXXDgss3zSBl02mt2aCxsBkixdRJoGAsoZme+xH9NaabmOdx9IDteg75 xha5drYml9dPd05BpA6n55HBuPl0cguIcx8sGBI5zqmU+jcEs35j2vWowlSNJlB0J0rCTOUMFr9J jnK3mRlJyovLKnpjnV2V5j5cd7PrraJTrsUEekqQ2TQkCdqCpqTVsAaIGZMQr1nDzEYWQpMUTibI bViImwsZuHRTJdoxq58pubFG/Bf6zdyT9hh1z6zQcfe9F73eMdEew4by0Ad5DekJjcULGqddeZqC k70WxKYzsL8LfpvsRs9k0ZEKtAQrVOZoWmM3yseWhk/6RFv0IYEGAEQsO0UorK8vP5neQBv9+MXc yjJRFpAErcytw+EzcXrDXtRvgi/VISgXLV7KlcZhylmkjv/oeTWV4of0BctcNKrI1NtXO/Gor1QQ xMTH0Q1d8mbFIFVEWEkL12xhscjPUry+7CJFS7/yrIHKKFW5/yRrLqlo1g/66FkwJLqSEb7Uz+hN xw/q5xjtM0duxOAnSQOHj4VEt85Qk5FDjI30Q704tfkpxVaA/RRL4GZonKpy/dVmjKkUMwqleyiZ S/JOWp/eX/R99smL/sUppSc8P59Dq0MO+JUdKJ1xSW/ML+ztPYF4A6cR7Dy4wGAu3Nk5vf/pXtTf q+9criz1UkRc1PYActsc9dxMukaCDxoa92O0i017cBmi1d4fyjbeIOaepMKVbJRJjG5SFYxPNcKl umKJnuc6rkOJCYip4XZw5aMZgQ3bj5YvllJDXaylq9g6i7f0/ppqoj4KHfL8Nfc091qyTaPyIP5o 2Gm03UXRh3JTPzYDqkzzouYu1TWXISMRuWJxCaXYPUHZZwYx8fB4mqqGuSU0ItzxyhvPn2+/e/ji 9BTN8xAmVFxg+LZOAWQTOIdOsKlhaXbpFJA0Z7eskyYR7p654f3/BxGsf/jNdfugkMBaJZYoYBkF DROy2zOSj2kqMtZGJ5YoQm205UaLGt7j7kPPrKF3le3pFT0hRs/jGJpwYUn6Tk0PUFMpr7elALVV Iy/N7Jum0pUEGRuCKssbEwBo3nBn5bwaOPTc016v9ogqES273ZYJII2m4f8yp5JPxYeMWRHM5saz uCn2mhm3brAzdErDDoPR0IVQNk5G33sW15JGsFWfhrd4/Wxiq9DpJSRk/SF6n61TUo/h9to5Pb83 P4P8zaVnsMRT20PsGNyD8PZccTDi9R76F57jFBvzunc2Mrfjgc1aOhJlqS2nXl7aYSetl8R3YdJX aVi2gkvByk219sXxZdOfD3U+n+sfYnGtWuB8algSPM2kEa6aLBPU3FUpWjKa/ovamhdRv8O74/dX k/uy++s2xi85SKtymVa2cM9CMpvdTSWWxzSo9UiqLewkpmsRUgaeVw3JbvRPxK5/FfybdqiRyCiE mTPHRSDaa2xOMfoUDdDtC/K/A3zeGsLaBaHqOtSHlJYweW9xcXZ2YbZPrp3z81PwGlsQsq6jfaZ8 6Ef/8bH+UM8ZuYwUR2bFXjIdtF8q2jxXPqRK4alMQ7xY0TKMDBJAWjSFrcszo3LiQ/6Sz61GxI+e hDH5EoIkl6X9SStTIU5NPDFDVShsnUn5YtmC4K4/nbwqTZRSIyCDiK3e2sddTymiuXu3aonzqQ0B 1ytSPoFop2FZTmTJxNq5RMNXo02l4qSvzDdWVRbbY7YMdeYaplwVPWzTPKBqDtfv21z5cfiPy5+f UHQUOqD1/iWanPWLSWA9iK6DzhmKDnKloqpgiIcbY4KipRAodS7182+f1E8qT2W9a+WcUYF4lYpi 1W6ioDcTHyJZfTvZ0Ao9zOQRPpw/2KfleN0cy1SnLXf0LwIKLGk5euKvzXwxFfhAKTcHYJKFkctf 6tdzkHzuiyVm0xcQ6F3M41xo9A2hsvoZKcUzMuWExtzmrboZ36wYulppjEKtpT3JHvWQXDPukKhS raZZKBGQtTkv5UpP0baK+M4iV76YwsbzAt1gyxIQg1hvoC/w8ifYlmG9oPiNU8o6XJ5AQMskon6Q HoXYTPK7Qzi/joNnGc0RGQknlx+9R6bvRz30PxwHl0Z4YxZtwUOMjbUC0dY6aFssT399yKO3zgYk Cg0S48abF9g+TGrQhw8Xlpbm57F7WOpJ6y160y0lkEPyDlxqJ6YSGS+3ahp4OpuhVbrwgpBl+hCz xzwNBEzQ54WHn0VzNxr1RDgkLrC1a0fOiHPxQBV3Zv5U8z2nrqYqVsXmEDwpschIbCIN5oRUJCtG v4lrAw+jL42NViOEX5g/tiwbrgzKSsUVXhWMNojNLeqqPdAXvd7YJ/fXn16gb35+OSQMCLl1CF5d nxg9pyNnBafR3OTy1BTGMRq9lqcmUVX4PoElGe8efZTg8k9tZR2tCkL0WOvGCouo83zKbhs16U3Q nieWgI5IT215hzEevH759sTWFsu+M+xY3NzefTizuETrO215Z2Y0wNqoAnhUXsKNNi1pvyTjody8 eodIOemnVMjWQn0pXBw8f0alFUxjtNY+urVGVHyIKtqZ7bnW0UBCO+MqS2KLoWz8UJtlLNm+pkfQ 9NW6aCUlwzhVJh0PMhiGpp/ADG98F6biVfXE0OiyDVv+Gaj76X18/nzXOr14dkoM2MXwAughmhz8 WDlHgALSOKYg6ACdsb41B1p+EvJ6vOA8QrP96OPd8IhfjdYw8wOR1TkePQs+lNsqKMnhx4Jifgoh 6iDl/gN+befly7cvn2G7GW+Pxc/QljzYxPfy487jx9vbRw9nFman7XiierIGSV3LBBgI8aYfiqgA 9WOzu0zaxxit54x2ppZncbG00XGRpefSgVzr5/oAZvT73HRPCTph3Jk5rFUq0fQiB2sc2RQVXQZt Sy3KhowSBGmpQipt4DRFU9ZbgJnE75Xsj1R2ZiSXAvCzo5YqxY+xG+SH337bGlLwMx1Az5DtDAMh 0p9xU+HsWVmmcqHsXlxf53Qe7ZxTkgtcqJjQTu9/XD/yyA/o8+QbJQVvWv0Y8j1ZlswcLclUmIzg xJiKekKZ3NsvX7958+btUzqBwOTi5cHm480Hj+kQeszf+IVqCj+TWoBfiXpaXIBnhlbA8nVH/VMy HXqjlA12SlkSyLVs8DHQpxPlOuT+JKvbN2rzEWUVT7S8RfLBSP1w1zMy5cSpwtA7ay3PF6x8rVyU vA8TWmgMaiNRounJd12MR0k2NInKz8XU0Ax4w1xFj+fTGPVU+f5rKU7nzlLRntU31A/650OgP6eU Mv98i+mvLYpIgFNnhRKA1jGwbxGXipMHzPzU1g4N8fj9xPrwowSX3/xzY22pPCipsTKCsgeyQsrG cWV7QpsOKkZLhEpuqrKE0pjab16+fPnm7ZvLVSodfH+AbBC8bOPM2eafUS7ygsp58JiqimuJi4pe Hst1x/UEB9Z+m/6L7HelnYj+nA86AebKkYKsZyydr+5iiIobSD5rlge8teHajWtDGBXTzgZIs5xM KS/XTdaMDQUHUkms9T0iqluOVUPxZbJ7OzYq+9JVljFQ3BuV9c4cQcL0tnuxfc2oR7k3FOn4F62x 3k3982H/8jngZ/BfVD4TK8hgXe9PYv6aRBL91vnc8hSFSlHqIcBDWmowcT6yMzG3cvWxBPG3SYHe aOJt98/q0z3arB2NZIlZjx6dn6PC1HpK4eLmmpOb5vjNaxTQy9cv3zy5c4cOl8fb7Kra3eZft+Xl 8bYW1ONtryb6IacSaoj+JV17eKOlpK25Mbm6FcUSl+i1A+kost4slbTgmT072qfrFJAUzlFctHXQ EDyD8jL0mc+fgzr6JipOvy9h5L73y8BKT5OwGJUqGm9ha+tcU5ZSqepG7J/nKKSqrGhxhVRMFuGe qrICyihAeVJ8Ql+o/PnbfQxfkM5zsRBueEFeZQDNffiY6c8rlJ2J7/1lao6WV+amEPCCKezq4wSX /6VJ6skRYx5h6CnLkA3JN+iBYH7B6sj6W51noquHhG+K2jvz43z8w8s3KCEqoh9u30VV7O6SsXPv ob3Anbe7fYSC2t2mcpKK2tWi0u944bLapHttKSUf/xnd5I9rIGdPtkxA35BD1ZRFCGqPq0TVZAPe BTOQVXDVwU34j9TU645u2PDjVRxWktWvrRGfNDy8U6RUrMqCPP3Py36HqkTF+sa5aJxvckNgsGks RA8UbOyTTp7kWTXyQKzFat0o3/h245Lurv7zZxeXqJkVoSh2KIMVjTI6Zprk16doGKP0H7rLwGYg nuPqYwnZN741p1J3yEBLRNppo6wUkBBqlcsoG+js4jLV/Gh3kiMvDOy+pdKhQ+jNyzcvHhztzmCK 5xegnAsLizP8QuW0J8VEP6imuJ7shLISQvk8nI2i01QgKWnVJ76biJejy4D+dzy4g1CAdGeDk+1q iblWpiT68qGDj++v4uKZOLT6UFQ+WfZeimUHTDK8ldnTmD28IVUN1wWp742Vj0JeK9lqWl7lt3xt kCw6N1VHFAa3LgsyPJMmNpb+VL2b5T9rfdIaYn8BAhQwesHdRX0QamZyuU9mVLiZKbwFrRDOI0Qg ooomRucmJz5J4Ph9Sj640NOSn5jRhEChG3WxVjRkmL9Cmd/KbjCZ4vngydYr0av46MrP3rxGB41D 6M3rN29/eAB/CIzk8kK/TtMLFdMs1dPi4gxM53Q8HUF8i5ddKSW76HaxH3RB+h/rx3T+YzSpMirX sB5p5aI2DQyeV4ZlGX1XRwcx8SXYbxTP1FRjGJsafX1YK+BkPbyFp+jXRaaO6AS4AJ1ZBLbSMvNH WEf1iYiFwzxttglSwdo6mQKEgefaZAsl/8qlrXrc4c8DVxrILXcT/QX4eZrCf4g+7V/Cf0EaVqqQ 5cnTCWmVceAgFXGLZdGUBY0TiQaz5U8CxH/p0Xs2kvMNJjWQFcjTL5gp5vnhSsrL4+LIMVm2WrSG yGVBg8vX3P1QD/32hx9WN9qd9gbWxR4crB1sbNDvrJK4mLDtk8qp1BMOKLru9riI4EHfe7hgCHYs kmwNcqP/faAfqrq+ghh8gx1WChpF7oGjt2k6NEY+fz7ivQwCmjiOmhGq/0PFRYuvgLa58hQTE5cz awrEykRqa2LVXVBHi46i5W8u0Ga21TbEWpgyFVNmeLvylb4WnGVWsNDkl/kxubF+IN+AdhXh4Wif T8/7k1CprnCRAAMiFeIyNUQYvpZBoPaR20shZDSG7ax8EqD5C3kGZSJvkxwpOStWl8PAjJEipwxM nTLEqwohOdDtFYrZpEbEbkrPXr5+TbfXmzc/XL44OZo/ONiQVJT5jXnKRdnH9338hBckpeAv5un7 NJXUtB5PvJlYygi904LxFVHFJVy9uSCgwYfFqKNacP22YUTODZtURvZ6yPkzMvLJ3cX9T2aTSpDN ZbzgT8pQ/vPoyJKEX2Vu4BPnAikuWllOs/XWyYXCjDAEDRgW6X/WgcwS7KuqrJW1FFPDfq6ZWqsi XvtM/ez1h8+B/jx/3t+avFgZgvXaAsUOORm4+IktOmqACmGL0zkMqiTkwPmzgxl/5X9/rKD/lWZe VUGoyZwFQoyWYVP5/Anb3oDI0KoRbReTPOF1aLeUAmPA6MtzgvqhA+jt5dPVzcczVDg4YEhjsoDj ZolOnTUqqo2DjYM1rqNOhwsKsTtrfjzRkYQ1xFREiyZDLf9HKlOgFlRWXbfOZdRi8KuCplwkO0ZY yaddEfUwnY+a5uLFGJ2kZ1hlrV+Vo2mNojylxAWlg6rYUhs6FSWCdftKrIslWxvrbAp41xeIRy4m y1SQ/puv4MpShNWAq+Bz9AVltdgUx26sn2/3oH5+Tvk/wH9AkGLsoogoRI2BQ8WmFMT9rJ+uX1B2 5jlwRVDzc0SLLZ/+4dMAoMixcywXztlE63rOFNqP+kAX0WTb8iciRBVPZP6aRsuZyHq53KbuGSTG UwKZjxZRJGvwNM7O4iCZRbTOAhWGNEX0g+uF77WN/YN9gIcoJjmisCF9fnoWC9JnslEVXjFKeUTD D2RESoMq+pBON5t2KTb8m0pC8Qu+tXH+3KA8pBNpamSyJXdQTnZy0VeBPuVsk7vvf5N9rQ3ehxOg G5qoSjSfiddPC6qTpft3ja41VxZFBwP9QD7xVKvdOZptxsQwVSPtni66sZvlq0fITkAA6wtEi5Bm HldZ/5ycYGDfEcUxsTOF5OdzXGrL1Fqv9yEP4pD601/fsH8HrA++dLi16ko/s0r3SNiGvFxVoRCO xLRnu9djcqYn6xXGXxWDjlJ8QO0P+p+nuHxmZqf3D0jftjA7s4iawTFEBYQqWthDs7OwQIcMRzXh r3CVTeOq21iT+w3bhTvtWfiR9rLAmAIyWyKi65cG+pw1MFGao8z0R8y631ULLCeBoO2owlt29LSZ mirasSm50SZBdlQCb2S9POjAlnEJ72Kgh6F8YQqTUmsPXdmNadpenVQ4BUKVJklZIUUQcwy+Gda8 TlF9CXyUBcFVsu0BDS484n9T33j+fPft4yenlyif509PLy4AEtIiOSwyuDh9PklTGciNLRw+F1vn 8Baun8/haDqnE2j9//3EwPNPbc6DJPoSYGXNUVbY2yuLBQbCX2a23+LzJwvAgJxyyjDlIiJ1xYI8 kFnIVDop7nD78/rlCSpkYe+ISFNcXeiO8TtUDlUR18wsaomOooVpKiI6lvBaKjEO/iLkeX5jHw7b h3tdu2K1WZdHzkgNXSsbk8NQCkzppKhiWtUaRRN6Z3lfOn/ROLL8egfx/XNzKCWhMt70nDgz+aPC pplanCzojS1TctOcak9dIK6LGDKj4VJU9AWm7EPLGouWTR4rBd6CHHt26QpPEup0PcOTB15FgQcc IzF2c/rYnefPnj95hkvsGQwWuJ+AOwNLxEaw5/31C4jqn8MYD3c8OPpnL86Gw//961///e9+93// 7yf5CQhgpeCbUNMS8VzjCAUFlFKrznSdEaeCzwp/GQeVphFhrqQtjvjUu7kuyxnrms8xPQMsy5X+ fET9M8avp5BvbN++fXt8lZhUIIFwNyIeTk6dWUKD8GN6WqLi6D7jQWx6Wk8j/C3qaJY66GxXEn/l B6E48YUFsy12QY5J3VHrxWJDZrRrLzfkti2uH8TVvuYlEKANaYnsDl9qk62BYBLGLSd5twzMUG9E 7ymEAnlwnGWVxQJInaX26QKg0alfi35MJXGoQnxNzfWVo0NvhaLNUef6SqfloJs01aYozYhpW+JH 4VEuXz15gWioy8tn/WfPL8i+g9H9YosJ+f76xAXifoZYzPwcMrLLPqXcqWj1N7/5tH7+BeXT2egc dyuqn2OJRGu1sjha6AORTaD0Z7qeyCeVTaNcc6dKj0qNV7J4LmbdAmiSxt0pBp9fYvi6c+c21tnz i/+6unqPBB2PUU2LM1xHKKBZPpOm5fSRE4hGsYM19D84f4JiwE3PmXuJ5A7J6kqT6ynzxcVwaM7R pB1G2rjWDX9f8/21s+O3GA4gZB/zn1Z6UqfSC4sQc6BTXnIhHp3aat9yaRNfRBKuVfOb1Trkm85T Dpwc1a4Rk8Ga4lPAs5npPOFkKrsh/ZMOnpaZFXuPokaqPyMfw8FziRLqX26dEr81SSsKJtHq8Eqe 4fDi+eVK/+I5uVOfIST6+X98WjhOoEI0/m44PJvG8wT75vH1hYyqW1NDROdPJlU5dEw1L5SlQwpP ETqaOA+Shtma7fO8PIhSLLjOaoTkxAE7wtMsLi8GEIH+PKG6GddvVkPj8tP4+PgqlRNiix4/QKcN J/+sANUyw0+v7XcO0HnT/aVziSHiqo+utNuJVfAunqgwX3mbQyyqbhueJSBORn8cCFw/U5NTZQYb 4XJCB7TS0p7PNFNFhqlggcx6mRuXZLCB3po8uknsS3RJGzvl6NBwB3kyGVlQaaxUmYLaOOiz3b08 OdozhCGigbyTqhpklaS3bqLfv/v21hP0PsjeAIYIxn0Ew7r4v87hVsYR1H92imCy4XMwHM9oTnvy 73/32Zfftlrp/vDHq6s2VTnRy4lGP5QPLrEue+F61PpTmDZO3Cp3pYgyLj3ccnRIVXQDUgOOaLcB yotLKcgzLaYlVA/dYIQfvuBiGR/3slnVP4436kir6fYqvuOue8BaM2iD1g46+0t0/hxHS4QzA0Qd nTewzkgn+WwPCz+SjNdRRSkabGmleWD7nLh+sMnodRMBmpjj31+MZbOwcRet626i2Gik+aDTgkOl rEUe+FhI5Zmzzh9KMWf7kAWsFRhLtE30vpTCrzmRLLMqj8jgLAVU26TAn0WuKn/W6DbvGFs3yn++ awF3fnZJ4kPsPsUzA2csubzOL87RM4OGX4e3p/9snRc89ZG08G+fr5//Vbfy1dmfloazubs23UE1 4SLKSNnCiUMHZ8b5Qx80jqljiAUP6VyKXTqDKvri4KSh2AGcTd0uApSThJ9Txi0NJvhyxWkiLkgE 9Pbt5ZPVBwB/9vYeb1I02vj4CVfNuJ5G4/TiR5L9jt9gFc3T2n67M831ozmDMZQOmadvT5TRw0Fs 5FmpdWstLbJIHz7LOacHYuD8xWSRPY+C+uEyumjF4ic2Zst0kVa62t7qARkbvm2hebJdtw58R9so GDWjXQ7J2nXdzCnR+6jFMURpZnSNyTlHV+AgW9Jy1tBTnfM+Q59mLE5h59fzFdBar6Z2driJntha vgCeCBa1TxPYKSh42hB2OvzD5+vnm6o+vno3Nj38kN5fXV1Ntz6cHafND4fV/Eav1W13W8DA0VdX KJI1/P3ZB+60UU8E3ncHlCZJ5086xpMLv9BtF6jq8DYwreABWnv5Wgb4t09XtxeW2u02hXG3CSQE pDO9OLv3cBvpVlZEt09Kh8RltMrn1MNZvr+ofh4em2K1DqaHjG5SZKjOHbLBbB7krQ3OtkbtYRXL ExafHwA6f2jg0pZHcENlwC5aylzqzRh806EYTKLFFSueVFRT1BRpEIKCRNxr64euWH40BjSbmUPg FGboIldQVqG+AafCw4pgLPN9lhmXy4LdhY/Cx2z517eHQ0S3PMOMDmRn5xXu5h3KSBjBrlx0QjiS UD8gv/D3FyDJ0Ez/6+fr5/etXmd4tnQ2nF8bns2fXXWXhu2Nq6tO52qzN312dbbWOtzdn17IqJ/p q/cLZ1eIQKZWG4JAGsVw+hCw3po/W0g9VM0xXoFjB6dYlSTI5OA1DfA4fe6B/UT9dI/bG/sbGwAG Oxsopg4BO3jp7G9QOSEt7QgxoAhLo+OIrzFUz/jq3sL0QefgYH4RA9hhKMQV3QMDuS0GyXlfRkQG 0aG0ytRCAh0Xz76+I33I6Kd9la3izhqRHN9lbaZHL3qKUrOaJdt7Izl/5uab569KqWfVwQg2LQ+9 DvWqsaP/Ow+iFQ9/0GrgshMsiPRtYAWHgyYKjlU2RAr9GE2MztOgfHSV0he3Plket4/OBqu/MGRd TI2MINp5amIKao1lsoCR1xRnEHjT5a0+aDGM9le/+3z9/LLVWsM9N9xsHQ2nxz5cdeav9o/OrjoL V2vtq/vT9+9X+1fvrh6BiGstXq21PlxNj81vHK4dtqpj1ErdxbWLoayaPZvG6dNCYdXzC+2xw4ND 9OHMheyT+gc32NvH1MXMH3bbhAQeYhcAvqN68EsbEDN+6bRRR4eHKCVgzp2NjWnAQ4hN20Q1rS6s tduAo+eJBzu0mVYOcPpi8rOfpYBZCLzoXtk4iEZtxOCx1vbMjwyWDlT5kan/EdkqHUDcPU9NjCqE eNFKRu3wgaNWETOcRBdSOxOvZ538LXUtA1Ob0H8pmgDtg2x0zCaXFlbC/3mQhLhYrkYG/o3rCzql RQ9JYX1T7/r8bv3PxsVln64wEohNjSDnemqS/DvYoztJKh+4vsCiXtAqZs50Gf795+vnF63W9HBz Y7/Vez9s56PhxsFw/mz36uDRVffoaqP37qyLa+vDPJy7re2rpfb7q4MWjEBX+/no/v0fj1vV/sJ8 d7/bOzrboO6b0KL3Z52xpbMlHFHUK+UOce+oobe7DC/PM6sFsosOIJQSiuiYX3hJySG96vCAziXU C0qsc0ivo7dEbYFxlfpRwq2spjWyVGPj7CHMIiiRyUuhc57OpEfIilBXYvmlLzj3zyN6AHHhvNyx EN+LMR6y1WHgsvACQw6KGKqI6GJqdECGO7iC0gMnpAnKenaymU6Z3qA5fMUhGYvVl05ZPqbMPlXo SXq5Wf7z7fQpZY/1cU1tTWG1OwaGqZ2pZdKIoYSgk4dbGYcRohMmOTb86tefr59f1fXiEOUx1toc dtL7YaeDYWzt6sPV5tj7q/tnV+/z7NVRC31Yqt5R/zM/ls+uHu0eLly9/3D2odq/f3b249lBC0Vz /OFDm/pulFz14WwNwxtGtW6GAJpusDdvj4Apr967BwkqdgIQezFPtcS7SejI6dBJhGLK3UOy8xy2 u+1jLim8HuUE3gxlNz+LC6ytQiTb625UCiHn5uax5S6K5HLXWmXXakhLEqI+WCbowTywb9qfuUmu G0DQKmYd4ftLFZpyACgGFVUsGgsdohYizaPPUbWmZm2R08XECjGbDVCXkDf5Z8Ye1agQSzRc03gu RIcqHqI9degw6l2XP9/S/mea9y6fUmg4er1XO3MjWBWHSwwh0MtTOziA+uRYpmXwaITgfP9S/YRq e3hQdVM9O1yYv7rqdq/OzvLZo6s11Mvuh+ku7rS1FsWbdO9fzf549aHXvjo7vJXv32/ls/e9H88+ TJ+dHVaP7h+jnCheO5/dr8ZQTjSBkg3nkNhTUq8+vX1n22Yq+r7K0CEkhXusjEcx6ZobOZmomLrH 7Lzo0kFEPZOcP205slWkpFos/fraMz8WHXdw4a1yCLVdYyy2yrYCnh91PX9GrAOS2X2EI6W2ep4G rG7F5I8rn2VZPwTZhpc1ZkTUHMHlnNLp52QOOS+boBO8hQpoUIn4luxzCyI7Yr7X7UtRzZik1Q12 M9O/+gz9vnCB7occzCsXc7iZ53aWsWhwboJ+mlufI/0hFsLDT0iXF3jUq3/4fP2AQP3wrt1C+t7h I7RBi63qbLhZ3b8666btq4PerS6Oof3uMQaww6v7t6pHZ4f7Zz+O9TpXZz8+Opvt3r9/3EXtHN9/ N3v2Y6YVEm1UVQuvotOH6uf4Lek3cH8B/3luw1UT7hkX7JCQw03CDReIqKBqIk0HnUudfTqA0BO1 OwCgHz7s+PYiF2Jb6rzhHmqlr7KtduHvWcHqLEsm5XaRMpRfIs1fblemA2h5uSQpbPXsHej0rWof PoiqpAYjK6hYGSnHDbBH+AldatJ/4fKzIVaWTqKGhBh0TOPGKDaOHmX/tHOyLiirwEBdnURf9G6i T4/WQZuCoeifruNzwwk0QifQBKZ4sH1Y3A0p2QTgaDhQcaPh/vqnz9fPP7Upu5Yp5cOlozXM3+/P 5lvvh0et+uDq7P2j+dajq0MGDDtX777tnV0dzl8tVK2Ds/vvf5zuHZ69b3UfPcqds/tn746BEbXy Bsrr+OxRlwiyTIfHDwCg37CA7O3lSRnJbzcgaMIKG6zGOJNkdx/TwCb3HKuEUEZrwJ+POmb90kYj mCDMwUPJItajXi6BrAPOwCUY+uqcSvYsz+9Tmp6Jryea56kCI/7QS56Ypc98heyib4SpVEsbcwmW DEWzloXYsnVEClHr/x5UQk5jWJYOPTSg0JTtlKSPflCFotQNwgIGx8T0w7uJfqcA8V3Q7f2tC3gE J0Zfjbyagz0JTA0YvmX0PNDRr9Mkv07CH0xkkyuf5mY2CLB9fKDAzZgC7uL6CUSMtg8OkZQ0//7R +/10H/0M2TTRRz86u3pXz17NV1Xn7D128Y3hsGltnL3PB2dnZ+9yDzRHnj77QGcQOucq8eVzKfcX IYiXL+7sPtzdBHa4yjU0blD0+MlHuKH8cjIuL6v3Hi6gfg7bG0jGergf7Kmq1eCGZoUVs5q+2Ptu z9dUZjCXRns/KyImunY6ah+kDmhl9PVcQwP0Q6+S7jab+yNoVE0sljI1dpvTcWCSMZOWRadeDHBw O1F0d4e2zdYjU/SvbYcyrrdSTZqUpWOX1iQpy/sZ+cb4BUkNL4AQYm6nrEeGvHaAfEE3PzlFXlNa 3A3qGDfZ5NYfv1Q//0JcBQk3gPYRB0+absDHLf6qEs5weEiRtDGtvX93//5mp4XhDNTG+6vd2R+7 uL/2d88+pKWzo10cW6Bd4wf8Oo9XgenoMtfxnAUcOH9+eHF7b3bt+Hif9GE4U2ZwmGw/eHBnlQ+k cQebx8vp5Kji9uIS+IsO6ufhw31W7gjR1WCh9WGxpXeVgjt6M/GzWZPACuBoUr9oUQx1pyE4RF8w 1wizO+2pKUUYC0lWbcxV3hkHPRYtYC1nd+q7IzwUBliFbCE30E0ODZKPa6BvHjyexD0jJa5EnxvO 2Uod3yjfQAE9ZfD5FPjg5Cg7bVE9UzTGj0KwQnt04d2B3XR5BUwO5Kt//M3n6+c3/yz6ZYbFWRpF xGkiTIfrKRIJDwIG11EEvQrEsDN9DIFH+/0ZDhmg1WePzpbwyzzOnEQM7I9n+y2M7zzN0/GTn7wW AeszqFcfzm60jzFMrW209zFSdQiFps4GV9T0wuLe0SaBPaiXEwUPjd+4/Rj8xUF7nwj4vYPoTkcL M1BExMwRHqSYqyTB9BVP3tmUFdGUZUbDR7OwtwvvNWW/1d/80EqWFil1WWL5dICXFajKchin6gET 0e9UHaOE9TIBkIqQoocxqeVlUGIGDA4KKl408XQwCNGwH+7CwW3fTL9/ezJk+oKi6+bQ+gA/hEge WRvQqpJLmc4eDPEECSHFZWf9C/Tp3/3mt2p8YgA1sng+E3HF1UO3EDUJNVERxGbR0cQWlJCPj0FY dKdnOx8OWh8edbrvzzaIoH931sb4Pk+KAy6ffMLysZcQkO3B+3XQ7e5TM0ybaTFiYbQ66Ky124IF 7RPUAyAaAiAA0Y8fEHLItBjqZ60D/JnqZ0brxwgKk88QDi3EVPGj+UonwW3UVxydRPB0GKHQWH9o 275Gp5oe5ik5f+JAmx9LSCrTtiZCxliqJVqegsQ72kI+676LaTNL52t1URsAoPRXsrHMU2+TFUuO Zp/JrsF0mW7rM+fPM+qecYeB7RpBxwyIHRcVR4iv0x5mSqNH5axc0F7d4cr5F+hTcqA2NiPzICKL YAlAoIrJHimb/aUmKi+0WNJbExMP7qs63G/TKXWEMezH+x1wqZnvr3ii19fJDABECkQ4IPsOzh0u oGPZbXx8TOdSu0M/MeDTxrvD4EWuDIjnF5foT/iHC3T+aHSVPvSD6K2MhykWXisGm3M9t6xqishs XtHNHXXn+ua4YgQbQf9s5ENs5PNVxnQ1t3hGD7bKeloVXFN9+Hb8KZurnCyfNqquLTk14qTSlKbg 4rnSwoVK5zNLkFQO9ub0jVtIjcLiZSjG1ql/xgQ2MjG6TCWECWyH1i0jyQWj1xaF90KW+Icv1c83 EkVjoejq+UfpsGI4Z91QapA9roRuN7ImEfcTvwmAZvKVg36PhEBD0dE9zKTo6PIb3dHz5wkwwz0s 49hbnFX0kBGffakk2ZPNeE/3mL7R9mzUFL0aL3gjVM/GBlkwNmSZhAWgNpgCi72trK1W2N+uHIf+ tC2VmN+oyqFU/BfNDQYjFuJyeUvXnqZoYE90r6nrf1SAKuCiwzZUtFlYVr61tH7lMzGRP8vvhNn3 ZAc5arIRwSKAc2RbLUmDcg2nrCdS+jT82ZZ/9U5J7nxB6M8EpLkjr9BDT716BQZ1DlzYCsAgdNAQ X56TgXBl4kv0KTtQPXVWQyN8hTfu80EVnSiQmZC6iszqbbbEcJNEZQNVAWkyIddk5aIcrvi7B69H mUF9Mb65e9d1GTSgY0MtDehUS+y5YLiHbjKQFsCgjwk2OKQuivBDJljXlmB2XssmjU8xNrLMlc1Q jNl3gylYUqlBg+lWi92XmzCrBicU/fynDozRkcsxCwmTh3dgHIhFZ/Ghkn2ASvJfqcCUkUqlVGUA zL5mJGsDpT20KfwV2NZEUZv3k9vIFTE0uNQaLduY+Wl4uNZPurzACXSB62t9Z07m97kpVn3PTVD9 7KxMYHRYp/BDXGnLw999qX5+mUryu+28rS0bpAoW35x0jSuPL132Xmc2MEDFkaKaq4iW5u6bO8HM xRUfv2T8EPrDt89fNJEf+Y7pi1SHkIkd7S0sOKtBF9yBXXEoIGq1gf8sgb9YU7lOaKTEZml7gmP+ drgHUXRICxFi6UxFY54tKE9t5TJ/jZTmuWEhvBxzPEkSALIdJwYbhjKAyT6+7Ik40qZldqvyGw2s da6MDMtOfESLdpDZBk9Yw0WjpbToU8XmgGiOD39X9DH1br6/us9xACE68wIhq4xW4NSh9EPk1u3M YWKfm0Bk1Bw1RCuj6xNzV1+sn180cgtDKguDxDKrC0OLOiWr21Q12vgK0eYi6pYAOLt+gl+LN6ID aI/9F4APAUG/cJQHMxbPWV5PPmxxWiKgQ7rkptfkktvgngnM6hLm97Uko7jlIkYPvdKbSmEUD7OL g+j5KJbBJ5FAJMXgmcGaT8xfU+58H9FNzFpDz8eMaDPrRyoYdNGFpMZ2Ij2Isl+bwdM/S6+bFZKK lUowJJI0VvYOo6JDIeVY8gZs/yObOXwIDLlQONQ/37qhfg4vh6QOA/68gssLAo4pIuFfUeGsjMyR Z4AOHtAXc7jGoCH79Zfq51fqFuE2jm1qvt80mUkv2LrTmOR+ZVxMi10T6elpSEOXfgZ1UCVCnqHD 56UgQM+Ipbi3qsBOU2bYUB96OVExsYD1IVFksO+0YeDZO5p3tVZWjahnjpnznvv+qGm6PLa7lsJC 7JLn6AgMWA34M+l8mjtvx9DzMda5DvTAqkw96ICgttBZUg689VG/vKe0adZzYULEtlN2IFXWuWUH p6OupKODPRsHrHxsDiYzSKnccXTa9j7pf3j9V4eUheQTvAD9RVFrFDILtHlnZWdkZ2WOAhQoexVT PO8F+xJ9+ne/+ZXFDGqSbWW3Q9KQ+UqP/2g3cYjmGVA7v8yomQ3fuu4p2gKeHJcEfwb8/Pzp3ZkF BCissa99EVs1aUC3ohkXFsMFrUVUj5e7M1I/S+C/5s1SrSOt7CyLlrDq5urkeSCWd5WMrrd0V1s/ lT2HqnNT6yM/Px8TIioLQULaCZOgmYcmSxAOlfdAgrSCo0HBtau+AqTKqbBXmmLsQ9Z1dKBkZIku xUWyMXojKFdhKgPFZ+6vgxXkRYGBR5HMjb56hbMHhiXs0EVMFCZ5yq2DiINSexFI399Z+RJ9ygSq 73VjfI1dkZUhXVEiR1jbkmXHrmf/lzUWtbgfPBGsSKJCnn79WvTzJ5uPIUAE5AO/EFzuG6w6PJg/ QDEB1tnevHOPi2m8QY2NM4eBX+7uzc5vAAGi/mdai9exZ89D8KE9FRVysGW0pkTUua1S4HlgrQYf ZXXHouenPskPp/rhy5C9D1FrKah5x2OIfKddVVKmJJLaIkHkQjWcmG03EiSgwibFlQ3fZCugJ5Wq 9zeWA8uTvbTH88TsG84fflkjazvE84jI3EH5oHeegsgJJTPH2jFcXojaABY0Qe3zyvKX6FMQqIe2 sCL4R2F5GrGBaOgGFdlnZXIt+dRYoGVqAsNfsiVFr4n/681bMgzOzrdbJAgTrKfdPhZenUh2dMsY saA6xB0lHNm40vP4cWdvaQ1Y9QEMGHvTZqZzFM83kkUDcO3R8mUsGg4t5T7QAS2WKFbttuv2tZKZ UvKCcaBnYxJxlu1BDh7boId10mGKeyqLZpDEJD3vVKoWzUem5kF5PmhllDZaGHrNAVV5SvBCko1Y +lQtKYKWgx0/2d1t6VHTQzAX/cs+fO/LKJ8dYr8md6gHAgqNsDpKAmJPGM9fE1+iT0GAdWyc8AQr axKCifnFDpiDj6NJtQXKxVhSX9IEYw401WC8tPHy5dxrrh/4A6fnO9wM06TOwA6N6UB86Hcdh6FJ 24qKAke2hJNpFyfTXQCIqC6qn4fzhf0JwQUbllpvz8zgNvjsYsBmsF7MBeDL5pWIdXtKm+bR8qse Q897dlcY+GgUe5U1JkGj2USirOdA+ohalVqwXBnpgWgYy1UJlXGER/0hiiaFEm2mz9FkEz8/UwbO FYs+cuxm+eEMhD+QZ5zC4T4H3AeFg/sLEztFbaBtnuyvs/V2bp1goMnJP365fv7FLG6ym9ug9yh7 amRg0SVnmhYb/APWBUm+2tJMUTrPsctp/yWP7y9/2IWy5+jxnniTTS9Gl1hHwMOuANH5mIDDw338 wjJplh8SsUEA4jQI1Gl1zym4m/25FxwrFGhlkIqIryznMhuzyiOCREurgLRq6+LckU+7oGdjObgQ TY3quRihnY/zoUxXVdG/ItqDqiSYOkhw6+x0Sy7glcRbsV4+NSbDUNQgoupOrqJ1oNp65yBmk5vl Y9/NoPe5pNWCXD/AfmgAwx2GNTtAoJdJPobuB+JV4EEQsn6JPiUCNSleE3KIZbd7qpIbYy2zL9I9 LkLuLHYU6R9CCdcy8rJ0tB3h31++vf1ge1E5dvUFznDEM2vFDlh62Gkr4nNMmc81GTm6fDq12xQR hPMIiYlL9OWuQuGeRTlqMQkhqKjK5NAxlgCwKustFm3S157Bsh/aU9dSo0YKizFC9WMyHRWoBVOw Woynr0WL1zap6XPKlobYKRTUcKS5IJYkY/O4LxS19YaurKx8H4iNlcatZjPg33T+KH16RLsvTil6 Y4vKhhRjo69wiy3vEBE/sUKObdQScuhXoKJf//cv1g8ifK2xT+piSHo+Sh6Ez5bZGyX1bdopo2OP pvjZzpNK7+P2GxngycAMAeLJdQUiW5aRyvqQAl2M06DEH9OwHhudQeWF/nlmb1YeF+8fVYWnGSgq oZfAIuUObCgJ2V3jqWFlluNEntHt0RsDgKR+YnDoTnMQYiMXO7gmg0N7zAAr+QxmBOSwCcOnYzJT czAAPKQcS76M/yUH5UcRrdqnXhIjfMV3VHZEjq76s/VD8A8OoK0Jgn9Gd0ZWlH2fgISefBhAEXfW wWOMoBX6Mn0KAtWgEPlAs6I+7FJuLBqxaG5+FELyJUvJ1ZTRnxHm3+ZH95DAHyoheMDenjTAnVUH De1VQKJZdjhj8npVRPPLwQYusenFPQSwSjfpqgkJOpG1ftptBA34FEjRuI6ByUqDSXSssRXyIHP/ 3Ly7plT8TL9/McYFaPRT1jxUbdmz/pdOcSbbmZvUecYQm43XIYbSQrHWTfg3T7S2U1MQ6BKUbmmm 1EjwtSvpNYPoS7E9VuJz6WOrSNh4vgL71+nk6Bw1P9DKQcBKPRCNXOtby7wRbPKcFzL3//Dl+vkm O56mHV/WQLYiPxd5ySDZw6DQjlmFPUsnFX0M22vprY8pwZccYNDQQ8DK+p6my7ScR4r7cC2t3oZX 484D3ovBEQrY3dM57BD+s+CTsgkhKofrgp4yBqhYnEF2coHNMRIvqLss+BGvK35yi/9dNRxTpYmm K+zJrcaiqawrY6tsW8g4mC344GRrliyiNkvWi+mCgg9ZkW3eUW3vNAPKzG64UdFwZzlrpA/NHs5g +xx1XJDnPP31Z84fLI87pc25FxP9HXCncyThmJuknBHcZOeE+VD80QqZwbBCd73/r1+un9/zp20b PPRpqaZMZ+lE8Kc/sl5y5hiWWUPabEZng+ot+b11TUD/5ofn97YXkIIAYyDLe6SUmskJ481a8nOJ UEQyMB8ctsnAvGBxGZaeFMuaVq11ifyUJ6m0o5LPZVeYDF6C4pj/gZ2/OH90Xh+5FoJIpYT6oRhe PXx5vzmzD7Exg9vJFv02iRZvrkblnO31wSU9albVrlySj1l236g51VorDjmQK7Kwsd6DSrpU/sL9 9R12NhH9ftHfmht5RfoNMO/ooZepEaIcX/IQwrwzSQGa55Nfpk9BoHKvn6Pt9LXtImVdVTJDiZzJ A8899GW3nlZp7beh9vjR/eH1ax7gn9/dxvCF4Rze0o7MXbiU5uepmOCAXx0fd9WqKVe5dOi3CFCY nt/g++toMesNICogvzQ8lM6RZ9tJLCsTfcJPvnuBIsRNyiHPoraMX43SGRnRlvpFz59BaVDU+6Ie zEYaGH+qAts8iGWbcIMok+3twdLbC4Zsscka1ancS6iyPmNiLCu2CjGg85oFZmncyI3w4Xe3hlhe uXVBWYfLNL2T7hldNBDoycnRiQua2XcuMIZhgAeDcf61+vmFRKrVHDos3Ey29aMmicrO89IXaqDj owvqotFPNmlKp6eW82figEcCGQkQ93OLwEPcRcf7hPJ02ocC+uzD3oX56uho+4HUkke5UFEhgAN6 130ywC/E5D7MSq/WMAjefOktqidNzB62OShdZ6hcA0a6uMr2i9H8PtUUbTTwnxdjWbcH2FouHcMV fPJcjaRagGD6VJXnDHwBnuT7yv4muf1Nu0jaz9CA1aPuddbhwMDOgZgjXcfPbVDxJkpf37r5/OlB OI+kKNxQk8s4gDBzjUL3AwhomdZ0c+T8BC38AvQ8h0CFq7//Yvn85le6eIA0O5TnQ2vU+DAXQlGx zKQCjkqdaSG5q63IATU7NLrPjv/mCefXvUSC5gIM8BDBI1h1f58N8CwYG9AvkMNCcojX7R/yuQTZ IViN3V1kKTCxurd0cLhPBtSZvcXM+4x4n2oTFYnBJa1R9n/qVz9mW0OVY2pI1zlw3rb5kFaa5q+p j9fvjIgfY2r0yZiO/kryZF0Cpi+DkHyRnd5MKRvBqQkgmhplbFfjUBQ1W/I533IZC1Vj61V8FBZw wMNfbA1aCTm/Of3n23SB84ccGCvrhP7QCDaCmwse5mXassg61gmCgaiWtua+SJ8KAZ9pLyyH9kCN Suk/yM8gFRglsVS1mgiIKzWtlq198EBay0AttiyBvugVT1QA9AQk+uzeomTNT/NwRZmqbUIJBTkk rAcRHgB8jsm8DHqD7O8keJ3eIAHZxgECOvYW8kBTUrL1bIrIpOynu6J83mdn6YiUeCW9kii+gnhw 7Ckv+rFr6KFDQKgfz1k076EwIuD/JDhbjBm81pmebQMp1iqUUNaCDpWOMnnisK7VVblVcbbZVC+h UTo2JDNgJONgXPksz+3WzfTX8SWCEy5OJ7G1aQfw8yu2EKJw5iaQ47JC2M86cESqqGVSc3yZPiUC lROuas7uqenYrynFkYKjOL+OZgL8HCXyOqsGM/hNUIJOQyMZoLGW94T1zzBgrN7d2+bOBtrDB2DD ZgXxWWMVKwE+HVar0vbBLmUwZvIgAvuho6qDNwF/gfqZWejmWJb0+uLvNDDhXpA4y8r1ycHnAlkQ Myi7VLltcA4y1XJ/acczNVI00KMjT8coWkf31FXNyCjNKZSFmOI31mRx9fRl2wskPwamVfJFT8m3 xvpgMhBlmirmVeCqoGllVgvXHvK0W7lJnh+Cm+qHlsddkn4Ve3P7/QnqnMknQMwXXMwjaINwDk0C /wEjNrGDlSk7X6ZPiUCtOcsQ8b38mFXdFidn8J+okCryXyQZkisfueyINt26I6uivCvbBMJt7n9e A/15+/zJNejwtiTUQSw2UyzwLD1UTuOw2yVTUE643dqEH9L5s8gFn6+d86FAmWbsIWk/F0xmR6GF HqoyLonGW/IrdO8UgZ6MH7JefsSFG1pMT3rMkmd7+st1UQeNpLeFBCxmzxJQLddUNHFzpHba4p6E hnYCT7JZciFR1W/NoJHJGbJmxXIBRZf2mX5UJLEGdH/u/trH3pTnuMCAH+7Q8IUiAYdK+CG+0cLK ScngAAs/NXGxPPxK/fxjB8p3ioDGcQMLKgX4QZJKDTWl++AsqoItFCdPl9ljuIkuVJhv/wt+f9lo DwH96zmh4N8+L+O6px3KywkDPqRinXF1vbFj6LbxYwN5U/MwoM50C9zq4sOcCktRiQYl2bo+u1jl CnP3ugk9ND2f4wQb/fNIs3+mX5+MJfewJxeMxYJmRiUiqLxx30vykO+pDsWkYZh98RHSGW/rmjUK MQwGHgUjjlrZUVKpab6scs2i3iM5blD5KktVmvT7rZK+sb+1tUVbv1ZO+5OjNHwt04ED+8UKWZah DDqn8Z2gQ1p5cfFl+p0IVNRPNY/EhFavu7Gw1GnRk4DCoMl+Aw8O/TXnPaOkWCzPe/0sS6m4L4Ot wTMSQc3iu69lgwrtb3o63gxPaARmNhyo7H9fvffgwa4C0XzFQXSGnnueDKjHZpqzPZq+ZzQZ5Ris eHkPrYqTq6j7tERYrCyLwBFZUcH2yGiBnKcaW3RHR5+OySFXmZE02RaySr4OtZ2GnuUZzOsjUkgC SSppbCztxQUvti3IcKCcDJcwWEtUc3rtCf2r0srGFjY3BuENbkw//O7b+UtsKsAFBmcOkq1xaxFz uowAuxXa/o4JDMVzipXM2AUGPmPly/QpEahVfXivv3WFTMN3CE486iE86lEbl9fa9CHdZxX1RTVl +4bMe2ztPA3R11zJI5IKum4iXHzbVQMqTp+Tzd2lDUphebi9DYWPW5YtjnX89kc5CqxhhSL6ARng 0R6tUYDdcVlTXQhpmWGytkG5NodDDGUoHMiXvc7BreNqE9Hknhzbo9fZU+mAqAcaedqj00syPTSQ SmxX5SfNvyYPnYwP4tCv6f+ttXFXBZpcQJUNjElT8QqdL9CDGgOzcBTZtGkq8eDg5Cx+eNMgGJKQ P5KP3RpT+mL6+Snp57G5e4U65wm0PMuvdvALRvgJojCgHUP0xvoyFudurW/9x9fq57d1vbB+dv6o lWb79+en29VZ/+z0UVW9Pz292qckw1bdnV87rPKxhPdmC4TLekLL4jh65Ny6H/TuJspj8bWscIIA 8YgS7HKHonwwxHd4F8/M7vbju6urLnvmwEPFDkW/SrX0YA8C1k4b9fPw6FCyuPX2Sik15OjR1o9m N/SqFldj4fxDlgG7YkqyjkqZVe1r+wddBk032UmP265knV+yByw3FlFHQQIU7MvROiIrVu6Ms28l tn0GWbxehFR7mqabS5SsyykUdFYNAAqTOiNiskZu+z6un+/kAJodogHauoQEcZ16Z3h2CPoBhYro BAahQb5PbDH+jEF+69//7isv/6tq5dn9/oOx3tXw+FbqbfTfUZjd9OnZwvA9Th8cPkvDIQLGu5SJ kJNNns3llbqRJ1lXWdmuNHxGs7L+9A0lIO4trB2PkTbskAd0eHJo6kIlHZCMdebh7jYH+xqHMW68 6vjm4hLur84a7i8E2JnOQQOeNYBAITZV4Kbow31w203SHFbdckz9xICYKWHI8NL+aO3XSPHynPQs 2ZShG5vKKy5Ks9NUrpqXxaS2EEdcRgOnmQVEVKGSWfLlfNJeuxIBY2mdopv37T+K9sxltNSnQaNo P5avFvod+vlTyuhF+DML5ydJdLiDVXKTWwj/wcr3FTKCoSVCo/SHr9XPN8iY782fH/Xa/auzq/ne UX96bLu/8a4/30IeK+fO378afiB0hhztMds6gEb8uoX3l1WFOhng05pWAf3l7iIEiGvQEe5T+qHA h4z5HHcI72Gd2Br53AEPIagO4RxUSyJhfbCI8sEIvzCzuHconYt5d6IvUwt8b6isR1J+QjA+1WLD 5DGVZALd4mbtuNaPLt3xCPoREZTd7ilUJFS/HCxCJYt0yhSDihJZFrj1QpVIFzwQUZKG9eDMvMqH XTsyjXuwguqH5J9m9YZpDlWKjbgfFfNn3fqUBp+h3x8j9hD5Y+igJ+cQuLE8Rd8xrYOtIBE9OeHP KfgQ8vnJr7lPmUClHQULW9O5c37+aNhvvzvd7x2dzp8ND1v38QN/uXH149X7riZqZCW17ROTlEG1 ESedQ1PJ00rzc3Nyf23DbLows8T4IQt9OvsqGGsTfohKQow0fmmzK77NyZn72BdGQPQiqec3EMCB hZaHybCBLLuPku4MiZw003Bk2ZSialftP4OGZtoyeO2tCaQr91cRQNtpdHssW05cMIO6QtCV7JIU pLDpaRblr+1F1W7RuR22q0gCbyxLsCr+xFIViiBRI2Dkbh5En3GDo7ma/apdmJTdZ9ITdjG+o37A wa9AtTHCC4Zwi02uILIPGyyxMQYrLdfPsbwJVfQz6ueXsK63ds/XWvP9R2iB5h9ttVt3+/PDq1b3 /vAY4fJ5c7jx6D4eXT6CsgqcqPuxLjk3nge20SybWnpD6K+Xb8YfPN7TbLFN+AMpqI5WVq454sPa QyokFCrWIqDdQjFRIYEjI/X8BgVwzMy0k6yIpI1/A2Vy2ZflGax2FlRB57Rou/6q4PGA/pSVZoJb zurwI+0hNc9KY4yPpQb4q2WUhQqMZcdGJbn7Hq9hLq4sl0xw9btDj8rHKaSlpGqVGlENQcd+YYdD iM2QVUEog5nzrHzS59IPV0+3Lql6ECGFxB/wX2h/QH9R2tEUDh9M8ZT6AxXrFn2/+t3X6ucXxHrd 29pvdfq7t6bPF971O/msv4GB/vjqChG9SD48+58fr8ArpCwdtNSLP6tsirfgouihWtzPHYh+gwWI L06K+pAuJqCHSNAkYT3o9TVFD/cNiW4TfggNKw6+Y07w3diAgH6mrbNXZTuxzUYUpA/NEmdkgtBk ezWVt/BFbcGRRCUqUwjtj31fbEbl366OscI+mBKBexC+VergC+GihfHYlrgi/WI7rtLslcM3IqVU ZZp19rIfwe0+pkQ0S1JyjiaVHsIWQXtG+WeW5377/QqQZ5QP+Atab7aM+QDWZRhOJ8j0Rcr5CWyw nFiBQLE/sT78+6/Vz6+oFTzrH1bd4VXnUX/j8dbBwXCIoklIpk+9XrVwen/63bDDeT6onwEHJhTL iMXsWKZlsgtEj6SO6Q/fEAB0TTPmIg1Z5IQtTob4UCntGxBNNBiL6A83KMClU/alySCr+4iNShVS 1JWkyXc5uWpDp/lKd5hkRQDxio/qx/bw0Py12vPgluAuaQ50jd7uhMYWF3m9CKzNV5Dla+U7NuUj GmhIdfO8yro2fiBKR5e0ksvXwPRKPalGqYob2tmYm+oH/MULWrGD7ExIgJCcCd0hqcfI+M64M0LD J89xDiF4dWL9vH/6NfqUHKgAdh5dgTZdPO+fn+W1/v2z88ets3579/QxuNR8nxaxDOfp9OlKJovh /+rBChaREkoMjgLtuNHbiv+8phXMJ76+6fbthn7VVD8yxRPg85hIjdnpaZNE4yfghxu0AWyfFxnx lnNZTRzcZ1X+f35mi/WcE+2Ig/HAbtl2b/FNWeWi1HQc+vpcITBG5AYjAGi8F+V/iropW7sqwTNy qqI5++SWNC1O8MWTjNjkYPEIelJXLgwqfVCRL6TUkMTKIpXs/xGXTiOOJqaGNO0z8vk/PesTtDyJ AI7JHaofYr/IeUrozwR10ZjHMNADRgSGuP41+pQIVIrtoe0nYe3xh3bdfTwc3j/uTQMlGO6jfjZO H3U2PmBBT6WJhh6vrmlfLrzJuvi4HhiZQK899PK5fHLvCDjg9Ozu9oM7Nqc3vKarRYK4KiJWiebY ZQM8AhSOD2Y5QBN8C32r9GdC6DTAyKPFOblID4ksgQ+KCOs55OJjM2jxcdb+xLnsXPxqr8F4VmUJ r4mUNSZcsjsNNmCtCd+MlbH/cmTq32lcbAjFwOAXknX9tmfIxIfCrVporHCpQVkxj/P7JD3Kl8dt XVxeQPvM7gsSb0DEMUrbU5ZH10cuCPuhld24x6boSpv8Gn3KDlRasyhPZgg4cnXYobF96d27DVqP cm843ao7wyMNH4vNUBvP0LIguNhINdX7/PgHOYBeXj54/HBhdh9OQcA9awe0631p4SH0YndXx69t JvRU1nFb6zR+Z2YJCRw4f7DBez+0iJ3jl0oqiH7hqBBhK0Q0yf4iTWGqDRShz1E2dlWyOM5RFV5Z 3J5qCICESbVJ/l5PjxNrcrTyKk5E4sXaEv1kGJ9Z1sUzH9wjhP/WpOS2d4k/6IF0Uqq4iiKBS9ZK 5src9uaxrSRPyLUqlgqi+sa6SX4V+rR1AeU8FoBhzeAysn+QoMAietxgiN8AgYGfkCE+QZaMSVp+ 8dX6+ceO5tmSDi+bzV8lHWgX16a7LaSLb+JhqC2HzAQFMjskw3qCQawmKaazigSsDAA93cNSwWnQ tWiFyX3ageWUY8YoXGyDFoPhWLqz6sJDuslM0Hpvhs6fNp0/ewcImsZLS16APlA8Pusnq6qy9Fxz 9tGzgpRxofLRR92EsvaYtHwDk36nuj11bXxv6g9Xe0kx3zaBn10x1VSWXif9VeVQWBKQXCfAgd58 pm713k0+kqQxJtGgzWRDvhIS2R0/NomZaViuriyqdF/t8+nyXDt/MqQbF7z8or9Mwp9XtLwA4b3k HYT0EPIfzPITWyug4sHET36NPmUHqpQ9a2557Zsk1WVa5FXXnA6NWGg8wytXykdfeZwakUplrU30 6SFlSoAGAf/2CQSI09O0AIzM7iT0yZRRX1NbTpFjhyxmhcwH25hnmCBbdRnrvZnpA8IPEaw5c9CS zhDbHbSO8KNHKgLSvEVlAkzwU3ODJKtuREpgktuggIzMamLjbxf0eWr0OhZ9t6dBYnFtnvRKh4n+ 79xryWpYz3JW4D0rJwvUOajYJyWbwOQ1lSwX10zw4M7AoD2Vx8lqh6UjfmoI9VQ6Y/E/KtoLvE7b Cqd3XT6GpV7kvkCGwjLrNxAThZYH+g10Peh+8KeVHdgG0SQhTnz9a/QpEaj8hBioc5xWbNLETMcj xUIRqEAEPO1HDR5ZmlQRl60P0lnY20D3ZSdKgGYB2VPMVksLkidPsI8M6m3BoRkaaGG2g/qQVqiQ bZkSMwmJ3r575w4M8O0Dmt9RP8dYXMeI2K3yJeppLZHULZvgU2+0mk4murcq2ZpVh6CEI6GIlekm 6PthM7q3oUQcofqp2BFdx/lpAOjtQ5QO940ViRNS1WU2wZLno+duWfyYqry4Smolz5NY5dmCKo7l 6PHhjUZAlmj45oQgMbApuCnDLAwWYUEPxWfkP23gPpQdhR9UPq9AeeHuQu4G0sLR8SCDAxEuc8sX cBBOQCP0H1+vn98qCckfVhCISpv5ysNmKGJJb1sDGLIbrYLTOtH4JVWw0mtevBQE8dn43aPt2wQf Uku8aMPVGvlNeU7v8P4dbF0lvh/XUrdGXXXbPLzTmjnMYNPAn//y01/+8pe//e1vlAgkpXRLS4kq qJYEB12whhZb+iQ6PWnHON0T9CeNcDK9qPLgH+PPTSPhgx47FPFpkvoWBURzB+1i7NL+KcCelVYh NyHK9Qtlxl9D2UydokeJy/YxQZGUAGN1R87Rs1qza/SKi9NsOyWhxnd3JsmjCSo/vCF97LsDmC8Q Xgf12MXEq/8Pl9fcK9KqYvaiCwtnEHhTxEthEqMz6Pyr9CkIVL5jucCph5C4bQ3dtigoutM4wVf9 Do3kGXOAydAh0IbbUumNn4oBw1cw+/Au1uUjFR9KHOvBfttjD1nGSg0O7eAhLuOA6+en2yc//fTT X/mblRKl2x0P+CZjSY9EXQaC1lvWKtX0jZrtijeS10KTV9kQc3o5/KhsGg3Q3Z7Rl7SMA6vuIK7F xwjZi5AvXdJBJ1eVpeSxK0lT2mSFOG+5kzAFtYrLyguN78gK5lSSviBaQ+01bQuV8uzBNLnG5iu7 JtV47fzpWfrqt2u0+wL6DQp/niB4YmJkmaIyp5YFOkT4/M7yBQzMyxc4jk7/8PX6+cY1I6IBUzxF bdxcC1Q33Wh5frH5Rcp2Ibv/QAhp38gwuC36MTYwv7h98wttBtvcZbMprjfGDzmbXizwxLFShsIx DKgze3/RRakQd5ycnHglSS21WzaCoDSoanraANgVV/pu0nYHy1NlVorww5HR6xZ4b4Me9CQFPFTg XQ4IjZKYh0T1w9RgsC24ZvSw+D8WCiUxBiZfcFkVd2BWj04Uqafq6rWb0p0Hui2+KrSGo1meaK7T GjfkN6SPQcDx3Ty4CwqQutzCxYXP6xWRX+iCYNaZ4vioc1iYSYuIveVYBv8z6uf3FucTxD0owk9N hjK8VAw8Sb2GrjSJvjtPsYnUsF8qpHiPxy+y8Lx99nT8E9/ydTCR9GJ0vzUl0ZxUtk9HTIcWyP1k BnqxhrnnkEtpvycR1hJJ3WthVsIkQvdb6ZaskjhmOImjTZIUb/Dv+AGE+0sTa9D/wJffYYaFTiDK 2U9dXgEmyHxwdZDrEKuBogTKual3NboMdqD9mgyNKZh3X8Bt64g8dbXERAt7pxxJiJ6k+Znlg0to f9Zp/9fWOrofehl5hUpCDwTB8zK10DS272wtQz+Ga+5fv14/vwwW3O44iM6AWZFdk4V6jH5QfF2l ocmMkMHDsN3wn+JdTmCl/Mwnt3cRhLlEflPBDz9KzVQI0fVjtGGOSokyg0CQQeaxtPgQ5894WRzG P3xOe/rTwZgIcyJ1O63e3/RYQrOEB7yrTaV1S9QsZVMis9/tkKViDfGG0V9TI5u9qNJlfCgbtEOa r9lDsht1VVdnDIaFMXqir+gvJDMglLk1sN89a5BGHex9mL+9bAhXb30eiL7DUsQ1Us1lZAxxC2Xz mfSxBbq8mEC9WKfg8DmygFEPTWcQ5R5y6g9usgmKk/o6fQoCVRc+lqB9VbR5gGwogk9HIQrfp5ZP 1xKk3NgngB+bIoB+/XZ1++ECNuF2MUjhdtrgFM09VvncvgYgjl+3w3ORQH+41unSBvi9nxQXsvIT kzNrFW//tDHGM/yAoGl0PlQ/f9UXK6W/EZJ0PKDTiS67PCjP3XBotLsk0I+4B2MU9cN2Yrzl/LzG gmg6vqoGBPGxzo8uIjzYg0EwWYUlr2hUuXRDVTTJj8QGxCLh9DUe2XdrBAuyMw2r4rQuW0nl7O9d m979/NkcAjs8HQ4hFtuR7Oc5SkBE/azQAjn0QeTDWJ+bogy7qauv0qdEgOkKVtNMCgxYcieV4ZZF oeaAs1FSgyMsi8B2fiYT3LAAmhN8f9glB/NGt9emLz2lgjPF3uHLCaEKe9t37twbb4rrx0uE5kMK YD3cpwCXv1h12c017kmJJ6iflkpYoQBpjf3ttvhXb49b023FRKWENR3eiIh1o02VMzX6ySWG324y f0GT9xpfX51O689//vN//8+3WCPDWxdi5WsUzE9kCmlfTehWjRI3WjaTJdoErg1BSh4HKqiQx0+Z ct6kA6LPNQX+wAf6z9Dvm5T7TOvj+uuULbJDPkGonifhHFwhAdDWOvBDiOgpknV99Kv0KRHw2RZl 2RagqOq96FvWbZ2orcqziFUJatJ5U1bgRM2YdH3Kguif3/ywTT0NfMgHvH4ZMA/xaceBumPam8sK RNKLQaSxy6eSLwAbp/qhHSqon72/nAg31kh+sQTpk5/Wei3VttL5M/a3E7kVVcY4/j03Sid6Km30 qoHt9BLA7nDKt+9c66KB1D7uSYNYpTX2Ox7+z382Xv7rv/6svgqjR1WtVrmJR9LBWDYrgfkCHSJ6 OogpUBEzyXjWRDOj2G2hhjBeWTxftaX2SfSJLaWWW/Yz8p87NLoDQoRPZ3mHeh/ghpO0a5Da5jmU D8J/4MVYRhLi+dbKz6mff+gmd8QF08YkscBZMllZ4aeAYWqclMk3BXgWrvdC+Oxntf+53ATmwwDi PIfWafRhW7am4BrBfJ26rcxxmhi39ll6uPeYJdFHsF9guMf8vviXhoLoIysZ6mesVcuTH/MV6sfX 9RJcoPsMqZ7w55/+egDwWBVbJOLAA3voqufRqWsZClMjj3uSGhTCBkXGHv73f15/+a+W4mXRNg9G 49cdHfbo5+K5bCinBYClQH+zJ0tKa1m2zDNOqTO3k9s66KTJrtRtfU4+NqTVcdRDn2O3NPHvtCUO +T9TvLOpv75yvnwOATSR71sXX6fflUCNvk3R19kZG5GyzYhi5bR9GZ6/LJCQGfpyTEUKSH87rRsI fzi5fXebGHVuihcXdLziQtr3uDoSH4IZ6MG6CCaXkDkiNbA9juiLAxTUDN1fq35rCU9mdx7Vj1qC uf+Z1vwOlYdwa85rMbl+cNlVtsGy4V/mBnpq6vogP7LbC5pWtd9B1/zn//y4fpw4rdnpZ/1ktG2m td1Syf8iqZUkxSKblNiKaMRYKCtiCPsJAwvJCdkPfrF1yLM9R4k+SjffX9+9GD47fQ5BzunFCvmS IGFdHgXnNUoLCCc59Xll+ZxYVPAXF+dfp985wjdYCrcFZ3iwkXmnPCCzCOeDJWw2Txt1pDY6oDSv /AUSpN5eFnKUQJ87tOliRpBoYCoHB8ppHOqZhDNkDLYZxg95Axjurxm6v8b1Viq6IfnDT/MoCea0 KjL0j82faGekl9iq9Nn0y/dUP6BdJVWWNbGR86NKzyxeeFVBH/UYFQP4iN4t/9d/flI/FVm8K1tV FBtZzjVDhDmWjb+Zrc+6/ctk2hxMyvGIXKd2lIsQrvJN32UDR5Z08hDKah5n7D8nf34C9dgFrO9I n6f19gjcwN4CODDOkX+4go0quL8o/ediZwsX2dbX6VMiUHU3h6n9K08cSE1WMMVG6GQj7b2xGjs4 AOoxQEiAlg1gWOBNK5ivwz+G/QCJRqSCYj7z10zwUkldktPDgEr8xUlZn2G+sVWZyW6jfmoxKRP8 Q/UzPj5+/axSRnZ1/Ke/rvH5E5J7+Cvpf0Zs/ioSoJHRoxYbtVA/GNrzf35aPyEaS15rVI8E19FB UnuyFr4idTIIMXhMWU4uHfQlC2HA/oCsOLXl5umiD2mn7PmrDJQoeukN6rGP5i9N/3l+Orw83Roi fwyXFsIzgfuQ32KS4uZXMNVTB8RJmojRPF//48+oHxColeXi29Isz7cN5kQW11plg9fAFy0oKZRS w+wWPS0U3w5eyvmDDOgigP4oqq78kTCfx7q5Ww6lfd7lhIEf+UDwiD28c3J79XpImXtVT/46PdaS fqEmW1Jv/kQmtNu6SGPV3nh19Xvuf+R5zpY/epyu8RcjGsAhO4qPemw0BPVBPduN9aOWwGSCVbUo 2x4sUQuJsocrKTfY0uBNvI3kOmCpMzZ5Rl9jlbOHdwXL77OEMk4/vPXp/VU9A/01JPjwguN+kDtP sXXLHNtLyT8UzHqO3DqEvCyvn3+dPiUC1SVh4t2XWUygKMM9LYveFJXBtc5JF3F6CrRkLGTLSSYB tBgI375YxZIvmLse8m7Ta1Ecti+3uYQZpbTJS+DpUMIdd3B4TAHif/npRNGf26vXSw/187deS91g sUL9rGn/vOpg9bijRzh/UD9JImc1sudYm+c5WeNNtWOn0MOWpMLVNaav7qfl86dgOiomqbJURFXL FiDN3eSMlaqR+R2CvHVQP7agypXmdwRb+ZvMLB4GckHE5DCQbDmQ+1E8hwQkVR+5T5V+r/uI7j2F eqzfn9xh+84ymeBxCI3MvYIVlXIzz/srtEUOQazrP4M+pQhfNYkHExbK7GlEkpysReXgGuNiHfbF n2LLCr5GA/+8/fK1nEBPmCs9zJQkhlMF6wThXuZJvTlD2Vb4Zluzem97cRb6w2MOUAAE+Je//MQv J/5vuDxQP2OM6eDxIFtbb43uLwrqpJ5n1bEgriKcP9wskUDHYmAOeXQf+YgFo0tt6qjFLAK6Fnzw n9TPf/WyUlU8XmuKSsXmdxX2psp2fdVJfGMcyFJJ5GGJMtFFzNn20AQz6BihFC1Zit+y8m3OKcQS TVw3tIdN+c8p7R5cp/W5mN6nlslBuE7mZVq7g8TVdSJTwZBNkCBofeXffk79fKMPf67ME+UWOEWk fc9M9IB1Df3PObkcMfhSVwt35NccSv3AAI/wlZlpQD2dA8xUvOgUC3M5YWwe+7t34YO/bYu7zZdh zcsmjIcHEuCyGFhA1upiwp/2UjphQvWnv/VqjqaMVj9Cjdnl5e8P/xPPX/RwSwgvyZSrw8bWU7V+ jWgcx2JL5CAhYWr8uH7+XB0fR6MBc2XR8nyRVWWxXdmboSF02QI9LZBL93tle36KR4FFKb7qd6DK vUo1sa7jECW4XHQ3L8/9rr11ebmFAGhkcGD5MlgvcF3LU5OjcHytkG0QOiCYCC+IwliHo/kPP6d+ fn89ubfYuPxD83ifsnC0LH2xxYm2PVLqzpJEEP7E8Yfg35/OUOTYIa1Qod2Uh2JHpIfwGPvA0ZVq iObi0e7d1fGGGhp/eDAzuwYJ2fzi4swCNnvyC5Og9MWpj9sb81xJf6HNz1y6FQOIaz/5rC+HjqTD 8J+4fw7s9uEASDo12tfDN5oQ4kJLwuxRP/hIxr797z//2Yew/6ZPRfbm6sVEydD88NO1WFXSFUhU aihEuYWw5thIRmDBNKmtKpP7S8dte2vcO56jGzXN9qUQbkO+catRP6AwDuDeIfU88sfIeEHWU6DP cytYmgvBDxJbttA6n8K4c46Iqcnhv/6c+vmlWUU0hUDN464CLYSFB4Sr/VIzEmLRApm3smwJQYKv GMDevqDlKdzMQIHIWp+O6g9pUu9S0BCaAQjGyBKPwKg1xI093rzDD/9dOJjbB515CkCMnJVW6zeu pFZPNa2gtCoioliEVtdrP2mchx9rzniscv3UJQienueHU5KYoD83oxAXa2mTqgz5BrZOSWZj6n37 P//zLW/skJ1wLt7VKOQsaXdRFyOTJtj404oXX8WqUBWDrLuag+8bzEZT2Mby2GDwpaKo06hykw5L N9LvxICtPR9ubWEBKu4wtpyOEH2K3/DSL4BBOHYw3ZN7GSqh9eHvfk79/KLsQY9lA7DFgGTLedCN e2pY0yjK0ACMki/NbuR5h9C9FPzw7eXTkzt3yN5F23JNn8GbU/BwCJlNNGQNqVHV62U8IJkiNClR YXr6YA1T2MH8LJ8/RG/VbsJoFBLkYzJRBdn/2/ZOSS64242jiPrn2hQ1lIuF1KzDqWvgT0M+NjLT 4tYFjzb4d6ofioyljD3aNQ550jEXTxLvh/Q5g2DZPRRIxn+oG+qpaBnaMRUfi3TBnvVcpYIjBU8w VcqaM2OlK+fVvPIRiFv14/TD78Q9OH15SQ009KuTkB6S/HkZsZlog0aIfZ+Dd3CCRnlQGOfYQPiz 68eEIxYp2IiTsPuLn11OWgST0QtTZgZimd0sVERuuHz5UjdYvn172UxAHFcnPFiNefXBMxINcoAC OSDJArxHt1TrOIPQ4AhNMsBTIGNgFWpFv6qRh0xhujqchc5MZ4YaB1Nv4PebNt1cST/9dR7NkqyA Z5EcIKPD0Wuq+eYC1JlaVdPdadIftmm5VJeVqygf+r09pbh7rvnszbERyeNPKJrbq8oNaAqXsdMx W4OczSnLdzGDhHq/DZiDr4ItBVPdlabce4xL61p4ndPvC+C+cARh/8463Vw4eDBzoXOWuB9wYBeU 3AvqHc3PxMrw1z+nfn7lsKjLOMxHkdxvokOVAA45JY1PsNi1bOa82NgKSl8owOovePqiAR4AUDO6 rmg2kFf3+Ogh71yelxRNF0hAWEzzCy3xbh+j/6EARK4ecn2xLY9/R2JCdoMFzhKUTwAPea2N0hh9 RykhREibbojNWnXwQIRA+sPDa7rDkZFCw48stiqOwciZ9Icodb16u6y0xTDAzLF4XuVyEoEsngOa WVj5qkJG2So3zQazHggilHT4koOcN6JLimaoShOh90Kxw3qOjjz/W2O9m+iLRYQnXEz2T1eeb00i cPUVzDujoL8msDSFVD/YXnlxCvc7BjCsXl7/efXzD93g8aGahin+XJbGuL+pwfc184zsPAqpxNkp xywFN3gq8zv738fHPxWvFiEY4XrwwRM71tzAQ33SPrVLXQ6Q6nKQhnhyIGhmOTxVUsXlxLFxKqBP 1AWpWLVVmm6upfqYOi5eWc/NMz/1m+cPby6YExE02LCFHs9EyHqeJv38gXN2bVkyJdZP1ErND7pE YuTYCNq03VFc4dkD+gUly+aRly/zgCyPWRsjScNSUWhwf2RzY2gyN63mJX5OvrF7+vwS4QnPIeKA /YJW7sD2DqEzJBuTFLpBAUDg588nsL97uPVz6FMQqMeVX1nuKLKeSO/pWOT/nnmePC3Fij8UG0+I nl37VBPo3z4bR9YGbynYBIDYOIXGGyCgr4gn+HDXOA1quQ/2MxtQj2mkSW56Jyk8d0R8+JiEQlMO 6kruNq4h+dFqvNS8O8lw4hwPp65rV0fsMEL91JqU211C/RAyvsFHZEdX3nXNOKBbz2Nlch/JEyvL 27nuZUuEpIRXouUIut0riA9PphOOXMhJVxhkE/zE4mMIJcaF4BSVQH5GvvoAwVFbQ2yAp/TwqRFe u0MU2PnyHEWuLg/PyfW1jAH+ApPY//mnn1M//9hJFg+ou/tsjZxJu6MJw5Jtk4mlF4puIPSpknNz B0r3VHdEQPb27YOHi4uz+1COoRk+AH6I/abwCd5ZvT1+LY213GtSTRQZ9HgG0qF25gCO4+iOo1qc mwMNlVStCUs4KzGEcnHV4iPURqmlXkOy9EikWFYjOuHPHhr1UR3NtmrdP7Q0jfIBTYfEmQNy8nB0 UfuYrTeBGXLL/tYtz6EYtAwMNG5DLKzi1Mjsecm2hVmCzVJliW7ZZWepkVze0NGrdVUui8+cP6uX F8/Q/ZxjfeUOba1EgOYk+U8nR0jyc4q93lg6uL4ODmPl/OJn0acgMP7F0B3fn2Vhzskj8bNCQ03h qmvnUiq6H2EDnUHDV+bua8Z/3l4SQzq7D5cVdQ8QnLdIrHGI1YI0qh/tIlTBeYxVJR48UOEI2Xed Yw7gOLYHw7sHPccro1P4YiNbreZHsT9eGiYpJ5neGGwhg7NFOJt/R/Q/HsHBr5ltBZkMukt8+KCG NsQqQscQHNmSHJcZ7KkFCOInH+2BqNVFMbC8cCr7SinRVBZtqm4+WAKgpLxKG6SothrDYwmRkqeO 7fXT5vsz8o0ntPgd9BdtN8UFjfkLU9cUtdC094LWYEwCWqQYaNoh98efVz//bI98EPzKy2LgtuvG HuzgeZSprE7xBcFBNmlJVpb8zeOXMoBdblNbg7xMTveRtoFiysGTV2TlhB9e4+oe31kdH79m01jF BnjEba7Bv/zwMNmCdQUZ5P9MdWV5LFrbGkzAP5M5nozGGqQvVnXbdivBIeL/kuQWXgE/VVT0OH/w r9kX0Z2m+Id9Xv57QKloDEDst4Vs5zDOrAL+JHovCUhUTM36Yg7IzJVvDGTXeI4y2Gpame+nqSxW WrXUlX3hsxqvm3Hk/AT6zPKvF0RfbJ2iPsh0IcgP9lRiFc8KDWArJEakPHG4CaFK/ONvflb9/NZF ALxkMPg6VE3TsjbPPj4f77OrTcK15GKVveqjt6v48w+bewvTnDU2T9z6gQwxh4REHwPzb5GhHJ5g yH0weCH5cH5hcfux5kSv7k1zVOsSQhgOg9kUjLqWRLs0CL5CpNjwg0QocL/NgzWHijPUn2UjO6N7 bKEMx6pfnWqkHwqWOLVUi1Uvdtc0wnFtw+ArigM9xsIEV/1kndhrc7hSN8SVmyyrka0frPgRGZlv KZBIYN/KM9CxLGnCtFqhYwmLTnZvVbpriL4ivc+k/wxPL59t9YfrFxOTrxDdAg3izihtSpni6lmB H2OFuHicRnNbw3//WfVDBKqtUZBnR9Ys7FhywI3aUmTLGh/LkAhu7LdtvL6ROi1w/CHm9+dPTlY5 H+ouG5g5SpP9y/udjtlOyUcFpU2rl2HQgrEKyByp62fWOhTTIQGaxuCGqqHGts1wnPVD4ALuJ8ZO al+zxdA0L7CVKYYyyBh4qFXCUWn9jBTwRwn4qf+ftTf7jeu+0kWTTtJJ+vaQRh7OSwO3z9u5wL3Y e5e9SUm2WAQnVFAkqziFNZDhhEIVyWLVG2kShjiBoI4A4jxFkv0QWwF4LTfcEgiDBgRJECArsiwh yEP/Q+f71lq/394lUTF5FMomKWoiWavW+A1dE3qEQn6o8nNCDiqVvBQodldN7Xci4+9Gppdq6EMh 6Zi+pmJ6ZLMcR6YA7dijEsrOx9ULQkWh3xLt6vMith20MfSs6fHDz9vgz1f2d8Be3tvZofgh5J65 RKR2y9RSYZNNEPLQAPioYGKgrx5YO9f5lAdUzaqJHn6QqF+ZJpGIJcexJ2xGYQKbSwn+BO6Lcb6c eFwnbHwH/vDmQa/zu1BpeW6ih7n1kUdBjJcVf4jiRgIXFtErlzBmC/5wuVKCAzMENN0x1+2bIu/F KjEiT9jAUTqdrHBoarumNBXo5TtjTZJUgWyblKicg8x7+XBmoomsI8mHJkBP0mlbZB7QCM1Vsq6j ZywLhMyU5iWEpIsRV2KjiUn8xy57m6hMysLHIzy0E1U8vV/2W1DJBjry064pfjCBrZypntC/dgD5 KJwvED+g7GByR+Gapf4GJnbULCg+0/sCTAwAyKbOdz6VA6oSAl9TBLeeRmlspqiTSeRCbICUecw5 2Ntv9qRbPjT53/9PjR8sEA/W1y+nOKjTCWpsfnFmPLFOsf0h97oULY/VBr69SgGOshjAh0aNkik4 UjtS8wvIRH4dEWYivwrNgOmQCWI/BoWKrqGGuD5PZH+YS7kud6XMVPJCFsv4r1pmTEk33NSQ/64A QNwogoSfK81uVgctv9n3JHAlEJr6i7TbiqTFZSJjpzCzlwvUdiq1YY4c7c6znc0AQ77wzNnqP1UA xzYQIQsLSDCQXu0C8AcraBQr3Cyo2gvszySOp9DTxDl141fni5+fRE7axg7qKS9GZxpqDY+jm9rV 0ck0RbuOWWi67lFylA+Lv9fogYLd/jSP373rbyygHRO5d0j2h7L1cYEk1Q0jzmqlujoCAGLFbZhC o9vFpsvrliGBEwZT4UHHd5EjlK1/M3q6zgROgFB3FM3ulOph5yCfDxRQF4liZtabg6sRkx4UdPhj 3cyIqcpurJqX4uSrFndKGJRPgJ93IhmmjYsTjQpDrxNlKq+ich2rDZ7DG/qT0q4qx8SqbM3IPhv+ 3N6heMvWwB4EonJKvwAACJNXgUL01AxfmNoE9QLLn9mNc56/hIEaO2dWEzXx6B0lh0fJwJyJHRls N3B0o0TKNwzd0tTJRMRhUeCr2P/c7B0VI9z8eG0UsLHptOtOCj/mLHhw00BO8vtDXA3ay9wfViKH ybPzYmA7Ttf6OxccfLuzQeDod7wjOI8bWQGLpqUYtxskK4P9Ty4Z2mUE03qGD+azCQVHjVUFFAaB LbzNxjZeKXUrcE8tic84NvSG3Koyzpw1a9qYoXOlj7KR14aKFLzgLfpih/PxcvoGL3c9auzGL5ve zs4/ZUA3phaIfwZoHt6DN2BhUOjqQyXry+Uw0wMytrS1JdqZs2uFP5w7fmKFWJoKizc6T0qtQz0Z oD5xL0iYO6HnyXmMriKjVv+nNtA310FAHSmit4GdIFTrKDWGYX2+Z/p1urKDN7vihou9OPBUl8Et bC0rGsB9d3edqpV3BPLu6gYJTrQpZMwSkNUufQDlCSuxKDN3Nts2wntXYt2kSI5Cdz6rrUxQ/f77 EsAk1LxS7UNHwNJVjSqPOUxyqP7VAoQXbQoPX47cmtWMdLImU6eyiFq91CEzUsMEc2RwBLLYiZh7 /yf9G6UCptUzV3z8fPjeKqWfoS8PhCHQ8xi/gDvsEwE7aEhtLhFE34X1M5UQZxfOIf7sGKhOLi1x bAgTd0GVY4k8MENiZjf2WklhygnQmTBGYaKrVoH6qjRAlwHeGSlWr5QrczixcyOTgVpUFbgxrKLH ZzCr96bMm3R76GQWFsdx9W4vU0Bh1ZOjIwP025PbMl7GxG3NzCTjxhgTvbBFSUZt47Kh87XhVaSd c4ifrhR2g6tE9j+iBRw179w5uXXr+q1X33zz/fdUHsLMqG6scUaAs2q84xURs1HWARxUazhybJ44 cjuh2EtvhHL1VdCQmmFrnYxNCkatvfVZHBv2PlLFjshxN86Ab1j+KQE7D/cCGOiu5RTfPUn7r25h oWIDBNUNKidglicF7A9/f774+WkcRinjUopK7nqx7DBKY3uixCDRALu+F4q8+Ia3FNYFXVn7509v rlPSuVSeE9Ux7HjKlB3APSok2QaLxCaxicWRemt00OjLhlZm/IzlySGkAP2qpUcj+mqsZ7zhYYrB kDGEW8a8CwRtaycavW9kpR+JM+bBLfOXLg9TMSRv8kLfw+8r37l7jPC5fnx8cusEr4/v3L1z/Aqx VCKrf9cxaSLBKJjAobYu6tah6OaMdyr1ih06ban6pKJW9Q6iCCItvpFzF3SX08BLbujfZSIFb1HP LKF+7c0CArQFmjsyLdLOEmEb7IJA2ukTBgZ7Z8jTL5zzfKoHVO+g42La4IjeYDCBGoROmCM5shrA wyGhbF6z53Xz94r/uXltEO2MqbEITEOm9aZDadDqB6KCwI+BdAqz9/wwxH17bERrjMyRwDwBAOuy JZTYEmJkJEf+61kHJBY83q4lHlvtC3Q0Y+uXyLxf/Aabj027O31AdVOY9s9w/xJnyubdu9fl5fjO HQTPnS+//frrR4++/fbb+/fuMZIQSgC4Qp5MdsoKTHNsHUfbCjR+sn5+yiTmm85wJXFGiYVC5hOQ 9lGJ8qFoiMRhkNTvAEI1Kysd5l+m/iPYQ7jHbUBzvkvyzxIT0BJ3zrOsX1tIQcAEzdLL6Q+/PF/8 /KJijYLb/DmshpPNd05U3su4Y0sUhOmKJ52kx+vyT7Q/FfYXEtDB4f66NTSNmcSqQHmnZZ3Wad0b C84CaFTO7cBEQ5xjQq8FFLBb1SkwtttEbNg8phnz8PPWoIpoU9px7IBcu85mRQ1vtOuXSSlq5hxv Obme5tTPqWgnz6hy984nHyN8Pv4EwXOC7PPwT+7la0TSw4cPv71///5d/BqTElhr7XjX6F0Z23Jk nFOOyChI7GaUQegwVIatd+xq/Tydk73Hou96l0eDsAeuI+8/+/w+vHewtkYDjLUpSPcC/1ygWgvg Yjh/bWF3CPLFUgHAnymZ8ffOGz//Yl2al1cLTVYt9COCk/jJGH7M+U0nKo4JiyxyMjgifkMAtOI3 bt7cAYY1pRNOBURPO1WwT8XWPtgfyol85dIl8SBrioPKMhyghud0CedtKJPkrToC2oNG/iO7ocK1 1Hbb0H0M8105Hqh/D1NLIPf37tQAlsgoIH5Mja585/j6bz/+GOHzyZd3x7/5/BZj6N79bx8++vrr VCQ9evTotUiqSFLSZYJ2AFmD9WSF5yMuuqYfrx6nQZJaA7fhsdRrJmOR0ur5Z3Yz9khxb7nyJnyV IbS4BuYpS9gC2Ms3uokdY+8M1A91NxYIQeymni82iAt9W+c7v5uEbyKr5xKPIZ9jdzVNdJ8j71wW eD3QMNHhcDZ5mq6j+ObvJf18Cv774XriW+CYW9wj4qahrNOi916ueGQN3HwJE+WtDPmnPmfjauTd J6waaYcQiApOkAhuO00nhwewjgM1JFaGDPYwWf3U2zmPfDbuO5UF5INFaW1Rwsp3Tq5/zJdPvvzy bqt16w7LFnqiY8TRHYmkbx89SiLJkhIjib+P5Q1MJCRaaVs0K7r9jwDhHWE19AOZHdDUX86Of2bA 6K98kfsStQdZSVGX0/Cftdk+ei9vTQ0QGgeiJDDzfYVuXCxyU4Svbg5sTWE0W4K598LA+c6nckCN vWGHk2aJjK3iLKjDhFzrupx0Xkq0/aJUO61Ge/sSPwyg7Z75IVXLuLye5p6mBBDJX/aoMTqnaCRR PgGSgxSQKhntIHIoQy1EQZy4bkmHGamWcMadV3YDU3CKaGcraBtxaw8ih33KtI17kehmKoWZ8ZO1 9Wn5+DgVP9+cfPunP/2XNUDSAaEpkqYakSRJ6U9vljf8tuOTV6++l0hCfbO+M3bdgTGgVeLZmtCs egsqqcE2i6kLmQMvZvSRe52+o/D59wY3drD72VjrW0Pe6YL7F86nuJ9qDUMfBNghVs99kwMLs0tr a//9nPHzo3+M1NAxTALBQZ6UhZOSRrBldJD4NUZBmN4SZdQY2wuVhfH+p7J+/vTmdKM2PAH/StDY aXsxfbljcejlfGTskpS0WE/wh/nicruJTmim6FQY48iD2xRtpf/irgJkMJrHsdeBU89aPTV5q+KE xB8LzS9qp4b2rk4j+KKIR6M7Y/xI/foY8fP5rTvf/eXP3yFO/ktC5L8YIgglRIhE0gk77LtnlDeJ JAbS3Yff3nn1/atXy4FB6/UKKjaEjpbnupvAKZPxuZBxGzCH+YsT78HojPrFCOoh8X1rZ21gow+B A8oy7eMKhW6YDw5QuK5AMjyr2SZxrOc8n+KA6oT7osBpSDiHssA7+yb7Hn/oClMadvqnbZ1h07tt hA9lfP/97w9adZSoCm7qq+KAAbgPCDm10ekhtzNM3cS0QYJaD3nwozizKgF+WOLH68OGceyYvM6v xCprHDirAhEZDkw3xQIojrIW70xjWeXxREn8pJQ3jEZYcu5MFfY/CKBPPvny3v3Pb9397v33T9+X l7+8Fkmaa+5aKElWuvdaJCGQHt47Prnz3cPv2VaWiUgQaJRFEoWoPJdTi29ggqVOnNV4qs64Tu+u K6+fL9R8eRvwnzUg6DG/U/EH8NXuAc7wffRgpo0uVBELU90Akk0CAP3r88bPv1oLn65Abp0bJsUr cHyUZLwSJWy3t1ZdG5OvdVtf/Ldu19OdxjgsMMrZpiBmcHIEcgzrnxjNMRjMkBrrSW99Ll9Oy/P2 1IXATAHNYqyAyGyYbKjcWtzhB4ztGwde2j1Wi68wTRK3MU2vmExI1VyK8yUyLg7+3F3S74fmn48/ /i3Tz737i9cZP/7lNBVJ37lI+pM1QJaTTiQrHVtSevTo/p2TOw8fPfyeAV+BB/UM7arGSdUFSo0t t+PA74bODz5IUe7MqTh0ckD6UK507J+9+s/+1sY+hq89aK923yjQuQnOlTh9dSN01paE+gUiD2aw pe6ppYVfnTd+fuLdfhNnTlPND51brcmNRM7Vw51Od9O8bRvnVVbQkTPCIdVv+fTmIsKnuCymp3QA oLkpeF6hEEix91kBFQ8eBSPDDej7ps3B8NJDAGK5wv65KJ9GJvEdCL2tkrsJWbhb/Mfm2K7LOGFP 4eKYMYnCSJ2P+evZdkH1onIp61w7yJcydvCv3Dn52OefW7c64qcjkk41kv7y3XffpiNJ+qS7XDqe YFmELuibV999/ej+Kkmzy/VGo7bYas2MDc/UW7XG6OKiIqVEMxbYqF11J7egUTddJ32r4A0FtpwN H7uE7Q8GsB0oiPeJcB3QP5DagOYq4meSLDCcvmaX0F5DIrrv3PHz8x97mkCccg31wNzIeIEpn8bA OZw5ZHjg5PPt4pOKyKBH4WNYQPeO1jBlyQJRmmOvN8anWQQoaxaNH+b1KqZ1rH2GZ3Acm57W/AMA 4jIIzPXhvFxDdSflXPcC486qjXpgdD17sopDs9LJnWWb8j5p/q73U0XPhdWU4kaH/XsuV8rolihc 1v4Z4YP4mb/+lvhJh9L7qfL2yEKJ8z1XjndffZ8/+frr707K2ALGq615snNnarWx8eFaa5HR1Ggs Mo5aIl+bd5EU+8116NWfY8//ypwBH0MFW9k4APsd6pkbW2Auk78D3yaixUC94OYZ71EJCDKaU5Mb Axv/fN788+MoYZ459rpXO3eaP47vZQtnPhi7ods7J7yARO/XQ10apj6G9c/B4brn5tTHde9TciAN t4iOwQvt77+EHSrcJdplbKLRNWu0FUFALXrFCp/0Qqds4kUZTc1kV3Jo7GThzc3DUBwEUCiH2PE7 q7lk6HICChZAc0q5icNl3f9w+kL+OUf8vBlKf/lzEkl3vxk7Rvh802ZszoFEMDg6uLhIV2G0eq0a EhBgCPPz+NBiTX7g11r4VZ+TYre9NdKPPFSd3hfe/GsDCpkL+8TQI/V0Td7g3RQDGOYwSvjS9Z0w MpiggtHT94e/O2/8/CyUW03kEP5BqpB5Roa2j8o98a4Xzl/ao5gc6yJ2jyr+rhljD2KBeHM/TdAR 4FhDKMx5hwMF0Ec9cxVejz38B0hI7JHAYK5AUmE4b2gszxsKlGpO2KpzrVEPbHepyJpistwhM87P xOPTBVQhn3ez0NUh2tKlmYit0KoRloLK8bGN7xePn9dT0vt/efjtK8TP/VfftPnsLM0MEpQ5P49v ysgYvlaUsXqtjkzUqtVqM4ieFlLR4gwqXK3WqrcgiDNG8vfqslQ3R7yK3wLfaO6gc4b1F71zsT+k bRydc/t4bF/g/gfgeRQyKPwCDL1x3vOpHVBt8xk5uTHvhxj6ht+g5qk+KXISxZ6bEXgJbXNJjoIx aX9+f/P3ImCXtqpM+mTxX657y1xKRKt/CksbEIU40+NQFiN+6vkwUOyOfIK73tDd5XG5dLkNiYnE 7SrFMDJlJlWXs9NLnFHEKZ4abSxDunNdlnvsGM9NW1f3XNasDZl/XP26N/p/Fj/Jy3cPT7658/X9 b76p8vMo1nqgCqAOV6SazMy08B9MX/FmBt4xLb6DDhuxhNyEj9fB5x6jqA281fLsCNS3LvsW86+t jY21/S0AOACU71piWcYJg5aDU6TCo3xt9C2g+YELD0LpvOdTHlBN9TzwsePZsSoLbu2QITRih4p2 a0Lnaxkk0hxOVhQP04SDr+5cG6zP9wjep/e1GPLbaC4QcdMwUVaR99UmiZ5JESpZayR09goiv+Sm EQPJ200xdvwdp8SteL1YdeW8epeg6RUtTf1U2T8L9LCgHJ5czhTpc6sCrMnGUcXHD/LP9ZN3jJ+H Gj+vvqnyO1tsDAE31wMD6sFFZp4ay1WtheTTmllEtKCpxoeBQRgephIAXoOPgvDBtCbY32JRWRlv yT/La3s07+6DfMISORe4n2KAFwJqIQcQNE7vBYhHgd0DDNDav/3yvPHzi7KOfmFiRub4XAnhzfc3 NrZEsTeOcKto915qVc1gmvi9oZ+3oZEwVmy2V4vIzbXRwd43bHNTkuAUellsOUy0EJjLzD8zI4Eb v1QQI44TBKizcve0Xj2hZgRQkzVOCA9PtjoUzE4ii9K2fU+OV1NjENoGejkrO5ZMsHrnjvU/yD+3 rt97x/zz7ck3d7++9+obKQH5GuIHNg5Dg4O1Fn6ge5bOZxHv1fjCdIQXKrlhFzI2wpcJ5h78V+QP M1M9O37mFvYWZsHA2FrbKPDmDvDhwA00zLxYoI9eWtiEi26OvqeQVFj6w/nj51/kIfHsfUf1iv2t a9fWdYl2gpMESC5dRpbftSgLrMmGYA6XhwygXiTbsVK80qT+iWDiwc0BSmMwcTrlZUO15P1G0QUS oENRcWxmZkynwEjziaQTQ6DSpjgTipZ8EDrocaTQVLWCV8muwC8XFNSjd2F8rJ3GHip5R2TsEFFz msmCCPVL0g/yz/3771y/HiJ+7iF+XvGmgviBBcjQ9OgQJZJai/ONeb5gBENHhEiqYR5DC4SE1JpB EkLZ4qKI8yxTT55jrWKy3wb/2YN2ywGyzxbY7wU6X+Bs2ocOCJhWKNeBu4xOCKN8Hw0MFs57PpUD qu6f4iilIetsC3TSdYfVwPOvvX6mb6LtuOSFgLR/iuZc/lkfRjqZi5s04AFWA+g/jFpXwNLBh0A8 rVMBkTKYlztswlQwrDZehHdPEQYYY44BHHj1vMhlHAdgsHVhaPZteOR33TlGTCkyVE6gcEeYAHJx G62a5qHDkHXJHUPaoGVhiyIGkX+s/bnL/PPu8fP5PdSvV/Kl5BeHhtgA9VDEGLWrMYqXeb5axFt5 aXAAYwRJJZvRLJTX6MlP5HWv+Bb4xgTAqzuze3tTawCPsTbTOJeY5ylohmP8wtK5bw3IZ5xQ+wqb 5z2fmoRvFCWn9V1DpEZOfT5wcMN09LhVs53DPWElSnkWscFd1fBB/LTGVdZZ/XOd+0UbjEGMmZeu BCshsNHY+yz2TKcKmeyhWwJAhIDd8JhC+dJqJaKWID725pfEJBNnjDYS22FDjpG2FLUDpUAoMsoy JtREo6Ug4E65v+saCN/rCsc0/tk5xI+rX/ffuX49un/r8/tf3z35RgxCR+aHhqSCoQfE0MV4GUQo MXQkfkaRhZCHGotWy7CrRhBJ8yzRw/gJzuifLX7GAZ2H+ekW5DZo+w75cHgPEq5B5OrsAEawzQ00 19BgHVjb3Dv3+ZQH1MhlCw8zjFKOKY7krrpYmdTaKnKsNzegBYkRQmRC4xVl70AAenu9l92xNjVF wY4tLzsJlCqR7St6Mm4zkPIT47Dx1vDpbU2sQgCRAmRjsZZFB7kL0+JocQKjIpJYfV3VxCQwldUE rY1HLKt1TiBaMfHP3baAttduCOteDdRdMMv4ua798/17f4P4OfkG8cP+BxVgQvpndD8IGmQfBA3e G5XXgw38P48gYjQ1GgwhTPUYzmaIKpcOCL1QPnjT/NSfT98b3NqZPdije1wfe2fIt+Sg0wseD4mE mN8hPrYwuwESIUQQNzf+/Ufnjx83bxk7yoAOmcQaztsOGtEtMVhODmLO9Mn5y5iuaFS29ufmzs2D g20nXieAH08YXPYbRADH+gOeNFC2r2RXonaZtimLeYpcVEhgHnOqTIn5sPlvaicm+nByGlU2ntQ3 EbsQWXGn8GgngMiTysGkqBro2RUvPV7Iq9WsGVGXpP/R+etvEj+ffyvxwyAfabB8AZ45ymZnVF8Q QYNIQ4OIHo0mVrFGTVtq9tKagdBHj4wVd6XLP7N+ffgh4Ruz4F+g/6H5ewHlK0ca4UBfN6SgwWbG +QtuKkBz0M7p1+ePn38Vz2muQTJRylxo18s17rqLqbs9RgmCNdRAiiN/HQsiDwpkUWlq/rmJBeIO AawdoxbWZY2WBtLcXBJIWCGWqe4rEr2X6P3X5hQv+afq7Vl2NUBj09uO3MlNS5dJWCjVJY79YjQw pQw1Z4hirWuc6KoFBZCpaYGsftwyaNlAgnHp+I7rn+/dG3zX+Pka9evbr+8dfy/z7tjiUA+ZbwgW FCnES4/mnkENHMTRvMTPKKuYtUEtDrVjI+iC0AoVJd3Gb+l/pvchnQkDJ4irLtF8h6Y7vL2DbAHG Mu13ciBeLBQoHj4wcIH4+YnrRDuJO/6S7gcsj8sIE8ENt8cN0hxUb4OB95ufCn6DCeiwZ/Q191yv XuewrEWHZV12C0S0SBmROYWAFAkYVTtfGYnQQHi7zq1PGzXxdDEjNgmRSAQTnHWJQNsFY581wXwi ndqFFGzVWiDEEA0ey5HC58M57p8/sf3hrY/v/fnd4uee5J+T7+WbPDwq3wg20Egw8xo3CKJBe4+5 aJ6v0AY1dDUkjfQYI2h8ZHysJB3D29SjtiEdfrB2MEX1uhvIONgedlEqYZLeg6DtbC5QGGgN6mN9 uam9X50/fn7sGbyW071YZrKYtmnMN9aGv3IjWeDJko4a4cDIUfumDmA3b2436sOL2PxM93b6qHjL AmGecvjwCoirKhRNCvNyKZqDAH29KtjIODTvIuU6arFStnvgzMRlCZ4VEeLImPuOuGM81N0MWyDL ndmqcyswG4xct4MfdlcYkdTpLMn8JfXrLvfPf4P4ecT+mUvP+uj0ZR2/ehAd89r78EdP0gfxI0xA DKCabBZrDKDxcQZRifqLOCCerd6yfkD++9rW/sbU5I3cFOIHVAvYnBL9gxM87ZcHaII6iyS0ef7z qUj4eolwX5mitCGBrnNifRhckgp3o4QlYMi5yFke6DmKH6ru2Px1UCOArFxtVjhvjtRt8ePwrL2m zGEmJ+IPJhmppAKIpeXs6ggMMKgWFCTSbY5WpLhg3fJZPnISQf7pIcT3UPQ140T5OSMLIMz01VyX pR4n4GIf6CpUJGJpX5Dsf+7dHb3+N8g/D9H/fC+rhdb8NMsXC1hD4mfQvdh72g1pCyRrxZoug7AJ YggNl2IpyCsd4r0WPx9+cIjitbe2t7a1R/oOoT+MGci2wC0FlB3Q39lFYyDLQYd+4+/OHz8/874F ZhEUuYAILSJs6k3UxrzepgdqOA0qk81MhFrjQ+uf9wFmGcs3szTNbTZpqAUDjDm/+DHa6frlDk9v gOuRkRaHxyfmKtEyBBTq7eTQFViaC7MZryUcmbFdJkggTQqu14OeTOGZ2N00QtoDZCUJZXT/k3PK UTmn4YJnagUVj6J4Mr+7/vnurb9J/CD/fC/a9IvAYgKNyV4Z0WEx08P/sJSWKNIW2i2CGEFcR8sm kQE0lxGiyUqne4rBDy/tU7gFw9fGwqSIr+b6cn193UJ776Nd0yzvqDivLgFMP3v+86kcUAPf08Sp Hsj64MD5EDqofGASNaYAtqsCnGGQyEaFya3Dxc/OQY13Lah5y9iOviakvjfWPhmMWauEaQBeb7vn Dn85/D9PAjMFOCig6aLYjr2OxhWHJinoeIXS8QaB9xONdiOzfVHdh10j/YbUmsPsUDXcfC5nV1NG jmIQK2Q/gKexW7T94SefSPzcf/f4ecT+R/b9LZxPGUGIkdqijFsIHAaQZB7JQvPsqVm+dAprsIDp NWOc3Dh9Pp9dv/qpHA79FlgwL3E+gP4YtKPAfse4hSv85mZuE9uhDeGfzs5+dIH4wQHVSc+5E6ib jMOEyOzscw1pqytrz3zXMT6TOHIa1gYfvfap1q/9odEWq5HtfohiFYIOGFAr/bJBxCq6DYXvESZy tznUM8b8+ASW1nMUoG+aILWBC3ejyH/qQvVUdIZXFqZG3K7a3VhDxjqcdcglcsBA55edY1VTT8r2 3cwIuT80Y65i6n6B+SsVP89ePNa3z07PHT9/Yvw8uvsqTyRStTYIwj+qV4/ED/tnwQNZ/cL/vKyO sn1m+uEycVHgivimMPvUwc2VBuMM9RZkoCyd3wcWDsBfRvgUwHov8ETcvUQaqmQhNEeze5OUDp+d Ov/5XQ6oTsFLaQ0pmaLImweHXrrOtkKxiqQ4C88gtUC0VluT0OVPbf+zv3+4DtcC0Zp3SA1TioKd BDhRbdi2i7z3bjVuIpDGa/ND2iONjs1hfhcCczNwVnbaITv1QAXGB17FOhNEiZu2J5aYM6C5H4l3 RGgWbOAPdilsI9dhRIifF5oiC4V/xecf3f8k8fPswdXnfPv06tHLFy+fvP/48eMfjKO/fH33FuLn zkmRarDVVg+yD+YvRA1ig7HTMyjT/KAmIeuBtH4xemQFJMjEcd7i6xX9zq+cef5qb+xs7O+hiG1s TeYEON8lIFY46C7R/ILKCQtLAgPa2iic/3wqB1RvpOOdd9w9ws/iUQqW4XkOgS92ZgMceXsYh8oJ em7a/LVzcHAN/useOtZKIX6WneQYTxpYYWSvZPHM6b+0AqMvnDTGKhSwA/UH+Scj1HZXZ53EthM7 0X9XaV+ZIE3kTyyms1nHwRAxcnbP0rDlksbHbQ9tDV0JTJZZ+udrmn/u3rqVxM/zq1cfIGAe007u 6tHjx0dXX5w+ffHg+bPHTx+/NSX9ycUP01+7Nt/L8sWc05D0w4wj45f+GE12QcxAtYZvgBA/qOzD 5eCvwDfKG7MHCCFwMBbIOy0QPD/AyQvju6hvYJ2IzQ9OqjkoIp7/fCoSvlFKG8mtlIPEAt4jyEzn IvYHTArpuONHYM/3wIE89CejXP5o/Byur3eYTkogjYLnZYanyoZfFusLsfPuR4sEF+b+kCY3zVUR QDStpIzzvTXEmtNOjs1ZK/bi2uLT55Q3CExk0HB2zNjk5my43Mxu8FXZQmv8lDMZ1U3//o7b/3TG zyntwFDATl9effACkfTi6tWnj/mxB3x19OD5y+dPEEgp8KHFz61HD++clCh13l7sQf2S9Q+2qvMS Pdb/9CRFbFSPqlLAeGVlA1Rn88OnlrarZ8dPZWHt4OBgjTewSfR3EE7A6hmCY1N9G5R8noQnM3R/ NimAiFp2/vOpHlCD0Jcgb+illqi64HXuaI5b6ACsVvO8JmIcJNr6dvmofarrw5sH2z2t6cuv2S87 T10k7gaRYwykOfVgNo8JMZrD+rDSLkv8hA6s722ODQ/mBnl0M7vOXiLrzJIkwLMqV2DJMfSsSOGP BVWn3OtGMAeFxv6QGEW40gesX9uKf75/d/7Yx89TOsm9lDz04gniB3no6Zs2309Pnzx48CTBrzJ+ vn5051WJBbXZGKQDCGNltNZg/6xpR6OHoaSRxJM8QdFyBmtp/CD9DIu3A7OrTl4rHfHzIcy/DlC9 FsitWBLVMSA1ClOoXziZznL7DG27WfTXUNQEgfki8fOjf3LOJ50e3Y4aqIeLIHYuur49ChJpphQQ OqGpqqTbDLtnxs/2fAsFa2SkNTo0/bqJiuOeUrqO6HrCWkpJh1Sh/kYV8VOfKcexYnr0c9QrXGwI SM9lFIkoD6SPHDA6TNnTywWVsnGqhAj9XrVN6XbJx3kQogOqEqmIrZGfv77U/Y+Ln9MHDI8Hzx4/ e3n1xfOrD1jHXsiHXj5+gvzz4gHL2lW+f/Xqk6T/uSPxczJHMkKzQVm/aYYIQT+ad3oscEZdGhpl SeMKUSD1i4s6gMkL4odffPbs82kRyJ+1g33in2n4hUVhgZo/hcIscPMYwQAEWoBsywCkNyan1i5w PlUJX0Ma+h7C63TpG2E4RwkFIzFOCRPpQac866CKWhPHP1X4/M40QE8jq81qmajUOWLH0oGUaI+t y2WsNy3PUTQCM0C/FTMgCwOP4LfuK+P5gEkwGwXVIG9603AsniBwpEibBnJdXd4zpeO/7jIV5dlz FY+Pk/i5dfytxQ/DBfHx8urLl1efP0/nnydITQ8en+Jjz4+eIEshkJ6m++dXXz+882qO0VxuwG1x iAUM89e8S0AWRtpN98j4LvtDph+EmQzwEkBczcvXnu00b7ryoQbQxA6UwxcOxLy7j33zFFnvfbS+ mERHRMUWboCW8HGcyP7vC8WP52R6h7jEjtJzZuOwQ5k+Un1NEy1wyonu6mUbI/4/ofvnT3cuky+w HFdXSRho98cUd4YfczEvQI0OXH3yRqnwreGJEuOHAqxqRZMA50N9ZB2tK3YEH5WmNHutRKIjzNh4 ZomTJw7FTFdT9l85hR/q9qer0MyI61IUfw/8qtAvwB+8e/1jyz8Yvp5oCWMSQiv98gnaZ0lAL95H OXv//Zfsgh6/cFUuyT+fM35W2cRXGkOG3wDocL4hBwttoaV11nl+1F1QSQxDFdMjvHTPw+NVee6+ Tf0HvAvxHtxA/cILwgYdzwDQQGh4gJsnDnFhC0K+SwPwQv31ReLnJ0Hy2Du0vKO3mzF15MWLHcRH pdX0d2Y8dD2ZykTAgw9wXtc/n+5Mz+AasQy2sriOKM9bbHAvgchcLcOfsj6q83pvWpm1Vwio+JPo f/B9WvZcNe/qZ5vPUIxtXPumVhG+q49Dp1NoVw8ACglAJC5aBOyI3+gyB4MuTUR6vqANfBPQNEI4 wu/Z/3zyids/u/h5ipzy5AGSD8Ln/ScaRk+kZD318XP09FRC7OhxOn6OET8ny1T/qcz3cPrCQDGK 8/uiwTUk88jNy13hBxXPyu5HkNFEkUkGGonT8LFLnfCN92a21vahfogj6sAkxH8gltDHFnpqKrek oj+bWC9OYvezADH6i5xP9QDvlFqczXTkj5AJVMYb6qatvL2egSsjVivcjSoqyv4Q4xf840br4jIo ivO0HbEVIq0m4ct96dKVDAQ1IadZowaid4EHXmhmpAQRl7wK2IlhxW7sTYD8kcW3bxmjhyvY2XYQ mSClJRs43SkRT2dq4v00l6x+ulIOGNI/i6j098a/+PIT9M8917/9cwdr+fTxU45YL49QsjjRX+VO 6AmLGNqip49fPjk6Svc/f9b4OTlZZnas4I7TQ/4F2uMW7l+GXdWTKV+kbimGjAHWcBcM9s8MoBF9 fDrzj1eva+2j/cH2Z2AN5pWUT0AJo4FTH2JnYbawhQMYxq81rp83ptb++SLx82PdonTYlCei80nb nILVx16nJXbi+vq4JTbf7v5UsvHr5v7O4eE6Sjz061rjhGoIQ0cHdsGOYQcL7fTsJdTv/gzkNEsj VFUg4WeoXiKJRwTs5J8PgmSfE4gxqDdRkLy465o409ZRTWZXs6gFJYcX2iTvOgvFOGXfpD8KrqCJ vjxBpt8bfkz6n8Hjb8++X8jC5/T5gwfPT1nbHjzT/pqboQep/PPnRxI/xydyHlldBHasZ5rhw8v6 fMNm9XlBa8yP6spZfyh+Q9tn4/TM1CcEk5npP9t8p+cAvoOAQBO+MUnpVdAsqJg5hZ3PAi4W8PxC 4wPrZRgSzu5dKH5+FqYMCDyjynErvHGeL2X6DI7d7sQkx9OmYJH/GwEZFvExJqD9g8Nk3NL+eHwk /6boWBWlBRhEzKAffLASk6YxIkMYCKjj9dXApnZVB1clgch0JbnfUausjN8+Kzl+V30ruJdSreuI 6svi0GV6D1FccN4XXV1eQJMlrZBrZ80b+Xv0z9uf2P109G3xk2aaPnvy5PH7T6SXfi5R5NvnU8s/ d15V2JDNNXAl7elhc8P4aTQULcZRS+D0XPgsSizJ/wCQCQKIEFZdH06Iqecb8FUtX+8NHezgAraF Ggak4QAsB3Oc3yFfx5K1RW7P5tomvHnWYN00dZHzqRxQNRwciD7wFu/u6KUrZRlkMg5j7P2cQq8Y Kr4hyd8jv68iwzvlD/evDQLicnn9cgdx0Ab2Ee9+oXqagq6PsjxoAM4awhkeAnZjkn+068mEjqTm WdcGiVbFXjoXmwa0bYjkK0hAA4Eb36zbq+aMfZozAHTBLYIKbR3VMro/dPgNzF9/OS9nGXvop6eP X2L9k3wQ+efkztffHr9qUg9qbl5XPMjOqEoaPQrXaOi13Qjwi2TykAZPBOIi84+OX5SWYB18S/7Z xuZwawfiLVsDyD6AHiJ2CrTNBV95YGuT4QPbOGSiKcjcfXSh+PllNYgDpwUSpapPkJIzDfzG0Lkk qmKUaRg5sk9sAFZ/0I/KN3X9A/Z7o17vWP2se8kxYjfIPcUqmglJ/XhUl1W48G06MFdxox+eczde 97hHTl0y3vVa7LG7yjlauLEE5d6VMaNNk/PKmJYI9X/MN67gBKRwyuA5o9BUf9HQ1y+Jn8+T+Hl8 zqvpafq3/fnR8S2NH/71c7XBwaHRnnnZCtYEAE0GBqoZE5CgfQQzppFj6FXJP3L8qg8X9alxdvxc 2od7E9DzqFQDOT41oDcGwVUMYRtUpN/aXNuYxYS2Sennjc2LnE8RP00jlbr/QlNnVMyDL0hRkECc XXry+5RAqaAq7BSY1hH/quanMr4Dvsp1DpDw+YkWTQY7oNDpZaIkpGHH0iAEkbULWrgxBOhnSg7Y E3jEs4poR942xeQZVXtZ/R7FvMBkstQP0h30zAgDr6sJdEynr66cjmC5paowT3Al/z51P03ln6c2 lj+VY9d5D/A+ftrcfxYXHTiMEgnYQLNtrrF66Z1Co6YmhOaaAz/b6MX983BR27wz4BtYAV0ic3CP 4s87fV3EOEOwt7CJ5hnS4RjfwY3f3NwQbwz8psm9X14kfiDhazNJJkEeKronMFUEUeVLyXypgo4C J5zOjjlMRyqIETp15bbln51rNSyA8s0IZHY4GFTZ14y1Bl/bRaewrUTCQElAAykPAlgkAppBytYu SEHJHH9WPWvU3FT1ATu26rGNXnHgrIylhQO6OVtITJcVPc99tBQ08tOzHBS+N/4O++c7Pn7kavpM wgg9ztGDF89xfv/BlHT6Hfrnu19/e/KqzW9UcZEcHUznCIvWYk1GLXIHhXpaaynhAv2y1C0hMgsF bJjXL2mhS6JaH6740T1tvrNC+CF2hGvg6UB4DDugPmptkHQ6tcYittW3hr0P1Z/X1jYvcj6VA6qb z6PYkn/o9ZlskxsHZgvi+404se52YA+319ODk/7J9o70zzs3D2uUQAoDyqrCNyKTXeGpJtvWzU9P p4VKOpTA0oAA2RzqF/aHxYBCXPIvZaLUGjzjUBoyBCIXZmKnoKYkZjVkz4iYnUR+JhsGif9IxviD he6utAEzLo3dXZNNWnJxVYT42eb9XePnxOLn9Ln0xc9kvpIXbqOPcOo6xdX09G0Z6buHx5/f/fr+ yQlNpcNiS66jbJYB021xx8yR3WoX/58RuCqUW0Sag7+Ls9cY8YdUUpjTs9HZ5nGZ/Q3ylymBiPwD 61wc32m/3A2yBQybMMRv8ni6hXcmt6b+288vFD//5C1MoyjxQvGOO15WMHDMUwds9VadZlIUxgl2 yw3P1X1tf3b252ewRl4VCuqc+cOBfcpRCzI/MmhNYGAfchJBPpKAaQUBtVQWAehi5PTBhYmjzkHi DOAFn/W67oDb/igWiTa0zOqCt9fPf1d7tljnL2cgJ3VM+ajoFv4XKEFqa2j9s8gf3hn19YsrwxeM ogd478EDfzB9zp8+RyS9fHr69vh5VRWnvRo3hPPcCEr9kjXhosB86jOuehEvNqOSLggfagDVBf1c F/hh+NfgPxs7OxsH+2sHe1tL4lMJojsB0N0QbCGDEAzULZzCQD0FifBC51McUN3yzT2F1fYj8PYS SaIX9QrRyTWpMaf07qQpzVLJk63C+NDmr4Pt7e3pUehFTciRXU+jpjvGVTTsKi+tXIqzUthqrrDp Lro2VsTvhanucNFvc1wHo+5J0nqF3iHQ5DwtJ+pxNaZlT+xMImM3Q8aadjF/pdaHKf9l9D/wx0Wg QePgm7t+/3M/qV+6ciZe4wUvYc+vPuEbC6Pn+uYlA+jpi9NO+Z9bjB/I/+AzzreI+0GjU2dkUH2j RojGDC9cCBkKRiFqeOeqU8hlZnjGNs+QURhGAsJmVVZZb6r/UH++vAD83gYW0ICPQW+ju0Cdjb4p wH5wNUULhL4ZaA6AEKfgHTf57xeLn3+MEoSVs5mKE3E9nbt24zBxKXAkzkT0zlRPPXPQ233H125q /4Mv4Nq2dciY2IeTiT1Ri6oq+/QS9eugq1AElpWnsd5WkRsgEgiLcZQsyLVllt2PCbnEBqAXdZ84 3jW7vjBRrM6ETl8/ciqJRqyVuT3nzQf1LdnwBY98tf5Z88+t6x3xwx3Py8d6oniGeobNIYPnKToi QwehwD3tzD+37jH/iJBIHiUL96x6a5gB1GpYm6w09zqZpuQr10V6Y5hyQHr1ohiQwp/1Mng2/OfD ZXC/2P4sHMzmlm4sdWF1CLDh5sAmpHoX0DPjOLa3AXAHkNBogi50PqWEbwLJSJTlE8RDYrcTOO1V b+AUugZDITYZHYoyqZ51/VNVrwN8aXvaA35k9dMzL/p1vLDbBpF3MSakoCrsUySkbIg4moOAHfIP YL5FzY5mqu70ifSdXS+1zSwYqMWss3gKMqFiOmK39hRKjhY/bCzDTC6Rnu9y/GVuhLoKYoKD+yn3 P9sO//zla/mHGUfjB62QXr4YP4B0vMT/PFu8TF9PvfwG5RPwuI/UULAAR50Zp/QYAsixc8ZlQThO iDMvpWh3xsfkHcYVfwXsQf5yWRPA2epjpb39vb0DOIAtkL2MFhpiz2DrwK5yCnsfMAexGdrbX5AR bGPj1xePH6lYGbttK3gjY8bFJmwaeuv3UOfeOFGwVCMBZ88g8/Ou67Mvy/oZBIz97dGZ3lQAOa2W Xjmxpyx5jMQM/WOck1HWgNyiXpAK2Blb2dEXI7dMDJMThuU+U5kXGRDxYFfvEbu7Bl77RWJq1+k/ 55xouOqvMiktVcUME0H2vfF3tH7defgXj169KpmG/c+Tx6dPnjB+gIXG22cvEDTPGD/PbExL4kfz zzdyBppAtWrIPDVcJy+QvU5NJOr4whzDVtnyDRHPTELEzvPXGFNlHUDfyD8fsoAVET4A0O8NbKwV KD4/SeMLgOUBQFxA74ytDw4Ya5sYvvAb9391sfj5ie39YndbcqrUThE8ZQQfe3Kys7qwahEZF942 Sd7dKhxk+qH+6vr8zDguOT29bw7s01bXRo0OX3IKv+6mgf1hpR/7w/qEanrGkXfSNLk0d7dVx6zA JKzUXlS7exvSzOLX6drJ6pQK4lXVHOtyt9MuZwCG/Y9VQZ2/LP8cD6bm9wcA/jw7EtTYERpoBNQT XOCJ+Dll/ADf+gRRlkb/SPxAvuUe8w8yI/MPtcWkOuGF20JuPMZJbqfUGOMEL3V5PayQ1WFK2siv jY81dXY5o34hgvJrh3scwSBAP0nB3iW0zRi/YJg7hZsXLqc4wO/BxxsfGNj6wz9fLH5+HASBN76N nJ+09MLmQaVkl8jdCMz4xYQTnUd2iiPmHVnwi6Nav3YOevDcGqnAuQDbnLHhBuNovff1vU+vnjQQ R86PB/M+QmmutLxSgpTvhNwg3JJZloG7Bm6P7BDmRdANeysLKUpvZMREy/y4hLqR0S2VIA3iXHda flVB9CLkstSOzanlG9y/rln83Lnu4od392dPTp8f2YlL0pD0zHgL1M/L09Ojo2fPjjrhP+8/cvET 7mar8RiVfWbIwxmv12Z0P4jkMiLSLBCq07fjFEuQiOHQPizFbGxEQqqdkYp9NvxwYm8PEYRXGwu0 jOvjlRSxgqF9amCDqFa0PVNLlAeaWtu4aPz8zKDzThUh8nFkMC1T/I+8bHWQAMYkXnYDpxrtzXic uUDQ0vnr5sEQtB4nytkYaFQ40GaBiq/QjlkHrfXLr7+QyKxY1gnKI61WNH6ixOPZO0cF5nKk13dv 76uq83JpFcNRc80SPXGH4BDNUjQ/YbZa6HIGch45JuHThfqVFa284JuU/gbi5+FfXrtynT598vzl iwdPJYwwbD3XOQwZ6ak1SakChvi5ZfkHn8MYOp6ZMWmGOZvLhodRQ3Uf0TlUsUPRahmbkJQzLn0R g2kEKppVKdhviZ+ZAzZA2EHvI/Fg+CqIXy5uX3DK3dzbw/ELP/b2ADEbwNu/u1j8/NT7d0Wuw/Ql yJWvBJJhNomhaQTp5UAF6xQyFid2qgylGU0/8H8HRgUCmmxxIH4NIjuUxzBqXYHiPMheLTXAcMYX aTHN3ukGCajRHFrGEf/ZONnwSCU0YtXz1SQYOO9iz+kXByfriQT0H8ZOvkhaIbTHudTuR2d41fPN 9TF6AKAPMt+k+x/kn79yLn2Clc9jWf8ojhXp5wXT0/PT1+R/QD9lah/HdgcNjaQTKj+ju2l/9UEW jMoiAogamQykCU1EEkTsglRJkxlopEpSUqJ+uNKhvlE7PET6AURsbXaS3Y9IHw5Arm5gFpYYvHuB G48Geo2hdLHzO+KnmhxOpXtw5PXA6b/5eIptKW18vShx+k7OYoHzDRBoRDDG9nkH8vPbvdOLY0WR e5xTZy+ix6CMAhHEK0Ah9rfLsvmZnu7Q910XAiEArM3SmAhIZXyxFMRG4JfhpsCa0EPUdN2QJOpj K409qRiRbaRNbi8k/936H9Ne9XIKk3ojw7/8TYp/eqfn+OEPnt2fPUNGevHi6ekpaT1yKDtNyf/c 0vjB9BfDzHwR7TICgWPC8MzY2JxbQx7d/uo9rMVg6iBaq3nJShOimiAJi9EzNqHtxZvwQ7H/Gtzb APYZDfQGIM60u8AJA2rzmN834WjJ0wbIy8hEANlvbFzsfGoHVOP7Bu54HiS6lHZliqxdcJg/BzdM 3OecO5jZYuteccLG9/3Dw2vr69LacGbvGLUIZkUZIfn00soKnFSKIzPEevTKjNZ7eXAYh9cmBMjG J7yXgsHZlPQVODmxyCDZZqDlxVw8DCVONqR6DFMjhMDhf9IOupaJJjOysgbtMFW/uD98eAGy+1Ni E58fPXiWko8S+Q3IJ7AVqyN+qAaFBINxCqTA/BdvEIAQSB9k2hUYFjGAxselix6znkghbm9RH7uM 8gX5Z2AQpwDcoGbLVG4W8Gfse/bQPsNaBdMZGPBkaWz+4ZcXi59f/Its3kJnEmGGlub8aFh0ZeI5 w5TE3Mn1Gubi7OLJ9jJ4ZPPS/aB/Bn5sWyUS2CLPixSidchEIRKroXJRUFbg5W8l5mmsBXmO6cEJ HuHnJryAnRNczJiQlLNaiJ0IkGbA2Ny/MsYDi0MVnKdIGXQ3MqK0GdtTp5DA5wvGBNMY6stSMgg9 Xvab41T+wfx+cfOC07R8wi2V3xAmCFz1qNsr6USKUvXq218kkPIiI45X7KyRf+SR6X+tfmn/88E1 BA8Y8PsbUP8BeoO8LyDHOLnPTm0soGdGB43sJK82/u0XF4sfHlCdAFCUUHKMT+4xPt670glsBJ0d ROBtDbyhMz9S5PFUE9D64KAxmK1hJgpRV9Elxz9VM2aeNCLBRGMXXa1UMMEvNyFAJihfrV8yxTvP WevV1WI99CoO8Ml1g0EmCNzrwN1X5IwqvxtCUtmCVx3LJTIKfPu/YqVDZ/tf3fnkt67/Ob7u88/p 4/OjNt6Q/6H8GHzLIYiEayluXWOU6RsbKR1d/YGX5YkRaYCkgx7Pa5no19BhJk+rR20f7i9w+jpY 2KLePDglIC8PQDMKSx900Zub5GaAXriJaFr4bxeMnx/9X7E2ARl3XndL5sgcLwKvT+fa1sCNWpHX 2HSQrI4GNwxKBj88OOxpDC+62FnvcMKgcYihWZ2i5rJZMRPMmuH6sB1DQKo+bu5F3l6KjU2cIDqU iWHNsToOBt6gIxN4JLdTAFYfcHFELSTu3UYEK+jP+qqxmIPHkfQ/Kt9y7zipX8+vCmoDaOfHF5Df UPmWeyffQwob8hsNcksbQITRO6U+lv2h8LlaydNmZXxcx/m8MBw76tdKAh873CP8B+lnDUcLLKDh PggavKyCJuXqxWSEWYwL6NkLnk9/9PN/TKzj/DYnStiadtsKU+7jDheRMAWdYKKjW7k1ULBs60MA gGYgBFXMwwZjOmEv93bsESUfKZhVzeErGkflZmU5Xp3A1SduFilA3vZ8atPM87TSjJolhkFK5jyN CQhctRMVcemh6LASZXLKeE8GeBNU6Orrj7PiYmj988di/3Xs65dRv45O9YT6RLjuz84RP6C/o34V 8bCH7VaNzNIGFTGRVGbqPxw/y4wfnb/QMRXV3eZs+OHKBpMPzl/7e0s4gIG8w4t7n1xRuYXGQEbg KoaztYELeOemJKBdMnGqcGGCykoxmuPAvC+SY6lpvqdsmbWxdWeQoMLkI/Bn7MNGSlUgx5BfVueI HptOmaikMT/MR7XhROF3dQ6a49Ey9j/1ahlpaoLPujxMsNTi0ag55i2lnDQHbXNX+sCkhwOnlk9/ sDj23oVBNZfI/qhtnJOTGtAiDa7Gq9T8leQfnCiI+HkuiglXre5w0fPs6V+NH8onUH4D3XkW8hs9 Q+ReODmElR8Kn6NlfhvYRMsCsaTPlJU31X8I/+HqB+P57P7hJsR66bkMoM8AZKMwvsO8AJUL0LFZ uGGAibFwwfMpPXQToecodBYYjiGs6xVjLXhvVIdyDtx5wBbUUcrwXp765R2tXzuHWI9NrLar6HOW kT/6CfzBMOHiqLcTfygfESwrRtn8CJBDcWUEd2jGz8SIYGDE2MjHkYZ3LGTk2PluJh6u5gcm3XYs 4mRi94gddMaeELnEOFehY06FfiDL1TWM4iPEz2+d/WCq/3lJivsDHtwT7A+mdczsuLo/RWV78vgs +R/JP5RPoE5tqyHg+VprWBeDH/xg/ED8b0yiR+b3EllMmRR8LOl/PnyvuSHLQ9if0qZykqqrfbRb JoNnk6M7NkHogxBCS/BX+fdf/uLnFzyA7TrzXlfFjF3qiKimvqTMQVfKzMZAA203GfrD5OKKnzeh /cz1z8EhDjxY46iBgYi0sEeWQzs9MEAb7ElJavY6SUTB1hOA2F4ewWa/2sQWW/35BNGpBF6kKjCc 1dc6UC5P7M10LemoULV87ll3PmXSyurdrlpwuB8BcSTNUPdAPyKSO+rw7Pr1vhCTn6v4z4sXj4Xn /tSog4/1wcb5/fTJy5dPO+RbTj5X+Q2Ux7K4D4IbB+g3F8rj7/1g/JQmdAnNq8b42Jyeb86Gj5XZ /uxvoEvemqJ83RKdv1T7ByCgPbQ/6J6xC9ri9nnvj5/9j//x3/+/X/3z3513D/QTb1Lp2KZKKQ2d mKBb/3urYwkstfMzsxJ36Ai8FrwioqP2jrbP+/sAkA21zAHVLX+kRcZ9oF8mhqgN8BilWdlTuySE l6FhOuiKAFC7OTzB+9mMoBxEw5ZAckq1lk1fz7BBnodmR41QNRpj8+ygL4+oCJHPihTE+ElYpx4B lGP8EJ5Gl4Mk/3D+6ogfHLkeGD+Z6glPBe6DLOR0XIyRmvB3KN9yovEDsG15sUGmu0wRY1wwf/VD 8fMF4kZWQHL9GpvTXddZ8J8P31vGdmefB9Q9UC44fk3S7p3mF1NcJk4RxgEcB+MIJLA/fvbZZ/z/ s1+f+4Bq3ISoQxnc9oQZC6bQjcqeLOb/gAberq7sMmpV7uSEED9sf3a4P7y2rr2Nm7XmPOxHRnYS vhhGK5m2gFmHTGH88vTMBDQ3V7HxGG+3x4vc6BPzUldTNcKA2Q9V+GlmtGxxNxS06RQeayEzh8Ss U68OJVmZ6j6/Stn/dHWlIYhmwDOQEfoOUprrfxg/15P5C7vlx88ev9Q++jnj57GggqjZItxlRBZu 8UcaRyn5qBORT+DtpQJgwhCsm3D0yvNmesb28PXxyy7yln9WA9FMXnmLedP+3hYFgBA/dC3g+AXx DVQvnlJR0AYWcE9FN42Dat++xM5nn/3ud+fEcfz8Z27nHCZoDTsTRA6Ias/qBCIdO4pp5OUXPJQi 0fgFLvQQ8aMJ6Nr6YnIohdbNfEt5OjZrUVNT/JiDLFDRvIwJBhHCrEN5zvOrIKAOt9v5VS5gmeWV eCAcBPI0yoFdRQ1aG5XrvBnB21G81P0v2FC2qzyyjCON5dK4+dQ7XQP93DeyAX9l+mOSf3z9OvVd D7HPTyx+9KPP3yd+QwReXp4+fvCa/A/yz/EryieE5dboNBUyW8PFImT9oH6EI3P/pa9uv20N9MWE LJ4lV/GSUVHMbqd3rouf1Y1rePruHYAEv4a1D61zEUK8xDP7mBQH1Z8hIT7wkcXPZ7/75wscUEPv XpOcSUWN226ONgUHUeJf6RJSnFL12vUetuoQgJ9d0/YZ4TO0ONZ7+fLlzns7icx18az0+WhZYIiQ 6mU6wnciAt0HLyQwDyMQbn8FXURZ4it0aoQbNOSzstLWzHoV75VrBEMIT3xRlpRiNet8vo1c6ESv Y6lfbndo6Hl5v2+Fhxua0yX5B/aV1+8+TMMPVTGK9y3koSenNtU/cPgfgW48vqoym0n8IP8scyVe mYH4COKnNkZ9ieUyFZJ4Ky3iUtiMr3z1xdHR6+lHeh+xAGMaKusT52z6RR7nU3z/9/YO92iPK4Y7 IMKTv0P3HbbS7IS64QO2OfXRH3/32e8kgM55h//5L6u+OLnu15ChcTLJdzB+XVsReRkeK2DKIwy8 zB1Kx7b2Pwf70zDJnZfehv+tr3egyGRkn0mN7FbWUNVg6w0LDCx9IEBWb1Y/SHb4sYSRHRSL5dhr fSrdqAzE8Myoc5Bo8D6A7Vye6ShWtLBtIWR/nksVLgFv2BzfPWVwxUz4yvXPd++l+h9e25+/eMIx ngIujKcHPJjKQ/70BUJG4ocT2YPO/HNM+mCFt9zVmkj9zM9gUCixdz76almcvYpzeWNSgvXU/94X lpC+EsyYLH8mLH74ZE+fTy8l8TMB8dtDAED3wX9f22TDQ5XDLbwCdRC9z4YYgVGFdWpq8tofUboY Pr/5+3MfUKkp6VjsspBz61ufZVI/YicqbqK/TgxMy5w3GnXHkMuSfjB/jaLcjJTbOI4O1xfdrPUG CbWnx3ny6EVDNoiVyupcswkBegjYffX6LeiL9wLKdcw1VUU8mQArteGZ1ih1LZRbLmzyRf7tRRG9 zjgGEr/mfocY8+dThz8cWMkEyhk6Ef0W63+uv37/On38hEdS8sDYLR89PlU6zxGUgZ68EOzhiwT/ Y/IJkN8gX430ZXyWVEpftctXu1Si2QPwGwgp5mY1g6UXbDtTGlNEoiAT+W5T2tKMLJ3fNI+7tn9t Yx8dxN4hYPSb2B1iVO+je/cC1KOImd+Y2tgChwcXsaWPfP/z9+c+oBq/PTBXksiNVYHJsqakEKMU pN6ByyIP2HKa4g6riJ8NMXzY/wzifJVv98MBdRV6z+0yDVAbCh/rdRYYtvvpVTArrtCWj/C6XJ5I xc/tla+Ovrh6lI6jfqenENj5oozmGs9r6qJAWUfN/SgvCHBjk1+IXl8zeuXoz3n4vGi36CGMoNYp bqppTRKfuP0h7hdvxk+C/nkMFaAXOsE/t6rGty+eHz15/zX5qONXZR7l5mpDPb3AgY8X5ypHV79A 3/NeZbV91F+ZY8CIJxFfzaGsUUFJjE8Vlyj9z8hIW6be7JUO/Iblnw8XDw529jc4wwBhWABKdWsA RoRrpBPCz2kD+hsbW7jP4w1O9J/90QLoo/PO7z//F9dWegmpjEnfRomSXZzgzy3NeHM5ZyJq5w1r X2MVCRvS/LO/PwqB1WK7PUf1KC5/Amhr9EPqBx5xMJ/p7T2Dx6wKv7IlXG2KgF051Mnki9VytvRF 6b3d6u2vrDfodxXX4ghLlfoMxPEYOtTmnqY9m9iLzgBtnjElBkEBEc2a6+7qHMCclO9sP59WGMJ2 T+z+pfP7lz94f4fqxpMXD46OXj573+CryXnsz18ff876dVJmsS1h+UXt5+HiHNLPexBkq5Sj21e/ uNRG6kHpImm3+gXbIqB/SwIDkrLNN7J/bssT963qPxvQbznAFghyvQPgCG6uSfECVwfRAwrG3t4W YEB7+H9t7zcY3tECffa7c5t400M31Q8beCP2+NTAgRI9bt791NnKJWIviTiQydwFDZm/iN7oHWrM iIMBvyXLDvcTiIEBUYgT/qSRwo5JPqqRgNrMD9dblVjj571SqVr8It/OL2fnvlqVB6caeo5aRjJQ uT5H1WisdockfsTfpoc4CbBd9OvKCsSV9NOs4DUca9DqGGOqa2DF6mL86jg1v18/J37jVG/zAEIf pcJH5KPukP7Ospst1UXveb4FiCafHTFm0dvS6JRWv7p9+4u4jPA5gnvRXJElTCy7xfi0qHlopCp9 ajaF3UAScvDDHgBXd7b2D/dhcIFT1xaaZhGKAvtrErRTJKGtrVny47FG3P5Iihf+O/8d/p80UMwc 0gmGB04MKOX6KDhRr2bnOYTOKMPZgVkbJb9thuufA9n/rPeiKgEkrvgxPKdWPX4MqB/Afi71Ax+V mOjqUQyqZYgfoINIQHXx80Xp9tV8dW5lLh+iN5D4aYYmp5ARh4IobM4sY5PEzF8qYmfdmlcrG/D0 6uVQhfDUj1CuqbnuTuuCbnW/6O7euhLbJrXz/tURPz98eD/tvKlCfuNzlW8BCCkozjA1Dg3WhhEv EjfNqsRPdVm/3hWGE+JoDvxcqWEUpVVpWmmBqvLcPtv89MPfonQdbKD9AUIVWQf3dlBO0S5D+RlM eEQPeDu4vAN7uLHxEZrnP0r8nP8O/49R2rY7kVYOPCven1Cdro7XyfUnem13PATQAYSCuvY/+4fX Drene6wqOY2WUkp8TNTCM1nEEQpbFWbMIzPzQwqGrk1wHKMDcyVM+ufbzeB2vhQWP8yLzHtFAf+x AVpxOqmXV/D3rK7mpYXAC56tGOJgfV2OFY2bwO+zBXc1la1zLpHi+HSFK1Hg8KX/Uf7Fvdfyz2O9 UBD1/P7pueVb7qp8CxqxYo3ih0OjNRSwZfkKb7eRiKBQ27x9mzPXF3rQyGSu9rObXimXdG02x2SE Bkgn4reo//wW1J39HSahAQzw7HKocgjI/Kw4LmMTBC3xrT4oAG3s/eHa7z77DWLod785/x3+H5ya hhcV0yBxsogdnC9PJAwMRSNcz0TDLvRQIO26x3T8OiD8cHHeg396HXzMwX4cXIM7xLiK78UKTHQR R7iwLubLNFDh4LYcvnYZOrr61VHbxU9anCwImsPtK/Qbq7Rq/MtXS74LnWsb8NlOvdhF9zv3glwy fgkZrGurP9bjK+Pnt8n+JxU/lDk8ZbsM0sWTBy+eUnfjXPFD+QS6IOZb0t8jM+I51exnzulH/LT5 xMq8h4C6/d5tzgjRF1dX8F3qv5pZXf3g6Oj2e2XED7dfelvqP3P9fOkQwnVreAwA/8HKB2xl+oDt 4ViBCQy6mVDgmKWvUyF3AzAOxtBHv/ndb35zfhrqvyrw09O47AbmLPx40XbMBadOGaQiLQo9KMjx nBNKRzjB+wXa58NreHbV3uR7rashc2oVbegxwlkjcWPuz4oJqgjYBR9a4HxVXTn6ML2fXQ59j4bl Mm6kzZkq42e5nM8zMitz1KFalVCqumNv5BNvobNv7nYojq7ZFX2GZIOT1P7wVjp+ngvr/YjrQV5P X8gG6MkLLIXeGj8qv4H8U2W852uyZUBnj88VXzqeJFfKV48wq1r9+iL+4OptCATcvnoJz7AvrvZX FOGxMldC/rH4eUv+Wdk/2NlZA3/5gCpjIF0A7bOJuwXsLDHBr2EaQyJaGOjbKszmbnQvdU9yjbr5 0flpYD9xHGTv52W1yO3XDGhol1FHxww6GD6R+Tp52rw9NnnNP/uH27iUj+OLdc3NeprM3Ct+zE4x Ku2iK2WNELKYBNTVyCFj0ElWR0rLxbIraKsZc5Rz2/J2vXoFwDNYZyzTwHm5WYb4fYngxmXhc0Um WM8bRibOOeeU7kTFRd7fWuHkiRk+c3L8SSp+7nr7ZRmunuviEIsf1QN6bjCOZ8+RkE6doGaHfALz z+dUp4omFhE9yD+In/LRFyv9Hx4dLSN+4NFo6farJuKnyaZoBe0iogg5F3PnEQZ8Me8Wia/47PyT PeDqhw3QIYA+kwtwucDwNQW5VbidQv95CtotW6AOopaJo+4AzHkKUxdQQfyxIyWkmxy5KoaOrueU WBOAqsn+BA6eGPg+22PPJBcU2T0Dvrq/TrPy5SoYqFK1EUcJBbUDPTbEy5i6MbsdIgBnlSg/NjMz FztgeXMivzxRDEfmvvD5x5/xNCG26/FKW5IZENRlQVaXyYTGS2xlLnbExyCrwqvmAGaddIHlbOuK 7Brxw+cf7J9vpfpnOZU+OGUYPX4Mihd1E+yqYeK9zzWOUvmI+Qf092O6vwfByOKQiD/XZibs6bCS ryBq+qtH6JqRZb8qX7r6RbuMLrofp5zbV69k2CN9mKnMEQc0UtSv+0zzwQ/be4d4AGDfdEAHpy1k GjBOF9BA4+ZOIuoGuDy5KdxU0U5D1WUAN/nNwtQFYNA/ixJr990gSrTg3H3UhLpckJn4sjdLiVIu qN4ezH5nVNrR88XhOkQS8svV9ir3P2LPVFWGxVBv7+s6iJaOGrxoqIDd3DLyT6s1F1Vc49Ncfa94 6WjkiithldAbx8kkGSH/ZGiYiZcSPcaQf5rLMStZxbyClK0vykFBQeV6TUPK55/urpsrxm4N0/Fz /daXLv/YAZW3dvpfPGYiIhjxObnLL3XJ+VQNDR74HPTdt8cqv7HLQ9AYjsQ4oOIAP56R3/8VvmJ5 y64Hrc+Hq5dQv9rlo6v9YM0hitocz44+IHIK/U9RcaNnww+b8G7C+hkaiIeQgObCZ2thfxYL5zXa NdECAzYGOMVPDgDVMZCbzcHKKXcBFaCf/zRMzHF3HUfc9E5So5lzs9wNUyo/Tt1011zo7B7pLEvC cHnngOZxwP/g6TU+lwDIqKwaED5GuEZ+vJE6afh1dK8Oazg5lMpxUQQ083PNzBV3mD66esl3QE2l whrHEZ9Xu97O8J9DORSxcjvPMpT61TVMpe0YcNA/dMhVv4Y2AtjNFTvPoH/+JJV/fP0yALT0PkCQ ST4y3Nizp7il8u7+8nX+8kNHX5bF7Bi+evXWQSMIuZoScT0oXUer9uVVi6jbX72H5fRXWJodXW2X JYDQ/xS5gJ6TbUqw8pbz+xriZwNL6IUB7AxBFlSfSrTR5H+hH+JHIMra1T3ZDTcVvF3qXruACsdP q9oRiz6yKPMGiTF27MuT4eUzDkYcR6ljBfGTptvt5zOJrbLWL/TP29u9PfWRvALI9EJKlRZsogMO 7ZFKRqVWiD4pgUBYKrfxTW2VMnkEE9RLGFHVDoCDq7wZLoDYBrXrzWguD8h+XpclGkjSQPeLJUaQ wtr2F7oT6FjKB7Urx/wjfMT4xOR7P7mb7n94Kn35wnXQijw8emaHC0EiWjA9f5Gc3yX/SPzIbAcT GZpXQj98UVXGaE05jPXnyPIXR1c//OAoP9GUpk+2QHhVmevPXrote1T2PyWuIzLZs8/vqzt7+zyd btxcG4A/0xYslxeYhLYKG1t9e/DMFR1ogKBRu3IwB4NkzVLhIiz4XzZt6xx6zrgXgQ7cJcuB1J0R UtJ++ondw8yi3cAsCoPdMlbnUr+uXdteH1SZFvNAtYRQdtaV/dkATxySvico0OH47yCgjk/g/k4B slJEG0vRlxQHR4hyzjVxl8Yj96HlyFicVFCa2vUKXJvHhDuOf5CXSLGTRzKiIJ3Ax3bNBCHMLnV5 y7jOI8ZCv9XwXbf/6eyfXzJmeGKn0YVUKPTQcEtxCnb4LQ4hlJJP8PItMjnVG3QaQgNEud4azZXp gEFKKo6GE+PFEsA+nMnyKw79vHp09AW+6mqpCCTH2JzykK50ujdZ/BQRPYcHG4d4GNAybyFeBra2 oNW7NiAA1sk+LKT7lqY4dk3lAIheQANUWLuIB+qycyFJ7lmxZ+QoOj7cNWSqE98wlUpvtLVrJ43Y 2QPabbvJ7LOj/OXRmSS1DA3ScTCf8i9QT2aUkli8vWmmMrJIHvPlwRHYrSyDwAyZ2jwklmZq81S7 VYsImtBAmrPSDLyanvAqourMarsoKKuJ/GBPfa4yAbsoeSny9u5VgCRnZgueeOGO8Dk11L25Yoy3 +PhOMn/5/ucUhB1JMQr9earj/AvqJZilE0PsyXMfTF5+TOuXCA4A/gPPGHo10ZQJUBNx8B4Vpx2R koIwYgur5rF8fBtboKMP8jpG3MaTgvJ1y6pv9Cb8kBvoiQOun5F/NmahQLbWB6oOkM4Q7YX0z8DS 2hY6oSUKi0MOEWpH0KeH7GzX/3sRD8t/8WucIPT2OzZwxYGz5PY0MHWK1vXibuAlFpy5k4s5XSa2 921+v3Z5cbh+Bn5sXqzhZYVYWvWaUUCz0o/50iUcfUpFMghJgB8uBkWgpoZb0CZdpA+6mYwQEVvy 7Byj7VdncCOjuCR0f0eHalDJWx7Lk2E+ka+qBbnqqIkFT6And9N87nJCdvjZxopwS2AEfiLyG4Kf /xb7n+86wBtiviMGBeydLeMQ/gzvnQcPgOV40Nn/3L8F+Q3QBwULOdOYhlcMbivk8CCzivEOspH6 feErRShRExGnZKxA8qv5ibn38FffLhfJ4hnGWkP4Smert+T3Nw6xQlw72MG+p28WEi4E/kDzkPY7 mwQkTm0VMMT3FQoQh75BbsaNrguJ2P2TuNbYftBIChYSbpaKPCYxuZO6a0ccOY+nSG203W6Rv6N6 aPnn2igGMBFWXU8tf9ZdGCEdeUfmZe+nAnQ9NH5h74aQKo1RwLc4DPTzDJ+l4ilC3SU6QsyM581+ 1TRlAJ2dGSvR9IkC/5AkYB1DNSBWeqJNSYRMxpYPYeDuF91evq7Lt9MbVwJxiuf9/RMn/4P9873v 3kJwh/Pyy8cPFC6vaQeWzMhPz9Pzl8q3IH7ESbM22ivmcQiaRXxto/Pil0vP5R4C4ObVzYla4hRF JO8d2I2SoOfIYq4v63PmbPO4kWuHmL4gQLAP/XDknwUcK7a46wF7ZwrR0yf23X05WDBDmrUw2UWK 4YVEpP4pcrdHZ/uuAMOUDY8upO2iGjilltB85Mgj96ufyGsq8APVaweWfyBgN0YFIOyRa6MqpbDe yUB1zZEJK9DqssLtDbaIlUpmjgKIYXEYuyPqaIslhNlYg9oKCm/s7ZiluamSoDHag/mN5rxkK0xQ QLCF+1czcNTrjCC5UTILXe7g3tE/d3fvrIiRZ4b16zgVP3e/+2veO6dPn1P2Wcf2p25GS+DzjyT/ kP5OX47avLTPtIeje1xjVO1yzX6wZ1TtuwF/Qy4S8XnlnIwQ4E25oLIS0M8+nzYO97dvMoJEQhyA VVo5odPp3liADw8WhngFPU1SMbqAjaa/XKHw04sxUJVrZx4XBotJWYmKzbJqp9jNMXAFz1u9Z7yK kAChrf4F1W3OX4eInwaeNaVmtsyFYJXC8+iSU8NWikKImb2xOCy7H4kjVLbSKvJPfXEiLI0R+Vyn BDKV3ugIgWjCvJvv9P4Noyok4FpsLBS5MTRKRwT8wbEZqCUHgTGPTPc8U0h5n/rlM4vZwRV1GXP9 j+JXhz7+a/GTiqSnz58jalw+6pBv0fih+TMEr7k/BINnEO5xDYkXdNPimismhIrClTKmVgY1finC Xaamb1nEkDKXOud3yz+LQD8fcoOys7F2CIjGFnyaILi62Tc83rP9W+ykC9ASv4HhvbCELxraHJsD /8+NC4ko/IMjIwd+dWOdcui83QOH+/H0d2fx7Q8WXlHOX0HkQ+vUbiH9YqhWnygB+k4wL411cGqH zjMPpLWey53GPO7IKijEEWKlSstVxA/yD2o/Nx5M4qLhL/+TBVVUyEBkZoooOfg4FKQFQNYr2B/B /zRAXK2oUL3J01Pel/VLhaOcbkvO5x9bYFTfdr84Zxx1yNCjdN1S+Q00YO3FUWIkxeEdNk10LhDn SvHPpTHYkOJv+QviaCDecaxipuvblGyfObN+fdhzDaHDHSKQG1v4D5M6IYd7ayiIfxwaWpuanMXS J3cDnTNeL3Ut/OZXP7oYA/VfnXPNbhh4gUEHDfbC9FHif6p9jkFZ44yjZzgpFfUyUyJiPH0g8/s1 ykeNCoCMOQUgVhNqIfAHolEYttAFpK7zKS/dITigzpVFADGMcc7C0gysnZkxMW8Uvi+eikXq0mlT L3THXcTP2DB8bfAICPzQAIh4BOqVUKWBvMBIKPufnF8hJjq+XTsrcJKXgfP4zsdJ/Ny699377/BC +THGT56Jv92Yn5b2Z1S6OXQ6nOXx2ToHeL4rExniZ3F+UNRZhXgrIlL14bZiUN6MH/6HJ/DBp2uQ 0NnBlQsqPwNbIO304Qo/+LvW4OjQ/iylELv74IoKZ4Ol7hubH11QwfdHP4nTouDG0nHDFonVpsSR 3lOHnW7x5lrr+mdv4xxF0wLfwP0U+58e0XrWwjSn7U3FPAxEfqyfux+iWZ0BhhHhsXbE5hr4jQln ZqFhhO6xLhxM9kyxbpQ9liSmtUhrUd2w+WyWdmIQFWC84tnYrmXS+b2rw4RHU9DOCjV/+aXY/P6l 4jfeMX5AX6Z8FH2XonINMc4FNLplCGk21G9Zyhdfjar1+6hsLNggYeBU88phjgfo7dpCOcmeeb74 oPfw4BDww4NDQcnT8B3DFqjKs+vbzGhbfdxKQ1cTUA5ggXKTex9dUIFVD6hp57jISfO6Q7qg6OPA GxqEiRGX7RKdqq+Vu8ihy8LRA6lf3D9fHh1S/bFphlGi9FxxVhhRLIT4fjBzAPsZ8uye+gSCjY33 hK4J/L8eVsm9QEeNG5AeQ3czDk0b49zPAFrUKXhUVnLipzVcIdlFDBAMXBC6+LHGWYlgwkk9WBH+ dpCppuRX7936+B3jh/I/jzB/8bvWnB8dEmw2RejFPaVBB9Qhix+JfhQ0KWANsSSkz1xLfLuZfupV +eafDd/4YPra4TWWANjnQnF1YwqCUWsAbgz0rc////N/rCFsFsgm7Jvs7lu6Mdk9sHdRBdaf/yyW CSt2ws6+DY7U8lrCR/0CPNLHu124YDJ6huk+m5ITXoD+5wIL8dNbG55PZna8HuqR9iaR+1Eqc7sd 8DLWfyWDqiaSmhOEkCN+6mMSJKpQZxLyav3TLpdjR99Ra54wHifRGc21rOTmaeXH8IHFxBjzjxqP y9cEGHS2YJevrkSCQw0JD1YMYFk9Tt2/Bi/Y/5yRf0R+o8gVW3mewxZjpNGAyMjiomwQmW4GWc1c /yzPA86c/LEoBnLa/4xJ/Dj46orbQqt45qXR6fX17X2A4w9A14FPCvCruIFBd2Oo9Z/AHQ3koLuR A4uwD1FUWOoe+OiiCqzw0NWqZZs3p+wTJigeP1oFDl8WpG17lNgcO8vaBHmPHzNyPcX++TK0oWqc 2Xs7Nog0052nYLilIx3a6e5dlSjqh2Z0lQtEMKAoYBco1SM2YJLJfNryKkjzrTneosnm7lYGFn7L dWwpKys+EAiHyEd1+scZA1VCaP9KVp3lq3d8/3Mf8fOu+efzzyn/U8IXky3DvRudvbgvD4u0yKJm Glp4z4sR6iDDaV4z06LZyc2YfVx9hOgkDx+79Bp8jLby2+vQPwTPq28N+MNJ3FM3IdVS5yFodIGL oIElEAUoWXNjtntt74IKrPDQDcKUg7tmkMBJidlZLN41+TobtdSnxGQ6nNJLaFpBmcBxCMOxHSlg h9d6Qd0bLopGr8OP+TWicL4GTTXKiyEKLBq9ETJSmXMbuIfeesB5B0axczR1aviRKRWFOC2KyCQN rmeo6i6jGrqiiYr+NXIq04t91vCrua7UHVUohAdXxJIOmyyz37H56137H5WPKiF8osoo6cssX6N0 2YEBIUwH2bgxYTaYghBPYgk2rw7MuGlQeQTfT87viB957M6uX/0YHH67vb2PNcoaLQYBE2vl6/mJ +Xpp4j9nWoN9XZNbk7NLBeqGTt7AFX5h74IKrDygBrtC2Ircs9h2gYEztEwhCmMb463nMbFld26l yqmpeQQiqjnCzRXzzzRXXuV+kFCIIIMwgid9pX0MOpjM6oC6TP6p0nhLzWpK99XMmeKUVZmOVXQL 5Ns2GS+yYpMoGqfiVJ06+B1K+/STk/1hl7U+HQdUzO+BhFr12Mn/vHv8/OVrlW85Ed+35VEpX7Sw rPG6V2uovzviCJ0y2mVsFBeleZNfSGRruFzHy4RSBs6G/1QH56d/u35tn+gxeHVDR2qhgXXHIM72 RFSBYTm9voHOmbpHELXr7t77t4vGzy8qyRBl4RIHgfPvC73jjfMEs+97kOAVY7c1Svg7saEY8zvS /uD+NYMxqlmtIApgp5xBacIyCLpRQwkGcT0FQ4SwQkvv9IRekPNjykHLItERGObRQ9Wc7o+jEkVO zDyOCXqEIMGYKr0Abl5WzH/GvkZkl5XE/0Jh9EpfRiztX6FCHuvX8Z3rfv/c8479M+RbbkE+4WQZ zVe4yk0z0g+yDG6EqLYNUcdanBcfucWa9jyy9XHWuXgmDIv5IAo01FdFVP9s+Gobq4ttdNDizwQL XRzeWxMo6fhGQPe/8Z+NsYnhGhL14GDvNcizThamL6zAigOqrXv8+dHUWFJPdHdWcsK9jmseOOvc NPg1tB0Afpbf0fRzbX1oaH5mmQy40qoi5Kthph9ze7UsO8QUiLV3OnWnn29RpZYgngkqmwh7NUOl 8ZUVijg71JpTjQmcnkzsmWoKzY5tdQS8ZyXyEr5m20D93g77AtOAlvgJRbAj3T/f6zn+W8QP6O/0 IZtrMH6g/zPf00CFlQLWYhTRPFccvRE8DTNgVhNUhNm4SLhQxCaflZXGSoo+uJKojw32rq9fOwR7 h465EEtY2JCt2QRyPGwTavgbxgaHR+hdJ//W/PxFFVgpAe3AGlEqo+jCx+mQmUFS5Cx49AyfmEb6 gFPsfeAuqEWyL/Z1fscCkUdSA5CBFSFLxFh2iP20ihuRMOpdT900GFLUXxHeHKKIloUc0qBaBpmg K+D54G2/k4L1xq3qGRS5ST/xT2Q6qsaB4whAWZVmZUEhlwqcLndBxTuHV2zf3ub8/onuD++hf/7z O9cv5J8K1clL2BUOKiKlgZ25WOVi7yAvLYYON87yrpy+aEc4rNpjI7KIL4qITrDypvkX4WNDKF/r gqDHphDMwamtlqovFMdmGiiYUKCqiWEdchwLZc+FFVh5QPVPYKq0Oyn6MGHiZJ0nrggMxo4lJg/R rqINM6HfGOk2QBLQ3IHOX4if9aGGYw+KNEJCs5BsBALBSiDOlWQyJ/tnxM84r81E7wjNR2wv7Q8B dtavrtXk0wsc1Zx1YjMSF1kIqUHOJDVjIudigqF7dbu/d7Q+CmS9doWRhoaqKvbdn+j+efTd40fk EyrsFIsCQuGJvadGtEBLLZdlzSx9kBh4y8oQ11/qZgEzNmzeTTTvpsXCbriyclb9Kh6sb1+7BvjY NSAPgX8GxWtkmKRDzjL/OYJzYH2sxTEVuWeogTj+7MIKrPRAVauvyFupay6KveaYTmOBG+tDv6GO HBvVYV6t3MUqWRmuCnmQ+MP1nlY9GduxQ5wx1ldpNeWF0a72h4gFgY/NcBUNBGJjRNmj+YkZVHMK l1krpIr1zEYcX2nBi4QEvpiap8QKQlEreFVBDxWzkbH9I52+ZbWl/XOXu4GlQmj/SqTqru077H+s fg0e3/9bxM8JOvlsVJyX+BmU+atu9u5skRfFdpktc0v833krHpa+mflHBFy4QCyp8rY9jd6AH8rp dGMHHnKT2AJNbe3Tw2diGI0lLtEoYqiEtXGhd4/yiPK7X184fv7BTg7OCj7yxAsNnDixeY+Sa6kx l51wS+T9LL2FCh+qyj6hJ4SvDmIAw/5HxnYnId5rALLi65YqIKGioK8EDKPWBAYpTlL5kRnVROzt CKNVdweBhkIVbRFsD65IHAVxFHiTaAW3hmk6tn59/DKyBeOemuxPl8ofAovI+iW/pW323VK/rr9r /qH8GOU36ElfxHyuR3fx6W5I+8Psg8AhCEXbITpaUvAR+JO6WTeNaAKaiwlQeov62MTB9rV9MuD3 1wA5xP1idmP/t9PbgzWkcTx9xwDoZAaaqc9jFmuM9iz+5lcXjp9/DQLP3YzUXy30irzmMxgZh9Cp a7hNodrZxh1jf2jrH364bOeLa9t4wIfpYLm+noKOra+7bNRyMnY6sltiISwaDqjtJsXGsEHslRc9 r4q47yBFJx0Ktuz+UDaWVIRnJP64pc1AfXWFtbPr157qYLCy5EwHu3PJHYwBdXiFYxpU4tuJfQru X++Yf/7rrshvnDSxV8sU6feOx67BzphxwzXPIoNqsaGW8DOLM9o5s3aJre74iHoRUsR3NcqyWp8N PxwDeGP/EAKC1G/Zmi0UtljDljan8nPsJxvzLF5Q+GshePFJ1Ob/D+LnJ4BnKgE1sK7ZO1wHzpTH emsHOrS9ofOr1dAR7e4M2TyyQZRIax7q+HW43UDXTyFenD2HF80IVcJnPeWoQu5gPqHEq4EB+KfL FJvIF8dMcao32R2p3tQg5PF8X+5sWOL+kOIw4imCOOKmcdeMcwNndallrT+XQA5TF1T8uLZiR5q2 v59y/vr4neIH8i0nd0AfvNXmJ1Jc5FW9Z5TW3S1uCOeJeSPYR+Cr3Bguihc8q9u4lC7zHtQEVJHu 9C3ww9o2HwA6EBL7DHg8xebBE9zCHAyg3kE3OM0H6E17hhC3tdHFz37zzxeOnx87W1BbGkZhguXx Dhdhip1jz2i7Prl0s+v+iPMy4Jv2oYxf6J8X6WCARTJXOE1MQatuan+dPnh5aDBtYEDWesnmd7bQ WCo3vMJLOh2hqg0utkhdFfBiRaoaoUYZhtAH7C/7GUZ6et/1PkGp/rm7c4bHHgj9c8b6H+PvQL4F 9/d3jR+lv7fxyIO+zJs7QmRRLnVYPaOcAcuBvSET0Ogo1ogK1ZXWiC7NbH6kjRkRVRv5Ks42/xpE 8OB8unG4D5o7tFomF4j+gYAC9mrA9B708epVIPIQx7G1w+2PPvq7C8fPz3Z1528o1cBYFhYq0uXE UeDup4F5g5n7sYfWOwtCD7SXt+1DPX9BvmV0fia/WrL9D7E/8B5sM4wWB3tfgyB6nRcRQ8yzzyuJ /Sd+AvohcgvqmQpO9fYmeUgzk9z3x0Ycz8yLVeFWAH7QCs4hlC33Jsx87fU3ur2InclJXbvCWysm t3b6/vWO9Yvxc0fkNwjDzrPF6ZnRXkdhAvOKeSZHYJ6kjEUBjslKmqcLXGK4/ZlQa+YyjgcosB3R 482/hnC5APVrbQek5QFmnz6eMdamNoriZghE61IfDqjAH1Aaunty46O/v3j8xG6bnBLXiLymS5we uCKzOnC7Xn038OovieWKlkEAoCV8AP+hFNSEHiLkgVUXAwjgQKiFLqhQcXuNxiw+ukM9TEZFOjiO jKm8As5itNvrB9KD/fXotGWiy9MuhswXiivstFB5udomdA0iF3PCg61mtPSu6P4w4Q06IGtu+5Kx cZs6f0n/fPfWO9cvyv8gfuisOYLIWdT5apE1q6GHU72ZjsrZYtRuFzrYy2BWNwz0yFiThngJ/Fm9 L7x5HMkXexugkW+gatHkoo8lbGCrOEMoJ34+tbUF+nKucOPGjaX/WNr7zU8vHD9goMZpz/dU7ERO zyJI2XJHjhUvtcppTfmhLREbZ+La3uf4LvzBbUiQ9QD/N5437SjV2JAag6NVFnDWVYZRr4OOWZfT gx09tf80gPKqZ8s/WCUMFosfID1KyEZD6eDrNSfe3o5RDd1XiRDspkpIl23N1V/oStin3Wkvy+0r ZsPbRP/s9H/uvWP9+k7lfyDfAnfeCCg4ph4uDdE0D5K/w2M788+oJCD+pEHU2CiGpIaEUb0lzbQY 6jYli2Y66YNOPeqakN+BITuYzAHrA+jhUgEqHIWDCWon5hFOcMLoAvUrV+i60bU0+YePfnnh+Pll O0wfktQd17mHRknJUg8JZ1zgRaLdESCFBkroq/H6voXPNa6g16VjFqKFNjim+KN0nWo7RFLJ8qIx vjjkIuByT32CLRC+VfVprmg57xf9uC8ARqDv4MRC/CJly7SptiCylIR/k6bY+LPMR02xQaCSncTP SiGtP6+dj3ZC21d4COb8fv068s9HxM/fvdvzTvXr9DvJP6hf1F5E/KAeyaoHwaK0SFxUdaNI6A+3 wjzGC2NJT2FSwwBGEVPvdobU8dfgq1e8+s/BTTQ/G3sLfShaUxBO2FoDhmxgH+3k2MSY+i2SMliA gBQArB9d+PzFA6oTvSUyNS1GbwugTKhojShR4YhDu4fJb4+Eee4GeLdvlJ8OWf/Ml/XXaV9mGJf4 V1b0ooGTRn/URhiByTx9uWeiIr6xOPdoamJKqY/l/R90MFgeNa5kIVsGGOx8T2qDfdkVt5kx6CmX 4EJH5cmx4bLZF/Z7/FiXl85UABDyTyj+O5Z/pH59OfrJu+Wfhye37kn8EM2Arfo4UxDpOYZ9HhTQ mAQTUa3zgqkfldlM7xoC4UAvzQRUlf1o9gz4xofv9R9QfXVj63BjD+rgXEFj9ILgT98G5CzQOxW6 UbbQ+Ajt9kbuRuGjf79w+MgBNfDnz9AhxYyCk9L2dcdKX508CTiMUtpjTvpOwmxQ40ciCAmok2Kh 41ZPB/LH2Q6221HEMCpXVokfq/D0OaytzWXdSw8xpegfTHuxaFG7khE0NeNP05H8qI+VBMMP2WAO cmW7FnP/87qApnbQrF+iGdG8jvlr3QRY59+xfpn8zytB7ojHO3Y5WA3WyP0i2FnwqqhiPY3B+R4l X7CCSQbim5qg4XBtRRlS6dlsh3euq18VKodTGRPgDSwQtxboXrAF9fCtJUge0nZnaQqtT/d/gMHz Hze6b/zv9r7tt+4qzdJUAQnUhW7VoHlpaWqkrqqumm79fntHx7Ed52wrBx8F+W6D72DjyLJ9bOw3 k44iEoMQxpLFEzjhAUxLaYOVLhSN5BfmjYdWhEb9kH9ovvVd9t7HNl1Fpk0VcH4G54JJnJzl77q+ tS5e//bjH1pglFFSviySj3rBxy26l4z+JKYSXsR9eyK2UjSqbGUOpfTTgxg/G3466ct+Sa7a88GP RCOav/YPjA0PxzFOlGel4LIyjFDd35lGh9p08dRooV+uDidVjlNWrAV6kgCbOg5jANDsMKkoj9Hv QNfko0usxIpZVWOoOX2lIdCtLmlGOX+Nj8sC7PPB+mf//u//9z8eGz8q3/K1pxl5hS4D4Lu8xNaD NA5eZNI2xSHi0YNKTxii8CMC+qCQKZGMlNCxR+/rE7OzWseJ9ReV0FPkvMwKph9i9f4KNOteuoE7 Hrp4v/qSHJu2X9u5Ru8uDu1QBfQY7RckEKNpYJSqs8NlF60Jy8T4KeJ9V1lE+Wjdw0epRPngRa5/ MEEkAFH5DB3xyCBr0nqOXfu6ncTP2Z5+ZZKLZ1r3UI3ZW+2JkmU9sYEXAcqoozgttTkWIRTFutDi EYzmh4kMRN6Pk7DzobIDxiO0HHVd19SuUoeIqiJFVdCtRkXY0mtU/oxLAfT53d763c+++PL/0AMY fWscqfzGvSMe/K/hlGQEtySEB3DdACNwJmklDvIqnmVkMdwwExmRQxCVQyiA5PIE8afRlW+/bP01 TZOet197l+TnSXX+xms3XoJ63UvX3qAyGseC1LjTycXVdrr7guwhFdF/98xj4OeJgusX3UpsWWte OvPwc8n2Wo6Xt2JbFqUQS3WylOOLeKwx+zYAxBOgdyf4GI6OS6mI5dUnXaGu45i5yddbzStV54XD Sh90WAZgoTHArjSrKLCrcudjU0Qtt1FU9afaiKY/07JQq3jq9ymWTUEKaFJsDKf4cy6kfs58u4VA hoi02dBEvra3Pa4Ejs8+H+zd5u/QKuOrLwlHXxKMvkU4+neW3/ji3sNS9kEw1KRPjG4ExkbY1Z3v I+1fWp4ucyUNjmsvchimjZhJ4xh+bFiUsGqnm8e9/uHt125QBfQWgefGK1feQgYj9jzull8m5V7q xl6i0Q+pRh0i+uz887dm/wiDPqKjEpdXZWqpZKyoo590Iqyb0iLR150aE6o7O743KvMfRCA6Uqn3 YMe7NCJOhBQp8OJOEztQKBsZ90dOd+pYjNGMHk5XFOZHR8SJCRxEOhfjkFKN6zCLSqIOrNFIURRX IdOggiD+wMJwStN0Y6g9Ny2IFpbtFzsb/KfwTuMP1z+fEuFrYlvXYZ9jo/HFV18hHH3552U1k/95 yPLlIPDjYATEkmJGWW6jTE1lLM2q0dkixkMQ42BKK1ET10HIHZ2Um8lvMI8D8fldcu+m9Sm5pbwE CfqXaBJ95eoLF1/YGboyRO37EJF2KQRxJf36rx4DPsSAVjq6EuYLpckXZgAWperKOGG0A68o/JIs CrVkEpbWAMof6Idv9solMWX0BYhpYn8sKvw0kUYwmp6UOuWYygsFI1jNDfOsHrjj8Y/OlAti/XBm EkmGnuSbqmqcqI3SKT3tZin00QyI5kw4gJ7SwUNjyPRWm0hA9L3NLjZUJSWjve1tG0DfpVhA19Dj 43UqirbvfUodvcyFgKM/A0ZffrWt8i2i/ypmD+LfCPU0XwF3GyOLEcQi0P5neWtB9Gi6/Ae7DCX0 MhO7h9kJ5hvW78PvQL739mvvvPPaa6TQ+wKLb5BAAm0srtA/XPNcxPH7ITyrdtr/+ZePg59zzzHN Cm40XpnFRVFEVRZZdFk4giiQgzlcps6qZOScKq3uByXbTxGANuvVzk0cEQt+oIXBFCj2kRnLYEQ5 jbqmJkMV6rKGxXaQDZiYx2oKv9CJJja1eGiQpU+2B5HN2ASGP5zU8LtQSkT8mSMxDtoYTG1J2deV 9A/btXVXIn1nlwZUij8IP28CKDcpmeAqGn+a/f2j9f39B5+JOAd3Z3/84gtEo//95TcVR19+cQ/4 ufcQkwpZHYGhhBgUAqtVFBX5MpwhCWsKRyNcW9OXD1t2jw4ASvCApa/DkWFZOTWayh+NP919b16n NzqBf+MGlBPohJm6rpcvvgC9jasvXdxpP4TJEO7e8dUztPP6k4+Dn7af+yI9RmUtogNGYXIshXFZ ndJYvYq48BrAs40cH+9JisOHDKP6eedduqzgEUwnnD6X5fyNMdQvHrCMIoUR1mNEeSdjnrqAYX1g jr1n+gaicD1XOHPWb5E/xgbqG9q2o91a0hliZ9Mh/SZNCqCciAZ+DoVG/9I0y+056d9PEbBrvzjR YFtV8vOx8HP387vbjB+cpffu0SZqdG9/fx/mYJskcGckVyqOBEYIR8eKI5H/+YzqH75jK6EhoLTP SvClONkXrE3Gw7cihiM5+Id93ICG77EV1gCufAP951V6yJHgxRdf/Pjj62+9QVqZV155feiFi6TY O3TxhWsv0eCHjr922umfQ5r//OP5x8LP3ybwhEL1fopCzb8KUD6V+CxiLkqyV5Z9EfnFxuVQCxXO DZOcvjYpc4E21sNyKiSDQbcGi3zOx2l+hI3P2c5co5EYl67xmnRweGp6BQKGw30Tm2Y6p5yhsSiB F1canjhkpZzS160/MzNeePkMYwkLO+f+pSknFIKuoXxvkdwrLxJ+2KPZcf916+YtBsdN0lKpcigd JPQfHX31+f7R/oNxfm6Ob44DSXcZR5LVsuLoPyJ+iP78EKdnDpezZERPVEhFTiXZREhNGfAhFdKE QPfId//4yhtlj8vJFZmgnIKf7u4LA6/a8+L1F/Fc//itf/yb3/+e5ObJRJe27ldZtA7Li52dw/ar v38s+MgGNZTsdAWx4+CjiUFcaBWqgKjsjcIsAIp0dOqSw67ys8gcDaUzrU43e5h5OEFOaXUKISwp AYICaAlwUYawIQQlNBoJjFDp8myZqqM5rO4n0mpV2v6eulDPouaUbjSwbKfaCF7O3OHpPm1hZG6O ay56CZYEP1z/XGui/ujwGT/sacg1EsWfe9sWXG7O83ymukfiwqMDR58/+mr/aHZ/TwC0eZPe3QKQ bo5byELNzWlNYET01f3PPn2w11fB3znLbzlcEnrxGOcXgK+wQdku4D+PLEdrdtRH6SQJhISx4Wl5 lRon6RvdF7r/LeJH4IMHBOfzzzz5k9/+9vfsUM795kuEHqqFfvt4+Dn/XIFRIR0D0PvgLBgxinxR 2MlyEU2WC9uC8XA2CFPaRj5aDDHHxq1S+NnkSQ8CEMIAv9/sYc1a6kl5Ucgc3yW1VJZwJP6MEoyG 9ZmEdtlEZlbYYw0X9/tphr0SOWTUnDfYF4qFPZamUaaz8RpOf8m4OKDSiP17uwYfW2S0T3Rxkub6 h7CwCQLrXeAHhkvV+WVqBI52Hj26t3+0qPghBPWMZw9QRDU2Fdmf3kOp/cfPvvjjvf31Tz+9R/IJ CG70KYLcFlhlwBdxQOuZhBfEzK9kRncZWeclpNMwll9dE+Je41T62HqKP/HJW6zzzzxNMPo9C9ZQ ANp5+vHwowWQD947A468L0Wkwlt7Fg0t8Qd0Lvb1NjWMMhzinUE+pG/f2sTalB+UspSBNicmQILG D6s9qCPqrBVAN04L6KqXlqQ4khJbfT778fOj/QN8CjayNB9165WuITkK+9XRzIslbcagMb22Ok2e SCs4RYSdISlJ4Q/jZf/V3n4sBGn+4nvsSmWK+/dXOZqg/qHPmibm8/QJvn54+OgLqoD2GS5U5o0T xClR40f1+jZkq/a+HjkaOLr36d7e9gOKY3tUMBF8qH5mu038FVcC02RL7udL6VEctvPBiWAp6sst URtGqSTDWj78Kr9R/bD70r8m+GgAut5MUJVxz7nzTzKOLj7zmPh5ovCSu/gfr+U04Qm3noYnKZAS yTUdzMsgUQQNYgsm0ohTbyLL3AJ4Nm8xaDjt0DqBZZ060cb0YDIkrRnm8+C4zM7K/INvnfo5WJC0 AChVy4vkMwuJBbnQMJWgzmyS2FnVxZgedyjBfqNCPj5T8N8gNhradzI0xKddcY2Mf3i1PV+FVRv6 Gk1J/8XpqA5ZDH4G+weWdwg/jx7QCQwhhz4DQg02JRhVTIxLpt4/orL34cOjI8pZe4Se6v7CLOOn qIiMnoxJCCv8XR9E26EirYrXKyP6z67CoYqi/ValtC9qpkf6rmb3U1GPuvAvTfEHRdD1U/V9zjV9 8+2fJzcAEsZKYalM0eINQMFrdeQLO0BVyrTdpNqbrDbE5rKyOty/UJfowzUQvb6kKNLJ39G9OICE mwqcYdahu6UiN3zovcSxiMl5slakzfHYmJZGJK0wxwyyns4m4foJdgVPNBG70yAdoeFVVOJCupmW 0xfMf9pTA3Y1C0G9DSVBSfyh/v36zZvby5BcIc0w6iUHXjik59He6Cwdmo8TeuR0jf9Q/GeCsXL/ 6NHR10dHD4++3t/bYxPWBeBnmI+MWABEFd4o0HEhracJFZGepr9LXxFxdLutlcmRV7Iw/Q+n0ee7 O/7leP6iKPTLtv/6h25QFTwSewIKtYAAxDFFAlDe5FswKuFAYjIq8fZZUtyWarzQly8pGdBwBsjZ pAy2yd31Jm/EGUabnRPELZvgwxwqiyZ6JzCsh/gEX9AtsgILbYNo7NtLjdu6HLKP6UUq8VnXIKTY RIuOu43O2O8DR33MZET+4vjjhIFSu5b2pu1N8YfwI8l5ijIP9u946vNcPrOg4uLsm9T43qMTGCro AB/+SqjXobkI52cqk9bRo3326FPCz9E+iBhgEd6794DkW/icsSJ6ehUhk1OyhAGDl7rLo6+hH2zx KWSpV3pmtB50p02F9qnqG12fnIKfp88AP23PFnnRrPWP94HjkFTU3JQF/o8hIY2/lT5Nz6mU2+pl eu3EjpQnRTOgmy7WCSs9t+gvexNg6VFyxcREZ52/I37JGM9VWbGwl3l4kGPvhcpbFX/9PDoaydp9 wGiKtxN9/cw864nm8j2RCAs69egYlBgof+HyjuMPzkUa7ze5XsRjjKvtvTX5ExTTvH7nFHYXWqgE DPkM1zcP2++SlME8/gjj+MOI4qv+S5/x+sCDR189evToKwpBR3RcgYvk9Xv39vZWMZvExAEYkvPN AvGHGedKkQGebCeg51JiUVJJzHQCUa2rKYGpeVMtx891gOeM8CMiZBJ0pA6yTKZVjXRdAifvY0Di SONtXMTWtfGIWQtor2c+POGG4wTPZkbnlW26qXULRSUEfO7NJriKIUBNUH3N2sgTvYASigkACHOj fu31+0dSo8bezoDRCghkE5ogzc0HQFruB37o48awFpidlriZ4s9x+wuKP/iCoDp2ZZvDDwaE40QO hOEtbTAQYN49HJqFVDC+CMbVLJy/DiQSEX5GCT3UpD169NnREfAPsiGFn70VKMOskqIwsdpWcJbP mYp7eM8Y4S9R2mY4LTKd52vqMm6nKYVhXFQJp6sfVvLwYwX0k2cRf555XgodDTCFrr5iSKK5YoDO g+cOH994Vr9A2+mD/C/OSia2BNAaOjZmLjJCpKAQw51BKokQipDMOpHNOmXI2MMD64kexCSuRiGg itQGrQG+ZAGGZuUWHItVmWEP9I1JMMKRBo5Xl4xsxPPLnuURCMJg/kzzH6p/SiHkNq61tzerb2j4 kfyFOEDxZxvpa/zmJvADZd16r0TJnnGaZdWl6+Jajr6H2MNxlPC+0N9OrfEOVdmP9gaWMfWinEz4 2ef2b7Uf1d4CuBiYmk7hjBbBG7X0VoV383yObcJZUbm0gskjQWwLX5a1JvaY0Q83InxS/379mbPA z7lfRPAwNgAVDjP2U6mO1kijNbX3PLlALPLaRnjbg5jmS1RLjHwQGzG6Cpboo7S24s37Zg/3+Jvc /050IhP0SE0kSY3gw1U25zOWnlctHN4O2VXvmA2NJtlpbIqW7bbeXxjDJoT+C9XPwI+y5Rpyv5PW p1etnZ+vyS0k8hcNBrkC2q5C07Uuz4TUyNUeMZlHS9lJ1U9dVMvr3A8svf0KzhsOdz6lY6V5nHot LlL42SMh4Yqfm2W6PInkLxHFgHSJ+WaEwxH08SsYrgXV6JLV0JYMqL0WnqzXFE5bv3evGXoYPtc5 Av3dmeCn7afcdYlchdNZUJE6Mkg2AS1WZPuyCFmpFFs1/F9emPaFfWNuBoXNi8RpyeISmg7WS1ii yfSmEVQnEI04NgFGmxqRqEKuQmy7DslAVkxeZiaw7EFGec1oC7W43scmZAMw6l8e28ApK7VjfUzQ mq7Iha3wx5rJh8Kgn68JDaFc2d5D8UPLiXGS6lmGouWESkvzBGtCsFSdkL4LT1WroOr8yNj6zs4r 41TEzy5LsFkX/BAKJmchEk6nFcSso3vSUd5x0dcGkZiIpz01w/No7/XoV1zWgjH5ONEdV6/riPiZ frV5fwEE/ffzZ4KfpyqKB55Be23cvQtcQwNLWkR7ji8yZ1T0BJkayfrDh2OdWqn2zbIBKZJOq1Lx fVTB5GBEot+cxnoEQ7I/RzdOP6rSEFvyAkohVgRnpxpqzmYlpfHkETvG0Th71AKb2n2mxMJPdXV6 DJIliyvSMlZ0f6ESrDqBZhwt1vRDpok/trnJSwmadc/3cgNW5fyFPbzhR4OlrPlEt7yODn50fBRg 7oc9E4lBLXD8mcIwZ3iWfgH6SljHLQELGorARj8kouhHdPtGF7sbFXb4U+ss3qvKJlJEdIpj9bPi Z5Vjj+wvrmsB9KszgQ9WGHjxbXYoYUSG0aVmrxBzVPAahfIiyRcxJHmrs53zsSpi2614ABT9v10m FywzRwpGkwOz81X+SoZIBw9zOKX1oM3v4a9zqozqXA9h8EhcYZpg820LMprMHjmhGYzG+P5Z7Agp P4zRaGlxYZWJEy7ff7XH+1P+Z7lWcttcAD/YTVAKmxgUUfI6h8JeGVrxsIfhbrgBxkX1nlBOgte0 riJ5BLBPoea5R/XPGjLU8HqVGfKIOAQY+iJgWwv8GejzJgIZL5bBuKM4ulExZQsdSXOjS3VQo4m9 0aH0w7mm+CP56ydng5+2n1vqisgJEj24ztHiJ2gnJnsOKZyD1kKpQPL84VI5acozalFZcLxxtoU1 8zBDleo4oDJiXhhZO8rtBDDUyXlMv7y5uJZqtSpv9MINwmVkQa7xZnmdJpv90dSmsesIDSShK7hS iDpNY6hpcZo9yzXmPhWVlb29TTTwWFEQeaBXBe0p81CuIeGl6jiPHriC5mg0od/i8+PBOgvzygEO gXePlhhraCOAHx52LcALBQQxfNaMIyb79DN8WG5sgG+4KZDibrcwFSMuihqxfs7NT/tePLG/OKPy BysMSztBgk3pBR3Opsw+ldRF85osa7/yps1yn9dQJehzttqX/0XmF3KaWBaJpC9FdgUOcmM000EL 1slAmpDqiFb5E50ai7jaoO/38hc79mm9LFohis8MI92lcX0BjUHBj6xe0L+3t2cVtKq3UAm0UMN+ jD6pFYk/28Q6HMehFvtSUAW2PsIWYwu9qJnrmrCsckaEqoq3TpUleBfEQodiJA2i9zcQzvvocLaK nmwRfh44S0b0xHYOnzHGnPp5jwpHagw/AAscKNrCoBF/r6fTf/peffU4gH597ozwwysMH5OPYoAK Nx8spZXehomOY493QUeEwbDns6KHP962Ij6irSnrmQi4jI5sMcJ8I+l7ZE5GwYhOtujyB8GoZ5Or I/567+yxaIRVWicWaejbqLatdvIMm4Z1KjkAHihYq+sQFezFDHhhRRVmG9eadFuyh+ofz/dfNP+Z kIX6dh0XWIPiqDQ7gKaJ0LlfxSdRnYiFUA6kOp9wiRoC42d2aY8AtIHZ2RjZX/Qi8zLIl/iMZ4nd hdghhTmGI3LgIwFUmC1s5YqpqRyOn65eN9A0fmYA/fKM4IMVRiplnJVBnNB8bNtTtNFtq7diyH4m FLGYzuDE22X9Wens6BeF+zoX6i5Kf9jeQ3ZqFVXvRDqD5BOC0TCNHiXgMCNkUyIP1UI9VU1tFKY4 saGtqYuJH2WHdZUvhcjOAlo3yAyuVHiB6nj+3HQ6KPHo6sX12hYXbuUq9V/j20DPRB3KqDwRn1+k smRs+/M9eqEH1ebH+vo619VVrp/VgAmXE8uQUqX+a39/72gGXzljiwg/y8TpHZA4gxA5Kqvj/hFZ IPPPkmiUTicGZFbKhyQr0s42TlWP+rcT9I2zmR7qCkNihNcX2gXnE2r4BeaRYdpfBA4SLkjH7hVA mCg6H3t7K39cth/JBtwpXAmY1ITMbARdHBhFBdgSwUi8vmVizUURzVw2JTRN2BSvh1/CKnbgJJON 3StcMEijFieeOIRZWFGF2a6XkviztO7aghF+pMxwhB8ufrYJl9jHgUFJQxt6KY++aL9JL++StOu8 dKmaZZR0iVQrsagGLv/AfKe8Okv4WZ8BcWdkFp8M/UojdMg8goUMncaOgKMqZOd++S56sRFBEM9J oYyNZ1WSfaNZvkXNv47Tf66/+N/OnRl+niqlZcc8kFFEFm/ah8VcxONnnwchmR3yrlW29tqbhdiV 0dyaReQYJzZBUmC5uE9zcU1beg1EziXlhi0TJo8qHzDfoW0X5DpoPiRV0SaOlekH0q7V63EZhS1I L2giEzikmpgYJFWYZdQ/0l11vZ/NftINIT2zNfUIW92rgs9DdJ494GeW2ijKi6PU0b21c3Ubg5vl qow46zF1YURdFWudZbpvXxb/HCjIz67vL+8fzYAvTrNNWonB18oiC5U4oyOSqAYYMNxEgrTaL6I/ 3E5iH0zvVmQme2x9oeotCzo9tPx1/ay6d11hNOccLqMlJAlIpAZWSHkFC+OJpbwtP2na08FjDED6 a8VqyGYF1vjHjYnzOqcHLzwOIMtSOzhhTtmitoKlbP8Sr5966rzU72T88HYWew8qtev1TVmlKf8a dhM4gFl1Qm7uuHJSuUWe9VohKqGrVD+TlcT4NiFC+QBEvCX8bNNo+Y93KWbMMiOI1qqIdhyE9B1R I7l3Gl1XMTqaMSwR32y9Ak4zqbDRZneRhbL6+lb9nTsQy5fZJyMKNqesdoj0NiaAgnw8FNnG+lZk cdl8faHrr+5/OVH//OTs8NP2C6P4yLwnVbxx3pNYQMFaMoOFhBFnoEGVo82+Fk+uCKnNt44tlk6x qJbdiEKkaROnHP4olV9Gl2dU2XR3MUZ7kAkdVmPaqDILmBhtouDe5L0aDbmreKOcsbyq5L1LL7Tz 0j3SnmMum63pJfcq7r2ogqZOqg59XdZ7Wic73yE6XPhqmwixs736VKXy6q2bZx3qX0g9ja2zGQEs W6n+2T+il95XSPAILJARPkybIV/ug1UKLMMzB7t3Lgzr0GpMhukDHH5EOHNMJf1WsOTgS/+ujuP0 1ZP0nzPr3nmFkWCiPZPLZoMSa+S1L+NmzD601O8BHSXWGCF1WQQDWXf4yGr0ha31BYwhlIpclyZJ wXADKqfSrot4mSa3juK8EAWvZljFfp7DzyZfDHVyj1alQpvI+5hg89SoypeFCys8kdoqul/B8eVO uzE3JJVhiEj5y21h7juH+LNNwNjrrYPVhvgzu7w+NvnC4c7hFahRrc+L5AobmQ5Kzcw/XOegwfkI Wqqo46l/JwRBiL2yRPGH8MMei6sEn8vdJFYzeQGOu4SkuY47uxeIcWLhiDXr8PSxJcgYmQkxsawJ Pt9M//n1GcKHVhiWnxKPnqNN6VLV0pyMYrfu4yYDo0WunHIekdXWPmTte1yKeMmUtnj1rvm30jmR GIcXZq1aKhVbVOb5Z7ZMCHaDfXnrPYqeTi2RJmQpy+QuqCQurOJP7LaKCzcOsSBPEGq3MDRbA/+G rmwmt7e5nunt3a6ypRjO0hcoAFUJevuUcsaWsdWA7Eqv6vUMimnpIvZaet02wO4owM/i/v4I+54s LVTrdEJCE53pyS6ghk4GV2fY8Ls2tXIf394nyu1Kx8GlVagWWflDR5SA0xT/1YSm7buu33P6z/Uz 7t7lDDWbBJapDw+J8SPlSypzdPasHxWacpLy7wU3Prsvk1mSQoWhJdfT3tss4CTT0RcqChJ0jVZE 8YbSGPxcWGMjCr8B9m8CRRqsxE0QZIXVivEwun7q8nuXV3Wd2/3OoT4gWuxYMCIE9decOIzN7XEl vNdL3y5AyXuRj4/W+/uGdt5AKpllyPA7oEYobxj5rFNbNTB2tH0X9Q1tTXiys0T4GaW/jbIyu0D9 +yzwszp5sHvh8i5t6KbuADZ33FqNcbQ7vTqFyLRL0iFQPARLBdeTyGtT/IVVOw0/Gf3n1Rd5/3WG 3busMNJAOQKDSxOpnwvZncZE1HSpweQmbrYKHTnHFFZyAmOekI6jmTkUmqHipbbSuinoQfWJVt9q orKMvDY9dtUoZOozzgvlYYPEOWdpD6L0+k1MjjaZkba4Sv0PotelzcPmR2BEEFoMHPYqVD8jtOzV B2n7vrAugpdQWFlaerd9nRLJEgR3sT7h4INZs8in0oyIJow3P//iyl0chvSvi/MtFUDrD8GInyGT cUqlVIhPrg5PTzcu3yfBEIHN/cYM44hC0tSBBKRhKan75g78Kl2y0ffXIL6Q0Tc6MvrYRsLPdcbP GXbvKgStk708xhQhDzuxzNWU5nzzS+xdc9gIUoyXWvAkRJTZCDL9X87waVsPIa4Fq5R81tvZ/kMk HMyCw9xZo0+97GjBEBmbHQQBpFMHRFXqv8DfLopLyyfww+93dmj/JSyUub35wb06W5EOzu/TG53x L7KM0/KbqIdpMdHLBpOMmXm2e+P3hJ++z0nkYuf1z1+5S0fZUEOgkSCVP2MeWz7S5IdyFibKBKGD y97Cz+Xdro7LuwdAjhNAXb40KU3X5MHl+2zpMDa2xsSFU9XrjP6T6Bt/c6bwaTu/kecLCzVGL1Qa tBNikK5PI9fVTjZSz5+FshDbMmncgm7EtKoK4GGGY8x875vrrWC/VXNU4izmbfdbmohwpdRjfQB3 y5lKLFdGGGB34nCod1X5kF1jOwqZ489EgJk8/Ts3v8el8fIgNlfztHPf7q3z4hyjbWSzRRaMH2T8 iNGknpEMrOM6eIfssffmBsSAYLSfmrA+fMIbC+S+3EvjQ/RTk9O7u2srK0HQcrlx5/LB/fu7l3cp Dt05oAzWPYehz/Bkg2rrjalpSBpviIHZafT5jP4j1J/rT58tftp+ViTyhpY2wbvgT1Ql8SqMr53T rCclP7v/8akkCtLiu5x/H2JHFaQkQjjQI9dg/6dwHDkjpsjIdVOIvxauGRlcXiVkimLLGjOTOK+Y kP4GGNL9871z6lrfWIGBAV7kw2M4ulUrWYC1nBvcZ03v+X1srvbq1MvjOLC3Pk+TbRaMhyjPPOvz LPOedFnsS2m03Df7AhdVVwcmafc/i+uR0dn9xT5s/zcW5jEhWhwFwWN47fIBqa5R0czlDkWd+8DP QYPgc58CUW2ObRinUBzRKRsJc4zMcNauxfKnkeWv1WPr0zOijmU6CmlfpZVOoV/2TgaFNhRypbXg DDV052WwsOG1H7MuzBb2OhPwslPNhj9lBsiERmfrMv19HdKYS+z+PCh6Kck5f1YsLqmhorrAGG8G yW1LXMJI91f0RRqVl6Egzo2Xwkift4Pqr80N7oF9MUhzG6qhx6sk/TNORdT2BDSA9up0F4iHqGIc dBg7UI1nC9PZvuGJq+2EuDny/gC9cAnOgft91NoVa7QUBn7WSW5trK9x2RE5ksCzSynsArVjF4Cf xv3Ld+4DP1OT0D2fQ3q7szazNkmF+Qz/4ZrpY0r/6Z471r7/jzOGD6yYXSIV2q4Tr68PVulY3jGA GZ0+K15E3oVzTjiRh5iElmerEJQqJCVzKOJwO8+CwWejqEJHnKiO+HyoPH6YJqbtrG9hSc6GjhU5 DinUDQ9QanTcvmb271fbDUQoot8JrG1M9c/gPpUy+/sEICjJVamdJ7jQIAmwIfiQudxNlojGe5Jv NrF4VmteJ/mrN8m8oU8IbVhIUAHUhwy7RpV9FbUU7NDGDu5AXGZXuy4aAh1cIOxQ80XfHlBsws50 lWdDuzOVtUlq5Gd4YnFSvQ4TxOGsfD7z7p07+Odlv5XJJ/BrpF/jQbrt+BpLZZ29xHKokQ14fNO3 Lr78zjfRX4U8HSx++RNHsPprYFDkbFLpS7k8y7o8xmLwNrTkbMlk3FI941Rjnx1HKqX4EoL9cP/W laGhq/kCjIPR1aHbOnVyc/v7NLihqR9pWFarBJ6JcSzEoMoKBJGcHex1t4EefJeD0SBfevG8Z5Tt JsYGFD4DA6Q6NYzlzNTyIC/IiP88MjC8u4bFluCnaxKl8y66Lwo3F+7fubyBhftkRdA146YQf+Qw o/Yn6D/Xufx58qzxIx28LzQiuHwTikSWpDlEryydEAbbemQJJvI1Mn6H7c5sVeZ9nBJ42fd7O4TV ojlt7m1R4hjIWVIsbayUd/lxGMW3RHJnIRo1ghmmhCghu3Fh9uX3BUBROhOK2kNXbs9UPOtbTO7v Y2azz8UPjSAJMcIG2gYlGpiR2HPzJiNJvsWzN4jktCT8NV6p8wbraBbGgWVlep6Pssl3h8w6Ji9h b9onAWZymIeHl+/MrV3mhHZnklncu9rTV0CJGuMA6k+jb2TqP9ffQvg54+6dR9AhFNl1YCxHfGh+ cSLTQxMY3mQE45vHQoYwF0MWNBlc8/araAZYMAmZEFe1PkgShZOJzwJYjINBZwDOxtqyNBHYBftv SeDB9ieymm1cmnzjPRKzhYTtxSEAiA/ir77/8u01VQedJBGEfaItz+/RdotYQCiggaFt1mcZ56Bz U6OPRCCBD0UqFkxYUvyM4l/4j/avTyI+Ts9jVYaqifQ+ZcU+MNxNCWulr2+DgTLF9TI9FeZrHGhz 5iuIP8OyvzlFPTNX/3nrY+Dnb9rO/vkFSb7XQkgzFs0GQTenemsRmgQ5slDg85ZK6UBpSN08HCo4 HXIUC1Y6e6cvu8/oij7y0tIJUQS1DspdAmvQcMYESRsalNnmLh3vqw18rWPmndcQgIYEQuRkBGGu ofc+eGdNlrblJN0eH1H4mUfFw5t4JrPWGS/jihaSl5I4dDMBaI8RNL8AZiGO1ZbAI4TAKhnf0l3H yjIR3EDwoK083x8x7wdWwRSI0IhVaLEqcYj5PjOXdyUA1XDGOzAsQ4o/gZ9rH6P+efo7wM/5Z3+2 BmvsmjRP3jpq0cSS1xkBIjTNg2NLbUOaEGeHcRGaZ7E4I8RsUHBQxgZfx8r6usvqVHorvcOXj8TK lRNnmcAVfxef5T2X/d5eFxaq+qXysoSf7s3bLzN4hoCdIbwNvf/+lRvvTKnTxxz0VxhBNAdC7bMN NtA44DKxHcugFHpubt8V/Nysoi2jvh+8n4UFqp8WxL9raRXDhlVoKTI1cXAWor0gHIJoOwJ9ugGa SaMgasjyApyfg8trJDtyH2sy6PwOC2H8VPWo7gXrvgg/18++e9ca+pmnnvjbXzw3E2p4Ixz5jC8W M4dTfQUtXVyqo5WNqPsxvOYKpiA7CyVEB2UGRT40Vz9BgWUMxJg3PZ+6Ok2pobBfNcuUXKAFHhvE 0TiHHxtnFxLudJ9XypiC+7JG14XRtz98j3RtL5KsJMkBDg1dI/RcufHR22s8N3JuEtjZ43/2j2gE VMUMaPuWIIhO42PVrN/cjWDapo3rHtOfB5l+OA8UEd1wZAUjqlW6pa/D2gLOQrjIXmIFLYQpTnZ8 uTO26jeGmSC0crkBSaPG5V1YUg9Q/uIRxKnqdZfmFT9vtF//Lrr34yh6llCkochrXeS4y4n3YRmm UgbSGY9SyvJ8FVI94stsD68n9P5YuDBGoudOO+3TkF5DJIAUaVJuEyTu54MRTkoOm5kolqREh4GR LmJrXZem37390QdX3n9/CGZ8VP4AQO+9/NE7UzxYcn5tro+jj5TQGCJyKU2TH4SeCSt87lrmyjIY PSTUyq5e+/Ag2F8g7hiVOXB/p7k2Zy86i69irUrPuh5mL62zKqQ6dUOBn96mO2BdNTdDq4xJ6PpO igReV8fx8XM3q/988ipW8C8dov36Sdt3/hCKfqooCvxlHTRHoLDg0OG4cE5HhzlWyiJjAXn9H/Hq 2sGYkEP4V5N5s48s/JBoIjwWYgRo6vI5GuNVUXFsTO6V/OiVzSZBpxSgGl9Sr20JP90T797+8IMr 77333vsUeYbev3bt/feuvPfR7WmGL69ESPKBiNcPE4pww8XvSVSMITS+rVMgng5tG372qvQP+wkO DtLugw7gKYYRfqD8M0cxaRCDyWovZMJx0A8MAT+zi6zZp2I1I6wOAeIY9fDTVF5Dh3V0jrcyzep1 HV3H6D+HO9ienm/7Cz3nGUXPl1oVhRB8om1wTtAa2WdFt7XhwcbXXrj3qaV3waWZs80Cgu1UhVXm dF7NdbPLiikr40W/wSt/2vhn2XWaMGxFNMRFwAUrmfjnQo2+ZIdvEYA+unHjg/deIBARdl544YMb hJ+K6OjGc1ncEhHxWtIZ4+frPXskZ9UFNeMMozqQVN/GjIfZZfPg3e9TMTQyAPefYhKcevIdnK9z 6qIItI4oREYq9P11/jGyGndvA4wiOaFFMqN+bQ4aLt+gfgj6DyD08c41Gv78uu0v9vDc4PyTT/30 5z973mss4i935+0ilaU3XdP1TpoW6hGrEJtd07be2iufDyyt8pGJodABIt9Rko9txYL14T77fbLr a13hx3GDfgjPQl3QEQKp53R0d1Q333z7w48IQh99cIPePrjx4Ye3b7+7oto7Ik0oaqJOlEOmJRhx EHoQQ9EDhdG2xiCqnut7dTRZeyiDBvkCleLP0gCUU+l8eYGvoYkChJsvaPYtiQnPKB8xsyItjsD6 5YiQB0R6ik2DyFU+kPMn1qeUvrorGn+uHV78ToaHf1YsevKJv/15atF03lsa9TlInWpjZM5WIR6o atErbVapp6lZjRQSSU1O0EJc6otGVYgMMyO66aiBekMv5Y7LD4fSrZAP2UaEPrTCNyE8QQBGOy5d uj9HBgvvvn37QzyvfQjw3L799pur3jjZYm0lgvCKIcodONbve/j1vsLnQXwHFFUlDFV7e1VKDRls fm9/cGF//WhgDV8GfQvrFH2WB/tZigbLDrGnZAjh4HFkHQf9o1BT4PswymID3InBgbN/FQbEFX+q ep3Qfz55dYfwc/0f2v56nnOCoo2GZjQtV1xIY2VNb1y76tRI9w+2utJyRl9ro9trK+aaSUamv3Ds ylXPq7OzalmAYDqtPLaEPx/JTF5vXlO95BHy7/cRVZFi0DtwHKF/yXrk7TdvzWFq6XI/EFVNFzZ/ oQ4ExAsZHjjaRzJ7sLf/QLLaAwYV9+/UglX3BvkNl8yIPyNrqPMJPwvr89AshiDIgj502iGy6qLE QW8jip+ReIKKYqh/mtNqkL1pQ1mI0n+B/sPp6/Dw2vW/RPX8J4troOi5DQZRjesi+XouQho3Z52/ 83GNFYd/4VjVbfTruKZ1VuhKx97E/aGPLW1DFnyamUeGWWjatnBjh/oZkwSdJNp8ACuj+xfmBpZI yerWm+I3BeOXW51zpeFTR48smc5KhbxaUwt53FLqfeyRltXapkHseR9Vc1349AssQEaLtCNW/u4j 1GDBQbUy32bwhQZf+fCBx8KslNIsJdLP5ikms9aHKfYU72LCMfN3Wb8L/eeT1w93Xnnxf7b9lT6n NPrKMsvAE4rmw51MqkEnP7p858GPU2q1lS0qGmyVUSY65JsptE1nR0FV9oiAIb94Juxo9H6v9bdj /Nw/uFQhzQ+SCKuzUiyJVk9U57IWUkgspRdRmngBgogU1Oqk3PLF1ga3aXupz+d2jfs2qp+rKKFJ gmN0A/yfPmqzUDGv86ZeKEPLzEAT+Vmipq3LAb+o0pCslIiGipTCFHEPi2+gHyr9h+7TXnnxqba/ 6kcb/Uot9Wh57PEhSpnpONL5JBCTldPBx0DlUvfv1BuiaVGWSIrxbsO7DKC+1Co8CIfSabgKWWyK IlgNxs/9gzt3du8cdGOz3b/IVe3gMH4RXeKArwykxOYAJ468+AuiA8+q8cxSI4oRV0YjR0DP19zw H8m/6/y2t95P+AmVMVJqISfcdR5Owx4OV0DyHgR8BKNFyWiCoXU2eBiwdf4ac2Vqp6hndndPIn99 cp1oKC//U9v34dFG3zesuA7ehjz8Hb3xKmM2S1u24BIl0cViOFiTb2kv3tUH4eIHDSGl0k5EALbg 6WQGTB0nOd+U07hyE3oa8HNB8MPrpd3dO/drayCoz/lSz91C+gUCJzElQ5b8ealtmleRx4JtCba4 2+c27Ugm1/Z2NHL0cIYioxvD6fwSe53C7nSe/2HkDi7zHccyq/QRZ3+BT16JK8uqRrgAIyb9Bno4 dyp9o1voP28QpfL1J9u+P482+o5jUS3Y9U/TocWx6sQuLnjdoatQX+QkaZ8LMfjYuEVJ6qBNn9Xd peztVPojkZSy6Wa8rw2Mn+77hp/dy0qRINLEwSW93Q+2gw0yWvc2TXJyiCYqzULCFMa+F9GHisyM KBiRdMg6xZ8jwIeehyyDSXXMyCgJg3AXj5sxiHOqay6GRb18xrEgVFhWD5IZI1mB4LZ5hlUBTuAH 9gXdfVw/X6X486u279tzTlEUBEMhLqVi2ePlDlU1GoKc0cdu30V6fJxMigxDcJK2TAktXd0rI0gi lvO5wJWLixGlNAnVSNfB9Fa7FPPXrkYgBdEd6/+dMI8sMwf5zHkJ653UbOxVpIy1ihijsR+TN4Gs CnzxxgRADx/yZpZngWOwU5CLQyDIXJfnDUXQG5rH3Ejkp9h+r78fFdAM7FQYP83mg+T9dWkA8LlO 59U7z7R9H59z1ug/j1BU81xei4ZHVj6nox4fsiKHj6AlFB1TnEoUWj358j7xzHzcZFgZpsTb45eJ aQ6ODwZ+JH/dkfjD4ME3d4LuP3w8749Hls72OWw0YNMGnTQyZzbIsT5qePbhE6fTyhpGj324iy5o C8E3yejP+6mI5ijEjRoDSdy74bmsB4ncmaHnpxBEXVhFzE9PMX/vvjSK9EXuQIe/bfteP4yiX6DR RzRi0oaPtzzNp8ouEV8zYWCnyUy3r85YJenDXXOpnhElMw5coXuMorTJpA6n6de0/LUrASglsd3A MsvpLklobFzUBVOTMF6UeOcxKCksSNDZcoXZFenZNc+xzKMaTgompc9CZhA2p0AzOCFFkMYi1EHL uEnkw6BFlgldoo283E6eZr7cTfQfKp9fJvw80/Y9f86lRn8jaHHtbalf2lI+5ZuQy+CFXAIkWOce CW1ak9uHZGt2TTOuiAJFRpwsNJsKuZ5WKoKfA40/Ah+JQ7s14cqWieIfQoxnpQ8uY3FHPjjXzxgJ 4bekgAMsOWGlOJGXR9jYYmU+BCPMcmTAM8o4AnODYLTMxu+DVQHSMt8IcTiiXp8aMdLsEKfrk/SN boo/i4g/O9/78PNNG30ujCSdBaNfKD9eSx4XjEWUhZA4Ako6RUZFs1uQYO8LFbxycfmVBEAiM5Ej Ii3gLzB+dpufy7uNIh1V2/Y1mC9MkYmJOhdcyMYLqsceSnP09OJQ5MRoRMwcxTyZC+zp5GzKJFeR jaIlxgIiEZ10oJaWLMaqjrRT7ZsUGaRG8/hQ8NPxr7w7PTw83/YDe86dz1GksSgeFfJ4zjeRe7w1 PL5JVdj6ebtPC7lieayygq1wbR4pzMUirvORcBDzm+JPjEIdGZU7yMWH0gmCah55lDdJe00/jgdQ QhDXDk32dKrFL7L2bP8ummpAVzEDfXzYvgtbWvUQBxCNFlEEMX4GaV9PIoq08xiYg0bkKfRDgk93 F+g/r+38oMLPiRbtWW30EYpcFG4IITJ2ms7rS59XM/zlL+Rpl0aKopJ37FhfD8V8Ij7GgYAQAuhk Qctne7PnUqkHakqyLCNF18ciKtgcKRhzVkncuvGjVJaOukVDnZHjzKXaqWIfW8FrMAJHWkLRiJz8 wMWTXfTkqhUyHpP4JbeKZvNBxs+lSw2oR11t3znX9kN+UFwrL0Qmjt54PL65tubj5WDMH+eCT5fU KUQE47pqXW1ENG8tGV8452sPKZy6LjXjJ0Hoggmq22ovmX34eCpgwdGEaDxnTfP/VK515GbDgMl7 GReVYmnhFFbi8cW92xYHI9wocx5jLqvKIdKyHkv6fjoB4oq8cYK+Qfjx1L1/3H74dNuP4LFGv7DJ tXTWLtNH5OpXJiwhEw1Wxr3LqWU6QCqLZsMyFwcGLr98RvryDcMPP7t3Ugy6HzIegJKstXQqpZJy SubXT7uMkvt2ExLDoc4gA/tyR7ZbLKZLIQcjEFXY9pQLbSmwB2wBz/JRA6qLOMcOhu6EeSXwM/PJ J6/+4+Hv2348D1P3f/7cmrVoaT9qgg76M2V2AOZDYVPHkmvxEHyTfZlXC8UQuzc9Q0vDJu9rl6z8 if9IDLpzXzJnMAZJTKili5qPcUgg10ohodrrb62ruEKWOuKoVoYkXWPmR6rD50q1dRSriwpsCrUy YtdFEdgkJQVeuZ2kH6L92qD48/S5c20/tidr9IPeEZVcd7LuC95Zq9YszhjvW+1KQ0bCusUQCr7M s2OblEQbatp+nXwOpMMKhbVNvIHX+TWHIK3wdafilcDkRfCBPvNSTkr0DryUC5CQy3TZOaWw+ZG/ 4nCUgxK3b+jSuNuf7BNzKhr/TDkmkhxfvxN6Oi5NffLJP5xv+7E+5xVFNVuAmHeC9fpCoggnL8vS bt3zC1gk6pkFD+fysw1GAeLPafDZPdCaRUbZTosoGiHKBixt1lgoMveqZggHJxCG9J7+d9OtcVEC 28JoEXVrhKYmA8mKnlp7EUsj8CIYwYa8b43H3b6rcaz+uXSpo2P6k1d/1fYjf3SjrwcgNVEFzk+k beUR9JI9GgClFsw76+/1FiOOk4UZwPPthJ8DvB3gW4s/gpUmPRv021I9hyKxLpuNZQAY7eF1oC6l WakrOR7+ONUHEdFGLdPYl7tULf5yq5QNWylGsvxzSHQIRjOc306YDwI+HUT/+Ulb60kbfR4XKUvN ZRuLUreXiRkfm/x4/CzdW5lp5TGPR/nb9M2lC5K+DrKHMXRQKwufFv/yawQfsuNFXaP5XJImass6 O6kNzBBg3w9jNemGWH6iFCYws1F8dNwLSh8RqdmKzMt5iA3yrPjLHsdPN/DTNfnqPzzTws6JFq1S S9z9eI+YLT+CKd15Tl8+0+YokiaxUPJl489f5x1c/jBomp47d2qJm83UaiEzhTj6DprfLB/yzDke VMsEHDksJElblONS7vNQKWTHuLawYf0HJqZJl28dJQ/BnO5pndBnj+MH0aeja/iTX7dQ800ocqnR 5798ZwgqQ8ZItEGP3fsU2QjHN58NAT8nHk5mDU2SPrkOaQ8o4iMuKu/rGMGL/3BhdLNQWAkke2MF uI/FdDDndFmtuUSRK5KCQClKj67csmUs66I1m1cq/5DniH2f/LKFl/8URWtxXBSScoMZvASzEjf+ WpN6UVz268+VXQyY+9mbPV1xgKPzmcwzKJ5/hCZfmKB2Rco1s80rQ8cVKU7qxxUhTbRkERPSIRuE IYMsdcrC3GMK3fWLdvFx83eZQw+8er6FlD85LtKNPjVp3nKaC1E2MSapKLGXbFpjruAjCuDnfvOj GOooskujMjGXnF1gez1ICuzr6YyO63LtrHi3aEQkJ5TdYOAsk5eRV4KJXFva4kzmnKiWdegezSSO 0Vft+bd/akHk242LanbPGJpaeeePKXXYi2eXHQglNd7An3wODrqDnA8VFudMUyQky4aMVxYsXQWf kfRDVDEWeqJaV+U3cCZBoWgsfWYFGXSwros16eNw0Mh/juP5S/HzZAscj8MLQaOfVbM+F87jV8zF 84nYPknlFILc3jXHoAuFEUCcEdMk+IjKcOS9BSO78UxRqntpyrOsGaRgZs0JUZjlakiGo6L074NZ zPBxUdnkaRN8dqorEwsXmvETOIV1df2vFioeB0WxRavJNjbyla3PLo0yJFL73l6ttNvQC05F0IUi 9wDxabhtCrVF3EmYkCdXPU6viDggxrmP0pE0kwV2em12s9KIhOZLHR6NHJJ/RIiF03H3ixn94VMt PDz+0JFQVCpHjVtrl9NAnH5ZyxG+0GPTabX6jtcaltGKuJFV90X51aJHlTfvRA02TkVgbWrepD9c slQkL0BUUY1DmWTewozPgwn1Odv8e1NHL4tci9KfyF+NGUlhv2sB4fHjEKOIG/1SQ1Fcn1smKRPP MV6O5RrpgjtIuyQam08Sj0EXbN6nCw/evHPMw51hmSQebEfHKwuXGEgxtnhRP5cQoxzMMuoQM3lI WE4h9v829i6P4Ud/cKkVfv5rRtes9JDphcRSOstax1j8RTaZZKFxrZBcImIryFx0gbFFhlc9dGcj BR8dZOyELPDgJihNSGngUdO2tHpcdW193gE4l1OtXdjiz6Rp+1UTALXCz38tinLqfkwsccvArXHS Bk7/1aQak91HUEW9MqlPi+aHso3MK7j0NiH3ZZLJTvroLm7tS4k5QdzLJUhFzfZk42DKFPzrOhMt BtmjCT/qgtEKP2cxdLQbfYZRaSRUPmUsk5WGFtm6XRCRDdFBQ3PllD8WRT5kshc3I+igZDtiKI2V UghllH5IfIEy6j54OWcT8zSfnSTJljW6V6kWJKpr1HKNk+3737de77NE0fOVSN33kS5YRCIR/5xT jc+MQGRKLtFEsQw6EBCYhZCBQt084ocn5km8HzIZrcJGhy6UyXQt8anLIiNYFrnJNVdGjROzQ98a PZ8ximjoyNfVzSnNp1GOUC+8iLXwIsIJvVAkX+yK2snSigfKgdWqzSAk2XcmPyufi67LCNCGyRzV VExUEmAZfM5xs/pLhBsSA7fwJ/DT+EPrJf5OUSRC19FwqiiyK3znohSw3RLxAFuKHnVydVKnlIWN eZI2sd3b63DJqZp/VPnUVl7AFkKIisUx9oA0kmYAzqRIyqC21Mfx06ica7243yWKftok6ZhmvEUm OyTcM1WICSrtmJkluuQ7E0fSyv3xVmap0GgO0SBnJenWyOmxB1KZE8EA3s8HKGGFmNlyicdGXjrT +9oTrVf1L4OijXjQKDp3Qbfs6jCeveS5yqveWegyg00ZQlKIDT4dNUpvjxa9dKaKFryNm0zWw4nA duJzB5HcE6Uj3FsLfcnoQin+hI2ZjZ892+q9/qIogndDHF2HOPPhiXBwNghKCmpSGklucz7n8nPl XLpE+JB6ybsiX70r4VZjUslVeBB3l+BEIt8obMGpL1r83X1T/fzs+WdalfNfHkVYgDxnW7Q0xyn4 lCLeWXu9YXQaNsqoTQ2yYZAXuUxOnEE9pe3AJwi2OPtZjyYTQyWn6dZdgp8Msp3o3Wqg0wvYiJ/W zuuvDkWuIaozQetrZ0zTuNBythpxaphgKrCyLY3lig/JRspchdVHoSxiSCrFKVQdPUsZfTuvp0DB fI11AyxTrIif51rB568SRay6z0u0uHgPUSw4mVnZTZpsP4NLR2q2YGepCFtGRE2qYE2b/A56kRhy JeIykYHEjZZbPWXCGn46nm29Xn/NKCpq8RrNXu7k1+Fywz1RmHXcbgXbqhaZ6XQZ4nmIL/RyxxBV RBPGUIQiN1/QKaYQXNnEjE/8G6309T1CEW/RtDASoiErM5ZFtO0tXLakNen6tIYvMu3iROhXJ2lB mujBsJSZLjdK1Ukq8z0viyrG+rnxfCt9fW9QFEMRUw3L5G3mwjGRM+UaBVuK6mZLKPOlnOQ0C9L6 aHfFezlR/JPQFZxTyRHVtsYlo8Wfn7denO8Viv7++Wz7oTa9QbVdeEMfXD4q1H1IZgpTMtxc5InZ TVmwqlxSnAuZppBWTrJdk2G2xZ/W2PB7iaLf1EzpQemymf1vyCgiQUlmygHhji0CRaghbGaksq7K /eceTEkBkjCd1epC1ffgbwBCrfT1fX0URdbom2iVi6cZRggTaXwT1lP1Vplxh/wUIzvfsCF3yG/r i0JVPfgDW+nrBxWLkl66vealDXoyAqOJG5VlsqpOmnzOejgvh/qqJ6qMxIg6Iapp/mqlrx8Cis4D RUYM8SY8o414MIeqKCzDquJi0evCMUHseLzmdF6pstnOxIxVEFLws9ZSS/jhPAlFMRIV6RY/NKmn y9G7D87uLOS0y/MZUOmig2wU60uCxiUPpBut9PUDRlGZqI42PnZGLctE9jO2K8+0y0ShVZtqFw0X vDHLpDRqtLqvHzaKnvj731Sy0XUUSVdRtahAZuRHGx+Fskj72pDSWtSXlX6P8TPTSl8/eBT9rhJl Z4wkW6aDryLhiP1bQ+Rlh6ASEGo2XRrVg9etgp9ftP6KfywomlFjtGMqZs5nilRlkwC/XcCHaHdW JDdZqZ9b6etH1KQ99cQffve8JrQkim4yVD7psrpo/1HmhlMciIKIUQW+/2qlrx8nin4j7tXHray8 2uZFhfsyapQp0dqr8pqX/UUrff2oUSSj6+iM54soT2XfRAJ2Ur52KveL9PXT1t/kjx1FPDCSyXWk 3PvMDM3oH2Xm3REUP64lFtV6aJEGFJk2qOc7MN/sECMrL5FE08U94afxs9bFV+uJKHrqDzwwChnB ETYIW6Wo5YV0W8+kolb6aj2no+h3FWn0RdJVCUaqFMy8tQACB4m2tNJX6/mGgdEfMDDS64+QlhzR ZxH1z89af1Gt5z9FEY0dn1eJ4mAywqVypLsarfTVev5MFP0mqoU46826Gi3mYev5Nq2+oKjQNXxL Lar1PEYsoiaNV2K+0dpdtJ7/j1j0XYaf/we32s0ldu83sQAAAABJRU5ErkJggg=="
            preserveAspectRatio="none"
            height={127.52917}
            width={152.13542}
            x={47.712765}
            y={62.890678}
          />
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
