import React from 'react';

const BotonEst = () => {
  return (
    <g
      id="st2"
      fill="#00990c"
      //className={classEstEq}
      fillOpacity="1"
      display="inline"
      transform="translate(-1.131 2.644)"
    >
      <g
        id="ats_st2"
        //fill="#00990c"
        fillOpacity="1"
        stroke="none"
        strokeOpacity="1"
        display="inline"
        transform="translate(-.035 -.19)"
      >
        <path
          id="path15781"
          strokeDasharray="0.0336987, 0.0168493"
          strokeDashoffset="0"
          strokeLinejoin="round"
          strokeMiterlimit="4"
          strokeWidth="0.017"
          d="M213.683 169.146a5.86 5.86 0 00-4.066 1.662 5.7 5.7 0 00-1.684 4.01 5.7 5.7 0 001.684 4.01 5.86 5.86 0 004.066 1.662 5.86 5.86 0 004.066-1.662 5.7 5.7 0 001.685-4.01 5.7 5.7 0 00-1.685-4.01 5.86 5.86 0 00-4.066-1.662zm0 .835a5.03 5.03 0 013.475 1.42 4.877 4.877 0 011.435 3.417 4.879 4.879 0 01-1.435 3.418 5.03 5.03 0 01-3.475 1.419 5.03 5.03 0 01-3.475-1.42 4.879 4.879 0 01-1.435-3.417c0-1.259.531-2.526 1.435-3.418a5.03 5.03 0 013.475-1.42z"
        ></path>
        <path
          id="path2489"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeWidth="0.265"
          d="M601.873 320.96c.063-1.311 2.784-1.358 2.69.07-.033.121 0 9.402 0 9.402-.347 1.077-2.153 1.292-2.713 0z"
          filter="url(#filter2703)"
          transform="matrix(.14885 0 0 .14885 123.933 125.484)"
        ></path>
        <path
          id="path2649"
          strokeLinecap="butt"
          strokeLinejoin="miter"
          strokeWidth="0.265"
          d="M597.565 325.633c1.698.334 1.59 1.335 1.404 2.364-1.342 1.49-2.997 2.762-2.868 5.287.434 2.218.972 4.389 3.417 5.673 2.377 1.244 4.814 1.155 6.562.177 1.889-1.079 3.77-3.085 3.728-5.85-.088-2.4-1.275-4.226-3.313-5.607.085-.983-.099-2.122 2.023-1.917 2.272 1.959 4.04 4.21 3.95 7.536-.001 3.557-1.8 6.426-5.654 8.51-2.77 1.343-5.445.865-8.09-.168-2.57-1.48-5.569-3.356-5.217-9.528.748-3.306 2.154-5.363 4.058-6.477z"
          filter="url(#filter2663)"
          transform="matrix(.14885 0 0 .14885 123.933 125.484)"
        ></path>
      </g>
    </g>
  );
};

export default BotonEst;
