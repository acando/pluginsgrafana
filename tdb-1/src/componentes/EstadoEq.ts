import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    EquipoOn: css`
      fill: green;
    `,
    EquipoOff: css`
      fill: #f51628;
    `,
    rectOn: css`
      fill: #1aea78;
    `,
    rectOff: css`
      fill: gray;
    `,
  };
});

const stylesEq = getStyles();

export default stylesEq;
