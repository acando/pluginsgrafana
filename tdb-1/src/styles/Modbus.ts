import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    ModbusON: css`
      fill: red;
    `,
    ModbusOff: css`
      fill: gray;
    `,
  };
});

const stylesMod = getStyles();

export default stylesMod;
