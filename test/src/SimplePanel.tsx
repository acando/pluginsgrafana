import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';
import './css/stylesPop.css';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  
  
  console.log(data)
  //data.series.find(({ name }) => name === 'Average DATA.TOT_FAULT.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let variables= data.series[0]?.fields[3]?.values?.buffer
  let variables2= data.series.find(({ refId }) => refId === 'uma')?.fields.find(({ name }) => name === 'estado')?.values?.buffer
  console.log("uno",variables)
  console.log("dos",variables2)
  for(let i=0;i<10;i++){
    console.log(variables[i])
  }
  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
   
   
   <h1>hola</h1>


    
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
