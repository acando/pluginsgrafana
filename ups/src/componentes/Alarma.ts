import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    circuloOn: css`
      fill: #f51628;
    `,
    circuloOff: css`
      fill: gray;
    `,
    botonOn: css`
      fill: #48ee8e;
    `,
    botonOff: css`
      fill: #d10818;
    `,
    rectOn: css`
      fill: #1aea78;
    `,
    rectOff: css`
      fill: gray;
    `,
  };
});

const alm = getStyles();

export default alm;
